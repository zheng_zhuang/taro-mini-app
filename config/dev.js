module.exports = {
  env: {
    NODE_ENV: '"development"',
    REQUEST_ENV: '"'+`${process.env.WK_ENV}`+'"'
  },
  defineConstants: {
  },
  mini: {
    optimizeMainPackage: {
      enable: true
    },
    // webpackChain: (chain, webpack) => {
    //   chain.merge({
    //     plugin: {
    //       install: {
    //         plugin: require('terser-webpack-plugin'),
    //         args: [
    //           {
    //             terserOptions: {
    //               compress: true, // 默认使用terser压缩
    //               keep_classnames: true, // 不改变class名称
    //               keep_fnames: true, // 不改变函数名称
    //             },
    //           },
    //         ],
    //       },
    //     },
    //   })
    // },
    webpackChain(chain) {
      chain.merge({
        plugin: {
          // install: {
          //   plugin: require('terser-webpack-plugin'),
          //   args: [
          //     {
          //       terserOptions: {
          //         compress: true, // 默认使用terser压缩
          //         // mangle: false,
          //         keep_classnames: true, // 不改变class名称
          //         keep_fnames: true // 不改变函数名称
          //       }
          //     }
          //   ]
          // }
        }
      })

      // chain.plugin('analyzer').use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [
      //   {
      //     analyzerPort: 8888,
      //     openAnalyzer: false
      //   }
      // ])
    }
  },
  h5: {}
}
