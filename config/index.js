const express = require('express');
const path = require('path');

function configDistName(env = process.env.TARO_ENV, pre = process.env.DIST_NAME) {
  let outputRoot = '';
  switch (env) {
    case 'h5':
      outputRoot = `${process.env.TARO_ENV}${pre ? '_' + pre : ''}`;
      break;
    default:
      outputRoot = `${process.env.TARO_ENV}`;
  }
  return outputRoot;
}

const setDistName = configDistName();

/**
 * 获取环境配置
 */
function getEnv() {
  const base = require('./env.js');
  try {
    const local = require('./env.local.js');
    return {...base, ...local};
  } catch (err) {
  }
  return base;
}

function stringifyEnv(object) {
  const newobj = {};
  for (const key in object) {
    newobj[`process.env.${key}`] = JSON.stringify(object[key]);
  }
  return newobj;
}

/**
 * 本地配置信息
 */
const env = getEnv();
const config = {
  framework: 'react',
  plugins: [
    // 'taro-plugin-react-devtools',
    // ['taro-plugin-polymorphic', {typeName: 'INDUSTRY'}],
    // 'taro-plugin-webpack-analyze',
    // 'taro-plugin-wk'
  ],
  alias: {
    '@': path.resolve(__dirname, '..', 'src'),
  },

  projectName: 'wkb-minapp',
  date: '2020-5-18',
  designWidth: 750,
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
  },

  terser: {
    enable: true,
    config: {
      // 配置项同 https://github.com/terser/terser#minify-options
      output: {
        keep_quoted_props: false,
        quote_keys: false,
      },
    },
  },

  sourceRoot: 'src',
  outputRoot: `dist/${setDistName}`,

  defineConstants: stringifyEnv(env),

  mini: {
    optimizeMainPackage: {
      enable: true
    },
    postcss: {
      pxtransform: {
        enable: true,
        config: {
          selectorBlackList: [/share-canvas$/],
        },
      },

      // css modules 功能开关与相关配置
      cssModules: {
        enable: true,
        config: {
          namingPattern: 'module',
          generateScopedName: '[name]-[local]__[hash:base64:5]',
        },
      },
    },
  },

  h5: {
    publicPath: env.PUBLIC_PATH,
    output: {
      filename: 'js/[name].js?[hash:8]',
      chunkFilename: 'js/chunk-[name].js?[chunkhash:8]',
    },

    miniCssExtractPluginOption: {
      filename: 'css/[name].css?[hash:8]',
      chunkFilename: 'css/chunk-[id].css?[hash:8]',
    },

    staticDirectory: 'static',

    devServer: {
      port: process.env.API_PROXY === 'UAT' ? 443 : 10086,
      before: (app) => {
        app.use('/images', express.static(path.resolve(__dirname, '../src/images')));
      },
      https: process.env.API_PROXY === 'UAT',
      // 设置接口代理
      proxy: {
        [env.API_SCOPE]: {
          target: process.env.API_PROXY === 'UAT' ? 'https://uat.ctlife.tv' : env.API_URL,
          secure: false,
          onProxyReq(proxyReq, req, res) {
            if (process.env.API_PROXY === 'UAT') {
              // add custom header to request
              proxyReq.setHeader('Host', 'waketest53.ctlife.tv');
            }
            // or log the req
          },
          changeOrigin: true,
        }
      }
    },

    postcss: {
      autoprefixer: {
        enable: true,
      },

      cssModules: {
        enable: true,
        config: {
          namingPattern: 'module',
          generateScopedName: '[name]-[local]__[hash:base64:5]',
        },
      },
    },
  },
};

const commonResource = [
  {from: 'sitemap.json', to: `dist/${process.env.TARO_ENV}/sitemap.json`},
  // 静态引用的图片，这些图片在后台配置, 没有经过 webpack 处理
  {from: 'src/images/', to: `dist/${process.env.TARO_ENV}/images/`}, // asset
];
const customConfig = {
  weapp: {
    copy: {
      patterns: [
        {from: 'ext.json', to: `dist/${process.env.TARO_ENV}/ext.json`}, // 指定需要 copy 的文件
        ...commonResource,
      ],
    },
  },

  tt: {
    copy: {
      patterns: [
        {from: 'ext.tt.json', to: `dist/${process.env.TARO_ENV}/ext.json`}, // 指定需要 copy 的文件
        ...commonResource,
      ],
    },
  },

  h5: {
    copy: {
      patterns: commonResource,
    },
  },
};

module.exports = function (merge) {
  const TARO_ENV = process.env.TARO_ENV;
  let newConfig = config;
  const merConfig = customConfig[TARO_ENV];
  if (merConfig) {
    newConfig = Object.assign({}, newConfig, merConfig);
  }

  if (process.env.NODE_ENV === 'development') {
    return merge({}, newConfig, require('./dev'));
  }
  return merge({}, newConfig, require('./prod'));
};
