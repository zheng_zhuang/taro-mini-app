const DOMAIN = {
  TEST: 'https://htrip.wakedata.com', // 测试环境
  UAT: 'https://waketest53.ctlife.tv', // 携旅测试环境
  UK: 'https://mall.htrip.tv', // 携旅优客环境
  EKS: 'https://eksmall.ctlife.tv', // 测试携旅优客环境
  PRODUCTION: 'https://mall.htrip.tv', // 正式环境
};

export {
  DOMAIN
}
