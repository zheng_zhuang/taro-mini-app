/**
 * 环境配置
 *
 * - 这里定义的变量可以在程序中使用 process.env.* 访问
 * - 如果你要覆盖这些信息，请在本地配置一个 env.local.js. env.local.js 不会提交到版本库
 */
const IS_WX_OA = process.env.WX_OA === 'true';
const IS_IOS_PAY = process.env.IOS_PAY === 'true';
const IS_OPEN_MP= process.env.OPEN_MP === 'true';
const IS_TEST = process.env.TEST === 'true';
let WK_ENV = process.env.WK_ENV || 'TEST';
if (process.env.NODE_ENV === 'development') {
  WK_ENV = 'EKS'
}
import { DOMAIN } from './config.js'
if (IS_WX_OA) {
  console.log('微信公众号开发');
}

if (IS_TEST) {
  console.log('测试模式');
}
const ENV_VERSION = {
  TEST: 'develop', // 测试环境
  UAT: 'develop', // 携旅测试环境
  UK: 'develop', // 携旅测试环境
  EKS: 'develop', // 携旅测试环境
  PRODUCTION: 'release', // 正式环境
};

module.exports = {
  // 运行环境
  ENVIRONMENT: ENV_VERSION[WK_ENV],
  // API 请求域名
  API_URL: process.env.NODE_ENV === 'production' ? '' : DOMAIN[WK_ENV],
  // API_URL: DOMAIN[WK_ENV],
  // API 请求路径，将被用于代理匹配
  API_SCOPE: '/cs',

  // 开启微信公众号
  WX_OA: IS_WX_OA.toString(),

  // ios虚拟商品支付H5
  IOS_PAY: IS_IOS_PAY.toString(),
  OPEN_MP: IS_OPEN_MP.toString(),

  // 腾讯地图 KEY
  // TODO: 注册正式的 KEY
  LOCATION_APIKEY: 'IXVBZ-UTEKW-LC6RY-RYDVY-KPKIS-3BFVX',

  HASH: Math.random().toFixed(5),

  // H5 部署路径
  PUBLIC_PATH:
  IS_OPEN_MP ? '/openmp/' :IS_IOS_PAY ? '/ios-pay/' : process.env.NODE_ENV === 'production' ? `/taro-h5/` : '/',
    // process.env.NODE_ENV === 'production' ? `/taro-h5/` : '/',
    // process.env.NODE_ENV === 'production' ? `https://${IS_TEST ? 'cdn-h5' : 'cdn'}.wakedata.com/h5/resources/` : '/',
};
