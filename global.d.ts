declare module '*.png';
declare module '*.gif';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.styl';

declare interface Window {
  SETUP_ERROR?: Error;
}

declare namespace NodeJS {
  interface Process {
    env: {
      TARO_ENV: 'weapp' | 'swan' | 'alipay' | 'h5' | 'rn' | 'tt' | 'quickapp' | 'qq';
      NODE_ENV: 'development' | 'production';
      API_URL: string;
      API_SCOPE: string;
      WX_OA: 'true' | 'false';
      IOS_PAY: 'true' | 'false';
      OPEN_MP: 'true' | 'false';
      PUBLIC_PATH: string;
      LOCATION_APIKEY: string;
      [key: string]: any;
    };
  }
}
