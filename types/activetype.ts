/**
 * 活动类型
 */
export enum ActivityType {
  Social, // 社区活动
  Academy, // 养老学院
}

export const ActivityTypeName = {
  [ActivityType.Social]: '社区活动',
  [ActivityType.Academy]: '养老学院',
};

/**
 * 活动状态
 */
export enum ActivityStatus {
  Removed, // 已删除
  Draft, // 操作
  Pending, // 进行中
  Doing, // 进行中
  Ended, // 已结束
}

/**
 * 活动状态名称
 */
export const ActivityStatusName = {
  [ActivityStatus.Removed]: '已删除',
  [ActivityStatus.Draft]: '操作',
  [ActivityStatus.Pending]: '进行中',
  [ActivityStatus.Doing]: '进行中',
  [ActivityStatus.Ended]: '已结束',
};
/**
 * 活动状态背景颜色
 */
export const ActivityStatusBgColor = {
  [ActivityStatus.Removed]: '已删除',
  [ActivityStatus.Draft]: '操作',
  [ActivityStatus.Pending]: 'linear-gradient(90deg,#4ee2b6, #30cd9e)',
  [ActivityStatus.Doing]: 'linear-gradient(90deg,#4ee2b6, #30cd9e)',
  [ActivityStatus.Ended]: 'linear-gradient(90deg,#dcdcdc 2%, #babbba 99%)',
};
export interface ActivityItem {
  endTime: string;
  id: number;
  startTime: string;
  activityStartTime?: string;
  activityEndTime?: string;
  ticketDTOList: ActivityTicketItem[];
}

export interface ActivityTicketItem {
  buyLimit: number;
  id: number;
  inventoryNum: number;
  itemId: number;
  price: number;
  saleNum: number;
  sku: string;
  ticketName: string;
  ticketDTOList: any;
}

export interface Activity {
  activityNo: string;
  activityTitle: string;
  activityDetail: string;
  id: number;
  imgUrl: string;
  ticketPrice: number;
  status: number;
  activityAddress: string;
  itemDTOList: Array<ActivityItem>;
  latitude: string;
  longitude: string;
  contactsDTOList: Array<{
    contactsName: string;
    contactsPhone: string;
    id: number;
  }>;
  joinPeopleList: Array<any>;
  virtualApplyPersonNum: number;
  // 限制人数
  inventoryNum: number;
  saleNum: number;
  // 跟随人限制
  followPersonMax: number;
  // 支付类型
  payType: number;
  activityJsonForm?: any;
}

// 报名详情
export interface Detail {
  payLoadList: any[];
  activityAddress: string;
  activityOrderItemDTOList: Array<{
    id: number;
    customerName: string;
    customerPhone: string;
    status: VerifyStatus;
    customerJsonRecord: Array<any>;
  }>;
  activityTitle: string;
  endTime: string;
  imgUrl: string;
  orderNo: string;
  createTime: string;
  startTime: string;
  accompanDTOList: Array<{
    accompanName: string;
    accompanPhone: string;
    createTime: string;
    id: number;
  }>;
  payStatus?: number;
  ticketType?: number;
  activityStartTime?: string;
  activityEndTime?: string;
}

/**
 * 核销状态
 */
export enum VerifyStatus {
  All = 0,
  Pending = 10,
  Verified = 20,
  Cancelled = 30,
}

/**
 * 活动电子票
 */
export interface ActivityTicket {
  id: number;
  activityAddress: string;
  activityTitle: string;
  customerName: string;
  customerPhone: string;
  endTime: string;
  startTime: string;
  status: VerifyStatus;
  verificationCode: string;
  verificationImage: string;
}
