import { loadSDK, configSDK, getParams } from '@/wxat-common/utils/platform/official-account.h5';
import login from '@/wxat-common/x-login';
import VConsole from 'vconsole';
import utils from "./wxat-common/utils/util";

if (process.env.WX_OA === 'true') {
  // 让 Taro 识别当前是公众号环境
  // @ts-expect-error
  window.wx = {};

  // 公众号 SDK 资源加载
  // @ts-expect-error
  window.LOCATION_APIKEY = process.env.LOCATION_APIKEY;
  let time = 1;
  const initialSDK = async () => {
    try {
      await loadSDK();
      console.log('wx jssdk 加载成功');
      await login.waitLogin();
      // SDK 配置
      try {
        await configSDK();
        console.log('wx config 成功');
      } catch (err) {
        console.error('SDK 配置失败', err);
      }
    } catch (err) {
      console.error('wx JSSDK 加载失败:' + err.message);
      setTimeout(initialSDK, 1000 * time++);
    }
  };

  // 参数检查
  const searchParams = getParams();
  if (searchParams.appid == null) {
    console.log('必须提供', searchParams);
    console.error('必须配置 appid');
    alert('必须配置 appid');
    window.SETUP_ERROR = new Error('必须配置 appid');
  }

  if (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true' && utils.getUrlParam('debug')) {
    new VConsole()
  }

  initialSDK();
}
