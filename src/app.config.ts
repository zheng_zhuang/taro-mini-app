export default {
  pages: [
    "wxat-common/pages/home/index",
    "pages/index/index",
    'wxat-common/pages/error-page/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '',
    navigationBarTextStyle: 'black',
  },
};
