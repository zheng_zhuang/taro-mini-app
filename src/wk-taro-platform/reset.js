if (process.env.TARO_ENV === 'alipay') {
  require('./style/reset/alipay.css');
} else if (process.env.TARO_ENV === 'h5') {
  require('./style/reset/h5.css');
}
