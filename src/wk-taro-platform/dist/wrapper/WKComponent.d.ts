/**
 * 让普通类组件支持监听页面函数
 *
 * @param Component
 * @deprecated 已废弃，不再强制使用
 * @returns
 */
export declare function WKComponent<T>(Component: T): T;
