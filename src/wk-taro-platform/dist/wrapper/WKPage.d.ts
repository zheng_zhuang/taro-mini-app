/**
 * 页面包装器
 * @deprecated 已废弃，不再强制使用
 * @param component
 */
export declare function WKPage<T>(component: T): T;
