/**
 * 让普通类组件支持监听页面函数
 *
 * @param Component
 * @returns
 */
export declare function WithPageLifecycles<T>(Component: T): T;
