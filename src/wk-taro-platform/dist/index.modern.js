import Taro, { useDidShow, useDidHide, useReady } from '@tarojs/taro';
import React, { useRef, Component, PureComponent, useEffect } from 'react';
import { findDOMNode } from 'react-dom';
import wkTaroApi from '@/wk-taro-api';
export { default as NativeAPI } from '@/wk-taro-api';
import camelCase from 'lodash/camelCase';
import LRU from 'lru-cache';

/**
 * ref context 构造器, 兼容 *.current 和对象使用形式
 */

function $createContextRef(factory, name) {
  let initialized = false;
  let current;
  let value;

  const setter = inst => {
    value = inst; // 重置

    initialized = false;
  };

  const getter = () => {
    if (initialized) {
      return current;
    }

    if (value == null) {
      console.warn(`请在 componentDidMount 或 useEffect 中调用 .current, 并且确保将 ref 传递给 <${name} />`);
      return null;
    }

    current = factory(value);
    initialized = true;
    return current;
  }; // 支持函数调用和 RefObject


  const ref = function ref(inst) {
    setter(inst);
  }; // 同时可以通过函数的形式调用


  Object.defineProperty(ref, 'current', {
    enumerable: true,

    get() {
      return getter();
    },

    set(inst) {
      setter(inst);
    }

  }); // @ts-expect-error

  return ref;
}
function $transformCreateToHooks(cb) {
  const ref = useRef();

  if (ref.current == null) {
    ref.current = cb();
  }

  return ref.current;
}

/**
 * 跨平台的 createVideoContext
 *
 *
 * @param id
 * @param scope 如果 Video 在自定义组件内，需要传递当前自定义组件实例
 * @example
 *
 * class Foo extends Component {
 *   this.videoRef = createVideoRef()
 *
 *   play = () => {
 *     if (this.videoRef.current) {
 *       this.videoRef.current.play()
 *     }
 *   }
 *
 *   render() {
 *     return (<View onClick={this.play}>
 *      <Video id="videoId" ref={this.videoRef}></Video>
 *     </View>)
 *   }
 * }
 */

function createVideoRef(id, scope) {
  return $createContextRef(instance => {
    id = id !== undefined ? id : instance.props ? instance.props.id : instance.id;

    if (id == null) {
      throw new TypeError('确保将 ref 传递给 <Video/>, 并且配置 id');
    }

    return process.env.TARO_ENV === 'h5' ? instance : Taro.createVideoContext(id);
  }, 'Video');
}
/**
 * 跨平台的 createVideoContext hooks 形式
 *
 * @param id
 * @example
 *
 * function Foo() {
 *   const videoRef = useVideoRef()
 *   const play = () => {
 *     if (videoRef.current) {
 *       videoRef.current.play()
 *     }
 *   }
 *
 *   return (<View onClick={play}>
 *     <Video id="videoId" ref={videoRef}></Video>
 *   </View>)
 * }
 */

function useVideoRef(id, scope) {
  return $transformCreateToHooks(() => createVideoRef(id));
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

/**
 * H5 创建 context
 */
/**
 * 创建 canvas 的绘图上下文 CanvasContext 对象
 * @param {string} canvasId 要获取上下文的 <canvas> 组件 canvas-id 属性
 * @returns {Taro.CanvasContext}
 */

const createCanvasContext = (canvasId, inst) => {
  /** @type {HTMLCanvasElement} */
  const canvas = findDOMNode(inst).querySelector(`canvas[canvas-id="${canvasId}"]`);
  /** @type {CanvasRenderingContext2D} */

  const ctx = canvas.getContext('2d');
  /**
   * @typedef {Object} Action
   * @property {Function} func
   * @property {any[]} args
   */

  /**
   * 操作队列
   * @type {Action[]}
   */

  const actions = [];

  const enqueueActions = func => {
    return (...args) => {
      actions.push({
        func,
        args
      });
    };
  };

  const emptyActions = () => {
    actions.length = 0;
  };
  /**
   * 将之前在绘图上下文中的描述（路径、变形、样式）画到 canvas 中。
   * @param {Boolean} [reserve=false] 本次绘制是否接着上一次绘制。
   * 即 reserve 参数为 false，则在本次调用绘制之前 native 层会先清空画布再继续绘制；
   * 若 reserve 参数为 true，则保留当前画布上的内容，本次调用 drawCanvas 绘制的内容覆盖在上面，
   * 默认 false。
   * @param {Function} [callback] 绘制完成后执行的回调函数
   * @todo 每次draw都会读取width和height
   */


  const draw = async (reserve = false, callback) => {
    try {
      if (!reserve) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
      } // 部分 action 是异步的


      for (const {
        func,
        args
      } of actions) {
        await func.apply(ctx, args);
      }

      emptyActions();
      callback && callback();
    } catch (e) {
      /* eslint-disable prefer-promise-reject-errors */
      throw {
        errMsg: e.message
      };
    }
  };

  const customProperties = [
  /**
   * 设置填充色。
   * @param {String} color 填充的颜色，默认颜色为 black。
   */
  ['setFillStyle', color => {
    ctx.fillStyle = color;
  }],
  /**
   * 设置字体的字号。
   * @param {Number} fontSize 字体的字号
   */
  ['setFontSize', fontSize => {
    ctx.font = fontSize;
  }, true],
  /**
   * 设置全局画笔透明度。
   * @param {Number} alpha 透明度。范围 0-1，0 表示完全透明，1 表示完全不透明。
   */
  ['setGlobalAlpha', alpha => {
    ctx.globalAlpha = alpha;
  }],
  /**
   * 设置虚线样式。
   * @param {Number[]} pattern 一组描述交替绘制线段和间距（坐标空间单位）长度的数字
   * @param {Number} offset 虚线偏移量
   */
  ['setLineDash', (pattern, offset) => {
    ctx.setLineDash(pattern);
    ctx.lineDashOffset = offset;
  }],
  /**
   * 设置线条的端点样式ind
   * @param {String} lineCap 线条的结束端点样式
   */
  ['setLineCap', lineCap => {
    ctx.lineCap = lineCap;
  }],
  /**
   * 设置线条的交点样式
   * @param {String} lineJoin 线条的结束交点样式
   */
  ['setLineJoin', lineJoin => {
    ctx.lineJoin = lineJoin;
  }],
  /**
   * 设置线条的宽度
   * @param {number} lineWidth 线条的宽度，单位px
   */
  ['setLineWidth', lineWidth => {
    ctx.lineWidth = lineWidth;
  }],
  /**
   * 设置最大斜接长度。斜接长度指的是在两条线交汇处内角和外角之间的距离。当 CanvasContext.setLineJoin() 为 miter 时才有效。超过最大倾斜长度的，连接处将以 lineJoin 为 bevel 来显示。
   * @param {number} miterLimit 最大斜接长度
   */
  ['setMiterLimit', miterLimit => {
    ctx.miterLimit = miterLimit;
  }],
  /**
   * 设定阴影样式。
   * @param {number} offsetX 阴影相对于形状在水平方向的偏移，默认值为 0。
   * @param {number} offsetY 阴影相对于形状在竖直方向的偏移，默认值为 0。
   * @param {number} blur 阴影的模糊级别，数值越大越模糊。范围 0- 100。，默认值为 0。
   * @param {string} color 阴影的颜色。默认值为 black。
   */
  ['setShadow', (offsetX, offsetY, blur, color) => {
    ctx.shadowOffsetX = offsetX;
    ctx.shadowOffsetY = offsetY;
    ctx.shadowColor = blur;
    ctx.shadowBlur = color;
  }],
  /**
   * 设置描边颜色。
   * @param {String} color 描边的颜色，默认颜色为 black。
   */
  ['setStrokeStyle', color => {
    ctx.strokeStyle = color;
  }],
  /**
   * 设置文字的对齐
   * @param {String} align 文字的对齐方式
   */
  ['setTextAlign', align => {
    ctx.textAlign = align;
  }, true],
  /**
   * 设置文字的竖直对齐
   * @param {string} textBaseline 文字的竖直对齐方式
   */
  ['setTextBaseline', textBaseline => {
    ctx.textBaseline = textBaseline;
  }, true], ['drawImage', (url, ...extra) => {
    // 需要转换为 Image
    if (typeof url === 'string') {
      const img = new Image();
      img.src = url;
      return new Promise((resolve, reject) => {
        img.onload = () => {
          ctx.drawImage(img, ...extra);
          resolve();
        };

        img.onerror = reject;
      });
    }

    ctx.drawImage(url, ...extra);
  }]];
  const functionProperties = [['arc'], ['arcTo'], ['beginPath'], ['bezierCurveTo'], ['clearRect'], ['clip'], ['closePath'], ['createLinearGradient', true], ['createPattern', true], ['fill'], ['fillRect'], ['fillText'], ['lineTo'], ['measureText', true], ['moveTo'], ['quadraticCurveTo'], ['rect'], ['restore'], ['rotate'], ['save'], ['scale'], ['setTransform'], ['stroke'], ['strokeRect'], ['strokeText'], ['transform'], ['translate']];
  const valueProperties = [['fillStyle'], ['font', true], ['globalAlpha'], ['lineCap'], ['lineDashOffset'], ['lineJoin'], ['lineWidth'], ['miterLimit'], ['shadowOffsetX'], ['shadowOffsetY'], ['shadowColor'], ['shadowBlur'], ['strokeStyle'], ['textAlign', true], ['textBaseline', true], ['direction', true], ['globalCompositeOperation'], ['imageSmoothingEnabled '], ['imageSmoothingQuality'], ['filter']];
  const CanvasContext = {
    __raw__: ctx
  };
  customProperties.forEach(([funcName, func, setImmediatly]) => {
    Object.defineProperty(CanvasContext, funcName, {
      get() {
        const fn = enqueueActions(func);
        return (...args) => {
          // 立即触发
          if (setImmediatly) {
            func.apply(ctx, args);
          }

          return fn(...args);
        };
      },

      enumerable: true
    });
  });
  functionProperties.forEach(([funcName, isSync]) => {
    Object.defineProperty(CanvasContext, funcName, {
      get: isSync ? () => ctx[funcName].bind(ctx) : () => enqueueActions(ctx[funcName]),
      enumerable: true
    });
  });
  valueProperties.forEach(([propertyName, setImmediatly]) => {
    Object.defineProperty(CanvasContext, propertyName, {
      get() {
        return ctx[propertyName];
      },

      set(value) {
        if (setImmediatly) {
          ctx[propertyName] = value;
        }

        enqueueActions(() => {
          ctx[propertyName] = value;
        })();
        return true;
      }

    });
  });
  Object.defineProperty(CanvasContext, 'createCircularGradient', {
    /**
     * @param {number} x
     * @param {number} y
     * @param {number} r
     */
    value: (x, y, r) => {
      const radialGradient = ctx.createRadialGradient(x, y, 0, x, y, r);
      return radialGradient;
    }
  });
  Object.defineProperty(CanvasContext, 'draw', {
    value: draw
  });
  return CanvasContext;
};

function wrapInstance(inst, canvasId, component, scope) {
  const _scope = process.env.TARO_ENV === 'h5' ? component : scope; // 代码重复，没办法 H5 端 这些方法不在 default 中


  Object.defineProperty(inst, 'canvasToTempFilePath', {
    value: option => {
      return Taro.canvasToTempFilePath(_extends({}, option, {
        canvasId
      }), _scope);
    }
  });
  Object.defineProperty(inst, 'canvasPutImageData', {
    value: option => {
      return Taro.canvasPutImageData(_extends({}, option, {
        canvasId
      }), _scope);
    }
  });
  Object.defineProperty(inst, 'canvasGetImageData', {
    value: option => {
      return Taro.canvasGetImageData(_extends({}, option, {
        canvasId
      }), _scope);
    }
  });
  return inst;
}
/**
 *
 * 跨平台的 createCanvasContext
 * @param id canvasId 非必须，从ref 中获取
 * @param scope 非必须
 *
 * class Foo extends React.Component {
 *   canvasRef = createCanvasRef()
 *
 *   componentDidMount() {
 *     const context = this.canvasRef.current;
 *     if (context) {
 *       context.setFillStyle("red");
 *       context.fillRect(0, 0, 30, 30);
 *       context.draw();
 *       console.log(context);
 *     }
 *   }
 *
 *   render() {
 *     return (<Canvas canvasId="canvasId" ref={this.canvasRef}></Canvas>)
 *   }
 * }
 */


function createCanvasRef(id, scope) {
  return $createContextRef(instance => {
    id = id !== undefined ? id : instance.props ? instance.props.canvasId : instance.canvasId;

    if (id == null) {
      throw new TypeError('确保将 ref 传递给 <Canvas/>, 并且配置 canvasId');
    }

    if (process.env.TARO_ENV === 'h5') {
      if (instance) {
        const el = findDOMNode(instance); //  H5 通过第二个参数传入节点来查找 canvas

        return wrapInstance(createCanvasContext(id, el), id, instance, scope);
      } else {
        throw new Error('请将 ref 传递给 Canvas');
      }
    }

    return wrapInstance(Taro.createCanvasContext(id, scope), id, instance, scope);
  }, 'Canvas');
}
/**
 * createCanvasRef hooks 版本
 *
 * @example
 *
 * function Foo() {
 *   const canvasRef = useCanvasRef()
 *
 *   useEffect(() => {
 *     const context = canvasRef.current;
 *     if (context) {
 *       context.setFillStyle("red");
 *       context.fillRect(0, 0, 30, 30);
 *       context.draw();
 *       console.log(context);
 *     }
 *   }, []);
 *
 *   return (<Canvas canvasId="canvasId" ref={canvasRef}></Canvas>)
 * }
 */

function useCanvasRef(id, scope) {
  return $transformCreateToHooks(() => createCanvasRef(id, scope));
}

function createCommonjsModule(fn) {
  var module = { exports: {} };
	return fn(module, module.exports), module.exports;
}

// Copyright Joyent, Inc. and other Node contributors.
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

var decode = function decode(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);
  var maxKeys = 1000;

  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length; // maxKeys <= 0 means that we should not limit keys count

  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr,
        vstr,
        k,
        v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (Array.isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

// Copyright Joyent, Inc. and other Node contributors.

var stringifyPrimitive = function stringifyPrimitive(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

var encode = function encode(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';

  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return Object.keys(obj).map(function (k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;

      if (Array.isArray(obj[k])) {
        return obj[k].map(function (v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);
  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq + encodeURIComponent(stringifyPrimitive(obj));
};

var querystring = createCommonjsModule(function (module, exports) {

  exports.decode = exports.parse = decode;
  exports.encode = exports.stringify = encode;
});

const NOOP_OBJECT = {};

if (process.env.NODE_ENV === 'development') {
  Object.freeze(NOOP_OBJECT);
}

const NOOP = () => {};
const COMPONENT_LIFECYCLES = ['componentDidShow', 'componentDidHide', 'onPullDownRefresh', 'onReachBottom', 'onPageScroll', 'onResize', 'onShareAppMessage', 'onTabItemTap', 'onTitleClick', 'onOptionMenuClick', 'onPullIntercept', 'onShareTimeline', 'onAddToFavorites', 'onReady'];
const COMPONENT_LIFECYCLES_TO_HOOKS = {
  componentDidShow: 'useDidShow',
  componentDidHide: 'useDidHide',
  onPullDownRefresh: 'usePullDownRefresh',
  onReachBottom: 'useReachBottom',
  onPageScroll: 'usePageScroll',
  onResize: 'useResize',
  onShareAppMessage: 'useShareAppMessage',
  onTabItemTap: 'useTabItemTap',
  onTitleClick: 'useTitleClick',
  onOptionMenuClick: 'useOptionMenuClick',
  onPullIntercept: 'usePullIntercept',
  onShareTimeline: 'useShareTimeline',
  onAddToFavorites: 'useAddToFavorites',
  onReady: 'useReady'
};

/**
 * 安全执行
 */
function safeCall(obj, name, ...args) {
  if (obj && typeof obj[name] === 'function') {
    try {
      return obj[name](...args);
    } catch (err) {
      console.error(err);
    }
  }

  return undefined;
}
/**
 * 判断原型链上是否存在对应属性
 * @param Ctor
 * @param name
 * @returns
 */

function isDefineProperty(Ctor, name) {
  return !!(typeof Ctor === 'function' && Ctor.prototype && Ctor.prototype[name]);
}
/**
 * 属性拦截
 * @param object
 * @param name
 * @param getter
 * @param setter
 */

function interceptProperty(object, name, getter, setter) {
  if (object == null) {
    return;
  }

  const desc = Object.getOwnPropertyDescriptor(object, name);

  if (desc) {
    // 已存在
    // @ts-ignore
    const value = object[name];
    Object.defineProperty(object, name, {
      enumerable: true,
      configurable: true,

      get() {
        return getter();
      },

      set(value) {
        desc.set == null ? void 0 : desc.set(value);
        setter == null ? void 0 : setter(value);
      }

    });
    return value;
  } else {
    Object.defineProperty(object, name, {
      enumerable: true,
      configurable: true,
      get: getter,
      set: setter
    });
    return undefined;
  }
}

/**
 * 是否为类组件
 * @param Ctor
 * @returns
 */

function isClassComponent(Ctor) {
  if (typeof Ctor !== 'function') {
    return false;
  }

  return Component.isPrototypeOf(Ctor) || PureComponent.isPrototypeOf(Ctor);
}

/**
 * 平台断言
 * @param env
 */

function assertPlatform(env) {
  const currentEnv = Taro.getEnv();
  console.assert(currentEnv === env, `环境不匹配: 目标环境(${env}), 当前环境(${currentEnv})`);
}

const _excluded = ["path", "params"];
/**
 * 是否为 Hash Mode 路由
 */

function $H5IsHashMode() {
  assertPlatform(Taro.ENV_TYPE.WEB);
  const app = Taro.getCurrentInstance().app;

  if (app && app.config && app.config.router && app.config.router.mode !== 'hash') {
    return false;
  } // 默认假设为 hash Mode


  return true;
}
/**
 * 未发现任何路由时默认为首页
 */

function createDefaultRouter() {
  // 从 App 实例中获取首页信息
  const app = Taro.getCurrentInstance().app;

  if (app) {
    const path = app.config && (app.config.entryPagePath || app.config.pages[0]);

    if (path) {
      return {
        path: '/' + path,
        params: NOOP_OBJECT
      };
    }
  }

  return {
    path: '/',
    params: NOOP_OBJECT
  };
}
/**
 * H5 端从 Url 从创建路由信息
 * @param hashMode
 * @returns
 */


function H5CreateRouter(hashMode) {
  if (hashMode) {
    const hash = window.location.hash;

    if (!hash) {
      return createDefaultRouter();
    }

    const url = hash.startsWith('#') ? hash.slice(1) : hash;

    if (!url || url === '/') {
      // 首页
      return createDefaultRouter();
    }

    const questionMarkIdx = url.indexOf('?');

    if (questionMarkIdx !== -1) {
      // 组装参数
      const search = url.slice(questionMarkIdx + 1); // + 去掉问好

      const path = url.slice(0, questionMarkIdx);
      return {
        path,
        params: querystring.parse(search)
      };
    } else {
      // 没有参数
      return {
        path: url,
        params: NOOP_OBJECT
      };
    }
  } else {
    const path = location.pathname;

    if (!path || path === '/') {
      // 首页
      return createDefaultRouter();
    }

    if (!location.search) {
      return {
        path,
        params: NOOP_OBJECT
      };
    }

    const search = location.search.startsWith('?') ? location.search.slice(1) : location.search;
    return {
      path,
      params: querystring.parse(search)
    };
  }
}
/**
 * 规范化路由对象
 *
 * @param router
 * @returns
 */


function normalizeRouter(router) {
  let {
    path,
    params
  } = router,
      other = _objectWithoutPropertiesLoose(router, _excluded);

  const questionMarkIdx = path.indexOf('?');

  if (questionMarkIdx !== -1) {
    path = path.slice(0, questionMarkIdx);
  }

  if (!path.startsWith('/')) {
    path = '/' + path;
  }

  params = params || {};
  return _extends({}, other, {
    params,
    // 向下兼容
    query: params,
    path
  });
}
/**
 * 跨平台兼容的 getCurrentInstance.router
 * 问题1：在 H5 端，App 生命周期中无法获取参数和路由信息，在 小程序中可以安全地获取
 * 问题2：在 H5 端，path 会携带查询字符串，而 小程序端没有
 * 问题3： 小程序的 App 中的 router 和 页面中的 router 也有区别：
 *   App 中 有 query、referrerInfo、scene、shareTicket 这些参数
 *   Page 中 有 onHide、onReady、onShow 等参数。
 *   所以这里最好只用 path、params、其他参数是平台相关的可以在 App onLaunch 的 option 中拿取
 */


function $getRouter() {
  let router = Taro.getCurrentInstance().router; // 在应用初始化之前调用是不允许的

  if (router == null) {
    throw new Error('请不要在应用未初始化完成之前获取路由，建议在组件内部获取');
  }

  if (process.env.TARO_ENV === 'h5') {
    // 在 App 中调用,
    if (router.params == null || router.path == null) {
      // 自己组装 router 对象
      router = H5CreateRouter($H5IsHashMode());
    }
  } // 规范化 router 对象


  router = normalizeRouter(router);
  return router;
}
/**
 * 获取页面栈中的上一个页面，Taro.Compoonent 实例
 * @deprecated 不建议使用，因为无法安全地获取。另外获取和操作上一个页面也是不推荐的
 */

function $getPreviousPage() {
  console.trace('$getPreviousPage() 已废弃，不建议使用，因为无法安全地获取。另外获取和操作上一个页面也是不推荐的');
  const pages = Taro.getCurrentPages();

  if (pages.length < 2) {
    return null;
  }

  return pages[pages.length - 2];
}
/**
 * 获取页面栈中的上一个页面，Taro.Compoonent 实例
 * @deprecated
 */

function $getPreviousTaroPage() {
  throw new Error('$getPreviousTaroPage 已废弃, Taro 3.x 不能直接获取到上一个页面 React 实例, 请规范化代码');
}
/**
 * 获取当前页面实例
 * 注意：
 * 问题1： 必须在任意一个页面挂载了之后才能获取到。比如你在 App componentDidMount 的时候是
 * 获取不到页面节点的，因为这个时候页面还没开始挂载渲染
 * 问题2：page.options 其他端没有
 *
 */

function $getCurrentPage() {
  const instance = Taro.getCurrentInstance();

  if (process.env.NODE_ENV === 'development' && instance.page == null) {
    console.trace('建议在组件中调用 $getCurrentPage, 否则可能为空');
  }

  return instance.page;
}
/**
 * 获取 Taro 页面实例
 * @deprecated 请使用 $getCurrentPage
 * @returns
 */

function $getCurrentTaroPage() {
  return $getCurrentPage();
}
/**
 * 去掉前缀和查询字符串
 */

function normalizePath(path) {
  if (!path) {
    return path;
  }

  path = path.startsWith('/') ? path.slice(1) : path;
  const qIdx = path.indexOf('?');

  if (qIdx !== -1) {
    path = path.slice(0, qIdx);
  }

  return path;
}
/**
 * 获取当前页面路由，需兼容不同平台
 * 注意只包含路径信息，不包含查询字符串, 而且没有 '/' 前缀，例如 wxat-common/xxx
 *
 * @deprecated 请使用 $getRouter 重构
 * @param currentPage
 * @return {string}
 */


function $getCurrentPageRoute(currentPage = $getCurrentPage()) {
  let route;

  if (currentPage) {
    // 多平台兼容
    route = // @ts-expect-error
    currentPage.route || // 微信小程序
    // @ts-expect-error
    currentPage.__route__ || // 头条
    currentPage.path; // H5
  }

  if (!route) {
    // 从 $getRouter 中获取
    route = $getRouter().path;
  }

  return normalizePath(route);
}
/**
 * 获取当前页带参数的url
 * @param {Boolean} withArgs 是否获取带参数的完整url，默认 true
 * @returns {string}
 */

function $getCurrentPageUrl(withArgs = true) {
  const router = $getRouter();

  if (withArgs) {
    const search = querystring.stringify(router.params);
    return router.path + (search ? '?' + search : '');
  }

  return router.path;
}

/**
 * 惟客宝自定义事件
 */

var WKEvent;

(function (WKEvent) {
  WKEvent["SET_SHARE_APP_MESSAGE"] = "WK_SET_SHARE_APP_MESSAGE";
  WKEvent["SET_SHARE_TIMELINE"] = "WK_SET_SHARE_TIMELINE";
})(WKEvent || (WKEvent = {}));

function $triggerWKEvent(event, ...args) {
  Taro.eventCenter.trigger(event, ...args);
}

function $listenWKEvent(event, cb) {
  Taro.eventCenter.on(event, cb);
  return () => {
    Taro.eventCenter.off(event, cb);
  };
}
var WKEvent$1 = WKEvent;

/**
 * 获取当前 $setShareAppMessage 设置的值
 * @returns
 */

function $getShareAppMessage(page) {
  var _page$__share_app_mes;

  const _page = page || $getCurrentPage();

  console.assert(_page != null, '请在页面组件或者其下级组件中调用');
  return (_page$__share_app_mes = _page.__share_app_message__) == null ? void 0 : _page$__share_app_mes.data;
}
/**
 * 获取当前 $setShareAppMessage 设置的值
 * @returns
 */

function $getShareTimeline(page) {
  var _page$__share_timelin;

  const _page = page || $getCurrentPage();

  console.assert(_page != null, '请在页面组件或者其下级组件中调用');
  return (_page$__share_timelin = _page.__share_timeline__) == null ? void 0 : _page$__share_timelin.data;
}
/**
 * 触发 App 分享, 主要用于 H5 端，模拟按钮点击分享。
 * @param evt
 */

function $triggerShareAppMessage(evt = NOOP_OBJECT) {
  assertPlatform(Taro.ENV_TYPE.WEB);
  const page = $getCurrentPage();
  console.assert(page != null, '请在页面组件或者其下级组件中调用');
  return page.onShareAppMessage == null ? void 0 : page.onShareAppMessage(evt);
}
/**
 * 触发 朋友圈 分享, 主要用于 H5 端，模拟按钮点击分享。
 * @param evt
 */

function $triggerShareTimeline(evt = NOOP_OBJECT) {
  assertPlatform(Taro.ENV_TYPE.WEB);
  const page = $getCurrentPage();
  console.assert(page != null, '请在页面组件或者其下级组件中调用');
  return page.onShareTimeline == null ? void 0 : page.onShareTimeline(evt);
}
/**
 * 命令式设置分享信息
 * @param data 分享信息, 结构同 onShareAppMessage
 * @param onShare 分享回调。当设置的 data 被最终分享时触发，目前仅小程序端支持
 * @returns
 */

function $setShareAppMessage(data, onShare) {
  var _page$config;

  // 获取页面实例
  const page = $getCurrentPage();
  console.assert(page != null, '请在页面组件或者其下级组件中调用'); // 判断分享是否开启

  if (page.onShareAppMessage === undefined && !((_page$config = page.config) != null && _page$config.enableShareAppMessage)) {
    console.error('页面需要设置 enableShareAppMessage 或者 定义 onShareAppMessage');
  } // 拦截覆盖 onShareAppMessage


  const isOverrided = page.__set_share_app_message_override__;
  const lastShareAppMessage = page.__share_app_message__;

  if (data) {
    page.__share_app_message__ = {
      data,
      callback: onShare
    };
  } else {
    page.__share_app_message__ = undefined;
  }

  console.log('$setShareAppMessage 正在设置分享信息', data); // 触发事件，方便外部做一些处理

  $triggerWKEvent(WKEvent$1.SET_SHARE_APP_MESSAGE, page, data);

  if (!isOverrided) {
    page.__set_share_app_message_override__ = true;
    const _onShareAppMessage = page.onShareAppMessage;

    page.onShareAppMessage = evt => {
      console.log('触发分享', evt);
      const rtn = _onShareAppMessage == null ? void 0 : _onShareAppMessage.call(page, evt); // 未设置，或者已经被重置

      if (page.__share_app_message__ == null) {
        return rtn;
      } // 冲突警告


      if (rtn != null && (rtn.path != null || rtn.imageUrl != null || rtn.title != null) && rtn !== page.__share_app_message__.data) {
        console.warn(`onShareAppMessage 返回了: ${JSON.stringify(rtn)}, 和 $setShareAppMessage ${JSON.stringify(page.__share_app_message__.data)} 冲突，将以 $setShareAppMessage 为主`);
      } // 回调


      safeCall(page.__share_app_message__, 'callback', evt);
      return page.__share_app_message__.data;
    };
  } // 重置


  return () => {
    page.__share_app_message__ = lastShareAppMessage;
  };
}
/**
 * 命令式设置分享朋友圈
 * @param data 分享信息, 结构同 onShareTimeline
 * @param onShare 分享回调。当设置的 data 被最终分享时触发，目前仅小程序端支持
 * @returns
 */

function $setShareTimeline(data, onShare) {
  var _page$config2;

  // 获取页面实例
  const page = $getCurrentPage();
  console.assert(page != null, '请在页面组件或者其下级组件中调用'); // 判断分享是否开启

  if (page.onShareTimeline === undefined && !((_page$config2 = page.config) != null && _page$config2.enableShareTimeline)) {
    console.error('页面需要设置 enableShareTimeline 或者 定义 onShareTimeline');
  } // 拦截覆盖 onShareTimeline


  const isOverrided = page.__set_share_timeline_override__;
  const lastShareTimeline = page.__share_timeline__;

  if (data) {
    page.__share_timeline__ = {
      data,
      callback: onShare
    };
  } else {
    page.__share_timeline__ = undefined;
  }

  console.log('$setShareTimeline 正在设置分享信息', data); // 触发事件，方便外部做一些处理

  $triggerWKEvent(WKEvent$1.SET_SHARE_TIMELINE, page, data);

  if (!isOverrided) {
    page.__set_share_timeline_override__ = true;
    const _onShareTimeline = page.onShareTimeline;

    page.onShareTimeline = evt => {
      console.log('触发分享', evt);
      const rtn = _onShareTimeline == null ? void 0 : _onShareTimeline.call(page, evt); // 未设置，或者已经被重置

      if (page.__share_timeline__ == null) {
        return rtn;
      } // 冲突警告


      if (rtn != null && (rtn.query != null || rtn.imageUrl != null || rtn.title != null) && rtn !== page.__share_timeline__.data) {
        console.warn(`onShareTimeline 返回了: ${JSON.stringify(rtn)}, 和 $setShareTimeline ${JSON.stringify(page.__share_timeline__.data)} 冲突，将以 $setShareTimeline 为主`);
      } // 回调


      safeCall(page.__share_timeline__, 'callback', evt);
      return page.__share_timeline__.data;
    };
  } // 重置


  return () => {
    page.__share_timeline__ = lastShareTimeline;
  };
}

/**
 * 是否为浏览器
 */
const $isBrowser = typeof document !== 'undefined' && !!document.scripts;

var index = {
  __proto__: null,
  $H5IsHashMode: $H5IsHashMode,
  $getRouter: $getRouter,
  $getPreviousPage: $getPreviousPage,
  $getPreviousTaroPage: $getPreviousTaroPage,
  $getCurrentPage: $getCurrentPage,
  $getCurrentTaroPage: $getCurrentTaroPage,
  $getCurrentPageRoute: $getCurrentPageRoute,
  $getCurrentPageUrl: $getCurrentPageUrl,
  $getShareAppMessage: $getShareAppMessage,
  $getShareTimeline: $getShareTimeline,
  $triggerShareAppMessage: $triggerShareAppMessage,
  $triggerShareTimeline: $triggerShareTimeline,
  $setShareAppMessage: $setShareAppMessage,
  $setShareTimeline: $setShareTimeline,
  get WKEvent () { return WKEvent; },
  $triggerWKEvent: $triggerWKEvent,
  $listenWKEvent: $listenWKEvent,
  $isBrowser: $isBrowser,
  NativeAPI: wkTaroApi
};

function _fixme_with_dataset_(fn, dataset = NOOP_OBJECT) {
  if (typeof fn !== 'function') {
    return fn;
  }

  return function (event) {
    if (process.env.TARO_ENV === 'h5') {
      // H5 中 target 为DOM 节点，dataset 只能写入字符串？
      // 这种行为有点危险，请尽快重构
      function rewriteDataset(target) {
        if (target.__dataset_defined__) {
          Object.assign(target.dataset, dataset);
        } else {
          target.__dataset_defined__ = true;
          Object.defineProperty(target, 'dataset', {
            value: _extends({}, target.dataset || NOOP_OBJECT, dataset)
          });
        }
      }

      if (event) {
        if (event.currentTarget) {
          rewriteDataset(event.currentTarget);
        }

        if (event.target && event.currentTarget !== event.target) {
          rewriteDataset(event.target);
        }
      }
    } else {
      if (event) {
        function redefineTarget(name) {
          const org = event[name] || NOOP_OBJECT;
          Object.defineProperty(event, name, {
            // @ts-ignore
            value: _extends({}, org, {
              // @ts-ignore
              dataset: _extends({}, org.dataset || NOOP_OBJECT, dataset)
            })
          });
        }

        redefineTarget('target');
        redefineTarget('currentTarget');
      }
    }

    fn(event);
  };
}

const cache = new LRU({
  max: 300
});
const RPX_REG = /[0-9\.]+\s*rpx/gi;
function styleStrToObj(value) {
  if (value == null) {
    return undefined;
  } else if (typeof value === 'string') {
    if (cache.has(value)) {
      return cache.get(value);
    } // 转换


    const rules = value.split(';');
    const styleObject = rules.filter(i => !!i.trim()).reduce((rules, rule) => {
      const colonIndex = rule.indexOf(':');

      if (colonIndex !== -1) {
        const name = rule.slice(0, colonIndex).trim();
        let value = rule.slice(colonIndex + 1).trim(); // rpx 转换

        value = value.replace(RPX_REG, i => Taro.pxTransform(parseFloat(i)));
        rules[camelCase(name)] = value;
      }

      return rules;
    }, {});
    cache.set(value, styleObject);
    return styleObject;
  }

  return value;
}
/**
 * 跨平台 style 适配, style 需要重构为对象形式，以在 H5 和 React Native 端适配。
 */

function _safe_style_(value) {
  return styleStrToObj(value);
}

/**
 * Taro App、Page 事件监听
 */
/**
 * 事件枚举
 */

var TARO_EVENT;

(function (TARO_EVENT) {
  // App
  TARO_EVENT["APP_LAUNCH"] = "TARO_APP_EVENT_LAUNCH";
  TARO_EVENT["APP_SHOW"] = "TARO_APP_EVENT_SHOW";
  TARO_EVENT["APP_HIDE"] = "TARO_APP_EVENT_HIDE";
  TARO_EVENT["APP_PAGE_NOT_FOUND"] = "TARO_APP_EVENT_PAGE_NOT_FOUND"; // Page

  TARO_EVENT["PAGE_LOAD"] = "TARO_PAGE_EVENT_LOAD";
  TARO_EVENT["PAGE_READY"] = "TARO_PAGE_EVENT_READY";
  TARO_EVENT["PAGE_UNLOAD"] = "TARO_PAGE_EVENT_UNLOAD";
  TARO_EVENT["PAGE_SHOW"] = "TARO_PAGE_EVENT_SHOW";
  TARO_EVENT["PAGE_HIDE"] = "TARO_PAGE_EVENT_HIDE";
  TARO_EVENT["PAGE_PULL_DOWN_REFRESH"] = "TARO_PAGE_EVENT_PULL_DOWN_REFRESH";
  TARO_EVENT["PAGE_REACH_BOTTOM"] = "TARO_PAGE_EVENT_REACH_BOTTOM";
  TARO_EVENT["PAGE_SHARE_APP_MESSAGE"] = "TARO_PAGE_SHARE_APP_MESSAGE";
  TARO_EVENT["PAGE_SHARE_TIMELINE"] = "TARO_PAGE_SHARE_TIMELINE";
  TARO_EVENT["PAGE_PAGE_SCROLL"] = "TARO_PAGE_EVENT_PAGE_SCROLL";
  TARO_EVENT["PAGE_RESIZE"] = "TARO_PAGE_EVENT_RESIZE";
  TARO_EVENT["PAGE_TAB_ITEM_TAP"] = "TARO_PAGE_EVENT_TAB_ITEM_TAP";
  TARO_EVENT["PAGE_TITLE_CLICK"] = "TARO_PAGE_EVENT_TITLE_CLICK";
  TARO_EVENT["PAGE_ADD_TO_FAVORITES"] = "TARO_PAGE_EVNET_ADD_TO_FAVORITES";
  /**
   * 只有支付宝支持
   */

  TARO_EVENT["PAGE_OPTION_MENU_CLICK"] = "TARO_PAGE_EVENT_OPTION_MENU_CLICK";
  /**
   * 只有支付宝支持
   */

  TARO_EVENT["PAGE_POP_MENU_CLICK"] = "TARO_PAGE_EVENT_POP_MENU_CLICK";
  /**
   * 下拉截断时触发, 只有支付宝支持
   */

  TARO_EVENT["PAGE_PULL_INTERCEPT"] = "TARO_PAGE_EVENT_PULL_INTERCEPT";
})(TARO_EVENT || (TARO_EVENT = {}));
/**
 * 应用事件拦截
 */


const APP_EVENTS = [['onLaunch', TARO_EVENT.APP_LAUNCH], ['onShow', TARO_EVENT.APP_SHOW], ['onHide', TARO_EVENT.APP_HIDE], ['onPageNotFound', TARO_EVENT.APP_PAGE_NOT_FOUND]];
/**
 * 页面事件拦截
 */

const PAGE_EVENTS = [// handleName, eventName, checkHandleExisted
// ["onLoad", TARO_EVENT.PAGE_EVENT_LOAD], // 不需要，onLoad 在 set page 之前触发
['onReady', TARO_EVENT.PAGE_READY, false], ['onUnload', TARO_EVENT.PAGE_UNLOAD, false], ['onShow', TARO_EVENT.PAGE_SHOW, false], ['onHide', TARO_EVENT.PAGE_HIDE, false], ['onPullDownRefresh', TARO_EVENT.PAGE_PULL_DOWN_REFRESH, false], ['onReachBottom', TARO_EVENT.PAGE_REACH_BOTTOM, false], // onShareAppMessage 和 onShareTimeline 一样，会影响小程序右上方按钮的选项，因此不能默认注册。
// onShareAppMessage 和 onShareTimeline Taro 只有在开启 onShareAppMessage 时才会添加到 config
['onShareAppMessage', TARO_EVENT.PAGE_SHARE_APP_MESSAGE, true], ['onShareTimeline', TARO_EVENT.PAGE_SHARE_TIMELINE, true]];
const TRIGGER_STATE = '__trigger_state__';
let initialized = false;

function triggerEvent(name, ...args) {
  Taro.eventCenter.trigger(name, ...args);
}

function createTriggerEventHandler(name, target) {
  return function (...args) {
    triggerEvent(name, target, ...args);
  };
}

function createTriggerEventOnPageHandler(name, target) {
  return function (...args) {
    const state = target[TRIGGER_STATE] = target[TRIGGER_STATE] || {}; // 记录事件触发

    state[name] = true; // 特殊处理隐藏和显示

    if (name === TARO_EVENT.PAGE_SHOW) {
      state[TARO_EVENT.PAGE_HIDE] = false;
    } else if (name === TARO_EVENT.PAGE_HIDE) {
      state[TARO_EVENT.PAGE_SHOW] = false;
    }

    triggerEvent(name, target, ...args);
  };
}
/**
 * 覆盖方法
 * @param obj
 * @param name
 * @param fn
 */


function redefineMethod(obj, name, fn) {
  const originFn = obj[name];

  obj[name] = function (...args) {
    let rtn;
    let error;

    if (originFn) {
      try {
        rtn = originFn.apply(obj, args);
      } catch (err) {
        error = err;
      }
    }

    try {
      fn.apply(null, [...args, rtn]);
    } catch (err) {
      console.error('taro interceptor uncatch error:', err);
    }

    if (error) {
      throw error;
    }

    return rtn;
  };
}

function onAppSet(value) {
  if (value == null) {
    return;
  } // 拦截 App 事件


  APP_EVENTS.forEach(([name, eventName]) => {
    redefineMethod(value, name, createTriggerEventHandler(eventName, value));
  });
}

function onPageSet(value) {
  if (value == null) {
    return;
  } // onShow 的时候也会执行一次 Set，不必重复拦截
  // @ts-ignore


  if (!value['__taro_intercepted__']) {
    // @ts-ignore
    value['__taro_intercepted__'] = true; // 拦截 Page 事件

    PAGE_EVENTS.forEach(([name, eventName, checkHandleExisted]) => {
      // @ts-ignore
      if (checkHandleExisted && value[name] == null) {
        return;
      }

      redefineMethod(value, name, // nextTick 执行，不阻塞主流程
      createTriggerEventOnPageHandler(eventName, value));
    }); // 触发 onLoad 事件, nextTick 触发，方便在事件处理器中拿到各种参数

    triggerEvent(TARO_EVENT.PAGE_LOAD, value);
  }
}
/**
 * 指定页面事件是否已经触发
 * @param event
 * @returns
 */


function isPageEventTriggered(event) {
  const current = Taro.getCurrentInstance(); // @ts-ignore

  if (current && current.page && current.page[TRIGGER_STATE] && current.page[TRIGGER_STATE][event]) {
    return true;
  }

  return false;
}
/**
 * 拦截 Taro 应用或页面事件
 * @param intercepters 事件拦截器，你也可以通过 Taro.eventCenter 来监听这些事件
 * @param options 用于初始化拦截器，你可以可以通过 initTaroInterceptor 来显式初始化
 * @returns Function 用于释放所有监听
 */

function interceptTaroEvents(intercepters) {
  const events = Object.keys(intercepters);

  if (!initialized) {
    initTaroInterceptor();
  }

  const disposers = events.map(name => {
    const listen = (...args) => {
      // @ts-ignore
      intercepters[name].apply(null, args);
    };

    Taro.eventCenter.on(name, listen);
    return () => Taro.eventCenter.off(name, listen);
  });
  return () => {
    disposers.forEach(fn => fn());
  };
}
/**
 * 初始化 App、Component 拦截
 * 调用 interceptTaroEvents 时也会自动初始化
 */

function initTaroInterceptor() {
  if (initialized) {
    return;
  }

  const current = Taro.getCurrentInstance();
  let app = current.app;
  let page = current.page;

  if (current.app) {
    console.warn('请在 App 创建之前调用 init, 这样可以才能监听到完整的 App、Page 事件');
  }

  initialized = true;
  interceptProperty(current, 'app', () => app, value => {
    app = value;
    onAppSet(value);
  });
  interceptProperty(current, 'page', () => page, value => {
    page = value;
    onPageSet(value);
  });
}

/**
 * 页面包装器
 * @deprecated 已废弃，不再强制使用
 * @param component
 */
function WKPage(component) {
  return component;
}

initTaroInterceptor();
const hooks = {
  [TARO_EVENT.PAGE_SHOW]: useDidShow,
  [TARO_EVENT.PAGE_HIDE]: useDidHide,
  [TARO_EVENT.PAGE_READY]: useReady
};
/**
 * 页面生命周期组件
 */

function addHooks(eventName, fn) {
  useEffect(() => {
    // 页面相关事件已经触发过了
    if (isPageEventTriggered(eventName)) {
      Taro.nextTick(() => {
        fn();
      });
    }
  }, []); // @ts-expect-error

  hooks[eventName](fn);
}

function usePageDidShow(fn) {
  addHooks(TARO_EVENT.PAGE_SHOW, fn);
}
function usePageDidHide(fn) {
  addHooks(TARO_EVENT.PAGE_HIDE, fn);
}
function usePageReady(fn) {
  addHooks(TARO_EVENT.PAGE_READY, fn);
}

const OVERRIDE_HOOKS = {
  useDidShow: usePageDidShow,
  useDidHide: usePageDidHide,
  useReady: usePageReady
};
/**
 * 让普通类组件支持监听页面函数
 *
 * @param Component
 * @returns
 */

function WithPageLifecycles(Component) {
  // @ts-ignore
  return decorate$1(Component);
}

function decorate$1(Ctor) {
  if (!isClassComponent(Ctor)) {
    throw new TypeError('只能用于包装类组件');
  }

  const Wrapper = React.forwardRef(function WithPageLifecyclesWrapper(props, ref) {
    const innerRef = useRef(null);
    const penddingCallback = useRef([]);

    const triggerHooks = (name, ...args) => {
      if (innerRef.current == null) {
        penddingCallback.current.push({
          name,
          args
        });
      } else {
        innerRef.current[name](...args);
      }
    };

    const setRef = r => {
      innerRef.current = r;
      const _ref = ref; // 向上转发

      if (_ref) {
        if (typeof _ref === 'function') {
          _ref(r);
        } else {
          _ref.current = r;
        }
      } // 处理未调用的回调


      if (r && penddingCallback.current.length) {
        const callbacks = penddingCallback.current;
        penddingCallback.current = [];
        callbacks.forEach(i => {
          triggerHooks(i.name, ...i.args);
        });
      }
    };

    COMPONENT_LIFECYCLES.forEach(name => {
      // 按需添加
      if (isDefineProperty(Ctor, name)) {
        const hookName = COMPONENT_LIFECYCLES_TO_HOOKS[name]; // @ts-ignore

        const hook = OVERRIDE_HOOKS[hookName] || Taro[hookName];
        hook((...args) => {
          triggerHooks(name, ...args);
        });
      }
    }); // @ts-ignore

    if (props.tid) {
      // Taro 页面, 不需要处理
      if (process.env.NODE_ENV === 'development') {
        console.trace('页面组件不需要添加 @WithPageLifecycles 装饰');
      }

      return React.createElement(Ctor, _extends({
        ref
      }, props));
    } else {
      return React.createElement(Ctor, _extends({
        ref: setRef
      }, props));
    }
  });
  Wrapper.displayName = `WithPageLifecycles(${Ctor.displayName || 'unknown'})`;
  return Wrapper;
}

/**
 * 让普通类组件支持监听页面函数
 *
 * @param Component
 * @deprecated 已废弃，不再强制使用
 * @returns
 */

function WKComponent(Component) {
  // @ts-ignore
  return decorate(Component);
}

function decorate(Ctor) {
  // @ts-ignore
  Ctor.__WKCOMPONENT__ = true;

  if (isClassComponent(Ctor)) {
    // @ts-ignore
    return WithPageLifecycles(Ctor);
  }

  if (process.env.NODE_ENV === 'development') {
    console.trace('建议只将 @WKComponent 用于类组件, 且必须在最里层');
  }

  return Ctor;
}

export { $H5IsHashMode, $getCurrentPage, $getCurrentPageRoute, $getCurrentPageUrl, $getCurrentTaroPage, $getPreviousPage, $getPreviousTaroPage, $getRouter, $getShareAppMessage, $getShareTimeline, $isBrowser, $listenWKEvent, $setShareAppMessage, $setShareTimeline, $triggerShareAppMessage, $triggerShareTimeline, $triggerWKEvent, NOOP, NOOP_OBJECT, TARO_EVENT, WKComponent, WKEvent, WKPage, WithPageLifecycles, _fixme_with_dataset_, _safe_style_, index as api, assertPlatform, createCanvasRef, createVideoRef, initTaroInterceptor, interceptTaroEvents, isPageEventTriggered, safeCall, styleStrToObj, useCanvasRef, usePageDidHide, usePageDidShow, usePageReady, useVideoRef };
//# sourceMappingURL=index.modern.js.map
