import { RefObject } from 'react';
/**
 * ref context 构造器, 兼容 *.current 和对象使用形式
 */
export declare function $createContextRef<T>(factory: (value: any) => T, name: string): RefObject<T>;
export declare function $transformCreateToHooks<T>(cb: () => RefObject<T>): RefObject<T>;
