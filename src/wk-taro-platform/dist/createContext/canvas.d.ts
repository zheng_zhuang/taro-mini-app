import { RefObject } from 'react';
import Taro from '@tarojs/taro';
export interface WKCanvasContext extends Taro.CanvasContext {
    canvasToTempFilePath(options: Omit<Taro.canvasToTempFilePath.Option, 'canvasId'>): Promise<Taro.canvasToTempFilePath.SuccessCallbackResult>;
    canvasPutImageData(options: Omit<Taro.canvasPutImageData.Option, 'canvasId'>): Promise<Taro.General.CallbackResult>;
    canvasGetImageData(options: Omit<Taro.canvasGetImageData.Option, 'canvasId'>): Promise<Taro.canvasGetImageData.SuccessCallbackResult>;
}
/**
 *
 * 跨平台的 createCanvasContext
 * @param id canvasId 非必须，从ref 中获取
 * @param scope 非必须
 *
 * class Foo extends React.Component {
 *   canvasRef = createCanvasRef()
 *
 *   componentDidMount() {
 *     const context = this.canvasRef.current;
 *     if (context) {
 *       context.setFillStyle("red");
 *       context.fillRect(0, 0, 30, 30);
 *       context.draw();
 *       console.log(context);
 *     }
 *   }
 *
 *   render() {
 *     return (<Canvas canvasId="canvasId" ref={this.canvasRef}></Canvas>)
 *   }
 * }
 */
export declare function createCanvasRef(id?: string, scope?: any): RefObject<WKCanvasContext>;
/**
 * createCanvasRef hooks 版本
 *
 * @example
 *
 * function Foo() {
 *   const canvasRef = useCanvasRef()
 *
 *   useEffect(() => {
 *     const context = canvasRef.current;
 *     if (context) {
 *       context.setFillStyle("red");
 *       context.fillRect(0, 0, 30, 30);
 *       context.draw();
 *       console.log(context);
 *     }
 *   }, []);
 *
 *   return (<Canvas canvasId="canvasId" ref={canvasRef}></Canvas>)
 * }
 */
export declare function useCanvasRef(id?: string, scope?: any): RefObject<WKCanvasContext>;
