import { RefObject } from 'react';
import Taro from '@tarojs/taro';
/**
 * 跨平台的 createVideoContext
 *
 *
 * @param id
 * @param scope 如果 Video 在自定义组件内，需要传递当前自定义组件实例
 * @example
 *
 * class Foo extends Component {
 *   this.videoRef = createVideoRef()
 *
 *   play = () => {
 *     if (this.videoRef.current) {
 *       this.videoRef.current.play()
 *     }
 *   }
 *
 *   render() {
 *     return (<View onClick={this.play}>
 *      <Video id="videoId" ref={this.videoRef}></Video>
 *     </View>)
 *   }
 * }
 */
export declare function createVideoRef(id?: string, scope?: any): RefObject<Taro.VideoContext>;
/**
 * 跨平台的 createVideoContext hooks 形式
 *
 * @param id
 * @example
 *
 * function Foo() {
 *   const videoRef = useVideoRef()
 *   const play = () => {
 *     if (videoRef.current) {
 *       videoRef.current.play()
 *     }
 *   }
 *
 *   return (<View onClick={play}>
 *     <Video id="videoId" ref={videoRef}></Video>
 *   </View>)
 * }
 */
export declare function useVideoRef(id?: string, scope?: any): RefObject<Taro.VideoContext>;
