/**
 * Taro App、Page 事件监听
 */
import Taro from '@tarojs/taro';
declare type AppInstance = Taro.AppInstance;
declare type PageInstance = Taro.PageInstance;
/**
 * 事件枚举
 */
export declare enum TARO_EVENT {
    APP_LAUNCH = "TARO_APP_EVENT_LAUNCH",
    APP_SHOW = "TARO_APP_EVENT_SHOW",
    APP_HIDE = "TARO_APP_EVENT_HIDE",
    APP_PAGE_NOT_FOUND = "TARO_APP_EVENT_PAGE_NOT_FOUND",
    PAGE_LOAD = "TARO_PAGE_EVENT_LOAD",
    PAGE_READY = "TARO_PAGE_EVENT_READY",
    PAGE_UNLOAD = "TARO_PAGE_EVENT_UNLOAD",
    PAGE_SHOW = "TARO_PAGE_EVENT_SHOW",
    PAGE_HIDE = "TARO_PAGE_EVENT_HIDE",
    PAGE_PULL_DOWN_REFRESH = "TARO_PAGE_EVENT_PULL_DOWN_REFRESH",
    PAGE_REACH_BOTTOM = "TARO_PAGE_EVENT_REACH_BOTTOM",
    PAGE_SHARE_APP_MESSAGE = "TARO_PAGE_SHARE_APP_MESSAGE",
    PAGE_SHARE_TIMELINE = "TARO_PAGE_SHARE_TIMELINE",
    PAGE_PAGE_SCROLL = "TARO_PAGE_EVENT_PAGE_SCROLL",
    PAGE_RESIZE = "TARO_PAGE_EVENT_RESIZE",
    PAGE_TAB_ITEM_TAP = "TARO_PAGE_EVENT_TAB_ITEM_TAP",
    PAGE_TITLE_CLICK = "TARO_PAGE_EVENT_TITLE_CLICK",
    PAGE_ADD_TO_FAVORITES = "TARO_PAGE_EVNET_ADD_TO_FAVORITES",
    /**
     * 只有支付宝支持
     */
    PAGE_OPTION_MENU_CLICK = "TARO_PAGE_EVENT_OPTION_MENU_CLICK",
    /**
     * 只有支付宝支持
     */
    PAGE_POP_MENU_CLICK = "TARO_PAGE_EVENT_POP_MENU_CLICK",
    /**
     * 下拉截断时触发, 只有支付宝支持
     */
    PAGE_PULL_INTERCEPT = "TARO_PAGE_EVENT_PULL_INTERCEPT"
}
/**
 * 指定页面事件是否已经触发
 * @param event
 * @returns
 */
export declare function isPageEventTriggered(event: TARO_EVENT): boolean;
/**
 * 拦截 Taro 应用或页面事件
 * @param intercepters 事件拦截器，你也可以通过 Taro.eventCenter 来监听这些事件
 * @param options 用于初始化拦截器，你可以可以通过 initTaroInterceptor 来显式初始化
 * @returns Function 用于释放所有监听
 */
export declare function interceptTaroEvents(intercepters: {
    [TARO_EVENT.APP_LAUNCH]?: (app: AppInstance, options: unknown) => void;
    [TARO_EVENT.APP_SHOW]?: (app: AppInstance, options: unknown) => void;
    [TARO_EVENT.APP_HIDE]?: (app: AppInstance, options: unknown) => void;
    [TARO_EVENT.APP_PAGE_NOT_FOUND]?: (res: Taro.PageNotFoundObject) => void;
    [TARO_EVENT.PAGE_LOAD]?: (page: PageInstance, options: unknown) => void;
    [TARO_EVENT.PAGE_READY]?: (page: PageInstance, options: unknown) => void;
    [TARO_EVENT.PAGE_UNLOAD]?: (page: PageInstance) => void;
    [TARO_EVENT.PAGE_SHOW]?: (page: PageInstance, options: unknown) => void;
    [TARO_EVENT.PAGE_HIDE]?: (page: PageInstance, options: unknown) => void;
    [TARO_EVENT.PAGE_PULL_DOWN_REFRESH]?: (page: PageInstance) => void;
    [TARO_EVENT.PAGE_REACH_BOTTOM]?: (page: PageInstance) => void;
    [TARO_EVENT.PAGE_SHARE_APP_MESSAGE]?: (page: PageInstance, evt: Taro.ShareAppMessageObject, rtn: Taro.ShareAppMessageReturn) => void;
    [TARO_EVENT.PAGE_SHARE_TIMELINE]?: (page: PageInstance, evt: Taro.ShareAppMessageObject, rtn: Taro.ShareTimelineReturnObject) => void;
}): () => void;
/**
 * 初始化 App、Component 拦截
 * 调用 interceptTaroEvents 时也会自动初始化
 */
export declare function initTaroInterceptor(): void;
export default interceptTaroEvents;
