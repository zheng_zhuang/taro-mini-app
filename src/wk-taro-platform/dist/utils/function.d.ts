/**
 * 安全执行
 */
export declare function safeCall(obj: any, name: string, ...args: any[]): any;
/**
 * 判断原型链上是否存在对应属性
 * @param Ctor
 * @param name
 * @returns
 */
export declare function isDefineProperty(Ctor: Function, name: string): boolean;
/**
 * 属性拦截
 * @param object
 * @param name
 * @param getter
 * @param setter
 */
export declare function interceptProperty<T = any>(object: Object, name: PropertyKey, getter: () => T, setter?: (value: T) => void): T | undefined;
