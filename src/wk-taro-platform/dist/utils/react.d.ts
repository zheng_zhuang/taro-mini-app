import { ComponentType, ComponentClass } from './react';
/**
 * 是否为类组件
 * @param Ctor
 * @returns
 */
export declare function isClassComponent<T>(Ctor: ComponentType<T>): Ctor is ComponentClass<T>;
