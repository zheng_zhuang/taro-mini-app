import Taro from '@tarojs/taro';
/**
 * 平台断言
 * @param env
 */
export declare function assertPlatform(env: Taro.ENV_TYPE): void;
