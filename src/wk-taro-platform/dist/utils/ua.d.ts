export declare function isH5(): boolean;
/**
 * 是否为微信公众号
 */
export declare function isWechatOfficialAccount(): boolean;
