export declare const NOOP_OBJECT: {};
export declare const NOOP: () => void;
export declare const COMPONENT_LIFECYCLES: Array<keyof typeof COMPONENT_LIFECYCLES_TO_HOOKS>;
export declare const COMPONENT_LIFECYCLES_TO_HOOKS: {
    componentDidShow: string;
    componentDidHide: string;
    onPullDownRefresh: string;
    onReachBottom: string;
    onPageScroll: string;
    onResize: string;
    onShareAppMessage: string;
    onTabItemTap: string;
    onTitleClick: string;
    onOptionMenuClick: string;
    onPullIntercept: string;
    onShareTimeline: string;
    onAddToFavorites: string;
    onReady: string;
};
