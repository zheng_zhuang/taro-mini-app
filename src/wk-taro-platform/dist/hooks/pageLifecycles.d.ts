export declare function usePageDidShow(fn: () => any): void;
export declare function usePageDidHide(fn: () => any): void;
export declare function usePageReady(fn: () => any): void;
