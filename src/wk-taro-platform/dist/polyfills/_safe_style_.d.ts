export declare function styleStrToObj(value: any): any;
/**
 * 跨平台 style 适配, style 需要重构为对象形式，以在 H5 和 React Native 端适配。
 */
export declare function _safe_style_(value: any): any;
