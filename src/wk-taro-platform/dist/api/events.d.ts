import Taro from '@tarojs/taro';
/**
 * 惟客宝自定义事件
 */
export declare enum WKEvent {
    SET_SHARE_APP_MESSAGE = "WK_SET_SHARE_APP_MESSAGE",
    SET_SHARE_TIMELINE = "WK_SET_SHARE_TIMELINE"
}
declare function $triggerWKEvent(event: WKEvent, ...args: any[]): void;
declare function $listenWKEvent(event: WKEvent.SET_SHARE_APP_MESSAGE, cb: (page: Taro.PageInstance, params: Taro.ShareAppMessageReturn | null) => void): Function;
declare function $listenWKEvent(event: WKEvent.SET_SHARE_TIMELINE, cb: (page: Taro.PageInstance, params: Taro.ShareTimelineReturnObject | null) => void): Function;
export { $triggerWKEvent, $listenWKEvent };
export default WKEvent;
