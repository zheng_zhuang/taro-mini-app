/**
 * 类型定义
 */
export interface WKRouterInfo<TParams extends Partial<Record<string, string>> = Partial<Record<string, string>>> {
    params: TParams;
    path: string;
}
