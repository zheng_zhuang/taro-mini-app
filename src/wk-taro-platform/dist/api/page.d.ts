import Taro from '@tarojs/taro';
import { WKRouterInfo } from './type';
/**
 * 是否为 Hash Mode 路由
 */
export declare function $H5IsHashMode(): boolean;
/**
 * 跨平台兼容的 getCurrentInstance.router
 * 问题1：在 H5 端，App 生命周期中无法获取参数和路由信息，在 小程序中可以安全地获取
 * 问题2：在 H5 端，path 会携带查询字符串，而 小程序端没有
 * 问题3： 小程序的 App 中的 router 和 页面中的 router 也有区别：
 *   App 中 有 query、referrerInfo、scene、shareTicket 这些参数
 *   Page 中 有 onHide、onReady、onShow 等参数。
 *   所以这里最好只用 path、params、其他参数是平台相关的可以在 App onLaunch 的 option 中拿取
 */
export declare function $getRouter<TParams extends Partial<Record<string, string>> = Partial<Record<string, string>>>(): WKRouterInfo<TParams>;
/**
 * 获取页面栈中的上一个页面，Taro.Compoonent 实例
 * @deprecated 不建议使用，因为无法安全地获取。另外获取和操作上一个页面也是不推荐的
 */
export declare function $getPreviousPage(): Taro.Page | null;
/**
 * 获取页面栈中的上一个页面，Taro.Compoonent 实例
 * @deprecated
 */
export declare function $getPreviousTaroPage(): void;
/**
 * 获取当前页面实例
 * 注意：
 * 问题1： 必须在任意一个页面挂载了之后才能获取到。比如你在 App componentDidMount 的时候是
 * 获取不到页面节点的，因为这个时候页面还没开始挂载渲染
 * 问题2：page.options 其他端没有
 *
 */
export declare function $getCurrentPage(): Taro.PageInstance | null;
/**
 * 获取 Taro 页面实例
 * @deprecated 请使用 $getCurrentPage
 * @returns
 */
export declare function $getCurrentTaroPage(): Taro.PageInstance | null;
/**
 * 获取当前页面路由，需兼容不同平台
 * 注意只包含路径信息，不包含查询字符串, 而且没有 '/' 前缀，例如 wxat-common/xxx
 *
 * @deprecated 请使用 $getRouter 重构
 * @param currentPage
 * @return {string}
 */
export declare function $getCurrentPageRoute(currentPage?: Taro.PageInstance | null): string;
/**
 * 获取当前页带参数的url
 * @param {Boolean} withArgs 是否获取带参数的完整url，默认 true
 * @returns {string}
 */
export declare function $getCurrentPageUrl(withArgs?: boolean): string;
