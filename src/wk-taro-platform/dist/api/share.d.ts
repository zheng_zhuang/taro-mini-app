import Taro from '@tarojs/taro';
/**
 * 获取当前 $setShareAppMessage 设置的值
 * @returns
 */
export declare function $getShareAppMessage(page?: Taro.PageInstance): Taro.ShareAppMessageReturn | undefined;
/**
 * 获取当前 $setShareAppMessage 设置的值
 * @returns
 */
export declare function $getShareTimeline(page?: Taro.PageInstance): Taro.ShareTimelineReturnObject | undefined;
/**
 * 触发 App 分享, 主要用于 H5 端，模拟按钮点击分享。
 * @param evt
 */
export declare function $triggerShareAppMessage(evt?: Taro.ShareAppMessageObject): Taro.ShareAppMessageReturn | undefined;
/**
 * 触发 朋友圈 分享, 主要用于 H5 端，模拟按钮点击分享。
 * @param evt
 */
export declare function $triggerShareTimeline(evt?: Taro.ShareAppMessageObject): Taro.ShareTimelineReturnObject | undefined;
/**
 * 命令式设置分享信息
 * @param data 分享信息, 结构同 onShareAppMessage
 * @param onShare 分享回调。当设置的 data 被最终分享时触发，目前仅小程序端支持
 * @returns
 */
export declare function $setShareAppMessage(data: Taro.ShareAppMessageReturn, onShare?: (evt: Taro.ShareAppMessageObject) => void): () => void;
/**
 * 命令式设置分享朋友圈
 * @param data 分享信息, 结构同 onShareTimeline
 * @param onShare 分享回调。当设置的 data 被最终分享时触发，目前仅小程序端支持
 * @returns
 */
export declare function $setShareTimeline(data: Taro.ShareTimelineReturnObject, onShare?: (evt: Taro.ShareAppMessageObject) => void): () => void;
