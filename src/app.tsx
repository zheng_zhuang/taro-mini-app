import {$getRouter} from '@/wk-taro-platform';
import React, {Component} from 'react';
import '@/wk-taro-platform/reset';
import './wxat-common/utils/polyfills';
import './pre';
import Taro, {getCurrentInstance} from '@tarojs/taro';
import {Provider} from 'react-redux';
import store from '@/store';

import login from '@/wxat-common/x-login';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';
import {updateGlobalDataAction} from './redux/global-data';
import shareUtil from '@/wxat-common/utils/share';
import {qs2obj} from '@/wxat-common/utils/query-string';
import {ignoreLoginList} from '@/wxat-common/x-login/ignoreLoginList';
import {notifyStoreDecorateRefresh} from '@/wxat-common/x-login/decorate-configuration-store';
import scan from './wxat-common/utils/scanH5'
import utils from "./wxat-common/utils/util";
import getUpdateManager from '@/wxat-common/utils/updateManage';
import './app.scss';
import {updateBaseAction} from './redux/base';
import sensors from '@/sdks/buried/sensors/saSdk.js';
class App extends Component {
  state = {
    router: {
      params: {
        query: {},
        path: ''
      }
    }
  }
  $sensors: typeof sensors;

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */


  /**
   * 检测启动场景
   */
  checkAppEnter() {
    const {params} = this.state.router; // 获取启动时所有参数
    const {path} = params || {scene: 0, path: ''};
    // 直播间进入场景
    if (!!path && path.includes('wx2b03c6e691cd7370/pages')) {
      return;
    }
    // 非直播间进入，隐藏tabbar，测试直播调用tabbar操作是否引起内存泄漏 TODO
    wxApi.hideTabBar();
  }


  onLaunch() {
    this.$sensors = sensors;
    this.setState({
      router: getCurrentInstance().router
    })
    // 触发版本更新
    getUpdateManager()
  }


  // 热启动，上报数据
  async componentDidShow() {
    this.state.router = $getRouter();
    const {params} = this.state.router; // 获取启动时所有参数
    if (process.env.IOS_PAY === 'true' || process.env.OPEN_MP === 'true') {
      return
    }
    let ignoreLoginInfo = true;

    // 判断扫是从影视过来的，跳到影视落地页
    if(params?.q) {
      const decodeUrl =decodeURIComponent(decodeURIComponent(params?.q))
      if (utils?.getQueryString(decodeUrl, 'qrCodeType') == 1) {
        const qrCodeId = utils.getQueryString(decodeUrl, 'qrCodeId');
        const apaasStoreId = utils.getQueryString(decodeUrl, 'apaasStoreId');
        wxApi.setStorageSync('qrCodeId', qrCodeId);
        wxApi.setStorageSync('filmQrCodeDetail', {'apaasStoreId': apaasStoreId});
        // 获取影视二维码详情
        await wxApi.request({
          url: api.qrCode.getDetail + `?qrCodeId=${qrCodeId}`,
          method: 'GET',
          loading: false,
          checkSession: false,
        }).then((res) => {
          if (res.data) {
            wxApi.setStorageSync('filmQrCodeDetail', res.data);
            utils.sensorsReportData('film_scan', {})
            Taro.getApp().sensors.registerApp({
              ...utils.getFilmScanData()
            })
            params._ref_storeId = res.data.apaasStoreId
          } else {
            wxApi.clearStorageSync('filmQrCodeDetail')
            wxApi.clearStorageSync('qrCodeId')
            Taro.getApp().sensors.registerApp({
              ...utils.getFilmScanData()
            })
            setTimeout(() => {
              wxApi.showModal({
                title: '提示',
                content: res.errorMessage,
                confirmText: "重新扫码",
                showCancel: false,
                success() {
                  wxApi.exitMiniProgram()
                }
              })
            }, 1500)
            wxApi.switchTab({
              url: '/wxat-common/pages/home/index',
            });
          }
        })
      }
    }

    // 店员核销不执行重新登录
    if (params && ~ignoreLoginList.findIndex((item) => item === params?.path)) {
      ignoreLoginInfo = false;
    }
    // 原生api挂载
    wxApi.$init();
    // 检测启动场景
    this.checkAppEnter();

    if (process.env.TARO_ENV === 'h5') {
      // @ts-expect-error
      window.wxApi = wxApi;
      // H5授权缺少appid拦截处理
      if (window.SETUP_ERROR) {
        return;
      }
    }
    // 错误页面，点击重试事件分发接受
    Taro.eventCenter.on('resetup', () => {
      this.setup(true);
    });
    // 根据链接携带参数，获取房间号相关信息  TODO: 部分代码逻辑跟initialShareSource有重复嫌疑
    this.cacheShareRef(params);
    // 获取页面来源，进行全局source缓存
    this.initialShareSource();

    this.setup(false, ignoreLoginInfo);
    // 查询微信模板id
    this.initialSubscribeList();

    const _sk = utils.getUrlParam('sk')
    // 判断扫tv二维带有sk码,跳到商品详情
    if (!!_sk) {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/goods-detail/index',
        data: {
          sk: _sk
        }
      })
    }
  }

  // 企业员工查询
  outStaff() {
    const pages = Taro.getCurrentPages();
    // 小程序兼容判断
    if (process.env.TARO_ENV == 'h5') {
      if (window.location.href.indexOf("separate-package") != -1) {
        return;
      }
    } else {
      if (pages && pages[0] && pages[0].route.indexOf("separate-package") != -1) {
        return;
      }
    }
    wxApi.request({
      url: api.user.outStaff,
      data: {},
      quite: true
    })
      .then(res => {
        if (!!res.data && !!res.data.id) {
          store.dispatch(updateBaseAction({
            enterpriseStaff: res.data.id ? true : false, // 是否携旅员工
            enterpriseStaffInfo: res.data // 携旅员工信息
          }));
        }
      })
      .catch(error => {
        console.log("outStaff: error: " + JSON.stringify(error));
        return false
      });
  }

  // 切换门店
  async changeStore(storeId) {
    await wxApi.request({
      url: api.store.choose_new + '?storeId=' + storeId,
      loading: true,
      quite: true
    })
      .then((res) => {
        const store = res.data;
        if (store) {
          notifyStoreDecorateRefresh(store);
        }
      });
  }

  private async cacheShareRef(options) {
    // TODO: 适配
    const query = options.query || {};
    const scene = options.scene || '';
    const queryScene = query.scene;
    const source = query.source;
    const sk = shareUtil.MapperQrCode.getMapperKey(queryScene);

    // 场景一:门店二维码扫码进入 query = {scene: "storeId%3D15806%26force%3D1"}
    // 场景一:好友分享小程序链接进入 query = {_ref_bz:'home',_ref_useId:4572,_ref_storeId:10286,_ref_wxOpenId:'oEwJI4_WWyyWdcTlBryzru4YvgXA'};
    if (options.id && options.tp) {
      const _data: any = await scan.scanRoomCodeFunc(options)
      options._ref_storeId = _data.customCodeData.sid
    }

    if (options._ref_storeId) {
      this.changeStore(options._ref_storeId);
    }

    let _ref_storeId = query._ref_storeId || options._ref_storeId || '';
    let _ref_useId = query._ref_useId || '';

    // 门店二维码进入，门店信息存放在scene中
    const qs = qs2obj(queryScene + '');
    if (qs.storeId) {
      _ref_storeId = qs.storeId;
      _ref_useId = -1;
    }

    let codeParams = {};

    if (query.scene || scene != '') {
      const _scene = query.scene ? decodeURIComponent(query.scene) : decodeURIComponent(scene);
      const params: any = {};
      _scene.split('&').forEach(item => {
        const key = item.split('=')[0];
        const value = item.split('=')[1];
        params[key] = value;
      })

      codeParams = params;

      const distributorId = params.distributorId;
      if (distributorId) {
        store.dispatch(updateGlobalDataAction({distributorId: distributorId}));
      }
      // 存在No订单号，则是扫商品二维码下单支付,不需要调取房间号接口，存在id则是新码，否则是旧码
    }
    // 新逻辑-自定义页面码兼容
    if (codeParams.tp == 4) {
      wxApi.request({
        url: api.wxoa.getWXOACustomParams,
        isLoginRequest: true,
        quite: true,
        method: 'GET',
        data: {
          id: codeParams.id
        },
      }).then(res => {
        let path = res.data.scene
        let templateId = res.data.templateId
        wxApi.$redirectTo({
          url: path,
          data: {
            pId: templateId
          }
        })
      })
    }
    // 商品码兼容
    else if (codeParams.tp == 3) {
      wxApi.request({
        url: api.wxoa.getWXOADetailParams,
        isLoginRequest: true,
        quite: true,
        method: 'GET',
        data: {
          id: codeParams.id
        },
      }).then(res => {
        if (res.data) {
          wxApi.$redirectTo({
            url: res.data.maPath,
            data: {
              id: codeParams.id,
              verificationNo: res.data.verificationNo,
              itemNo: res.data.verificationNo,
              storeId: res.data.storeId
            }
          })
        }
      })
    }
    else if (codeParams.tp || (!options.No && options.tp && options.tp != 3 && options.tp != 4)) {
      if (codeParams.tp || options.id) {
        wxApi.request({
          url: api.queryQrCodeScenes,
          isLoginRequest: true,
          quite: true,
          method: 'GET',
          data: {
            id: codeParams.id || options.id
          },
        }).then(({data}) => {
          const dataObj: any = {};
          data.split('&').forEach(item => {
            const key = item.split('=')[0];
            const value = item.split('=')[1];
            dataObj[key] = value;
          })
          return wxApi.request({
            url: api.selectQrCodeNumber,
            isLoginRequest: true,
            quite: true,
            method: 'POST',
            data: {
              id: codeParams.id || options.id,
              hotelId: dataObj.pId || dataObj.storeId,
              qrCodeNumber: options.qrcodeNo,
            },
          })
        }).then(({data: {roomCode, roomNumber, qrCodeTypeId, qrCodeTypeName, storeId}}) => {
          if (storeId) {
            this.changeStore(storeId)
          }
          store.dispatch(updateGlobalDataAction({
            roomCode: options.tp == 2 ? roomNumber : roomCode,
            roomCodeTypeId: qrCodeTypeId, // 房间号类型id
            roomCodeTypeName: qrCodeTypeName // 房间号类型名称
          }));
        }).catch(err => {
        });
      }
    }
    if (source) {
      shareUtil.setSource(source);
    }

    login.setBuryInfo({
      sk, // 分享参数唯一ID
      source,
      scene,
      bzscene: query.bzscene || '',

      _ref_storeId,
      _ref_useId,
      _ref_wxOpenId: query._ref_wxOpenId || '',
      _ref_bz: query._ref_bz || '',
      _ref_bz_id: query._ref_bz_id || '',
      _ref_bz_name: query._ref_bz_name || '',
      _ref_scene_name: query._ref_scene_name || '',
    });
  }
  /**
   * 应用启动
   */
  async setup(retry = false, ignoreLoginInfo = false) {
    const {dispatch} = store;
    try {
      await login.login({initialLogin: true, ignoreLoginInfo});
      dispatch(
        updateBaseAction({
          loginEnvError: false,
          loginEnvErrorMessage: null,
        })
      );
    } catch (error) {
      let msg;
      if (!!error && !!error.stack) {
        msg = error.stack;
      } else if (!!error && !!error.errMsg) {
        msg = error.errMsg;
      } else {
        msg = (error || '').toString();
      }
      dispatch(
        updateBaseAction({
          loginEnvError: true,
          loginEnvErrorMessage: msg,
        })
      );

      if (!retry) {
        wxApi.navigateTo({
          url: `/wxat-common/pages/error-page/index`,
        });
      }
    } finally {
      // 企业员工查询 TODO: 这个有冗余调用嫌疑
      // app 重复调用登录接口
      // this.outStaff();
      // 获取手机型号及ios情况下获取支付配置 TODO：了解获取支付配置的作用
      this.isIOS()
      // 查询是否开启送礼 TODO：一定要在入口页面调用？
      // app 重复调用登录接口
      // wxApiProxy.queryIsGiveGift();
      // 获取模板消息
      // app 重复调用登录接口
      // wxApiProxy.getTemplateIdList();
    }
  }

  // 查询微信模板id
  private initialSubscribeList = async () => {
    // app 重复调用登录接口
    // const {data} = await wxApi.request({
    //   quite: true,
    //   url: api.getTemplateID,
    //   loading: false,
    //   data: {},
    // });
    //
    // const subscribeList = {};
    // if (data) {
    //   data.forEach(({id, wxSubMsgId}) => (subscribeList[id] = wxSubMsgId));
    // }
    // store.dispatch(updateGlobalDataAction({subscribeList}));
  };

  // 获取分享来源
  initialShareSource = () => {
    if (typeof this.state.router.params.query !== 'string' && this.state.router.params.query) {
      const {source = 'wechat'} = this.state.router.params.query;
      shareUtil.setSource(source);
    }
  };

  // 获取ios支付设置
  queryIosSetting = () => {
    wxApi.request({
      url: api.IosPay.queryIosSetting,
      quite: true,
      loading: true,
      data: {
        appid: store.getState().ext.appId
      }
    })
      .then(res => {
        if (res.data && res.data.payType) {
          store.dispatch(updateGlobalDataAction({
            iosSetting: res.data
          }))
        }
      })
      .catch(error => {
        console.log('app-queryIosSetting', error);
      })
  }

  isIOS = () => {
    const res = wxApi.getSystemInfoSync()
    store.dispatch(updateGlobalDataAction({
      isIOS: Boolean(/(iPhone|iPad|iPod|iOS)/i.test(res.platform)) || Boolean(/(iPhone|iPad|iPod|iOS)/i.test(res.system))
    }))
    if (Boolean(/(iPhone|iPad|iPod|iOS)/i.test(res.platform)) || Boolean(/(iPhone|iPad|iPod|iOS)/i.test(res.system))) {
      //获取ios 支付配置
      this.queryIosSetting()
    }
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数

  // 应用卸载
  componentDidHide() {
    // wxApi.clearStorageSync('filmQrCodeDetail')
    // wxApi.clearStorageSync('qrCodeId')
  }
  render() {
    return <Provider store={store}>{this.props.children}</Provider>;
  }
}

export default App;
