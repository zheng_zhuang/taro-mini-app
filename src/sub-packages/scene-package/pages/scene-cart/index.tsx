import { $getRouter,_fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, Radio, Text, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';

import './index.scss';

interface IState{
  tmpStyle: any;
  editAllChecked: boolean;
  goodsList: any[];
  goodsEdit: boolean;
  allChecked: boolean;
  count:number;
  totalPrice:number
}
class SceneCart extends React.Component<{},IState> {
  state={
    tmpStyle: {},
    editAllChecked: false,
    goodsList: [],
    goodsEdit: false,
    allChecked: false,
    count:0,
    totalPrice:0
  }

  $router = $getRouter()
  componentDidMount() {
    const that = this;
    wxApi.getStorage({
      key: 'sceneCartList',
      success(res: { data: any[]; }) {
        // 处理表单数据
        const arr = res.data.map((item: string) => {
          return {
            ...JSON.parse(item),
            checked: true,
            editChecked: false,
            isOpen: '',
          };
        });
        that.setState(
          {
            goodsList: arr,
          },

          () => {
            that.count();
            that.judgeAllCheck();
          }
        );
      },
    });

    this.getTemplateStyle();
  }

  handleAllCheck=()=>{
    // 全选
    console.log(121212)
    let list:any[] = this.state.goodsList;
    const allCheck = this.state.allChecked;
    const editAllChecked = this.state.editAllChecked;
    const goodsEdit = this.state.goodsEdit;
    if (goodsEdit) {
      if (editAllChecked) {
        list = list.map((item) => {
          return {
            ...item,
            editChecked: false,
          };
        });
      } else {
        list = list.map((item) => {
          return {
            ...item,
            editChecked: true,
          };
        });
      }
    } else {
      if (allCheck) {
        list = list.map((item) => {
          return {
            ...item,
            checked: false,
          };
        });
      } else {
        list = list.map((item) => {
          return {
            ...item,
            checked: true,
          };
        });
      }
    }
    this.setState(
      {
        goodsList: list,
        allChecked: goodsEdit ? allCheck : !allCheck,
        editAllChecked: goodsEdit ? !editAllChecked : editAllChecked,
      },

      () => {
        this.count();
      }
    );
  }

  judgeAllCheck=()=>{
    // 判断是否全选
    const list:any[] = this.state.goodsList;
    for (let i = 0; i < list.length; i++) {
      if (this.state.goodsEdit) {
        if (!list[i].editChecked) {
          this.setState({
            editAllChecked: false,
          });

          break;
        }
        this.setState({
          editAllChecked: true,
        });
      } else {
        if (!list[i].checked) {
          this.setState({
            allChecked: false,
          });

          break;
        }
        this.setState({
          allChecked: true,
        });
      }
    }
  }

  count=()=>{
    // 计算总价和下单数量
    const serviceTreeData = this.state.goodsList.filter((item:any) => item.checked);
    console.log(this.state.goodsList);
    let count = 0;
    let totalPrice = 0;
    serviceTreeData.forEach((item:any) => {
      count += item.checked ? item.count : 0;
      totalPrice += item.totalPrice;
    });
    this.setState({
      count,
      totalPrice: totalPrice / 100 || 0,
    });
  }

  handleCheck=(index: number)=>{
    // 选择商品
    const list:any[] = this.state.goodsList;
    if (this.state.goodsEdit) {
      list[index].editChecked = !list[index].editChecked;
    } else {
      list[index].checked = !list[index].checked;
    }
    this.setState(
      {
        goodsList: list,
      },

      () => {
        this.judgeAllCheck();
        this.count();
      }
    );

    this.count();
  }

  // 编辑
  handleEdit=()=>{
    let list:any[] = this.state.goodsList;
    if (!this.state.goodsEdit) {
      list = list.map((item) => {
        return {
          ...item,
          editChecked: false,
        };
      });
    }
    this.setState({
      goodsEdit: !this.state.goodsEdit,
      goodsList: list,
    });
  }

  handleFoot=async ()=>{
    // 立即下单或删除
    if (!authHelper.checkAuth()) return;
    const goodsEdit = this.state.goodsEdit;
    const list = this.state.goodsList;
    if (goodsEdit) {
      const ishasDeleteList = list.find((item:any) => {
        return item.editChecked == true;
      });
      if (!ishasDeleteList) {
        wxApi.showToast({
          icon: 'none',
          title: '请选择要删除的服务',
        });

        return;
      }
      const filterList = list.filter((item:any) => {
        return !item.editChecked;
      });
      // console.log(filterList)
      // if (filterList.length) {
      //   wx.showToast({
      //     icon: 'none',
      //     title: '请选择要删除的服务',
      //   })
      //   return
      // }
      this.setState({
        goodsEdit: !this.state.goodsEdit,
        goodsList: filterList,
      },()=>{
        this.count();
      });


      // 缓存购物车数据
      const stringifyList = filterList.map((item) => {
        return JSON.stringify(item);
      });
      wxApi.setStorageSync('sceneCartList', stringifyList);
      wxApi.setStorageSync('sceneCartSelectList', stringifyList);
      this.count();
    } else {
      // 下单数据
      const orderList = this.state.goodsList.filter((item:any) => {
        return item.checked;
      });
      const stringifyOrderList = orderList.map((item) => {
        return JSON.stringify(item);
      });
      wxApi.setStorageSync('sceneCartSelectList', stringifyOrderList);
      wxApi.$navigateTo({
        url: '/sub-packages/scene-package/pages/scene-confirm-order/index',
      });

      // this.confirmOrder()

      // 判断是否包含需要扫码的商品
      // const flag = await utils.checkNeedScan(stringifyOrderList);
      // if (!flag) {
      //   wxApi.$navigateTo({
      //     url: '/sub-packages/scene-package/pages/scene-confirm-order/index',
      //     data: {}
      //   })
      //   return
      // }

      // if (state.roomCode) {
      //   if (orderList.length == 0) {
      //     wx.showToast({
      //       icon: 'none',
      //       title: '请选择要下单的服务',
      //     })
      //     return
      //   }
      //   wx.navigateTo({
      //     url: '/sub-packages/scene-package/pages/scene-confirm-order/index',
      //     data: {
      //       roomCode: state.roomCode,
      //       roomCodeTypeId: state.roomCodeTypeId,
      //       roomCodeTypeName: state.roomCodeTypeName,
      //       trueCode: state.roomCodeTypeName ? state.roomCodeTypeName + '-' + state.roomCode : state.roomCode
      //     }
      //   })
      // } else {
      //   wxApi.$navigateTo({
      //     url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
      //   })
      // }
    }
  }

  confirmOrder=()=>{
    let reservationItemList:any[] = [];
    reservationItemList = wxApi.getStorageSync('sceneCartSelectList');
    reservationItemList = reservationItemList.map((item) => {
      return {
        ...JSON.parse(item),
        isOpen: false,
      };
    });
    const data:any[] = [];
    reservationItemList.forEach((current) => {
      data.push({
        reservationId: current.id,
        reservationSpecId: current.specInfo.id,
        startTime: current.reserveStartTime,
        endTime: current.reserveEndTime,
        formExt: JSON.stringify(current.formExt),
      });
    });

    const param = {
      reservationItemList: data,
      // orderMessage: this.data.orderMessage,
    };

    wxApi
      .request({
        url: api.scence.ordercreateReservationOrder,
        method: 'post',
        loading: true,
        data: param,
      })
      .then((res: { data: any; }) => {
        const payInfo = res.data;
        wxApi.requestPayment({
          timeStamp: payInfo.timeStamp + '',
          nonceStr: payInfo.nonceStr,
          package: 'prepay_id=' + payInfo.prepayId,
          signType: 'RSA',
          paySign: payInfo.paySign,
          success(e: any) {
            const treeData = (Taro.getStorageSync('sceneCartList') || []).map((item: string) => {
              return JSON.parse(item);
            });

            for (let v = 0; v < treeData.length; v++) {
              for (let i = 0; i < reservationItemList.length; i++) {
                if (treeData[v].id == reservationItemList[i].id) {
                  treeData.splice(v, 1);
                }
              }
            }
            const stringifyTreeData = treeData.map((item: any) => {
              return JSON.stringify(item);
            });
            Taro.setStorageSync('sceneCartList', stringifyTreeData);
            Taro.setStorageSync('sceneCartSelectList', stringifyTreeData);

            wxApi.$navigateTo({
              url: '/sub-packages/marketing-package/pages/living-service/order-list/index',

              data: {
                orderNo: payInfo.orderNo,
              },
            });
          },
          fail(e: any) {
            wxApi.$navigateTo({
              url: '/sub-packages/marketing-package/pages/living-service/order-list/index',

              data: {
                orderNo: payInfo.orderNo,
              },
            });
          },
        });
      });
  }

  showMoreInfo=(item: never,index: number)=>{
    // 展开备注信息
    // let isOpen = e.target.dataset.item.isOpen;
    // let index = e.target.dataset.index;
    const list:any[] = this.state.goodsList
    item.isOpen = !item.isOpen
    list.splice(index,1,item)
    this.setState({
      goodsList:list
    });
  }

  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      Taro.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { goodsEdit, editAllChecked, allChecked, tmpStyle, goodsList, totalPrice } = this.state;
    return (
      <View data-scoped="wk-ssps-SceneCart" className="wk-ssps-SceneCart">
        {goodsList.length !== 0 && (
          <View className="header">
            <View>
              {/* <RadioGroup> */}
              {
                process.env.TARO_ENV === 'weapp' ? <Radio
                value={goodsEdit ? editAllChecked : allChecked}
                color={tmpStyle.btnColor}
                checked={goodsEdit ? editAllChecked : allChecked}
                onClick={this.handleAllCheck}
              ></Radio> : <Radio
              value={goodsEdit ? editAllChecked : allChecked}
              color={tmpStyle.btnColor}
              checked={goodsEdit ? editAllChecked : allChecked}
              onChange={this.handleAllCheck}
            ></Radio>
              }

              {/* </RadioGroup> */}
              <Text className="text">全选</Text>
            </View>
            <Text className="text" onClick={this.handleEdit}>{goodsEdit ? '完成' : '编辑'}</Text>
          </View>
        )}

        <View className="container">
          {goodsList.length !== 0 ? (
            <View>
              {goodsList?.map((item, index) => {
                  return (
                    <View key={index} className="goods-item">
                      <View className="goods-item-info">
                        <View className="radio-image-box">
                          {
                            process.env.TARO_ENV === 'weapp' ? <Radio
                            className="radio"
                             value={goodsEdit ? item.editChecked : item.checked}
                             color={tmpStyle.btnColor}
                             checked={goodsEdit ? item.editChecked : item.checked}
                             onClick={()=>this.handleCheck(index)}
                           ></Radio> : <Radio
                           className="radio"
                            value={goodsEdit ? item.editChecked : item.checked}
                            color={tmpStyle.btnColor}
                            checked={goodsEdit ? item.editChecked : item.checked}
                            onChange={()=>this.handleCheck(index)}
                          ></Radio>
                          }

                          <Image src={filters.imgListFormat(item.imgUrl)} className="goods-item-info-img"></Image>
                        </View>
                        <View className="goods-item-box">
                          <View className="goods-item-info-name">
                            <View>{item.name}</View>
                            <View className="cash">{'规格 ' + item.specInfo.spec}</View>
                            <View className="cash time">
                              {'时间 ' + item.showReserveStartTime + ' - ' + item.showReserveEndTime}
                            </View>
                          </View>
                          <View className="goods-item-info-price">
                            <Text className="text">{'￥' + item.totalPrice / 100}</Text>
                            {!item.isOpen ? (
                              <Image
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                className="arrow"
                                onClick={()=>this.showMoreInfo(item,index)}
                              ></Image>
                            ) : (
                              <Image
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/top-arrow.png"
                                className="arrow"
                                onClick={()=>this.showMoreInfo(item,index)}
                              ></Image>
                            )}
                          </View>
                        </View>
                      </View>
                      <View className={'goods-item-info-more ' + (item.isOpen ? 'open' : 'close')}>
                        备注信息
                        <View className="goods-item-info-form">
                          {item.formExt?.map((el: { label: string; value: any; }, index: string | number | undefined) => {
                              return (
                                <View className="view" key={index}>
                                  <View>{el.label + '：' + (el.value ? el.value : '')}</View>
                                </View>
                              );
                            })}
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>
          ) : (
            <View className="empty">暂无预定清单</View>
          )}
        </View>
        {goodsList.length !== 0 && (
          <View className="goods-all-info">
            <View className="goods-all-info-price view">
              <View>合计：</View>
              <Text className="font-color text">{'￥' + totalPrice}</Text>
            </View>
            {goodsEdit && (
              <View
                className="mini-btn view"
                type="warn"
                size="mini"
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleFoot}
              >
                删除
              </View>
            )}

            {!goodsEdit && (
              <View
                className="mini-btn"
                type="warn"
                size="mini"
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleFoot}
              >
                立即下单
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}


export default SceneCart;
