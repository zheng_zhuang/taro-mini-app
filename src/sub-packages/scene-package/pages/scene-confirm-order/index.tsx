import { $getRouter,_fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Text, Textarea, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import utils from '@/wxat-common/utils/util.js';
import priceHandler from '../../utils/priceHandler.js';
import constants from '@/wxat-common/constants/index.js';
import couponEnum from '@/wxat-common/constants/couponEnum.js';
import {connect} from 'react-redux'
import hoc from '@/hoc/index'
import BottomDialog from '@/wxat-common/components/bottom-dialog/index';
import List from '../../components/list/index';
import './index.scss';
import payUtils from '@/wxat-common/utils/pay';
import pay from '@/wxat-common/utils/pay';

interface IState{
  tmpStyle:any,
  totalPrice: number,
  cashPledge: number,
  orderMessage: string,
  detail: any,
  coupons: any,
  couponEnum:any,
  priceData: any,
  goodsList:any[],
  isCoupon:number | boolean | null,
  curCoupon:any,
  curRedPacke:any,
  serveCard:any,
  curGiftCard:any,
  payAble:boolean,
  price: Object | null,
  count:number | null,
  priceNoPledge:number | null,
  payDetails:any,
  seckillNo:any,
  useLogisticsCoupon:any,
  choosedDelivery:any,
  deliveryWay:any,
  scoreName:any,
  goodsIntegral:any,
  goodsInternal:any,
  isCardpackType:any,
  goodsInfoList:any,
  goodsTotalPrice:any,
  isEstateType:any,
  allFreightCollect:any,
  isGiftActivity:any,
  isFrontMoneyItem:any,
}
interface IProps{
  currentStore:any
}
const mapStateToProps = (state) => {
  return {currentStore:state.base.currentStore};
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class SceneConfirmOrder extends React.Component<IProps,IState>{
  state={
    payLoading: false, // 支付提交中
    tmpStyle: {},
    totalPrice: 0,
    cashPledge: 0,
    orderMessage: '',
    detail: '',
    coupons: {},
    couponEnum:couponEnum,
    priceData: {},
    goodsList:[],
    isCoupon:null,
    curCoupon:{},
    curRedPacke:{},
    serveCard:{},
    curGiftCard:{},
    payAble:true,
    price: null,
    count:null,
    priceNoPledge:null,
    payDetails:[],
    seckillNo:null,
    useLogisticsCoupon:null,
    choosedDelivery:null,
    deliveryWay:{
      EXPRESS:{
        value:null
      },
      SELF_DELIVERY:{
        value:null
      },
      CITY_DELIVERY:{
        value:null
      }
    },
    scoreName:null,
    goodsIntegral:null,
    goodsInternal:null,
    isCardpackType:null,
    goodsInfoList:[],
    goodsTotalPrice:null,
    isEstateType:null,
    allFreightCollect:null,
    isGiftActivity:null,
    isFrontMoneyItem:null,
  }
  $router = $getRouter()
  _choseDialog = null
  chooseCardRef = React.createRef<any>()
  componentDidMount(){
    if (this.$router.params.detail) {
      this.setState({
        detail: JSON.parse(this.$router.params.detail),
      },()=>{
          this.getStorageCartList()
      });
    }else{
      this.getStorageCartList()
    }


    this.getTemplateStyle();
  }
  getStorageCartList=()=>{
    let that = this;
        wxApi.getStorage({
          key: 'sceneCartList',
          success(res) {
            // 处理表单数据
            let arr = res.data.map((item) => {
              return {
                ...JSON.parse(item),
                isOpen: '',
              };
            });
            if (that.state.detail) {
              that.setState(
                {
                  goodsList: [that.state.detail],
                },

                () => {
                  that.count();
                  that.handleGoodsInfoChange();
                }
              );
            } else {
              that.setState(
                {
                  goodsList: arr,
                },

                () => {
                  that.count();
                  that.handleGoodsInfoChange();
                }
              );
            }
          },
          fail(res) {
            if (that.state.detail) {
              that.setState(
                {
                  goodsList: [that.state.detail],
                },
                () => {
                  that.count();
                  that.handleGoodsInfoChange();
                }
              );
            }
          },
        });
  }
  errorTip=(msg:string)=>{
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }

  handleGoToCouponList=()=>{
    const couponSet = this.state.coupons.couponSet;
    if (!couponSet || couponSet.length < 1) {
      return this.errorTip('无可用优惠券');
    }
    this.setState({
      isCoupon: 1,
    });

    this.getChoseDialog().showModal();
  }

  // 获取优惠对话框
  getChoseDialog=():any=>{
    if (!this._choseDialog) {
      this._choseDialog = this.chooseCardRef.current
    }
    return this._choseDialog;
  }

  // 不使用优惠
  onNoUseCard=()=>{

    const keyObj = {
      1: 'curCoupon',
      2: 'curRedPacke',
      3: 'serveCard',
      4: 'curGiftCard'
    }
    const keyStr = keyObj[this.state.isCoupon]
    let _state = {}
    _state[keyStr] = {}
    this.setState(
      _state,
      () => {
        this.getChoseDialog().hideModal()
        this.handlePreferentialChange()
      }
    )
  }

  onSelectCard=(coupon)=>{
    // 优惠券或红包状态是可用的或者优惠类型为礼品卡或次数卡（次数后端返回都是可用的）
    if (
      (coupon.status && coupon.status === 1) ||
      coupon.cardType === constants.card.type.gift ||
      coupon.cardType === constants.card.type.coupon
    ) {
      const keyObj = {
        1: 'curCoupon',
        2: 'curRedPacke',
        3: 'serveCard',
        4: 'curGiftCard'
      }
      const keyStr = keyObj[this.state.isCoupon]
      let _state = {}
      _state[keyStr] = coupon
      this.setState(
        _state,
        () => {
          this.getChoseDialog().hideModal()
          this.handlePreferentialChange()
        }
      )
    } else {
      // this.errorTip(coupon.rejReason || "该优惠不能使用。");
      wxApi.showToast({
        icon: 'none',
        title: coupon.rejReason || '该优惠不能使用。',
      });
    }
  }

  /**
   * 优惠信息变化时的handler，
   * 如：优惠券选择，礼品卡选择、红包选择等
   */
  handlePreferentialChange=()=>{
    this.setState({
      payAble: false,
    });

    wxApi.showLoading({
      title: '请稍等',
    });

    let _state = this.state.priceData
    _state['orderRequestPromDTO.couponNo'] = this.state.curCoupon.code || ''
    this.setState(
      {
        priceData: _state
      },
      () => {
        priceHandler
        .getPrice(this.state.priceData)
        .then((res) => {
          const { price } = res;
          this.setState({
            price,
            payAble: true,
          });
        })
        .catch((error) => {
          if (!error.quite) {
            //未选择收货地址时报错
            wxApi.showToast({
              icon: 'none',
              title: error.msg,
            });
          }
        })
        .finally(() => {
          wxApi.hideLoading();
        });
      }
    )
  }

  //格式化数据
  formatData=()=>{
    let data:any = {
      expressType: 0,
      storeId: this.props.currentStore.id,
      'orderRequestPromDTO.couponNo': '',
    };

    data.itemDTOList = this.state.goodsList.map((item:any) => {
      let spec = '';
      switch (item.specInfo.specUnit) {
        case '1':
          spec = 'HOUR';
          break;
        case '2':
          spec = 'DAY';
          break;
        case '3':
          spec = 'WEEK';
          break;
        case '4':
          spec = 'MONTH';
          break;
        case '5':
          spec = 'YEAR';
      }

      const p = {
        itemType: 45,
        itemNo: item.itemNo,
        skuId: item.skuId,
        itemCount: item.count || 1,
        reservationItem: {
          reservationId: item.id,
          reservationSpecId: item.specInfo.id,
          spec,
          reservationStartTime: this.newDateFormate(item.reserveStartTime),
          reservationEndTime: this.newDateFormate(item.reserveEndTime),
        },
      };

      if (item.itemType !== null && item.itemType !== undefined) {
        p.itemType = item.itemType;
      }

      //促销相关的请求参数
      if (item.mpActivityType) {
        p.mpActivityType = item.mpActivityType;
      }
      if (item.mpActivityId) {
        p.mpActivityId = item.mpActivityId;
      }
      if (item.mpActivityName) {
        p.mpActivityName = item.mpActivityName;
      }

      //赠品相关的请求参数
      if (item.activityType) {
        p.activityType = item.activityType;
      }
      if (item.activityId) {
        p.activityId = item.activityId;
      }
      return p;
    });
    data = utils.formatParams(data);
    return data;
  }

  //格式化 获取价格需要的数据

  formatPriceData=()=>{
    let data:any = {
      expressType: 0,
      storeId: this.props.currentStore.id,
      'orderRequestPromDTO.couponNo': '',
      itemType: 45,
    };

    data.itemDTOList = this.state.goodsList.map((item:any) => {
      let spec = '';
      switch (item.specInfo.specUnit) {
        case '1':
          spec = 'HOUR';
          break;
        case '2':
          spec = 'DAY';
          break;
        case '3':
          spec = 'WEEK';
          break;
        case '4':
          spec = 'MONTH';
          break;
        case '5':
          spec = 'YEAR';
      }

      const p = {
        itemType: 45,
        itemNo: item.itemNo,
        skuId: item.skuId,
        itemCount: item.count || 1,
        reservationItemList: {
          reservationId: item.id,
          reservationSpecId: item.specInfo.id,
          reservationStartTime: this.newDateFormate(item.reserveStartTime),
          reservationEndTime: this.newDateFormate(item.reserveEndTime),
        },
      };

      if (item.itemType !== null && item.itemType !== undefined) {
        p.itemType = item.itemType;
      }

      //促销相关的请求参数
      if (item.mpActivityType) {
        p.mpActivityType = item.mpActivityType;
      }
      if (item.mpActivityId) {
        p.mpActivityId = item.mpActivityId;
      }
      if (item.mpActivityName) {
        p.mpActivityName = item.mpActivityName;
      }

      //赠品相关的请求参数
      if (item.activityType) {
        p.activityType = item.activityType;
      }
      if (item.activityId) {
        p.activityId = item.activityId;
      }
      return p;
    });

    data.reservationItemList = this.state.goodsList.map((item:any) => {
      let spec = '';
      switch (item.specInfo.specUnit) {
        case '1':
          spec = 'HOUR';
          break;
        case '2':
          spec = 'DAY';
          break;
        case '3':
          spec = 'WEEK';
          break;
        case '4':
          spec = 'MONTH';
          break;
        case '5':
          spec = 'YEAR';
      }

      const p = {
        reservationId: item.id,
        reservationSpecId: item.specInfo.id,
        reservationStartTime: this.newDateFormate(item.reserveStartTime),
        reservationEndTime: this.newDateFormate(item.reserveEndTime),
      };

      return p;
    });

    data = utils.formatParams(data);
    return data;
  }

  newDateFormate (date) {
    date = date.replace(/-/g, '/');
    return new Date(date).getTime() / 1000;
  }

  //计算优惠劵的价格
  handleGoodsInfoChange() {
    /*const denyPay = checkStockAndShefHandler.checkGoodsDenyPay(this.data);
    if (denyPay.deny) {
      wx.showToast({
        icon: "none",
        title: denyPay.reason,
      });
      return;
    }
    this.setData({
      payAble: false,
    });
    wx.showLoading({
      title: "请稍等",
    });*/

    let data = this.formatData();
    let priceData = this.formatPriceData();

    priceHandler
      .fetchPreferentialList(data)
      .then((res) => {
        if (res) {
          const { curRedPacke, curCoupon, coupons } = res;
          this.setState({
            curRedPacke,
            curCoupon,
            coupons,
          });

          priceData['orderRequestPromDTO.couponNo'] = curCoupon.code || '';
        }
        //获取订单价格
        this.setState({
          priceData,
        });

        return priceHandler.getPrice(priceData);
      })
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
          payAble: true,
        });
      })
      .catch((error) => {
        if (!error.quite) {
          //未选择收货地址时报错
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  count=()=>{
    // 计算总价和下单数量
    let serviceTreeData = this.state.goodsList;
    let count = 0;
    let totalPrice = 0;
    let cashPledge = 0;
    serviceTreeData.forEach((item:any) => {
      count += item.checked ? item.count : 0;
      totalPrice += item.totalPrice;
      cashPledge += item.cashPledge;
    });
    this.setState({
      count,
      totalPrice: totalPrice / 100 || 0,
      cashPledge: cashPledge / 100 || 0,
      priceNoPledge: Number((totalPrice / 100 - cashPledge / 100).toFixed(2)),
    });

  }

  //获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }
  //确认下单
  confirmOrder () {
    //有限判断是否有登录
    if (!authHelper.checkAuth()) return;
    if(this.state.payLoading){
      return
    }
    //数据序列化处理
    //判断时候是详情页立即下单 直接进入的
    let reservationItemList:any = [];
    if (this.state.detail) {
      reservationItemList = [this.state.detail];
    } else {
      reservationItemList = wxApi.getStorageSync('sceneCartList');
      reservationItemList = reservationItemList.map((item) => {
        return {
          ...JSON.parse(item),
          isOpen: false,
        };
      });
    }
    let data:any = [];
    reservationItemList.forEach((current) => {
      data.push({
        reservationId: current.id,
        reservationSpecId: current.specInfo.id,
        startTime: current.reserveStartTime,
        endTime: current.reserveEndTime,
        formExt: JSON.stringify(current.formExt),
      });
    });

    let param = {
      reservationItemList: data,
      payTradeType:payUtils.returnPayTradeType(),
      orderMessage: this.state.orderMessage,
      orderRequestPromDTO: {
        couponNo: this.state.curCoupon.code || '',
      },
    };
    // this.state.payLoading = true
    this.setState({
      payLoading:true
    })
      wxApi
      .request({
        url: api.scence.ordercreateReservationOrder,
        method: 'post',
        loading: true,
        data: param,
      })
      .then((res) => {
        let payInfo = res.data;

        pay.payByOrder(
          payInfo.orderNo,
          null,
          {
            wxPaySuccess (res) {
            },

            wxPayFail(err){
              wxApi.$navigateTo({
                url: '/sub-packages/marketing-package/pages/living-service/order-list/index',

                data: {
                  orderNo: payInfo.orderNo,
                },
              });
            },

            success(res){
              wxApi.removeStorageSync('sceneCartList');
              wxApi.removeStorageSync('sceneCartSelectList');
              Taro.redirectTo({
                url: '/sub-packages/marketing-package/pages/living-service/order-list/index',
                data: {
                  orderNo: payInfo.orderNo,
                },
              });
            },

            cancel(){}
          }
        )

      }).catch((error) => {
        console.log(error)
      }).finally((res) => {
        this.setState({
          payLoading:false
        })
      });
  }

  bindBlur=(e)=>{
    this.setState({
      orderMessage: e.detail.value,
    });
  }
  handleRedPackeList=()=>{

  }
  handleGoToGiftCardList=()=>{

  }
  handleGoToCard=()=>{

  }
  render() {
    const {
      payLoading,
      goodsList,
      cashPledge,
      priceNoPledge,
      curCoupon,
      couponEnum,
      price,
      coupons,
      payDetails,
      curGiftCard,
      seckillNo,
      useLogisticsCoupon,
      choosedDelivery,
      deliveryWay,
      serveCard,
      curRedPacke,
      scoreName,
      goodsIntegral,
      goodsInternal,
      isCardpackType,
      goodsInfoList,
      goodsTotalPrice,
      isEstateType,
      allFreightCollect,
      isGiftActivity,
      isFrontMoneyItem,
      tmpStyle,
      isCoupon,
    } = this.state;

    return (
      <View data-scoped="wk-ssps-SceneConfirmOrder" className="wk-ssps-SceneConfirmOrder">
        {/* 购买的服务事项 */}
        <View className="server-info-wrap">
          <View className="wrap">
            <List list={goodsList}></List>
          </View>
          <View className="price-info">
            <View className="view">
              <Text>应缴押金</Text>
              <Text>{'￥' + cashPledge}</Text>
            </View>
            <View className="view">
              <Text>应收费用</Text>
              <Text>{'￥' + priceNoPledge}</Text>
            </View>
          </View>
          <View className="remark">
            <View className="view">备注</View>
            <Textarea className="textarea" placeholder="请输入..." onInput={this.bindBlur}></Textarea>
          </View>
          <View className="row-box" onClick={this.handleGoToCouponList}>
            <View className="row-label">优惠劵</View>
            {curCoupon && curCoupon.name ? (
              <View className="right-text">
                {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                  <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                )}

                {'减￥' + filters.moneyFilter(!!price && price.couponPrice, true)}
              </View>
            ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
              <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
            ) : (
              <View className="right-text">无可用</View>
            )}

            <Image
              className="right-icon"
              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
            ></Image>
          </View>
          {/*  优惠信息  */}
          <View className="shipping-method">
            {!!price && price?.fullDecrementPromFee && (
              <View className="row-box">
                <View className="row-label">满减</View>
                <View className="right-text">{'减￥' + filters.moneyFilter(!!price && price.fullDecrementPromFee, true)}</View>
              </View>
            )}

            {payDetails &&
              payDetails.map((item, index) => {
                return (
                  <Block key={item.index}>
                    {item.isCustom ? (
                      <View className="row-box">
                        <View className="row-label">{item.label}</View>
                        <View className="right-text">{item.value}</View>
                      </View>
                    ) : (
                      <Block>
                        {item.name === 'gift-card' && (
                          <View className="row-box" onClick={this.handleGoToGiftCardList}>
                            <View className="row-label">礼品卡</View>
                            {curGiftCard && curGiftCard.cardItemName ? (
                              <View className="right-text">
                                {'抵扣￥' + filters.moneyFilter(!!price && price.giftCardPrice, true)}
                              </View>
                            ) : coupons && coupons.userGiftCardDTOSet && coupons.userGiftCardDTOSet.length ? (
                              <View className="right-text">{'可用' + coupons.userGiftCardDTOSet.length + '张'}</View>
                            ) : (
                              <View className="right-text">无可用</View>
                            )}

                            <Image
                              className="right-icon"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                            ></Image>
                          </View>
                        )}

                        {item.name === 'coupon' && !seckillNo && (
                          <View className="row-box" onClick={this.handleGoToCouponList}>
                            <View className="row-label">优惠劵</View>
                            {curCoupon && curCoupon.name ? (
                              <View className="right-text">
                                {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                  <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                )}

                                {'减￥' + filters.moneyFilter(!!price && price.couponPrice, true)}
                              </View>
                            ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                              <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                            ) : (
                              <View className="right-text">无可用</View>
                            )}

                            <Image
                              className="right-icon"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                            ></Image>
                          </View>
                        )}

                        {item.name === 'coupon' &&
                          useLogisticsCoupon == 1 &&
                          seckillNo &&
                          choosedDelivery !== deliveryWay.SELF_DELIVERY.value && (
                            <View className="row-box" onClick={this.handleGoToCouponList}>
                              <View className="row-label">运费券</View>
                              {curCoupon && curCoupon.name ? (
                                <View className="right-text">
                                  {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                    <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                  )}

                                  {'减￥' + filters.moneyFilter(!!price && price.couponPrice, true)}
                                </View>
                              ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                                <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                              ) : (
                                <View className="right-text">无可用</View>
                              )}

                              <Image
                                className="right-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                              ></Image>
                            </View>
                          )}

                        {item.name === 'card' && (
                          <View className="row-box" onClick={this.handleGoToCard}>
                            <View className="row-label">次数卡</View>
                            <View className="right-text">{serveCard.cardItemName}</View>
                            <Image
                              className="right-icon"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                            ></Image>
                          </View>
                        )}

                        {item.name === 'redpacke' && (
                          <View className="row-box" onClick={this.handleRedPackeList}>
                            <View className="row-label">红包</View>
                            {curRedPacke && curRedPacke.code ? (
                              <View className="right-text">
                                {'减￥' + filters.moneyFilter(!!price && price.redPacketPrice, true)}
                              </View>
                            ) : coupons && coupons.redPacketSet && coupons.redPacketSet.length ? (
                              <View className="right-text">{'可用' + coupons.redPacketSet.length + '个'}</View>
                            ) : (
                              <View className="right-text">无可用</View>
                            )}

                            <Image
                              className="right-icon"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                            ></Image>
                          </View>
                        )}

                        {item.name === 'discount' && (
                          <View className="row-box">
                            <View className="row-label">会员折扣</View>
                            <View className="right-text">
                              {(coupons.memberDiscount && coupons.memberDiscount < 100
                                ? coupons.memberDiscount / 10 + '折'
                                : '无') +
                                '\r\n            减￥' +
                                filters.moneyFilter(!!price && price.memberPrice, true)}
                            </View>
                          </View>
                        )}

                        {item.name === 'integral' && (
                          <View className="row-box">
                            <View className="row-label">{scoreName + '抵扣'}</View>
                            <View className="right-text">
                              {goodsIntegral + scoreName + '减￥' + filters.moneyFilter(!!price && price.integralPromFee, true)}
                            </View>
                          </View>
                        )}

                        {item.name === 'innerBuy' && (
                          <View className="row-box">
                            <View className="row-label">内购券</View>
                            <View className="right-text">
                              {goodsInternal + '张减￥' + filters.moneyFilter(!!price && price.innerBuyPromFee, true)}
                            </View>
                          </View>
                        )}

                        {item.name === 'seckill' && (
                          <View className="row-box">
                            <View className="row-label">秒杀优惠</View>
                            <View className="right-text">{'减￥' + filters.moneyFilter(!!price && price.secKillPromFee, true)}</View>
                          </View>
                        )}
                      </Block>
                    )}
                  </Block>
                );
              })}
            {price?.groupPromFee && (
              <View className="row-box">
                <View className="row-label">拼团优惠</View>
                <View className="right-text">{'-¥ ' + filters.moneyFilter(!!price && price.groupPromFee, true)}</View>
              </View>
            )}

            {!!isCardpackType && (
              <Block>
                <View className="row-box">
                  <View className="row-label">卡包明细</View>
                </View>
                {goodsInfoList[0].wxItem.couponInfos &&
                  goodsInfoList[0].wxItem.couponInfos.map((item, index) => {
                    return (
                      <Block key={item.index}>
                        <View className="row-box card-pack-box">
                          <View className="label">{item.name}</View>
                          <View className="right-text">{(item.couponNum || 0) + '张'}</View>
                        </View>
                      </Block>
                    );
                  })}
              </Block>
            )}

            <Block>
              {isEstateType ? (
                <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                  <View className="row-label">认筹金</View>
                  <View className="right-text">
                    {'¥ ' + filters.moneyFilter(!!price && price.totalPrice || goodsTotalPrice, true)}
                  </View>
                </View>
              ) : (
                <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                  <View className="row-label">商品原价</View>
                  {/*  <View wx:if="{{goodsInfoList[0].seckillPrice > (price.totalPrice||goodsTotalPrice)}}" class="right-text">¥ {{filters.moneyFilter(goodsInfoList[0].seckillPrice,true)}}</View>  */}
                  <View className="right-text">
                    {'¥ ' + filters.moneyFilter(!!price && price?.totalPrice || goodsTotalPrice, true)}
                  </View>
                </View>
              )}
            </Block>

                {payDetails &&
                  payDetails.map((item, index) => {
                    return (
                      <Block key={item.index}>
                        {
                          item.name === 'freight' && choosedDelivery === deliveryWay.EXPRESS.value && (
                            <View className="row-box" >
                          <View className="row-label">运费</View>
                          {!!price && price.freight && price.freight > 0 ? (
                            <View className="right-text">{'￥' + filters.moneyFilter(!!price && price.freight, true)}</View>
                          ) : (
                            <View className="right-text">{allFreightCollect ? '到付' : '包邮'}</View>
                          )}
                          </View>
                          )
                        }
                      </Block>


                    )
                  })
            }

            {choosedDelivery === deliveryWay.CITY_DELIVERY.value && (
              <View className="row-box">
                <View className="row-label">配送费</View>
                {!!price && price.freight && price.freight > 0 ? (
                  <View className="right-text">{'￥' + filters.moneyFilter(!!price && price.freight, true)}</View>
                ) : (
                  <View className="right-text">免配送费</View>
                )}
              </View>
            )}

            <View className="row-box">
              <View className="row-label">共优惠</View>
              <View className="right-text">
                {'- ¥ ' +
                  (isGiftActivity
                    ? filters.moneyFilter(!!price && price.totalPrice || goodsTotalPrice, true)
                    : filters.moneyFilter(!!price && price.totalFreePrice || 0, true) < 0
                    ? 0
                    : filters.moneyFilter(!!price && price.totalFreePrice || 0, true))}
              </View>
            </View>
            {price && isFrontMoneyItem ? (
              <View className="row-box">
                <View className="row-label">尾款</View>
                <View className="right-text">{'¥ ' + filters.moneyFilter(!!price && price.restMoney, true)}</View>
              </View>
            ):''}
          </View>
          {/* <View className="place-holder"></View> */}
        </View>
        <View className="bottom-btn-wrap">
          <View className="bottom">
            <Text>{'合计：￥' + filters.moneyFilter(!!price && price.needPayPrice, true)}</Text>
            <View
              className="bottom-btn"
              onClick={this.confirmOrder.bind(this)}
              style={_safe_style_('background-color: ' + tmpStyle.btnColor)}
            >
              确定提交
            </View>
          </View>
        </View>

        {/*  选择优惠对话框  */}
        <BottomDialog ref={this.chooseCardRef} id="choose-card" customClass="bottom-dialog-custom-class">
          {isCoupon === 4 && (
            <View className="coupon-select">
              {coupons.userGiftCardDTOSet &&
                coupons.userGiftCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={()=>this.onSelectCard(item)}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name limit-line">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (!!item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用礼品卡
              </View>
            </View>
          )}

          {/*  优惠券优惠类型  */}
          {isCoupon === 1 && (
            <View className="coupon-select">
              {coupons.couponSet &&
                coupons.couponSet.map((item) => {
                  return (
                    <View
                      className={'coupon-tab couponSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={()=>this.onSelectCard(item)}
                    >
                      <View className="coupon-img-box">
                        {!item.status ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gray.png"
                          ></Image>
                        ) : item.couponCategory === 1 ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-blue.png"
                          ></Image>
                        ) : (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gren.png"
                          ></Image>
                        )}
                      </View>
                      <View className="coupon-tab-left">
                        {item.discountFee === 0 ? (
                          <View className="coupon-freeFreight">免运费</View>
                        ) : (
                          <View className="coupon-price">
                            {(item.couponCategory === couponEnum.TYPE.freight.value ||
                              item.couponCategory === couponEnum.TYPE.fullReduced.value) && (
                              <Block>
                                <Text className="price-logo">￥</Text>
                                <Text className="price-num">{filters.moneyFilter(item.discountFee, true)}</Text>
                              </Block>
                            )}

                            {item.couponCategory === couponEnum.TYPE.discount.value && (
                              <Block>
                                <Text className="price-num">{filters.discountFilter(item.discountFee, true)}</Text>
                                <Text className="price-logo">折</Text>
                              </Block>
                            )}
                          </View>
                        )}

                        {item.minimumFee === 0 ? (
                          <View className="coupon-minimumFee">无门槛</View>
                        ) : (
                          <View className="coupon-minimumFee">
                            {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                          </View>
                        )}

                        {item.couponCategory === 1 ? (
                          <View className="coupon-type">运费券</View>
                        ) : (
                          <View className="coupon-type">满减券</View>
                        )}
                      </View>
                      <View className="coupon-tab-right">
                        <View className="coupon-name limit-line">{item.name}</View>
                        {item.endTime && (
                          <View className="coupon-validityDate">{'有效期：' + filters.dateFormat(item.endTime)}</View>
                        )}
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}

          {/*  红包优惠类型  */}
          {isCoupon === 2 && (
            <View className="coupon-select">
              {coupons.redPacketSet &&
                coupons.redPacketSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab redPacketSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={()=>this.onSelectCard(item)}
                    >
                      <View className="coupon-tab-top">
                        {item.thresholdFee === 0 ? (
                          <View className="coupon-name limit-line">无门槛</View>
                        ) : (
                          <View className="coupon-name limit-line">
                            {'满' + filters.moneyFilter(item.thresholdFee, true) + '可用'}
                          </View>
                        )}

                        {item.endTime && (
                          <View className="coupon-validityDate">
                            {'有效期：' + filters.dateFormat(item.endTime, 'yyyy-MM-dd hh:mm:ss')}
                          </View>
                        )}
                      </View>
                      <View className="coupon-tab-bottom">
                        金额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.luckMoneyFee, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用红包
              </View>
            </View>
          )}

          {/*  卡项优惠类型  */}
          {isCoupon === 3 && (
            <View className="coupon-select">
              {coupons.userCardDTOSet &&
                coupons.userCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={()=>this.onSelectCard(item)}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (!!item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}
        </BottomDialog>
      </View>
    );
  }
}


export default SceneConfirmOrder;
