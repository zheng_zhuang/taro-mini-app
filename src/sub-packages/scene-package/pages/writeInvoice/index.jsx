import React from 'react'
import {$getRouter, _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import Taro from '@tarojs/taro';
import './index.scss'
import UploadImg from "@/wxat-common/components/upload-img";
import wxApi from '@/wxat-common/utils/wxApi';
import {View, Text,Form,Input,Image,Block,Textarea } from '@tarojs/components';

const forEachObj = ['name','companyAddress','phone','companyBank','bankAccount','ein','mailingAddress','email','contactNumber','contactPerson']
class WriteInvoice extends React.Component{
  textareaRef = Taro.createSelectorQuery().in(this);
  constructor(props){
    super(props)
    this.state={
      tmpStyle:'',
      type:1,
      billType:1,
      name:'',  //发票抬头
      invoiceSwitch:null,
      companyAddress:'',
      phone:'',
      companyBank:'',
      ein:'',
      selectSpecialTab:true,
      invoiceSwitch:null,
      invoiceCheckList:[],
      receiveValue:null,
      bankAccount:null,
      receiveTypeIndex:0,
      mailingAddress:'',
      contactNumber:'',
      roomNo:'',
      contactPerson:'',
      email:'',
      receiveType:'',
      picture:[],
      success: false,
    }
  }

  componentDidMount(){
    const params = $getRouter().params
    const invoiceCheckList = this.sortInvoiceCheck(JSON.parse(params.invoiceCheckList))
    let options1 = {
      invoiceCheckList:invoiceCheckList,
      invoiceSwitch: params.invoiceSwitch == 'false' ? false : true
    }
    this.getReceiveType(options1.invoiceCheckList[0])
    this.setState({
      invoiceSwitch: options1.invoiceSwitch,
      invoiceCheckList: options1.invoiceCheckList
    })
    this.getTemplateStyle()
  }
  //获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }
   /**
   * 隐藏dialog
   */
    _cancelEvent = () => {
      this.setState({
        success: false,
      });
    }
  //返回首页
  goHome = () => {
    this.setState({
      success: false,
    });
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };
  //提交事件
  submitForm=()=>{
    const error = this.checkForm(this.state);
    if (typeof error === 'string') {
      wxApi.showToast({icon: 'none', title: error});
      return;
    }
    const params = {
      type: this.state.type,
      receiveType: this.state.receiveType,
      roomNo: this.state.roomNo,
      picture: this.state.picture.join(',')
    }
    if(this.state.type === 1){
      params.billType = this.state.billType
    }
    for(const key in error){
      params[key] = error[key].value
    }
    wxApi
    .request({
      url: api.invoice.insertInvoice,
      data:params,
      loading:true,
      method:'POST'
    })
    .then(res => {
      this.setState({success:true})
    }).catch(error => {
    })
  }
  checkForm=(data)=>{
    const parmas = this.integrationData(data)
    for(let i=0;i<forEachObj.length;i++){
      const key = forEachObj[i]
      if(parmas[key]){
        if(parmas[key].require){
          if(parmas[key].validator === '' && !parmas[key].value){
            return parmas[key].msg
          }else if(parmas[key].validator){
            if(parmas[key].validator1){
              if(!parmas[key].validator.test(parmas[key].value) && !parmas[key].validator1.test(parmas[key].value)){
                return parmas[key].msg
              }
            }else{
              if(!parmas[key].validator.test(parmas[key].value)){
                return parmas[key].msg
              }
            }
          }
        }else{
          //不必填,但存在验证格式
          if(parmas[key].value && parmas[key].validator){
            if(parmas[key].validator1){
              if(!parmas[key].validator.test(parmas[key].value) && !parmas[key].validator1.test(parmas[key].value)){
                return parmas[key].msg
              }
            }else{
              if(!parmas[key].validator.test(parmas[key].value)){
                return parmas[key].msg
              }
            }
          }
        }
      }
    }
    return parmas
  }
  //数据整合
  integrationData = (data) =>{
    const params = {
      name:{
        value:data.name,
        require:true,
        validator:'',
        msg:'请输入企业抬头'
      },
      companyAddress:{
        value:data.companyAddress,
        require:data.type === 1 && data.billType === 2 ? true:false,
        validator:'',
        msg:'请输入公司地址'
      },
      phone:{
        value:data.phone ? data.phone.trim() : '',
        require:false,
        validator:/^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
        validator1:/^(?:(?:\d{3}-)?\d{5,8}|^(?:\d{4}-)?\d{5,8})?$/,
        msg:'无效的公司电话'
      },
      companyBank:{
        value:data.companyBank,
        require:data.type === 1 && data.billType === 2 ? true:false,
        validator:'',
        msg:'请输入开户银行'
      },
      bankAccount:{
        value:data.bankAccount ? data.bankAccount.trim() : '',
        require:data.type === 1 && data.billType === 2 ? true:false,
        validator:/^\d+$|^\d+[.]?\d+$/,
        msg:'银行卡账号格式不正确'
      },
      contactNumber:{
        value:data.contactNumber ? data.contactNumber.trim() : '',
        require:data.receiveType !== 2 ? true:false,
        validator:/^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
        msg:'无效的联系电话'
      },
      contactPerson:{
        value:data.contactPerson,
        require:data.receiveType !== 2 ? true:false,
        validator:'',
        msg:'请输入联系人员'
      },
    }
    if(data.type === 1){
      params.ein = {
        value:data.ein,
        require:true,
        validator:'',
        msg:'请输入纳税人识别号'
      }
    }
    if(data.receiveType !== 1){
      params.mailingAddress={
        value:data.mailingAddress,
        require:data.receiveType == 2 ? false:true,
        validator:'',
        msg:'请输入邮寄地址'
      }
    }else{
      params.email = {
        value:data.email ? data.email.trim() : '',
        require:true,
        validator:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
        msg:'邮箱格式不正确'
      }
    }
    return params
  }
  tabSelectTap=(index)=>{
    this.setState({
      type:index,
    })
  }
  sortInvoiceCheck=(list)=>{
    const params = list.map((item)=>{
      let value = 1
      switch (item) {
        case "":
          value = 1
          break;
        case "纸质发票店取":
          value = 2
          break;
        case "纸质发票邮寄":
          value = 3
          break;
      }
      return {
        label:item,
        value:value
      }
    })
    return params.sort((x,y)=>{
      return x.value - y.value
    }).map((item)=>{
      return item.label
    })
  }
  inputValue=(e,type)=>{
    const value = e.detail.value;
    this.setState({
      [type]: value
    })
    if(type == 'name' || type == 'mailingAddress'){
      const textarea = type == 'name' ? document.querySelector('.name-input') : document.querySelector('.item-input-address-mailing')
      textarea.style.height = '20px'
      textarea.style.height = textarea.scrollHeight + 'px'
    }
  }
  getWXinvoice=()=>{
    wx.invoke(
      'chooseInvoiceTitle',
      {'scene': 1 },
      (res = {}) => {
        const data = JSON.parse(res.choose_invoice_title_info)
        this.setState({
          name:data.title,
          companyAddress:data.companyAddress,
          phone:data.telephone,
          companyBank:data.bankName,
          bankAccount:data.bankAccount.replace(/\s*/g,""),
          ein:data.taxNumber
        })
      }
    )
  }
  selectSpecialTab=()=>{
    this.setState({
      selectSpecialTab: !this.state.selectSpecialTab
    })
  }
  specialInvoicTap=(index)=>{
    this.setState({
      billType: index,
    })
  }
  //获取发票获取方式
  getReceiveType=(value)=>{
    let receiveValue;
    switch (value) {
      case "电子发票":
        receiveValue = 1
        break;
      case "纸质发票店取":
        receiveValue = 2
        break;
      case "纸质发票邮寄":
        receiveValue = 3
        break;
    }
    this.setState({
      receiveType:receiveValue
    })
  }
  billTypeTap = (item,index) =>{
    this.getReceiveType(item)
    this.setState({
      receiveTypeIndex: index
    });
  }
  handleImagesChange = (images) =>{
    this.setState({
      picture:images
    })
  }
  render(){
    const {tmpStyle,type,name,invoiceSwitch,companyAddress,phone,companyBank,bankAccount,ein,selectSpecialTab,billType,mailingAddress,contactNumber,roomNo,contactPerson, invoiceCheckList,receiveTypeIndex,email,receiveType,picture,success } = this.state

    return (
      <View className="write-inovice">
        <View className="invoice">
          <View className="invoiceTabBox">
            <View className="invoiceTab">
              <View className="tab-item">
                <View className={type==1 ? 'active tabItemText' : 'tabItemText'} onClick={()=>this.tabSelectTap(1)}
                  style={_safe_style_('border-bottom-color:' + tmpStyle.btnColor + ';color:' + (type==1 ? tmpStyle.btnColor : ''))}>
                    企业发票
                </View>
              </View>
              <View className="tab-item">
                <View className={type==2 ? 'active tabItemText' : 'tabItemText'} onClick={()=>this.tabSelectTap(2)}
                  style={_safe_style_('border-bottom-color:' + tmpStyle.btnColor + ';color:' + (type==2 ? tmpStyle.btnColor : ''))}>
                    个人/非企业单位
                </View>
              </View>
            </View>
          </View>
          <Form>
            <View className="invoiceform">
              <View className="invoiceformItem">
                <View className="form-item-rise required">
                  <View className="item-label">
                    {type == 1 ? '企业抬头' : '抬头名称'}
                  </View>
                  <Textarea autoHeight className="name-input" value={name} onInput={(e)=>this.inputValue(e,'name')} placeholder={type == 1 ? '请填写企业抬头':'请填写抬头名称'}></Textarea>
                  {
                    type == 1 ? <View onClick={this.getWXinvoice} className="wxIinvoice" style={_safe_style_('color:' + tmpStyle.btnColor)}>获取发票抬头</View> : ''
                  }
                </View>
                <View className="form-item">
                  <View className="item-label redMore">
                    更多发票
                  </View>
                  <View className={selectSpecialTab ? 'down selectDown':'up selectDown'} onClick={this.selectSpecialTab}>
                    <Image className="selectDown image" style={_safe_style_('display:block;width:100%')}
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/images/down-angle.png" mode='widthFix'>
                    </Image>
                  </View>
                  {
                    type == 1 ? <View className="specialInvoice">
                    <View className="special-item" onClick={()=>this.specialInvoicTap(1)}
                      style={_safe_style_('background:' + (billType == 1 ? tmpStyle.btnColor : '') + ';color:' + (billType == 1 ? '#ffffff':''))}>
                      <View className="specialText">普票</View>
                    </View>
                    { invoiceSwitch ?
                      <View className="special-item" onClick={()=>this.specialInvoicTap(2)}
                      style={_safe_style_('background:' + (billType==2 ? tmpStyle.btnColor:'') + ';color:' + (billType==2 ? '#ffffff':''))}>
                      <View className="specialText">专票</View>
                    </View> : '' }
                  </View> : ''
                  }
                </View>
                {
                  selectSpecialTab ?
                  <View className={ billType==2 ? 'required specialInvoiceContent':'specialInvoiceContent'}>
                    <View className="form-item">
                      <View className="item-label">
                        公司地址
                      </View>
                      <Input className="item-input" value={companyAddress} data-name="companyAddress" onInput={(e)=>this.inputValue(e,'companyAddress')}
                        placeholder="请输入公司地址">
                      </Input>
                    </View>
                    <View className="form-item">
                      <View className="item-label-phone">
                        公司电话
                      </View>
                      <Input className="item-input" value={phone} data-name="phone" onInput={(e)=>this.inputValue(e,'phone')}
                        placeholder="请输入公司电话">
                      </Input>
                    </View>
                    <View className="form-item">
                      <View className="item-label">
                        开户银行
                      </View>
                      <Input className="item-input" value={companyBank} data-name="companyBank"  onInput={(e)=>this.inputValue(e,'companyBank')}
                        placeholder="请输入开户银行">
                      </Input>
                    </View>
                    <View className="form-item">
                      <View className="item-label">
                        银行账号
                      </View>
                      <Input className="item-input" type="number" value={bankAccount}  data-name="bankAccount" onInput={(e)=>this.inputValue(e,'bankAccount')}
                        placeholder="请输入银行账号">
                      </Input>
                    </View>
                    {
                      type == 1 ?
                      <View className="form-item required">
                        <View className="item-label-ein">
                          <View className="ein">
                            纳税人
                          </View>
                          <View>
                            识别号
                          </View>
                        </View>
                        <Input className="item-input" value={ein}  data-name="ein" onInput={(e)=>this.inputValue(e,'ein')}
                          placeholder="请填写纳税人识别号">
                        </Input>
                      </View>
                       : ''
                    }
                  </View> : ''
                }

              </View>
              <View className="form-item" style={_safe_style_('background:#f5f5f5;padding:0 20px;')}>
                <View className="item-label redMore">
                  配送信息
                </View>
                <View className="deliveryInformation">
                  {
                    invoiceCheckList.map((item,index)=>{
                      return(
                        <Block key={index}>
                          <View className="deliveryInformation-item" onClick={()=>this.billTypeTap(item,index)}
                              style={_safe_style_('background:' + (receiveTypeIndex == index ? tmpStyle.btnColor:'') + ';color:'+ (receiveTypeIndex==index ? '#ffffff':''))}>
                              <View className="specialText">{item}</View>
                          </View>
                        </Block>
                      )
                    })
                  }
                </View>
              </View>
              <View className="invoiceformItem">
                <View className={receiveType != 2 ? 'deliveryInformationContent required' : 'deliveryInformationContent'}>
                  {
                    receiveType == 1 ?
                    <View className="form-item">
                      <View className="item-label">
                        邮箱地址
                      </View>
                      <Input className="item-input" value={email} data-name="email" onInput={(e)=>this.inputValue(e,'email')}
                        placeholder="请输入邮箱地址">
                      </Input>
                    </View> :
                    <View className="address-mailing">
                      <View className="item-label">
                        邮寄地址
                      </View>
                      <Textarea autoHeight className="item-input-address-mailing" value={mailingAddress} data-name="mailingAddress" onInput={(e)=>this.inputValue(e,'mailingAddress')}
                        placeholder="请输入邮寄地址">
                      </Textarea>
                    </View>
                  }
                  <View className="form-item">
                    <View className="item-label">
                      联系电话
                    </View>
                    <Input className="item-input" value={contactNumber} data-name="contactNumber" onInput={(e)=>this.inputValue(e,'contactNumber')}
                      placeholder="请输入联系电话">
                    </Input>
                  </View>
                  <View className="form-item">
                    <View className="item-label">
                      联系人员
                    </View>
                    <Input className="item-input" value={contactPerson} data-name="contactPerson"  onInput={(e)=>this.inputValue(e,'contactPerson')}
                      placeholder="请输入联系人员姓名">
                    </Input>
                  </View>
                  <View className="form-item-tips">
                    <View>注意事项：</View>
                    <View>1.请仔细核对您填写的发票信息，以免再次修改。</View>
                    <View>
                      <View>2.纸质发票,快递到付，费用有客户自行解决。</View>
                    </View>
                  </View>
                </View>
              </View>
              <View className="form-item" style={_safe_style_('background:#f5f5f5;padding:0 20px;')}>
                <View className="item-label redMore">
                  备注信息
                </View>
              </View>
              <View className="invoiceformItem">
                 <View className="specialInvoiceContent">
                  <View className="form-item">
                    <View className="item-label">
                      房间号
                    </View>
                    <Input className="item-input" value={roomNo} data-name="roomNo" onInput={(e)=>this.inputValue(e,'roomNo')}
                      placeholder="请输入房间号">
                    </Input>
                  </View>
                  <View className="item-form">
                    <View className="form-item-ticket-label">
                      <View>小票消息</View>
                      <View  className="uploadImage-box">
                        <View className="img-box">
                        <UploadImg value={picture} onChange={this.handleImagesChange}  max={9}></UploadImg>
                        </View>
                      </View>
                    </View>

                  </View>
                </View>
              </View>
            </View>
          </Form>
        </View>
        <View className="formBtn" onClick={this.submitForm} style={_safe_style_('background-color: ' + tmpStyle.btnColor)}>
          提交
        </View>
        {success && (
          <View className="successBox">
          <View className='wx-mask' onClick={this.cancelEven}></View>
          <View className="successBoxContent">
            <View>
              <View className="successTitle">提交成功</View>
              <View className="successText">
              <Image class="successImg" mode="widthFix" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/success.svg"></Image>
              </View>
           </View>
           <View className="successbtn" onClick={this.goHome} style={_safe_style_('background-color: ' + tmpStyle.btnColor)}>
                确定
           </View>
          </View>
        </View>
        )}
      </View>

    )
  }
}
export default WriteInvoice
