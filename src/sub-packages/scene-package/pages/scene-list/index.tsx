import {$getRouter, _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import {connect} from 'react-redux'
import hoc from '@/hoc/index'
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import Empty from '@/wxat-common/components/empty/empty';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import ReserveDialog from '@/sub-packages/scene-package/components/reserve-dialog/index';
import './index.scss';
import template from '@/wxat-common/utils/template.js';
import util from "@/wxat-common/utils/util.js";
interface IState{
  tmpStyle:any;
  firstNavList:any[];
  categoryLevel2:any[];
  navigationSelect:number;
  seccurrentIndex:number;
  sceneList:any[];
  cartNumber:number;
  selectDetail:any;
  showInDialogSign:boolean;
  serviceCateGoryIdList:string;
  parentIdList:string;
  serviceCateGoryId:number | string;
  pageNo:number;
  secondid:string | number | null;
  specVal:any;
  loadMoreStatus:any;
  hasMore:any
}
interface IProps{
  currentStore:any
}

const mapStateToProps = (state) => {
  return {currentStore:state.base.currentStore};
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class SceneList extends React.Component<IProps,IState> {
  state = {
    tmpStyle:{},
    firstNavList: [], // 一级分类
    categoryLevel2: [], // 二级分类
    navigationSelect: 0, // 一级导航选中下标
    seccurrentIndex: 0, // 二级导航选中下标
    sceneList: [],
    cartNumber: 0, // 购物车数量
    selectDetail: {},
    showInDialogSign: false,
    serviceCateGoryIdList: '',
    parentIdList:'',
    serviceCateGoryId:'',
    pageNo:1,
    secondid:null,
    specVal:null,
    loadMoreStatus:null,
    hasMore:null
  }
  $router = $getRouter();
  animatDialogRef = React.createRef<any>()
  componentDidShow (){
    this.calcCount()
  }
  componentDidMount(){
    this.getTemplateStyle();
    this.getPcProductList();
  }
  //获取pc配置的分类id
  getPcProductList = () => {
    // let optionConfig = wxApi.getCurrentPages().slice(-1)[0].options;
    let optionConfig = this.$router.params
    let serviceCateGoryIdList = !!decodeURIComponent(optionConfig.id)
      ? JSON.parse(decodeURIComponent(optionConfig.id))
      : [];
      console.log('optionConfig',serviceCateGoryIdList)
    if (serviceCateGoryIdList.length > 0) {
      const idsList:any[] = [];
      const parentIdList:any[] = [];
      let resList = serviceCateGoryIdList.filter((num) => {
        return parseInt(num.parentId) > 0;
      });
      serviceCateGoryIdList.forEach((item) => {
        parentIdList.push(item.id);
        idsList.push(item.id);
        if (resList && resList.length != serviceCateGoryIdList.length) {
          if (item.parentId != 0) {
            idsList.push(item.parentId);
          }
        }
      });
      let newidsList = idsList.join(',');
      let newparentIdList = parentIdList.join(',');

      this.setState({
        serviceCateGoryIdList: newidsList,
        parentIdList: newparentIdList,
      },()=>{
        this.getFirstNav();
      });
    } else {
      this.setState({
        serviceCateGoryIdList: '',
        parentIdList: '',
      },()=>{
        this.getFirstNav();
      });
    }
  }
  selectSidebar =(index,servicecategoryid)=> {
    // 切换一级导航
    const serviceCateGoryId = parseInt(servicecategoryid);
    if (this.state.navigationSelect === index) return;
    this.setState(
      {
        navigationSelect: index,
        seccurrentIndex: 0,
        serviceCateGoryId,
      },
      () => {
        this.getSecondNav();
      }
    );
  }
  selectSecondSidebar=(secondId,index)=>{
    // 切换二级导航
    if (this.state.seccurrentIndex === index) return;
    this.setState({
      seccurrentIndex: index,
      pageNo: 1,
    });
    this.queryItemSku(secondId);
  }
  getFirstNav=()=>{
    // 获取一级导航
    wxApi
      .request({
        url: api.livingService.getNewFir,
        method: 'GET',
        loading: true,
        data: {
          serviceCateGoryIdList: this.state.serviceCateGoryIdList,
          categoryType: 4,
        },
      })
      .then((res) => {
        let firstNavList = res.data || [];
        if (firstNavList.length == 0) return;
        let serviceCateGoryId = res.data[0].id;
        this.setState({
          firstNavList,
          serviceCateGoryId,
        });

        this.getSecondNav();
      });
  }
  getSecondNav=()=>{
    // 获取二级导航
    wxApi
      .request({
        url: api.livingService.getNewSec,
        method: 'GET',
        loading: true,
        data: {
          categoryType: 4,
          parentId: this.state.serviceCateGoryId,
          storeId: this.props.currentStore.id,
        },
      })
      .then((res) => {
        let categoryLevel2 = res.data || [];
        if (categoryLevel2.length && categoryLevel2) {
          let secondid = categoryLevel2[0].id;
          this.queryItemSku(res.data[0].id);
          this.setState({
            categoryLevel2: this.flatterArray(categoryLevel2),
            secondid,
          });
        } else {
          this.setState({
            categoryLevel2,
          });
          // 没有子分类  展示商品数据
          this.queryItemSku();
        }
      });
  }
  flatterArray=(arr)=>{
    let x:any[] = [];
    arr.forEach((item) => {
      if (item.childrenCategory && item.childrenCategory.length) {
        x = x.concat(this.flatterArray(item.childrenCategory));
      } else {
        x.push(item);
      }
    });
    return x;
  }
  queryItemSku=(id?)=>{
    const categoryId = id || this.state.firstNavList[this.state.navigationSelect].id;
    const params = {
      platformServiceCategoryId: categoryId,
      categoryType: 4,
    };

    wxApi
      .request({
        url: api.scence.getSceneList,
        loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true) {
          let hasMore = true;
          let pageNo = this.state.pageNo;
          let resData = res.data || [];
          resData.forEach((item) => {
            // 标签最多只显示3个
            item.labelArr = JSON.parse(item.lable).splice(0, 2);
            item.vitrualQty = Number(item.vitrualQty);
            item.sales = Number(item.sales);
          });
          console.log('sceneList',resData)
          this.setState({
            sceneList: resData,
            hasMore,
            pageNo,
          });
        }
      })
      .catch((error) => {
        this.setState({
          sceneList: [],
        });

        if (this.isLoadMoreRequest()) {
          this.setState({
            // loadMoreStatus: loadMoreStatus.ERROR || '',
          });
        }
      });
  }
  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }
  showDialog=(detail,e)=>{
    e.stopPropagation()
    wxApi
      .request({
        url: api.scence.getSceneSpec,
        loading: true,
        data: {
          reservationId:detail.id,
        },
        // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        const data = res.data.map((item) => {
          let specName = '';
          switch (item.specUnit) {
            case '1':
              specName = '按小时预定';
              break;
            case '2':
              specName = '按天预定';
              break;
            case '3':
              specName = '按周预定';
              break;
            case '4':
              specName = '按月预定';
              break;
            case '5':
              specName = '按年预定';
          }

          return {
            ...item,
            specName,
          };
        });
        this.setState(
          {
            selectDetail: detail,
            specVal: this.deleteRepeat(data),
          },

          () => {
            // 弹出预定表单弹窗
            this.animatDialogRef.current.show({
              translateY: 300,
            });
          }
        );
      })
      .catch((err) => {});
  }
  deleteRepeat=(data)=>{
    // 组装数据
    var result:any[] = [];
    var obj = {};
    for (var i = 0; i < data.length; i++) {
      if (!obj[data[i].spec]) {
        result.push(data[i]);
        obj[data[i].spec] = true;
      }
    }
    let arr:any[] = result.map((item) => {
      return {
        spec: item.spec,
      };
    });
    arr.forEach((item) => {
      item.specArr = [];
      data.forEach((el) => {
        if (el.spec == item.spec) {
          item.specArr.push(el);
        }
      });
    });
    return arr;
  }
  confirmAdd=()=>{
    this.animatDialogRef.current.hide();
    this.calcCount();
  }
  calcCount=()=>{
    // 计算购物车数量
    let that = this;
    wxApi.getStorage({
      key: 'sceneCartList',
      success(res) {
        console.log(res);
        that.setState({
          cartNumber: res.data.length,
        });
      },
      fail(err){
        that.setState({
          cartNumber: 0,
        });
      }
    });
  }
  showInDialog=(info)=>{
    // 根据二级弹窗显示设置一级弹窗是否可以点击遮罩层关闭
    this.setState({
      showInDialogSign: info.detail,
    });
  }
  toCart=()=>{
    // 去购物车
    wxApi.$navigateTo({
      url: '/sub-packages/scene-package/pages/scene-cart/index',
    });
  }
  confirmOrder() {
    // 立即下单
    wxApi.getStorage({
      key: 'sceneCartList',
      complete(res) {
        if (res.data && res.data.length != 0) {
          wxApi.$navigateTo({
            url: '/sub-packages/scene-package/pages/scene-confirm-order/index',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: '请先选择预定的场景',
          });

          return;
        }
      },
    });
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }
  //前往详情页
  toDetail=(item)=>{
    wxApi.$navigateTo({
      url: `/sub-packages/scene-package/pages/scene-detail/index?reservationId=${item.id}`,
    });
  }
  render() {
    const {
      navigationSelect,
      tmpStyle,
      firstNavList,
      seccurrentIndex,
      categoryLevel2,
      sceneList,
      cartNumber,
      showInDialogSign,
      selectDetail,
      specVal,
    } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-ssps-SceneList" className="wk-ssps-SceneList">
        <View>
          <View className="container">
            <View className="container-sidebar">
              {firstNavList &&
                firstNavList.map((navigationItem:any, index) => {
                  return (
                    <View
                      className={'container-sidebar-item ' + (navigationSelect == index ? 'navigation-select' : '')}
                      style={_safe_style_('color: ' + (navigationSelect == index ? tmpStyle.btnColor : '#333'))}
                      key={index}
                      onClick={()=>this.selectSidebar(index,navigationItem.id)}
                    >
                      <View className="categoryName">{navigationItem.categoryName}</View>
                    </View>
                  );
                })}
            </View>
            <View className="container-content">
              {categoryLevel2 && categoryLevel2.length != 0 && (
                <View className="container-content-title">
                  {categoryLevel2 &&
                    categoryLevel2.map((categoryLevel2Item:any, index) => {
                      return (
                        <View
                          key={index}
                          onClick={()=>this.selectSecondSidebar(categoryLevel2Item.id,index)}
                          style={_safe_style_(
                            seccurrentIndex === index
                              ? 'color:' +
                                  tmpStyle.btnColor +
                                  ';border-color:' +
                                  tmpStyle.btnColor +
                                  ';background-color:' +
                                  filters.handleOpacity(tmpStyle.btnColor, 0.04)
                              : ''
                          )}
                          className="categoryName"
                        >
                          {categoryLevel2Item.categoryName}
                        </View>
                      );
                    })}
                </View>
              )}

              {!!sceneList && sceneList.length != 0 && (
                <View className="scene-list-box">
                  {sceneList &&
                    sceneList.map((item:any,index) => {
                      return (
                        <View className="container-content-list" key={index}>
                          <View
                            className="list-item"
                            onClick={()=>this.toDetail(item)}
                          >
                            <Image className="image" mode="aspectFill" src={filters.imgListFormat(item.imgUrl)}></Image>
                            <View className="view">
                              <View className="list-item-title">{item.name}</View>
                              <View className="list-item-tag">
                                {item.labelArr &&
                                  item.labelArr.map((el,index) => {
                                    return (
                                      <View className="view"
                                        key={index}
                                        style={_safe_style_(
                                          'border-color: ' +
                                            tmpStyle.btnColor +
                                            '; background-color: filters.handleOpacity(' +
                                            tmpStyle.btnColor +
                                            ', 0.04); color: ' +
                                            tmpStyle.btnColor
                                        )}
                                      >
                                        <Text>{el}</Text>
                                      </View>
                                    );
                                  })}
                              </View>
                              {item.detailAddress && (
                                <View className="list-item-site">
                                  <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/address.svg"></Image>
                                  <Text className="text">{item.detailAddress}</Text>
                                </View>
                              )}

                              <View className="list-item-info">
                                <Text className="info-price">
                                  {'￥' +
                                    (item.minPrice == item.maxPrice
                                      ? filters.moneyFilter(item.minPrice || 0, true)
                                      : filters.moneyFilter(item.minPrice || 0, true) +
                                        '~￥' +
                                        filters.moneyFilter(item.maxPrice || 0, true))}
                                </Text>
                                <Text className="info-sale">{item.vitrualQty + item.sales + '人预定'}</Text>
                                <View className="info-add">
                                  <View
                                    className="info-add-btn"
                                    style={_safe_style_('background-color: ' + tmpStyle.btnColor)}
                                    onClick={(e)=>this.showDialog(item,e)}
                                  >
                                    <Text>+</Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      );
                    })}
                </View>
              )}

              {!!sceneList && sceneList.length == 0 && <Empty emptyStyle={'width:auto;'} message="敬请期待..."></Empty>}
            </View>
          </View>
          <View className="bottom-box">
            <View className="bottom-cart" onClick={this.toCart}>
              <View className="bottom-cart-btn">
                <View className="bottom-cart-btn-circle" style={_safe_style_('background-color: ' + tmpStyle.btnColor)}>
                  <Image className="image"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/scene-package/images/scene-order.svg"
                  ></Image>
                  {cartNumber ? <View className="bottom-cart-btn-badge">{cartNumber}</View> : null}
                </View>
              </View>
            </View>
            <View
              className="bottom-confirm-btn"
              onClick={this.confirmOrder}
              style={_safe_style_('background-color: ' + tmpStyle.btnColor)}
            >
              立即下单
            </View>
          </View>
        </View>
        <AnimatDialog ref={this.animatDialogRef} id="animat-dialog" animClass="commodity_attr_box" clickMaskHide={showInDialogSign ? false : true}>
          <ReserveDialog
            onConfirm={this.confirmAdd}
            onShowInDialog={this.showInDialog}
            detail={selectDetail}
            specVal={specVal}
          ></ReserveDialog>
        </AnimatDialog>
        <HomeActivityDialog showPage="sub-packages/scene-package/pages/scene-list/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default SceneList;
