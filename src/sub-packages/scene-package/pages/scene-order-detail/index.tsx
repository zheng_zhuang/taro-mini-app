import { $getRouter } from 'wk-taro-platform'
import {View, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import date from '@/wxat-common/utils/date.js';
import List from '../../components/list/index';
import './index.scss';

interface IProps{
  orderNo:string
}
interface IState{
  tmpStyle:any;
  list:any[];
  detail:any
}
class SceneOrderDetail extends React.Component<IProps,IState>{
  state={
    tmpStyle: {},
    list: [],
    detail:null
  }
  $router = $getRouter()
  componentDidMount(){
    this.getTemplateStyle();
    this.getDetail(this.$router.params.orderNo);
  }
  //获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }
  getDetail=(orderNo)=>{
    wxApi
      .request({
        url: api.scence.getOrderDetail,
        loading: true,
        data: {
          orderNo,
        },
      })
      .then((res) => {
        let detail = res.data;
        switch (detail.status) {
          case '1':
            detail.statusText = '待接单';
            break;
          case '1':
            detail.statusText = '服务中';
            break;
          case '1':
            detail.statusText = '待退押';
            break;
          case '1':
            detail.statusText = '完成';
            break;
          case '1':
            detail.statusText = '不需要处理';
        }

        let list = detail.reservationDetailItem;
        list.forEach((item) => {
          item.formExtParse = JSON.parse(item.formExt);
          item.showReserveStartTime = date.format(new Date(item.reservationTimeStart * 1000), 'yyyy-MM-dd hh:mm:ss');

          item.showReserveEndTime = date.format(new Date(item.reservationTimeEnd * 1000), 'yyyy-MM-dd hh:mm:ss');

          // 时间差
          let differ = item.reservationTimeEnd - item.reservationTimeStart + 60;
          let differText = '';
          if (differ % 3600 == 0) {
            let differHour = differ / 3600;
            if (differHour >= 24 * 30 * 12) {
              differText = `${differHour / 24 / 30 / 12}年`;
            } else if (differHour < 24 * 30 * 12 && differHour >= 24 * 30) {
              differText = `${differHour / 24 / 30}个月`;
            } else if (differHour < 24 * 30 && differHour >= 24) {
              differText = `${differHour / 24}天`;
            } else if (differHour < 24) {
              differText = `${differHour}个小时`;
            }
          } else {
            differText = `${parseInt(differ / 3600)}小时${parseInt((differ % 3600) / 60)}分钟`
          }
          console.log(differText);
          item.imgUrl = item.img;
          item.createTime = ''; // 清空返回的这个字段去显示这个场景的时间段
          item.differText = differText;
        });
        detail.formateCreateTime = date.format(new Date(detail.createTime), 'yyyy-MM-dd hh:mm:ss');

        this.setState({
          detail,
          list: detail.reservationDetailItem,
        });
      });
  }
  render() {
    const { list, detail } = this.state;
    return (
      <View data-scoped="wk-ssps-SceneOrderDetail" className="wk-ssps-SceneOrderDetail wrap">
        <List list={list} fromList={true} className="list-box"></List>
        <View className="detail-wrap">
          <View className="detail-title">
            <Text className="text">订单信息</Text>
            <Text className="text">{detail?.statusText}</Text>
          </View>
          <View className="view">{'服务订单号：' + detail?.orderNo}</View>
          <View className="view">{'下单时间：' + detail?.formateCreateTime}</View>
          <View className="view">
            押金：¥<Text>{filters.moneyFilter(detail?.deposit || 0, true)}</Text>
          </View>
          <View className="view">
            费用：¥
            <Text>{filters.moneyFilter(detail?.actualFee || 0, true)}</Text>
          </View>
          <View className="view">付款方式：{detail?.payChannel === 4 ? '账户余额支付':'微信支付'}</View>
          <View className="view">{'姓名：' + detail?.userName}</View>
          <View className="view">{'号码：' + detail?.userPhone}</View>
          <View className="view">{'备注：' + (detail?.orderMessage || '--')}</View>
        </View>
      </View>
    );
  }
}
export default SceneOrderDetail;
