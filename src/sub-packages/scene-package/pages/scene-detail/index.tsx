import {$getRouter, _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Swiper, SwiperItem, Video, Image, Text, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import util from '@/wxat-common/utils/util.js';
import CustomerHeader from '@/wxat-common/components/customer-header/index';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import ReserveDialog from '../../components/reserve-dialog/index';
import './index.scss';
import {connect} from 'react-redux'
import hoc from '@/hoc/index'
const app = Taro.getApp();

interface IState{
  tmpStyle:any,
  list:any[],
  selectDetail:any,
  isDetail:boolean,
  reservationId:string | undefined,
  detail: any,
  label:any[],
  listReservationPriceSpec:any[],
  headerDetail:any,
  selectEmployeeInfo:any,
  showModal: boolean,
  specVal: any,
  navOpacity: number,
  navBgColor: string | null,
  videoUrl: string,
}
interface IProps{
  tabbars:any
}
const mapStateToProps = (state) => {
  return {tabbars:state.globalData.tabbars};
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class SceneDetail extends React.Component<IProps,IState>{
  state={
    tmpStyle: {},
    list: [],
    selectDetail: {},
    isDetail: true,
    reservationId: '',
    detail: {},
    label: [],
    listReservationPriceSpec: [],
    headerDetail: {},
    selectEmployeeInfo: {},
    showModal: false,
    specVal: '',
    navOpacity: 0,
    navBgColor: null,
    videoUrl: '',
  }
  $router = $getRouter();
  animatDialogRef=React.createRef<any>()
  componentDidMount(){
    console.log(this.$router.params,'this.$router.params')
    this.setState({
      reservationId: this.$router.params.reservationId,
      navBgColor: this.props.tabbars.list[0].navBackgroundColor,
    },()=>{
      this.getDetailData();
      this.listReservationPriceSpec();
      this.selectEmployeeInfo();
    });

    this.getTemplateStyle();
  }

  //获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //预定会议室
  bookMeetRoom=(detail,headerdetail)=>{
    wxApi
      .request({
        url: api.scence.getSceneSpec,
        loading: true,
        data: {
          reservationId:detail.id,
        },

        // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        const data = res.data.map((item) => {
          let specName = '';
          switch (item.specUnit) {
            case '1':
              specName = '按小时预定';
              break;
            case '2':
              specName = '按天预定';
              break;
            case '3':
              specName = '按周预定';
              break;
            case '4':
              specName = '按月预定';
              break;
            case '5':
              specName = '按年预定';
          }

          return {
            ...item,
            specName,
          };
        });
        this.setState(
          {
            selectDetail: detail,
            headerDetail: headerdetail,
            specVal: this.deleteRepeat(data),
          },

          () => {
            // 弹出预定表单弹窗
            this.animatDialogRef.current.show({
              translateY: 300,
            });
          }
        );
      })
      .catch((err) => {});
  }

  confirmAdd=(info)=>{
    this.animatDialogRef.current.hide();
  }

  deleteRepeat=(data)=>{
    // 组装数据
    var result:any[] = [];
    var obj = {};
    for (var i = 0; i < data.length; i++) {
      if (!obj[data[i].spec]) {
        result.push(data[i]);
        obj[data[i].spec] = true;
      }
    }
    let arr = result.map((item) => {
      return {
        spec: item.spec,
      };
    });
    arr.forEach((item:any) => {
      item.specArr = [];
      data.forEach((el) => {
        if (el.spec == item.spec) {
          item.specArr.push(el);
        }
      });
    });
    return arr;
  }

  //返回
  confirmOrder=()=>{
    wxApi.$navigateBack();
  }

  //获取详情数据
  getDetailData=()=>{
    wxApi
      .request({
        url: api.scence.getSceneDetail,
        loading: true,
        data: util.formatParams({ reservationId: this.state.reservationId }), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        this.setState({
          detail: res.data,
          label: res.data.lable && JSON.parse(res.data.lable),
          list: res.data.imgUrl.split(','),
          videoUrl: res.data.videoUrl,
        });

        console.log(res.data);
      });
  }

  //获取规格列表
  listReservationPriceSpec=()=>{
    wxApi
      .request({
        url: api.scence.listReservationPriceSpec,
        loading: true,
        data: util.formatParams({ reservationId: this.state.reservationId }), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        res.data.forEach((item) => {
          switch (item.specUnit) {
            case '1':
              item.specName = '小时';
              break;
            case '2':
              item.specName = '天';
              break;
            case '3':
              item.specName = '周';
              break;
            case '4':
              item.specName = '月';
              break;
            case '5':
              item.specName = '年';
          }
        });
        this.setState({
          listReservationPriceSpec: res.data,
        });
      });
  }

  //获取规格列表
  selectEmployeeInfo=()=>{
    wxApi
      .request({
        url: api.scence.selectEmployeeInfo,
        loading: true,
        data: util.formatParams({ reservationId: this.state.reservationId }), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        console.log(res.data);
        this.setState({
          selectEmployeeInfo: res.data,
        });
      });
  }
  //打电话

  callPhone=()=>{
    if (!this.state.selectEmployeeInfo || !this.state.selectEmployeeInfo.phone) {
      wxApi.showToast({
        icon: 'none',
        title: '暂无客服电话',
      });

      return;
    }

    wxApi.makePhoneCall({
      phoneNumber: this.state.selectEmployeeInfo.phone,
    });
  }

  //展示微信二维码

  addWxAccount=()=>{
    if (!this.state.selectEmployeeInfo || !this.state.selectEmployeeInfo.qrcodeUrl) {
      wxApi.showToast({
        icon: 'none',
        title: '暂无微信客服',
      });

      return;
    }
    this.setState({
      showModal: true,
    });
  }

  //关系弹窗
  ok= ()=>{
    this.setState({
      showModal: false,
    });
  }

  //下载图片地址并保存到相册，提示保存成功
  savePhoto=()=>{
    let that = this;
    if(process.env.TARO_ENV !== 'h5'){
      wxApi.showLoading({
        title: '请稍后',
      });
    }

    wxApi.downloadFile({
      // 图片下载地址
      url: this.state.selectEmployeeInfo.qrcodeUrl,
      // url: app.apiUrl.url + 'Userequity/poster',
      success: function (res) {
        if(process.env.TARO_ENV === 'h5'){
          return
        }else{
          wxApi.showLoading({
            title: '请稍后',
          });
          wxApi.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
            success(res) {
              wxApi.showToast({
                title: '保存成功',
                icon: 'success',
                duration: 1000,
              });
          that.setState({
            showModal: false,
          });

            },
            complete(res) {
              wxApi.hideLoading();
            },
          });
        }
      },
      complete(res) {
        wxApi.hideLoading();
      },
    });
  }

  /**
   * 页面滚动
   * @param {*} ev
   */
  onPageScroll(e) {

    this.setState({
      navOpacity: e.scrollTop / 50,
    });
  }
  render() {
    const {
      navOpacity,
      navBgColor,
      videoUrl,
      list,
      detail,
      tmpStyle,
      label,
      listReservationPriceSpec,
      isDetail,
      selectDetail,
      headerDetail,
      specVal,
      showModal,
      selectEmployeeInfo,
    } = this.state;
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-ssps-SceneDetail"
        className="wk-ssps-SceneDetail"
      >
        {
          process.env.TARO_ENV === 'weapp' ? <CustomerHeader navOpacity={navOpacity} title="场景详情" bgColor={navBgColor}></CustomerHeader> : null
        }

        <View>
          <Swiper className="swiper" indicatorDots={true}>
            {videoUrl && (
              <SwiperItem className="swiper-item">
                <Video className="video" src={videoUrl}></Video>
              </SwiperItem>
            )}

            {list &&
              list.map((item:any,index) => {
                return (
                  <SwiperItem className="swiper-item" key={index}>
                    <Image className="image" src={item}></Image>
                  </SwiperItem>
                );
              })}
          </Swiper>
          <View className="scene-detail">
            <Text className="scene-title">{detail.name}</Text>
            <View className="tag">
              {label &&
                label.map((item) => {
                  return (
                    <Text
                     className="text"
                      style={_safe_style_(
                        'background-color: ' +
                          filters.handleOpacity(tmpStyle.btnColor, '0.06') +
                          ';border-color:' +
                          tmpStyle.btnColor +
                          ';color:' +
                          tmpStyle.btnColor
                      )}
                      key={item.id}
                    >
                      {item}
                    </Text>
                  );
                })}
            </View>
            <View className="detail">{detail.shortDesc}</View>
            {detail.detailAddress && (
              <View className="address">
                <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/address.svg"></Image>
                <Text>{detail.detailAddress || '--'}</Text>
              </View>
            )}

            <View className="king-kong-area">
              <View className="king-item">
                <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/meet-room-sizes.svg"></Image>
                <Text className="text">{detail.acreage}</Text>
              </View>
              <View className="king-item">
                <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/meet-room-time.svg"></Image>
                <Text className="text">{detail.startTime || detail.endTime ? detail.startTime + '-' + detail.endTime : null}</Text>
              </View>
              <View className="king-item">
                <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/meet-room-money.svg"></Image>
                <Text className="text">{detail.cashPledge == 0 ? '免押金' : detail.cashPledge / 100 + '元'}</Text>
              </View>
            </View>
          </View>
          <View className="scene-specification">
            <Text className="specification-title">选择场地规格</Text>
            {listReservationPriceSpec &&
              listReservationPriceSpec.map((item, index) => {
                return (
                  <View
                    key={item.id}
                    className="specification-item"
                    style={_safe_style_('background-color: ' + filters.handleOpacity(tmpStyle.btnColor, '0.06') + ';')}
                  >
                    <View className="item-left">
                      <View className="view">
                        <Text className="text">{'规格：' + item.spec}</Text>
                        <Text className="text" style={_safe_style_('color: ' + tmpStyle.btnColor + ';')}>
                          {'¥' + item.unitPrice / 100 + '元/' + item.specName}
                        </Text>
                      </View>
                      <View className="view">
                        <Text className="text">
                          {detail.reservationDays === 0 ? '无需提前预定' : '需提前' + detail.reservationDays + '天预定'}
                        </Text>
                        <Text className="text">{'｜' + item.minimum + item.specName + '起定'}</Text>
                      </View>
                    </View>
                    <Button
                    className="button"
                      style={_safe_style_('background-color: ' + tmpStyle.btnColor + ';')}
                      onClick={()=>this.bookMeetRoom(detail,item)}
                    >
                      预定
                    </Button>
                  </View>
                );
              })}
          </View>
          <View className="place-holder"></View>
          <View className="scene-bottom">
            <View className="bottom-left">
              <View className="view" onClick={this.callPhone}>
                <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/phone.svg"></Image>
                <Text className="text">电话</Text>
              </View>
              <View className="view" onClick={this.addWxAccount}>
                <Image className="image" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/weixin.svg"></Image>
                <Text className="text">微信</Text>
              </View>
            </View>
            <View className="bottom-right">
              <Button className="button" style={_safe_style_('background-color: ' + tmpStyle.btnColor + ';')} onClick={this.confirmOrder}>
                返回
              </Button>
            </View>
          </View>
        </View>
        <AnimatDialog ref={this.animatDialogRef} id="animat-dialog" animClass="commodity_attr_box" clickMaskHide={true}>
          <ReserveDialog
            onConfirm={this.confirmAdd}
            isDetail={isDetail}
            // onShowInDialog={this.showInDialog}
            detail={selectDetail}
            headerDetail={headerDetail}
            specVal={specVal}
          ></ReserveDialog>
        </AnimatDialog>
        {/*  弹出层  */}
        <Block>
          {showModal && (
            <View
              className="mask"
              // onTouchMove={this.privateStopNoop.bind(this, this.preventTouchMove)}
              onClick={this.ok}
            ></View>
          )}

          {showModal && (
            <View className="modalDlg">
              <View className="modalDlg-top">
                <View className="modalDlg-right">
                  <View className="name">{'员工姓名:' + selectEmployeeInfo["name"]}</View>
                  <View className="position">{'手机号:' + selectEmployeeInfo["phone"]}</View>
                </View>
              </View>
              <Image src={selectEmployeeInfo["qrcodeUrl"]} className="code"></Image>
              <View className="text">扫一扫上面的二维码，加我微信</View>
              <View
                onClick={this.savePhoto}
                className="ok"
                style={_safe_style_('background-color: ' + tmpStyle["btnColor"])}
              >
                 {process.env.TARO_ENV == 'h5'?'长按保存图片':'保存图片'}
              </View>
            </View>
          )}
        </Block>
      </View>
    );
  }
}

export default SceneDetail;
