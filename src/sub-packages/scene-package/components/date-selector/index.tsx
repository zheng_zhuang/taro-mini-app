import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import {View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import getTime from '../../time.js';

import './index.scss';

interface IProps{
  reservationDays:number;
  lockDays:any[];
  showDateSelector:boolean | undefined | null;
  selectDate:any;
  onCancel:Function;
  onConfirm:Function;
  isLock:boolean
}
interface IState{
  tmpStyle:any;
  dateTitle:{
    year: number,
    month: number,
    date:string,
    day:number
  };
  daysList:any[];
  selectDate:any;
  futureDate:any
}

class DateSelector extends React.Component<IProps,IState>{
  static defaultProps = {
    reservationDays:0,
    lockDays:[],
    showDateSelector:false,
    selectDate:{},
    onCancel:() => {return null},
    onConfirm:() => {return null},
    isLock:false
  }
  
  state = {
    tmpStyle: {},
    dateTitle: {
      // 选择器头部信息
      year: 0,
      month: 0,
      date: '',
      day:0
    },
    daysList: [], // 日期渲染列表
    selectDate:null,
    futureDate:null
  }

  componentDidMount() {
    this.getTemplateStyle();
    this.getNowDate().then(()=>{
      this.getDaysList();
    });
  }

  getTemplateStyle=()=>{
    // 获取模板配置
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  getNowDate= async()=>{
    // 获取现在的年月
    const date = await getTime.getTime()
    const year = date.year;
    const month = date.month;
    const day = date.day;
    const { dateTitle } = this.state;
    const {selectDate} = this.props
    // 通过提前预订天数算出可以预订的日期
    const calcDate = this.calcAfterDate(this.props.reservationDays + 1, `${year}-${month}-${day}`);

    const calcDateArr = calcDate.split('-');
    const calcYear = calcDateArr[0];
    const calcMonth = calcDateArr[1];
    const calcDay = calcDateArr[2];
    dateTitle.year = selectDate.year || calcYear;
    dateTitle.month = selectDate.month || calcMonth;
    dateTitle.day = selectDate.day || calcDay;
    dateTitle.date = `${selectDate.year || calcYear}年${selectDate.month || calcMonth}月`;
    this.setState({
      dateTitle,
      // 选择日期初始赋值
      selectDate: {
        year: selectDate.yaer || calcYear,
        month: selectDate.month || calcMonth,
        day: selectDate.day || calcDay,
      },

      // 提前天数的日期
      futureDate: {
        year: calcYear,
        month: calcMonth,
        day: calcDay,
      },
    });
  }

  getMonthDays=async()=>{
    // 获取当前月份有几天
    const { dateTitle } = this.state;
    const date = await getTime.getTime()
    const year = date.year;
    const month = date.month;
    return new Promise((resolve) => {
      resolve(
        new Date(dateTitle.year ? dateTitle.year : year, dateTitle.month ? dateTitle.month : month, 0).getDate()
      );
    });
  }
  getDaysList=()=>{
    // 获取月份的天数数据
    this.getMonthDays().then((res) => {
      let days:any = res;
      const { dateTitle, selectDate, futureDate } = this.state;
      const {lockDays,isLock} = this.props
      console.log('props',isLock)
      const daysList:any[] = [];
      let firstWeek = new Date(dateTitle.year, dateTitle.month - 1, 1).getDay();
      if (firstWeek != 0) {
        // 如果不是周日，则需要在日期前面添加空数据
        for (let i = 0; i < firstWeek; i++) {
          daysList.push({});
        }
      }
      for (let i = 1; i <= days; i++) {
        let disable = false;
        if (dateTitle.year < futureDate.year && !isLock) {
          // 以前
          disable = true;
        } else if (dateTitle.year == futureDate.year) {
          // 今年
          if (dateTitle.month < futureDate.month && !isLock) {
            // 这个月以前
            disable = true;
          } else if (dateTitle.month == futureDate.month) {
            // 当月
            disable = i < futureDate.day && !isLock ? true : false;
          } else {
            // 以后的每个月
            disable = false;
          }
        } else {
          // 以后
          disable = false;
        }
        daysList.push({
          day: i,
          // 如果是现在的年月，并且天数是今天
          active:
            selectDate.year == dateTitle.year && selectDate.month == dateTitle.month && i == selectDate.day
              ? true
              : false,
          disable,
        });
      }
      // 锁场
      if(!isLock){
        for (let i = 0; i < lockDays.length; i++) {
          let arr = lockDays[i].split('-');
          let iYear = arr[0];
          let iMonth = arr[1];
          let iDay = arr[2];
          if (iYear == dateTitle.year && iMonth == dateTitle.month) {
            daysList.forEach((item) => {
              if (item.day == iDay) {
                item.disable = true;
              }
            });
          }
        }
      }

      this.setState({
        daysList,
      });
    });
  }
  minusMonth=()=>{
    // 减少月份
    const dateTitle = this.state.dateTitle;
    if (dateTitle.month != 1) {
      // 如果不是1月，正常减少
      dateTitle.month--;
    } else {
      // 如果是1月，则年-1，月变为12
      dateTitle.year--;
      dateTitle.month = 12;
    }
    dateTitle.date = `${dateTitle.year}年${dateTitle.month}月`;
    this.setState(
      {
        dateTitle,
      },

      () => {
        this.getDaysList();
      }
    );
  }
  plusMonth=()=>{
    // 增加月份
    const dateTitle = this.state.dateTitle;
    if (dateTitle.month != 12) {
      // 如果不是12月，正常增加
      dateTitle.month++;
    } else {
      // 如果是12月，则年+1，月变为1
      dateTitle.year++;
      dateTitle.month = 1;
    }
    dateTitle.date = `${dateTitle.year}年${dateTitle.month}月`;
    this.setState(
      {
        dateTitle,
      },

      () => {
        this.getDaysList();
      }
    );
  }
  selectDay=(index,item)=>{
    if (item.disable) {
      return;
    }
    // 选择日期
    const selectIndex =index;
    const dateTitle = this.state.dateTitle;
    let daysList = this.state.daysList.map((item:any, index) => {
      if (index == selectIndex) {
        this.setState({
          selectDate: {
            year: dateTitle.year,
            month: dateTitle.month,
            day: item.day,
          },
        });

        return {
          ...item,
          active: true,
        };
      } else {
        return {
          ...item,
          active: false,
        };
      }
    });
    this.setState({
      daysList,
    });
  }
  cancel=()=>{
    // this.setState(
    //   {
    //     showDateSelector: false,
    //   },

    //   () => {
        this.props.onCancel();
      // }
    // );
  }
  confirm=()=>{
    // this.setState(
    //   {
    //     showDateSelector: false,
    //   },

    //   () => {
        this.props.onConfirm(this.state.selectDate);
      // }
    // );
  }
  calcAfterDate(num, day) {
    // 计算多少天后的日期
    const dayArr = day.split('-');
    var newdate = new Date(Number(dayArr[0]), Number(dayArr[1]) - 1, Number(dayArr[2]));

    var newtimems = newdate.getTime() + (num - 1) * 24 * 60 * 60 * 1000;
    newdate.setTime(newtimems);
    let newMonth = newdate.getMonth() + 1;
    let newDay = newdate.getDate();
    var time = `${newdate.getFullYear()}-${String(newMonth).length == 1 ? `0${newMonth}` : `${newMonth}`}-${
      String(newDay).length == 1 ? `0${newDay}` : `${newDay}`
    }`;
    return time;
  }
  render() {
    const { tmpStyle, dateTitle, daysList } = this.state;
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-sscd-DateSelector"
        className="wk-sscd-DateSelector"
      >
        <View className="selector-date-head">
          <View className="selector-cancel" onClick={this.cancel}>
            取消
          </View>
          <View
            className="selector-confirm"
            style={_safe_style_('color: ' + tmpStyle.btnColor)}
            onClick={this.confirm}
          >
            确定
          </View>
        </View>
        <View className="selector-date-handle">
          <Image
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/left-arrow.png"
            className="arrow-img"
            onClick={ this.minusMonth}
          ></Image>
          <Text>{dateTitle.date}</Text>
          <Image
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
            className="arrow-img"
            onClick={this.plusMonth}
          ></Image>
        </View>
        <View className="date-box">
          <View className="date-box-week">
            <Text className="week-item">日</Text>
            <Text className="week-item">一</Text>
            <Text className="week-item">二</Text>
            <Text className="week-item">三</Text>
            <Text className="week-item">四</Text>
            <Text className="week-item">五</Text>
            <Text className="week-item">六</Text>
          </View>
          <View className="days-box">
            {daysList &&
              daysList.map((item:any, index) => {
                return (
                  <View
                    key={index}
                    className={'day-item ' + (item.disable ? 'disable' : '')}
                    style={_safe_style_(
                      'background-color: ' +
                        (item.active ? tmpStyle.btnColor : '') +
                        '; color: ' +
                        (item.disable ? '#949494' : item.active ? '#fff' : '#000')
                    )}
                    onClick={()=>this.selectDay(index,item)}
                  >
                    {item.day || ''}
                  </View>
                );
              })}
          </View>
        </View>
      </View>
    );
  }
}

export default DateSelector;
