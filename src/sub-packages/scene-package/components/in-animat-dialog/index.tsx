import { _safe_style_ } from 'wk-taro-platform';
import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';
interface IProps{
  duration:number;
  clickMaskHide:boolean;
  customStyle:any;
  hide:Function;
  animClass?:string;
  fixFullScreenClass?:string;
  customClass?:string;
  modalClass?:string
}
interface IState{
  start:{
    scale: boolean,
    translateY: number,
  },
  animationData:any,
  showInModalStatus:boolean
}
class InAnimatDialog extends React.Component<IProps,IState>{
  // 外部样式类
  //modal-class 遮罩层样式
  // externalClasses: ['anim-class', 'fix-full-screen-class', 'custom-class', 'modal-class'],
  static defaultProps={
    duration:200,
    clickMaskHide:true,
    customStyle:null
  }
  state={
    start: {
      scale: false,
      translateY: 0,
    },
    animationData:null,
    showInModalStatus:false
  }
  animation:any = null
  componentDidMount() {
    this.animation = wxApi.createAnimation({
      duration: this._getDuration(),
      timingFunction: 'linear',
      delay: 0,
    });
  }
  preventTouchMove() {}
  _getDuration=():number=>{
    return this.props.duration || 200;
  }
  _raf=(callback)=>{
    setTimeout(
      function () {
        callback();
      }.bind(this),
      16
    );
  }
  _scale=(isShow, immediate = false)=>{
    if (isShow) {
      this.setState({
        animationData: this.animation.scale(0, 0).step().export(),
        showInModalStatus: true,
      });

      this._raf(() => {
        this.setState({
          animationData: this.animation.scale(1, 1).step().export(),
        });
      });
    } else {
      this.setState({
        animationData: this.animation.scale(0, 0).step().export(),
      });

      if (immediate) {
        this.setState({
          showInModalStatus: false,
        });
      } else {
        setTimeout(() => {
          this.setState({
            showInModalStatus: false,
          });
        }, this._getDuration() + 16);
      }
    }
  }
  _translateY=(isShow)=>{
    const { translateY } = this.state.start;
    if (isShow) {
      this.setState({
        animationData: this.animation.translateY(translateY).step().export(),
        showInModalStatus: true,
      });

      this._raf(() => {
        this.setState({
          animationData: this.animation.translateY(0).step().export(),
        });
      });
    } else {
      this.setState({
        animationData: this.animation.translateY(translateY).step().export(),
      });

      setTimeout(() => {
        this.setState({
          showInModalStatus: false,
        });
      }, this._getDuration() + 16);
    }
  }
  show=(options)=>{
    const { scale, translateY } = options;
    this.setState({
      start: {
        scale,
        translateY,
      },
    });

    if (scale) {
      this._scale(true);
    } else if (translateY) {
      this._translateY(true);
    }
  }
  hide(immediate = false) {
    const { scale, translateY } = this.state.start;
    if (scale) {
      this._scale(false, immediate);
    } else if (translateY) {
      this._translateY(false);
    }

    // this.props.hide()
  }
  maskHide=()=>{
    if (this.props.clickMaskHide) {
      this.hide();
    }
  }
  render() {
    const { showInModalStatus, animationData } = this.state;
    const {customStyle} = this.props
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-ssci-InAnimatDialog"
        className="wk-ssci-InAnimatDialog"
      >
        {showInModalStatus && (
          <View
            onTouchMove={this.preventTouchMove}
            className={`in_commodity_screen ${this.props.modalClass}`}
            onClick={this.maskHide}
          ></View>
        )}

        {/* 弹出框   */}
        {showInModalStatus && (
          <View
            animation={animationData}
            className={`${this.props.animClass} ${this.props.fixFullScreenClass} ${this.props.customClass}`}
            // style={_safe_style_(customStyle)}
          >
            {this.props.children}
          </View>
        )}
      </View>
    );
  }
}
export default InAnimatDialog;
