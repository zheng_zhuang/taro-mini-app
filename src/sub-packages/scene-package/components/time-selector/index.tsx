import { _safe_style_ } from 'wk-taro-platform';
import { View, PickerView, PickerViewColumn } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';
import MultiPicker from "../picker/multi-picker"
import PickerComponent from "../picker"

interface IProps{
  hours:any[];
  minutes:any[];
  startTimeArr:string[];
  endTimeArr:string[];
  leastTime:number | null;
  isNextDay:boolean | null,
  onCancel:Function;
  onConfirm:Function
}
interface IStats{
  tmpStyle:any;
  selectTime:any;
  indexVal:number[];
  minutes:any[];
  value:number[]
}
class TimeSelector extends React.Component<IProps,IStats>{
  nextProps(){
    const { hours, startTimeArr, endTimeArr } = this.props;
    const {indexVal} = this.state
    let minutes:any[] = [];
    if (hours[indexVal[0]] == startTimeArr[0]) {
      // 起始时间，设置起始时间的分钟数
      minutes = this.setMinutes(startTimeArr[1]);
    } else if (hours[indexVal[0]] == endTimeArr[0]) {
      // 终止时间，设置终止时间的分钟数
      minutes = this.setMinutes(endTimeArr[1], 'end');
    } else {
      minutes = this.setMinutes(0);
    }
    console.log('${hours[indexVal[0]]}:${minutes[indexVal[1]]}',`${hours[indexVal[0]]}:${minutes[indexVal[1]]}`)
    this.setState({
      selectTime: `${hours[indexVal[0]]}:${minutes[indexVal[1]]}`,
    });
  }

  state={
    tmpStyle: {},
    selectTime: '',
    indexVal: [0, 0], // 选择器下标数组
    minutes:[]
  }

  componentDidMount() {
    this.getTemplateStyle();
    this.nextProps()
  }

  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  changeSelectorTime=(e)=>{
    const { hours, startTimeArr, endTimeArr } = this.props;
    // 时间选择器更改时间
    let val:any = null
    if(Array.isArray(e)){
      val = e
    }else{
     val = e.detail.value;
    }

    let minutes:any[] = [];
    if (hours[val[0]] == startTimeArr[0]) {
      // 起始时间，设置起始时间的分钟数
      minutes = this.setMinutes(startTimeArr[1]);
    } else if (hours[val[0]] == endTimeArr[0]) {
      // 终止时间，设置终止时间的分钟数
      minutes = this.setMinutes(endTimeArr[1], 'end');
    } else {
      minutes = this.setMinutes(0);
    }
    this.setState({
      selectTime: `${hours[val[0]]}:${minutes[val[1]]}`,
      indexVal: val,
      value:val
    });
  }

  setMinutes=(minute, type?)=>{
    // 时间选择器设置分钟
    const minutes:any[] = [];
    if (type && type == 'end') {
      // 如果是终止时间
      for (let i = 0; i <= Number(minute); i++) {
        let stringI = i + '';
        if (stringI.length === 1) {
          stringI = `0${stringI}`;
        }
        minutes.push(stringI);
      }
    } else {
      // 如果是起始时间
      for (let i = Number(minute); i <= 59; i++) {
        let stringI = i + '';
        if (stringI.length === 1) {
          stringI = `0${stringI}`;
        }
        minutes.push(stringI);
      }
    }
    this.setState({
      minutes,
    });

    return minutes;
  }

  cancel=()=>{
    this.props.onCancel()
  }

  confirm=()=>{
    const { leastTime, hours, minutes, startTimeArr,endTimeArr,isNextDay } = this.props;
    const { indexVal } = this.state
    let nextDayIndex; // 第二天的下标
    let endTimeHours = Number(endTimeArr[0])
    if(isNextDay){
      endTimeHours = Number(endTimeArr[0]) + 24
      nextDayIndex = hours.indexOf('00')
    }
    let selectHours; 
    if(isNextDay && nextDayIndex > -1 && indexVal[0]>=nextDayIndex){
      selectHours = Number(hours[indexVal[0]]) + 24
    }else{
      selectHours = Number(hours[indexVal[0]])
    }
    // 24小时不做判断
    if(startTimeArr[0] != endTimeArr[0] && startTimeArr[1] != endTimeArr[1]){
      if (endTimeHours - selectHours < Number(leastTime)) {
        let endTimeMinutes= (Number(endTimeArr[1]) + 1) % 60
        // 小59分钟
        if(!(endTimeHours - selectHours == (Number(leastTime)-1) && endTimeMinutes == Number(minutes[indexVal[1]]))){
          wxApi.showToast({
            icon: 'none',
            title: '日期时间无法满足最少预订时长'
          })
          return
        }
      }
    }
    if (endTimeHours - selectHours == Number(leastTime)) {
      // 如果选择的小时数刚好是最晚预订的时间，则比较分钟
      if (minutes[indexVal[1]] > (Number(endTimeArr[1])+1)) {
        wxApi.showToast({
          icon: 'none',
          title: '日期时间无法满足最少预订时长',
        });

        return;
      }
    }
    this.props.onConfirm(this.state.selectTime || `${this.props.hours[0]}:${this.props.minutes[0]}`)
  }

  render() {
    const { tmpStyle, minutes,value } = this.state;
    const {hours} = this.props
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-ssct-TimeSelector"
        className="wk-ssct-TimeSelector"
      >
        <View className="selector-date-head">
          <View className="selector-cancel" onClick={this.cancel}>
            取消
          </View>
          <View
            className="selector-confirm"
            style={_safe_style_('color: ' + tmpStyle.btnColor)}
            onClick={this.confirm}
          >
            确定
          </View>
        </View>
        { process.env.TARO_ENV === 'weapp' ?
          <PickerView
          indicatorStyle="height: 50px;"
          style={_safe_style_('width: 100%; height: 300px;')}
          onChange={this.changeSelectorTime}
        >
          <PickerViewColumn>
            {hours &&
              hours.map((item, index) => {
                return (
                  <View key={index} style={_safe_style_('height: 50px;line-height: 50px;text-align: center;')}>
                    {item}
                  </View>
                );
              })}
          </PickerViewColumn>
          <PickerViewColumn>
            {minutes &&
              minutes.map((item, index) => {
                return (
                  <View key={index} style={_safe_style_('height: 50px;line-height: 50px; text-align: center;')}>
                    {item}
                  </View>
                );
              })}
          </PickerViewColumn>
        </PickerView>
        :
        <MultiPicker selectedValue={value} onValueChange={this.changeSelectorTime} className="multi-picker">
          <PickerComponent indicatorClassName="my-picker-indicator">
                {hours &&
              hours.map((item:any, index) => {
                return (
                  <PickerComponent.Item key={index} className="my-picker-view-item" value={index}>{item}</PickerComponent.Item>
                );
              })}
          </PickerComponent>
          <PickerComponent indicatorClassName="my-picker-indicator">
                {minutes &&
              minutes.map((item:any, index) => {
                return (
                  <PickerComponent.Item key={index} className="my-picker-view-item" value={index}>{item}</PickerComponent.Item>
                );
              })}
          </PickerComponent>
        </MultiPicker>
      }
      </View>
    );
  }
}


export default TimeSelector;
