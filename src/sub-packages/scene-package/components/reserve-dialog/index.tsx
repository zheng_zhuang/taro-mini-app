import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import {
  ScrollView,
  View,
  Text,
  Image,
  RadioGroup,
  Label,
  Radio,
  CheckboxGroup,
  Checkbox,
  Picker,
  Input,
  Textarea,
  PickerView,
  PickerViewColumn,
} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import getTime from '../../time.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import utilDate from "@/wxat-common/utils/date.js";

import DateSelector from "../date-selector";
import TimeSelector from "../time-selector";
import InAnimatDialog from "../in-animat-dialog";
import RegionPicker from "../region-picker"
import MultiPicker from "../picker/multi-picker"
import PickerComponent from "../picker"
import './index.scss';

const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};
const year = []
for(let i=1970;i<=2099;i++){
  year.push(i)
}
const month = []
for(let i=0;i<12;i++){
  month.push(i)
}
interface IProps{
  detail?:any;
  specVal?:any;
  isDetail?:boolean;
  headerDetail?:any;
  onConfirm:Function;
  onShowInDialog:Function
}
interface IState{
    type:string;
    tmpStyle: any,
    form: any,
    selectSpecIndex:number, // 范围规格默认选择第一个
    selectSpec: any, // 范围规格默认选择第一个
    selectTimeIndex: number, // 时间规格默认选择第一个
    selectTimeSpecification: any, // 时间规格默认选择第一个
    hours: any[], // 时间选择器小时数组
    minutes: any[], // 时间选择器分钟数组
    showSelectTime: any, // 时间选择器选择的时间
    showSelectDate: any, // 日期选择器选择的日期(用于显示)
    selectDate:any, // 日期选择器选择的日期
    singleArr:any[], // 单项选择器数据
    showSelectDuration: string, // 预定时长
    showSelectDays: string | number, // 预定天数
    showSelectMonths: string | number, // 预定月数
    showSelectYears: string | number, // 预定年数
    totalPrice: number,
    currentHeader: any,
    calcEndTime:any,
    formExt:any[],
    leastTime:any,
    startTimeArr:string[],
    endTimeArr:string[],
    durations:any[],
    selectDuration:any,
    selectDays:null | string,
    inType:string,
    selectMonths:null | string,
    selectYears:null | string,
    showDateSelector:boolean | null,
    lockDays:any[],
    value:number,
    pickerDateIndex:number
}

class ReserveDialog extends React.Component<IProps,IState>{
  state = {
    type: '',
    tmpStyle: {},
    form: {},
    selectSpecIndex: 0, // 范围规格默认选择第一个
    selectSpec: {}, // 范围规格默认选择第一个
    selectTimeIndex: 0, // 时间规格默认选择第一个
    selectTimeSpecification: {}, // 时间规格默认选择第一个
    hours: [], // 时间选择器小时数组
    minutes: [], // 时间选择器分钟数组
    showSelectTime: '', // 时间选择器选择的时间
    showSelectDate: '', // 日期选择器选择的日期(用于显示)
    selectDate: {}, // 日期选择器选择的日期
    singleArr: [], // 单项选择器数据
    showSelectDuration: '', // 预定时长
    showSelectDays: '', // 预定天数
    showSelectMonths: '', // 预定月数
    showSelectYears: '', // 预定年数
    totalPrice: 0,
    currentHeader: {},
    calcEndTime:'',
    formExt:[],
    leastTime:null,
    startTimeArr:[],
    endTimeArr:[],
    durations:[],
    selectDuration:null,
    selectDays:null,
    inType:'',
    selectMonths:null,
    selectYears:null,
    showDateSelector:null,
    lockDays:[],
    value:0,
    pickerDateIndex:0,
    isNextDay:null, // 是否跨天
  }

  animation:any
  inAnimatDialogRef = React.createRef<any>()
  isUnMount = false
  static defaultProps = {
    detail:{},
    specVal:{},
    isDetail:false,
    headerDetail:{},
    onConfirm:()=>{return null},
    onShowInDialog:()=>{return null}
  }

  componentDidMount() {
    this.getTemplateStyle();
    this.animation = wxApi.createAnimation({
      duration: 200,
      timingFunction: 'linear',
      delay: 0,
    });
    if(!this.isUnMount){
      this.nextPropsDetail(this.props.detail)
      this.nextPropsSpecVal(this.props.specVal)
      this.nextPropsHeaderDetail(this.props.headerDetail)
    }
  }

  componentWillUnmount(){
    this.isUnMount = true
  }

  nextPropsDetail = (newV) =>{
    let formExt:any[] = [];
    if (newV.formInfo) {
      formExt = JSON.parse(newV.formInfo).widgetValue;
    }
    formExt.forEach((item:any, index) => {
      // 地址栏多添加一行详细地址
      if (item.type === 'address') {
        formExt.splice(index + 1, 0, {
          label: '详细地址',
          type: 'detail-address',
          required: false,
          value: '',
        });
      }
      // 多选
      if (item.type === 'checkbox') {
        item.options = item.options.map((el) => {
          return {
            value: el,
            checked: false,
          };
        });
      }
    });
    this.setState({
      formExt,
      showSelectDate: '',
      showSelectTime: '',
      showSelectDuration: '',
      showSelectDays: '',
      showSelectMonths: '',
      showSelectYears: '',
      calcEndTime: '',
      totalPrice: 0,
    });
  }

  nextPropsSpecVal = (newV) =>{
    this.setState(
      {
        selectSpec: newV[0],
        selectTimeSpecification: newV.length != 0 ? newV[0].specArr[0] : [],
      },
      () => {
        if (newV && newV.length) {
          if (!this.props.isDetail) {
            // 列表进入，规格和时间恢复默认
            this.setState({
              selectSpecIndex: 0,
              selectTimeIndex: 0,
            });

            return;
          }
          const current = newV.find((e) => e.spec === this.props.headerDetail.spec);

          const selectSpecIndex = newV.findIndex((e) => e.spec === this.props.headerDetail.spec);

          const selectTimeIndex = current.specArr.findIndex(
            (e) => e.specUnit === this.props.headerDetail.specUnit
          );

          this.setState({
            currentHeader: current.specArr[selectTimeIndex],
            selectSpec: current,
            selectTimeSpecification: current.specArr[selectTimeIndex],
            selectSpecIndex: selectSpecIndex != -1 ? selectSpecIndex : 0,
            selectTimeIndex: selectTimeIndex != -1 ? selectTimeIndex : 0,
          });
        }
      }
    );
  }

  nextPropsHeaderDetail = (newV) =>{
    switch (newV.specUnit) {
      case '1':
        newV.type = '小时';
        break;
      case '2':
        newV.type = '天';
        break;
      case '3':
        newV.type = '周';
        break;
      case '4':
        newV.type = '月';
        break;
      case '5':
        newV.type = '年';
        break;
    }

    this.setState({
      type: newV.type,
    });
  }

  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  closeDialog=()=>{
    // 关闭弹窗
    this.props.onConfirm('123');
  }

  doNotCloseDialog=()=>{
    // 点击弹窗部分不关闭弹窗
    return false;
  }

  emptyFunction=()=>{
    console.log('禁止背景滚动');
  }

  inputVal=(e)=>{
    // 输入框输入
    const key = e.currentTarget.dataset.item;
    const value = e.detail.value;
    const form = this.state.form;
    form[key] = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState(
      {
        form,
      },

      () => {
        console.log(this.state.form);
      }
    );
  }

  selectSpecification=(specification,specindex)=>{
    // 选择范围规格
    const index = specindex;
    const detail = specification;
    let type = '';
    switch (detail.specArr[0].specUnit) {
      case '1':
        type = '小时';
        break;
      case '2':
        type = '天';
        break;
      case '3':
        type = '周';
        break;
      case '4':
        type = '月';
        break;
      case '5':
        type = '年';
        break;
    }

    if (index == this.state.selectSpecIndex) {
      return;
    }
    // 切换范围规格重置时间规格选择
    this.setState({
      selectSpecIndex: index,
      selectSpec: detail,
      selectTimeSpecification: detail.specArr[0],
      selectTimeIndex: 0,
      currentHeader: detail.specArr[0],
      showSelectTime: '',
      showSelectDuration: '',
      showSelectDays: '',
      showSelectMonths: '',
      showSelectYears: '',
      calcEndTime: '',
      type,
      totalPrice: 0,
    });
  }

  selectTime=(specification,index)=>{
    // 选择时间规格
    if (this.state.selectTimeIndex == index) {
      return;
    }
    let type = '';
    switch (specification.specUnit) {
      case '1':
        type = '小时';
        break;
      case '2':
        type = '天';
        break;
      case '3':
        type = '周';
        break;
      case '4':
        type = '月';
        break;
      case '5':
        type = '年';
        break;
    }

    this.setState({
      selectTimeIndex: index,
      selectTimeSpecification: specification,
      showSelectDate: '',
      showSelectTime: '',
      showSelectDuration: '',
      showSelectDays: '',
      showSelectMonths: '',
      showSelectYears: '',
      calcEndTime: '',
      currentHeader: specification,
      type,
      totalPrice: 0,
    });
  }

  checkForm=(dataList)=>{
    let error = '';
    dataList.forEach((item) => {
      if (error) return;
      if (item.required && !item.value) {
        error = item.label + '不能为空';
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });
    return error;
  }

  chooseGender=({ value, index })=>{
    // 选择性别
    const formExt:any = this.state.formExt;
    formExt[index].value = value;
    this.setState({
      formExt: formExt,
    });
  }

  inputValue=(index,e)=>{
    console.log(e,'e')
    const value = e.detail.value;
    const formExt:any = this.state.formExt;
    formExt[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({
      formExt,
    });
  }

  onRegionPickerChange=(value,{index})=>{
    const formExt:any = this.state.formExt;
    formExt[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({
      formExt,
    });
  }

  bindValue=(index,item,e)=>{
    // 多选修改
    const items = item.options;
    const values = e.detail.value;
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false;
      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].value == values[j]) {
          items[i].checked = true;
          break;
        }
      }
    }
    const formExt:any = this.state.formExt;
    const valueArr:any[] = [];
    items.forEach((item) => {
      if (item.checked) {
        valueArr.push(item.value);
      }
    });
    formExt[index].options = items;
    formExt[index].value = valueArr;
    this.setState({
      formExt,
    });
  }

  promiseState=(value)=>{
    return new Promise((resolve,reject)=>{
      this.setState({
        ...value
      },()=>{
        resolve(null)
      })
    })
  }

  confirmReserve=(isDetail)=>{
    const {
      selectTimeSpecification,
      showSelectDate,
      showSelectTime,
      showSelectDuration,
      showSelectDays,
      showSelectMonths,
      showSelectYears,
      formExt,
      selectSpec,
      selectTimeIndex,
      calcEndTime,
      totalPrice,
    } = this.state;
   let {detail} = this.props
    if (!showSelectDate) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择开始日期',
      });

      return;
    }
    if (selectTimeSpecification.specUnit == '1') {
      if (!showSelectTime) {
        wxApi.showToast({
          icon: 'none',
          title: '请选择开始时间',
        });

        return;
      }
      if (!showSelectDuration) {
        wxApi.showToast({
          icon: 'none',
          title: '请选择预定时长',
        });

        return;
      }
    }
    if (selectTimeSpecification.specUnit == '2' && !showSelectDays) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择预定天数',
      });

      return;
    }
    if (selectTimeSpecification.specUnit == '4' && !showSelectMonths) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择预定月数',
      });

      return;
    }
    if (selectTimeSpecification.specUnit == '5' && !showSelectYears) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择预定年数',
      });

      return;
    }
    const error = this.checkForm(formExt);
    if (error) {
      wxApi.showToast({ icon: 'none', title: error });
      return;
    }
    let reserveStartTime = '';
    let reserveEndTime = '';
    let showReserveStartTime = '';
    let showReserveEndTime = '';
    console.log(calcEndTime);
    if (selectTimeSpecification.specUnit == '1') {
      // 如果是按小时预定
      reserveStartTime = `${showSelectDate} ${showSelectTime}:00`;
      showReserveStartTime = `${showSelectDate} ${showSelectTime}`;
      reserveEndTime = `${calcEndTime}:00`;
      showReserveEndTime = `${calcEndTime}`;
    } else {
      // 如果不是按小时预定
      reserveStartTime = `${showSelectDate} 00:00:00`;
      showReserveStartTime = `${showSelectDate}`;
      reserveEndTime = `${calcEndTime} 00:00:00`;
      showReserveEndTime = `${calcEndTime}`;
    }
    detail = {
      ...detail,
      formExt, // 自定义表单信息
      reserveStartTime, // 预约开始时间
      reserveEndTime, // 预约结束时间
      showReserveStartTime, // 预约开始时间-展示用
      showReserveEndTime, // 预约结束时间-展示用
      timeSpecUnit: selectTimeSpecification.specUnit, // 时间规格信息，1为小时，其他为天
      showSelectDuration, // 预定时长
      specInfo: selectSpec.specArr[selectTimeIndex], // 场景规格信息
      totalPrice, // 金额
    };
    console.log(detail);
    // 加入预定
    const sceneCartList = wxApi.getStorageSync('sceneCartList') || [];
    sceneCartList.push(JSON.stringify(detail));
    if (this.props.isDetail && isDetail) {
      // 立即下单
      wxApi.$navigateTo({
        url: '/sub-packages/scene-package/pages/scene-confirm-order/index',
        data: {
          detail: JSON.stringify(detail),
        },
      });
    } else {
      wxApi.setStorageSync('sceneCartList', sceneCartList);
      wxApi.setStorageSync('sceneCartSelectList', sceneCartList);
    }
    this.closeDialog();
  }

  showInDialog= async (type)=>{
    // 选择的规格信息，包含最小预定时间minimum,specUnit为1是小时为单位
    const leastTime = this.state.selectSpec.specArr[this.state.selectTimeIndex].minimum;
    this.setState({
      leastTime,
    });

    const inType = type;
    // 选择的场景详情
    const detail = this.props.detail;
    const startTimeArr = detail.startTime ? detail.startTime.split(':') : [];
    const endTimeArr = detail.endTime ? detail.endTime.split(':') : [];
    const isNextDay = getTime.isNextDay(detail.startTime,detail.endTime)
    await this.promiseState({
      startTimeArr,
      endTimeArr,
      isNextDay
    });

    // 现在的时间
    const date = await getTime.getTime()
    const nowHour = date.hour;
    // 选择的时间
    const selectDate = this.state.selectDate;
    if (inType !== 'date') {
      if (!this.state.showSelectDate) {
        // 必须先选择开始日期
        wxApi.showToast({
          icon: 'none',
          title: '请选择开始日期',
        });

        return;
      }
      if (inType === 'time') {
        // 选择开始时间
        if (date.year == selectDate.year && date.month == selectDate.month && date.day == selectDate.day) {
          // 跨天
          if(isNextDay){
            const endTimeHour = Number(endTimeArr[0]) + 24;
            if (endTimeHour - nowHour < leastTime) {
              // 当前时间距离最晚服务时间超出最少可预定时长
              wxApi.showToast({
                icon: 'none',
                title: '日期时间无法满足最少预定时长'
              })
              return
            }
            // if(nowHour < endTimeArr[0] || (nowHour == endTimeArr[0] && date.minute < endTimeArr[1])){
            //   nowHour = nowHour+24
            // }
            // 如果是今天，并且当前小时是起始时间的小时，设置返回分钟
            if (startTimeArr[0] == nowHour) {
              // 现在的时间是起始时间，则比较分钟
              if (startTimeArr[1] != '00') {
                // 如果分钟数不是0，则设置小时为现在的小时
                this.setHours(nowHour, endTimeHour,date)
              } else {
                // 如果分钟数是0，则设置小时为后一个小时
                this.setHours(nowHour + 1, endTimeHour,date)
              }
            } else {
              // 如果不是起始时间
              if (startTimeArr[0] < nowHour) {
                // 现在时间大于起始时间，则设置时间为后一个小时
                this.setHours(nowHour + 1, endTimeHour,date)
              } else {
                // 现在时间小于起始时间，则设置为起始时间
                this.setHours(nowHour, endTimeHour,date)
              }
            }
          }else{
            // 如果选择的日期是今天，先计算剩余服务时间是否足够接收预定
            if (Number(nowHour) > Number(endTimeArr[0])) {
              // 当前时间超出最晚服务时间
              wxApi.showToast({
                icon: 'none',
                title: '日期时间超出最晚预定时间',
              });

              return;
            }
            if (endTimeArr[0] - nowHour < leastTime) {
              // 当前时间距离最晚服务时间超出最少可预定时长
              wxApi.showToast({
                icon: 'none',
                title: '日期时间无法满足最少预定时长',
              });

              return;
            }
            // 如果是今天，并且当前小时是起始时间的小时，设置返回分钟
            if (startTimeArr[0] == nowHour) {
              // 现在的时间是起始时间，则比较分钟
              if (startTimeArr[1] != '00') {
                // 如果分钟数不是0，则设置小时为现在的小时
                this.setHours(nowHour, endTimeArr[0]);
              } else {
                // 如果分钟数是0，则设置小时为后一个小时
                this.setHours(nowHour + 1, endTimeArr[0]);
              }
            } else {
              // 如果不是起始时间
              if (startTimeArr[0] < nowHour) {
                // 现在时间大于起始时间，则设置时间为后一个小时
                this.setHours(nowHour + 1, endTimeArr[0]);
              } else {
                // 现在时间小于起始时间，则设置为起始时间
                this.setHours(startTimeArr[0], endTimeArr[0]);
              }
            }
          }
        } else {
          // 如果不是今天
          // 跨天
          if(isNextDay){
            this.setHours(startTimeArr[0], Number(endTimeArr[0])+24)
          }else{
            this.setHours(startTimeArr[0], endTimeArr[0])
          }
        }
      }
      // 选择预计时长
      if (inType === 'duration') {
        if (!this.state.showSelectTime) {
          // 必须先选择开始时间
          wxApi.showToast({
            icon: 'none',
            title: '请选择开始时间',
          });

          return;
        }
        // 选择的时间
        const timeArr = this.state.showSelectTime.split(':');
        let timeArrHour = Number(timeArr[0]);
        // 跨天
        if(isNextDay){
          const endHour = Number(endTimeArr[0]) + 24
          // 开始时间=结束时间代表24小时
          if(detail.startTime == detail.endTime){
            this.setDuration(leastTime, 24)
          }else{
            if(timeArrHour<startTimeArr[0]){
              timeArrHour = timeArrHour+24
            }
            const selectMinute = Number(timeArr[1]);
            const endMinute = Number(endTimeArr[1]);
            if(selectMinute < endMinute || selectMinute == endMinute){
              this.setDuration(leastTime, endHour - timeArrHour + 1)
            }else{
              this.setDuration(leastTime, endHour - timeArrHour)
            }
          }
        }else{
          if (Number(endTimeArr[0]) - timeArrHour == leastTime && timeArr[1] != '00' && (Number(timeArr[1]) > Number(endTimeArr[1]+1))) {
            // 如果选择的时候减去结束时间时长刚好等于最小时长，则进行下面的判断
            // 如果不是整点则比较分钟，分钟小于正常计算，分钟大于不计算
            wxApi.showToast({
              icon: 'none',
              title: '日期时间无法满足最少预定时长'
            })
            return
          }
          const selectMinute = Number(timeArr[1]);
          const endMinute = Number(endTimeArr[1]);
          if(selectMinute < endMinute || selectMinute == endMinute){
            this.setDuration(leastTime, Number(endTimeArr[0]) - timeArrHour + 1)
          }else{
            this.setDuration(leastTime, Number(endTimeArr[0]) - timeArrHour)
          }
        }
      }
      // 设置预定天数
      const days:any[] = [];
      for (let i = leastTime; i <= 30; i++) {
        days.push(i);
      }
      // 设置预定月数
      const months = [];
      for (let i = leastTime; i <= 12; i++) {
        months.push(i);
      }
      // 设置预定年数
      const years = [];
      for (let i = leastTime; i <= 5; i++) {
        years.push(i);
      }
      // inType --- 时长duration 天数days 月数months 年数years
      let singleArr = [];
      switch (inType) {
        case 'duration':
          singleArr = this.state.durations;
          break;
        case 'days':
          singleArr = days;
          break;
        case 'months':
          singleArr = months;
          break;
        case 'years':
          singleArr = years;
      }

      this.setState({
        singleArr,
      });
    } else {
      // 点击开始日期查询锁场日期
      await wxApi
        .request({
          url: api.scence.getLockDay,
          loading: true,
          data: {
            reservationId: detail.id,
          },

          // 将接口请求参数转化成单个对应的参数后再传参
        })
        .then((res) => {
          // 只有按小时预定才锁场
          this.setState({
            lockDays: this.state.selectTimeSpecification.specUnit == 1 ? res.data : [],
          });
        });
    }
    // 显示二级弹窗
    this.setState(
      {
        inType,
        showDateSelector: true,
      },

      () => {
        this.inAnimatDialogRef.current.show({
          translateY: 300,
        })
        this.props.onShowInDialog(true)
      }
    );
  }

  closeInDialog=()=>{
    // 关闭二级弹窗
    this.inAnimatDialogRef.current.hide();
    this.props.onShowInDialog(false);
  }

  setHours=async (startTime, endTime,date)=>{
    const {isNextDay,startTimeArr,endTimeArr} = this.state
    // 时间选择器设置小时
    const hours:any[] = [];
    for (let i = Number(startTime); i <= Number(endTime); i++) {
      const h = i%24
      if(!date || (i<24 && i>date.hour) || (i>=24 && h>date.hour)){
        let stringI = h + ''
        if (stringI.length === 1) {
          stringI = `0${stringI}`
        }
        if(isNextDay){
          if(h>=startTimeArr[0] || h<=endTimeArr[0]){
            if(!hours.includes(stringI)){
              hours.push(stringI)
            }
          }
        }else{
          if(!hours.includes(h)){
            hours.push(stringI)
          }
        }
      }
    }
    await this.promiseState({
      hours,
    });
  }

  setDuration=async (startTime, endTime)=>{
    console.log(startTime, endTime);
    // 计算可选时长
    const durations:any[] = [];
    for (let i = Number(startTime); i <= Number(endTime); i++) {
      durations.push(Number(i));
    }
    await this.promiseState({
      durations,
    });
  }

  confirmSelectTime=(info)=>{
    // // 确认选择时间
    this.setState(
      {
        // 如果没有滚动选择时间，默认为第一项
        showSelectTime: info,
        showSelectDuration: '',
        selectDuration: '',
        totalPrice: 0,
      },

      () => {
        this.closeInDialog();
      }
    );
  }

  confirmSelectDate=(info)=>{
    const selectDateArr = this.state.showSelectDate.split('-') || [];
    if (
      info.year == selectDateArr[0] &&
      info.month == selectDateArr[1] &&
      info.day == selectDateArr[2]
    ) {
      this.closeInDialog();
      return;
    }
    const month = String(info.month).length == 1 ? `0${info.month}` : info.month;
    const day = String(info.day).length == 1 ? `0${info.day}` : info.day;
    this.setState(
      {
        // 每次重新选择日期都清空数据
        selectDate: {
          ...info,
        },

        showSelectDate: `${info.year}-${month}-${day}`,
        showSelectTime: '',
        showSelectDuration: '',
        showSelectDays: '',
        showSelectMonths: '',
        showSelectYears: '',
        calcEndTime: '',
        totalPrice: 0,
      },

      () => {
        this.closeInDialog();
      }
    );
  }

  changeSelectorSingle=(e)=>{
    let value:any = null
    if(Array.isArray(e)){
      value = e
    }else{
      value = e.detail.value[0]
    }
    if (this.state.inType === 'duration') {
      // 单项选择器为时长
      this.setState({
        selectDuration: this.state.singleArr[value],
        value:value
      });
    } else if (this.state.inType === 'days') {
      // 单项选择器为天数
      this.setState({
        selectDays: this.state.singleArr[value],
        value:value
      });
    } else if (this.state.inType === 'months') {
      // 单项选择器为月数
      this.setState({
        selectMonths: this.state.singleArr[value],
        value:value
      });
    } else if (this.state.inType === 'years') {
      // 单项选择器为年数
      this.setState({
        selectYears: this.state.singleArr[value],
        value:value
      });
    }
  }

  confirmHandleSingle=()=>{
    const {
      showSelectDate,
      inType,
      showSelectTime,
      selectDuration,
      singleArr,
      selectDays,
      selectMonths,
      selectYears,
      selectSpec,
      isNextDay
    } = this.state;
    const {detail} = this.props
    // 当前选择时间的规格
    const spec = selectSpec.specArr[this.state.selectTimeIndex];
    // 计算价格的系数 duration为小时，非duration为天
    let number = 0;
    if (inType === 'duration') {
      const timeArr = showSelectTime.split(':');
      number = selectDuration || singleArr[0];
      let timeHour:number |string = Number(timeArr[0]) + Number(selectDuration || singleArr[0]);
      let timeMinute
      if(timeArr[1] == '00'){
        timeMinute = 59
        timeHour = timeHour - 1
      }else{
        timeMinute = Number(timeArr[1]) - 1
      }
      // 跨天
      let newSelectDate:Date | string = new Date(showSelectDate)
      let isToday = false; //加上时长后是否第二天
      if(timeHour >= 24){
        timeHour = timeHour%24
        isToday = true
        if(isNextDay){
          newSelectDate.setDate(newSelectDate.getDate()+1)
        }
      }
      newSelectDate = utilDate.format(newSelectDate, 'yyyy-MM-dd');
      timeHour = String(timeHour).length == 1 ? `0${timeHour}` : timeHour;
      timeMinute = String(timeMinute).length == 1 ? `0${timeMinute}` : timeMinute
      let calcEndTime = `${newSelectDate} ${timeHour}:${timeMinute}`
      // let calcEndTimestamp = new Date(calcEndTime).getTime()
      const endTimeArr =  detail.endTime.split(':')
      // 24小时的不做计算
      if(detail.startTime !== detail.endTime){
        // 计算后的时大于服务的时，或时一样但分大于服务的分，则按限定的服务时间
        if(
          (Number(timeHour) > Number(endTimeArr[0]) || 
          (Number(timeHour) == Number(endTimeArr[0]) && Number(timeMinute) > Number(endTimeArr[1]))) || 
          (!isNextDay && isToday)
        ){
          calcEndTime = `${newSelectDate} ${detail.endTime}`
        }
        if(isNextDay && !isToday){
          calcEndTime = `${newSelectDate} ${timeHour}:${timeMinute}`
        }
      }
      this.setState({
        showSelectDuration: selectDuration || singleArr[0],
        calcEndTime: calcEndTime
      });
    } else if (inType === 'days') {
      number = selectDays || singleArr[0];
      this.setState({
        showSelectDays: selectDays || singleArr[0],
        calcEndTime: this.calcAfterDate(selectDays || singleArr[0], showSelectDate),
      });
    } else if (inType === 'months') {
      number = selectMonths || singleArr[0];
      this.setState({
        showSelectMonths: selectMonths || singleArr[0],
        calcEndTime: this.calcAfterDate((selectMonths || singleArr[0]) * 30, showSelectDate),
      });
    } else if (inType === 'years') {
      number = selectYears || singleArr[0];
      this.setState({
        showSelectYears: selectYears || singleArr[0],
        calcEndTime: this.calcAfterDate((selectYears || singleArr[0]) * 365, showSelectDate),
      });
    }
    console.log(inType, number);
    wxApi
      .request({
        url: api.scence.getReservationSpec,
        loading: true,
        data: {
          reservationId: detail.id,
          reservationSpecId: spec.id,
          specifyDate: showSelectDate,
          nums: number,
        },

        // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        this.setState(
          {
            totalPrice: res.data.unitPriceTotal + detail.cashPledge,
          },

          () => {
            this.closeInDialog();
          }
        );
      });
  }

  calcAfterDate=(num, day)=>{
    // 计算多少天后的日期
    const dayArr = day.split('-');
    const newdate = new Date(Number(dayArr[0]), Number(dayArr[1]) - 1, Number(dayArr[2]));

    const newtimems = newdate.getTime() + (num - 1) * 24 * 60 * 60 * 1000;
    newdate.setTime(newtimems);
    const newMonth = newdate.getMonth() + 1;
    const newDay = newdate.getDate();
    const time = `${newdate.getFullYear()}-${String(newMonth).length == 1 ? `0${newMonth}` : `${newMonth}`}-${
      String(newDay).length == 1 ? `0${newDay}` : `${newDay}`
    }`;
    return time;
  }

  onPickerDate=(index)=>{
    this.setState({
      inType:'isCustom',
      pickerDateIndex:index
    },()=>{
      this.inAnimatDialogRef.current.show({
        translateY: 300,
      })
    })
  }

  onClosePIckerDate = () =>{
    this.inAnimatDialogRef.current.hide();
  }

  onConfirmSelectDate = (info) =>{
    console.log(info,this.state.pickerDateIndex)
    const date = `${info.year}-${info.month}-${info.day}`
    const formExt:any = this.state.formExt;
    formExt[this.state.pickerDateIndex].value = Object.prototype.toString.call(date) === '[object String]' ? date.trim() : date;
    this.setState({
      formExt,
    },()=>{
      this.inAnimatDialogRef.current.hide();
    });

  }

  render() {
    const {
      value,
      tmpStyle,
      selectSpec,
      currentHeader,
      type,
      selectTimeIndex,
      selectSpecIndex,
      showSelectDate,
      showSelectTime,
      selectTimeSpecification,
      showSelectDuration,
      showSelectDays,
      showSelectMonths,
      showSelectYears,
      calcEndTime,
      formExt,
      totalPrice,
      hours,
      minutes,
      startTimeArr,
      endTimeArr,
      leastTime,
      isNextDay,
      inType,
      lockDays,
      selectDate,
      showDateSelector,
      singleArr,
    } = this.state;
    const animationData = this.animation
    const {detail,isDetail,specVal}  = this.props
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-sscr-ReserveDialog"
        className="wk-sscr-ReserveDialog"
      >
        <ScrollView
          className="dialog"
          animation={animationData}
          scrollY
          onClick={this.doNotCloseDialog}
        >
          <ScrollView className="dialog-content" scrollY>
            {isDetail && (
              <View
                style={_safe_style_(
                  "background-image: url('https://front-end-1302979015.file.myqcloud.com/images/c/images/sence-reserve-dialog-header-pic.png');background-color: " +
                    tmpStyle.btnColor
                )}
                className="dialog-content-detail-title"
              >
                <View>
                  <Text className="text">{'面积：' + detail.acreage + 'm²'}</Text>
                  <Text className="text">{'规格：' + selectSpec.spec}</Text>
                </View>
                <View>
                  <Text className="text">{'押金金额：' + (detail.cashPledge ? '￥' + detail.cashPledge / 100 : '免押金')}</Text>
                  <Text className="text">{currentHeader.minimum + type + '起定'}</Text>
                </View>
                <View>
                  <Text className="text">{'计价单位：' + type}</Text>
                  <Text className="text">
                    {detail.reservationDays === 0 ? '无需提前预定' : '需提前' + detail.reservationDays + '天预定'}
                  </Text>
                </View>
              </View>
            )}

            <View className="padding-box">
              {!isDetail && (
                <View className="dialog-content-title">
                  <Image
                    className="dialog-content-title-img"
                    mode="aspectFill"
                    src={filters.imgListFormat(detail.imgUrl)}
                  ></Image>
                  <View className="dialog-content-title-info">
                    <View className="list-item-title">{detail.name}</View>
                    <View className="list-item-tag">
                      {detail.labelArr &&
                        detail.labelArr.map((el, index) => {
                          return (
                            <View
                              className="list-item-tag-item"
                              key={index}
                              style={_safe_style_(
                                'border-color: ' +
                                  tmpStyle.btnColor +
                                  '; background-color: filters.handleOpacity(' +
                                  tmpStyle.btnColor +
                                  ', 0.04); color: ' +
                                  tmpStyle.btnColor
                              )}
                            >
                              {el}
                            </View>
                          );
                        })}
                    </View>
                    {detail.detailAddress && (
                      <View className="list-item-site">
                        <Image
                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/address.svg"
                          className="list-item-site-logo"
                        ></Image>
                        <Text className="list-item-site-text">{detail.detailAddress}</Text>
                      </View>
                    )}

                    <View className="list-item-info">
                      {/* <Text className="info-price">
                        {'￥' + filters.moneyFilter(selectSpec && Array.isArray(selectSpec.specArr) ? selectSpec.specArr[selectTimeIndex].unitPrice || 0 : 0, true)}
                      </Text> */}
                      <Text className="info-sale">{detail.vitrualQty + detail.sales + '人预定'}</Text>
                    </View>
                  </View>
                </View>
              )}

              <View className="dialog-content-form">
                <View className="form-item">
                  <View className="form-title">规格</View>
                  <View className="specification-info">
                    {specVal &&
                      specVal.map((specValItem, index) => {
                        return (
                          <View
                            className="specification-info-item"
                            key={index}
                            style={_safe_style_(
                              'background-color: ' +
                                (selectSpecIndex === index ? tmpStyle.btnColor : '#f5f5f5') +
                                ';color: ' +
                                (selectSpecIndex === index ? '#fff' : '#666')
                            )}
                            onClick={()=>this.selectSpecification(specValItem,index)}
                          >
                            {specValItem.spec}
                          </View>
                        );
                      })}
                  </View>
                </View>
                <View className="form-item">
                  <View className="form-title">时间</View>
                  <View className="time-info">
                    {selectSpec.specArr &&
                      selectSpec.specArr.map((timeItem, index) => {
                        return (
                          <View
                            className="time-info-item"
                            key={index}
                            onClick={()=>this.selectTime(timeItem,index)}
                            style={_safe_style_(
                              'background-color: ' +
                                (selectTimeIndex === index ? tmpStyle.btnColor : '#F5F5F5') +
                                '; color: ' +
                                (selectTimeIndex === index ? '#fff' : '#666')
                            )}
                          >
                            {timeItem.specName}
                          </View>
                        );
                      })}
                  </View>
                </View>
                <View className="form-item">
                  <View className="form-title">开始日期</View>
                  <View
                    className="ipt"
                    onClick={()=>this.showInDialog('date')}
                  >
                    <Text className="arrow-text">{showSelectDate}</Text>
                    <Image
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                      className="arrow-image"
                    ></Image>
                  </View>
                </View>
                {selectTimeSpecification.specUnit == '1' && (
                  <View className="form-item">
                    <View className="form-title">开始时间</View>
                    <View
                      className="ipt"
                      onClick={()=>this.showInDialog('time')}
                    >
                      <Text className="arrow-text">{showSelectTime}</Text>
                      <Image
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="arrow-image"
                      ></Image>
                    </View>
                  </View>
                )}

                {selectTimeSpecification.specUnit == '1' && (
                  <View className="form-item">
                    <View className="form-title">预定时长</View>
                    <View
                      className="ipt"
                      onClick={()=>this.showInDialog('duration')}
                    >
                      <Text className="arrow-text">{showSelectDuration}</Text>
                      <Image
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="arrow-image"
                      ></Image>
                    </View>
                  </View>
                )}

                {selectTimeSpecification.specUnit == '2' && (
                  <View className="form-item">
                    <View className="form-title">预定天数</View>
                    <View
                      className="ipt"
                      onClick={()=>this.showInDialog('days')}
                    >
                      <Text className="arrow-text">{showSelectDays}</Text>
                      <Image
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="arrow-image"
                      ></Image>
                    </View>
                  </View>
                )}

                {selectTimeSpecification.specUnit == '4' && (
                  <View className="form-item">
                    <View className="form-title">预定月数</View>
                    <View
                      className="ipt"
                      onClick={()=>this.showInDialog('months')}
                    >
                      <Text className="arrow-text">{showSelectMonths}</Text>
                      <Image
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="arrow-image"
                      ></Image>
                    </View>
                  </View>
                )}

                {selectTimeSpecification.specUnit == '5' && (
                  <View className="form-item">
                    <View className="form-title">预定年数</View>
                    <View
                      className="ipt"
                      onClick={()=>this.showInDialog('years')}
                    >
                      <Text className="arrow-text">{showSelectYears}</Text>
                      <Image
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="arrow-image"
                      ></Image>
                    </View>
                  </View>
                )}

                <View className="form-title form-item">{'结束时间：' + calcEndTime}</View>
                {formExt &&
                  formExt.map((item:any, index) => {
                    return (
                      <View key={index}>
                        <View className="custom-form form-item">
                          {item.type === 'sex' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'sex' && (
                            <View className="form-sex">
                              <View
                                onClick={()=>this.chooseGender({ value: '男', index: index })}
                                className={'form-sex-item ' + (item.value === '男' ? 'selected' : '')}
                              >
                                <Image
                                  mode="aspectFit"
                                  src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/male.png"
                                  className="gender-img"
                                ></Image>
                                <View>男</View>
                              </View>
                              <View
                                onClick={()=>this.chooseGender({ value: '女', index: index })}
                                className={'form-sex-item ' + (item.value === '女' ? 'selected' : '')}
                              >
                                <Image
                                  mode="aspectFit"
                                  src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/female.png"
                                  className="gender-img"
                                ></Image>
                                <View>女</View>
                              </View>
                            </View>
                          )}

                          {/*  单选框  */}
                          {item.type === 'radio' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'radio' && (
                            <RadioGroup
                              className="phone-input radio-box"
                              name="radio-group"
                              onChange={(e)=>this.inputValue(index,e)}
                            >
                              {item.options &&
                                item.options.map((optionItem,index) => {
                                  return (
                                    <Label className="radio-box-item" key={index}>
                                      <Radio value={optionItem} color={tmpStyle.btnColor}></Radio>
                                      {optionItem}
                                    </Label>
                                  );
                                })}
                            </RadioGroup>
                          )}

                          {/*  多选框  */}
                          {item.type === 'checkbox' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'checkbox' && (
                            <CheckboxGroup
                              className="phone-input checkout-box"
                              name="radio-group"
                              onChange={(e)=>this.bindValue(index,item,e)}
                            >
                              {item.options &&
                                item.options.map((child, index) => {
                                  return (
                                    <Label className="checkout-box-item" key={index}>
                                      <Checkbox value={child.value} checked={child.checked}></Checkbox>
                                      {child.value}
                                    </Label>
                                  );
                                })}
                            </CheckboxGroup>
                          )}

                          {/*  时间选择器  */}
                          {item.type === 'date' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'date' && (
                            // <Picker
                            //   className="ipt"
                            //   mode="date"
                            //   start={item.dateRange?.[0]}
                            //   end={item.dateRange?.[1]}
                            //   value={item.dateRange}
                            //   onChange={(e)=>this.inputValue(index,e)}
                            // >
                            //   {item.value ? (
                            //     <Text className="picker">{item.value}</Text>
                            //   ) : (
                            //     <Text className="picker" style={_safe_style_('color: #888;')}>
                            //       请选择
                            //     </Text>
                            //   )}

                            //   <Image
                            //     className="arrow-image"
                            //     src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                            //   ></Image>
                            // </Picker>
                            <View
                              className="ipt"
                              onClick={()=>this.onPickerDate(index)}
                            >
                              <Text className="arrow-text">{this.state.formExt[this.state.pickerDateIndex].value}</Text>
                              <Image
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                className="arrow-image"
                              ></Image>
                            </View>
                          )}

                          {/*  文本类型输入框  */}
                          {(item.type === 'text' || item.type === 'email') && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {(item.type === 'text' || item.type === 'email') && (
                            <Input
                              className="ipt"
                              placeholder="请输入"
                              value={item.value}
                              onBlur={(e)=>this.inputValue(index,e)}
                            ></Input>
                          )}

                          {/*  数字类型单行输入框  */}
                          {item.type === 'phone' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'phone' && (
                            <Input
                              className="ipt"
                              placeholder="请输入"
                              // type="phone"
                              value={item.value}
                              onBlur={(e)=>this.inputValue(index,e)}
                            ></Input>
                          )}

                          {/*  多行输入框  */}
                          {item.type === 'textarea' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'textarea' && (
                            <View className="ipt-textarea">
                              <Textarea
                                maxlength={-1}
                                placeholder="请输入留言内容"
                                value={item.value}
                                onInput={(e)=>this.inputValue(index,e)}
                              ></Textarea>
                            </View>
                          )}

                          {/*  地址  */}
                          {item.type === 'address' && (
                            <View className={'form-title ' + (item.required ? 'required' : '')}>{item.label}</View>
                          )}

                          {item.type === 'address' && (
                            // RegionPicker></RegionPicker>

                            <RegionPicker
                              className="ipt"
                              options={{index:index}}
                              // mode='region'
                              // onChange={(e)=>this.inputValue(index,e)}
                              onChange={this.onRegionPickerChange}
                            >
                              {item.value ? (
                                <Text className="picker">{item.value}</Text>
                              ) : (
                                <Text className="picker" style={_safe_style_('color: #888;')}>
                                  请选择
                                </Text>
                              )}

                              <Image
                                className="arrow-image"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                              ></Image>
                            </RegionPicker>
                          )}

                          {/*  详细地址  */}
                          {item.type === 'detail-address' && <View className="form-title">{item.label}</View>}

                          {item.type === 'detail-address' && (

                              process.env.TARO_ENV === 'weapp' ?
                              <Textarea
                              className="ipt-textarea"
                              disableDefaultPadding
                              autoHeight
                              placeholder="请输入详细地址"
                              onInput={(e)=>this.inputValue(index,e)}
                              value={item.value}
                              ></Textarea> :
                              <Textarea
                              className="ipt-textarea"
                              placeholder="请输入详细地址"
                              onInput={(e)=>this.inputValue(index,e)}
                              value={item.value}
                            ></Textarea>


                          )}
                        </View>
                      </View>
                    );
                  })}
              </View>
            </View>
          </ScrollView>
          <View className="dialog-bottom">
            {totalPrice !== 0 && (
              <View
                className="dialog-price"
                style={_safe_style_('background-color:#FFF')}
              >
                {'￥' + filters.moneyFilter(totalPrice || 0, true)}
                {detail.cashPledge !==0 && <Text className="dialog-price-pledge">(含押金)</Text>}
              </View>
            )}

            <View
              className="dialog-btn"
              style={_safe_style_('background-color: ' + tmpStyle.btnColor)}
              onClick={()=>this.confirmReserve(false)}
            >
              加入预定
            </View>
            {isDetail && (
              <View
                className="dialog-btn"
                style={_safe_style_('background-color: ' + tmpStyle.btnColor)}
                onClick={()=>this.confirmReserve(true)}
              >
                立即下单
              </View>
            )}
          </View>
        </ScrollView>
        <InAnimatDialog ref = {this.inAnimatDialogRef} id="in-dialog" animClass="in_dialog_box" clickMaskHide={false}>
          <View>
            {inType === 'time' ? (
              <View>
                <TimeSelector
                  hours={hours}
                  minutes={minutes}
                  startTimeArr={startTimeArr}
                  endTimeArr={endTimeArr}
                  leastTime={leastTime}
                  isNextDay={isNextDay}
                  onCancel={this.closeInDialog}
                  onConfirm={this.confirmSelectTime}
                ></TimeSelector>
              </View>
            ) : inType === 'date' ? (
              <View>
                <DateSelector
                  reservationDays={detail.reservationDays || 0}
                  lockDays={lockDays}
                  selectDate={selectDate}
                  showDateSelector={showDateSelector}
                  onCancel={this.closeInDialog}
                  onConfirm={this.confirmSelectDate}
                ></DateSelector>
              </View>
            ) : inType === 'isCustom' ? <View>
              <DateSelector
                  onCancel = {this.onClosePIckerDate}
                  isLock
                  onConfirm = {this.onConfirmSelectDate}
                ></DateSelector>


            </View> : (
              <View>
                <View className="selector-date-head">
                  <View className="selector-cancel" onClick={this.closeInDialog}>
                    取消
                  </View>
                  <View
                    className="selector-confirm"
                    style={_safe_style_('color: ' + tmpStyle.btnColor)}
                    onClick={this.confirmHandleSingle}
                  >
                    确定
                  </View>
                </View>
                {
                  process.env.TARO_ENV === 'weapp' ?
                <PickerView
                  indicatorStyle={{'height': '50px'}}
                  style={_safe_style_('width: 100%; height: 300px;')}
                  onChange={this.changeSelectorSingle}
                  >
                  <PickerViewColumn>
                    {singleArr &&
                      singleArr.map((item:any, index) => {
                        return (
                          <View
                            key={index}
                            style={_safe_style_('height: 50px;line-height: 50px;text-align: center;')}
                          >
                            {item}
                          </View>
                        );
                      })}
                  </PickerViewColumn>
                  </PickerView>:
                <MultiPicker selectedValue={value} onValueChange={this.changeSelectorSingle} className="multi-picker">
                  <PickerComponent indicatorClassName="my-picker-indicator">
                        {singleArr &&
                      singleArr.map((item:any, index) => {
                        return (
                          <PickerComponent.Item key={index} className="my-picker-view-item" value={index}>{item}</PickerComponent.Item>
                        );
                      })}
                  </PickerComponent>
                </MultiPicker>
                }

              </View>
            )}
          </View>
        </InAnimatDialog>
      </View>
    );
  }
}


export default ReserveDialog;
