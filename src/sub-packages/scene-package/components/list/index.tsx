import { _fixme_with_dataset_ } from 'wk-taro-platform';
import { View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../wxat-common/utils/money.wxs.js';
import template from '../../../../wxat-common/utils/template.js';

import './index.scss';

interface IProps{
  list:any[];
  fromList:boolean
}
interface IState{
  tmpStyle:any;
  list:any[];
}
class List extends React.Component<IProps,IState>{
  static defaultProps = {
    list:[],
    fromList:false
  }
  state={
    tmpStyle: {},
    list:[]
  }

  componentDidMount() {
    this.getTemplateStyle();
    this.setState({
      list:this.props.list
    })
  }
  componentDidUpdate(prevProps, prevState){
    if(this.props.list !== prevProps.list){
      this.setState({
        list:this.props.list
      })
    }
  }
  //获取模板配置
  getTemplateStyle=()=> {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  showDetail=(item,index)=>{

    // let isOpen = item.isOpen;
    // let current = `list[${index}].isOpen`;
    const list:any[] = this.state.list
    item.isOpen = !item.isOpen
    list.splice(index,1,item)
    this.setState({
      list:list
    });
  }
  render() {
    const { list } = this.state;
    const {fromList} = this.props
    console.log(list)
    return (
      <View data-scoped="wk-sscl-List" className="wk-sscl-List wrap">
        {list &&
          list.map((item:any, index) => {
            return (
              <View className="list-wrap" key={index}>
                <View className="item-top">
                  <Image className="image" src={filters.imgListFormat(item.imgUrl)}></Image>
                  <View className="item-detail-right">
                    <Text className="text">{item.reservationName || item.size || item.name}</Text>
                    <Text className="text">{'规格：' + (item.spec || item.specInfo.spec)}</Text>
                    <Text className="text">
                      {'时间：' +
                        (item.createTime ? item.createTime : '') +
                        ' ' +
                        (item.showReserveStartTime ? item.showReserveStartTime : '') +
                        (item.showReserveEndTime ? '-' : '') +
                        '  ' +
                        (item.showReserveEndTime ? item.showReserveEndTime : '') +
                        (item.differText ? '（共' + item.differText + '）' : '')}
                    </Text>
                    <View className="money">
                      {!fromList && (
                        <Text>
                          {'￥' +
                            (item.unitPrice ? filters.moneyFilter(item.unitPrice || 0, true) : item.totalPrice / 100)}
                        </Text>
                      )}

                      {fromList && (
                        <Text>
                          {'￥' +
                            (item.minSalePrice == item.maxSalePrice
                              ? filters.moneyFilter(item.minSalePrice || 0, true)
                              : filters.moneyFilter(item.minSalePrice || 0, true) +
                                '-' +
                                filters.moneyFilter(item.maxSalePrice || 0, true))}
                        </Text>
                      )}

                      {!item.isOpen ? (
                        <Image
                        className="image"
                          onClick={()=>this.showDetail(item,index)}
                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        ></Image>
                      ) : (
                        <Image
                        className="image"
                        onClick={()=>this.showDetail(item,index)}
                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/top-arrow.png"
                        ></Image>
                      )}
                    </View>
                  </View>
                </View>
                {item.formExtParse || item.formExt ? (
                  <View
                    className={
                      'item-bottom ' +
                      (item.isOpen === true ? 'open' : '') +
                      ' ' +
                      (item.isOpen === false ? 'close' : '')
                    }
                  >
                    {(item.formExtParse || item.formExt) &&
                      (item.formExtParse || item.formExt).map((el, index) => {
                        return <Text className="text" key={el.label}>{el.label + '：' + ( el.value? el.value:'')}</Text>;
                      })}
                  </View>
                ) : (
                  <View
                    className={
                      'item-bottom ' +
                      (item.isOpen === true ? 'open' : '') +
                      ' ' +
                      (item.isOpen === false ? 'close' : '')
                    }
                  >
                    {item.formExtParse &&
                      item.formExtParse.map((item, index) => {
                        return <Text className="text" key={item.item.label}>{item.label + '：' + ( item.value? item.value:'')}</Text>;
                      })}
                  </View>
                )}
              </View>
            );
          })}
      </View>
    );
  }
}


export default List;
