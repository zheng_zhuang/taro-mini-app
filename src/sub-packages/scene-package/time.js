import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';

// 获取当前时间
async function getTime() {
  const date = await getNowTime()
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  return {
    year,
    month,
    day,
    hour,
    minute,
  };
}

// 获取服务器时间
function getNowTime(){
  return wxApi.request({
    url: api.livingService.getNowDate,
  }).then(timeRes=>{
    return new Date(timeRes.data)
  }).catch(err=>{
    return new Date()
  })
}

// 是否跨天
function isNextDay(startTime,endTime){
  var startArr = startTime.split(":");
  var endArr = endTime.split(":");
  if(startTime == endTime){
    return true
  }else{
    if(startArr[0] == endArr[0]){
      return startArr[1] > endArr[1]
    }else if(startArr[0] < endArr[0]){
      return false
    }else{
      return true
    }
  }
}

export default {
  getTime,
  isNextDay
};
