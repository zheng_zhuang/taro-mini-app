import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { NOOP } from '@/wxat-common/utils/noop';
import filters from '@/wxat-common/utils/money.wxs';
import './index.scss';

export interface GoodsProps {
  goodsInfoList?: any[];
  address: boolean;
  tmpStyle: any;
  isExpress: boolean;
  freightType: Record<string, any>;
  onIncr?: (barcode: string) => void;
  onDecr?: (barcode: string) => void;
}

class Goods extends React.Component<GoodsProps> {
  static defaultProps = {
    goodsInfoList: null,
    address: {},
    tmpStyle: {},
  };

  render() {
    const { goodsInfoList, address, tmpStyle, onIncr = NOOP, onDecr = NOOP, isExpress, freightType } = this.props;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-ppg-Goods' className='wk-ppg-Goods'>
        <View className='goods-list'>
          {(goodsInfoList || []).map((goodsInfo, index) => {
            return (
              <View
                className='a-goods'
                style={_safe_style_('border-top: ' + (index > 0 ? '1px solid #E2E5EB' : ''))}
                key={index}
              >
                <View className='img-box'>
                  <Image
                    src={goodsInfo.pic || 'http://img0.imgtn.bdimg.com/it/u=2174541640,3479780226&fm=26&gp=0.jpg'}
                    mode='aspectFill'
                    className='img'
                  ></Image>
                  {!!((goodsInfo.itemStock < goodsInfo.itemCount || !goodsInfo.isShelf) && !address) && (
                    <View className='low-stock'>
                      {goodsInfo.isShelf ? (
                        <View className='low-label'>{goodsInfo.itemStock === 0 ? '缺货' : '库存不足'}</View>
                      ) : (
                        <View className='low-label' style={_safe_style_('width: 80rpx')}>
                          已下架
                        </View>
                      )}
                    </View>
                  )}
                </View>
                <View className='goods-info-box'>
                  <View className='goods-name-box'>
                    <View className='goods-name limit-line'>
                      {!!goodsInfo.drugType && (
                        <Text className='drug-tag' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                          处方药
                        </Text>
                      )}

                      {goodsInfo.name}
                    </View>
                    <View className='goods-label limit-line'>{goodsInfo.skuTreeNames || ''}</View>
                  </View>
                  <View>
                    {!!(goodsInfo.freightType === freightType.freightCollect && isExpress) && (
                      <View className='goods-label freight-collect'>运费到付</View>
                    )}

                    <View className='price-stock-box'>
                      <View className='goods-price'>{'¥ ' + filters.moneyFilter(goodsInfo.salePrice, true)}</View>
                      {goodsInfo.itemStock >= goodsInfo.itemCount && (
                        <View className='goods-num'>{'x ' + goodsInfo.itemCount}</View>
                      )}

                      {goodsInfo.itemStock < goodsInfo.itemCount && (
                        <View className='remain-stock'>
                          剩余库存：
                          <Text style={{ color: '#FB4938' }}>{goodsInfo.itemStock}</Text>
                        </View>
                      )}

                      {!!(
                        goodsInfo.itemStock < goodsInfo.itemCount &&
                        goodsInfo.itemStock !== 0 &&
                        goodsInfo.isShelf
                      ) && (
                        <View className='num-box'>
                          <View
                            className={'num-decrease left-radius ' + (goodsInfo.itemCount == 0 ? 'num-disable' : '')}
                            onClick={() => onDecr(goodsInfo.barcode)}
                          >
                            -
                          </View>
                          <View className='num-input'>
                            <Input className='input-comp' type='number' value={goodsInfo.itemCount} disabled></Input>
                          </View>
                          <View
                            className='num-increase right-radius'
                            onClick={_fixme_with_dataset_(() => onIncr(goodsInfo.barcode), {
                              index: goodsInfo.barcode,
                            })}
                          >
                            +
                          </View>
                        </View>
                      )}
                    </View>
                    {!!(goodsInfo.wxItem && goodsInfo.wxItem.frontMoneyItem) && (
                      <View className='front-money'>
                        {'¥' +
                          filters.moneyFilter(
                            goodsInfo.sku ? goodsInfo.sku.frontMoney : goodsInfo.wxItem.frontMoney,
                            true
                          ) +
                          '（定金）'}
                      </View>
                    )}
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}

export default Goods;
