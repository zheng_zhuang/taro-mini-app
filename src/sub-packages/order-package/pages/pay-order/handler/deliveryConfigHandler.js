import store from '@/store/index';
import deliveryWay from '@/wxat-common/constants/delivery-way';

export default {
  //合并品牌及门店物流设置
  mergeDeliveryConfig(currentStore, choosed) {
    const state = store.getState();
    let { supportLocal, supportExpress, supportPickUp } = state.base.appInfo,
      choosedDelivery = deliveryWay.EXPRESS.value;

    supportLocal = currentStore.intraCityFb === 1 ? 0 : supportLocal;
    supportExpress = currentStore.expressFb === 1 ? 0 : supportExpress;
    supportPickUp = currentStore.selfPickUpFb === 1 ? 0 : supportPickUp;

    //如果快递未开启，显示自提
    if (!supportExpress) {
      choosedDelivery = deliveryWay.SELF_DELIVERY.value;
    }

    //如果自提未开启&同城开启，显示同城
    if (!supportPickUp && supportLocal) {
      choosedDelivery = deliveryWay.CITY_DELIVERY.value;
    }

    if (choosed) {
      choosedDelivery = supportPickUp ? choosed : choosedDelivery;
    }

    return {
      supportDelivery: {
        supportLocal,
        supportExpress,
        supportPickUp,
      },
      choosedDelivery,
    };
  },
};
