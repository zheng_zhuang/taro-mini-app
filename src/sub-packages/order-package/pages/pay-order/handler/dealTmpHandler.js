export default {
  parseDeal(dealContent, config) {
    let cpDealContent = dealContent;
    if (config && dealContent) {
      Object.keys(config).forEach((key) => {
        cpDealContent = cpDealContent.replace('${' + key + '}', config[key]);
      });
    }
    return cpDealContent;
  },
};
