import itemTplMap from './itemTplMap';
import deliveryWay from '../../../../../wxat-common/constants/delivery-way';
import store from '../../../../../store/index';
import goodsTypeEnum from '../../../../../wxat-common/constants/goodsTypeEnum';

export default {
  init(config) {
    const state = store.getState();
    const {option, goodsInfoList, deliveryConfig} = config;
    const isScan = option.isScan || false;
    let isGiftActivity = false;
    let payDetails = [];
    let params = {};
    let activityId = null;
    let groupNo = null;
    let seckillNo = null;
    let infoTplName = 'goods-tpl';
    let isNeedPay = true;
    let isFrontMoneyItem = false;
    const storeInfo = state.base.currentStore;
    const supportDelivery = deliveryConfig.supportDelivery;

    if (option.payDetails && option.payDetails !== 'undefined') {
      // payDetails = JSON.parse(option.payDetails);
      payDetails = JSON.parse(decodeURIComponent(option.payDetails));
      isGiftActivity = !!payDetails.find((item) => {
        return item.name === 'giftActivity';
      });
    } else {
      payDetails = [
        {
          name: 'gift-card',
        },
        {
          name: 'coupon',
        },
        {
          name: 'redpacke',
        },
        {
          name: 'discount',
        },
        {
          name: 'freight',
        },
      ];
    }

    if (option.params && option.payDetails !== 'undefined') {
      console.log("option.params",option.params)
      params = JSON.parse(decodeURIComponent(option.params));

      // 获取上一个页面传入的订单参数
      if (params.orderRequestPromDTO) {
        activityId = params.orderRequestPromDTO.activityId || null;
        groupNo = params.orderRequestPromDTO.groupNo || null;
        seckillNo = params.orderRequestPromDTO.secKillNo || null;
      }
    }

    if (goodsInfoList && goodsInfoList.length > 0) {
      const goodsItem = goodsInfoList[0];

      infoTplName = itemTplMap[goodsItem.itemType] || 'goods-tpl';

      const finded = goodsInfoList.find((item) => !item.noNeedPay);
      isNeedPay = !!finded;
      isFrontMoneyItem = goodsItem.wxItem && goodsItem.wxItem.frontMoneyItem;
    }

    let choosedDelivery = deliveryConfig.choosedDelivery;

    /*快递方式：扫码购和定金商品都默认选自提*/
    if (state.base.appInfo && supportDelivery.supportPickUp && (!!option.isScan || isFrontMoneyItem)) {
      choosedDelivery = deliveryWay.SELF_DELIVERY.value;
    }

    //如果是订购配送方式为提交审核时的配送方式
    if (infoTplName === 'micro-tpl') {
      if (goodsInfoList[0].expressType == deliveryWay.SELF_DELIVERY.value) {
        choosedDelivery = deliveryWay.SELF_DELIVERY.value;
      } else {
        choosedDelivery = deliveryWay.EXPRESS.value;
      }
    }

    if (goodsInfoList && goodsInfoList[0]?.itemType === goodsTypeEnum.CARDPACK.value) {
      choosedDelivery = deliveryWay.SELF_DELIVERY.value;
    }

    return {
      isFrontMoneyItem,
      isScan,
      isGiftActivity,
      choosedDelivery,
      storeInfo,
      supportDelivery,
      payDetails,
      goodsInfoList,
      infoTplName,
      isNeedPay,
      params,
      activityId,
      groupNo,
      seckillNo
    };
  },
};
