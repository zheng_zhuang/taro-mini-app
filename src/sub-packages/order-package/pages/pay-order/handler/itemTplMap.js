import goodsTypeEnum from '../../../../../wxat-common/constants/goodsTypeEnum.js';

export default {
  [goodsTypeEnum.PRODUCT.value]: 'goods-tpl',
  [goodsTypeEnum.SERVER.value]: 'serve-tpl',
  [goodsTypeEnum.MICRO_ORDER.value]: 'micro-tpl'
};
