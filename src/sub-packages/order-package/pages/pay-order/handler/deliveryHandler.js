import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import deliveryWay from '../../../../../wxat-common/constants/delivery-way.js';

export default {
  handleDeliveryChanged (data) {
    const { choosedDelivery, address, infoTplName } = data;
    return new Promise((resolve, reject) => {
      //如果是快递或者同城配送查询收货地址
      if (choosedDelivery === deliveryWay.EXPRESS.value || choosedDelivery === deliveryWay.CITY_DELIVERY.value) {
        if (address) {
          resolve();
        } else {
          return wxApi
            .request({
              url: api.address.list,
              quite: true,
              loading: false,
            })
            .then(
              (res) => {
                const data = res.data || [];
                let address = null;
                if (data.length) {
                  address = data[0];
                  const finded = data.find((address) => address.isDefault === 1);
                  if (finded) {
                    address = finded;
                  }
                }
                if (address || infoTplName !== "goods-tpl") {
                  resolve(address);
                } else {
                  reject({
                    msg: '请选择收货地址',
                  });
                }
              },
              (error) => {
                reject({
                  msg: error && error.errorMessage ? error.errorMessage : '获取收货地址失败',
                });
              }
            );
        }
      } else {
        resolve();
      }
    });
  },
};
