import wxApi from '../../../../../wxat-common/utils/wxApi';
import deliveryWay from '../../../../../wxat-common/constants/delivery-way.js';
import api from '../../../../../wxat-common/api/index.js';
import util from '../../../../../wxat-common/utils/util.js';

export default {
  //获取优惠信息列表
  fetchPreferentialList(data) {
    const { isGiftActivity, infoTplName, choosedDelivery, curAddressData, currentStore } = data;
    return new Promise((resolve, reject) => {
      //赠品不参与优惠活动
      if (isGiftActivity) {
        resolve();
      } else {
        const reqData = this.assembleRequestParams(data);
        //发货类型 获取优惠券，需要传发货类型
        if (infoTplName === 'goods-tpl' || infoTplName === 'micro-tpl') {
          reqData.expressType = choosedDelivery;
        }
        if (
          choosedDelivery === deliveryWay.EXPRESS.value &&
          (infoTplName === 'goods-tpl' || infoTplName === 'micro-tpl')
        ) {
          reqData.userAddressId = curAddressData.id;
        } else if (choosedDelivery === deliveryWay.SELF_DELIVERY.value) {
          reqData.storeId = currentStore.id;
        }
        return wxApi
          .request({
            url: api.order.coupons,
            quite: true,
            loading: false,
            data: reqData,
          })
          .then(
            (res) => {
              const coupons = res.data;
              const totalGoodsPrice = this.getTotalGoodsPrice(data);
              let curCoupon = {
                maxDiscountFee: 0,
              };
              if (!!coupons.couponSet) {
                coupons.couponSet.forEach((item) => {
                  item.couponMinimumFee =
                    item.minimumFee === 0 ? '无门槛' : '满' + parseFloat((item.minimumFee / 100).toFixed(2));
                  switch (item.couponCategory) {
                    //满减券
                    case 0:
                      item.couponDiscountFee = '减￥' + parseFloat((item.discountFee / 100).toFixed(2));
                      if (item.status && curCoupon.maxDiscountFee < item.discountFee) {
                        curCoupon = item;
                        curCoupon.maxDiscountFee = item.discountFee;
                      }
                      break;
                    //  运费券
                    case 1:
                      item.couponDiscountFee =
                        item.discountFee === 0 ? '免运费' : '减￥' + parseFloat((item.discountFee / 100).toFixed(2));
                      if (item.status && curCoupon.maxDiscountFee < item.discountFee) {
                        curCoupon = item;
                        curCoupon.maxDiscountFee = item.discountFee;
                      }
                      break;
                    //  折扣券
                    case 2:
                      item.couponDiscountFee = '折扣券: ' + parseFloat((item.discountFee / 10).toFixed(1)) + '折';
                      const totalDiscount = totalGoodsPrice - (totalGoodsPrice * item.discountFee) / 100;
                      if (item.status && curCoupon.maxDiscountFee < totalDiscount) {
                        curCoupon = item;
                        item.maxDiscountFee = totalDiscount;
                      }
                      break;
                  }
                });
              }
              //默认选择礼品卡

              //默认选择红包优惠
              let curRedPacke = {
                luckMoneyFee: 0,
              };
              const redPacketSet = coupons.redPacketSet;
              if (!!redPacketSet) {
                redPacketSet.forEach((item) => {
                  if (item.status && item.luckMoneyFee > curRedPacke.luckMoneyFee) {
                    curRedPacke = item;
                  }
                });
              }
              resolve({
                curRedPacke,
                curCoupon,
                coupons,
              });
            },
            (error) => {
              reject({
                msg: error && error.errorMessage ? error.errorMessage : '获取优惠信息失败',
              });
            }
          );
      }
    });
  },

  getPrice(data) {
    const { choosedDelivery, infoTplName, curAddressData, currentStore } = data;
    const reqData = this.assembleRequestParamsNoFormat(data);
    //发货类型
    // reqData.expressType = choosedDelivery;
    if (
      (choosedDelivery === deliveryWay.EXPRESS.value || choosedDelivery === deliveryWay.CITY_DELIVERY.value) &&
      infoTplName === 'goods-tpl'
    ) {
      // reqData.userAddressId = curAddressData.id;
      reqData.orderRequestDTOS.forEach(i => {
        i.expressType = choosedDelivery;
        i.userAddressId = curAddressData ? curAddressData.id : '';
      })
      // reqData.userAddressId = curAddressData ? curAddressData.id : '';
    } else if (choosedDelivery === deliveryWay.SELF_DELIVERY.value) {
      reqData.storeId = currentStore.id;
    }
    let url = api.order.cal_price_new;
    //订购订单计算价格
    if (infoTplName === 'micro-tpl') {
      url = api.microOrder.cal_price;
      if (choosedDelivery === deliveryWay.EXPRESS.value) {
        reqData.orderRequestDTOS.forEach(i => {
          i.expressType = choosedDelivery;
          i.userAddressId = curAddressData ? curAddressData.id : '';
        })
        // reqData.userAddressId = this.data.curAddressData.id;
      }
    }
    return new Promise((resolve, reject) => {
      wxApi
        .request({
          url: url,
          quite: true,
          loading: false,
          method: 'POST',
          data: reqData,
        })
        .then(
          (res) => {
            const price = res.data;
            resolve({
              price,
            });
          },
          (error) => {
            reject({
              msg: error && error.data && error.data.errorMessage ? error.data.errorMessage : '获取价格失败',
            });
          }
        );
    });
  },
  getTotalGoodsPrice(data) {
    const { goodsInfoList, infoTplName } = data;
    let totalPrice = 0;
    if (goodsInfoList && goodsInfoList.length) {
      goodsInfoList.forEach((goods) => {
        totalPrice += goods.itemCount * goods.salePrice;
      });

      if (infoTplName === 'micro-tpl') {
        totalPrice = goodsInfoList[0].fee * goodsInfoList[0].num;
      }
    }
    return totalPrice;
  },
  //组装请求参数
  assembleRequestParams(data) {
    const { infoTplName, goodsInfoList, payDetails, curGiftCard, curCoupon, curRedPacke, serveCard, params } = data;
    const cpParams = {
      ...params,
    };
    //如果是订购
    if (infoTplName === 'micro-tpl') {
      if (goodsInfoList.length) {
        cpParams.itemType = goodsInfoList[0].itemType;
        cpParams.customizeOrderDTO = {
          logisticsFee: goodsInfoList[0].logisticsFee,
          num: goodsInfoList[0].num,
          fee: goodsInfoList[0].fee,
          totalFee: goodsInfoList[0].totalFee,
          customizeOrderNo: goodsInfoList[0].customizeOrderNo,
        };
      }
    } else {
      cpParams.itemDTOList = goodsInfoList.map((item) => {
        const p = {
          itemNo: item.itemNo,
          skuId: item.skuId,
          itemCount: item.itemCount,
        };
        if (item.itemType !== null && item.itemType !== undefined) {
          p.itemType = item.itemType;
        }
        //促销相关的请求参数
        if (item.mpActivityType) {
          p.mpActivityType = item.mpActivityType;
        }
        if (item.mpActivityId) {
          p.mpActivityId = item.mpActivityId;
        }
        if (item.mpActivityName) {
          p.mpActivityName = item.mpActivityName;
        }
        //赠品相关的请求参数
        if (item.activityType) {
          p.activityType = item.activityType;
        }
        if (item.activityId) {
          p.activityId = item.activityId;
        }
        return p;
      });
    }

    const orderRequestPromDTO = cpParams.orderRequestPromDTO || {};
    if (curGiftCard.userCardNo) {
      orderRequestPromDTO.userGiftCardNoList = curGiftCard.userCardNo;
    } else {
      delete orderRequestPromDTO.userGiftCardNoList;
    }
    if (curCoupon.code) {
      /*运费券的字段跟普通优惠券的字段不同*/
      if (curCoupon.couponCategory == 1) {
        orderRequestPromDTO.logisticsCouponNo = curCoupon.code;
      } else {
        orderRequestPromDTO.couponNo = curCoupon.code;
      }
    } else {
      delete orderRequestPromDTO.logisticsCouponNo;
      delete orderRequestPromDTO.couponNo;
    }
    if (curRedPacke.code) {
      orderRequestPromDTO.redPacketNoList = curRedPacke.code;
    } else {
      delete orderRequestPromDTO.redPacketNoList;
    }
    if (serveCard.id) {
      orderRequestPromDTO.serviceCardNo = serveCard.id;
    } else {
      delete orderRequestPromDTO.serviceCardNo;
    }
    //判断有无优惠券红包次数卡，去除默认赋值
    const isCoupon = payDetails.find((item) => item.name === 'coupon');
    if (!isCoupon) {
      delete orderRequestPromDTO.couponNo;
    }
    const isRedpack = payDetails.find((item) => item.name === 'redpacke');
    if (!isRedpack) {
      delete orderRequestPromDTO.redPacketNoList;
    }
    const isCard = payDetails.find((item) => item.name === 'card');
    if (!isCard) {
      delete orderRequestPromDTO.serviceCardNo;
    }
    cpParams.orderRequestPromDTO = orderRequestPromDTO;
    return util.formatParams(cpParams);
  },
  assembleRequestParamsNoFormat(data) {
    const { infoTplName, goodsInfoList, payDetails, curGiftCard, curCoupon, curRedPacke, serveCard, params } = data;
    const cpParams = {
      ...params,
    };
    //如果是订购
    if (infoTplName === 'micro-tpl') {
      if (goodsInfoList.length) {
        cpParams.itemType = goodsInfoList[0].itemType;
        cpParams.customizeOrderDTO = {
          logisticsFee: goodsInfoList[0].logisticsFee,
          num: goodsInfoList[0].num,
          fee: goodsInfoList[0].fee,
          totalFee: goodsInfoList[0].totalFee,
          customizeOrderNo: goodsInfoList[0].customizeOrderNo,
        };
      }
    } else {
      cpParams.orderRequestDTOS = [
        {
          expressType: 1,
          userAddressId: '',
          itemDTOList: goodsInfoList.map((item) => {
              const p = {
                itemNo: item.itemNo,
                skuId: item.skuId,
                itemType: item.type,
                itemCount: item.itemCount,
              };
              if (item.itemType !== null && item.itemType !== undefined) {
                p.itemType = item.itemType;
              }
              //促销相关的请求参数
              if (item.mpActivityType) {
                p.mpActivityType = item.mpActivityType;
              }
              if (item.mpActivityId) {
                p.mpActivityId = item.mpActivityId;
              }
              if (item.mpActivityName) {
                p.mpActivityName = item.mpActivityName;
              }
              //赠品相关的请求参数
              if (item.activityType) {
                p.activityType = item.activityType;
              }
              if (item.activityId) {
                p.activityId = item.activityId;
              }
              return p;
            })
        }
      ]
    }

    const orderRequestPromDTO = cpParams.orderRequestPromDTO || {};
    if (curGiftCard.userCardNo) {
      orderRequestPromDTO.userGiftCardNoList = curGiftCard.userCardNo;
    } else {
      delete orderRequestPromDTO.userGiftCardNoList;
    }
    if (curCoupon.code) {
      /*运费券的字段跟普通优惠券的字段不同*/
      if (curCoupon.couponCategory == 1) {
        orderRequestPromDTO.logisticsCouponNo = curCoupon.code;
      } else {
        orderRequestPromDTO.couponNo = curCoupon.code;
      }
    } else {
      delete orderRequestPromDTO.logisticsCouponNo;
      delete orderRequestPromDTO.couponNo;
    }
    if (curRedPacke.code) {
      orderRequestPromDTO.redPacketNoList = curRedPacke.code;
    } else {
      delete orderRequestPromDTO.redPacketNoList;
    }
    if (serveCard.id) {
      orderRequestPromDTO.serviceCardNo = serveCard.id;
    } else {
      delete orderRequestPromDTO.serviceCardNo;
    }
    //判断有无优惠券红包次数卡，去除默认赋值
    const isCoupon = payDetails.find((item) => item.name === 'coupon');
    if (!isCoupon) {
      delete orderRequestPromDTO.couponNo;
    }
    const isRedpack = payDetails.find((item) => item.name === 'redpacke');
    if (!isRedpack) {
      delete orderRequestPromDTO.redPacketNoList;
    }
    const isCard = payDetails.find((item) => item.name === 'card');
    if (!isCard) {
      delete orderRequestPromDTO.serviceCardNo;
    }
    cpParams.orderRequestPromDTO = orderRequestPromDTO;
    return cpParams;
  },
};
