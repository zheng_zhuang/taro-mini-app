import Taro from '@tarojs/taro';
import Promise from '@/wxat-common/lib/promise/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import deliveryWay from '@/wxat-common/constants/delivery-way.js';
import api from '@/wxat-common/api/index.js';
import constants from '@/wxat-common/constants/index.js';
import jsonp from '@/wxat-common/utils/wxApi/jsonp';

const freightType = constants.goods.freightType;

export default {
  /**
   * 验证商品库存和上下架状态
   */
  queryGoodsStockAndShelf (data) {
    const { infoTplName, choosedDelivery, storeInfo, curAddressData, goodsInfoList, isGiftActivity } = data;

    return new Promise((resolve, reject) => {
      /*商品类型检测库存*/
      if (infoTplName === 'goods-tpl') {
        const barcodeList = [];
        const combinationItemNoList = [];
        goodsInfoList.forEach((item) => {
          //组合商品
          if (item.itemNo.substring(item.itemNo.length - 3) == '002') {
            combinationItemNoList.push(item.itemNo);
          }
          barcodeList.push(item.barcode);
        });
        const stockQueryParams = {
          isPickUp: choosedDelivery == deliveryWay.SELF_DELIVERY.value,
          combinationItemNoList: combinationItemNoList.toString(),
          barcodeList: barcodeList.toString(),
          pageNo: 1,
          pageSize: barcodeList.length,
          expressType: choosedDelivery,
        };

        const shelfQueryParams = {
          barcodeList: barcodeList.toString(),
          chooseStoreId: storeInfo.id,
          pageNo: 1,
          pageSize: barcodeList.length,
          expressType: choosedDelivery,
        };

        if (stockQueryParams.isPickUp) {
          stockQueryParams.chooseStoreId = storeInfo.id;
        } else if (!!curAddressData) {
          //计算库存获取收货地址的省市区经纬度数据
          if (curAddressData.provinceId) {
            stockQueryParams.provinceId = curAddressData.provinceId;
          }
          if (curAddressData.cityId) {
            stockQueryParams.cityId = curAddressData.cityId;
          }
          if (curAddressData.regionId) {
            stockQueryParams.regionId = curAddressData.regionId;
          }

          //同城配送需要经纬度
          if (choosedDelivery === deliveryWay.CITY_DELIVERY.value) {
            if (curAddressData.latitude) {
              stockQueryParams.latitude = curAddressData.latitude;
            }
            if (curAddressData.longitude) {
              stockQueryParams.longitude = curAddressData.longitude;
            }
            if (!curAddressData.longitude || !curAddressData.latitude) {
              Taro.showToast({
                title: '地址信息缺失，请重新修改地址',
                mask: true,
              });
            }
          }
        }

        const requestList = [
          wxApi.request({
            url: api.goods.stockQueryNew,
            quite: true,
            loading: false,
            data: stockQueryParams,
          }),
        ];

        if (!isGiftActivity) {
          requestList.push(
            wxApi.request({
              url: api.goods.skuListWitSpecItemNos,
              quite: true,
              loading: false,
              data: shelfQueryParams,
            })
          );
        }

        return Promise.all(requestList).then(
          (resList) => {
            const stockMap = resList[0].data || {};
            const shelfList = resList[1] ? resList[1].data : null;
            //计算商品库存
            goodsInfoList.forEach((goods) => {
              const barcode = goods.barcode;
              goods.itemStock = (stockMap && stockMap[barcode]) || 0;
              if (shelfList) {
                const specShelfItem = shelfList.find((shelfItem) => {
                  return shelfItem.barcode === goods.barcode;
                });

                goods.isShelf = (specShelfItem && specShelfItem.itemShelf) || false;
                goods.freightType = (specShelfItem && specShelfItem.freightType) || '';
                goods.salePrice = (specShelfItem && specShelfItem.salePrice) || goods.salePrice;
              }
            });

            let allFreightCollect = goodsInfoList.every((item) => item.freightType === freightType.freightCollect);

            resolve({
              stockMap,
              shelfList,
              goodsInfoList,
              allFreightCollect,
            });
          },
          (error) => {
            let beyoundScope = false;
            //code等于100300超出配送范围
            if (error.data.errorCode === 100300) {
              beyoundScope = true;
            }
            reject({
              msg: error && error.data && error.data.errorMessage ? error.data.errorMessage : '计算库存失败',
              beyoundScope: beyoundScope,
            });
          }
        );
      } else {
        resolve();
      }
    });
  },
  /**
   * 检查商品是否允许支付
   */
  checkGoodsDenyPay (data) {
    const { goodsInfoList, infoTplName } = data;
    let denyPay = {
      deny: false,
      reason: '',
    };

    if (!goodsInfoList.length) {
      denyPay.deny = true;
      denyPay.reason = '请重新选择商品购买';
    } else {
      const invalidGoods = goodsInfoList.find((goods) => {
        return !goods.isShelf || goods.itemCount > goods.itemStock;
      });

      //拒绝支付标识，如果商品列表里有一个不满足条件，则不允许支付
      denyPay.deny = !!invalidGoods && infoTplName === 'goods-tpl';
      {
        denyPay.reason = '商品库存不足或已下架';
      }
    }
    return denyPay;
  },

  promCheckGoodsDenyPay (data, quite = true) {
    return new Promise((resolve, reject) => {
      const denyPay = this.checkGoodsDenyPay(data);
      if (denyPay.deny) {
        reject({
          quite,
          msg: denyPay.reason,
        });
      } else {
        resolve();
      }
    });
  },
};
