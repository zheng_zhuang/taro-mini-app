import { $getRouter } from 'wk-taro-platform';
import React from 'react'; /* eslint-disable react/sort-comp */
// @externalClassesConvered(BottomDialog)
// @externalClassesConvered(AnimatDialog)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Form, Image, Text, Input, Button, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import constants from '@/wxat-common/constants/index';
import deliveryWay from '@/wxat-common/constants/delivery-way';
import pay from '@/wxat-common/utils/pay';
import moneyUtil from '@/wxat-common/utils/money';
import template from '@/wxat-common/utils/template';
import buyHub from '@/wxat-common/components/cart/buy-hub';
import scanBuyHub from '@/wxat-common/components/cart/scan-buy-hub';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import couponEnum from '@/wxat-common/constants/couponEnum';
import errorCodeFilter from '@/wxat-common/constants/error-code-filter';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import initHandler from './handler/initHandler';
import deliveryHandler from './handler/deliveryHandler';
import checkStockAndShefHandler from './handler/checkStockAndShefHandler';
import priceHandler from './handler/priceHandler';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum';
import dealTmpHandler from './handler/dealTmpHandler';
import store from '@/store';
import FrontMoneyState from '@/wxat-common/components/front-money-state/index';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import BottomDialog from '@/wxat-common/components/bottom-dialog';
import AddressList from '@/wxat-common/components/address/list/index';
import MicroDeliveryTmpl from '@/imports/MicroDeliveryTmpl';
import MicroTplTmpl from '@/imports/MicroTplTmpl';
import Goods from './Goods';
import './index.scss';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import { updateBaseAction } from '@/redux/base';
import classNames from 'classnames';
import deliveryConfigHandler from './handler/deliveryConfigHandler';

import './serve-tpl.scss';
import './micro-tpl.scss';
import screen from '@/wxat-common/utils/screen';
import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'

const storeData = store.getState();
const PAY_CHANNEL = constants.order.payChannel;
const freightType = constants.goods.freightType;
const mergeDeliveryConfig = deliveryConfigHandler.mergeDeliveryConfig;
const couponImg = cdnResConfig.coupon;
const commonImg = cdnResConfig.common;
const scan_codeImg = cdnResConfig.scan_code;

/**
 * url 参数描述
 * goodsInfoList Array 数组，商品信息
 * [{
 pic: detail.pic, // 商品图片链接
  salePrice: detail.salePrice, // 商品价格（必填）
  itemCount: detail.itemCount, // 购买数量（必填）
  noNeedPay: detail.noNeedPay, // 是否需要支付
  name: detail.activityName, // 显示名称（商品名称或活动名称)（必填）
  itemNo: detail.itemNo, // 商品编号（必填）
  skuId: detail.skuId, // 商品规格id
  skuTreeIds: detail.skuTreeIds, // 商品选择规格
  skuTreeNames: activity.skuTreeNames, // 商品规格描述
  itemType: 类型： 服务 or 商品
}]

payDetails Array 数组，支付详情，不传默认带卡项除外，所有支付信息
  [{
    name: 'coupon' // 显示优惠卷
  }, {
    name: 'redpacke' // 显示红包
  }, {
    name: 'discount' // 显示折扣
  }, {
    name: 'freight' // 显示邮费
  }，{
    name: 'card'
  },{
    isCustom: Boolean 是否自定义，
    label: String,
    value: String
  }]
**/

const mapStateToProps = (state) => ({
  scoreName: state.globalData.scoreName || '',
  industry: state.globalData.industry,
  appInfo: state.base.appInfo,
  currentSessionStore: state.base.currentStore,
  base: state.base,
  roomCode: state.roomCode,
  trueCode: state.roomCodeTypeName + '-' + state.roomCode,
  storeInfo: state.base.currentStore
});

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateBase: (data) => dispatch(updateBaseAction(data)),
});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class PayOrder extends React.Component {
  $router = $getRouter();
  state = {
    currentStore: { ...this.props.currentSessionStore },
    choosedDelivery: deliveryWay.EXPRESS.value,
    storeInfo: null,
    supportDelivery: {},
    goodsInfoList: null,
    payDetails: [],
    payChannel: PAY_CHANNEL.WX_PAY,
    isNeedPay: true,
    coupons: {},
    curGiftCard: {}, // 当前选择使用的礼品卡
    curCoupon: {}, // 当前选择使用的优惠券
    curRedPacke: {}, // 当前选择使用的红包
    serveCard: {}, // 服务卡
    selectAddress: false,
    curAddressData: null,
    orderMessage: '', // 备注
    balance: null,
    price: {},
    isCoupon: 1, // 是否为优惠劵
    infoTplName: 'goods-tpl',
    tmpStyle: {},
    isScan: 0, // 是否是扫码购
    activityId: null, // 拼团活动id
    groupNo: null, // 拼团订单号，判断是否参团进入的订单，用于当前拼团超出限制人数时，获取活动id跳转该拼团活动
    bgGroupFullImg: cdnResConfig.group.bgGroupFullImg, // 参团人员已满对话弹框背景图片
    isGiftActivity: false,
    isFrontMoneyItem: false, // 是否为定金订单
    confirmAgreement: false, // 确认同意认筹协议
    signatureImageUrl: null, // 保存的签名图片Url
    isCardpackType: false, // 是否为代金卡包订单
    useLogisticsCoupon: 0,
    beyoundScope: false, // 超出配送范围
    prescriptionId: null, // 处方药id
    drugUser: null, // 用药人
    isDrug: false, // 是否是处方药
    errorText: '订单中包含处方药，需提交个人信息和处方信息',
    payable: false, // 是否能够支付，如果未计算价格，不允许支付
    freightType,
    allFreightCollect: false, // 是否全部是到付商品
    seckillNo: null,
    orderNotes: [], // 订单备注列表项
    isPromoter: false, // 推广大使订单
    categoryTypecate: null, // 5: 服务配送
    isRoomScan: false,
    needScan: false, // 公众号h5标识，需要扫码
    trueCode: '', // 展示用的房间号
    roomCode:'',
    isDistributionServices:false,   // 是否是配送服务事项下单进来
    qrCodeTypeId: '', // 二维码类型Id
  };

  isMount = false
  promoterActivityId = null;
  promoterDetail = {};
  refUseId = null;
  stockMap = null;
  shelfList = null;
  unpayableReason = ''; // 不能支付的原因
  denyRepeatPay = false; // 拒绝重复支付
  agreementParams = {}; // 协议参数，用于传给协议页面展示用户信息及购买的商品信息

  /**
  * 生命周期函数--监听页面加载
  */
  componentDidMount() {

    if (this.isMount) return false
    this.isMount = true

    const option = this.$router.params;

    wxApi.setNavigationBarTitle({
      title: option.title || "待付款订单"// 页面标题为路由参数
    });

    this.getTemplateStyle();
    this.getBalance();

    // const serverDeliverGoodsList = wxApi.getStorageSync('storageOrderList') || [];
    const serverDeliverGoodsList = Taro.getStorageSync('storageOrderList') || [];
    const isRoomScan = serverDeliverGoodsList.some(item => !!item.scan);

    if(option.orderMessage){
      const orderMessage = option.orderMessage
      this.setState({orderMessage})
    }
    // 配送服务
    if (option.categoryType == 5) {
      // 立即下单购买
      const serviceData = serverDeliverGoodsList.map(item => {
        item = typeof(item) === 'string' ? JSON.parse(item) : item
        const wxItem = item.wxItem ? item.wxItem : []
        return {
          itemCount: item.count,
          itemNo: wxItem.itemNo,
          barcode: wxItem.barcode,
          name: wxItem.name,
          skuId: item.skuId  || wxItem.skuId || null,
          pic: wxItem.thumbnail,
          skuTreeNames: item.skuInfo || '',
          freight: wxItem.freight,
          noNeedPay: wxItem.noNeedPay,
          reportExt: item.reportExt || null,
          wxItem: wxItem,
          isShelf: wxItem.isShelf,
          itemStock: wxItem.itemStock,
          price:item.price?item.price:item.salePrice
        }
      });
      this.setState({
        choosedDelivery: deliveryWay.SELF_DELIVERY.value
      },() => {
        this.initDistrubutionData(option, isRoomScan, serviceData);

      });
    } else {
      if(option.isDistributionServices){
        const serviceData = Taro.getStorageSync("goodsInfo")
        this.setState({
          isDistributionServices:true,
          choosedDelivery: deliveryWay.SELF_DELIVERY.value
        },() => {
          this.initDistrubutionData(option, isRoomScan, serviceData);

        });
      }else{
        this.initDistrubutionData(option, isRoomScan);
      }
    }
  }

  /* 配送服务商品展示 */
  initDistrubutionData(option, isRoomScan=false, goodsInfoList= protectedMailBox.read("goodsInfoList") || []) {

    // // 防止两次渲染没有数据 干啥用的？？？
    if (!goodsInfoList.length) {
      goodsInfoList = Taro.getStorageSync('GOODS_LIST_CUT') ? JSON.parse(Taro.getStorageSync('GOODS_LIST_CUT')) : []
      Taro.removeStorageSync('GOODS_LIST_CUT')
    }
    if (!goodsInfoList.length) {
      goodsInfoList = window.sessionStorage.getItem('GOODS_LIST_CUT') ? JSON.parse(window.sessionStorage.getItem('GOODS_LIST_CUT')) : []
      window.sessionStorage.removeItem('GOODS_LIST_CUT')
    }
    // 是否是推广大使订单
    const isPromoter = protectedMailBox.read('isPromoter');
    // 是否是推广大使订单参数
    const promoterDetail = protectedMailBox.read('promoterDetail');

    // 设置身份证号码隐藏
    const userInfo = goodsInfoList[0]?.userInfo || {}; // 用户信息
    const idCardNo = userInfo.idCardNo || null;
    if (idCardNo) {
      userInfo.idCardNoEncrypt = idCardNo.slice(0, 2) + '**************' + idCardNo.slice(-2); // 获取身份证的前两位及后两位数字拼接
    }

    let { isCardpackType, payChannel } = this.state;
    const { currentStore } = this.state;
    // 根据当前门店设置物流方式
    const deliveryConfig = mergeDeliveryConfig(currentStore);
    const initData = initHandler.init({
      option,
      goodsInfoList,
      deliveryConfig,
    });

    // 判断是否为代金卡包
    if (goodsInfoList && goodsInfoList[0]?.itemType === goodsTypeEnum.CARDPACK.value) {
      isCardpackType = true;
      payChannel = PAY_CHANNEL.WX_PAY;
    }
    // 判断是否推广大使订单
    if (!!isPromoter && promoterDetail) {
      this.promoterDetail = promoterDetail;
      this.promoterActivityId = promoterDetail.promoterActivityId;
      this.refUseId = promoterDetail.refUseId;
    }

    // 判断是否是砍价订单
    if (goodsInfoList && goodsInfoList[0]?.isCutPriceOrder) {
      this.getPromoterOrder(goodsInfoList[0]);
    }
    this.setState(
      {
        isCardpackType,
        isPromoter,
        payChannel,
        ...initData,
        useLogisticsCoupon: option.useLogisticsCoupon,
        roomCode: this.state.roomCode || option.roomCode || '',
        needScan: !!option.needScan,
        qrCodeTypeId: option.qrCodeTypeId,
        trueCode: this.state.trueCode || option.trueCode,
        categoryType: option.categoryType == 5 ? option.categoryType : null,
        choosedDelivery: option.categoryType == 5 ? deliveryWay.SELF_DELIVERY.value : (deliveryConfig ? deliveryConfig.choosedDelivery : this.state.choosedDelivery),
        isRoomScan
      },

      () => {
        if (isCardpackType) { // 判断是否为代金卡包的订单
          this.handleCardpackStock();
        } else {
          this.handleDeliveryChange(); // 初始化判断发货方式，校验下单的正确性
        }
      }
    );
  }

  getPromoterOrder = async (activity) => {
    const params = {
      contentActivityId: activity.id,
      storeNo: this.state.currentStore.id,
    };
     wxApi
      .request({
        url: api.ambassador.ambassadorOrder,
        data: params,
      })
      .then((res) => {
        this.setState({
          isPromoter: !!(res.data && res.data.length),
        });
      });
  };

  // 发货方式变化的相关操作。
  componentDidShow() {
    // const { currentStore, isCardpackType } = this.state;
    // // 门店地址变化
    // const selectStore = protectedMailBox.read('payOrderPageKey');
    // if (selectStore && selectStore.id !== currentStore.id) {
    //   this.setState(
    //     {
    //       currentStore: selectStore,
    //     },

    //     () => {
    //       if (isCardpackType) {
    //         this.handleCardpackStock();
    //       } else {
    //         const deliveryConfig = mergeDeliveryConfig(selectStore, this.state.choosedDelivery);

    //         this.setState({
    //           ...deliveryConfig,
    //         },() =>{
    //           this.handleDeliveryChange(); // 初始化判断发货方式，校验下单的正确性
    //         });
    //       }
    //     }
    //   );
    // }

    // // 保存的签名图片Url
    // const signatureImageUrl = protectedMailBox.read('signatureImageUrl') || null;
    // if (signatureImageUrl) {
    //   this.setState(
    //     {
    //       signatureImageUrl,
    //     },

    //     () => {}
    //   );
    // }
  }

  /**
   * 错误提示方法
   * @param {*} msg
   */
  errorTip(msg) {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve);
    });
  }

  /**
   * 发货方式变化handler
   * 触发机制：onLoad初始化时，发货方式变化时，收货地址变化时（快递），门店地址变化时（自提）
   * 发货方式变化，会直接影响到整条链路：库存判断、优惠获取、价格获取
   */
  handleDeliveryChange() {
    let { stockMap, shelfList, goodsInfoList, curRedPacke, curCoupon, coupons, price, allFreightCollect } = this.state;
    // 查询商品库存和上下架状态
    this.setState({ payable: false, beyoundScope:false });
    wxApi.showLoading({
      title: '请稍等',
    });
    deliveryHandler
      .handleDeliveryChanged(this.state)
      .then(async (address) => {
        // 第一次初始化收货地址
        if (address) {
          await this.setStateAsync({
            curAddressData: address,
            address:address.address
          });
        }
        // 判断商品的上下架状态
        return checkStockAndShefHandler.queryGoodsStockAndShelf({ ...this.state, ...{ curAddressData: address } });
      })
      .then(async (res) => {
        if (res) {
          const { stockMap, shelfList, goodsInfoList, allFreightCollect } = res;
          if (this.state.categoryType == 5) {
            goodsInfoList.forEach(item => {
                item.itemNo = item.wxItem.itemNo;
                item.barcode = item.wxItem.barcode;
                item.skuId = item.skuId  || item.wxItem.skuId || null;
                item.salePrice = item.wxItem.salePrice;
                item.freight = item.wxItem.freight;
                item.isShelf = item.wxItem.isShelf;
                item.itemStock = item.wxItem.itemStock;
            });
          }
          await this.setState({
            stockMap,
            shelfList,
            goodsInfoList,
            allFreightCollect,
          });
        }
        // 校验商品是否允许下单（是否库存不足、是否上下架）
        return checkStockAndShefHandler.promCheckGoodsDenyPay({
          ...this.state,
          ...{ goodsInfoList, stockMap, shelfList },
        });
      })
      .then(() => {
        // 获取优惠信息
        return priceHandler.fetchPreferentialList(this.state);
      })
      .then(async (res) => {
        if (res) {
          curRedPacke = res.curRedPacke;
          curCoupon = res.curCoupon;
          coupons = res.coupons;
          await this.setState({
            curRedPacke,
            curCoupon,
            coupons,
          });
        }
        // 获取订单价格
        return priceHandler.getPrice({ ...this.state, ...{ curRedPacke, curCoupon, coupons } });
      })
      .then(async (res) => {
        price = res.price;
        await this.setState({
          price,
          beyoundScope: false,
        });

        this.setState({ payable: true });
      })
      .catch(async (error) => {
        this.unpayableReason = error.msg;
        if (!error.quite) {
          // 未选择收货地址时报错
          this.errorTip(error.msg);
        }
        // 超出配送范围
        if (error.beyoundScope) {
          await this.setState({
            beyoundScope: error.beyoundScope,
          });
        } else {
          await this.setState({
            beyoundScope: false,
          });
        }
      })
      .finally(() => {
        setTimeout(() => {
          wxApi.hideLoading();
        }, 1500);
      });
  }

  /**
   * 代金卡包发货方式变化handler
   * 触发机制：onLoad初始化时，发货方式变化时，门店地址变化时（自提）
   * 发货方式变化，会直接影响到整条链路：库存判断、优惠获取、价格获取
   */
  handleCardpackStock() {
    const { goodsInfoList } = this.state;
    let { stockMap } = this.state;
    // 查询商品库存和上下架状态
    this.setState({ payable: false });
    wxApi.showLoading({
      title: '请稍等',
    });

    const params = {
      itemNo: goodsInfoList[0].itemNo,
    };

    // 代金卡包从卡包详情接口获取上架、库存信息

    wxApi
      .request({
        url: api.couponpocket.detail,
        data: params,
      })
      .then((res) => {
        if (res.data) {
          stockMap = {
            // 真实库存：provNum(总量) - cardSalesVolume(已售出) provNum为0时不限量
            [goodsInfoList[0].itemNo]:
              res.data.provNum === 0 ? 9999999999 : res.data.provNum - res.data.cardSalesVolume,
          };

          goodsInfoList[0].itemStock =
            res.data.provNum === 0 ? 9999999999 : res.data.provNum - res.data.cardSalesVolume;
          goodsInfoList[0].isShelf = res.data.isShelf;
          this.setState({
            goodsInfoList,
            stockMap,
          });
        }
        // 校验商品是否允许下单（是否库存不足、是否上下架）
        return checkStockAndShefHandler.promCheckGoodsDenyPay({ ...this.state, ...{ goodsInfoList, stockMap } });
      })
      .then(() => {
        // 获取订单价格
        return priceHandler.getPrice(this.state);
      })
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
        });

        this.setState({ payable: true });
      })
      .catch((error) => {
        this.unpayableReason = error.msg;
        if (!error.quite) {
          // 未选择收货地址时报错
          this.errorTip(error.msg);
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  /**
   * 商品信息发生变化时，并计算价格的handler
   * 如删除无库存商品、删除上下架商品，增减商品数量等，获取优惠信息
   * 商品信息发生变化，只会影响到优惠获取以及价格的计算
   */
  handleGoodsInfoChange() {
    let { curRedPacke, curCoupon, coupons } = this.state;
    const denyPay = checkStockAndShefHandler.checkGoodsDenyPay(this.state);
    if (denyPay.deny) {
      wxApi.showToast({
        icon: 'none',
        title: denyPay.reason,
      });

      return;
    }
    this.setState({ payable: false });
    wxApi.showLoading({
      title: '请稍等',
    });

    priceHandler
      .fetchPreferentialList(this.state)
      .then((res) => {
        if (res) {
          curRedPacke = res.curRedPacke;
          curCoupon = res.curCoupon;
          coupons = res.coupons;
          this.setState({
            curRedPacke,
            curCoupon,
            coupons,
          });
        }
        // 获取订单价格
        return priceHandler.getPrice({ ...this.state, ...{ curRedPacke, curCoupon, coupons } });
      })
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
        });

        this.setState({ payable: true });
      })
      .catch((error) => {
        this.unpayableReason = error.msg;
        if (!error.quite) {
          // 未选择收货地址时报错
          this.errorTip(error.msg);
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  /**
   * 优惠信息变化时的handler，
   * 如：优惠券选择，礼品卡选择、红包选择等
   */
  handlePreferentialChange = () => {
    this.setState({ payable: false });
    wxApi.showLoading({
      title: '请稍等',
    });

    priceHandler
      .getPrice(this.state)
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
        });

        this.setState({ payable: true });
      })
      .catch((error) => {
        this.unpayableReason = error.msg;
        if (!error.quite) {
          // 未选择收货地址时报错
          this.errorTip(error.msg);
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  };

  /**
   * 获取余额
   * @param {*} isSingleRequest
   */
  getBalanceRequest(isSingleRequest) {
    return wxApi.request({
      url: api.userInfo.balance,
      quite: !isSingleRequest,
      loading: isSingleRequest,
      data: {},
    });
  }

  // 设置余额
  getBalance() {
    this.getBalanceRequest(true).then((res) => {
      this.setState({
        balance: res.data.balance,
      });
    });
  }

  // 设置新增地址
  addAddress = () => {
    this.setState({
      selectAddress: true,
    });
  };

  /**
   * 改变地址
   * @param {*} e
   */
  handleAddressChange = (e) => {
    this.setState(
      {
        selectAddress: false,
        curAddressData: e,
      },

      () => {
        this.handleDeliveryChange();
      }
    );
  };

  /**
   * 选择配送方式
   * @param {*} e
   */
  handleChooseDelviery = (e) => {
    const { choosedDelivery } = this.state;
    const pendingDelivery = e.target.dataset.choosed;
    if (choosedDelivery !== pendingDelivery) {
      this.setState(
        {
          choosedDelivery: e.target.dataset.choosed,
        },

        () => {
          this.handleDeliveryChange();
        }
      );
    }
  };

  /**
   * 商品信息发生变化的相关操作
   */
  handleClearNoStockGoods = () => {
    const { goodsInfoList } = this.state;
    const filteredGoodsInfoList = [];
    goodsInfoList.forEach((item) => {
      if (item.itemStock && item.isShelf) {
        filteredGoodsInfoList.push(item);
      }
    });

    // 判断是否有处方药
    const isDrug = false;

    this.setState(
      {
        isDrug,
        goodsInfoList: filteredGoodsInfoList,
      },

      () => {
        this.handleGoodsInfoChange();
      }
    );
  };

  handleIncreaseBuyNum = (barcode) => {
    const { goodsInfoList } = this.state;

    let index = null;
    goodsInfoList.forEach((item, i) => {
      if (item.barcode === barcode) {
        index = i;
      }
    });
    if (index != null && goodsInfoList[index].itemCount < goodsInfoList[index].itemStock) {
      goodsInfoList[index].itemCount++;
      this.setState(
        {
          goodsInfoList,
        },

        () => {
          this.handleGoodsInfoChange();
        }
      );
    } else {
      this.errorTip('库存不足');
    }
  };

  handleDecreaseBuyNum = (barcode) => {
    const { goodsInfoList } = this.state;
    let index = null;
    goodsInfoList.forEach((item, i) => {
      if (item.barcode === barcode) {
        index = i;
      }
    });

    if (index != null && goodsInfoList[index].itemCount > 1) {
      goodsInfoList[index].itemCount--;
      this.setState(
        {
          goodsInfoList,
        },

        () => {
          this.handleGoodsInfoChange();
        }
      );
    } else {
      this.errorTip('不能再减了');
    }
  };

  // 优惠信息变化的相关操作
  handleGoToGiftCardList = () => {
    const { coupons } = this.state;
    const userGiftCardDTOSet = coupons.userGiftCardDTOSet;
    if (checkStockAndShefHandler.checkGoodsDenyPay(this.state).deny) {
      return;
    } else if (!userGiftCardDTOSet || userGiftCardDTOSet.length < 1) {
      return this.errorTip('无可用礼品卡');
    }

    this.setState({
      isCoupon: 4,
    });

    this.getChoseDialog().showModal();
  };

  handleGoToCouponList = () => {
    const { coupons } = this.state;
    const couponSet = coupons.couponSet;
    if (checkStockAndShefHandler.checkGoodsDenyPay(this.state).deny) {
      return;
    } else if (!couponSet || couponSet.length < 1) {
      return this.errorTip('无可用优惠券');
    }

    this.setState({
      isCoupon: 1,
    });

    this.getChoseDialog().showModal();
  };

  handleGoToCard = () => {
    const { coupons } = this.state;
    const userCardDTOSet = coupons.userCardDTOSet;
    if (checkStockAndShefHandler.checkGoodsDenyPay(this.state).deny) {
      return;
    } else if (!userCardDTOSet || userCardDTOSet.length < 1) {
      return this.errorTip('无可用次数卡');
    }

    this.setState({
      isCoupon: 3,
    });

    this.getChoseDialog().showModal();
  };

  handleRedPackeList = () => {
    const { coupons } = this.state;
    const redPacketSet = coupons.redPacketSet;
    if (checkStockAndShefHandler.checkGoodsDenyPay(this.state).deny) {
      return;
    } else if (!redPacketSet || redPacketSet.length < 1) {
      return this.errorTip('无可用红包');
    }

    this.setState({
      isCoupon: 2,
    });

    this.getChoseDialog().showModal();
  };

  // 获取优惠对话框
  getChoseDialog() {
    return this.chooseCardDialogCOMP;
  }

  // 不使用优惠
  handleNoUseCard = () => {
    const { isCoupon } = this.state;
    switch (isCoupon) {
      case 1:
        this.setState(
          {
            curCoupon: {},
          },

          this.handlePreferentialChange
        );

        break;
      case 2:
        this.setState(
          {
            curRedPacke: {},
          },

          this.handlePreferentialChange
        );

        break;
      case 3:
        this.setState(
          {
            serveCard: {},
          },

          this.handlePreferentialChange
        );

        break;
      case 4:
        this.setState(
          {
            curGiftCard: {},
          },

          this.handlePreferentialChange
        );

        break;
    }

    this.getChoseDialog().hideModal();
  };

  onSelectCard = (e) => {
    const coupon = e.currentTarget.dataset.coupon;
    // 优惠券或红包状态是可用的或者优惠类型为礼品卡或次数卡（次数后端返回都是可用的）
    if (
      (coupon.status && coupon.status === 1) ||
      coupon.cardType === constants.card.type.gift ||
      coupon.cardType === constants.card.type.coupon
    ) {
      let key;
      switch (this.state.isCoupon) {
        case 1:
          key = 'curCoupon';
          break;
        case 2:
          key = 'curRedPacke';
          break;
        case 3:
          key = 'serveCard';
          break;
        case 4:
          key = 'curGiftCard';
          break;
      }

      if (key) this.setState({ [key]: coupon }, this.handlePreferentialChange);
      this.getChoseDialog().hideModal();
    } else {
      this.errorTip(coupon.rejReason || '该优惠不能使用。');
    }
  };

  // 取消选择
  onSelectCancel() {
    this.getChoseDialog().hideModal();
  }

  // 备注
  handleMessageInput = (e) => {
    this.setState({
      orderMessage: e.detail.value,
    });
  };

  inputRoomCode=({ detail: { value } }) =>{
    // state.roomCode = value
    this.setState({
      trueCode: value
    });
  }

  // 订单备注
  noteValueInput = (i) => (e) => {
    const { orderNotes } = this.state;
    orderNotes[i].noteValue = e.detail.value;
    this.setState({
      orderNotes,
    });
  };

  /**
   * 确认协议
   * @param {*} e
   */
  checkboxChange = (e) => {
    const { signatureImageUrl, confirmAgreement } = this.state;
    // 判断是否有保存的签名图片Url，没有则提示用户签署协议
    if (!signatureImageUrl) {
      this.setState({
        confirmAgreement: false,
      });

      return this.errorTip('请先阅读并签署认购协议');
    } else {
      this.setState({
        confirmAgreement: !confirmAgreement,
      });
    }
  };

  /**
   * 查看协议
   */
  handleAgreement = () => {
    wxApi
      .request({
        url: api.estate.agreement,
        loading: true,
        data: {},
      })
      .then((res) => {
        if (res.data) {
          const dealContent = dealTmpHandler.parseDeal(res.data.content, this.agreementParams);
          protectedMailBox.send('sub-packages/mine-package/pages/deal/index', 'content', dealContent);
          wxApi.$navigateTo({
            url: `/sub-packages/mine-package/pages/deal/index`,
            data: {
              title: res.data.title,
            },
          });
        }
      })
      .catch((error) => {});
  };

  /**
   * 去签署
   */
  handleSignAgreement = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/order-package/pages/agreement-sign/index',
    });
  };

  // 扫码获取房间号
  scanCode() {
    Taro.scanCode({
      needResult:1,
      success: (res) => {

        if (!res.path) {
          return Taro.showToast({
            title: '无法识别房间信息',
            icon: 'none',
          });
        }

        let path = decodeURIComponent(res.path);

        path = path.substring(path.lastIndexOf('?') + 1)
        path = path.split('&')
        const params = {}
        for(let i=0;i<path.length;i++){
          const val = path[i].split('=')
          params[val[0]] = val[1]
        }

        // 取url后面的参数 无需关注
        // let vars = res.path.split('=');
        // vars.splice(0, 1);
        // vars = vars.join('=');
        // let query = vars.split('&');
        // let params = {};
        // for (let i = 0; i < query.length; i++) {
        //   let q = query[i].split('=');
        //   if (q.length === 2) {
        //     params[q[0]] = q[1];
        //   }
        // }

        if (params.id) {
          wxApi
            .request({
              url: api.queryQrCodeScenes,
              isLoginRequest: true,
              method: 'GET',
              data: {
                id: params.id,
              },
            })
            .then(({ data }) => {
              const dataObj = {};
              data.split('&').forEach((item) => {
                const key = item.split('=')[0];
                const value = item.split('=')[1];
                dataObj[key] = value;
              });
              return wxApi.request({
                url: api.selectQrCodeNumber,
                isLoginRequest: true,
                method: 'POST',
                data: {
                  id: params.id,
                  hotelId: params.pId || params.hId,
                  qrCodeNumber: params.qrcodeNo,
                },
              });
            })
            .then(({ data: { qrCodeNumber, roomCode, roomNumber, qrCodeTypeId, qrCodeTypeName,storeId } }) => {
              if (storeData.base.userInfo.storeId !== storeId) {
                wxApi.showToast({
                  title: '小程序门店与二维码门店不匹配',
                  icon: 'none'
                });
                return
              }
              if (!roomCode && (params.tp == 1 || !params.tp)) {
                Taro.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                Taro.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? qrCodeTypeName + '-' + roomNumber
                      : qrCodeTypeName + '-' + (params.id ? roomCode : qrCodeNumber)
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
                qrCodeTypeId: qrCodeTypeId,
              });
            })
        } else if (params.qrcodeNo) {
          wxApi
            .request({
              url: api.selectQrCodeNumber,
              isLoginRequest: true,
              method: 'POST',
              data: {
                id: params.id,
                hotelId: params.pId || params.hId,
                qrCodeNumber: params.qrcodeNo,
              },
            })
            .then(({ data: { qrCodeNumber, roomCode, roomNumber, qrCodeTypeId, qrCodeTypeName } }) => {
              if ((!roomCode || !qrCodeNumber) && (params.tp == 1 || !params.tp)) {
                Taro.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                Taro.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? qrCodeTypeName + '-' + roomNumber
                      : qrCodeTypeName + '-' + (params.id ? roomCode : qrCodeNumber)
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
                qrCodeTypeId: qrCodeTypeId,
              });
            })
        }
      },
    });
  }

  /**
   * 支付
   * @param {*} e
   */
  handlePay = (e) => {
    const {
      currentStore,
      curAddressData: address,
      infoTplName,
      choosedDelivery,
      params,
      price,
      curGiftCard,
      curCoupon,
      curRedPacke,
      goodsInfoList,
      serveCard,
      useLogisticsCoupon,
      activityId,
      groupNo,
      isScan,
      orderMessage,
      beyoundScope,
      isDrug,
      prescriptionId,
      isCardpackType,
      orderNotes,
      roomCode,
      needScan,
    } = this.state;

    let { payChannel } = this.state;

    if (needScan && !roomCode) {
      wxApi.showToast({
        icon: 'none',
        title: '请输入位置信息'
      })
      return
    }

    // 判断必填订单备注是否填写
    for (let i = 0; i < orderNotes.length; i++) {
      if (orderNotes[i].isMust && !orderNotes[i].noteValue) {
        wxApi.showToast({
          title: `请填写${orderNotes[i].noteKey}`,
          icon: 'none',
        });

        return;
      }
    }

    // 判断是否达到起送金额
    if (beyoundScope) {
      wxApi.showToast({
        title: '该收货地址超出同城配送范围',
        icon: 'none',
      });

      return;
    }

    if (!this.state.payable) {
      const denyPay = checkStockAndShefHandler.checkGoodsDenyPay(this.state);
      if (denyPay.deny) {
        wxApi.showToast({
          icon: 'none',
          title: denyPay.reason,
        });

        return;
      } else if (this.unpayableReason) {
        return this.errorTip(this.unpayableReason);
      } else {
        return this.errorTip('计算价格失败，无法支付');
      }
    }

    // 判断是否达到起送金额
    if (choosedDelivery == deliveryWay.CITY_DELIVERY.value) {
      if (price.deliveryCostFee > price.totalPrice) {
        wxApi.showToast({
          title: '未达到起送金额',
          icon: 'none',
        });

        return;
      }
    }

    const orderInfo = Object.assign(
      {
        itemDTOList: goodsInfoList.map((goods) => {
          const item = {
            itemNo: goods.itemNo,
            skuId: goods.skuId,
            itemCount: goods.itemCount,
            reportExt: goods.reportExt,
            freightType: goods.freightType,
          };

          if (goods.itemType !== null && goods.itemType !== undefined) {
            item.itemType = goods.itemType;
          }
          // 促销相关的请求参数
          if (goods.mpActivityType) {
            item.mpActivityType = goods.mpActivityType;
          }
          if (goods.mpActivityId) {
            item.mpActivityId = goods.mpActivityId;
          }
          if (goods.mpActivityName) {
            item.mpActivityName = goods.mpActivityName;
          }

          // 赠品相关的请求参数
          if (goods.activityType) {
            item.activityType = goods.activityType;
          }
          if (goods.activityId) {
            item.activityId = goods.activityId;
          }
          return item;
        }),
        orderMessage: orderMessage,
        formId: !e.detail || e.detail.formId === 'the formId is a mock one' ? null : e.detail.formId,
        // 订单备注项信息
        orderNotes: orderNotes,
      },

      params
    );


    if (goodsInfoList && goodsInfoList.length > 0) {
      // 订购订单没有itemDTOList
      if (infoTplName === 'micro-tpl') {
        delete orderInfo.itemDTOList;
        orderInfo.customizeOrderNo = goodsInfoList[0].customizeOrderNo;
      }

      // 代金卡包推广大使
      if (isCardpackType) {
        const { channel, refUseId, activityId } = goodsInfoList[0];

        if (channel === 2 && (!refUseId || !activityId)) {
          return wxApi.showToast({
            title: '大使信息缺失，请重新扫码购买卡包',
            icon: 'none',
          });
        } else {
          orderInfo.promoterId = refUseId;
          orderInfo.activityId = activityId;
        }
      }
    }

    // 推广大使-拼团 带推广活动id
    if (this.promoterActivityId && this.refUseId) {
      orderInfo.promoterId = this.refUseId;
      orderInfo.activityId = this.promoterActivityId;
    }

    // 发货地址
    if (choosedDelivery == deliveryWay.EXPRESS.value || choosedDelivery == deliveryWay.CITY_DELIVERY.value) {
      if (address) orderInfo.userAddressId = address.id;
    } else if (choosedDelivery == deliveryWay.SELF_DELIVERY.value) {
      orderInfo.storeId = currentStore.id;
      orderInfo.storeName = currentStore.name;
      orderInfo.storeTel = currentStore.tel;
    }
    // 发货类型
    if (infoTplName === 'goods-tpl') {
      orderInfo.expressType =  this.state.choosedDelivery;
    }
    const orderRequestPromDTO = orderInfo.orderRequestPromDTO || {};

    if (curGiftCard && curGiftCard.userCardNo) {
      orderRequestPromDTO.userGiftCardNoList = curGiftCard.userCardNo;
    }
    if (curCoupon && curCoupon.code) {
      /* 运费券的字段跟普通优惠券的字段不同,折扣券和满减券字段一样 */
      if (curCoupon.couponCategory == 1) {
        orderRequestPromDTO.logisticsCouponNo = curCoupon.code;

        // 秒杀订单没有设置可用运费券时去除运费券
        if (!Number(useLogisticsCoupon) && orderRequestPromDTO.secKillNo) {
          delete orderRequestPromDTO.logisticsCouponNo;
        }
      } else {
        orderRequestPromDTO.couponNo = curCoupon.code;
      }
    }
    if (curRedPacke.code) {
      orderRequestPromDTO.redPacketNoList = curRedPacke.code;
    }
    if (serveCard.id) {
      orderRequestPromDTO.serviceCardNo = serveCard.id;
    }

    orderInfo.orderRequestPromDTO = orderRequestPromDTO;

    orderInfo['orderServerPlatformDTO.categoryType'] = this.state.categoryType;
    orderInfo.roomCode = this.state.roomCode;
    orderInfo.qrCodeTypeId = this.state.qrCodeTypeId;

    console.log("orderInfo",orderInfo)
    let newParams = {
      appId: store.getState().ext.appId,
      channelId: 0,
      ...orderInfo,
      orderRequestDTOS: [{
        expressType: orderInfo.expressType,
        itemDTOList: orderInfo.itemDTOList,
        userAddressId: orderInfo.userAddressId,
      }],
      payChannel: 1,
      wxAppId: store.getState().globalData.maAppId,
      storeId: store.getState().globalData.currentStore.id
    }
    console.log("newParams",newParams)

    // 判断订阅消息模板
    const Ids = [];
    // 如果是拼团
    if (activityId || groupNo) {
      // Ids = [subscribeEnum.GROUP_SUCCESS.value];
    }
    // 如果是微信支付，显示支付成功通知
    if (payChannel === PAY_CHANNEL.WX_PAY) {
      // Ids.push(subscribeEnum.PAY_SUCCESS.value);
    }
    // 如果是余额支付，显示余额支付通知
    if (payChannel === PAY_CHANNEL.ACCOUNT) {
      Ids.push(subscribeEnum.BALANCE.value);
    }
    // 如果是快递发货，显示发货通知
    if (choosedDelivery == deliveryWay.EXPRESS.value) {
      // Ids.push(subscribeEnum.DELIVERY.value);
      Ids.push(subscribeEnum.ORDER_SEND.value)
    }


    // 先授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      let url = '';
      let groupUrl = '';
      let scanPayCompletedUrl = '';
      // 是否为扫码购
      if (isScan) {
        orderInfo.type = 1;
      }
      // 是否为处方药
      if (isDrug) {
        orderInfo.prescriptionId = prescriptionId;
      }
      const options = {
        success: (res) => {
          const { base, dispatchUpdateBase } = this.props;
          // 推广任务 && 携带分享Id && 邀请新用户 && 非会员 && 首次发起砍价
          if (base.promotion && base.shareId && base.targetType && !base.vip && base.isFirst) {
            // 4为秒杀 3为拼团
            if (base.taskType == 4) {
              this.addInviteRecord(base.shareId, res.data.orderNo);
            } else {
              this.addInviteRecord(base.shareId, res.data.groupNo);
            }
            dispatchUpdateBase({ isFirst: false });
          }
          // 下单成功后，清除本地购物车数据
          if (goodsInfoList && goodsInfoList.length) {
            goodsInfoList.forEach((item) => {
              // fixme format Cart时，这里会有很多缺失数据，不过没关系，这里只是为了删除购物车用而已
              const cartItem = buyHub.formatCart(item);
              cartItem.count = 0; // count = 0代表删除该条购物车记录
              if (isScan) {
                scanBuyHub.hub.put(scanBuyHub.uuid(cartItem), cartItem, false);
              } else {
                buyHub.hub.put(buyHub.uuid(cartItem), cartItem, false);
              }
            });
          }
          let urlPayType = '微信支付';
          if (payChannel !== PAY_CHANNEL.WX_PAY) {
            urlPayType = '余额支付';
          }

          const urlPrice = moneyUtil.fen2YuanFixed(price.needPayPrice);
          const paySuccessInfo = {
            infoTplName,
            payType: urlPayType,
            itemType: goodsInfoList && goodsInfoList[0].itemType ? goodsInfoList[0].itemType : null,
            money: urlPrice,
            orderNo: res.data.orderNo,
          };

          protectedMailBox.send(pageLinkEnum.orderPkg.paySuccess, 'paySuccessInfo', paySuccessInfo);
          url = pageLinkEnum.orderPkg.paySuccess;

          if (res && res.data && res.data.groupNo) {
            groupUrl = `/sub-packages/marketing-package/pages/group/join/index?groupNo=${res.data.groupNo}`;
          }

          if (isScan) {
            // 0微信支付 1余额支付
            let payType = '微信支付';
            if (payChannel !== PAY_CHANNEL.WX_PAY) {
              payType = '余额支付';
            }
            const needPayPrice = moneyUtil.fen2YuanFixed(price.needPayPrice);
            scanPayCompletedUrl = `/wxat-common/pages/scan-code/pay-completed/index?payType=${payType}&money=${needPayPrice}&orderNo=${res.data.orderNo}`;
          }
          if (payChannel !== PAY_CHANNEL.WX_PAY) {
            wxApi.$redirectTo({
              url: scanPayCompletedUrl || groupUrl || url,
            });
          }
        },
        fail: (res) => {
          this.denyRepeatPay = false;
          if (res.data.errorCode === errorCodeFilter.groupFullCode.value && groupNo) {
            // 打开参团人数已满提示对话弹框
            this.handleGroupFullDialog();
          } else if (res.data.errorCode === errorCodeFilter.seckillRemainLackCode.value) {
            // 秒杀活动商品剩余数量不足提示
            return this.errorTip('活动商品剩余数量不足');
          }
        },
        cancel: () => {
          // 余额支付点击取消时
          this.denyRepeatPay = false;
        },
        wxPaySuccess: () => {
          // 支付成功跳转到支付成功页面
          if (url) {
            wxApi.$redirectTo({
              url: scanPayCompletedUrl || groupUrl || url,
            });
            // wxApi.$redirectTo({
            //   url: scanPayCompletedUrl || groupUrl || url,
            // });
          }
        },
        wxPayFail: () => {
          // 支付失败跳转到订单列表
          url = '/wxat-common/pages/tabbar-order/index';
          if (infoTplName === 'serve-tpl') {
            url = '/sub-packages/server-package/pages/appointment/list/index';
          }
          if (url) {
            wxApi.$redirectTo({
              url: url + '?tabType=mall_order',
            });
          }
        },
      };

      if (price && price.needPayPrice === 0) {
        payChannel = PAY_CHANNEL.ACCOUNT;
        options.extConfig = {
          hideAccountModal: true,
        };
      } else {
        options.extConfig = {
          hideAccountModal: false,
        };
      }
      // 是否立即进行回调
      options.extConfig.immediateCallback = true;
      if (!this.denyRepeatPay) {
        this.denyRepeatPay = true;
        // 没办法监听到取消，所以采用定时器
        if (process.env.TARO_ENV === 'h5') {
          setTimeout(() => {
            this.denyRepeatPay = false;
          }, 3000);
        }
        if (infoTplName === 'micro-tpl') {
          // pay.handleBuyNow(orderInfo, payChannel, options, api.microOrder.placeAnOrder);
          pay.handleBuyNow(newParams, payChannel, options, api.microOrder.placeAnOrder);
        } else {
          // pay.handleBuyNow(orderInfo, payChannel, options);
          pay.handleBuyNow(newParams, payChannel, options);
        }
      }
    });
  };

  /**
   * 打开参团人数已满提示对话弹框
   */
  handleGroupFullDialog() {
    this.groupFullDialogCOMPT &&
      this.groupFullDialogCOMPT.show({
        scale: 1,
      });
  }

  /**
   * 关闭参团人数已满提示对话弹框
   */
  handleCloseGroupFullDialog() {
    this.groupFullDialogCOMPT && this.groupFullDialogCOMPT.hide();

    // 点击关闭，跳转参团页面
    wxApi.$redirectTo({
      url: '/sub-packages/marketing-package/pages/group/join/index',
      data: {
        groupNo: this.state.groupNo,
      },
    });
  }

  /**
   * 点击发起拼团，跳转拼团列表页面
   */
  handleGoToGroupList() {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/group/list/index',
    });
  }

  handleChooseStore = () => {
    wxApi.$navigateTo({
      url: 'wxat-common/pages/store-list/index',
      data: {
        mailKey: 'payOrderPageKey',
      },
    });
  };

  handlePayChannelChange = (e) => {
    if (e.currentTarget.dataset.payChannel === PAY_CHANNEL.ACCOUNT && this.state.balance == null) {
      this.getBalance();
    }
    this.setState({
      payChannel: e.currentTarget.dataset.payChannel,
    });
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });

    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }
  /**
   * 任务中心邀请上报
   */

  addInviteRecord(shareId, referId) {
    const data = {
      shareId,
      referId,
    };

    wxApi.request({
      url: api.taskCenter.addInviteRecord,
      data,
      method: 'POST',
    });
  }

  // 计算所有商品的价格
  funGoodsTotalPrice = () => {
    return priceHandler.getTotalGoodsPrice(this.state);
  };

  // 需要的内购券张数
  funGoodsInternal = (goodsInfoList) => {
    let internal = 0;
    if (goodsInfoList && goodsInfoList.length) {
      goodsInfoList.forEach((goods) => {
        if (goods.innerBuyCouponNum) {
          internal += goods.itemCount * goods.innerBuyCouponNum;
        }
      });
    }
    return internal;
  };

  // 需要的积分
  funGoodsIntegral = (goodsInfoList) => {
    let integral = 0;
    if (goodsInfoList && goodsInfoList.length) {
      goodsInfoList.forEach((goods) => {
        if (goods.skuIntegral) {
          integral += goods.itemCount * goods.skuIntegral;
        }
      });
    }
    return integral;
  };

  funAddress = (curAddressData) => {
    if (!curAddressData) {
      return '';
    }
    return `${curAddressData.province || ''}${curAddressData.city || ''}${curAddressData.region || ''}${
      curAddressData.address || ''
    }${curAddressData.roomNumber || ''}`;
  };

  funHasStockGoodsList = (stockMap, goodsInfoList) => {
    if (stockMap && goodsInfoList && goodsInfoList.length) {
      return goodsInfoList.filter((item) => {
        return !!item.itemStock && !!item.isShelf;
      });
    }
    return [];
  };

  funNoStockGoodsList = (stockMap, goodsInfoList) => {
    if (stockMap && goodsInfoList && goodsInfoList.length) {
      return goodsInfoList.filter((item) => {
        return !item.itemStock || !item.isShelf;
      });
    }
    return [];
  };

  funIsOpenInvoice = (appInfo) => {
    return false;
    // return appInfo && appInfo.useInvoice;
  };

  funIsExpress = (choosedDelivery) => {
    return choosedDelivery === deliveryWay.EXPRESS.value;
  };

  funIsError(price, beyoundScope, isDrug, prescriptionId) {
    return (
      price &&
      (price.deliveryCostFee > price.totalPrice || // 同城配送未达到起送价格
        beyoundScope || // 超出配送范围
        !this.state.payable || // 计算价格失败
        (isDrug && !prescriptionId))
    );

    // 处方药未填写处方信息
  }

  refChooseCardDialogCOMP = (node) => (this.chooseCardDialogCOMP = node);
  refGroupFullDialogCOMPT = (node) => (this.groupFullDialogCOMPT = node);

  render() {
    const { appInfo, scoreName, storeInfo } = this.props;
    const {
      isDistributionServices,
      tmpStyle,
      isCardpackType,
      currentStore,
      roomCode,
      needScan,
      isRoomScan,
      infoTplName,
      activityId,
      groupNo,
      goodsInfoList,
      trueCode,
      categoryType,
      choosedDelivery,
      isFrontMoneyItem,
      curAddressData,
      isScan,
      payChannel,
      balance,
      isNeedPay,
      price,
      payDetails,
      curGiftCard,
      coupons,
      curCoupon,
      serveCard,
      curRedPacke,
      orderMessage,
      isGiftActivity,
      selectAddress,
      isCoupon,
      bgGroupFullImg,
      stockMap,
      beyoundScope,
      isDrug,
      prescriptionId,
      errorText,
      freightType,
      allFreightCollect,
      supportDelivery,
      useLogisticsCoupon,
      seckillNo,
      orderNotes,
      isPromoter,
    } = this.state;

    // 计算所有商品的价格
    const goodsTotalPrice = this.funGoodsTotalPrice();

    // 需要的内购券张数
    const goodsInternal = this.funGoodsInternal(goodsInfoList);

    // 需要的积分
    const goodsIntegral = this.funGoodsIntegral(goodsInfoList);
    const address = this.funAddress(curAddressData);
    const hasStockGoodsList = this.funHasStockGoodsList(stockMap, goodsInfoList);
    const noStockGoodsList = this.funNoStockGoodsList(stockMap, goodsInfoList);
    const isOpenInvoice = this.funIsOpenInvoice(appInfo);
    const isExpress = this.funIsExpress(choosedDelivery);
    const isError = this.funIsError(price, beyoundScope, isDrug, prescriptionId);
    const icLocationDarkPng = getStaticImgUrl.images.ic_location_dark_png;
    const rightAngleGrayPng = getStaticImgUrl.images.rightAngleGray_png;
    return (
      <ScrollView
        scrollY
        data-fixme='02 block to view. need more test' data-scoped='wk-opp-PayOrder' className='wk-opp-PayOrder'>
        {!selectAddress || isScan ? (
          <View className='pay-order-container'>
            <Form reportSubmit='true'>
              {isCardpackType ? (
                <Block>
                  <View className='card-pack-tips'>代金卡包选定指定门店后，仅可在对应门店使用，请谨慎选择</View>
                  {/*  门店自提  */}
                  {(!!(currentStore && currentStore && infoTplName === 'goods-tpl') && !(categoryType == 5 && needScan)) && (
                    <View className='self-delivery-store-box' onClick={this.handleChooseStore}>
                      <View className='store-box'>
                        <View className='delivery-store-info-item store-name limit-line'>
                          <Image className='ic-location-dark' src={icLocationDarkPng}></Image>
                          {'取货门店：' + (currentStore.abbreviation || currentStore.name || '-')}
                        </View>
                        <View className='delivery-store-info-item phone'>{'电话：' + (currentStore.tel || '-')}</View>
                        <View className='delivery-store-info-item address limit-line line-2'>
                          <Text className='address-title'>地址：</Text>
                          <Text className='address-text'>{currentStore.address || '-'}</Text>
                        </View>
                      </View>
                      <Image className='right-icon' src={rightAngleGrayPng}></Image>
                    </View>
                  )}
                </Block>
              ) : (
                <Block>
                  {!isScan ? (
                    <View className='top'>
                      {!!(activityId || groupNo) && (
                        <View className='group-steps'>
                          <View className='tab'>
                            <View className='step' style={_safe_style_('background-color:' + tmpStyle.btnColor)}>
                              1
                            </View>
                            <View
                              className='dotted-line'
                              style={_safe_style_('border-color:' + tmpStyle.bgColor)}
                            ></View>
                            <View className='step-text'>选择商品开团/参团</View>
                          </View>
                          <View className='tab'>
                            <View className='step' style={_safe_style_('background-color:' + tmpStyle.btnColor)}>
                              2
                            </View>
                            <View
                              className='dotted-line'
                              style={_safe_style_('border-color:' + tmpStyle.bgColor)}
                            ></View>
                            <View className='step-text'>邀请好友参团</View>
                          </View>
                          <View className='tab'>
                            <View className='step' style={_safe_style_('background-color:' + tmpStyle.btnColor)}>
                              3
                            </View>
                            <View
                              className='dotted-line'
                              style={_safe_style_('border-color:' + tmpStyle.bgColor)}
                            ></View>
                            <View className='step-text'>人满成团</View>
                          </View>
                        </View>
                      )}

                      {/*  订购顶部配送方式信息  */}
                      {infoTplName === 'micro-tpl' && (
                        <MicroDeliveryTmpl
                          goodsInfoList={goodsInfoList}
                          deliveryWay={deliveryWay}
                          onAddAddress={this.addAddress}
                        ></MicroDeliveryTmpl>
                      )}

                      {/*  选择配送方式  */}
                      {(infoTplName === 'goods-tpl' && categoryType != 5 && !isDistributionServices ) && (
                        <View className='delivery-way-box'>
                          <View className='delivery-way-label'>配送</View>
                          <View className='delivery-way-choose'>
                            {!!(!isFrontMoneyItem && supportDelivery.supportLocal) && (
                              <View
                                className={classNames(
                                  'delivery-name',
                                  choosedDelivery == deliveryWay.CITY_DELIVERY.value ? 'delivery-choosed' : ''
                                )}
                                onClick={_fixme_with_dataset_(this.handleChooseDelviery, {
                                  choosed: deliveryWay.CITY_DELIVERY.value,
                                })}
                                style={_safe_style_(
                                  'background:' +
                                    (choosedDelivery === deliveryWay.CITY_DELIVERY.value ? tmpStyle.btnColor : '')
                                )}
                              >
                                同城配送
                              </View>
                            )}

                            {!!supportDelivery.supportPickUp && (
                              <View
                                className={classNames(
                                  'delivery-name',
                                  choosedDelivery == deliveryWay.SELF_DELIVERY.value ? 'delivery-choosed' : ''
                                )}
                                onClick={_fixme_with_dataset_(this.handleChooseDelviery, {
                                  choosed: deliveryWay.SELF_DELIVERY.value,
                                })}
                                style={_safe_style_(
                                  'background:' +
                                    (choosedDelivery === deliveryWay.SELF_DELIVERY.value ? tmpStyle.btnColor : '')
                                )}
                              >
                                自提
                              </View>
                            )}

                            {!!(!isFrontMoneyItem && supportDelivery.supportExpress) && (
                              <View
                                className={classNames(
                                  'delivery-name',
                                  choosedDelivery == deliveryWay.EXPRESS.value ? 'delivery-choosed' : ''
                                )}
                                onClick={_fixme_with_dataset_(this.handleChooseDelviery, {
                                  choosed: deliveryWay.EXPRESS.value,
                                })}
                                style={_safe_style_(
                                  'background:' +
                                    (choosedDelivery === deliveryWay.EXPRESS.value ? tmpStyle.btnColor : '')
                                )}
                              >
                                快递
                              </View>
                            )}
                          </View>
                        </View>
                      )}

                      {/*  提货时间  */}
                      {!!(
                        choosedDelivery === deliveryWay.SELF_DELIVERY.value &&
                        goodsInfoList &&
                        goodsInfoList.length &&
                        goodsInfoList[0] &&
                        goodsInfoList[0].verificationStartTime &&
                        goodsInfoList[0].verificationEndTime
                      ) && (
                        <View className='verification-time-box'>
                          <View className='verification-time-label'>提货时间</View>
                          <View className='verification-time'>
                            {filters.dateFormat(goodsInfoList[0].verificationStartTime, 'yyyy-MM-dd hh:mm') +
                              ' 至 ' +
                              filters.dateFormat(goodsInfoList[0].verificationEndTime, 'yyyy-MM-dd hh:mm')}
                          </View>
                        </View>
                      )}

                      {/*  门店自提  */}
                      {(!!(
                        choosedDelivery === deliveryWay.SELF_DELIVERY.value &&
                        currentStore &&
                        currentStore &&
                        infoTplName === 'goods-tpl'
                      ) && !(categoryType == 5 && needScan)) && (
                        <View className='self-delivery-store-box'>
                          <View className='store-box'>
                            <View className='delivery-store-info-item store-name limit-line'>
                              <Image
                                className='ic-location-dark'
                                src={icLocationDarkPng}
                              ></Image>
                              {'取货门店：' + (currentStore.abbreviation || currentStore.name || '-')}
                            </View>
                            <View className='delivery-store-info-item phone'>
                              {'电话：' + (currentStore.tel || '-')}
                            </View>
                            <View className='delivery-store-info-item address limit-line line-2'>
                              <Text className='address-title'>地址：</Text>
                              <Text className='address-text'>{currentStore.address || '-'}</Text>
                            </View>
                          </View>
                        </View>
                      )}

                      {/*  快递配送  */}
                      {!!(
                        (choosedDelivery === deliveryWay.EXPRESS.value ||
                          choosedDelivery === deliveryWay.CITY_DELIVERY.value) &&
                        infoTplName === 'goods-tpl'
                      ) && (
                        <View className='address-box'>
                          <View className={(curAddressData ? 'global-hidden ' : '') + 'add-address'}>
                            <View className='title' onClick={this.addAddress}>
                              新增收货地址
                            </View>
                          </View>
                          {!!curAddressData && (
                            <View className='show-address' onClick={this.addAddress}>
                              <View className='name-tel'>
                                <Image
                                  className='ic-location-dark'
                                  src={icLocationDarkPng}
                                ></Image>
                                {'收货人：' + curAddressData.consignee}
                              </View>
                              <View className='addr-phone'>{curAddressData.mobile}</View>
                              <View className='addr-text'>{address}</View>
                            </View>
                          )}

                          <Image className='right-icon' src={rightAngleGrayPng}></Image>
                        </View>
                      )}

                      {/* 扫码 */}
                      {!!trueCode &&
                        ((choosedDelivery === deliveryWay.SELF_DELIVERY.value &&
                          storeInfo &&
                          storeInfo &&
                          infoTplName === 'goods-tpl') ||
                          (choosedDelivery === deliveryWay.CITY_DELIVERY.value &&
                            infoTplName === 'goods-tpl' &&
                            curAddressData)) &&
                        isRoomScan && (
                          <View onClick={this.scanCode} className="scan-wrap">
                            <View>配送位置</View>
                            <Text style={_safe_style_('text-align:right;flex:1;color:#8893a6')}>{trueCode}</Text>
                            {!trueCode && (
                              <Image
                                style={_safe_style_(
                                  'background-color:' +
                                    (tmpStyle.btnColor || 'rgba(243, 95, 40, 0.8)') +
                                    ";background-image: url('https://front-end-1302979015.file.myqcloud.com/images/scan.svg')"
                                )}
                                className="mask-img"
                              ></Image>
                            )}
                          </View>
                        )}

                      {(!!trueCode &&
                        ((choosedDelivery === deliveryWay.SELF_DELIVERY.value &&
                          storeInfo &&
                          storeInfo &&
                          infoTplName === 'goods-tpl') ||
                          (choosedDelivery === deliveryWay.CITY_DELIVERY.value &&
                            infoTplName === 'goods-tpl' &&
                            curAddressData)) &&
                        !isRoomScan || needScan) && (
                          <View className="scan-wrap">
                            <View>配送位置<Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text></View>
                            <Input
                              value={trueCode}
                              style={_safe_style_('text-align:right;flex:1')}
                              placeholder="请输入位置信息"
                              onInput={this.inputRoomCode}
                            ></Input>
                            {/*  <image wx:if="{{!trueCode}}"  style="background-color:{{ tmpStyle.btnColor || 'rgba(243, 95, 40, 0.8)' }};background-image: url('https://front-end-1302979015.file.myqcloud.com/images/scan.svg')" class="mask-img"></image>  */}
                          </View>
                        )}


                    </View>
                  ) : (
                    <View className='scan-header-tip' style={_safe_style_('background:' + tmpStyle.btnColor)}>
                      <Image
                        style={_safe_style_('width: 50rpx; height: 50rpx;margin-right: 8rpx;')}
                        src={scan_codeImg.scan}
                      ></Image>
                      <View>扫码购订单</View>
                    </View>
                  )}

                  {/*  扫码购订单  */}
                </Block>
              )}

              {/*  代金卡包订单  */}
              {/*  不是地产订单  */}
              {/*  选择支付方式  */}
              { categoryType != 5 && !isDistributionServices && (
                  <View className='box-pay-channel'>
                    <View className='pay-title'>支付方式</View>
                    <View className='pay-channel-group'>
                      <View
                        className={classNames('pay-channel', payChannel === PAY_CHANNEL.WX_PAY ? 'pay-checked' : '')}
                        style={_safe_style_(
                          'background:' +
                            (payChannel === PAY_CHANNEL.WX_PAY ? tmpStyle.bgColor : '') +
                            ';color:' +
                            (payChannel === PAY_CHANNEL.WX_PAY ? tmpStyle.btnColor : '') +
                            ';border-color:' +
                            (payChannel === PAY_CHANNEL.WX_PAY ? tmpStyle.btnColor : '')
                        )}
                        onClick={_fixme_with_dataset_(this.handlePayChannelChange, { payChannel: PAY_CHANNEL.WX_PAY })}
                      >
                        微信支付
                      </View>
                      {/* {!(isCardpackType || isPromoter) && (
                        <View
                          className={classNames(
                            'pay-channel pay-accout',
                            payChannel === PAY_CHANNEL.ACCOUNT ? 'pay-checked' : ''
                          )}
                          style={_safe_style_(
                            'background:' +
                              (payChannel === PAY_CHANNEL.ACCOUNT ? tmpStyle.bgColor : '') +
                              ';color:' +
                              (payChannel === PAY_CHANNEL.ACCOUNT ? tmpStyle.btnColor : '') +
                              ';border-color:' +
                              (payChannel === PAY_CHANNEL.ACCOUNT ? tmpStyle.btnColor : '')
                          )}
                          onClick={_fixme_with_dataset_(this.handlePayChannelChange, { payChannel: PAY_CHANNEL.ACCOUNT })}
                        >
                          <View className='pay-accout-text'>余额支付</View>
                          <View className='pay-accout-balance'>{'￥' + filters.moneyFilter(balance, true)}</View>
                        </View>
                      )} */}

                      {!isNeedPay && (
                        <View
                          className={classNames('pay-channel', payChannel == 5 ? 'pay-checked' : '')}
                          style={_safe_style_(
                            'margin-top:20rpx;' +
                              'background:' +
                              (payChannel === 5 ? tmpStyle.bgColor : '') +
                              ';color:' +
                              (payChannel === 5 ? tmpStyle.btnColor : '') +
                              ';border-color:' +
                              (payChannel === 5 ? tmpStyle.btnColor : '')
                          )}
                          onClick={_fixme_with_dataset_(this.handlePayChannelChange, { payChannel: '5' })}
                        >
                          到店支付
                        </View>
                      )}
                    </View>
                  </View>
                )
              }

              {/*  订购信息  */}
              {infoTplName === 'micro-tpl' && <MicroTplTmpl goodsInfoList={goodsInfoList}></MicroTplTmpl>}

              {/*  商品信息  */}
              <Block>
                {!!(
                  infoTplName === 'goods-tpl' &&
                  choosedDelivery == deliveryWay.EXPRESS.value &&
                  goodsInfoList &&
                  goodsInfoList.length &&
                  !curAddressData
                ) && (
                  <Goods
                    goodsInfoList={goodsInfoList}
                    address
                    tmpStyle={tmpStyle}
                    onIncr={this.handleIncreaseBuyNum}
                    onDecr={this.handleDecreaseBuyNum}
                    isExpress={isExpress}
                    freightType={freightType}
                  ></Goods>
                )}

                {!!(
                  ((infoTplName === 'goods-tpl' && curAddressData) || choosedDelivery !== deliveryWay.EXPRESS.value) &&
                  price
                ) && (
                  <View>
                    {/*  未达到配送门槛  */}
                    {!!(
                      price.deliveryCostFee > price.totalPrice &&
                      (!noStockGoodsList || !noStockGoodsList.length)
                    ) && (
                      <View className='no-stock-label' style={_safe_style_('background:' + tmpStyle.btnColor)}>
                        <View>
                          {'起送' +
                            filters.moneyFilter(price.deliveryCostFee, true) +
                            '元，再消费' +
                            filters.moneyFilter(price.deliveryCostFee - price.totalPrice, true) +
                            '元可配送'}
                        </View>
                      </View>
                    )}

                    {/*  超出配送范围  */}
                    {!!beyoundScope && (
                      <View className='no-stock-label' style={_safe_style_('background:' + tmpStyle.btnColor)}>
                        <View>该收货地址超出配送范围，请切换或更改地址</View>
                      </View>
                    )}

                    {/*  商品信息  */}
                    {!!(infoTplName === 'goods-tpl' && hasStockGoodsList && hasStockGoodsList.length) && (
                      <Goods
                        goodsInfoList={hasStockGoodsList}
                        address
                        tmpStyle={tmpStyle}
                        onIncr={this.handleIncreaseBuyNum}
                        onDecr={this.handleDecreaseBuyNum}
                        isExpress={isExpress}
                        freightType={freightType}
                      ></Goods>
                    )}

                    {/*  无库存商品信息  */}
                    {!!(
                      (infoTplName === 'goods-tpl' &&
                      noStockGoodsList &&
                      noStockGoodsList.length &&
                      !beyoundScope) && categoryType != 5
                    ) && (
                      <View className='no-stock-label' style={_safe_style_('background:' + tmpStyle.btnColor)}>
                        <View>以下商品暂时缺货或已下架，请编辑后下单</View>
                        <View className='del-btn' onClick={this.handleClearNoStockGoods}>
                          一键移除
                        </View>
                      </View>
                    )}

                    {!!(infoTplName === 'goods-tpl' && noStockGoodsList && noStockGoodsList.length) && (
                      <Goods
                        goodsInfoList={noStockGoodsList}
                        address
                        tmpStyle={tmpStyle}
                        onIncr={this.handleIncreaseBuyNum}
                        onDecr={this.handleDecreaseBuyNum}
                        isExpress={isExpress}
                        freightType={freightType}
                      ></Goods>
                    )}
                  </View>
                )}

                {/* 定金展示 */}
                {!!(
                  goodsInfoList &&
                  goodsInfoList.length &&
                  goodsInfoList[0].wxItem &&
                  goodsInfoList[0].wxItem.frontMoneyItem
                ) && (
                  <FrontMoneyState
                    frontMoney={
                      (goodsInfoList[0].sku ? goodsInfoList[0].sku.frontMoney : goodsInfoList[0].wxItem.frontMoney) *
                      goodsInfoList[0].itemCount
                    }
                    restMoney={
                      price ? price.restMoney : goodsInfoList[0].wxItem.salePrice - goodsInfoList[0].wxItem.frontMoney
                    }
                  ></FrontMoneyState>
                )}

                {/* 间隔 */}
                <View className='divider'></View>
              </Block>

              {/*  发票需求  */}
              {!!isOpenInvoice && (
                <View className='receipt-box'>
                  <View className='title-label'>发票需求</View>
                  <View className='msg-label'>
                    如需发票，请在交易订单完成后在相应订单处申请，我们将为您提供电子发票
                  </View>
                </View>
              )}

              {/*  优惠信息  */}
              <View className='shipping-method'>
                {/*  先减再折扣，满减在会员折扣上面  */}
                {!!price.fullDecrementPromFee && (
                  <View className='row-box'>
                    <View className='row-label'>满减</View>
                    <View className='right-text'>减￥{filters.moneyFilter(price.fullDecrementPromFee, true)}</View>
                  </View>
                )}

                {payDetails.map((item, index) => {
                  return (
                    <Block key={index}>
                      {item.isCustom ? (
                        <View className='row-box'>
                          <View className='row-label'>{item.label}</View>
                          <View className='right-text'>{item.value}</View>
                        </View>
                      ) : (
                        <Block>
                          {item.name === 'gift-card' && (
                            <View className='row-box' onClick={this.handleGoToGiftCardList}>
                              <View className='row-label'>礼品卡</View>
                              {curGiftCard && curGiftCard.cardItemName ? (
                                <View className='right-text'>
                                  {'抵扣￥' + filters.moneyFilter(price.giftCardPrice, true)}
                                </View>
                              ) : coupons && coupons.userGiftCardDTOSet && coupons.userGiftCardDTOSet.length ? (
                                <View className='right-text'>{'可用' + coupons.userGiftCardDTOSet.length + '张'}</View>
                              ) : (
                                <View className='right-text'>无可用</View>
                              )}

                              <Image className='right-icon' src={rightAngleGrayPng}></Image>
                            </View>
                          )}

                          {!!(item.name === 'coupon' && !seckillNo) && (
                            <View className='row-box' onClick={this.handleGoToCouponList}>
                              <View className='row-label'>优惠券</View>
                              {curCoupon && curCoupon.name ? (
                                <View className='right-text'>
                                  {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                    <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                  )}

                                  {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                                </View>
                              ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                                <View className='right-text'>{'可用' + coupons.couponSet.length + '张'}</View>
                              ) : (
                                <View className='right-text'>无可用</View>
                              )}

                              <Image className='right-icon' src={rightAngleGrayPng}></Image>
                            </View>
                          )}

                          {!!(
                            item.name === 'coupon' &&
                            useLogisticsCoupon == 1 &&
                            seckillNo &&
                            choosedDelivery !== deliveryWay.SELF_DELIVERY.value
                          ) && (
                            <View className='row-box' onClick={this.handleGoToCouponList}>
                              <View className='row-label'>运费券</View>
                              {curCoupon && curCoupon.name ? (
                                <View className='right-text'>
                                  {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                    <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                  )}

                                  {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                                </View>
                              ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                                <View className='right-text'>{'可用' + coupons.couponSet.length + '张'}</View>
                              ) : (
                                <View className='right-text'>无可用</View>
                              )}

                              <Image className='right-icon' src={rightAngleGrayPng}></Image>
                            </View>
                          )}

                          {item.name === 'card' && (
                            <View className='row-box' onClick={this.handleGoToCard}>
                              <View className='row-label'>次数卡</View>
                              <View className='right-text'>{serveCard.cardItemName}</View>
                              <Image className='right-icon' src={rightAngleGrayPng}></Image>
                            </View>
                          )}

                          {item.name === 'redpacke' && (
                            <View className='row-box' onClick={this.handleRedPackeList}>
                              <View className='row-label'>红包</View>
                              {curRedPacke && curRedPacke.code ? (
                                <View className='right-text'>
                                  {'减￥' + filters.moneyFilter(price.redPacketPrice, true)}
                                </View>
                              ) : coupons && coupons.redPacketSet && coupons.redPacketSet.length ? (
                                <View className='right-text'>{'可用' + coupons.redPacketSet.length + '个'}</View>
                              ) : (
                                <View className='right-text'>无可用</View>
                              )}

                              <Image className='right-icon' src={rightAngleGrayPng}></Image>
                            </View>
                          )}

                          {item.name === 'discount' && (
                            <View className='row-box'>
                              <View className='row-label'>会员折扣</View>
                              <View className='right-text'>
                                {(coupons.memberDiscount && coupons.memberDiscount < 100
                                  ? coupons.memberDiscount / 10 + '折'
                                  : '无') +
                                  ' 减￥' +
                                  filters.moneyFilter(price.memberPrice, true)}
                              </View>
                            </View>
                          )}

                          {item.name === 'integral' && (
                            <View className='row-box'>
                              <View className='row-label'>{scoreName}抵扣</View>
                              <View className='right-text'>
                                {(goodsIntegral || '') + scoreName + '减￥' + filters.moneyFilter(price.integralPromFee, true)}
                              </View>
                            </View>
                          )}

                          {item.name === 'innerBuy' && (
                            <View className='row-box'>
                              <View className='row-label'>内购券</View>
                              <View className='right-text'>
                                {goodsInternal + '张减￥' + filters.moneyFilter(price.innerBuyPromFee, true)}
                              </View>
                            </View>
                          )}

                          {item.name === 'seckill' && (
                            <View className='row-box'>
                              <View className='row-label'>秒杀优惠</View>
                              <View className='right-text'>
                                {'减￥' + filters.moneyFilter(price.secKillPromFee, true)}
                              </View>
                            </View>
                          )}
                        </Block>
                      )}
                    </Block>
                  );
                })}

                {!!price.groupPromFee && (
                  <View className='row-box'>
                    <View className='row-label'>拼团优惠</View>
                    <View className='right-text'>{'-¥ ' + filters.moneyFilter(price.groupPromFee, true)}</View>
                  </View>
                )}

                {!!isCardpackType && (
                  <Block>
                    <View className='row-box'>
                      <View className='row-label'>卡包明细</View>
                    </View>
                    {goodsInfoList[0].wxItem.couponInfos.map((item, index) => {
                      return (
                        <Block key={index}>
                          <View className='row-box card-pack-box'>
                            <View className='label'>{item.name}</View>
                            <View className='right-text'>{(item.couponNum || 0) + '张'}</View>
                          </View>
                        </Block>
                      );
                    })}
                  </Block>
                )}

                <View className='row-box' style={_safe_style_('margin-top: 20rpx;')}>
                  <View className='row-label'>留言备注</View>
                  <View className='right-text'>
                    <Input
                      name='remark'
                      type='text'
                      className='note'
                      placeholderClass='placeholder'
                      placeholder='建议留言前先与商家沟通确认'
                      onBlur={this.handleMessageInput}
                      value={orderMessage}
                    ></Input>
                  </View>
                </View>
                {/* 订单备注 */}
                {orderNotes.map((item, index) => {
                  return (
                    <Block key={index}>
                      <View className='row-box'>
                        <View className={classNames('noteKey-left-text', { required: item.isMust })}>
                          {item.noteKey}:
                        </View>
                        <View className='right-text'>
                          <Input
                            type='text'
                            className='noteKey'
                            placeholder={`请输入${item.noteKey}`}
                            onBlur={this.noteValueInput(index)}
                            value={item.noteValue}
                          ></Input>
                        </View>
                      </View>
                    </Block>
                  );
                })}

                <Block>
                  <View className='row-box' style={_safe_style_('margin-top: 20rpx;')}>
                    <View className='row-label'>商品原价</View>
                    {/*  <View wx:if="{{goodsInfoList[0].seckillPrice> (price.totalPrice||goodsTotalPrice)}}" class="right-text">¥ {{filters.moneyFilter(goodsInfoList[0].seckillPrice,true)}}</View>  */}
                    <View className='right-text'>
                      {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                    </View>
                  </View>
                </Block>
                {payDetails.map((item, index) => {
                  return (
                    <Block key={index}>
                      {!!(item.name === 'freight' && choosedDelivery === deliveryWay.EXPRESS.value) && (
                        <View className='row-box'>
                          <View className='row-label'>运费</View>
                          {price.freight && price.freight > 0 ? (
                            <View className='right-text'>{'￥' + filters.moneyFilter(price.freight, true)}</View>
                          ) : (
                            <View className='right-text'>{allFreightCollect ? '到付' : '包邮'}</View>
                          )}
                        </View>
                      )}
                    </Block>
                  );
                })}
                {choosedDelivery === deliveryWay.CITY_DELIVERY.value && (
                  <View className='row-box'>
                    <View className='row-label'>配送费</View>
                    {price.freight && price.freight > 0 ? (
                      <View className='right-text'>{'￥' + filters.moneyFilter(price.freight, true)}</View>
                    ) : (
                      <View className='right-text'>免配送费</View>
                    )}
                  </View>
                )}

                <View className='row-box'>
                  <View className='row-label'>共优惠</View>
                  <View className='right-text'>
                    {'- ¥ ' +
                      (isGiftActivity
                        ? filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)
                        : filters.moneyFilter(price.totalFreePrice || 0, true) < 0
                        ? 0
                        : filters.moneyFilter(price.totalFreePrice || 0, true))}
                  </View>
                </View>
                {!!(price && isFrontMoneyItem) && (
                  <View className='row-box'>
                    <View className='row-label'>尾款</View>
                    <View className='right-text'>{'¥ ' + filters.moneyFilter(price.restMoney, true)}</View>
                  </View>
                )}
              </View>

              {/*  错误信息  */}
              {!!(isDrug && !prescriptionId) && <View className='error-blank'></View>}
              {!!(isDrug && !prescriptionId) && <View className='error-text'>{errorText}</View>}

              {/*  支付按钮  */}
              <View className={classNames('pay-box', screen.isFullScreenPhone ? 'fix-full-screen' : '')}>
                <View className='total-price'>
                  <Text>
                    {goodsInfoList &&
                    goodsInfoList.length &&
                    goodsInfoList[0].wxItem &&
                    goodsInfoList[0].wxItem.frontMoneyItem
                      ? '需付定金：'
                      : '合计：'}
                  </Text>
                  <Text className='price-logo'>¥</Text>
                  <Text className='price-num'>{filters.moneyFilter(price ? price.needPayPrice : 0, true)}</Text>
                </View>
                <Button
                  className='to-pay-btn'
                  style={_safe_style_('background:' + (isError ? '#cccccc' : tmpStyle.btnColor))}
                  onClick={this.handlePay}
                >
                  {isGiftActivity ? '免费领取' : '确认支付'}
                </Button>
              </View>
            </Form>
          </View>
        ) : (
          <AddressList onChange={this.handleAddressChange} />
        )}

        {/*  选择优惠对话框  */}
        <BottomDialog ref={this.refChooseCardDialogCOMP} customClass='bottom-dialog-custom-class'>
          <View>
            {isCoupon === 4 && (
              <View className='coupon-select'>
                {!!coupons &&
                  !!coupons.userGiftCardDTOSet &&
                  (coupons.userGiftCardDTOSet || []).map((item, index) => {
                    return (
                      <View
                        className='coupon-tab card-tab'
                        key={item.id}
                        onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                      >
                        <View className='coupon-tab-top'>
                          <Text className='coupon-name limit-line'>{item.cardItemName}</Text>
                          <Text className='coupon-validityDate'>
                            {'有效期：' + (item.validityDate ? item.validityDate : '永久')}
                          </Text>
                        </View>
                        <View className='coupon-tab-bottom'>
                          余额<Text className='price-logo'>￥</Text>
                          <Text className='price-num'>{filters.moneyFilter(item.balance, true)}</Text>
                        </View>
                      </View>
                    );
                  })}
                <View className='no-use-coupon' onClick={this.handleNoUseCard}>
                  不使用礼品卡
                </View>
              </View>
            )}

            {/* 优惠券优惠类型 */}
            {isCoupon === 1 && (
              <View className='coupon-select'>
                {!!coupons &&
                  !!coupons.couponSet &&
                  coupons.couponSet.length &&
                  (coupons.couponSet || []).map((item, index) => {
                    return (
                      <View
                        className={classNames('coupon-tab', 'couponSet-tab', !item.status ? 'disable-coupon' : '')}
                        key={item.code}
                        onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                      >
                        <View className='coupon-img-box'>
                          {!item.status ? (
                            <Image className='coupon-img' src={couponImg.bgGray}></Image>
                          ) : item.couponCategory === 1 ? (
                            <Image className='coupon-img' src={couponImg.bgBlue}></Image>
                          ) : (
                            <Image className='coupon-img' src={couponImg.bgGren}></Image>
                          )}
                        </View>
                        <View className='coupon-tab-left'>
                          {item.discountFee === 0 ? (
                            <View className='coupon-freeFreight'>免运费</View>
                          ) : (
                            <View className='coupon-price'>
                              {!!(
                                item.couponCategory === couponEnum.TYPE.freight.value ||
                                item.couponCategory === couponEnum.TYPE.fullReduced.value
                              ) && (
                                <Block>
                                  <Text className='price-logo'>￥</Text>
                                  <Text className='price-num'>{filters.moneyFilter(item.discountFee, true)}</Text>
                                </Block>
                              )}

                              {item.couponCategory === couponEnum.TYPE.discount.value && (
                                <Block>
                                  <Text className='price-num'>{filters.discountFilter(item.discountFee, true)}</Text>
                                  <Text className='price-logo'>折</Text>
                                </Block>
                              )}
                            </View>
                          )}

                          {item.minimumFee === 0 ? (
                            <View className='coupon-minimumFee'>无门槛</View>
                          ) : (
                            <View className='coupon-minimumFee'>
                              {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                            </View>
                          )}

                          {item.couponCategory === 1 ? (
                            <View className='coupon-type'>运费券</View>
                          ) : (
                            <View className='coupon-type'>满减券</View>
                          )}
                        </View>
                        <View className='coupon-tab-right'>
                          <View className='coupon-name limit-line'>{item.name}</View>
                          {!!item.endTime && (
                            <View className='coupon-validityDate'>{'有效期：' + filters.dateFormat(item.endTime)}</View>
                          )}
                        </View>
                      </View>
                    );
                  })}
                <View className='no-use-coupon' onClick={this.handleNoUseCard}>
                  不使用优惠
                </View>
              </View>
            )}

            {/* 红包优惠类型 */}
            {isCoupon === 2 && (
              <View className='coupon-select'>
                {coupons &&
                  coupons.redPacketSet &&
                  coupons.redPacketSet.length &&
                  (coupons.redPacketSet || []).map((item, index) => {
                    return (
                      <View
                        className={classNames('coupon-tab', 'redPacketSet-tab', !item.status ? 'disable-coupon' : '')}
                        key={item.code}
                        onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                      >
                        <View className='coupon-tab-top'>
                          {item.thresholdFee === 0 ? (
                            <View className='coupon-name limit-line'>无门槛</View>
                          ) : (
                            <View className='coupon-name limit-line'>
                              {'满' + filters.moneyFilter(item.thresholdFee, true) + '可用'}
                            </View>
                          )}

                          {!!item.endTime && (
                            <View className='coupon-validityDate'>
                              {'有效期：' + filters.dateFormat(item.endTime, 'yyyy-MM-dd hh:mm:ss')}
                            </View>
                          )}
                        </View>
                        <View className='coupon-tab-bottom'>
                          金额<Text className='price-logo'>￥</Text>
                          <Text className='price-num'>{filters.moneyFilter(item.luckMoneyFee, true)}</Text>
                        </View>
                      </View>
                    );
                  })}
                <View className='no-use-coupon' onClick={this.handleNoUseCard}>
                  不使用红包
                </View>
              </View>
            )}

            {/* 卡项优惠类型 */}
            {isCoupon === 3 && (
              <View className='coupon-select'>
                {coupons &&
                  coupons.userCardDTOSet &&
                  coupons.userCardDTOSet.length &&
                  (coupons.userCardDTOSet || []).map((item) => {
                    return (
                      <View
                        className='coupon-tab card-tab'
                        key={item.id}
                        onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                      >
                        <View className='coupon-tab-top'>
                          <Text className='coupon-name'>{item.cardItemName}</Text>
                          <Text className='coupon-validityDate'>
                            {'有效期：' + (item.validityDate ? item.validityDate : '永久')}
                          </Text>
                        </View>
                        <View className='coupon-tab-bottom'>
                          余额<Text className='price-logo'>￥</Text>
                          <Text className='price-num'>{filters.moneyFilter(item.balance, true)}</Text>
                        </View>
                      </View>
                    );
                  })}
                <View className='no-use-coupon' onClick={this.handleNoUseCard}>
                  不使用优惠
                </View>
              </View>
            )}
          </View>
        </BottomDialog>
        {/*  参团人数已满提示对话弹框  */}
        <AnimatDialog ref={this.refGroupFullDialogCOMPT} animClass='group-full-dialog'>
          <View className='group-full'>
            <Image className='bg-group-full' src={bgGroupFullImg}></Image>
            <View className='group-full-content'>
              <View className='group-full-title'>参团人数已满</View>
              <View className='group-full-text'>快去发起自己的拼团吧</View>
            </View>
            <View className='btn-group-full' onClick={this.handleGoToGroupList.bind(this)}>
              发起拼团
            </View>
          </View>
          <Image
            className='icon-group-full-close'
            src={commonImg.close}
            onClick={this.handleCloseGroupFullDialog.bind(this)}
          ></Image>
        </AnimatDialog>
      </ScrollView>
    );
  }
}

export default PayOrder;
