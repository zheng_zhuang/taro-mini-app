import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import './index.scss';

type NoOwnerProps = {
  errorTips: string;
};

interface NoOwner {
  props: NoOwnerProps;
}

class NoOwner extends React.Component {
  static defaultProps = {
    errorTips: '您不是该订单的下单客户，不可确认订单，有疑问请和相关服务顾问联系',
  };

  render() {
    const imgUrl = cdnResConfig.auth.withoutAuth;
    const { errorTips } = this.props;

    return (
      <View data-scoped='wk-ocn-owner' className='wk-ocn-owner no-owner'>
        <View className='tips-wraper'>
          <Image className='tips-img' src={imgUrl} mode='aspectFill'></Image>
          <View className='tips'>{errorTips}</View>
        </View>
      </View>
    );
  }
}

export default NoOwner;
