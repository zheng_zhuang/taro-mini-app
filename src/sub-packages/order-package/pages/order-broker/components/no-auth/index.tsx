import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import authHelper from '../../../../../../wxat-common/utils/auth-helper';
import cdnResConfig from '../../../../../../wxat-common/constants/cdnResConfig';
import './index.scss';

class NoAuth extends React.Component {
  onCheckAuth = () => {
    authHelper.checkAuth();
  };

  render() {
    const imgUrl = cdnResConfig.auth.withoutAuth;

    return (
      <View data-scoped='wk-ocn-NoAuth' className='wk-ocn-NoAuth wx-auth'>
        <View className='tips-wraper'>
          <Image className='tips-img' src={imgUrl} mode='aspectFit'></Image>
          <View className='tips'>您还未授权，请授权后查询订单信息</View>
          <Button className='auth-btn' onClick={this.onCheckAuth}>
            去授权
          </Button>
        </View>
      </View>
    );
  }
}

export default NoAuth;
