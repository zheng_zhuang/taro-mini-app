import { $getRouter } from 'wk-taro-platform';
import React from 'react'; /* eslint-disable react/sort-comp */
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Form, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../wxat-common/utils/money.wxs';
/**
 * 代客下单中转页
 */
import api from '../../../../wxat-common/api';
import wxApi from '../../../../wxat-common/utils/wxApi';
import constants from '../../../../wxat-common/constants';
import template from '../../../../wxat-common/utils/template';
import authHelper from '../../../../wxat-common/utils/auth-helper';
import shareUtil from '../../../../wxat-common/utils/share';
import pay from '../../../../wxat-common/utils/pay';
import moneyUtil from '../../../../wxat-common/utils/money';
import protectedMailBox from '../../../../wxat-common/utils/protectedMailBox';
import subscribeMsg from '../../../../wxat-common/utils/subscribe-msg';
import subscribeEnum from '../../../../wxat-common/constants/subscribeEnum';
import pageLinkEnum from '../../../../wxat-common/constants/pageLinkEnum';

import NoOwner from './components/no-owner';
import NoAuth from './components/no-auth';
import GoodsTplTmpl from '../../../../imports/GoodsTplTmpl';
import hoc from '@/hoc';
import { connect } from 'react-redux';
import { ITouchEvent } from '@tarojs/components/types/common';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum';

import { notifyStoreDecorateSwitch } from '@/wxat-common/x-login/decorate-configuration-store';

import './goods-tpl.scss';
import './index.scss';
import { WaitComponent } from '@/decorators/Wait';

const customGoodsType = goodsTypeEnum.CUSTOM_GOODS.value;

interface StateProps {
  sessionId: string;
  templ: Record<string, any>;
}

const PAY_CHANNEL = constants.order.payChannel;

const mapStateToProps = (state) => ({
  sessionId: state.base.sessionId,
  templ: template.getTemplateStyle(),
});

interface OrderBroker {
  props: StateProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class OrderBroker extends WaitComponent {
  $router = $getRouter();
  state = {
    authAble: false, // 用户是否已授权
    ableShow: false, // 显示授权组件
    isOrderOwner: true, // 当前订单用户
    errorTips: '您不是该订单的下单客户，不可确认订单，有疑问请和相关服务顾问联系',
    showShoppingGuideTips: true,
    payChannel: PAY_CHANNEL.WX_PAY,
    // 订单信息
    orderInfo: {},
    balance: null, // 余额
    valetOrderNo: null, // 代客订单(预订单)单号
    btnText: '确认支付',
    denyRepeatPay: false, // 拒绝重复支付
    infoTplName: 'goods-tpl',
    payBtnDisabled: false,
  } as Record<string, any>;

  onWait() {
    this.setNavigationBarColor();
    this.handlerLoadArgument(this.$router.params);
  }

  // 处理从二维码扫码进入
  async handlerLoadArgument(options: any = {}) {
    if (!this.checkAuth()) return;

    if (this.isQrCodeMapperLaunchEnter(options)) {
      const fld = this.formatLaunchData(await shareUtil.MapperQrCode.apiMapperContent(options.scene));
      console.log('handlerLoadArgument--->', fld);

      if (fld) {
        if (fld.storeId) await this.onChangeStore(fld.storeId);
        this.init(fld);
      }
    } else {
      this.init(options);
    }
  }

  init = (options) => {
    const { valetOrderNo } = options;
    this.setState(
      {
        valetOrderNo: valetOrderNo + '',
      },

      () => {
        this.getBalance();
        this.fetchPreOrderDetail();
      }
    );
  };

  // 从邮箱中拿启动参数
  formatLaunchData = (shareObj) => {
    return shareObj
      ? {
          valetOrderNo: shareObj.valetOrderNo ? shareObj.valetOrderNo + '' : '', // 订单编号
          storeId: shareObj.storeId || '', // 来源门店
        }
      : null;
  };

  // 判断是否二维码分享进入
  isQrCodeMapperLaunchEnter = (options) => {
    if (!options || !options.scene) return false;
    return !!shareUtil.MapperQrCode.getMapperKey(options.scene);
  };

  // 处理授权逻辑
  checkAuth = () => {
    const isAuth = authHelper.checkAuth(false);
    this.setState({
      authAble: isAuth,
      ableShow: !isAuth,
    });

    return isAuth;
  };

  // 用户接收代客订单(获取预订单详情)
  fetchPreOrderDetail = async () => {
    if (!this.state.valetOrderNo) return;
    try {
      const res = await wxApi.request({
        url: api.order.valetOrderConfirm,
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded',
        },

        quite: true,
        loading: true,
        data: { valetOrderNoStr: this.state.valetOrderNo + '' },
      });

      const data = res.data;
      if (data) {
        // 判断订单取消
        if (data.orderStatus === constants.order.orderStatus.cancel) {
          this.setState({
            isOrderOwner: false,
            errorTips: '该订单已失效，如需下单请联系专属服务顾问',
          });

          return;
        }

        this.setState({
          orderInfo: data,
          isOrderOwner: true,
          btnText: data.orderStatus && data.orderStatus !== constants.order.orderStatus.waitPay ? '已完成' : '确认支付',
          payBtnDisabled: !!(data.orderStatus && data.orderStatus !== constants.order.orderStatus.waitPay),
        });
      }
    } catch (err) {
      // error
      console.log(err);
      if (err.data.errorCode === 130005) {
        this.setState({ isOrderOwner: false });
      }
    }
  };

  // 关闭顶部提示
  onCloseTips = () => {
    this.setState({ showShoppingGuideTips: false });
  };

  setNavigationBarColor = () => {
    const { templ } = this.props;
    if (templ.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templ.titleColor, // 必写项
      });
    }
  };

  /**
   * 获取余额
   * @param {*} isSingleRequest
   */
  getBalanceRequest = (isSingleRequest: boolean) => {
    return wxApi.request({
      url: api.userInfo.balance,
      quite: !isSingleRequest,
      loading: isSingleRequest,
      data: {},
    });
  };

  // 设置余额
  getBalance = async () => {
    const res = await this.getBalanceRequest(true);
    this.setState({ balance: res.data.balance });
  };

  // 切换支付方式
  handlePayChannelChange = (ev: ITouchEvent) => {
    if (ev.currentTarget.dataset.payChannel === PAY_CHANNEL.ACCOUNT && this.state.balance == null) {
      this.getBalance();
    }
    this.setState({ payChannel: ev.currentTarget.dataset.payChannel });
  };

  // 校验商品上架、库存状态
  checkGoodsItemStatus = (itemList = []) => {
    let canPay = true;
    itemList.forEach((item: any) => {
      if ((item.itemCount > item.itemStock || !item.itemShelf) && item.itemType !== customGoodsType) {
        canPay = false;
      }
    });
    return canPay;
  };

  // 支付
  handlePay = () => {
    if (!this.state.orderInfo || !this.state.orderInfo.valetOrderNo) return;
    if (this.state.payBtnDisabled) return;

    // 校验代客订单商品上架、库存状态，如已生成订单号不需校验
    if (!this.state.orderInfo.orderNo && !this.checkGoodsItemStatus(this.state.orderInfo.valetItemList || [])) {
      wxApi.showToast({
        title: '订单中部分商品暂时缺货或已下架，请联系专属服务顾问',
        icon: 'none',
      });

      return;
    }
    const itemList = this.state.orderInfo.valetItemList || null;
    const valetOrderNo = this.state.orderInfo.valetOrderNo; // 预订单号
    const payChannel = this.state.payChannel;
    const payFee = this.state.orderInfo.payFee;

    // 判断订阅消息模板
    const ids: number[] = [];
    // 如果是微信支付，显示支付成功通知
    if (payChannel === PAY_CHANNEL.WX_PAY) {
      ids.push(subscribeEnum.PAY_SUCCESS.value);
    }
    // 如果是余额支付，显示余额支付通知
    if (payChannel === PAY_CHANNEL.ACCOUNT) {
      ids.push(subscribeEnum.BALANCE.value);
    }
    // 先授权订阅消息
    subscribeMsg.sendMessage(ids).then(() => {
      let url = '';
      const options = {
        success: (res) => {
          let urlPayType = '微信支付';
          if (payChannel !== PAY_CHANNEL.WX_PAY) {
            urlPayType = '余额支付';
          }

          const urlPrice = moneyUtil.fen2YuanFixed(+payFee);
          const paySuccessInfo = {
            payType: urlPayType,
            itemType:
              this.state.orderInfo &&
              this.state.orderInfo.valetItemList &&
              this.state.orderInfo.valetItemList[0].itemType
                ? this.state.orderInfo.valetItemList[0].itemType
                : null,
            money: urlPrice,
            orderNo: res.data.orderNo,
            infoTplName: this.state.infoTplName,
          };

          protectedMailBox.send(pageLinkEnum.orderPkg.paySuccess, 'paySuccessInfo', paySuccessInfo);
          url = pageLinkEnum.orderPkg.paySuccess;

          if (payChannel !== PAY_CHANNEL.WX_PAY) {
            wxApi.$redirectTo({ url });
          }
        },
        fail: () => {
          this.setState({ denyRepeatPay: false });
        },
        cancel: () => {
          // 余额支付点击取消时
          this.setState({ denyRepeatPay: false });
        },
        wxPaySuccess: () => {
          // 支付成功跳转到支付成功页面
          if (url) {
            wxApi.$redirectTo({
              url,
            });
          }
        },
        wxPayFail: () => {
          // 支付失败跳转到订单列表
          url = `/wxat-common/pages/tabbar-order/index`;
          const index = 1;
          if (url) {
            wxApi.$redirectTo({
              url: url + '?index=' + index,
            });
          }
        },
        originParams: {
          itemDTOList: itemList,
        },

        // 埋点上报需要商品像信息
      };
      // 是否立即进行回调
      // options.extConfig.immediateCallback = true;
      if (!this.state.denyRepeatPay) {
        this.setState({ denyRepeatPay: true }, () => {
          // 已有订单号则按普通订单拉取支付
          if (
            this.state.orderInfo.orderNo &&
            this.state.orderInfo.orderStatus === constants.order.orderStatus.waitPay
          ) {
            pay.payByOrder(this.state.orderInfo.orderNo, payChannel, options);
          } else {
            pay.payByValetOrder(valetOrderNo, payChannel, api.order.payValetOrder, options);
          }
        });
      }
    });
  };

  // 切换当前门店
  onChangeStore = async (storeId: string | number) => {
    const res = await wxApi.request({
      url: api.store.choose_new + '?storeId=' + storeId,
      loading: true,
    });

    const store = res.data;
    if (store) {
      notifyStoreDecorateSwitch(store);
    }
  };

  render() {
    const {
      authAble,
      ableShow,
      isOrderOwner,
      errorTips,
      orderInfo,
      showShoppingGuideTips,
      payChannel,
      balance,
      btnText,
    } = this.state;

    const { templ } = this.props;

    return (
      <View data-scoped='wk-opo-OrderBroker' className='wk-opo-OrderBroker pay-order-container'>
        {!authAble && ableShow ? (
          <NoAuth />
        ) : authAble && !ableShow && !isOrderOwner ? (
          <NoOwner errorTips={errorTips}></NoOwner>
        ) : (
          authAble &&
          !ableShow &&
          isOrderOwner && (
            <Form reportSubmit>
              {!!(orderInfo && orderInfo.guideName && showShoppingGuideTips) && (
                <View className='shopping-guide-tips'>
                  <View className='tips-content'>
                    {'本订单由专属服务顾问' + orderInfo.guideName + '代您下单，请确认'}
                  </View>
                  <Image
                    className='close-icon'
                    src="https://bj.bcebos.com/htrip-mp/static/app/images/common/line-close.png"
                    onClick={this.onCloseTips}
                  ></Image>
                </View>
              )}

              {/*  配送方式  */}
              <View className='delivery-way-box'>
                <View className='delivery-way-label'>配送</View>
                <View className='delivery-micro-name'>自提</View>
              </View>
              {/*  门店信息  */}
              <View className='self-delivery-store-box'>
                <View className='store-box'>
                  <View className='delivery-store-info-item'>
                    <View
                      className='icon-wraper'
                      style={_safe_style_('background:' + (templ.bgColor || 'rgba(243, 95, 40, 0.8)'))}
                    >
                      <Image
                        className='ic-location-light'
                        src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-location-light.png"
                      ></Image>
                    </View>
                  </View>
                  <View className='delivery-store-info-item store-info'>
                    <View className='store-name'>{'取货门店：' + (orderInfo.storeName || '-')}</View>
                    <View className='store-phone'>{orderInfo.storeMobile || orderInfo.storeTel || '-'}</View>
                    <View className='store-address'>{orderInfo.storeAddress || '-'}</View>
                  </View>
                </View>
              </View>
              {/*  选择支付方式  */}
              <View className='box-pay-channel'>
                <View className='pay-title'>支付方式</View>
                <View className='pay-channel-group'>
                  <View
                    className={'pay-channel pay-wx ' + (payChannel === PAY_CHANNEL.WX_PAY ? 'pay-checked' : '')}
                    style={_safe_style_(
                      'background:' +
                        (payChannel === PAY_CHANNEL.WX_PAY ? templ.bgColor : '') +
                        ';color:' +
                        (payChannel === PAY_CHANNEL.WX_PAY ? templ.btnColor : '') +
                        ';border-color:' +
                        (payChannel === PAY_CHANNEL.WX_PAY ? templ.btnColor : '')
                    )}
                    onClick={_fixme_with_dataset_(this.handlePayChannelChange, { payChannel: PAY_CHANNEL.WX_PAY })}
                  >
                    微信支付
                  </View>
                  <View
                    className={'pay-channel pay-accout ' + (payChannel === PAY_CHANNEL.ACCOUNT ? 'pay-checked' : '')}
                    style={_safe_style_(
                      'background:' +
                        (payChannel === PAY_CHANNEL.ACCOUNT ? templ.bgColor : '') +
                        ';color:' +
                        (payChannel === PAY_CHANNEL.ACCOUNT ? templ.btnColor : '') +
                        ';border-color:' +
                        (payChannel === PAY_CHANNEL.ACCOUNT ? templ.btnColor : '')
                    )}
                    onClick={_fixme_with_dataset_(this.handlePayChannelChange, { payChannel: PAY_CHANNEL.ACCOUNT })}
                  >
                    <View className='pay-accout-text'>余额支付</View>
                    <View className='pay-accout-balance'>{'￥' + filters.moneyFilter(balance, true)}</View>
                  </View>
                </View>
              </View>
              {/*  商品信息  */}
              {!!(orderInfo && orderInfo.valetItemList && orderInfo.valetItemList.length) && (
                <GoodsTplTmpl data={{ goodsInfoList: orderInfo.valetItemList, filters }}></GoodsTplTmpl>
              )}

              {/*  订单信息  */}
              <View className='order-info'>
                <View className='order-info-title'>订单信息</View>
                <View className='order-info-detail'>
                  <View className='detail-item'>
                    <View className='detail-item-label'>原价</View>
                    <View className='detail-item-info'>{'¥ ' + filters.moneyFilter(orderInfo.totalFee, true)}</View>
                  </View>
                  <View className='detail-item'>
                    <View className='detail-item-label'>优惠</View>
                    <View className='detail-item-info'>{'-¥ ' + filters.moneyFilter(orderInfo.storeFee, true)}</View>
                  </View>
                  <View className='detail-item order-price'>
                    <View className='detail-item-label'>订单金额</View>
                    <View className='detail-item-info'>{'¥ ' + filters.moneyFilter(orderInfo.payFee, true)}</View>
                  </View>
                  <View className='detail-item'>
                    <View className='detail-item-label'>备注</View>
                    <View className='detail-item-info'>{orderInfo.guideComment || ''}</View>
                  </View>
                  <View className='detail-item'>
                    <View className='detail-item-label'>专属服务顾问</View>
                    <View className='detail-item-info'>{orderInfo.guideName || orderInfo.guideUserName || ''}</View>
                  </View>
                </View>
              </View>
              {/*  支付按钮  */}
              <View className='pay-box'>
                <View className='total-price'>
                  <Text>合计：</Text>
                  <Text className='price-logo'>¥</Text>
                  <Text className='price-num'>{filters.moneyFilter(orderInfo.payFee, true)}</Text>
                </View>
                <Button
                  className='to-pay-btn'
                  style={_safe_style_('background:' + templ.btnColor)}
                  onClick={this.handlePay}
                >
                  {btnText}
                </Button>
              </View>
            </Form>
          )
        )}
      </View>
    );
  }
}

export default OrderBroker;
