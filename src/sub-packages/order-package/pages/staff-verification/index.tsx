import React from 'react'; // @scoped
import { View, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';

// 可以被核销的状态
import './index.scss';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import hoc, { HocState } from '@/hoc';
import { connect } from 'react-redux';
import style from '@/sub-packages/marketing-package/pages/activity-module/verify/index.module.scss';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';

const STATUS = [20, 30, 50, 70, 40, -30, 36, 37, 38];
const STATE_TIPS = {
  0: '该优惠券为无效优惠券',
  2: '该优惠券已使用',
  3: '该优惠券已过期',
};

const mapStateToProps = (state) => {
  return {
    currentStore: state.base.currentStore,
  };
};

type IProps = { currentStore: Record<string, any> } & HocState;

interface StaffVerification {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class StaffVerification extends React.Component {
  componentDidUpdate() {
    const {
      $tmpStyle: { titleColor },
    } = this.props.$global;
    if (titleColor) {
      wxApi.setNavigationBarColor({ frontColor: '#ffffff', backgroundColor: titleColor });
    }
  }

  getOrderDetail = async (orderNo: string) => {
    const res = await wxApi.request({
      url: api.order.detail,
      loading: true,
      quite: true,
      data: { orderNo: orderNo },
    });

    protectedMailBox.send('sub-packages/order-package/pages/verification/index', orderNo, res);
    return res;
  };

  getCouponDetail = async (code: string) => {
    const res = await wxApi.request({
      url: api.coupon.detailByCode,
      loading: true,
      quite: true,
      data: { code },
    });

    protectedMailBox.send('sub-packages/order-package/pages/verification/index', code, res);
    return res;
  };

  getEquityCardDetail = async (userCardId: string) => {
    const res = await wxApi.request({
      url: api.quityCard.staffVerDetail,
      loading: true,
      quite: true,
      data: { userCardId },
    });

    protectedMailBox.send('sub-packages/order-package/pages/verification/index', userCardId, res);
    return res;
  };

  handleScan = () => {
    wxApi.scanCode({
      success: async (res) => {
        console.log(res, 'ressressssssssssss');
        const type = +res.result.slice(0, 1);

        if (type === 7) {
          // 电子票

          const pagePath = `/sub-packages/order-package/pages/verification/index?barcode=${res.result}`;

          wxApi.$navigateTo({ url: pagePath });

          return;
        }
        if (res.path) {
          wxApi.$navigateTo({ url: res.path });
          return;
        }
        const pagePath = `/sub-packages/order-package/pages/verification/index?barcode=${res.result}`;

        if (type === 4) {
          try {
            const orderNo = res.result.slice(1);
            const {
              data: { orderStatus, orderStatusDesc, storeId },
            } = await this.getOrderDetail(orderNo);

            if (storeId !== (this.props.currentStore || {}).id) {
              wxApi.showToast({
                title: '核销失败：非本门店订单暂无权核销',
                icon: 'none',
                mask: true,
              });

              return;
            } else if (STATUS.findIndex((i) => i === orderStatus) < 0) {
              const tips = orderStatus === 60 ? '该订单已被核销' : orderStatusDesc;
              wxApi.showToast({
                title: `核销失败: ${tips || '订单状态不正确'}`,
                icon: 'none',
                mask: true,
              });

              return;
            }
          } catch (e) {
            // ignore
            console.error(e);
          }
        } else if (type === 2) {
          try {
            const couponNo = res.result.slice(1);

            const {
              data: { status },
            } = await this.getCouponDetail(couponNo);

            const tips = STATE_TIPS[status];
            if (tips) {
              wxApi.showToast({
                title: `核销失败: ${tips || '该优惠券无法核销'}`,
                icon: 'none',
                mask: true,
              });

              return;
            }
          } catch (e) {
            // ignore
            console.error(e);
          }
        } else if (type === 3) {
          // 权益卡
          try {
            const no = res.result.slice(1);
            // 删除补0
            const userCardId = no.replace(/^0+/, '');

            const {
              data: { orderStatus, storeId },
            } = await this.getEquityCardDetail(userCardId);

            if (storeId !== (this.props.currentStore || {}).id) {
              wxApi.showToast({
                title: '核销失败：非本门店订单暂无权核销',
                icon: 'none',
                mask: true,
              });

              return;
            } else if (orderStatus !== 38) {
              wxApi.showToast({
                title: '核销失败: 该订单已被核销',
                icon: 'none',
                mask: true,
              });

              return;
            }
          } catch (e) {
            // ignore
            console.error(e);
          }
        }
        wxApi.$navigateTo({ url: pagePath });
      },
      fail: () => {
        wxApi.showToast({ title: '扫码失败', icon: 'none', mask: true });
      },
    });
  };

  render() {
    const { titleColor } = this.props.$global.$tmpStyle;

    return (
      <View data-scoped='wk-sops-StaffVerification' className='wk-sops-StaffVerification'>
        <View className='tip-list'>
          <View className='tip-item'>
            <Text className='tip-title'>Q: 如何核销提货码？</Text>
            <View className='tip-content'>
              <Text>
                引导顾客进入微商城-我的-订单。进入门店待提货订单详情，即可查看门店自提订单的提货码。扫描顾客的提货码即可核销。
              </Text>
            </View>
          </View>

          <View className='tip-item'>
            <Text className='tip-title'>Q: 如何核销优惠券？</Text>
            <View className='tip-content'>
              <Text>
                引导顾客进入微商城-我的-优惠券。点击立即使用进入优惠券详情，即可查看券码。扫描顾客的券码即可核销。
              </Text>
            </View>
          </View>
          <View className='tip-item'>
            <Text className='tip-title'>Q: 如何核销活动劵码？</Text>
            <View className='tip-content'>
              <Text>
                引导用户进入小程序，
                <Text className='bold'>
                  选择我的{'>'}我的活动{'>'}点击查看电子票进入电子票详情
                </Text>
                ，即可查看电子二维码。
              </Text>
              <View>1.点击扫描二维码，扫描用户电子二维码完成核销</View>
              <View>2.点击输入劵码，输入用户数字劵码，完成核销</View>
            </View>
          </View>

          <View className='tip-item'>
            <Text className='tip-title'>Q：如何核销权益卡？</Text>
            <View className='tip-content'>
              <Text>
                引导顾客进入微商城-我的-订单。进入权益卡订单详情，即可查看门店权益卡的核销码。扫描顾客的核销码即可核销。
              </Text>
            </View>
          </View>

          <View className='tip-item'>
            <Text className='tip-title'>Q：如何核销电子票？</Text>
            <View className='tip-content'>
              <Text>
                引导顾客进入微商城-我的-活动预约。进入电子票详情，即可查验电子票的核销码。扫描顾客的核销码即可核销。
              </Text>
            </View>
          </View>
        </View>
        <View className='flex'>
          <Button className='to-scan' style={_safe_style_({ backgroundColor: titleColor })} onClick={this.handleScan}>
            去扫码
          </Button>
        </View>
      </View>
    );
  }
}

export default StaffVerification;
