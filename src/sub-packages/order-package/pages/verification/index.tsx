import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import hoc, { HocState } from '@/hoc/index';
import filters from '@/wxat-common/utils/money.wxs.js';
import constant from '@/wxat-common/constants/index.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import utils from '@/wxat-common/utils/util.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import qrCodeEnum from '@/wxat-common/constants/qrCodeEnum.js';

import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import couponEnum from '@/wxat-common/constants/couponEnum.js';
import Error from '@/wxat-common/components/error/error';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';

import './index.scss';
import { WaitComponent } from '@/decorators/Wait';

const couponImg = cdnResConfig.coupon;
const TIME_CARD_BG = 'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tools/card-min-count.png';
const CHARGE_CARD_BG =
  'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tools/recharge-card.png';

type IProps = {
  currentStore: Record<string, any>;
  tabbars: any;
} & HocState;

interface VerificationPage {
  props: IProps;
}

const mapStateToProps = (state) => ({
  currentStore: state.base.currentStore,
  tabbars: state.globalData.tabbars,
});

const defaultOrderDetail = {
  name: '',
  itemList: [],
  itemType: null,
  frontMoneyItemOrder: null,
  payFee: null,
  restMoney: null,
  payChannel: null,
  storeName: null,
  userWxNickName: null,
  userMobile: null,
  orderTime: null,
  orderNo: null,
  validityType: 0,
  validityBeginTime: '',
  validityEndTime: '',
  storeId: 0,
  activityEndTime: '',
  buyerName: null,
  buyerPhone: null,
  createTime: null,
  imgUrl: '',
  activityTitle: '',
  ticketType: null,
  activityOrderItemDTOList: [],
};

const defaultCouponDetail = {
  name: '',
  couponCategory: 0,
  minimumFee: '',
  discountFee: '',
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class VerificationPage extends WaitComponent {
  $router = $getRouter();
  state = {
    verificationNo: 0,
    verificationType: 1,
    orderDetail: defaultOrderDetail,
    couponDetail: defaultCouponDetail,
    couponTypeMap: {},
    loaded: false,
    error: null,
  };

  onWait() {
    // 微信小程序貌似有个 bug
    // 微信扫一扫 => scene=No%3D3941386409998272355%26Type%3D2
    // 通过 wx.scanCode => scene=No=3941386409998272355&Type=2

    // 电子票 eticket 7

    let { scene } = this.$router.params;
    const { Type } = this.$router.params;
    const { barcode } = this.$router.params;

    if (scene) {
      scene = '?' + decodeURIComponent(scene + '');
      const verificationNo = utils.getQueryString(scene, 'No').replace(/^0+/, '');
      const verificationType = parseInt(utils.getQueryString(scene, 'Type') || Type);

      this.setState({ verificationNo, verificationType }, this.init.bind(this));
    } else if (barcode) {
      const verificationNo = barcode.slice(1);
      const verificationType = parseInt(barcode.slice(0, 1));

      this.setState({ verificationNo, verificationType }, this.init.bind(this));
    }
  }

  onVerificationResult = (status) => {
    switch (status.code) {
      // 成功
      case constant.verification.status.SUCCESS:
        wxApi.showToast({ title: '核销成功', icon: 'success' });

        setTimeout(() => {
          wxApi.$navigateTo({ url: `/wxat-common/pages/home/index` });
        }, 1500);
        break;

      // 没有权限
      case constant.verification.status.NOT_PERMIT:
        wxApi.showModal({
          title: '核销失败',
          content: '您没有核销权限哦',
          showCancel: false,
          confirmText: '确定',
        });

        break;

      // 不适用门店
      case constant.verification.status.NOT_APPLICABLE_STORE:
        wxApi.showModal({
          title: '核销失败',
          content: '不适用当前门店,请更换门店重试',
          showCancel: false,
          confirmText: '确定',
        });

        break;

      // 未支付的预约
      case constant.verification.status.NOT_PAY:
        wxApi.showModal({
          title: '核销失败',
          content: '该预约还未支付，请先支付',
          showCancel: false,
          confirmText: '确定',
        });

        break;

      // 次数卡不足
      case constant.verification.status.INADEQUATE_CARD:
        wxApi.showModal({
          title: '核销失败',
          content: '次数卡不足',
          showCancel: false,
          confirmText: '确定',
        });

        break;

      default:
        wxApi.showModal({
          title: '核销失败',
          content: status.msg,
          showCancel: false,
          confirmText: '确定',
        });

        break;
    }
  };

  init = async () => {
    if (this.state.verificationType === qrCodeEnum.order) {
      await this.getOrderDetail();
    } else if (this.state.verificationType === qrCodeEnum.coupon) {
      await this.getCouponDetail();

      const couponTypeMap = Object.keys(couponEnum.TYPE).reduce((map, key) => {
        const { label, value } = couponEnum.TYPE[key];
        map[value] = label;
        return map;
      }, {});
      this.setState({ couponTypeMap });
    } else if (this.state.verificationType === qrCodeEnum.card) {
      await this.getEquityCardDetail();
    } else if (this.state.verificationType === qrCodeEnum.eticket) {
      // 电子票
      await this.queryVerifyDetail();
    } else {
      this.setState({ loaded: true });
    }
  };

  // 获取电子票详情
  queryVerifyDetail = async () => {
    const { barcode } = this.$router.params;

    try {
      const params = { verificationCode: barcode };
      const res = await wxApi.request({
        url: api.activeModule.queryVerify,
        method: 'POST',
        data: params,
        loading: true,
      });

      this.setState({ orderDetail: res.data || defaultOrderDetail });
    } catch ({ error: err }) {
      this.setState({ error: err.data });
      console.error('error', err);
    } finally {
      this.setState({ loaded: true });
    }
  };

  handleConfirm = () => {
    if (authHelper.checkAuth(true, false)) {
      this.doVerification();
    }
  };

  handleCancel = () => {
    const pages = Taro.getCurrentPages();
    // 太阳码核销
    if ((pages || []).length <= 1) {
      this.navHome();
    } else {
      wxApi.$navigateBack();
    }
  };

  /**
   * 获取权益卡详情
   */
  getEquityCardDetail = async () => {
    try {
      let res = protectedMailBox.read(this.state.verificationNo);
      if (!res) {
        res = await wxApi.request({
          url: api.quityCard.staffVerDetail,
          loading: true,
          quite: true,
          data: { userCardId: this.state.verificationNo },
        });
      }
      this.setState({ orderDetail: res.data });
    } finally {
      this.setState({ loaded: true });
    }
  };

  /**
   * 获取订单详情
   * @param {*} showLoading
   */
  getOrderDetail = async () => {
    try {
      let res = protectedMailBox.read(this.state.verificationNo);
      this.setState({ error: null });
      if (!res) {
        res = await wxApi.request({
          url: api.order.detail,
          loading: true,
          data: { orderNo: this.state.verificationNo },
        });
      }
      this.setState({ orderDetail: res.data || defaultOrderDetail });
    } catch ({ error: err }) {
      this.setState({ error: err.data });
      console.error('error', err);
    } finally {
      this.setState({ loaded: true });
    }
  };

  /**
   * 获取优惠券详情
   * @param {*} showLoading
   */
  getCouponDetail = async () => {
    try {
      let res = protectedMailBox.read(this.state.verificationNo);

      this.setState({ error: null });
      if (!res) {
        res = await wxApi.request({
          url: api.coupon.detailByCode,
          loading: true,
          data: { code: this.state.verificationNo },
        });
      }
      this.setState({ couponDetail: res.data || defaultCouponDetail });
    } catch (err) {
      this.setState({ error: err.data });
      console.error('error', err);
    } finally {
      this.setState({ loaded: true });
    }
  };

  // 时间校验
  timeCheck = (startTime: number, endTime: number) => {
    if (!startTime) {
      return endTime < Date.now();
    }
    return startTime > Date.now() || endTime < Date.now();
  };

  // 对话框
  showModalChange = async (isEticket: boolean) => {
    let confirm = false;
    const content = isEticket ? '该活动已结束，是否继续核销 ？' : '该笔订单已超过规定的核销时间，确定仍然进行核销吗？';
    confirm = await new Promise((resolve) => {
      wxApi.showModal({
        content: content,
        showCancel: true,
        cancelText: '取消',
        confirmText: '确定',
        success: (res) => {
          if (res.confirm) {
            console.log('用户点击确定');
            resolve(true);
          } else if (res.cancel) {
            console.log('用户点击取消');
            resolve(false);
          }
        },
        fail: () => resolve(false),
      });
    });

    return confirm;
  };

  doVerification = async () => {
    const { verificationNo, verificationType } = this.state;

    // 电子票
    const isEticket = verificationType === qrCodeEnum.eticket;

    const params = { verificationNo, verificationType };

    if (this.state.orderDetail && [qrCodeEnum.card, qrCodeEnum.eticket].includes(verificationType)) {
      let start = new Date(this.state.orderDetail.validityBeginTime).getTime();
      let end = new Date(this.state.orderDetail.validityEndTime).getTime();

      if (Number(this.state.orderDetail.storeId) !== this.props.currentStore.id) {
        wxApi.showToast({
          title: '核销失败：非本门店订单暂无权核销',
          icon: 'none',
          mask: true,
        });

        return;
      }

      // 电子票取值
      if (isEticket) {
        start = 0;
        end = new Date(this.state.orderDetail.activityEndTime).getTime();
      }

      let confirm = true;

      // 电子票类型 qrCodeEnum.eticket
      if ((this.state.orderDetail.validityType === 2 || isEticket) && this.timeCheck(start, end)) {
        confirm = await this.showModalChange(isEticket);
      }

      if (!confirm) return;
    }

    try {
      // 电子票核销
      if (isEticket) {
        const { barcode } = this.$router.params;
        await wxApi.request({
          url: api.activeModule.verify,
          method: 'POST',
          data: {
            verificationCode: barcode,
          },

          loading: true,
        });
      } else {
        await wxApi.request({
          url: api.verification.verificate,
          loading: true,
          checkSession: true,
          quite: true,
          data: params,
        });
      }

      // 核销成功, 跳转到小程序主页
      this.onVerificationResult({
        code: constant.verification.status.SUCCESS,
        msg: '核销成功',
      });

      // 核销失败，更加失败状态码处理，跳转到主页，弹框提示核销员
    } catch (err) {
      this.onVerificationResult({
        code: err.data.errorCode,
        msg: err.data.errorMessage || '系统错误，请重新核销',
      });
    }
  };

  handleChooseStore = () => {
    wxApi.$navigateTo({ url: '/wxat-common/pages/store-list/index' });
  };

  navHome = () => {
    let url = '/wxat-common/pages/home/index';
    const { tabbars } = this.props;
    if (tabbars && tabbars.list && tabbars.list.length) {
      const list = tabbars.list;
      url = list[0].pagePath;
    }
    wxApi.$navigateTo({ url });
  };

  render() {
    const { couponDetail, couponTypeMap, loaded, verificationType, orderDetail, error } = this.state;

    const { currentStore } = this.props;

    let $content: JSX.Element | null = null;

    if (!loaded) {
      return null;
    }

    if (verificationType === qrCodeEnum.coupon) {
      $content = (
        <View className='coupon-verification'>
          <View className='header-tips'>请开启手机GPS并允许微信位置授权，定位核销门店</View>
          <Image className='coupon-icon' src={couponImg.icon}></Image>
          {!!couponDetail && (
            <Block>
              <View className='infos-group'>
                <View className='info-label'>名称</View>
                <View className='info-value'>{couponDetail.name}</View>
              </View>
              <View className='infos-group'>
                <View className='info-label'>类型</View>
                <View className='info-value'>{couponTypeMap[couponDetail.couponCategory]}</View>
              </View>
              <View className='infos-group'>
                <View className='info-label'>优惠</View>
                <View className='info-value'>
                  {couponDetail.couponCategory === couponEnum.TYPE.freight.value ||
                  couponDetail.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                    <Block>
                      {'满' +
                        filters.moneyFilter(couponDetail.minimumFee, true) +
                        '-' +
                        filters.moneyFilter(couponDetail.discountFee, true)}
                    </Block>
                  ) : (
                    couponDetail.couponCategory === couponEnum.TYPE.discount.value && (
                      <Block>
                        {filters.discountFilter(couponDetail.discountFee, true) +
                          '折（满' +
                          filters.moneyFilter(couponDetail.minimumFee, true) +
                          '元可用）'}
                      </Block>
                    )
                  )}
                </View>
              </View>
            </Block>
          )}

          <View className='infos-group'>
            <View className='info-label'>核销门店</View>
            <View className='info-value current-store' onClick={this.handleChooseStore}>
              {currentStore.abbreviation || currentStore.name || '请选择'}
              {!!currentStore && <Image className='right-icon' src={getStaticImgUrl.images.rightAngleGray_png}></Image>}
            </View>
          </View>

          <View className='ver-box ver-box-v2'>
            <View className='cancel-btn' onClick={this.handleCancel}>
              取消核销
            </View>
            <View className='ok-btn' onClick={this.handleConfirm}>
              确认核销
            </View>
          </View>
        </View>
      );
    } else if ([qrCodeEnum.order, qrCodeEnum.card, qrCodeEnum.eticket].includes(verificationType)) {
      $content = orderDetail ? (
        <View className='order-container'>
          {verificationType === qrCodeEnum.eticket ? (
            <View className='item-box-etick'>
              <View className='item-box-left'>
                <Image className='box-left-img' src={orderDetail.imgUrl}></Image>
              </View>
              <View className='item-box-right'>
                <View className='box-right-title'>{orderDetail.activityTitle}</View>
                {/* 1: 免费; 2: 付费; 3: 保证金 */}
                <View className='box-right-ticketType'>
                  {orderDetail.ticketType === 1 ? '免费票' : orderDetail.ticketType === 2 ? '付费票' : '保证金'}
                </View>
                <View className='box-right-price'>
                  {orderDetail.activityOrderItemDTOList[0].price / 100}
                  <Text className='box-right-count'>x1</Text>
                </View>
              </View>
            </View>
          ) : (
            <View className='item-box'>
              {(orderDetail.itemList || []).map((item: any, index) => {
                return (
                  <View className='item-tab' key={index}>
                    <View className='item-icon-box'>
                      {item.thumbnail ? (
                        <Image className='item-icon' src={item.thumbnail}></Image>
                      ) : (
                        <Image
                          className='item-icon'
                          src={
                            orderDetail.itemType === 4
                              ? CHARGE_CARD_BG
                              : item.itemType === constant.card.type
                              ? TIME_CARD_BG
                              : ''
                          }
                        ></Image>
                      )}
                    </View>
                    {/*  商品详情  */}
                    <View className='item-info'>
                      <View>
                        <View className='item-name limit-line'>{item.itemName || ''}</View>
                        {/*  商品规格  */}
                        {!!item.itemAttribute && <View className='item-attribute'>{item.itemAttribute || ''}</View>}
                      </View>
                      <View>
                        <View className='item-count-price'>
                          <View className='item-price'>{'￥' + filters.moneyFilter(item.salePrice, true)}</View>
                          <View className='item-count'>{'x ' + item.itemCount}</View>
                        </View>
                        {!!orderDetail.frontMoneyItemOrder && (
                          <View className='front-money'>
                            {'￥' + filters.moneyFilter(orderDetail.payFee, true) + '（定金）'}
                          </View>
                        )}
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          )}

          <View className='divider'></View>
          <View className='info-item'>
            <View className='item-label'>{orderDetail.frontMoneyItemOrder ? '已支付定金' : '实收'}</View>
            {orderDetail.payFee ? (
              <View className='item-value'>{'￥' + filters.moneyFilter(orderDetail.payFee, true)}</View>
            ) : (
              <View className='item-value'>{'￥' + orderDetail.activityOrderItemDTOList[0].price / 100}</View>
            )}
          </View>
          {!!orderDetail.frontMoneyItemOrder && (
            <View className='info-item'>
              <View className='item-label'>待支付尾款</View>
              <View className='item-value'>{'￥' + filters.moneyFilter(orderDetail.restMoney, true)}</View>
            </View>
          )}

          <View className='info-item'>
            <View className='item-label'>支付方式</View>
            <View className='item-value'>{orderDetail.payChannel || '微信支付'}</View>
          </View>
          <View className='info-item'>
            <View className='item-label'>下单门店</View>
            <View className='item-value'>{orderDetail.storeName}</View>
          </View>
          <View className='divider'></View>
          <View className='info-item'>
            <View className='item-label'>会员姓名</View>
            <View className='item-value'>{orderDetail.userWxNickName || orderDetail.buyerName || ''}</View>
          </View>
          <View className='info-item'>
            <View className='item-label'>会员手机号</View>
            <View className='item-value'>{orderDetail.userMobile || orderDetail.buyerPhone || ''}</View>
          </View>
          <View className='info-item'>
            <View className='item-label'>下单时间</View>
            <View className='item-value'>{orderDetail.orderTime || orderDetail.createTime}</View>
          </View>
          <View className='info-item'>
            <View className='item-label'>订单号</View>
            <View className='item-value'>{orderDetail.orderNo}</View>
          </View>
          <View className='ver-box'>
            <View className='cancel-btn' onClick={this.handleCancel}>
              取消核销
            </View>
            <View className='ok-btn' onClick={this.handleConfirm}>
              确认核销
            </View>
          </View>
        </View>
      ) : null;
    } else if (error) {
      const { errorMessage } = error || { errorMessage: '意料之外的错误' };
      $content = <Error message={errorMessage}></Error>;
    } else {
      $content = <Error message='无效的核销码'></Error>;
    }

    return (
      <View data-scoped='wk-opv-Verification' className='wk-opv-Verification'>
        {$content}
      </View>
    );
  }
}

export default VerificationPage;
