import { $getRouter } from 'wk-taro-platform'; // @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ComponentClass } from 'react';
import { connect } from 'react-redux';

import hoc from '@/hoc/index';
import filters from '../../../../wxat-common/utils/money.wxs.js';
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import couponEnum from '../../../../wxat-common/constants/couponEnum.js';
import Empty from '../../../../wxat-common/components/empty/empty';
import './index.scss';
import template from '@/wxat-common/utils/template.js';
import Error from '../../../../wxat-common/components/error/error';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const couponImg = cdnResConfig.coupon;

type PageStateProps = {
  currentStore: Record<string, any>;
  templ: any;
};

type PageDispatchProps = {};

type PageOwnProps = {};

type PageState = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface ToVerificationPage {
  props: IProps;
}

const mapStateToProps = (state) => ({
  currentStore: state.base.currentStore,
  templ: template.getTemplateStyle(),
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class ToVerificationPage extends React.Component {
  $router = $getRouter();
  state = {
    title: [
      { id: 0, name: '订单' },
      { id: 1, name: '卡券' },
    ],

    couponEnum,
    pitchId: 0,
    pageNo: 1,
    couponPageNo: 1,
    pageSize: 10,
    orderList: [],
    couponList: [],
    dataList: [],
    hasMore: true,
    cookie: '',
    verificationUserId: '', //核销员ID
    orderStatusList: [20, 30, 36, 37, 38, 40, 50, 70, -30], //固定接口参数
    storeId: null,
    error: false,
  };

  componentDidMount() {
    const { scene } = this.$router.params;
    if (scene) {
      const _scene = decodeURIComponent(scene as string);

      const result = _scene.split('-');
      const userId = result[0];
      const storeId = result[1];

      if (!userId || !storeId) {
        this.setState({ error: true });
        return;
      }

      this.setState({
        error: false,
        verificationUserId: parseInt(userId),
        storeId: parseInt(storeId),
      });
    }
  }

  componentDidShow() {
    if (this.state.error) {
      this.getOrderList();
      this.getCouponsList();
    }
  }

  // 跳转待核销详情
  handleChoose = async (item, code?: any) => {
    const params = {
      verificationNo: this.state.pitchId === 0 ? '4' + item : '2' + item,
      verificationUserId: this.state.verificationUserId,
    };

    await wxApi.request({
      url: api.order.verification_choose,
      method: 'GET',
      data: params,
    });

    wxApi.showModal({
      content: '店员正在核销，若需取消，请与店员沟通',
      showCancel: false,
      confirmText: '知道了',
      confirmColor: this.props.templ.btnColor,
      success(res) {
        console.log(res);
      },
    });
  };

  //选择核销订单返回后端状态
  handleStatus(params) {
    return wxApi.request({
      url: api.order.verification_choose,
      method: 'GET',
      data: {
        ...params,
      },
    });
  }

  handleSwitch = (id) => {
    this.setState(
      {
        pitchId: id,
        pageNo: 1,
        couponPageNo: 1,
        orderList: [],
        couponList: [],
      },

      () => {
        if (id === 0) {
          this.getOrderList();
        } else {
          this.getCouponsList();
        }
      }
    );
  };

  getOrderList(isStart = false) {
    if (isStart) {
      this.state.hasMore = true;
      this.state.pageNo = 1;
    }
    let orderStatusList = '';
    this.state.orderStatusList.forEach((item) => {
      orderStatusList += '&orderStatusList=' + String(item);
    });
    const params = {
      pageNo: this.state.pageNo,
      pageSize: this.state.pageSize,
      tag1Or: 131104,
      isComplete: false,
      orderStatusList: this.state.orderStatusList,
      storeId: this.state.storeId,
    };

    wxApi
      .request({
        url: api.order.verification_order,
        method: 'POST',
        loading: true,
        data: params,
      })
      .then((res) => {
        const totalCount = res.totalCount;
        const data = res.data || [];
        let dataList = [];

        if (this.isLoadMoreRequest()) {
          dataList = this.state.orderList.concat(data);
        } else {
          dataList = data || [];
        }
        const hasMore = dataList.length < totalCount;
        this.setState({
          orderList: dataList,
          hasMore,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  //获取卡券列表
  getCouponsList(isStart = false) {
    if (isStart) {
      this.state.couponPageNo = 1;
      this.state.hasMore = true;
    }
    wxApi
      .request({
        method: 'POST',
        url: api.order.verification_coupon,
        data: {
          pageNo: this.state.couponPageNo,
          pageSize: this.state.pageSize,
          statusList: [1, 4],
          storeId: this.state.storeId,
        },
      })
      .then((res) => {
        const totalCount = res.totalCount;
        const data = res.data || [];
        let dataList;
        if (this.isLoadMoreRequest()) {
          dataList = this.state.couponList.concat(data);
        } else {
          dataList = data || [];
        }
        if (typeof dataList == 'string') {
          dataList = [];
        }
        dataList.forEach((item) => {
          item.validityDate = parseInt((item.endTime - item.beginTime) / 86400000);
          if (item.useScopeType === 1) {
            item.useScopeTypeDesc = '线上商城';
          } else if (item.useScopeType === 2) {
            item.useScopeTypeDesc = '线下门店';
          } else {
            item.useScopeTypeDesc = '通用券';
          }
        });
        const hasMore = dataList.length < totalCount;
        this.setState({
          couponList: dataList,
          hasMore,
        });
      });
  }

  //上拉触底加载更多
  onReachBottom() {
    if (!this.state.hasMore) return;
    if (this.state.pitchId === 0) {
      this.setState({
        pageNo: ++this.state.pageNo,
      });

      this.getOrderList();
    } else {
      this.setState({
        couponPageNo: ++this.state.couponPageNo,
      });

      this.getCouponsList();
    }
  }

  isLoadMoreRequest() {
    return this.state.pageNo > 1 || this.state.couponPageNo > 1;
  }

  render() {
    const { pitchId, title, orderList, couponList, couponEnum, error } = this.state;
    if (error) {
      return (
        <View
          data-fixme='03 add view wrapper. need more test'
          data-scoped='wk-opt-ToVerification'
          className='wk-opt-ToVerification'
        >
          <Error message='参数出错' />
        </View>
      );
    }

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-opt-ToVerification'
        className='wk-opt-ToVerification'
      >
        <View className='verification-top'>
          {title.map((item, index) => {
            return (
              <View
                className={pitchId == item.id ? 'title' : 'default'}
                key={item.id}
                onClick={() => this.handleSwitch(item.id)}
              >
                {item.name}
              </View>
            );
          })}
        </View>
        <View className='verification-top'>
          {title.map((item, index) => {
            return (
              <Text
                className={pitchId == item.id ? 'solid' : 'transparency'}
                key={item.id}
                onClick={() => this.handleSwitch(item.id)}
              ></Text>
            );
          })}
        </View>
        {/*  订单列表  */}
        {pitchId == 0 ? (
          <Block>
            {!!orderList && orderList.length == 0 ? (
              <Empty message='暂无订单'></Empty>
            ) : (
              <View className={((orderList && orderList.length ? false : true) ? 'global-hidden ' : '') + 'order-list'}>
                {orderList.map((order: any, idx) => {
                  return (
                    <View className='tab' key={idx}>
                      <View className='order-info'>
                        <View className='orderNo'>{'订单号: ' + order.orderNo}</View>
                        {!!order.orderTime && <View className='orderTime'>{order.orderTime}</View>}
                      </View>
                      {/*  商品信息  */}
                      <View className='item-box'>
                        {order.itemList.map((item, index) => {
                          return (
                            <View
                              className='item-tab'
                              // onClick={this.handleGoToDetail}
                              key={index}
                              data-orde={order}
                              data-index={idx}
                            >
                              {index === 0 && (
                                <View
                                  className='orderStatus-box'
                                  // style={'color:' + tmpStyle.btnColor}
                                >
                                  <View className='order-status'>{order.orderStatusDesc}</View>
                                </View>
                              )}

                              {/*  商品详情  */}
                              <View className='item-info-box'>
                                <View className='img-box'>
                                  {!!item.thumbnail && <Image src={item.thumbnail} className='item-img'></Image>}
                                </View>
                                <View className='item-info'>
                                  <View>
                                    <View className='item-name limit-line line-2'>
                                      {item.itemName ? item.itemName : ''}
                                    </View>
                                    {!!item.itemAttribute && (
                                      <View className='item-attribute'>{item.itemAttribute || ''}</View>
                                    )}
                                  </View>
                                </View>
                                <View className='item-count-price'>
                                  <Text className='item-price'>{'￥' + filters.moneyFilter(item.salePrice, true)}</Text>
                                  <Text className='item-count'>{'x ' + item.itemCount}</Text>
                                </View>
                              </View>
                              {/*  合计信息  */}
                              <View className='total-price'>
                                {'共' +
                                  item.itemCount +
                                  '件商品 合计：￥' +
                                  filters.moneyFilter(item.payFee, true) +
                                  '(含运费)'}
                              </View>
                            </View>
                          );
                        })}
                        <View className='footer-button'>
                          <View className='choose-btn' onClick={() => this.handleChoose(order.orderNo)}>
                            选择
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                })}
              </View>
            )}
          </Block>
        ) : (
          pitchId == 1 && (
            <Block>
              {!!couponList && couponList.length == 0 ? (
                <Empty message='暂无卡券'></Empty>
              ) : (
                <View
                  className={
                    ((couponList && couponList.length ? false : true) ? 'global-hidden ' : '') + 'coupons-list'
                  }
                >
                  {couponList.map((item: any, index) => {
                    return (
                      <View className='coupon-detail' key={item.id}>
                        <Image className='coupon-img' src={couponImg.toVerification}></Image>
                        <View className='coupon-content'>
                          <View className='row-item'>
                            <View className='price-box'>
                              {item.couponCategory == couponEnum.TYPE.freight.value ||
                              item.couponCategory == couponEnum.TYPE.fullReduced.value ? (
                                <Block>
                                  <Text className='money-mark'>￥</Text>
                                  <Text className='coupon-bigprice' style={_safe_style_(item.fontStyle)}>
                                    {filters.moneyFilter(item.discountFee, true)}
                                  </Text>
                                </Block>
                              ) : (
                                item.couponCategory == couponEnum.TYPE.discount.value && (
                                  <Block>
                                    <Text className='coupon-bigprice' style={_safe_style_(item.fontStyle)}>
                                      {filters.discountFilter(item.discountFee, true)}
                                    </Text>
                                    <Text className='coupon-bigprice' style={_safe_style_('font-size: 36rpx')}>
                                      折
                                    </Text>
                                  </Block>
                                )
                              )}

                              {/*  折扣  */}
                            </View>
                            {item.minimumFee === 0 ? (
                              <View className='coupon-full'>无门槛</View>
                            ) : (
                              <View className='coupon-full'>
                                {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                              </View>
                            )}

                            <View className='coupon-type-box'>
                              {item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                                <View className='coupon-type'>{couponEnum.TYPE.fullReduced.label}</View>
                              ) : item.couponCategory === couponEnum.TYPE.freight.value ? (
                                <View className='coupon-type'>{couponEnum.TYPE.freight.label}</View>
                              ) : (
                                item.couponCategory === couponEnum.TYPE.discount.value && (
                                  <View className='coupon-type'>{couponEnum.TYPE.discount.label}</View>
                                )
                              )}
                            </View>
                          </View>
                          {/*  卡券信息  */}
                          <View className='coupons-info'>
                            <View className='coupons-name'>{item.name}</View>
                            <View className='coupon-use-type'>{item.useScopeTypeDesc}</View>
                            <View className='coupon-data'>{'有效期:' + item.validityDate + '天'}</View>
                          </View>
                          <View className='coupon-btn'>
                            <View className='choose-btn' onClick={() => this.handleChoose(item.code, item.id)}>
                              选择
                            </View>
                          </View>
                        </View>
                        {/* {!item.canUse && (
                    <View className='coupon-tip'>
                    <View className='coupon-tip-content'>本优惠券不适用于该门店</View>
                    </View>
                    )} */}
                      </View>
                    );
                  })}
                </View>
              )}
            </Block>
          )
        )}

        {/*  卡券列表  */}
      </View>
    );
  }
}

export default ToVerificationPage as ComponentClass<PageOwnProps, PageState>;
