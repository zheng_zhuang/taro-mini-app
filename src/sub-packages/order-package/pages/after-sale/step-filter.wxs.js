// 进度显示状态过滤器
export default {
  // 已发起申请
  requestRefund: function (status, refundType) {
    // refundType 申请的售后类型，1：退货退款，2：仅退款
    if (
      status === -1 || // 已发起申请
      status === -2 || // 已同意售后申请
      status === -3 || // 退款商品已发货
      status === -4 || // 已确认退货
      status === -5 || // 已确认退款
      status === -6 || // 已拒绝退款
      status === -10 || // 退款中
      status === -30 || // 退款失败
      status === -20
    ) {
      // 退款完成
      return true;
    }
    return false;
  },
  // 卖家已同意售后申请
  agreeRefundRequest: function (status, refundType) {
    // refundType 申请的售后类型，1：退货退款，2：仅退款
    if (
      status === -2 || // 已同意售后申请
      status === -3 || // 退款商品已发货
      status === -4 || // 已确认退货
      status === -5 || // 已确认退款
      status === -6 || // 已拒绝退款
      status === -10 || // 退款中
      status === -30 || // 退款失败
      status === -20
    ) {
      // 退款完成
      return true;
    }
    return false;
  },
  // 已快递退货
  refundItemShipping: function (status, refundType) {
    // refundType 申请的售后类型，1：退货退款，2：仅退款
    if (
      (refundType === 1 && status === -3) || // 退款商品已发货
      status === -4 || // 已确认退货
      status === -5 || // 已确认退款
      status === -6 || // 已拒绝退款
      status === -10 || // 退款中
      status === -30 || // 退款失败
      status === -20
    ) {
      // 退款完成
      return true;
    }
    return false;
  },
  // 退款已完成
  refundSuccess: function (status, refundType) {
    // refundType 申请的售后类型，1：退货退款，2：仅退款
    if (status === -20) {
      // 退款完成
      return true;
    }
    return false;
  },
  // 申请的售后类型
  refundType: function (status, refundType) {
    // refundType 申请的售后类型，1：退货退款，2：仅退款
    if (refundType === 1) {
      return true;
    }
    return false;
  },
};
