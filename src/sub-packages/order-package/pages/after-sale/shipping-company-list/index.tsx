import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ComponentClass } from 'react';

import hoc from '@/hoc/index';
import template from '../../../../../wxat-common/utils/template.js';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import pageLinkEnum from '../../../../../wxat-common/constants/pageLinkEnum.js';

import './index.scss';

type PageStateProps = {};

type PageDispatchProps = {};

type PageOwnProps = {};

type PageState = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface ShippingCompanyListPage {
  props: IProps;
}

@hoc
class ShippingCompanyListPage extends React.Component {
  state = {
    tmpStyle: {}, // 主题模板配置

    shippingCompanyList: [], //物流公司列表
  };

  componentDidMount() {
    this.getTemplateStyle(); // 获取主题模板配置

    this.getShippingCompanyList(); // 查询物流公司列表
  }

  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }

    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 查询物流公司列表
  getShippingCompanyList() {
    wxApi
      .request({
        url: api.order.shippingCompanyList,
        loading: true,
        data: {},
      })
      .then((res) => {
        this.setState({
          shippingCompanyList: res.data || [],
        });
      })
      .catch((error) => {
        if (error.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '物流公司列表查询失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.errorMessage,
          });
        }
      });
  }

  // 选择物流公司
  choiceShippingCompany = (e) => {
    // 当前选中的物流公司
    let refundShippingCompany = null;
    if (e.currentTarget.dataset.name) {
      refundShippingCompany = e.currentTarget.dataset.name;
    }

    const refundOrderInfo = {
      refundShippingCompany: refundShippingCompany,
    };

    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.refundOrder, 'refundOrderInfo', refundOrderInfo);
    // 返回上一页面
    wxApi.navigateBack({
      delta: 1,
    });
  };

  render() {
    const { shippingCompanyList } = this.state;
    return (
      <View data-scoped='wk-pas-ShippingCompanyList' className='wk-pas-ShippingCompanyList shipping-company-list'>
        {shippingCompanyList.map((item, index) => {
          return (
            <View
              className='shipping-company'
              key={index}
              onClick={_fixme_with_dataset_(this.choiceShippingCompany, { name: item })}
            >
              {item}
            </View>
          );
        })}
      </View>
    );
  }
}

export default ShippingCompanyListPage as ComponentClass<PageOwnProps, PageState>;
