// @externalClassesConvered(BottomDialog)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import React, { ComponentClass } from 'react';
import { Block, View, Image, Text, ScrollView, Input, Picker, Radio } from '@tarojs/components';
import Taro, { Config } from '@tarojs/taro';
import { connect } from 'react-redux';

import hoc from '@/hoc/index';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl'
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import template from '../../../../../wxat-common/utils/template.js';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from "../../../../../wxat-common/api";
import goodsTypeEnum from '../../../../../wxat-common/constants/goodsTypeEnum.js';
import utilDate from '../../../../../wxat-common/utils/date.js';
import subscribeMsg from '../../../../../wxat-common/utils/subscribe-msg.js';
import subscribeEnum from '../../../../../wxat-common/constants/subscribeEnum.js';
import pageLinkEnum from '../../../../../wxat-common/constants/pageLinkEnum.js';

import BottomDialog from '../../../../../wxat-common/components/bottom-dialog';
import AnimatDialog from "../../../../../wxat-common/components/animat-dialog";
import UploadImg from "../../../../../wxat-common/components/upload-img";
import './index.scss';

interface PageStateProps {
  appInfo: Record<string, any>;
}

interface PageDispatchProps {}

interface PageOwnProps {}

interface PageState {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface ApplyRefundPage {
  props: IProps;
}

const mapStateToProps = (state) => ({
  appInfo: state.base.appInfo,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class ApplyRefundPage extends React.Component {
  state = {
    tmpStyle: {}, // 主题模板配置

    itemType: null, // 申请售后的商品类型，用于判断显示的申请售后参数
    orderNo: null, // 申请售后的订单编号
    itemList: [], // 申请售后订单的商品列表
    refundItemList: [], // 选中的商品列表
    preSell: false, // 预售商品标志
    refundType: null, // 申请的售后类型，1：退货退款，2：仅退款
    tourismOrderVO: null, // 旅游的订单信息

    applyItemCount: null, // 申请售后的商品数量

    refundStartTime: '', // 客房退住开始日期
    refundEndTime: '', // 客房退住结束日期
    refundStartTimeRange: [], // 客房退住开始日期范围
    refundEndTimeRange: [], // 客房退住结束日期范围

    selectedGoodsStatus: null, // 选择的货物状态
    selectedRefundGist: null, // 选择的退款原因
    currentGoodsStatus: null, // 当前货物状态
    currentRefundGist: null, // 当前退款原因

    refundFee: null, // 申请售后的退款金额
    refundReason: null, // 退款说明
    imgRefundReason: [], // 退款图片凭证

    isEdit: false, // 控制编辑区域显示与否
    allChecked: false, // 是否选中全部商品

    isShowGoodsStatusBox: false, // 货物状态弹框
    isShowRefundGistBox: false, // 退款原因弹框
    // 货物状态列表
    goodsStatusList: [
      {
        value: 0,
        label: '未收到货',
      },

      {
        value: 1,
        label: '已收到货',
      },
    ],

    // 退款原因列表
    refundGistList: [],

    refundOrderNo: null, // 当前修改的退款订单号

    partialRefund: 1, // 是否允许整单商品中单商品退款，0：不允许，1：允许。默认为1
  };

  refBottomDialogCMPT: Taro.RefObject<any> = React.createRef();

  componentDidMount() {
    const applyRefundInfo = Object.assign({}, protectedMailBox.read('applyRefundInfo') || {});
    if (applyRefundInfo) {
      // 判断是否有退款单号，有则是修改退款申请
      if (applyRefundInfo.refundOrderNo && applyRefundInfo.status !== -50) {
        this.setState({
          refundOrderNo: applyRefundInfo.refundOrderNo || null, // 当前修改的退款订单号
          imgRefundReason: [], // 退款图片凭证，进入页面再次赋值为空数组是为了避免选择了图片之后退出当前页面再进来是会保留之前选择的图片
        });

        this.getRefundDetail(applyRefundInfo.refundOrderNo || null); // 查询退款订单详情
      } else {
        let applyItemCount = 0; // 申请售后的商品数量
        if (Array.isArray(applyRefundInfo.refundItemList) && applyRefundInfo.refundItemList.length > 1) {
          applyRefundInfo.refundItemList.forEach((item, index) => {
            applyItemCount += item.itemCount;
          });
        } else {
          applyItemCount = applyRefundInfo.refundItemList[0].itemCount;
        }

        // 获取退款原因列表
        const refundGistList = this.getRefundGistList(applyRefundInfo.itemType || null);

        this.setState({
          status: applyRefundInfo.status || null,
          itemType: applyRefundInfo.itemType || null, // 申请售后的商品类型
          orderNo: applyRefundInfo.orderNo || null, // 申请售后的订单编号
          itemList: applyRefundInfo.itemList || [], // 申请售后订单的商品列表
          refundItemList: applyRefundInfo.refundItemList || [], // 选中的商品列表
          preSell: applyRefundInfo.preSell || null, // 预售商品标志
          refundType: applyRefundInfo.refundType || null, // 申请的售后类型，1：退货退款，2：仅退款
          applyItemCount: applyItemCount || null, // 申请售后的商品数量
          refundGistList: refundGistList, // 退款原因列表
          imgRefundReason: [], // 退款图片凭证
          tourismOrderVO: applyRefundInfo.tourismOrderVO || null, // 旅游的订单信息
        });

        // 设置酒店退住时间及退住时间范围
        const tourismOrderVO: any = applyRefundInfo.tourismOrderVO;
        let refundStartTime = '';
        let refundEndTime = '';
        if (tourismOrderVO && tourismOrderVO.beginTime && tourismOrderVO.endTime) {
          // 判断当前日期是否大于酒店入住日期，是则将当前如期设置为退住时间开始日期
          const beginTime =
            new Date() > new Date(tourismOrderVO.beginTime) ? new Date() : new Date(tourismOrderVO.beginTime);
          refundStartTime = utilDate.format(beginTime, 'yyyy-MM-dd'); // 转换时间类型
          refundEndTime = utilDate.format(new Date(tourismOrderVO.endTime), 'yyyy-MM-dd'); // 转换时间类型
          if (refundStartTime && refundEndTime) {
            this.setState({
              refundStartTime: refundStartTime, // 客房退住开始日期
              refundEndTime: refundEndTime, // 客房退住结束日期
              refundStartTimeRange: [refundStartTime, refundEndTime], // 客房退住开始日期范围
              refundEndTimeRange: [refundStartTime, refundEndTime], // 客房退住结束日期范围
            });
          }
        }

        this.queryRefundFee(
          applyRefundInfo.orderNo,
          applyItemCount,
          applyRefundInfo.refundItemList,
          refundStartTime,
          refundEndTime
        );

        // 查询申请售后的退款金额
      }
    }

    this.setState({
      partialRefund: this.props.appInfo && this.props.appInfo.partialRefund === 0 ? 0 : 1, // 是否允许整单商品中单商品退款，0：不允许，1：允许。默认为1
    });

    // 动态设置当前页面标题
    wxApi.setNavigationBarTitle({
      title: this.state.refundType === 1 ? '申请退款退货' : '申请退款',
    });

    this.getTemplateStyle(); // 获取主题模板配置
  }

  /**
   * 获取主题模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }

    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
   * 退款原因列表
   */
  getRefundGistList(type) {
    // 判断是否为客房订单，是则更改退款理由
    if (type === 41) {
      return [
        {
          value: 8,
          label: "会员都没过期，扫码就提示付费了",
        },
        {
          value: 9,
          label: "付费后提示试看，播放提示无版权",
        },
        {
          value: 10,
          label: "网络问题，断网或网络太慢",
        },
        {
          value: 11,
          label: "电视设备问题，闪退/黑屏/无画面/无声音",
        },
        {
          value: 12,
          label: "电视屏幕提示未安装软件/播放器",
        },
        {
          value: 13,
          label: "热门综艺电视剧没有更新",
        },
        {
          value: 14,
          label: "其他请说明",
        }
      ]
    } else {
      return [
        {
          value: 0,
          label: '无理由退款',
        },

        {
          value: 1,
          label: '配送超时',
        },

        {
          value: 2,
          label: '卖家发错货',
        },

        {
          value: 3,
          label: '少发/漏发',
        },

        {
          value: 4,
          label: '包装/商品破损/污渍',
        },

        {
          value: 5,
          label: '商品信息描述不符',
        },

        {
          value: 6,
          label: '已过/临近保质期',
        },

        {
          value: 7,
          label: '质量问题',
        },
      ];
    }
  }

  /**
   * 查询退款订单详情
   */
  getRefundDetail(refundOrderNo) {
    wxApi
      .request({
        url: api.order.refundDetail,
        loading: true,
        data: {
          refundOrderNo: refundOrderNo, // 退款订单编号
        },
      })
      .then((res) => {
        if (res.data) {
          // 获取退款原因列表
          const refundGistList = this.getRefundGistList();
          this.setState({
            refundGistList: refundGistList, // 退款原因列表
          });
          // 退款原因转换
          const refundGistObj = refundGistList.find((item) => {
            return item.label === res.data.refundGist;
          });
          let refundGist = null;
          if (refundGistObj) {
            refundGist = refundGistObj.value;
          }

          // 退款图片凭证，将后台获取的退款图片做处理，避免修改退款申请时清空之前的图片后，后台的退款图片凭证中还是返回一个有值的参数，引起前端显示错误
          const refundReasonImgPaths = res.data.refundReasonImgPaths;
          let imgRefundReason = [];
          if (refundReasonImgPaths && refundReasonImgPaths[0] !== '[]') {
            imgRefundReason = refundReasonImgPaths;
          }

          let applyItemCount = 0; // 申请售后的商品数量
          if (res.data.itemList.length > 1) {
            res.data.itemList.forEach((item, index) => {
              applyItemCount += item.itemCount;
            });
          } else {
            applyItemCount = res.data.itemList[0].itemCount;
          }

          this.setState({
            itemType: res.data.itemType || null, // 申请售后的商品类型
            orderNo: res.data.orderNo || null, // 申请售后的订单编号
            refundItemList: res.data.itemList || [], // 申请售后订单的商品列表
            preSell: res.data.preSell || null, // 预售商品标志
            currentGoodsStatus: res.data.goodsStatus || res.data.goodsStatus === 0 ? res.data.goodsStatus : null, // 当前货物状态
            currentRefundGist: refundGist || refundGist === 0 ? refundGist : null, // 当前退款原因
            refundFee: res.data.refundFee || null, // 退款金额
            applyItemCount: applyItemCount || null, // 申请售后的商品数量5759801654243893007
            refundReason: res.data.refundReason || null, // 退款说明
            imgRefundReason: imgRefundReason, // 退款图片凭证
            tourismOrderVO: res.data.tourismOrderVO || null, // 旅游的订单信息
          });

          // 设置酒店退住时间
          if (res.data.itemList[0].refundStartTime && res.data.itemList[0].refundEndTime) {
            const refundStartTime = utilDate.format(new Date(res.data.itemList[0].refundStartTime), 'yyyy-MM-dd'); // 转换时间类型
            const refundEndTime = utilDate.format(new Date(res.data.itemList[0].refundEndTime), 'yyyy-MM-dd'); // 转换时间类型
            if (refundStartTime && refundEndTime) {
              this.setState({
                refundStartTime: refundStartTime, // 客房退住开始日期
                refundEndTime: refundEndTime, // 客房退住结束日期
              });
            }
          }
          // 设置酒店退住时间范围
          const tourismOrderVO = this.state.tourismOrderVO;
          if (tourismOrderVO && tourismOrderVO.beginTime && tourismOrderVO.endTime) {
            const timeRange0 = utilDate.format(new Date(tourismOrderVO.beginTime), 'yyyy-MM-dd'); // 转换时间类型
            const timeRange1 = utilDate.format(new Date(tourismOrderVO.endTime), 'yyyy-MM-dd'); // 转换时间类型
            if (timeRange0 && timeRange1) {
              this.setState({
                refundStartTimeRange: [timeRange0, timeRange1], // 客房退住开始日期范围
                refundEndTimeRange: [timeRange0, timeRange1], // 客房退住结束日期范围
              });
            }
          }
        }
      })
      .catch((error) => {
        if (error.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '退款订单详情查询失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.errorMessage,
          });
        }
      });
  }

  /**
   * 申请的商品数量减1
   * @param {*} e
   */
  /* handleDecreaseItemCount(e) {
                                                    if (this.state.applyItemCount > 1) {
                                                      let currentItemCount = this.state.applyItemCount;
                                                      currentItemCount--;
                                                       this.setState({
                                                        applyItemCount: currentItemCount //申请的商品数量
                                                      })
                                                       this.queryRefundFee(); // 查询申请售后的退款金额
                                                    } else {
                                                      wx.showToast({
                                                        icon: 'none',
                                                        title: '申请的商品数量最少为一件'
                                                      });
                                                    }
                                                  }, */

  /**
   * 申请的商品数量加1
   * @param {*} e
   */
  /* handleIncreaseItemCount(e) {
                                                                                                                                                                                                                                                                                                            const itemCount = e.currentTarget.dataset.itemCount
                                                                                                                                                                                                                                                                                                            if (this.state.applyItemCount < itemCount) {
                                                                                                                                                                                                                                                                                                              let currentItemCount = this.state.applyItemCount;
                                                                                                                                                                                                                                                                                                              currentItemCount++;
                                                                                                                                                                                                                                                                                                               this.setState({
                                                                                                                                                                                                                                                                                                                applyItemCount: currentItemCount //申请的商品数量
                                                                                                                                                                                                                                                                                                              })
                                                                                                                                                                                                                                                                                                               this.queryRefundFee(); // 查询申请售后的退款金额
                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                              wx.showToast({
                                                                                                                                                                                                                                                                                                                icon: 'none',
                                                                                                                                                                                                                                                                                                                title: '申请的商品数量不可以超过商品总数量'
                                                                                                                                                                                                                                                                                                              });
                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                          }, */

  /**
   * 添加或编辑申请的商品列表
   */
  handleAddOrEdit = () => {
    let { itemList, refundItemList, allChecked } = this.state;

    // 循环判断哪些商品是已被选中的
    itemList.forEach((item: any) => {
      item.checked = false;
      refundItemList.forEach((rItem) => {
        if (rItem.id === item.id) {
          item.checked = true;
        }
      });
    });

    // 判断是否已选中所有商品
    if (itemList.length === refundItemList.length) {
      allChecked = true; // 将全选按钮选中
    } else {
      allChecked = false; // 将全选按钮去除选中
    }

    // 刷新值
    this.setState({
      isEdit: true, // 显示编辑区
      itemList: itemList, // 申请售后订单的商品列表
      allChecked: allChecked, // 是否选中全部商品
    });
  };

  /**
   * 选中或删除某一个商品
   * @param {*} e
   */
  handleCheck = (index: number) => {
    // const index = e.currentTarget.dataset.index; // 操作的商品下标位置
    const item = this.state.itemList[index]; // 操作的商品

    // 判断该操作的商品是否已选中，是则返回该商品在选中的商品列表中的下标位置，否则返回-1
    const isHashIndex = this.state.refundItemList.findIndex((t) => {
      return t.id == item.id;
    });

    // 判断是有下标位置，是则去除选中，否则选中该商品
    if (isHashIndex !== -1) {
      this.state.itemList[index].checked = false; //  去除选中
      this.state.refundItemList.splice(isHashIndex, 1); // 在选中的商品列表中删除该商品
    } else {
      this.state.itemList[index].checked = true; // 选中该商品
      this.state.refundItemList.push(item); // 在选中的商品列表中添加该商品
    }

    // 判断是否已选中所有商品
    if (this.state.itemList.length === this.state.refundItemList.length) {
      this.state.allChecked = true; // 将全选按钮选中
    } else {
      this.state.allChecked = false; // 将全选按钮去除选中
    }

    // 刷新值
    this.setState({
      itemList: this.state.itemList, // 申请售后订单的商品列表
      refundItemList: this.state.refundItemList, // 选中的商品列表
      allChecked: this.state.allChecked, // 是否选中全部商品
    });
  };

  /**
   * 选中全部商品
   * @param {*} e
   */
  handleAllCheck = () => {
    this.state.refundItemList = [];
    if (this.state.allChecked) {
      this.state.itemList.forEach((item) => {
        item.checked = false;
      });
      this.state.allChecked = false;
    } else {
      this.state.itemList.forEach((item) => {
        item.checked = true;
        this.state.refundItemList.push(item);
      });
      this.state.allChecked = true;
    }

    // 刷新值
    this.setState({
      itemList: this.state.itemList, // 申请售后订单的商品列表
      refundItemList: this.state.refundItemList, // 选中的商品列表
      allChecked: this.state.allChecked, // 是否选中全部商品
    });
  };

  /**
   * 提交编辑申请的商品列表
   */
  handleCommitEdit = () => {
    let refundItemCount = 0; // 申请售后的退款金额
    this.state.refundItemList.forEach((item) => {
      // 申请售后的商品数量
      refundItemCount = parseInt(refundItemCount + item.itemCount);
    });

    // 刷新值
    this.setState(
      {
        isEdit: false, // 隐藏编辑区
        applyItemCount: refundItemCount, // 申请售后的商品数量
      },
      () => {
        this.queryRefundFee(); // 查询申请售后的退款金额
      }
    );
  };

  // 查询申请售后的退款金额
  queryRefundFee = (
    orderNo = this.state.orderNo,
    applyItemCount = this.state.applyItemCount,
    refundItemList = this.state.refundItemList,
    refundStartTime = this.state.refundStartTime,
    refundEndTime = this.state.refundEndTime
  ) => {
    const params = {
      orderNo, // 订单编号
    };

    // 申请选中的商品列表
    refundItemList.forEach((item, index) => {
      params['requestItemDTOS[' + index + '].orderItemId'] = item.id;
      params['requestItemDTOS[' + index + '].itemNo'] = item.itemNo;
      params['requestItemDTOS[' + index + '].skuId'] = item.skuId;
      params['requestItemDTOS[' + index + '].itemCount'] = item.itemCount;
    });
    // 只有一个商品的时候
    if (refundItemList.length === 1) {
      params['requestItemDTOS[0].itemCount'] = applyItemCount;
    }

    // 在申请退款的商品列表中添加退房日期
    if (refundStartTime && refundEndTime) {
      this.state.refundItemList.forEach((item, index) => {
        params['requestItemDTOS[' + index + '].refundStartTime'] = refundStartTime;
        params['requestItemDTOS[' + index + '].refundEndTime'] = refundEndTime;
      });
      // 只有一个商品的时候
      if (refundItemList.length === 1) {
        params['requestItemDTOS[0].refundStartTime'] = refundStartTime;
        params['requestItemDTOS[0].refundEndTime'] = refundEndTime;
      }
    }

    wxApi
      .request({
        url: api.order.queryRefundFee,
        loading: true,
        data: params,
      })
      .then((res) => {
        // 赋值后台获取到的退款金额
        this.setState({
          refundFee: res.data || null,
        });
      })
      .catch((error) => {
        if (error.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '退款金额查询失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.errorMessage,
          });
        }
      });
  };

  /**
   * 选择退住日期
   * @param {*} e
   */
  bindDateChange = (e: any, key: string) => {
    // const key = e.currentTarget.dataset.key; // 要改变的参数名
    const value = e.detail.value; // 要改变的参数值
    const refundStartTime = key === 'refundStartTime' ? value : this.state.refundStartTime;
    const refundEndTime = key === 'refundEndTime' ? value : this.state.refundEndTime;

    if (!refundStartTime) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择退房开始日期',
      });
    } else if (!refundEndTime) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择退房结束日期',
      });
    } else if (new Date(refundStartTime) >= new Date(refundEndTime)) {
      wxApi.showToast({
        icon: 'none',
        title: '退房开始日期必须小于结束日期',
      });

      return;
    }

    this.setState(
      {
        [key]: value,
      },

      () => {
        if (refundStartTime && refundEndTime) {
          this.queryRefundFee(); // 查询申请售后的退款金额
        }
      }
    );
  };

  /**
   * 显示选择退款属性对话弹框
   * @param {*} e
   */
  showBottomDialog = (boxType: string) => {
    // 判断是否有退款单号，有则是修改退款申请，不允许修改退款原因
    // if (this.state.refundOrderNo) {
    //   return;
    // }

    this.refBottomDialogCMPT.current && this.refBottomDialogCMPT.current.showModal();

    // 显示当前弹框之前先隐藏全部弹框
    this.setState({
      isShowGoodsStatusBox: false, // 货物状态弹框
      isShowRefundGistBox: false, // 退款原因弹框
    });

    if (boxType === 'goodsStatus') {
      this.setState({
        isShowGoodsStatusBox: true, // 货物状态弹框
        selectedGoodsStatus: this.state.currentGoodsStatus, // 选择的货物状态
      });
    } else if (boxType === 'refundGist') {
      this.setState({
        isShowRefundGistBox: true, // 退款原因弹框
        selectedRefundGist: this.state.currentRefundGist, // 选择的退款原因
      });
    }
  };

  /**
   * 隐藏选择退款属性对话弹框
   * @param {*} e
   */
  hideBottomDialog = () => {
    this.refBottomDialogCMPT.current && this.refBottomDialogCMPT.current.hideModal();

    // 隐藏全部弹框
    this.setState({
      isShowGoodsStatusBox: false, // 货物状态弹框
      isShowRefundGistBox: false, // 退款原因弹框
    });
  };

  /**
   * 选择的货物状态
   * @param {*} e
   */
  handleChangeGoodsStatus = (option: number) => {
    // const option = e.currentTarget.dataset.option;
    this.setState({
      selectedGoodsStatus: option,
    });
  };

  /**
   * 确定选择的货物状态
   * @param {*} e
   */
  handleConfirmGoodsStatus = () => {
    this.setState({
      currentGoodsStatus: this.state.selectedGoodsStatus, // 确定的货物状态
    });
    this.hideBottomDialog(); // 隐藏选择退款属性对话弹框
  };

  /**
   * 选择的退款原因
   * @param {*} e
   */
  handleChangeRefundGist = (option) => {
    this.setState({
      selectedRefundGist: option,
    });
  };

  /**
   * 确定选择的退款原因
   * @param {*} e
   */
  handleConfirmRefundGist = () => {
    this.setState({
      currentRefundGist: this.state.selectedRefundGist, // 确定的退款原因
    });
    this.hideBottomDialog(); // 隐藏选择退款属性对话弹框
  };

  /**
   * 退款说明输入
   */
  refundReasonInput = (e) => {
    const refundReason = e.detail.value.trim();
    this.setState({
      refundReason,
    });

    // let refundReason = this.state.refundReason;
    if (refundReason.length > 32) {
      wxApi.showToast({
        icon: 'none',
        title: '退款说明长度为32个字符以内，请重新输入',
      });
    }
  };

  /**
   * 获取上传的图片
   * @param {*} e
   */
  handleImagesChange = (images: string[]) => {
    // 刷新值
    this.setState({
      imgRefundReason: images, // 退款原因说明图片
    });
  };

  /**
   * 提交退款订单申请
   */
  handleCommitRefund = () => {
    // 判断是否有退款单号，无则是首次申请退款，有则是修改退款申请
    if (!this.state.refundOrderNo) {
      this.firstApplyRefund(); // 首次申请退款
    } else {
      this.updateApplyRefund(); // 修改退款申请
    }
  };

  /**
   * 首次申请退款
   */
  firstApplyRefund = () => {
    const {
      orderNo,
      refundFee,
      refundType,
      refundItemList,
      itemType,
      currentGoodsStatus,
      currentRefundGist,
      refundGistList,
      refundReason,
      imgRefundReason,
      refundStartTime,
      refundEndTime,
    } = this.state;
    const params = {
      orderNo, // 订单编号
      refundFee: refundFee || 0, // 退款金额
      refundType, // 申请的售后类型，1：退货退款，2：仅退款
    };

    if (refundItemList.length < 1) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择退款的商品',
      });

      return;
    }

    // 申请选中的商品列表
    refundItemList.forEach((item, index) => {
      params['requestItemDTOS[' + index + '].orderItemId'] = item.id;
      params['requestItemDTOS[' + index + '].itemNo'] = item.itemNo;
      params['requestItemDTOS[' + index + '].skuId'] = item.skuId;
      params['requestItemDTOS[' + index + '].itemCount'] = item.itemCount;
    });
    // 只有一个商品的时候
    if (refundItemList.length === 1) {
      params['requestItemDTOS[0].itemCount'] = this.state.applyItemCount;
    }

    // 判断货物状态是否展示
    if (!currentGoodsStatus && currentGoodsStatus !== 0) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择货物状态',
      });

      return;
    } else {
      params.goodsStatus = currentGoodsStatus; // 当前货物状态
    }

    // 当前退款原因
    if (!currentRefundGist && currentRefundGist !== 0) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择退款原因',
      });

      return;
    } else {
      const refundGist = refundGistList.find((item) => {
        return item.value === currentRefundGist;
      });
      params.refundGist = refundGist.label || null;
    }

    // 判断退款说明输入是否正确
    if (!!refundReason && refundReason.length > 32) {
      wxApi.showToast({
        icon: 'none',
        title: '退款说明长度为32个字符以内，请重新输入',
      });

      return;
    } else if (refundReason) {
      params.refundReason = refundReason; // 退款原因
    }

    // 退款原因说明图片
    if (imgRefundReason && imgRefundReason.length > 0) {
      imgRefundReason.forEach((item, index) => {
        params['imgRefundReason[' + index + ']'] = item;
      });
    }
    // 退款通知的订阅消息
    const Ids = [subscribeEnum.REFUND_SUCCESS.value];
    // 授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      wxApi
        .request({
          url: api.order.refundOrder,
          loading: true,
          data: params,
        })
        .then((res) => {
          const params = {
            refundOrderNo: res.data.refundOrderNo, // 退款订单编号
          };
          protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.refundDetail, 'refundDetailInfo', params);
          wxApi.$redirectTo({
            url: pageLinkEnum.orderPkg.afterSale.refundDetail,
          });
        })
        .catch((error) => {
          if (error.errorCode === 1001) {
            wxApi.showToast({
              icon: 'none',
              title: '提交退款申请失败，请稍后重试',
            });
          } else {
            wxApi.showToast({
              icon: 'none',
              title: error.errorMessage,
            });
          }
        });
    });
  };

  /**
   * 修改退款申请
   */
  updateApplyRefund = () => {
    const {
      refundOrderNo,
      currentRefundGist,
      currentGoodsStatus,
      refundGistList,
      refundReason,
      imgRefundReason,
    } = this.state;
    const params = {
      refundOrderNo: refundOrderNo, // 退款订单号
    };

    // 判断货物状态是否展示
    params.goodsStatus = currentGoodsStatus;

    // 退款原因
    const refundGist = refundGistList.find((item) => {
      return item.value === currentRefundGist;
    });
    params.refundGist = refundGist.label || null;

    // 判断退款说明输入是否正确
    if (!!refundReason && refundReason.length > 32) {
      wxApi.showToast({
        icon: 'none',
        title: '退款说明长度为32个字符以内，请重新输入',
      });

      return;
    } else if (refundReason) {
      params.refundReason = refundReason; // 退款说明
    } else {
      params.refundReason = ''; // 退款说明
    }

    // 退款凭证图片
    if (imgRefundReason && imgRefundReason.length > 0) {
      imgRefundReason.forEach((item, index) => {
        params['imgRefundReason[' + index + ']'] = item;
      });
    } else {
      params.imgRefundReason = [];
    }

    // 退款通知的订阅消息
    const Ids = [subscribeEnum.REFUND_SUCCESS.value];
    // 授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      wxApi
        .request({
          url: api.order.updateApplyRefund,
          loading: true,
          data: params,
        })
        .then((res) => {
          const params = {
            refundOrderNo: this.state.refundOrderNo, // 退款订单编号
          };
          protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.refundDetail, 'refundDetailInfo', params);
          // 修改申请成功后返回上一页面，即退款详情页面，避免点左上角返回按钮时出现两次退款详情页面
          wxApi.$navigateBack({
            delta: 1,
          });
        })
        .catch((error) => {
          if (error.errorCode === 1001) {
            wxApi.showToast({
              icon: 'none',
              title: '提交退款申请失败，请稍后重试',
            });
          } else {
            wxApi.showToast({
              icon: 'none',
              title: error.errorMessage,
            });
          }
        });
    });
  };

  render() {
    const {
      preSell,
      itemType,
      refundItemList,
      refundFee,
      applyItemCount,
      partialRefund,
      itemList,
      refundOrderNo,
      currentGoodsStatus,
      goodsStatusList,
      refundStartTime,
      refundStartTimeRange,
      refundEndTime,
      refundEndTimeRange,
      currentRefundGist,
      refundGistList,
      refundReason,
      imgRefundReason,
      tmpStyle,
      isEdit,
      allChecked,
      selectedGoodsStatus,
      isShowGoodsStatusBox,
      selectedRefundGist,
      isShowRefundGistBox,
    } = this.state;
    return (
      <View data-scoped='wk-paa-ApplyRefund' className='wk-paa-ApplyRefund apply-refund'>
        {!isEdit && (
          <View className='refund-box'>
            <View className='top'>
              <View className='goods-box'>
                {refundItemList.length === 1 && (
                  <Block>
                    {refundItemList.map((item: any, idx) => {
                      return (
                        <View className='one-goods-box' key={idx}>
                          <View className='img-box'>
                            <Image src={item.thumbnail} className='img' mode='aspectFill'></Image>
                            {/*  预售标志  */}
                            {!!preSell && <View className='pre-sale-label'>预售</View>}
                          </View>
                          {/*  商品详情  */}
                          <View className='goods-info'>
                            <View className='name limit-line'>{item.itemName}</View>
                            {/*  商品规格  */}
                            {!!item.itemAttribute && <View className='attribute'>{'规格：' + item.itemAttribute}</View>}
                            {/*  商品数量及单价  */}
                            <View className='count'>
                              <Text className='num'>{'x' + item.itemCount}</Text>
                              <Text className='price'>
                                {
                                  item.metadataJson && item.metadataJson?.marketActivityPrice
                                    ? '单价:￥' + filters.moneyFilter(Number(item.metadataJson?.marketActivityPrice), true)
                                    : '单价:￥' + filters.moneyFilter(item.salePrice, true)
                                }
                              </Text>
                            </View>
                          </View>
                        </View>
                      );
                    })}
                  </Block>
                )}

                {/*  多件商品申请售后显示模块  */}
                {refundItemList.length > 1 && (
                  <View className='more-goods-box'>
                    <View className='left'>
                      <ScrollView scrollX>
                        <View className='scroll-content'>
                          {refundItemList.map((item, index) => {
                            return (
                              <View className='img-box' key={index}>
                                <Image src={item.thumbnail} className='img'></Image>
                                {!!preSell && <View className='pre-sale-label'>预售</View>}
                              </View>
                            );
                          })}
                        </View>
                      </ScrollView>
                    </View>
                    <View className='right'>
                      <View className='right-content'>
                        <View className='refund-price'>{filters.moneyFilter(refundFee, true)}</View>
                        {!!applyItemCount && <View className='refund-item-count'>{'共' + applyItemCount + '件'}</View>}
                      </View>
                    </View>
                  </View>
                )}
              </View>
              {/*  添加和编辑按钮  */}
              {!!(partialRefund && itemList.length > 1 && !refundOrderNo) && (
                <View className='edit-btn' onClick={this.handleAddOrEdit}>
                  添加和编辑
                </View>
              )}
            </View>
            {/*  退款详细信息编辑区域  */}
            <View className='refund-container'>
              {false && (
                <View className='tab'>
                  <View className='text-type'>商品数量</View>
                  <View className='text-box'>
                    <View className='text'></View>
                    <View className='num-box'>
                      <View
                        className='num-decrease'
                        // onClick={this.handleDecreaseItemCount}
                        data-item-count={refundItemList[0].itemCount}
                      >
                        -
                      </View>
                      <View className='num-input-box'>
                        <Input className='num-input' type='number' value={applyItemCount} disabled></Input>
                      </View>
                      <View
                        className='num-increase'
                        // onClick={this.handleIncreaseItemCount}
                        data-item-count={refundItemList[0].itemCount}
                      >
                        +
                      </View>
                    </View>
                  </View>
                </View>
              )}

              <View className='tab' onClick={() => this.showBottomDialog('goodsStatus')}>
                  <View className='tab-type'>货物状态</View>
                  <View className='text-box'>
                    {!!(!currentGoodsStatus && currentGoodsStatus !== 0) && (
                      <View className='text no-value'>请选择</View>
                    )}

                    {goodsStatusList
                      .filter((goodsStatusItem) => {
                        return goodsStatusItem.value === currentGoodsStatus;
                      })
                      .map((goodsStatusItem, index) => {
                        return (
                          <View className='text' key={index}>
                            {goodsStatusItem.label}
                          </View>
                        );
                      })}
                    <Image className='arrow-select' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
                  </View>
                </View>

              {/*  非客房订单不展示退住日期  */}
              <View className='tab' onClick={() => this.showBottomDialog('refundGist')}>
                <View className='tab-type'>退款原因</View>
                <View className='text-box'>
                  {!!(!currentRefundGist && currentRefundGist !== 0) && <View className='text no-value'>请选择</View>}
                  {refundGistList
                    .filter((refundGistItem) => {
                      return refundGistItem.value === currentRefundGist;
                    })
                    .map((refundGistItem) => {
                      return <View className='text'>{refundGistItem.label}</View>;
                    })}
                  <Image className='arrow-select' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
                </View>
              </View>
              <View className='tab'>
                <View className='tab-type'>退款金额</View>
                <View className='text price'>{'￥' + filters.moneyFilter(refundFee, true)}</View>
              </View>
              <View className='tab'>
                <View className='tab-type'>退款说明：</View>
                <Input
                  className='tab-input'
                  placeholder='选填'
                  placeholderStyle='color:rgba(194,194,194,1)'
                  value={refundReason}
                  onInput={this.refundReasonInput}
                ></Input>
              </View>
              {/*  图片上传模块  */}
              <View className='tab upload-box'>
                <View className='tab-type'>上传凭证 (最多3张)</View>
                <View className='img-box'>
                  {!!(refundItemList && refundItemList.length > 0) && (
                    <UploadImg onChange={this.handleImagesChange} value={imgRefundReason} max={3}></UploadImg>
                  )}
                </View>
              </View>
            </View>
            {/*  底部按钮  */}
            <View className='btn-box'>
              <View
                className='btn-commit'
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleCommitRefund}
              >
                提交
              </View>
            </View>
          </View>
        )}

        {/*  编辑退货商品区域  */}
        {!!isEdit && (
          <View className='edit-box'>
            <View className='goods-box'>
              {itemList.map((item, index) => {
                return (
                  <View className='tab' key={index} onClick={() => this.handleCheck(index)}>
                    <Radio value={index} checked={item.checked} color={tmpStyle.btnColor}></Radio>
                    {/*  商品图片  */}
                    <View className='img-box'>
                      <Image src={item.thumbnail} className='img'></Image>
                      {!!preSell && <View className='pre-sale-label'>预售</View>}
                    </View>
                    {/*  商品信息详情  */}
                    <View className='goods-info'>
                      <View className='name limit-line'>{item.itemName}</View>
                      {!!item.itemAttribute && <View className='attribute'>{'规格：' + item.itemAttribute}</View>}
                      <View className='count'>
                        <Text className='num'>{item.itemCount + '件'}</Text>
                        <Text className='price'>{'单价:￥' + filters.moneyFilter(item.salePrice, true)}</Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
            {/*  底部按钮  */}
            <View className='btn-box'>
              <View
                className='all-check'
                style={_safe_style_('background-color:' + tmpStyle.btnColor1)}
                onClick={this.handleAllCheck}
              >
                <Radio value={allChecked} checked={allChecked} color={tmpStyle.btnColor}></Radio>
                <Text>全选</Text>
              </View>
              <View
                className='btn-commit'
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleCommitEdit}
              >
                提交
              </View>
            </View>
          </View>
        )}

        {/*  选择退款属性对话弹框  */}
        <BottomDialog ref={this.refBottomDialogCMPT}>
          {!!isShowGoodsStatusBox && (
            <View className='select-box goods-status-box'>
              <View className='title'>货物状态</View>
              <View className='options'>
                {goodsStatusList.map((item, index) => {
                  return (
                    <View className='option-tab' key={index} onClick={() => this.handleChangeGoodsStatus(item.value)}>
                      <Text className='option-tab__text'>{item.label}</Text>
                      <View
                        className='logo-select'
                        style={_safe_style_(
                          'background-color:' +
                            (selectedGoodsStatus === item.value ? tmpStyle.btnColor : '') +
                            ';border-color:' +
                            (selectedGoodsStatus === item.value ? tmpStyle.btnColor : 'rgba(93, 93, 93, 1)')
                        )}
                      >
                        {selectedGoodsStatus === item.value && (
                          <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
                        )}
                      </View>
                    </View>
                  );
                })}
              </View>
              <View
                className='btn'
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleConfirmGoodsStatus}
              >
                确定
              </View>
            </View>
          )}

          {/*  退款原因选择弹框  */}
          {!!isShowRefundGistBox && (
            <View className='select-box refund-gist-box'>
              <View className='title'>退款原因</View>
              <View className='options'>
                {refundGistList.map((item: any, index) => {
                  return (
                    <View className='option-tab' key={index} onClick={() => this.handleChangeRefundGist(item.value)}>
                      <Text className='option-tab__text'>{item.label}</Text>
                      <View
                        className='logo-select'
                        style={_safe_style_(
                          'background-color:' +
                            (selectedRefundGist === item.value ? tmpStyle.btnColor : '') +
                            ';border-color:' +
                            (selectedRefundGist === item.value ? tmpStyle.btnColor : 'rgba(93, 93, 93, 1)')
                        )}
                      >
                        {selectedRefundGist === item.value && (
                          <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
                        )}
                      </View>
                    </View>
                  );
                })}
              </View>
              <View
                className='btn'
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleConfirmRefundGist}
              >
                确定
              </View>
            </View>
          )}
        </BottomDialog>
      </View>
    );
  }
}

export default ApplyRefundPage as ComponentClass<PageOwnProps, PageState>;
