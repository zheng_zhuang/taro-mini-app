import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ComponentClass } from 'react';

import hoc from '@/hoc/index';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import stepFilters from '../step-filter.wxs.js';
import template from '../../../../../wxat-common/utils/template.js';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import goodsTypeEnum from '../../../../../wxat-common/constants/goodsTypeEnum.js';
import utilDate from '../../../../../wxat-common/utils/date.js';
import pageLinkEnum from '../../../../../wxat-common/constants/pageLinkEnum.js';

import './index.scss';

interface RefundItem {
  thumbnail: string;
  itemName: string;
  itemAttribute: string;
  itemCount: number;
  salePrice: number;
}

type PageStateProps = {};

type PageDispatchProps = {};

type PageOwnProps = {};

type PageState = {
  refundOrderNo: string; // 退款订单编号

  refundDetail: {
    itemList: Array<RefundItem>;
    status: number;
    refundType: number;
    preSell?: boolean;
    refundFee?: number;
    refundUserGiftCardFee?: number;
    requestTime?: string;
    refundOrderNo: string;
    refundConsignee: string;
    sellerPhone: string;
  };

  tmpStyle: {
    btnColor: string;
  };
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface RefundDetailPage {
  props: IProps;
}

@hoc
class RefundDetailPage extends React.Component<IProps, PageState> {
  state = {
    // goodsTypeEnum, // 商品类型常量

    refundOrderNo: '', // 退款订单编号

    refundDetail: {
      itemList: [],
      status: 0,
      refundType: 0,
      preSell: false,
      refundFee: 0,
      refundUserGiftCardFee: 0,
      requestTime: '',
      refundOrderNo: '',
      refundConsignee: '',
      refundConsigneePhone: '',
      refundAddress: '',
      sellerPhone: '',
      refundStartTime: '',
      refundEndTime: '',
      refundGist: '',
      refundShippingCompany: '',
      refundShippingNo: '',
      itemType: null,
      goodsStatus: null,
    },

    // 退款订单详情

    tmpStyle: {
      btnColor: '',
      btnColor1: '',
    },

    // 主题模板配置
  };

  componentDidMount() {
    this.getTemplateStyle(); //获取主题模板配置
  }

  componentDidShow() {
    const refundDetailInfo = protectedMailBox.read('refundDetailInfo');
    if (refundDetailInfo) {
      this.setState({
        refundOrderNo: refundDetailInfo.refundOrderNo || null, // 申请售后的订单编号
      });
    }

    this.getRefundDetail(); //查询退款订单详情
  }

  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();

    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //查询退款订单详情
  getRefundDetail() {
    wxApi
      .request({
        url: api.order.refundDetail,
        loading: true,
        data: {
          refundOrderNo: this.state.refundOrderNo, // 退款订单编号
        },
      })
      .then((res) => {
        if (res.data) {
          let refundDetail = res.data || {};
          if (
            res.data.itemList &&
            res.data.itemList.length > 0 &&
            res.data.itemList[0].refundStartTime &&
            res.data.itemList[0].refundEndTime
          ) {
            // 设置酒店退住时间
            const refundStartTime = utilDate.format(new Date(res.data.itemList[0].refundStartTime), 'yyyy-MM-dd'); // 转换时间类型
            const refundEndTime = utilDate.format(new Date(res.data.itemList[0].refundEndTime), 'yyyy-MM-dd'); // 转换时间类型
            if (refundStartTime && refundEndTime) {
              refundDetail.refundStartTime = refundStartTime; // 客房退住开始日期
              refundDetail.refundEndTime = refundEndTime; // 客房退住结束日期
            }
          }
          refundDetail.itemList.map(i => {
            i.orderItemExt = i.orderItemExt ? JSON.parse(i.orderItemExt) : {};
            return i;
          })
          this.setState({
            refundDetail,
          });
        }
      })
      .catch((error) => {
        if (error.data.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '退款订单详情查询失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.data.errorMessage,
          });
        }
      });
  }

  // 复制功能
  handleCopy = (text) => {
    wxApi.setClipboardData({
      data: text,
      success: function (res) {
        wxApi.getClipboardData({
          success: function (res) {
            wxApi.showToast({
              image: "https://bj.bcebos.com/htrip-mp/static/app/images/mine/success.png",
              title: '复制成功',
            });
          },
        });
      },
    });
  };

  // 拨打电话
  handleCallSale = () => {
    wxApi.makePhoneCall({
      phoneNumber: this.state.refundDetail.sellerPhone,
    });
  };

  /**
   * 撤销退款操作
   */
  handleRevertRefund = () => {
    wxApi.showModal({
      title: '温馨提示',
      content: '确定撤销当前退款订单吗？撤销之后无法重新发起退款申请操作',
      success: (res) => {
        if (res.confirm) {
          console.log('用户点击确定');
          wxApi
            .request({
              url: api.order.revertRefund,
              loading: true,
              data: {
                refundOrderNo: this.state.refundOrderNo, // 退款订单编号
              },
            })
            .then((res) => {
              this.getRefundDetail(); //查询退款订单详情
            })
            .catch((error) => {
              if (error.errorCode === 1001) {
                wxApi.showToast({
                  icon: 'none',
                  title: '撤销退款操作失败，请稍后重试',
                });
              } else {
                wxApi.showToast({
                  icon: 'none',
                  title: error.errorMessage,
                });
              }
            });
        } else if (res.cancel) {
          console.log('用户点击取消');
        }
      },
    });
  };

  /**
   * 修改退款申请
   */
  handleUpdateApplyRefund = () => {
    const params = {
      refundOrderNo: this.state.refundOrderNo, // 退款订单编号
    };

    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.applyRefund, 'applyRefundInfo', params);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.afterSale.applyRefund,
    });
  };

  render() {
    const { tmpStyle, refundDetail } = this.state;
    return (
      <View data-scoped='wk-par-RefundDetail' className='wk-par-RefundDetail refund-detail'>
        {stepFilters.refundSuccess(refundDetail.status, refundDetail.refundType) && (
          <View className='refund-status' style={_safe_style_('background-color:' + tmpStyle.btnColor)}>
            退款成功
          </View>
        )}

        {/*  退款进度条  */}
        <View className='steps'>
          <View className='main'>
            <View
              className='tab'
              style={_safe_style_(
                'color:' +
                  (stepFilters.requestRefund(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
              )}
            >
              <View
                className='step-logo'
                style={_safe_style_(
                  'background-color:' +
                    (stepFilters.requestRefund(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
                )}
              >
                <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
              </View>
              <View className='dotted-line'></View>
              <View className='step-text'>发起申请</View>
            </View>
            <View
              className='tab'
              style={_safe_style_(
                'color:' +
                  (stepFilters.agreeRefundRequest(refundDetail.status, refundDetail.refundType)
                    ? tmpStyle.btnColor
                    : '')
              )}
            >
              <View
                className='step-logo'
                style={_safe_style_(
                  'background-color:' +
                    (stepFilters.agreeRefundRequest(refundDetail.status, refundDetail.refundType)
                      ? tmpStyle.btnColor
                      : '')
                )}
              >
                <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
              </View>
              <View className='dotted-line'></View>
              <View className='step-text'>{refundDetail.status === -6 ? '卖家拒绝' : '卖家同意'}</View>
            </View>
            {!!(refundDetail.status !== -6 && refundDetail.refundType !== 2) && (
              <View
                className='tab'
                style={_safe_style_(
                  'color:' +
                    (stepFilters.refundItemShipping(refundDetail.status, refundDetail.refundType)
                      ? tmpStyle.btnColor
                      : '')
                )}
              >
                <View
                  className='step-logo'
                  style={_safe_style_(
                    'background-color:' +
                      (stepFilters.refundItemShipping(refundDetail.status, refundDetail.refundType)
                        ? tmpStyle.btnColor
                        : '')
                  )}
                >
                  <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
                </View>
                <View className='dotted-line'></View>
                <View className='step-text'>快递退货</View>
              </View>
            )}

            {!!(refundDetail.status !== -6 && refundDetail.refundType !== 2) && (
              <View
                className='tab'
                style={_safe_style_(
                  'color:' +
                    (stepFilters.refundSuccess(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
                )}
              >
                <View
                  className='step-logo'
                  style={_safe_style_(
                    'background-color:' +
                      (stepFilters.refundSuccess(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
                  )}
                >
                  <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
                </View>
                <View className='dotted-line'></View>
                <View className='step-text'>退款完成</View>
              </View>
            )}
          </View>
        </View>
        {/*  商品详情区域  */}
        {refundDetail.itemList.map((item: RefundItem, index) => {
          console.log(item)
          return (
            <View className='goods-box' key={index}>
              <View className='img-box'>
                <Image src={item.thumbnail} className='img'></Image>
                {!!refundDetail.preSell && <View className='pre-sale-label'>预售</View>}
              </View>
              {/*  商品信息详情  */}
              <View className='goods-info'>
                <View className='name limit-line'>{item.itemName || ''}</View>
                {!!item.itemAttribute && <View className='attribute'>{item.itemAttribute || ''}</View>}
                <View className='count'>
                  <Text className='num'>{item.itemCount + '件'}</Text>
                  <Text className='price'>
                    {
                      item.orderItemExt && (item.orderItemExt?.marketActivityPrice >= 0)
                        ? '单价:￥' + filters.moneyFilter(Number(item.orderItemExt?.marketActivityPrice), true)
                        : '单价:￥' + filters.moneyFilter(item.salePrice, true)
                    }
                  </Text>
                </View>
              </View>
            </View>
          );
        })}
        {/*  退款详情区域  */}
        <View className='refund-info'>
          <View className='tab'>
            <Text>退款类型：</Text>
            <Text>{refundDetail.refundType === 1 ? '退货退款' : '仅退款'}</Text>
          </View>
          {
            <View className='tab'>
              <Text>货物状态：</Text>
              <Text>{refundDetail.goodsStatus === 1 ? '已收到货' : '未收到货'}</Text>
            </View>
          }

          <View className='tab'>
            <Text>退款原因：</Text>
            <Text>{refundDetail.refundGist}</Text>
          </View>
          <View className='tab'>
            <Text>退款金额：</Text>
            <Text>{'￥' + filters.moneyFilter(refundDetail.refundFee, true)}</Text>
          </View>
          {!!refundDetail.refundUserGiftCardFee && (
            <View className='tab'>
              <Text>礼品卡退款金额：</Text>
              <Text>{'￥' + filters.moneyFilter(refundDetail.refundUserGiftCardFee, true)}</Text>
            </View>
          )}

          {/*  <View class="tab">
                         <text>申请件数：</text>
                         <text>1件</text>
                    </View>  */}
          <View className='tab'>
            <Text>申请时间：</Text>
            <Text>{refundDetail.requestTime}</Text>
          </View>
          <View className='tab'>
            <Text>退款编号：</Text>
            <Text>{refundDetail.refundOrderNo}</Text>
          </View>
          {!!refundDetail.refundConsignee && (
            <View className='tab'>
              <Text>收件人：</Text>
              <Text>{refundDetail.refundConsignee}</Text>
            </View>
          )}

          {!!refundDetail.refundConsigneePhone && (
            <View className='tab'>
              <Text>收件号码：</Text>
              <Text>{refundDetail.refundConsigneePhone}</Text>
            </View>
          )}

          {!!refundDetail.refundAddress && (
            <View className='tab'>
              <Text>退货地址：</Text>
              <Text>{refundDetail.refundAddress}</Text>
            </View>
          )}
        </View>
        {/*  物流信息  */}
        {!!(!!refundDetail.refundShippingCompany && !!refundDetail.refundShippingNo) && (
          <View className='logistics-box'>
            <View className='tab'>
              <Text>物流公司：</Text>
              <Text>{refundDetail.refundShippingCompany}</Text>
            </View>
            <View className='tab'>
              <Text>快递单号：</Text>
              <Text>{refundDetail.refundShippingNo}</Text>
              <Text className='copy' onClick={() => this.handleCopy(refundDetail.refundShippingNo)}>
                复制单号
              </Text>
            </View>
          </View>
        )}

        {/*  订单撤销及修改申请按钮操作区域 refundDetail.status: -1：已发起申请  */}
        {refundDetail.status === -1 && (
          <View className='operation-box'>
            <View className='btn' onClick={this.handleRevertRefund}>
              撤销申请
            </View>
            <View className='btn' onClick={this.handleUpdateApplyRefund}>
              修改申请
            </View>
          </View>
        )}

        {/*  底部按钮  */}
        <View className='btn-box'>
          <View
            className='btn-call'
            style={_safe_style_('background-color:' + tmpStyle.btnColor)}
            onClick={this.handleCallSale}
          >
            拨打电话
          </View>
        </View>
      </View>
    );
  }
}

export default RefundDetailPage as ComponentClass;
