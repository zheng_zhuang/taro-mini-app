import '@/wxat-common/utils/platform';
import React, { ComponentClass } from 'react';
import { Block, View, Image, Text, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import filters from '@/wxat-common/utils/money.wxs';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import api from '@/wxat-common/api/index';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl'

import './index.scss';

interface PageStateProps {}

interface PageDispatchProps {}

interface PageOwnProps {}

interface PageState {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface ApplyTypePage {
  props: IProps;
}

@hoc
class ApplyTypePage extends React.Component {
  state = {
    itemType: null, // 申请售后的商品类型
    orderNo: null, // 申请售后的订单编号
    itemList: [], // 申请售后订单的商品列表
    refundItemList: [], // 选中的商品列表
    preSell: false, // 预售商品标志
    showReturn: false, // 是否显示退款退货类型
    tourismOrderVO: null, // 旅游的订单信息

    refundFee: null, // 申请售后的退款金额
    applyItemCount: null, // 申请售后的商品数量

    tmpStyle: {}, // 主题模板配置
  };

  componentDidMount() {
    const applyOrderInfo = Object.assign({}, protectedMailBox.read('applyOrderInfo') || {});
    if (applyOrderInfo) {
      this.setState({
        status: applyOrderInfo.status || null,
        refundOrderNo: applyOrderInfo.refundOrderNo || null,
        itemType: applyOrderInfo.itemType || null, // 申请售后的商品类型
        orderNo: applyOrderInfo.orderNo || null, // 申请售后的订单编号
        itemList: applyOrderInfo.itemList || [], // 申请售后订单的商品列表
        refundItemList: applyOrderInfo.refundItemList || [], // 选中的商品列表
        preSell: applyOrderInfo.preSell || null, // 预售商品标志
        showReturn: applyOrderInfo.showReturn || null, // 显示退款退货
        tourismOrderVO: applyOrderInfo.tourismOrderVO || null, // 旅游的订单信息
      });
      if (applyOrderInfo.refundItemList && applyOrderInfo.refundItemList.length > 1) {
        this.queryRefundFee(applyOrderInfo.orderNo, applyOrderInfo.refundItemList); // 查询申请售后的退款金额
      }
    }

    this.getTemplateStyle(); // 获取主题模板配置
  }

  /**
   * 获取主题模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }

    this.setState({
      tmpStyle: templateStyle, // 主题模板配置
    });
  }

  /**
   * 查询申请售后的退款金额
   */
  queryRefundFee(orderNo, refundItemList) {
    const params = {
      orderNo: orderNo, // 订单编号
    };

    let applyItemCount = 0; // 申请售后的商品数量

    // 申请选中的商品列表
    refundItemList.forEach((item: any, index) => {
      params['requestItemDTOS[' + index + '].orderItemId'] = item.id;
      params['requestItemDTOS[' + index + '].itemNo'] = item.itemNo;
      params['requestItemDTOS[' + index + '].skuId'] = item.skuId;
      params['requestItemDTOS[' + index + '].itemCount'] = item.itemCount;
      applyItemCount += item.itemCount;
    });

    wxApi
      .request({
        url: api.order.queryRefundFee,
        loading: true,
        data: params,
      })
      .then((res) => {
        // 赋值后台获取到的退款金额
        this.setState({
          refundFee: res.data || null,
          applyItemCount: applyItemCount,
        });
      })
      .catch((error) => {
        if (error.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '退款金额查询失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.errorMessage,
          });
        }
      });
  }

  /**
   * 跳转申请退款页面
   */
  handleGoToApplyRefund = (refundType: number) => {
    const { status, refundOrderNo, orderNo, itemList, refundItemList, preSell, itemType, tourismOrderVO } = this.state;
    const params = {
      status,
      orderNo, // 申请售后的订单编号
      itemList, // 申请售后订单的商品列表
      refundItemList, // 选中的商品列表
      preSell, // 预售商品标志
      refundType, // 申请的售后类型，1：退货退款，2：仅退款
      refundOrderNo: refundOrderNo || null,
    };

    // 申请售后的商品类型，用于判断显示的申请售后参数
    if (itemType) {
      params.itemType = itemType;
    }
    // 判断是否有旅游的订单信息
    if (tourismOrderVO) {
      params.tourismOrderVO = tourismOrderVO;
    }
    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.applyRefund, 'applyRefundInfo', params);
    wxApi.$redirectTo({
      url: pageLinkEnum.orderPkg.afterSale.applyRefund,
    });
  };

  render() {
    const { preSell, refundItemList, refundFee, applyItemCount, showReturn } = this.state;
    return (
      <View data-scoped='wk-paa-ApplyType' className='wk-paa-ApplyType apply-type'>
        {!!refundItemList.length && (
          <View className='goods-box'>
            {refundItemList.length === 1 && (
              <Block>
                {refundItemList.map((item, idx) => {
                  return (
                    <View className='one-goods-box' key={idx}>
                      <View className='img-box'>
                        <Image src={item.thumbnail} className='img' mode='aspectFill'></Image>
                        {/*  预售标志  */}
                        {!!preSell && <View className='pre-sale-label'>预售</View>}
                      </View>
                      {/*  商品详情  */}
                      <View className='goods-info'>
                        <View className='name limit-line'>{item.itemName}</View>
                        {/*  商品规格  */}
                        {!!item.itemAttribute && <View className='attribute'>{'规格：' + item.itemAttribute}</View>}
                        {/*  商品数量及单价  */}
                        <View className='count'>
                          <Text className='num'>{'x' + item.itemCount}</Text>
                          <Text className='price'>
                            {
                              item.metadataJson && item.metadataJson?.marketActivityPrice
                                ? '单价:￥' + filters.moneyFilter(Number(item.metadataJson?.marketActivityPrice), true)
                                : '单价:￥' + filters.moneyFilter(item.salePrice, true)
                            }
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                })}
              </Block>
            )}

            {/*  多件商品申请售后显示模块  */}
            {refundItemList.length > 1 && (
              <View className='more-goods-box'>
                <View className='left'>
                  <ScrollView scrollX>
                    <View className='scroll-content'>
                      {refundItemList.map((item, index) => {
                        return (
                          <View className='img-box' key={index}>
                            <Image src={item.thumbnail} className='img'></Image>
                            {!!preSell && <View className='pre-sale-label'>预售</View>}
                          </View>
                        );
                      })}
                    </View>
                  </ScrollView>
                </View>
                <View className='right'>
                  <View className='right-content'>
                    <View className='refund-price'>{filters.moneyFilter(refundFee, true)}</View>
                    {!!applyItemCount && <View className='refund-item-count'>{'共' + applyItemCount + '件'}</View>}
                  </View>
                </View>
              </View>
            )}
          </View>
        )}

        {/*  退款类型  */}
        <View className='type'>
          <View className='tab' onClick={() => this.handleGoToApplyRefund(2)}>
            <View className='text-box'>
              <View className='title'>仅退款</View>
              <View className='desc'>未收到货(包含未签收)，或卖家协商同意前提下</View>
            </View>
            <View className='arrow'>
              <Image className='icon' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
            </View>
          </View>
          {!!showReturn && (
            <View className='tab' onClick={() => this.handleGoToApplyRefund(1)}>
              <View className='text-box'>
                <View className='title'>退货退款</View>
                <View className='desc'>已收到货，需要退换已收到的货物</View>
              </View>
              <View className='arrow'>
                <Image className='icon' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default ApplyTypePage as ComponentClass<PageOwnProps, PageState>;
