import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ComponentClass } from 'react';

import hoc from '@/hoc/index';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import stepFilters from '../step-filter.wxs.js';
import template from '../../../../../wxat-common/utils/template.js';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from "../../../../../wxat-common/api";
import pageLinkEnum from '../../../../../wxat-common/constants/pageLinkEnum.js';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl'
import './index.scss';

interface RefundItem {
  thumbnail: string;
  itemName: string;
  itemAttribute: string;
  itemCount: number;
  salePrice: number;
}

interface PageStateProps {}

interface PageDispatchProps {}

interface PageOwnProps {}

interface PageState {
  refundOrderNo: string; // 退款订单编号
  refundShippingCompany: string;
  refundShippingNo: string;
  refundDetail: {
    itemList: RefundItem[];
    status: number;
    refundType: number;
    preSell?: boolean;
    refundFee?: number;
    refundUserGiftCardFee?: number;
    requestTime?: string;
    refundOrderNo: string;
    refundConsignee: string;
    sellerPhone: string;
  };

  tmpStyle: {
    btnColor: string;
  };
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface RefundOrderPage {
  props: IProps;
}

@hoc
class RefundOrderPage extends React.Component<IProps, PageState> {
  state = {
    refundOrderNo: '', // 退款订单编号

    refundDetail: {
      itemList: [],
      status: 0,
      refundType: 0,
      preSell: false,
      refundFee: 0,
      refundUserGiftCardFee: 0,
      requestTime: '',
      refundOrderNo: '',
      refundConsignee: '',
      refundConsigneePhone: '',
      refundAddress: '',
      sellerPhone: '',
    },

    // 退款订单详情

    refundShippingCompany: '', // 物流公司
    refundShippingNo: '', // 快递单号

    tmpStyle: {
      btnColor: '',
      btnColor1: '',
    },

    // 主题模板配置
  };

  componentDidMount() {
    const refundOrderInfo = protectedMailBox.read('refundOrderInfo');
    if (refundOrderInfo) {
      this.setState(refundOrderInfo);
    }
    this.getTemplateStyle(); // 获取主题模板配置
  }

  componentDidShow() {
    const refundOrderInfo = protectedMailBox.read('refundOrderInfo');
    if (refundOrderInfo) {
      this.setState(refundOrderInfo);
    }

    this.getRefundDetail(); // 查询退款订单详情
  }

  // 获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }

    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
   * 查询退款订单详情
   */
  getRefundDetail() {
    wxApi
      .request({
        url: api.order.refundDetail,
        loading: true,
        data: {
          refundOrderNo: this.state.refundOrderNo, // 退款订单编号
        },
      })
      .then((res) => {
        this.setState({
          refundDetail: res.data || {},
        });
      })
      .catch((error) => {
        if (error.data.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '退款订单详情查询失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.data.errorMessage || '',
          });
        }
      });
  }

  /**
   * 跳转物流公司选择页面
   */
  handleShippingCompany = () => {
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.afterSale.shippingCompany,
    });
  };

  /**
   * 快递单号
   * @param {*} e
   */
  shippingNoInput = (e) => {
    const refundShippingNo = e.detail.value.trim();
    this.setState({
      refundShippingNo,
    });

    if (!/^[0-9A-Za-z]{0,19}$/.test(refundShippingNo)) {
      wxApi.showToast({
        icon: 'none',
        title: '快递单号由19个字符以内的字母、数字组成，请重新输入',
      });
    }
  };

  // 拨打电话
  handleCallSale = () => {
    wxApi.makePhoneCall({
      phoneNumber: this.state.refundDetail.sellerPhone,
    });
  };

  // 提交退货单物流信息
  handleCommitRefundOrder = () => {
    const params: {
      refundOrderNo: string;
      shippingCompany?: string;
      shippingNo?: string;
    } = {
      refundOrderNo: this.state.refundOrderNo, // 退款订单编号
      // 判断是否有选择物流公司
    };

    const refundShippingCompany = this.state.refundShippingCompany;
    if (!refundShippingCompany) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择物流公司',
      });

      return;
    } else {
      params.shippingCompany = refundShippingCompany; // 退货物流公司
    }

    // 判断快递单号输入是否正确
    const refundShippingNo = this.state.refundShippingNo;
    if (!refundShippingNo) {
      wxApi.showToast({
        icon: 'none',
        title: '请输入快递单号',
      });

      return;
    } else if (!/^[0-9A-Za-z]{0,19}$/.test(refundShippingNo)) {
      wxApi.showToast({
        icon: 'none',
        title: '快递单号由19个字符以内的字母、数字组成，请重新输入',
      });

      return;
    } else {
      params.shippingNo = refundShippingNo; // 退货物流单号
    }

    wxApi
      .request({
        url: api.order.refundFillLogistics,
        loading: true,
        data: params,
      })
      .then((res) => {
        const params = {
          refundOrderNo: this.state.refundOrderNo, // 退款订单编号
        };
        protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.refundDetail, 'refundDetailInfo', params);
        wxApi.$redirectTo({
          url: pageLinkEnum.orderPkg.afterSale.refundDetail,
        });
      })
      .catch((error) => {
        if (error.errorCode === 1001) {
          wxApi.showToast({
            icon: 'none',
            title: '提交退货单物流信息失败，请稍后重试',
          });
        } else {
          wxApi.showToast({
            icon: 'none',
            title: error.errorMessage,
          });
        }
      });
  };

  render() {
    const { refundDetail, tmpStyle, refundShippingCompany, refundShippingNo } = this.state;
    return (
      <View data-scoped='wk-par-RefundOrder' className='wk-par-RefundOrder refund-order'>
        <View className='steps'>
          <View className='main'>
            <View
              className='tab'
              style={_safe_style_(
                'color:' +
                  (stepFilters.requestRefund(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
              )}
            >
              <View
                className='step-logo'
                style={_safe_style_(
                  'background-color:' +
                    (stepFilters.requestRefund(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
                )}
              >
                <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
              </View>
              <View className='dotted-line'></View>
              <View className='step-text'>发起申请</View>
            </View>
            <View
              className='tab'
              style={_safe_style_(
                'color:' +
                  (stepFilters.agreeRefundRequest(refundDetail.status, refundDetail.refundType)
                    ? tmpStyle.btnColor
                    : '')
              )}
            >
              <View
                className='step-logo'
                style={_safe_style_(
                  'background-color:' +
                    (stepFilters.agreeRefundRequest(refundDetail.status, refundDetail.refundType)
                      ? tmpStyle.btnColor
                      : '')
                )}
              >
                <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
              </View>
              <View className='dotted-line'></View>
              <View className='step-text'>卖家同意</View>
            </View>
            <View
              className='tab'
              style={_safe_style_(
                'color:' +
                  (stepFilters.refundItemShipping(refundDetail.status, refundDetail.refundType)
                    ? tmpStyle.btnColor
                    : '')
              )}
            >
              <View
                className='step-logo'
                style={_safe_style_(
                  'background-color:' +
                    (stepFilters.refundItemShipping(refundDetail.status, refundDetail.refundType)
                      ? tmpStyle.btnColor
                      : '')
                )}
              >
                <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
              </View>
              <View className='dotted-line'></View>
              <View className='step-text'>快递退货</View>
            </View>
            <View
              className='tab'
              style={_safe_style_(
                'color:' +
                  (stepFilters.refundSuccess(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
              )}
            >
              <View
                className='step-logo'
                style={_safe_style_(
                  'background-color:' +
                    (stepFilters.refundSuccess(refundDetail.status, refundDetail.refundType) ? tmpStyle.btnColor : '')
                )}
              >
                <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/checked.png"></Image>
              </View>
              <View className='dotted-line'></View>
              <View className='step-text'>退款完成</View>
            </View>
          </View>
        </View>
        {/*  商品详情区域  */}
        {refundDetail.itemList.map((item: RefundItem, index) => {
          return (
            <View className='goods-box' key={index}>
              <View className='img-box'>
                <Image src={item.thumbnail} className='img'></Image>
                {!!refundDetail.preSell && <View className='pre-sale-label'>预售</View>}
              </View>
              {/*  商品信息详情  */}
              <View className='goods-info'>
                <View className='name limit-line'>{item.itemName || ''}</View>
                {!!item.itemAttribute && <View className='attribute'>{item.itemAttribute || ''}</View>}
                <View className='count'>
                  <Text className='num'>{item.itemCount + '件'}</Text>
                  <Text className='price'>{'单价:￥' + filters.moneyFilter(item.salePrice, true)}</Text>
                </View>
              </View>
            </View>
          );
        })}
        {/*  退款详情区域  */}
        <View className='refund-info'>
          <View className='tab'>
            <Text>退款类型：</Text>
            <Text>{refundDetail.refundType === 1 ? '退货退款' : '仅退款'}</Text>
          </View>
          <View className='tab'>
            <Text>退款金额：</Text>
            <Text>{'￥' + filters.moneyFilter(refundDetail.refundFee, true)}</Text>
          </View>
          {!!refundDetail.refundUserGiftCardFee && (
            <View className='tab'>
              <Text>礼品卡退款金额：</Text>
              <Text>{'￥' + filters.moneyFilter(refundDetail.refundUserGiftCardFee, true)}</Text>
            </View>
          )}

          {/*  <View class="tab">
                     <text>申请件数：</text>
                     <text>1件</text>
                     </View>  */}
          <View className='tab'>
            <Text>申请时间：</Text>
            <Text>{refundDetail.requestTime}</Text>
          </View>
          <View className='tab'>
            <Text>退款编号：</Text>
            <Text>{refundDetail.refundOrderNo}</Text>
          </View>
          {!!refundDetail.refundConsignee && (
            <View className='tab'>
              <Text>收件人：</Text>
              <Text>{refundDetail.refundConsignee}</Text>
            </View>
          )}

          {!!refundDetail.refundConsigneePhone && (
            <View className='tab'>
              <Text>收件号码：</Text>
              <Text>{refundDetail.refundConsigneePhone}</Text>
            </View>
          )}

          {!!refundDetail.refundAddress && (
            <View className='tab'>
              <Text>退货地址：</Text>
              <Text>{refundDetail.refundAddress}</Text>
            </View>
          )}
        </View>
        {/*  填写物流信息  */}
        <View className='logistics-box'>
          <View className='tab' onClick={this.handleShippingCompany}>
            <Text>物流公司：</Text>
            <Text className='logistics-detail'>{refundShippingCompany || '请选择物流公司'}</Text>
            <Image className='icon' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
          </View>
          <View className='tab'>
            <Text>快递单号：</Text>
            <Input
              className='logistics-detail'
              placeholder='请输入快递单号'
              value={refundShippingNo}
              onInput={this.shippingNoInput}
              maxlength={30}
            ></Input>
          </View>
        </View>
        {/*  底部按钮  */}
        <View className='btn-box'>
          <View
            className='btn-call'
            style={_safe_style_('background-color:' + tmpStyle.btnColor1+';color:'+tmpStyle.assistContentColor+';')}
            onClick={this.handleCallSale}
          >
            拨打电话
          </View>
          <View
            className='btn-commit'
            style={_safe_style_('background-color:' + tmpStyle.btnColor)}
            onClick={this.handleCommitRefundOrder}
          >
            提交
          </View>
        </View>
      </View>
    );
  }
}

export default RefundOrderPage as ComponentClass;
