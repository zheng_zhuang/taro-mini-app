import { $getRouter } from 'wk-taro-platform'; // @externalClassesConvered(AnimatDialog)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ComponentClass } from 'react';
import { connect } from 'react-redux';

import hoc from '@/hoc/index';
import { updateBaseAction } from '@/redux/base';
import filters from '../../../../wxat-common/utils/money.wxs.js';
import orderWxs from '../../../../wxat-common/utils/order.wxs.js';
import btnFilters from './btn-filter.wxs.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import api from "../../../../wxat-common/api";
import constants from "../../../../wxat-common/constants";
import pay from '../../../../wxat-common/utils/pay.js';
import deliveryWay from '../../../../wxat-common/constants/delivery-way.js';
import template from '../../../../wxat-common/utils/template.js';
import goodsTypeEnum from '../../../../wxat-common/constants/goodsTypeEnum.js';
import protectedMailBox from '../../../../wxat-common/utils/protectedMailBox.js';
import businessTime from '../../../../wxat-common/utils/businessTime.js';
import timer from '../../../../wxat-common/utils/timer.js';
import cdnResConfig from '../../../../wxat-common/constants/cdnResConfig.js';
import errorCodeFilter from '../../../../wxat-common/constants/error-code-filter.js';
import pageLinkEnum from '../../../../wxat-common/constants/pageLinkEnum.js';

import FrontMoneyState from "../../../../wxat-common/components/front-money-state";
import AnimatDialog from "../../../../wxat-common/components/animat-dialog";
import screen from '@/wxat-common/utils/screen';
import Express from './Express';
import SelfDelivery from './SelfDelivery';
import GetImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'
import './index.scss';
import subscribeMsg from "@/wxat-common/utils/subscribe-msg.js";
import subscribeEnum from "@/wxat-common/constants/subscribeEnum.js";
import WxOpenWeapp from '@/wxat-common/components/wx-open-launch-weapp'
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'
import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'
import store from '@/store';
const commonImg = cdnResConfig.common;

const CARD_TYPE = constants.card.type;
const GoodsType = constants.goods.itemType;
const PAY_CHANNEL = constants.order.payChannel;
const ORDER_STATUS = constants.order.orderStatus;
const freightType = constants.goods.freightType;

interface PageStateProps {
  industry: string;
  appInfo: Record<string, any>;
  iosSetting:any
}

interface PageDispatchProps {
  dispatchUpdateBase: (object) => void;
}

interface PageOwnProps {}

interface PageState {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface OrderDetailsPage {
  props: IProps;
}

const mapStateToProps = (state) => ({
  scoreName:state.globalData.scoreName || '积分',
  appInfo: state.base.appInfo,
  industry: state.globalData.industry, // 当前行业类型
  iosSetting:state.globalData.iosSetting,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateBase: (data) => dispatch(updateBaseAction(data)),
});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class OrderDetailsPage extends React.Component {
  $router = $getRouter();
  state = {
    deliveryWay, // 发货类型
    storeInfo: {}, // 门店信息

    orderNo: 0, // 订单编号
    orderDetail: {
      groupLeaderPromFee: '',
      groupPromFee: '',
      orderStatusDesc: '',
      expressType: '',
      orderStatus: null,
      itemType: null,
      itemList: [],
      preSell: null,

      expectShippingDate: '',
      frontMoneyItemOrder: null,
      payFee: 0,
      restMoney: 0,
      showItemAfterSaleBox: null,
      showItemAfterSaleBtn: null,
      orderNo: '',
      totalFee: '',
      userGiftCardPromFee: '',
      couponPromFee: '',
      cardPromFee: '',
      bargainPromFee: '',
      redPacketPromFee: '',
      memberPromFee: '',
      secKillPromFee: '',
      integralPromFee: '',
      showConfirmDeliveredBtn: null,
      showUpdateAddressBtn: null,
      showOrderAfterSaleBtn: null,
      showCancelBtn: null,
      showPayBtn: null,
      showAgreementDownloadBtn: null,
      payChannel: '',
      userMessage: '',
      showOperationBtnBox: '',
      innerBuyCouponAmount: '',
      innerBuyPromFee: '',
      logisticsFee: '',
      logisticsCouponPromFee: '',
      orderNotes: [] as any[],
      orderRoomTicketCodeDTOList: [],
      roomTicketAppointmentUrl: '',
      virtualItemType: '',
    },

    // 订单详情

    showHome: false, // 是否展示返回首页按钮

    ORDER_STATUS, // 订单状态常量
    GoodsType, // 商品类型常量

    intervalId: 0, // 行业id

    tmpStyle: {
      btnColor: '',
      btnColor1: '',
    },

    // 主题模板

    timeCardBg: GetImgUrl.goods.cardMinCount_png, // 次数卡背景图片
    chargeCardBg: GetImgUrl.goods.rechargeCard_png, // 充值卡背景图片

    coupon: CARD_TYPE.coupon, // 商品类型为优惠券

    partialRefund: 1, // 是否允许整单商品中单商品退款，0：不允许，1：允许。默认为1

    bgGroupFullImg: cdnResConfig.group.bgGroupFullImg, // 参团人员已满对话弹框背景图片

    demands: {
      refuseReason: '',
    },

    // 用药人信息
    showRefuseReason: false, // 显示处方药拒绝理由
    flowStatus: null, // 处方药状态： 0：待开方 1：医院审核完成，待人工审核 2：处方订单完成 3：医院开方拒绝 4: 商家拒绝
  };

  _thredId = 0;

  refGroupFullDialogCMPT: Taro.RefObject<any> = React.createRef();

  componentDidMount() {
    const { orderNo, showHome } = this.$router.params;

    this.setState(
      {
        orderNo,
        showHome: !!showHome,
      },

      () => {
        this.getOrderDetail();
      }
    );

    this.getTemplateStyle();

    // 定时器格式化时间
    this._thredId = timer.addQueue(() => {
      this.updateCounterRefundTime(); // 格式化允许申请退款剩余时间，只有产品类型，且确认收货或已自提后才有退款剩余时间
    });
  }

  iosPayLimitDialog = React.createRef<any>()
  componentDidShow() {
    if (this.state.orderNo) {
      this.getOrderDetail(); // 获取订单详情
    }
  }

  componentDidHide() {
    clearTimeout(this.state.intervalId);
  }

  componentWillUnmount() {
    // 清除周期性检测订单定时器
    clearTimeout(this.state.intervalId);
    // 判断定时器是否存在，删除定时器
    if (this._thredId) {
      timer.deleteQueue(this._thredId);
      this._thredId = 0;
    }
  }

  // 虚拟商品 - ios 获取 url
  fetchIosVirtualPayUrl=(orderNo)=>{
    wxApi.request({
      url: api.order.payOrder,
      method: 'GET',
      loading: true,
      data: {
        orderNo: orderNo,
        payChannel: 1,// 微信支付
        payTradeType:4
      }
    }).then((res) => {
      // console.log('fetchIosVirtualPayUrl', orderNo, res)
      if(res.success && res.data ){
        const {payUrl = ''}= res.data
        if(payUrl){
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/payH5-submit-success/index',
            data:{
              H5PayUrl: encodeURIComponent(payUrl),
            }
          })
        }else {
          wxApi.showToast({
            title: `支付链接获取失败`
          })
        }
      }else{
        wxApi.showToast({
          title: res.errorMessage
        })
      }
    })
  }

  /**
   * 周期性检测订单状态，核销员核销之后需要检测订单完成状态
   */
  intervalGetOrderDetail() {
    this.state.intervalId = window.setTimeout(() => {
      // this.state.intervalId = setInterval(() => {
      this.getOrderDetail(false);
    }, 2000);
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 计算订单总价格
  getTotalGoodsPrice() {
    if (this.state.orderDetail) {
      return this.state.orderDetail.itemList[0].itemCount * this.state.orderDetail.itemList[0].salePrice;
    }
    return '';
  }

  /**
   * 获取订单详情
   * @param {*} showLoading
   */

  getOrderDetail = (showLoading = true) => {
    wxApi
      .request({
        url: api.order.detail,
        loading: showLoading,
        data: {
          orderNo: this.state.orderNo,
        },
      })
      .then((res) => {
        const orderDetail = res.data || {};
        this.getStoreInfo(orderDetail); // 获取门店地址

        let partialRefund = 1; // 是否允许整单商品中单商品退款，0：不允许，1：允许。默认为1
        if (orderDetail && (orderDetail.partialRefund || orderDetail.partialRefund === 0)) {
          partialRefund = orderDetail.partialRefund;
        }
        this.setState({
          partialRefund,
        });

        // 计算已优惠价格
        orderDetail.itemList.forEach((item) => {
          item.reducePrice =
            item.salePrice * item.itemCount - item.payFee ? item.salePrice * item.itemCount - item.payFee : null;
        });

        // 待医院审核改成：处方审核中
        if (orderDetail.orderStatus === 92) {
          orderDetail.orderStatusDesc = '处方审核中';
        }
        // 待人工审核改成：卖家确认中
        if (orderDetail.orderStatus === 93) {
          orderDetail.orderStatusDesc = '卖家确认中';
        }

        // 更新内存中-品牌信息appInfo中的partialRefund
        // state.base.appInfo.partialRefund = partialRefund;
        this.props.dispatchUpdateBase({
          appInfo: {
            ...this.props.appInfo,
            partialRefund,
          },
        });

        // 调配订单操作按钮(该方法放在这里执行，一是为了先赋值新的partialRefund，再进行“调配订单操作按钮”的显示，二是为了调配完成之后赋值新的orderDetail)
        this.assembleOrderList(orderDetail, partialRefund);
        this.setState({
          orderDetail: orderDetail,
        });
      })
      .finally(() => {
        const orderDetail = this.state.orderDetail;
        if (orderDetail) {
          if (
            orderDetail.itemType === GoodsType.product && // 产品类型
            (orderDetail.orderStatus === ORDER_STATUS.paid || // 已支付
              orderDetail.orderStatus === ORDER_STATUS.confirm || // 商家已确认
              orderDetail.orderStatus === ORDER_STATUS.shipping || // 已发货
              orderDetail.orderStatus === ORDER_STATUS.refund)
          ) {
            // 退款中
            const url = wxApi.$getCurrentPageRoute(); // 当前页面url

            // 判断当前页面是订单详情页面才周期性检测订单状态，避免页面操作太快而出现在其他页面也加载当前接口
            if (url === pageLinkEnum.orderPkg.orderDetail) {
              this.intervalGetOrderDetail(); // 周期性检测订单状态，核销员核销之后需要检测订单完成状态
            }
          }
        }
      });
  };

  /**
   * 调配订单操作按钮
   * @param {*} orderDetail
   * @param partialRefund
   */
  assembleOrderList = (orderDetail, partialRefund = this.state.partialRefund) => {
    if (orderDetail) {
      // 显示商品维度申请售后按钮操作区域
      Object.assign(orderDetail, {
        showItemAfterSaleBox:
          (orderDetail.itemType === goodsTypeEnum.PRODUCT.value || // 产品类型
            orderDetail.itemType === goodsTypeEnum.ROOMS.value || // 客房类型
            orderDetail.itemType === goodsTypeEnum.SERVER.value || // 服务类型
            !this.state.isEstateType) && // 楼盘类型
          orderDetail.orderStatus !== ORDER_STATUS.waitPay && // 待支付
          !orderDetail.frontMoneyItemOrder && // 定金订单不展示售后信息
          orderDetail.orderStatus !== ORDER_STATUS.distribution, // 骑手配送中不可申请退款
      });
      // 显示商品维度申请售后按钮
      Object.assign(orderDetail, {
        showItemAfterSaleBtn:
          partialRefund && // 是否允许整单商品中单商品退款，0：不允许，1：允许。默认为1
          (!orderDetail.counterRefundTime || orderDetail.counterRefundTime > 0) && // 允许申请退款剩余时间
          orderDetail.itemType !== CARD_TYPE.cardPack && // 卡包类型不展示
          orderDetail.itemType !== goodsTypeEnum.GIFT_GOODS.value && // 赠品订单不允许退款
          (orderDetail.itemType !== goodsTypeEnum.ROOMS.value || // 客房类型
            (orderDetail.orderStatus !== ORDER_STATUS.success && // 已完成
              orderDetail.orderStatus !== ORDER_STATUS.checkin)) // 已完成的客房不支持退款
      });


      // 显示修改收货地址按钮
      Object.assign(orderDetail, {
        showUpdateAddressBtn:
          orderDetail.itemType === goodsTypeEnum.PRODUCT.value && // 产品类型
          orderDetail.expressType == deliveryWay.EXPRESS.value && // 到店自提
          (orderDetail.orderStatus === ORDER_STATUS.waitPay || // 待支付
            orderDetail.orderStatus === ORDER_STATUS.paid || // 已支付
            orderDetail.orderStatus === ORDER_STATUS.confirm), // 商家已确认
      });
      // 显示底部订单维度申请售后按钮
      Object.assign(orderDetail, {
        showOrderAfterSaleBtn:
          (orderDetail.itemType === goodsTypeEnum.PRODUCT.value || // 产品类型
            orderDetail.itemType === goodsTypeEnum.ROOMS.value || // 客房类型
            orderDetail.itemType === goodsTypeEnum.SERVER.value || // 服务类型
            !this.state.isEstateType) && // 楼盘类型
          orderDetail.itemType !== CARD_TYPE.cardPack && // 卡包类型不展示
          orderDetail.orderStatus !== ORDER_STATUS.waitPay && // 待支付
          orderDetail.orderStatus !== ORDER_STATUS.refund && // 退款中
          orderDetail.orderStatus !== ORDER_STATUS.refundSuccess && // 退款成功
          orderDetail.orderStatus !== ORDER_STATUS.refundReject && // 退款已拒绝
          orderDetail.orderStatus !== ORDER_STATUS.cancel && // 订单已取消
          (!orderDetail.counterRefundTime ||
            orderDetail.counterRefundTime > 0) && // 允许申请退款剩余时间
          orderDetail.itemType !== goodsTypeEnum.GIFT_GOODS.value && // 赠品订单不允许退款
          (orderDetail.itemType !== goodsTypeEnum.ROOMS.value ||
            (orderDetail.orderStatus !== ORDER_STATUS.success &&
              orderDetail.orderStatus !== ORDER_STATUS.checkin)) && // 已使用的客房不支持退款
          !orderDetail.frontMoneyItemOrder && // 定金订单不允许退款
          orderDetail.orderStatus !== ORDER_STATUS.distribution // 骑手配送中不可申请退款
      });
      // 显示取消订单按钮
      Object.assign(orderDetail, {
        showCancelBtn: orderDetail.orderStatus === ORDER_STATUS.waitPay, // 待支付
      });
      // 显示付款按钮
      Object.assign(orderDetail, {
        showPayBtn: orderDetail.orderStatus === ORDER_STATUS.waitPay, // 待支付
      });
      // 显示确认收货按钮
      Object.assign(orderDetail, {
        showConfirmDeliveredBtn:
          orderDetail.itemType === goodsTypeEnum.PRODUCT.value && orderDetail.orderStatus === ORDER_STATUS.shipping, // 产品类型 // 已发货
      });
      // 显示订单维度按钮操作区域
      Object.assign(orderDetail, {
        showOperationBtnBox: this.state.showHome || // 是否展示返回首页按钮
          orderDetail.showAgreementDownloadBtn || // 显示订单维度合同下载按钮
          orderDetail.showUpdateAddressBtn || // 显示修改收货地址按钮
          orderDetail.showOrderAfterSaleBtn || // 显示底部订单维度申请售后按钮
          orderDetail.showCancelBtn || // 显示取消订单按钮
          orderDetail.showPayBtn || // 显示付款按钮
          orderDetail.showConfirmDeliveredBtn // 显示确认收货按钮
      });
    }
  };

  /**
   * 格式化允许申请退款剩余时间，只有产品类型，且确认收货或已自提后才有退款剩余时间
   */
  updateCounterRefundTime() {
    const orderDetail = this.state.orderDetail;
    if (orderDetail) {
      if (!orderDetail.counterRefundTime || orderDetail.counterRefundTime <= 0) {
        // 判断定时器是否存在，删除定时器
        if (this._thredId) {
          timer.deleteQueue(this._thredId);
          this._thredId = 0;
        }
        return;
      } else {
        orderDetail.counterRefundTime -= 1;
      }

      this.assembleOrderList(orderDetail); // 调配订单操作按钮
      this.setState({
        orderDetail,
      });


    }
  }

  /**
   * 获取门店地址
   * @param {*} orderDetail
   */
  getStoreInfo(orderDetail) {
    const time = businessTime.computed(
      orderDetail.businessStartHour,
      orderDetail.businessEndHour,
      orderDetail.businessDayOfWeek
    );

    const storeInfo = {
      address: orderDetail.storeAddress,
      name: orderDetail.abbreviation || orderDetail.storeName,
      tel: orderDetail.storeTel,
      businessTime: time,
      latitude: orderDetail.latitude,
      longitude: orderDetail.longitude,
    };

    this.setState({
      storeInfo,
    });
  }

  /**
   * 复制文字
   * @param {*} e
   */
  copyText = (text) => {
    wxApi.setClipboardData({
      data: text,
      success: function (res) {
        wxApi.getClipboardData({
          success: function (res) {
            wxApi.showToast({
              // image: require('@/images/mine/success.png'),
              title: '复制成功',
            });
          },
        });
      },
    });
  };

  /**
   * 返回首页
   */
  gotoHome = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  /**
   * 点击修改订单收货地址
   * @param e
   */
  handleUpdateAddress = () => {
    protectedMailBox.send('wxat-common/pages/address/update/index', 'orderNo', this.state.orderNo);
    wxApi.$navigateTo({
      url: '/wxat-common/pages/address/update/index',
    });
  };

  /**
   * 取消订单
   * @param {*} e
   */
  handleCancelOrders = () => {
    wxApi.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.cancel,
              loading: {
                title: '正在取消订单',
              },

              data: {
                orderNo: this.state.orderNo,
              },
            })
            .then((res) => {
              return this.getOrderDetail();
            })
            .then((res) => {
              const orderDetail = res.data;
              this.assembleOrderList(orderDetail); // 调配订单操作按钮
              this.setState({
                orderDetail: orderDetail,
              });
            })
            .catch((error) => {});
        }
      },
    });
  };

  /**
   * 点击付款
   * @param e
   */
  handlePay = (itemType) => {
    const that = this;
    const orderNo = this.state.orderNo;
    const _iosSetting = this.props.iosSetting
    if(_iosSetting?.payType && itemType == goodsTypeEnum.VIRTUAL_GOODS.value){
      if(_iosSetting.payType  === 2){
        this.fetchIosVirtualPayUrl(orderNo)
        return
      }else if(_iosSetting.payType  === 3){
        this.iosPayLimitDialog.current.showIospayLimitDialog();
        return
      }
    }
    // let itemType = e.currentTarget.dataset.itemType;
    if (itemType == CARD_TYPE.charge || itemType === CARD_TYPE.gift) {
      pay.payByOrder(orderNo, PAY_CHANNEL.WX_PAY, {
        wxPaySuccess() {
          that.getOrderDetail();
        },
        fail(res) {
          if (res.data.errorCode === errorCodeFilter.groupFullCode.value) {
            // 打开参团人数已满提示对话弹框
            that.handleGroupFullDialog();
          } else if (res.data.errorCode === errorCodeFilter.seckillRemainLackCode.value) {
            // 秒杀活动商品剩余数量不足提示
            wxApi.showToast({
              title: '活动商品剩余数量不足',
              icon: 'none',
            });

            // 定时返回上一页面
            const timeOut = setTimeout(() => {
              if (timeOut) {
                clearTimeout(timeOut);
              }
              // 返回上一页面
              wxApi.navigateBack({
                delta: 1,
              });
            }, 1500);

          } else {
            // 返回上一页面
            wxApi.navigateBack({
              delta: 1,
            });
          }
        },
      });
    } else {
      let payChannel = '';
      if (itemType == CARD_TYPE.cardPack || itemType === CARD_TYPE.equityCard) payChannel = PAY_CHANNEL.WX_PAY;
      subscribeMsg.sendMessage([subscribeEnum.ORDER_SEND.value]).then(() =>{
        pay.payByOrder(orderNo, payChannel, {
          success(res) {
            that.getOrderDetail();
          },
          fail(res) {
            console.log(res);
          },
          wxPaySuccess() {
            that.getOrderDetail();
          },
        });
      })

    }
  };

  /**
   * 打开参团人数已满提示对话弹框
   */
  handleGroupFullDialog() {
    this.refGroupFullDialogCMPT.current &&
      this.refGroupFullDialogCMPT.current.show({
        scale: 1,
      });
  }

  /**
   * 关闭参团人数已满提示对话弹框，返回上一页面
   */
  handleCloseGroupFullDialog = () => {
    this.refGroupFullDialogCMPT.current && this.refGroupFullDialogCMPT.current.hide();

    // 返回上一页面
    wxApi.navigateBack({
      delta: 1,
    });
  };

  /**
   * 点击发起拼团，跳转拼团列表页面
   */
  handleGoToGroupList = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/group/list/index',
    });
  };

  /**
   * 确认收货
   * @param {*} e
   */
  handleConfirmDelivery = () => {
    wxApi.showModal({
      title: '请确定已收到货物',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.confirmDelivery,
              loading: {
                title: '正在通知商家',
              },

              data: {
                orderNo: this.state.orderNo,
              },
            })
            .then((res) => {
              return this.getOrderDetail();
            })
            .then((res) => {
              const orderDetail = res.data;
              this.assembleOrderList(orderDetail); // 调配订单操作按钮
              this.setState({
                orderDetail: orderDetail,
              });
            })
            .catch((error) => {});
        }
      },
    });
  };

  /**
   * 订单维度申请售后，跳转申请售后页面
   */
  handleOrderApplyAfterSale = () => {
    const { orderDetail } = this.state;
    // 申请售后订单的商品列表
    const itemList = [];
    orderDetail.itemList.forEach((item) => {
      if (!item.orderItemStatus || item.orderItemStatus === -50) {
        // 去除该订单中已经申请过售后的商品
        itemList.push(item);
      }
    });

    const params = {
      itemType: orderDetail.itemType, // 申请售后的商品类型
      orderNo: orderDetail.orderNo, // 申请售后的订单编号
      itemList: itemList, // 申请售后订单的商品列表
      refundItemList: JSON.parse(JSON.stringify(itemList)), // 选中的商品列表，这里进行一个深拷贝，是为了在申请退款页面操作的时候指向不同的对象
      preSell: orderDetail.preSell, // 预售商品标志
      showReturn:
        orderDetail.orderStatus !== ORDER_STATUS.confirm &&
        orderDetail.orderStatus !== ORDER_STATUS.taking &&
        orderDetail.itemType === GoodsType.product, // 显示退款退货
    };
    // 判断是否有旅游的订单信息
    if (orderDetail.tourismOrderVO) {
      params.tourismOrderVO = orderDetail.tourismOrderVO;
    }

    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.applyType, 'applyOrderInfo', params);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.afterSale.applyType,
    });
  };

  /**
   * 去使用
   */
  handleToUse() {
    const { orderDetail } = this.state;
    if (orderDetail.orderRoomTicketCodeDTOList && orderDetail.orderRoomTicketCodeDTOList.length > 0) { // 客房券
      let h5Link = 'https://kfqtest.ctlife.tv/pages/bookProduct/index'
      if (process.env.ENVIRONMENT === 'release') { // 线上域名
        h5Link = 'https://kfq.ctlife.tv/pages/bookProduct/index'
      }
      const { ext } = store.getState();
      // window.location.href = `${h5Link}?order_no=${this.state.orderNo}&app_id=${ext.appId}`
      wxApi.$locationTo(`${h5Link}?order_no=${this.state.orderNo}&app_id=${ext.appId}`)
    } else {
      Taro.navigateToMiniProgram({
        appId: 'wx2560348cdcdd57fe',
        // appId: 'wxed6c99dc0cd41de7',
        path: 'pages/menber/menber1'
      })
    }
  }

  getShowDelivery () {
    const {orderDetail} = this.state
    const isBook = orderDetail.roomTicketAppointmentUrl
    const unpaid = orderDetail.orderStatus === 10
    if (isBook && unpaid) {
      return false
    }
    return true
  }

  /**
   * 商品维度申请售后，跳转申请售后页面
   * @param status
   * @param refundOrderNo
   * @param itemIndex
   */
  handleItemApplyAfterSale = (status, refundOrderNo, itemIndex) => {
    const { orderDetail } = this.state;
    // 申请售后订单的商品列表
    const itemList = [];
    orderDetail.itemList.forEach((item) => {
      if (!item.orderItemStatus) {
        // 去除该订单中已经申请过售后的商品
        itemList.push(item);
      }
    });

    // 选中的商品列表
    const refundItemList = [];
    refundItemList.push(orderDetail.itemList[itemIndex]);

    // 撤销退款的订单状态重置
    // const status = e.currentTarget.dataset.status;
    // const refundOrderNo = e.currentTarget.dataset.refundOrderNo;

    const params = {
      itemType: orderDetail.itemType, // 申请售后的商品类型
      orderNo: orderDetail.orderNo, // 申请售后的订单编号
      itemList: itemList, // 申请售后订单的商品列表
      refundItemList: refundItemList, // 选中的商品列表
      preSell: orderDetail.preSell, // 预售商品标志
      status: status,
      refundOrderNo: refundOrderNo,
      showReturn:
        orderDetail.orderStatus !== ORDER_STATUS.confirm &&
        orderDetail.orderStatus !== ORDER_STATUS.taking &&
        orderDetail.itemType === GoodsType.product, // 显示退款退货
    };
    // 判断是否有旅游的订单信息
    if (this.state.orderDetail.tourismOrderVO) {
      params.tourismOrderVO = orderDetail.tourismOrderVO;
    }

    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.applyType, 'applyOrderInfo', params);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.afterSale.applyType,
    });
  };

  /**
   * 跳转填写退货单页面
   * @param {*} e
   */
  handleGoToFillInRefundOrder = (itemIndex) => {
    // 选中的商品列表
    const refundOrderNo = this.state.orderDetail.itemList[itemIndex].refundOrderNo;
    const params = {
      refundOrderNo: refundOrderNo, // 申请售后的退款订单编号
    };
    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.refundOrder, 'refundOrderInfo', params);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.afterSale.refundOrder,
    });
  };

  /**
   * 跳转售后详情页面
   * @param {*} e
   */
  handleGoToAfterSaleDetail = (itemIndex) => {
    const refundOrderNo = this.state.orderDetail.itemList[itemIndex].refundOrderNo;
    const params = {
      refundOrderNo: refundOrderNo, // 申请售后的退款订单编号
    };
    protectedMailBox.send(pageLinkEnum.orderPkg.afterSale.refundDetail, 'refundDetailInfo', params);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.afterSale.refundDetail,
    });
  };

  /**
   * 跳转订单物流信息查询页面
   * @param {*} e
   */
  handleGoToOrderLogistics(e) {
    const orderNo = e.currentTarget.dataset.orderNo; // 订单编号
    protectedMailBox.send(
      "wxat-common/pages/logistics/order-logistics/index",
      "orderNo",
      orderNo
    );
    wxApi.$navigateTo({
      url: "/wxat-common/pages/logistics/order-logistics/index"
    });
  }

  handleDownloadDeal() {
    wxApi.downloadFile({
      url: api.agreement.download + '?orderNo=' + this.data.orderDetail.orderNo
    }).then(res => {
      // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
      if (res.statusCode === 200) {
        const tempFilePath = res.tempFilePath;
        // const filePath = wx.env.USER_DATA_PATH + '/合同.pdf';
        // 保存文件
        Taro.saveFile({
          tempFilePath,
          // filePath,
          success: function (res) {
            const savedFilePath = res.savedFilePath;
            console.log('saveFilePath', savedFilePath);
            // 打开文件
            Taro.openDocument({
              filePath: savedFilePath,
              success: function (res) {

              },
              fail: function (error) {
                Taro.showToast({
                  title: '协议生成中',
                  icon: 'none'
                })
              }
            });
          },
          fail: function (err) {
            console.log('保存失败：', err)
            Taro.showToast({
              title: '协议生成中',
              icon: 'none'
            })
          }
        })
      } else {
        Taro.showToast({
          title: '协议生成中',
          icon: 'none'
        })
      }
    }).catch(res => {
      Taro.showToast({
        title: '协议生成中',
        icon: 'none'
      })
    })
  }

  /**
   * 跳转订单用药人信息详情页面
   * @param {*} e
   */
  goDrugDemand(){
    wxApi.$navigateTo({
      url: 'sub-packages/order-package/pages/prescription-drugs/demand/index',
      data:{
        orderNo: this.state.orderNo
      }
    });
  }

  /**
   * 重新下单,跳转到下单页
   */
  handleReOrder = () => {
    const skuList = this.state.orderDetail.itemList.map((item: any) => ({
      ...item,
      itemCount: item.itemCount,
      itemNo: item.itemNo,
      barcode: item.barcode,
      name: item.itemName,
      skuId: item.skuId || null,
      salePrice: item.salePrice,
      pic: item.thumbnail,
      skuTreeNames: item.skuInfo,
      freight: item.freight,
      noNeedPay: item.noNeedPay,
      reportExt: item.reportExt,
    }));

    protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', skuList);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.payOrder,
    });
  };

  goGoodsDetail = (itemNo: string) => {
    if (this.state.orderDetail.itemType === 41) return
    const {
      orderDetail: { itemType },
    } = this.state;
    if(itemType === 41) return
    const isCardPack = itemType === goodsTypeEnum.CARDPACK.value;
      const isEquityCard = itemType === goodsTypeEnum.EQUITY_CARD.value;
      const url = isCardPack
        ? '/sub-packages/marketing-package/pages/card-pack/detail/index'
        // : isEquityCard
        // ? '/sub-packages/marketing-package/pages/equity-card/detail/index'
        : '/wxat-common/pages/goods-detail/index';

    wxApi.$navigateTo({
      url,
      data: {
        itemNo,
      },
    });
  };

  virtualItemType = (itemList) => {
    for(let i = 0;i <= itemList.length;i++){
      if(itemList[i].metadataJson?.virtualItemType == 1){
        return true
      }
    }
    return false;
  }

  returnToGoods = (e) => {
    // 阻止冒泡, 用于占位
    e.stopPropagation();
  };

  render() {
    const {
      storeInfo,
      orderDetail,
      tmpStyle,
      GoodsType,
      deliveryWay,
      ORDER_STATUS,
      chargeCardBg,
      coupon,
      timeCardBg,
      showHome,
      demands,
      bgGroupFullImg,
      showRefuseReason,
    } = this.state;
    const { scoreName,iosSetting } = this.props;
    const transPx = document.documentElement.clientWidth / (Taro.config.designWidth || 750);
    const btnStyles = `.btn {
      min-width: ${100 * transPx}px;
      height: ${50 * transPx}px;
      padding: 0px ${25 * transPx}px;
      border-radius: ${30 * transPx}px;
      text-align: center;
      line-height: ${50 * transPx}px;
      color: #a2a2a4;
      border: 1px solid #ccc;
      font-size: ${24 * transPx}px;
      display: inline-block;
      margin-left: ${20 * transPx}px;
    }`;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-opo-OrderDetails'
        className='wk-opo-OrderDetails'
      >
        {/*  到店自提店铺及核销码模板  */}
        {/* 已支付、已确认、退款中、拒绝退款、发货中这些状态展示核销码 */}
        {/*  快递发货地址信息模板  */}
        {/*  客房订单信息及核销码模板  */}
        {/* 已支付、已确认、退款中、拒绝退款这些状态展示核销码 */}
        {/*  楼盘订单信息及核销码模板  */}
        {/*  已支付、已确认、退款中、拒绝退款这些状态展示核销码 */}
        {/*  订单状态  */}
        {!!orderDetail.orderNo && (
          <View>
            <View className='order-top-info-box'>
              <View className='order-status-desc'>
                {!!(!!orderDetail.groupLeaderPromFee || !!orderDetail.groupPromFee) && <Text>拼团订单，</Text>}

                {
                  !orderDetail.estateOrderStatusDesc &&!showRefuseReason && <Text>{orderDetail && orderDetail.orderStatusDesc ? orderDetail.orderStatusDesc : '-'}</Text>
                }

                {!!showRefuseReason && (
                  <Text>{demands.refuseReason ? '退款已完成-' + demands.refuseReason : '退款已完成'}</Text>
                )}

                {!!(
                  orderDetail.itemType === GoodsType.product &&
                  orderDetail.expressType == deliveryWay.SELF_DELIVERY.value &&
                  orderDetail.orderStatus === ORDER_STATUS.success
                ) && <Text>，已核销取货</Text>}
              </View>
              <View className='item'>
                <View className='label'>订单编号：</View>
                <View className='text'>
                  <View>{orderDetail.orderNo}</View>
                  <View className='copy' onClick={() => this.copyText(orderDetail.orderNo)}>
                    复制
                  </View>
                </View>
              </View>
              <View className='item'>
                <View className='label'>下单时间：</View>
                <View className='text'>{orderDetail.orderTime}</View>
              </View>
            </View>
          </View>
        )}

        {/*  订单详情  */}
        {!!orderDetail.orderNo && (
          <View className='detail-container'>
            {
              !orderDetail.orderRoomTicketCodeDTOList && this.getShowDelivery()  && orderDetail.itemType !== 41 && <Block>
                {
                  orderDetail.expressType == deliveryWay.SELF_DELIVERY.value ? (
                    <SelfDelivery storeInfo={storeInfo} orderDetail={orderDetail}></SelfDelivery>
                  ) : (
                    <Express orderDetail={orderDetail} storeInfo={storeInfo}></Express>
                  )
                }
              </Block>
            }
            <View className='item-box'>
              <View className='order-note-box'>
                <View className='label'>备注：</View>
                <View className='text'>
                  <View>{orderDetail.userMessage || ''}</View>
                  <View className='notes-box'>
                    {(orderDetail.orderNotes || []).map((note: any) => {
                      return (
                        <View key={note.id}>
                          {note.noteKey}: {note.noteValue || '--'}
                        </View>
                      );
                    })}
                  </View>
                </View>
              </View>
            </View>
            {/*  快递发货地址信息模板  */}
            {/*  订单商品详情  */}
            <View className='item-box'>
              <View className='item-box-top'>
                <View className='t1'>商品信息</View>
                <View className='t2'>(共{ orderDetail.itemList.reduce((sum, e) => sum + Number(e.itemCount || 0), 0)}件)</View>
              </View>
              {(orderDetail.itemList || []).map((item: any, index) => {
                return (
                  <View className='item-tab' key={index} onClick={() => this.goGoodsDetail(item.itemNo)}>
                    <View className='item-icon-box'>
                      {item.thumbnail ? (
                        <Image
                          className='item-icon'
                          src={
                            orderDetail.itemType === goodsTypeEnum.EQUITY_CARD.value
                              ? (item.thumbnail || '').split(',')[0]
                              : item.thumbnail
                          }
                          mode='aspectFill'
                        ></Image>
                      ) : (
                        <Image
                          className='item-icon'
                          src={orderDetail.itemType === 4 ? chargeCardBg : item.itemType === coupon ? timeCardBg : ''}
                          mode='aspectFill'
                        ></Image>
                      )}

                      {!!orderDetail.preSell && <View className='pre-sale-label'>预售</View>}
                    </View>
                    {/*  商品详情  */}
                    <View className='item-info'>
                      <View>
                        {/* 商品名称 */}
                        <View className='item-name-box'>
                          <View className='item-name limit-line'>{item.itemName || ''}</View>
                          <View className='item-price'>
                            <Text className='symbol'>￥</Text>
                            {filters.moneyFilter(item.payFee, true)}
                          </View>
                        </View>
                        {/*  商品规格  */}
                        <View className='item-attribute-box'>
                          <View className='item-attribute limit-line'>
                            {!!item.itemAttribute && (
                              <View>{item.itemAttribute || ''}</View>
                            )}
                          </View>
                          <View className='item-count'>{'x ' + item.itemCount}</View>
                        </View>
                      </View>

                      <View>
                      {/*{*/}
                      {/*  (orderDetail.itemType !== 43) &&  (<View className='item-count-price'>*/}
                      {/*    {!!(item.reducePrice && !orderDetail.frontMoneyItemOrder) && (*/}
                      {/*      <View className='item-price-sale'>*/}
                      {/*        已优惠：{item.reducePrice >= 0 ? '-' : ''}￥*/}
                      {/*        {filters.moneyFilter(item.reducePrice, true, true)}*/}
                      {/*      </View>*/}
                      {/*    )}*/}

                      {/*    <View className='item-price'>*/}
                      {/*      <Text className='symbol'>￥</Text>*/}
                      {/*      {filters.moneyFilter(item.payFee, true)}*/}
                      {/*    </View>*/}
                      {/*  </View>)*/}
                      {/*}*/}
                       {/* 取件码 */}
                        <View style='display: flex;'>
                          {
                            (!!item.robotFetchCode) && (<View className="pickUpCode" >
                              取件码：<text>{item.robotFetchCode}</text>
                            </View>)
                          }
                          {!!orderDetail.frontMoneyItemOrder && (
                            <View style={_safe_style_('color: ' + tmpStyle.btnColor + ';')} className='front-money'>
                              {'￥' + filters.moneyFilter(orderDetail.payFee, true) + '（定金）'}
                            </View>
                          )}
                        </View>
                      </View>
                    </View>
                    {/*  预计发货时间  */}
                    {!!(orderDetail.preSell && orderDetail.expectShippingDate) && (
                      <View className='expect-shipping-info'>
                        <View>预计发货时间：</View>
                        <View className='date'>{orderDetail.expectShippingDate}</View>
                      </View>
                    )}

                    {/*  售后流程操作按钮区域  */}
                    {!!orderDetail.showItemAfterSaleBox && orderDetail.itemType !==48 && (
                      <View className='after-sale-container' onClick={this.returnToGoods}>
                        {/*{!!item.orderItemStatusDesc && (*/}
                        {/*  <View className='after-sale-status'>{item.orderItemStatusDesc}</View>*/}
                        {/*)}*/}

                        {!!orderDetail.showItemAfterSaleBtn && btnFilters.applyAfterSale(item.orderItemStatus) && orderDetail.itemType !== 42 && orderDetail.itemType !== 43 && (
                          <View
                            className='after-sale-btn'
                            onClick={() =>
                              this.handleItemApplyAfterSale(item.orderItemStatus, item.refundOrderNo, index)
                            }
                          >
                            申请售后
                          </View>
                        )}

                        {btnFilters.fillInRefundOrder(item.orderItemStatus, item.refundType) && (
                          <View className='after-sale-btn' onClick={() => this.handleGoToFillInRefundOrder(index)}>
                            填写退货单
                          </View>
                        )}

                        {btnFilters.afterSaleDetail(item.orderItemStatus) && (
                          <View className='after-sale-btn' onClick={() => this.handleGoToAfterSaleDetail(index)}>
                            售后详情
                          </View>
                        )}

                        {btnFilters.refundSuccess(item.orderItemStatus) && (
                          <View className='after-sale-btn' onClick={() => this.handleGoToAfterSaleDetail(index)}>
                            退款成功
                          </View>
                        )}
                      </View>
                    )}
                  </View>
                );
              })}
            </View>
            {/* 定金状态 */}
            {!!orderDetail.frontMoneyItemOrder && (
              <FrontMoneyState frontMoney={orderDetail.payFee} restMoney={orderDetail.restMoney}></FrontMoneyState>
            )}

            {/*  订单支付价格信息  */}
            <View className='info-box'>
              {/*<View className='info-title'>订单信息</View>*/}
              {/*<View className='info-tab'>*/}
              {/*  <Text>{'订单号：' + orderDetail.orderNo}</Text>*/}
              {/*  <Text className='copy' onClick={() => this.copyText(orderDetail.orderNo)}>*/}
              {/*    复制*/}
              {/*  </Text>*/}
              {/*</View>*/}

              <View className='info-tab'>
                <Text>原价：</Text>
                <Text className='goods-price-light'>{'￥' + filters.moneyFilter(orderDetail.totalFee, true)}</Text>
              </View>

              {!!orderDetail.userGiftCardPromFee && (
                <View className='info-tab'>
                  <Text>礼品卡：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.userGiftCardPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.couponPromFee && (
                <View className='info-tab'>
                  <Text>优惠劵：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.couponPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.cardPromFee && (
                <View className='info-tab'>
                  <Text>次数卡：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.cardPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.groupLeaderPromFee && (
                <View className='info-tab'>
                  <Text>团长优惠：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.groupLeaderPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.groupPromFee && (
                <View className='info-tab'>
                  <Text>拼团优惠：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.groupPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.bargainPromFee && (
                <View className='info-tab'>
                  <Text>砍价优惠：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.bargainPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.redPacketPromFee && (
                <View className='info-tab'>
                  <Text>红包：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.redPacketPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.memberPromFee && (
                <View className='info-tab'>
                  <Text>{'会员折扣：-￥' + filters.moneyFilter(orderDetail.memberPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.secKillPromFee && (
                <View className='info-tab'>
                  <Text>秒杀优惠：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.secKillPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.integralPromFee && (
                <View className='info-tab'>
                  <Text>{scoreName} 抵扣：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.integralPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.innerBuyCouponAmount && (
                <View className='info-tab'>
                  <Text>消耗内购券：</Text>
                  <Text>{orderDetail.innerBuyCouponAmount + '张'}</Text>
                </View>
              )}

              {!!orderDetail.innerBuyPromFee && (
                <View className='info-tab'>
                  <Text>内购券：</Text>
                  <Text>{'-￥' + filters.moneyFilter(orderDetail.innerBuyPromFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.fullDecrementPromFee && (
                <View className='info-tab'>
                  <Text>满减：</Text>
                  <Text className='goods-label'>
                    -￥
                    {filters.moneyFilter(orderDetail.fullDecrementPromFee, true)}
                  </Text>
                </View>
              )}

              {!!(orderDetail.expressType != deliveryWay.SELF_DELIVERY.value) && (
                <View className='info-tab'>
                  <Text>运费：</Text>
                  {orderDetail.logisticsFee ? (
                    <Text className='goods-price-light'>
                      {'￥' + filters.moneyFilter(orderDetail.logisticsFee, true)}
                    </Text>
                  ) : (
                    <Text>
                      {orderWxs.getFreightLabel(
                        orderDetail.itemList,
                        orderDetail.expressType,
                        deliveryWay,
                        freightType,
                        '包邮'
                      )}
                    </Text>
                  )}
                </View>
              )}

              {!!(orderDetail.expressType != deliveryWay.SELF_DELIVERY.value && orderDetail.logisticsCouponPromFee) && (
                <View className='info-tab'>
                  <Text>运费券：</Text>
                  <Text className='goods-price-light'>
                    {'￥-' + filters.moneyFilter(orderDetail.logisticsCouponPromFee, true)}
                  </Text>
                </View>
              )}

              {orderDetail.orderStatus === ORDER_STATUS.waitPay ? (
                <View className='info-tab'>
                  <Text>应付：</Text>
                  <Text className='goods-price-light'>{'￥' + filters.moneyFilter(orderDetail.payFee, true)}</Text>
                </View>
              ) : (
                <View className='info-tab'>
                  <Text>实付：</Text>
                  <Text className='goods-price-light'>{'￥' + filters.moneyFilter(orderDetail.payFee, true)}</Text>
                </View>
              )}

              {!!orderDetail.frontMoneyItemOrder && (
                <View className='info-tab'>
                  <Text>尾款：</Text>
                  <Text>{'￥' + filters.moneyFilter(orderDetail.restMoney, true)}</Text>
                </View>
              )}

              <View className='info-tab'>
                <Text>付款方式：</Text>
                <Text>{orderDetail.payChannel}</Text>
              </View>
            </View>
            {/*  客房券信息  */}
            {
              orderDetail.orderRoomTicketCodeDTOList && orderDetail.orderRoomTicketCodeDTOList.length > 0 && (
                orderDetail.orderRoomTicketCodeDTOList.map(item => {
                  return (
                    <View className='info-box' key={item.code}>
                      <View className='info-tab'>
                        <Text>{'预售券号：' + item.code}</Text>
                        <Text className='copy' onClick={() => this.copyText(item.code)}>
                          复制
                        </Text>
                      </View>
                      <View className='info-tab'>
                        <Text>适用酒店：</Text>
                        {
                          item.storeHotelList && item.storeHotelList.length > 0 && (
                            item.storeHotelList.map(s => {
                              return <Text className='hotel-item' key={s.name}>{s.name}</Text>
                            })
                          )
                        }
                      </View>
                      <View className='info-tab'>
                        <Text>适用时间：</Text>
                        <Text>{filters.dateFormat(item.usageDatetimeBegin, 'yyyy-MM-dd') + " ~ " + filters.dateFormat(item.usageDatetimeEnd, 'yyyy-MM-dd')}</Text>
                      </View>
                    </View>
                  )
                })
              )
            }
            {/*  底部按钮操作区域  */}
            {!!orderDetail.showOperationBtnBox && orderDetail.itemType !==48 && (
              <View className={`operation-box ${screen.isFullScreenPhone ? 'fix-full-screen' : ''}`}>
                {!!showHome && (
                  <View className='btn-home' onClick={this.gotoHome}>
                    <Image className='home-icon' src={getStaticImgUrl.tabbar.homeOff_png}></Image>
                    <View className='home-text'>首页</View>
                  </View>
                )}

                {!showHome && orderDetail.itemType === 37 && (
                  <View style={{ flex: 1, color: '#6d6d6d', paddingLeft: Taro.pxTransform(10) }}>
                    如需售后，请联系门店
                  </View>
                )}

                {/*  订单操作按钮  */}
                {!!orderDetail.showUpdateAddressBtn && (
                  <View className='btn' onClick={this.handleUpdateAddress}>
                    修改收货地址
                  </View>
                )}

                {!!orderDetail.showOrderAfterSaleBtn && orderDetail.itemType !== 42 && orderDetail.itemType !== 43 && orderDetail.itemType !==48 && (
                  <View className='btn' onClick={this.handleOrderApplyAfterSale}>
                    申请售后
                  </View>
                )}



                {!!orderDetail.showOrderAfterSaleBtn && orderDetail.itemType === 41 && Taro.getEnv() !== Taro.ENV_TYPE.WEB && !this.virtualItemType(orderDetail.itemList) && (
                  <View className='btn' onClick={() => {this.handleToUse()}}>
                    去使用
                  </View>
                )}

                {!!orderDetail.showOrderAfterSaleBtn && orderDetail.itemType === 41 && Taro.getEnv() === Taro.ENV_TYPE.WEB && !this.virtualItemType(orderDetail.itemList) &&(
                  orderDetail.orderRoomTicketCodeDTOList && orderDetail.orderRoomTicketCodeDTOList.length > 0 ? (
                    <View className='btn' onClick={() => {this.handleToUse()}}>
                      去使用
                    </View>
                  ) : <WxOpenWeapp
                  username="gh_73c309d26824"
                  path="pages/menber/menber1"
                  styles={btnStyles}
                  slotTel={
                    <View className='btn'>
                    去使用
                    </View>}>
                </WxOpenWeapp>
                )}

                {!!orderDetail.showCancelBtn && (
                  <View className='btn' onClick={this.handleCancelOrders}>
                    取消订单
                  </View>
                )}

                {!!orderDetail.showPayBtn && (
                    <View
                    className='btn btn-light'
                    style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor)}
                    onClick={() => this.handlePay(orderDetail.itemType)}
                  >
                    {
                      iosSetting?.payType && orderDetail.itemType == goodsTypeEnum.VIRTUAL_GOODS.value && iosSetting?.buttonCopywriting ? iosSetting?.buttonCopywriting : '付款'
                    }
                  </View>
                )}

                {!!orderDetail.showConfirmDeliveredBtn && (
                  <View
                    className='btn btn-light'
                    style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor)}
                    onClick={this.handleConfirmDelivery}
                  >
                    确认收货
                  </View>
                )}

                {orderDetail.orderStatus === ORDER_STATUS.refundSuccess && (
                  <View
                    className='btn btn-light'
                    style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor)}
                    onClick={this.handleReOrder}
                  >
                    重新提交
                  </View>
                )}
              </View>
            )}
          </View>
        )}

        {/*  参团人数已满提示对话弹框  */}
        <AnimatDialog id='group-full-dialog' ref={this.refGroupFullDialogCMPT} animClass='group-full-dialog'>
          <View className='group-full'>
            <Image className='bg-group-full' src={bgGroupFullImg}></Image>
            <View className='group-full-content'>
              <View className='group-full-title'>参团人数已满</View>
              <View className='group-full-text'>快去发起自己的拼团吧</View>
            </View>
            <View className='btn-group-full' onClick={this.handleGoToGroupList}>
              发起拼团
            </View>
          </View>
          <Image
            className='icon-group-full-close'
            src={commonImg.close}
            onClick={this.handleCloseGroupFullDialog}
          ></Image>
        </AnimatDialog>
        <IosPayLimitDialog ref={this.iosPayLimitDialog} tipsCopywriting={iosSetting?.tipsCopywriting}></IosPayLimitDialog>
      </View>
    );
  }
}

export default OrderDetailsPage as ComponentClass<PageOwnProps, PageState>;
