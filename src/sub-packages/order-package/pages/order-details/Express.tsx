import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import wxApi from '@/wxat-common/utils/wxApi';
import React, { ComponentClass } from 'react';

import './Express.scss';

interface ExpressProps {
  storeInfo: object;
  orderDetail: object;
}

type IProps = ExpressProps;
interface Express {
  props: IProps;
}

class Express extends React.Component {
  static defaultProps: {
    storeInfo: {};
    orderDetail: {};
  };

  /**
   * 跳转订单物流信息查询页面
   * @param {*} e
   */
  handleGoToOrderLogistics = (e) => {
    const orderNo = e.currentTarget.dataset.orderNo; // 订单编号
    protectedMailBox.send('wxat-common/pages/logistics/order-logistics/index', 'orderNo', orderNo);
    wxApi.$navigateTo({
      url: '/wxat-common/pages/logistics/order-logistics/index',
    });
  };
  /**
   * 打开店铺地图
   */
  clickLocation = () => {
    const { storeInfo } = this.props as Record<string, any>;
    wxApi.openLocation({
      latitude: parseFloat(storeInfo.latitude),
      longitude: parseFloat(storeInfo.longitude),
      name: storeInfo.name,
      address: storeInfo.address,
      success: function (res) {},
    });
  };
  render() {
    const { orderDetail } = this.props as Record<string, any>;
    return (
      <Block>
        {!!(!!orderDetail && orderDetail.showConfirmDelivered) && (
          <View className='order-logistics-box'>
            {orderDetail.shippingType === 3 ? (
              <View className='tab'>
                <View className='logistics-info'>
                  <View className='title'>物流信息：</View>
                  <View className='text noNeedText'>无需物流</View>
                </View>
              </View>
            ) : (
              <View
                className='tab'
                onClick={_fixme_with_dataset_(this.handleGoToOrderLogistics, {
                  orderNo: orderDetail ? orderDetail.payOrderNo : '',
                })}
              >
                <View className='logistics-info'>
                  <View className='title'>物流信息</View>
                  <View className='text'>{'当前已' + (orderDetail.countLogistics || 1) + '个包裹寄出'}</View>
                </View>
                <Image className='arrow-right' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
              </View>
            )}

            {/*  有物流信息的  */}
          </View>
        )}

        {/*  收货信息  */}
        {!!orderDetail.detailAddress && (
          <View className='order-address-box'>
            <View className='address-concat-box' onClick={this.clickLocation}>
              <Text>{orderDetail.consignee || '-'}</Text>
              <Text className='concat-phone'>{orderDetail.consigneeMobile}</Text>
            </View>
            <View className='address-detail limit-line'>{orderDetail.detailAddress || '-'}</View>
          </View>
        )}
      </Block>
    );
  }

  static options = {
    addGlobalClass: true,
  };
}

export default Express as ComponentClass<ExpressProps, any>;
