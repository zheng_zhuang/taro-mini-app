// 按钮显示状态过滤器
export default {
  /**
   * 申请售后
   * @param status 退款状态。无退款状态则表示未申请售后;已撤销退款申请(status === -50);允许显示申请售后按钮
   * @param refundType 申请的售后类型，1：退货退款，2：仅退款
   */
  applyAfterSale: function (status, refundType) {
    if (!status || status === -50) {
      return true;
    }
    return false;
  },

  /**
   * 填写退货单
   * @param status 退款状态
   * @param refundType 申请的售后类型，1：退货退款，2：仅退款
   */
  fillInRefundOrder: function (status, refundType) {
    if (status === -2 && refundType === 1) {
      // 已同意售后申请
      return true;
    }
    return false;
  },

  /**
   * 售后详情
   * @param status 退款状态
   * @param refundType 申请的售后类型，1：退货退款，2：仅退款
   */
  afterSaleDetail: function (status, refundType) {
    if (
      status === -1 || // 申请退款
      status === -3 || // 退款商品已发货
      status === -4 || // 已确认退货
      status === -5 || // 已确认退款
      status === -6 || // 已拒绝退款
      status === -10 || // 退款中
      status === -30 // 退款失败
    ) {
      return true;
    }
    return false;
  },

  /**
   * 退款成功
   * @param status 退款状态
   * @param refundType 申请的售后类型，1：退货退款，2：仅退款
   */
  refundSuccess: function (status, refundType) {
    if (status === -20) {
      // 退款完成
      return true;
    }
    return false;
  },
};
