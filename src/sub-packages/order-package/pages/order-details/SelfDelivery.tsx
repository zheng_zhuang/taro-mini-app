// @scoped
import wxApi from '@/wxat-common/utils/wxApi';
import '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ComponentClass } from 'react';
import QrCode from '@/wxat-common/components/qr-code';
import qrCodeEnum from '@/wxat-common/constants/qrCodeEnum';
import constants from '@/wxat-common/constants/index.js';
import groupEnum from '@/wxat-common/constants/groupEnum';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import './SelfDelivery.scss';

const decorateImg = cdnResConfig.decorate;

const CARD_TYPE = constants.card.type;
const ORDER_STATUS = constants.order.orderStatus;

interface SelfDeliveryProps {
  storeInfo: object;
  orderDetail: object;
}

interface SelfDeliveryState {
  showQrCode: boolean;
}

type IProps = SelfDeliveryProps & SelfDeliveryState;

interface SelfDelivery {
  props: IProps;
}

class SelfDelivery extends React.Component {
  static defaultProps = {
    storeInfo: {},
    orderDetail: {},
  };

  state = {
    showQrCode: false,
    codeView: 'barcode',
    isEquityCard: false,
  };

  componentDidMount() {
    this.setState({
      showQrCode: this.isShowQrCode(),
      isEquityCard: this.judgeEquityCard(),
    });
  }

  /**
   * 核销码
   */
  isShowQrCode() {
    const { orderDetail } = this.props;
    if (!orderDetail) {
      return false;
    } else {
      const orderSuccess =
        orderDetail.orderStatus === ORDER_STATUS.paid /* 已支付 */ ||
        orderDetail.orderStatus === ORDER_STATUS.confirm /* 商家确认 */ ||
        orderDetail.orderStatus === ORDER_STATUS.frontMoneyPaid /* 定金已支付，也是待核销订单 */ ||
        (orderDetail.orderStatus === ORDER_STATUS.refundReject && !orderDetail.completeTime) /* 退款拒绝 */ ||
        orderDetail.orderStatus === ORDER_STATUS.shipping; /* 发货中 */
      if (orderDetail.groupNo) {
        /* 拼团订单必须拼团成功才能展示核销码 */
        const groupSuccess = orderDetail.groupStatus === groupEnum.STATUS.SUCCESS.value;
        return orderSuccess && groupSuccess;
      } else if (orderDetail.itemType === CARD_TYPE.cardPack) {
        // 代金卡包不展示核销码
        return false;
      } else if (orderDetail.itemType === CARD_TYPE.equityCard) {
        // 权益卡：待核销状态38 根据userCardId获取二维码
        return orderDetail.orderStatus === ORDER_STATUS.frontMoneyPaid;
      } else {
        return orderSuccess;
      }
    }
  }

  judgeEquityCard() {
    const { orderDetail } = this.props;
    if (!orderDetail) {
      return false;
    } else {
      return orderDetail.itemType === CARD_TYPE.equityCard;
    }
  }

  /**
   * 打开店铺地图
   */
  clickLocation = () => {
    const { storeInfo } = this.props;
    wxApi.openLocation({
      latitude: parseFloat(storeInfo.latitude),
      longitude: parseFloat(storeInfo.longitude),
      name: storeInfo.name,
      address: storeInfo.address,
      success: function (res) {},
    });
  };

  handleSwitchCode = () => {
    const { codeView } = this.state;
    this.setState({ codeView: codeView === 'qrcode' ? 'barcode' : 'qrcode' });
  };

  /**
   * 拨打电话
   */
  clickPhone = () => {
    if (this.props.storeInfo.tel) {
      wxApi.makePhoneCall({
        phoneNumber: this.props.storeInfo.tel,
      });
    } else {
      console.error('电话号格式错误：', this.props.storeInfo.tel)
    }
  };

  render() {
    const { storeInfo, orderDetail } = this.props;
    const { showQrCode, codeView, isEquityCard } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-sopo-SelfDelivery'
        className='wk-sopo-SelfDelivery'
      >
        <View className='self-delivery-box'>
          <View className='store-name'>{storeInfo.name}</View>
          {/*<View className='self-delivery-tab'>*/}
          {/*  <Text className='left'>自提门店：</Text>*/}
          {/*  <Text className='right'>{storeInfo.name}</Text>*/}
          {/*</View>*/}
          <View className='self-delivery-tab'>
            <View className='left business-time'>
              <Text>营业时间：</Text>
              <Text className='time'>{storeInfo.businessTime}</Text>
            </View>
            {/*<View className='right navgation'>*/}
            {/*  <Image className='right-icon' onClick={this.clickLocation} src={decorateImg.location}></Image>*/}
            {/*  <Image className='right-icon' onClick={this.clickPhone} src={decorateImg.phone}></Image>*/}
            {/*</View>*/}
          </View>
          <View className='self-delivery-tab' onClick={this.clickLocation}>
            <Text className='left'>门店地址：</Text>
            <Text className='right address'>{storeInfo.address}</Text>
          </View>
          {storeInfo.tel && (
            <View className='self-delivery-tab'>
              <View className='phone-box'>
                <View className='number' onClick={this.clickPhone}>{storeInfo.tel}</View>
              </View>
            </View>
          )}
        </View>
        {/*  核销码 订购订单不展示核销码 后续可能需要。先保留  */}
        {!!showQrCode && (
          <View className='qr-code-box'>
            <QrCode
              showQrcode={codeView === 'qrcode'}
              showBarcode={codeView === 'barcode'}
              isScale
              showTip={false}
              verificationNo={isEquityCard ? orderDetail.userCardId : orderDetail.orderNo}
              verificationType={isEquityCard ? qrCodeEnum.card : qrCodeEnum.order}
            ></QrCode>
            <View className='switch' onClick={this.handleSwitchCode}>
              <Image src={cdnResConfig.coupon.switch} className='switch-icon' />
              切换{codeView === 'qrcode' ? '条形码' : '微信扫一扫'}核销
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default SelfDelivery as ComponentClass<SelfDeliveryProps, SelfDeliveryState>;
