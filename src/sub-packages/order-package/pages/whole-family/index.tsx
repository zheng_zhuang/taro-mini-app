import React from 'react'; // 整家订单列表
import Taro from '@tarojs/taro';
import { View, Text, ScrollView } from '@tarojs/components';

import { _safe_style_ } from '@/wxat-common/utils/platform';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import template from '@/wxat-common/utils/template';
import orderEnum from '@/wxat-common/constants/orderEnum';
import constant from '@/wxat-common/constants';
import './index.scss';

class WholeFamily extends React.Component {
  state = {
    topTabs: [
      {
        id: 1,
        name: 'all',
        text: '全部订单',
      },

      {
        id: 3,
        name: 'uncompleted',
        text: '未完成',
      },

      {
        id: 2,
        name: 'completed',
        text: '已完成',
      },
    ],

    currentTab: 'all',
    orderList: [],
    pageNo: 1,
    count: 0, // 数据量
    tmpStyle: {},
    isError: false,
    loadMoreStatus: constant.order.loadMoreStatus.HIDE,
  };

  componentDidMount() {
    this.getTemplateStyle();
    this.getOrderList();
  }

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  tabClick(currentTab) {
    this.setState({ currentTab, orderList: [], pageNo: 1 }, () => {
      let params: any = {};
      if (currentTab === 'uncompleted') {
        params.orderStatusList = '0,1,2,3,4,5';
      } else if (currentTab === 'completed') {
        params.orderStatus = 6;
      }
      this.getOrderList(params);
    });
  }

  getOrderList(params = {}) {
    this.setState({ isError: false });
    params = { ...params, pageNo: this.state.pageNo, pageSize: 3 };
    wxApi
      .request({
        url: api.order.wholeFamilyList,
        loading: true,
        method: 'GET',
        data: params,
      })
      .then((res) => {
        if (res.success) {
          this.setState((preState) => ({
            orderList: preState.orderList.concat(res.data),
            count: res.totalCount,
            loadMoreStatus: constant.order.loadMoreStatus.HIDE,
          }));
        }
      })
      .catch(() => {
        if (this.state.orderList.length === 0) this.setState({ isError: true });
        if (this.state.pageNo > 1) this.setState({ loadMoreStatus: constant.order.loadMoreStatus.ERROR });
      });
  }

  toOrderDetail(orderNo) {
    wxApi.$navigateTo({ url: `sub-packages/order-package/pages/whole-family/detail/index?orderNo=${orderNo}` });
  }

  scrollToLower() {
    if (this.state.orderList.length === this.state.count) return;
    this.setState({
      loadMoreStatus: constant.order.loadMoreStatus.LOADING,
    });

    this.setState(
      (preState) => ({
        pageNo: ++preState.pageNo,
      }),

      () => {
        this.getOrderList();
      }
    );
  }

  onRetryLoadMore() {
    this.setState({
      loadMoreStatus: constant.order.loadMoreStatus.LOADING,
    });

    this.getOrderList();
  }

  render() {
    const { topTabs, currentTab, orderList, tmpStyle, isError, loadMoreStatus } = this.state;
    const isEmpty = orderList && orderList.length === 0;
    return (
      <View data-scoped='wk-whole-family' className='wk-whole-family'>
        <View className='top-tabs'>
          {topTabs.map((item) => {
            return (
              <View key={item.id} onClick={this.tabClick.bind(this, item.name)} className='top-tabs-item'>
                <Text
                  style={_safe_style_(item.name === currentTab ? `color: ${tmpStyle.btnColor}; font-weight: 600;` : '')}
                >
                  {item.text}
                </Text>
                {item.name === currentTab && (
                  <View
                    style={_safe_style_(item.name === currentTab ? `background-color: ${tmpStyle.btnColor}` : '')}
                    className='underline'
                  ></View>
                )}
              </View>
            );
          })}
        </View>
        <ScrollView scrollY onScrollToLower={this.scrollToLower} className='scroll-box'>
          <View className='order-list'>
            {orderList.map((item: any) => {
              return (
                <View key={item.id} onClick={this.toOrderDetail.bind(this, item.orderNo)} className='order-item'>
                  <View className='num-status'>
                    <Text className='num'>主订单号：{item.orderNo}</Text>
                    {item.orderStatus === orderEnum.wholeFamily.status.COMPLETED.value ? (
                      <Text className='completed'>已完成</Text>
                    ) : (
                      <Text className='uncompleted'>{orderEnum.wholeFamily.simpleMapping[item.orderStatus]}</Text>
                    )}
                  </View>
                  <View className='label-value'>
                    <View className='row'>
                      <Text>下单客户：</Text>
                      <Text className='value'>{item.consignee}</Text>
                    </View>
                    <View className='row'>
                      <Text>销售总额：</Text>
                      <Text className='value'>¥{item.totalFee && item.totalFee / 100}</Text>
                    </View>
                    <View className='row'>
                      <Text>尾款：</Text>
                      <Text className='value special-color'>¥{item.arrearsFee && item.arrearsFee / 100}</Text>
                    </View>
                    <View className='row'>
                      <Text>商品数量：</Text>
                      <Text className='value'>{item.itemCount}</Text>
                    </View>
                    <View className='row'>
                      <Text>下单时间：</Text>
                      <Text className='value'>{item.orderTime}</Text>
                    </View>
                    <View className='row'>
                      <Text>期望发货时间：</Text>
                      <Text className='value'>{item.deliveryDate}</Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
          {isError && <Error />}
          {!isError && isEmpty && <Empty message='暂无订单' />}
          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore} />
        </ScrollView>
      </View>
    );
  }
}

export default WholeFamily;
