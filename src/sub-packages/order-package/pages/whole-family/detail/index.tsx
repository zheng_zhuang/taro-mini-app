import {$getRouter} from 'wk-taro-platform';
import React from 'react'; // 整家订单详情
import Taro from '@tarojs/taro';
import {Image, Text, View} from '@tarojs/components';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
// import Error from '@/wxat-common/components/error/error';
import template from '@/wxat-common/utils/template';
import orderEnum from '@/wxat-common/constants/orderEnum';
import './index.scss';

// import homeFurnishing from '@/images/ming-zhu/home-furnishing.png';

class WholeFamilyDetail extends React.Component {
  $router = $getRouter();
  state = {
    detailInfo: {},
    goodsList: [],
    appId: Taro.getAccountInfoSync().miniProgram.appId,
  };

  componentDidMount() {
    this.getTemplateStyle();
    this.getWholeFamilyDetail(this.$router.params.orderNo);
  }
  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  };

  getWholeFamilyDetail(orderNo) {
    const params = { orderNo };
    wxApi
      .request({
        url: api.order.wholeFamilyDetail,
        loading: true,
        method: 'GET',
        data: params,
      })
      .then((res) => {
        if (res.success) {
          this.setState({
            detailInfo: res.data,
          });

          const ary = res.data.orderItemList || res.data.childrenItemList;
          if (ary && ary.length) {
            this.setState({
              goodsList: this.deepData(ary),
            });
          }
        }
      })
      .catch(() => {});
  }

  deepData(ary) {
    const res = [];
    ary.map((v) => {
      res.push(v);
      const ary = v.orderItemList || v.childrenItemList;
      if (ary && ary.length) {
        res.push(...this.deepData(ary));
      }
    });
    return res;
  }

  copyText = (text) => {
    wxApi.setClipboardData({
      data: text,
      success: function () {
        wxApi.getClipboardData({
          success: function () {
            wxApi.showToast({
              image: "https://bj.bcebos.com/htrip-mp/static/app/images/mine/success.png",
              title: '复制成功',
            });
          },
        });
      },
    });
  };

  toOtherPage() {
    const url = 'https://mp.weixin.qq.com/s/DPYDWMj5gz9g8qfV26N9kA';
    wxApi.$navigateTo({ url: `wxat-common/pages/webview/index?url=${url}` });
  }

  render() {
    const {
      detailInfo,
      appId,
      goodsList,
    }: { detailInfo: any; appId: number | string; goodsList: Array<any> } = this.state;
    const mingzhu_appId = 'wx7fa22295877c0537';
    return (
      <View data-scoped='wk-whole-family-detail' className='wk-whole-family-detail'>
        <View className='top-Block'></View>
        <View className='order-detail'>
          <View className='recipient-info'>
            <View className='row'>
              <Text>收货人：{detailInfo.consignee || '-'}</Text>
            </View>
            <View className='row'>
              <Text>手机号：{detailInfo.consigneeMobile || '-'}</Text>
            </View>
            <View className='row'>
              <Text>收货地址：{detailInfo.address || '-'}</Text>
            </View>
            <View className='row'>
              <Text>期望发货时间：{detailInfo.deliveryDate || '-'}</Text>
            </View>
          </View>
          {goodsList && goodsList.length && (
            <View className='line-numbers-list'>
              <View className='child-order-item'>
                {goodsList.map((item) => {
                  return (
                    <View key={item.id} className='line-numbers-item'>
                      <View className='num-status'>
                        <Text className='num'>行号：{item.lineItemNo || '-'}</Text>
                        <Text className='status'>{orderEnum.wholeFamily.simpleMapping[item.logisticsStatus]}</Text>
                      </View>
                      <View className='img-goods'>
                        <Image className='img' src={item.thumbnail} />
                        <View className='goods'>
                          <View className='goods-name'>{item.itemName || '-'}</View>
                          <View className='goods-count'>{(item.itemCount && `x${item.itemCount}`) || '-'}</View>
                          <View className='goods-sku'>{item.skuInfo || '-'}</View>
                          <View className='goods-sap'>SAP物料号：{item.barcode || '-'}</View>
                        </View>
                      </View>
                    </View>
                  );
                })}
              </View>
            </View>
          )}

          <View className='summary'>
            <View className='row'>
              主订单号：{detailInfo.orderNo || '-'}
              <Text onClick={this.copyText.bind(this, this.state.detailInfo.orderNo)} className='copy'>
                复制
              </Text>
            </View>
            <View className='row'>
              销售总额：<Text className='money'>￥{(detailInfo.totalFee && detailInfo.totalFee / 100) || '-'}</Text>
            </View>
            <View className='row'>
              尾款：<Text className='money'>￥{(detailInfo.arrearsFee && detailInfo.arrearsFee / 100) || '-'}</Text>
            </View>
            <View className='row'>下单时间：{detailInfo.orderTime || '-'}</View>
            <View className='row'>所属门店：{detailInfo.storeName || '-'}</View>
          </View>
          {/* 明珠使用，私有化后删除 */}
          {appId === mingzhu_appId && (
            <View onClick={this.toOtherPage} className='img-link'>
              <Image src="https://htrip-static.ctlife.tv/wk/home-furnishing.png" />
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default WholeFamilyDetail;
