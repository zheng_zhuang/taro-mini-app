import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import wxApi from '../../../../wxat-common/utils/wxApi';
import api from "../../../../wxat-common/api";
import cdnResConfig from '../../../../wxat-common/constants/cdnResConfig';
import protectedMailBox from '../../../../wxat-common/utils/protectedMailBox';
import PayGift from "../../../../wxat-common/components/pay-gift";
import './index.scss';
import { connect } from 'react-redux';
import constants from '../../../../wxat-common/constants';

const ORDER_STATUS = constants.order.orderStatus;
const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
  tabbars: state.globalData.tabbars,
});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class PaySuccess extends React.Component {
  state = {
    payType: null, // 支付类型
    money: null, // 支付金额
    orderNo: null, // 订单号
    infoTplName: null, // 订单信息类型
  };

  /**
   * 生命周期函数--监听页面加载
   * **/
  componentDidMount() {
    const paySuccessInfo = protectedMailBox.read('paySuccessInfo') || null;
    this.setState({
      payType: paySuccessInfo.payType || null, // 支付类型
      money: paySuccessInfo.money || null, // 支付金额
      orderNo: paySuccessInfo.orderNo || null, // 订单号
      infoTplName: paySuccessInfo.infoTplName || null, // 订单信息类型
    });
  }

  /**
   * 回到首页
   */
  goHome = () => {
    const { tabbars } = this.props;
    let url = '/wxat-common/pages/home/index';
    if (tabbars && tabbars.list) {
      const tabbarList = tabbars.list;
      url = tabbarList[0].pagePath;
    }
    0;
    wxApi.$navigateTo({
      url: url,
    });
  };

  /**
   * 查看订单
   */
  handleOrder = () => {
    const { infoTplName, orderNo } = this.state;
    // 跳转到不同的订单列表
    let url = `/wxat-common/pages/order-list/index`;
    const index = 2;
    if (infoTplName === 'serve-tpl') {
      url = `/sub-packages/server-package/pages/appointment/list/index`;
    } else {
      url += '?orderNo=' + orderNo + '&showHome=1';
    }
    wxApi.$navigateTo({
      url,
      data: {
        index,
        tabType: ORDER_STATUS.labelList[0].value,
      },
    });
  };

  /**
   * 查看协议
   */
  handleAgreement = () => {
    const { orderNo } = this.state;
    wxApi
      .$downloadFile({
        url: api.agreement.download + '?orderNo=' + orderNo,
      })
      .then((res) => {
        // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
        if (res.statusCode === 200) {
          const tempFilePath = res.tempFilePath;
          // const filePath = wx.env.USER_DATA_PATH + '/合同.pdf';
          // 保存文件
          wxApi.saveFile({
            tempFilePath,
            // filePath,
            success: function (res) {
              const savedFilePath = res.savedFilePath;
              console.log('saveFilePath', savedFilePath);
              // 打开文件
              wxApi.openDocument({
                filePath: savedFilePath,
                success: function (res) {},
                fail: function (error) {
                  wxApi.showToast({
                    title: '协议生成中',
                    icon: 'none',
                  });
                },
              });
            },
            fail: function (err) {
              console.log('保存失败：', err);
              wxApi.showToast({
                title: '协议生成中',
                icon: 'none',
              });
            },
          });
        } else {
          wxApi.showToast({
            title: '协议生成中',
            icon: 'none',
          });
        }
      })
      .catch((error) => {
        wxApi.showToast({
          title: '协议生成中',
          icon: 'none',
        });
      });
  };

  render() {
    const {
      $global: { $tmpStyle },
    } = this.props;
    const { payType, money, orderNo } = this.state;
    return (
      <View data-scoped='wk-opp-PaySuccess' className='wk-opp-PaySuccess pay-success'>
        <Block>
          <View className='pay-area'>
            <Image className='pay-img' mode='aspectFit' src={cdnResConfig.pay_success.pay_success}></Image>
            <Text className='pay-title'>支付成功</Text>
            <Text className='pay-desc'>{payType + '：¥' + money}</Text>
          </View>
          <View className='btn-group'>
            <Button className='back' onClick={this.goHome} style={_safe_style_('background: ' + $tmpStyle.btnColor)}>
              返回首页
            </Button>
            <Button className='watch-order' onClick={this.handleOrder}>
              查看订单
            </Button>
          </View>
          {!!orderNo && <PayGift order={orderNo}></PayGift>}
        </Block>
      </View>
    );
  }
}

export default PaySuccess;
