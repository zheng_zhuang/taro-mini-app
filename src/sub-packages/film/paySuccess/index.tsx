import { View } from '@tarojs/components'
import Taro, { FC } from '@tarojs/taro';
import filmApi from "@/wxat-common/api/film";
import wxApi from '@/wxat-common/utils/wxApi';
import { useEffect, useState } from 'react';

import './index.scss';
import CustomerHeader from '@/wxat-common/components/customer-header';
import { _safe_style_ } from '@/wxat-common/utils/platform';

const FilmPaySuccess: FC = () => {

  const [userInfo, setUserInfo] = useState<any>(null);

  // 获取系统信息
  const systemInfo = wxApi.getSystemInfoSync();
  const menuButtonInfo = wxApi.getMenuButtonBoundingClientRect();
  // 导航栏高度 = 状态栏到胶囊的间距（胶囊距上距离-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
  const navBarHeight = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;

  useEffect(() => {
    wxApi.request({
      url: `${filmApi.film.expireTime}?qrCodeId=${wxApi.getStorageSync('qrCodeId')}`,
      method: 'GET',
    }).then((res: any) => {
      setUserInfo(res.data)
    })
  }, [])

  const terminalPass = () => {
    wxApi.request({
      url: `${filmApi.film.terminalPass}?qrCodeId=${wxApi.getStorageSync('qrCodeId')}`,
      method: 'POST',
      data: {
        qrCodeId: wxApi.getStorageSync('qrCodeId')
      }
    }).then((res: any) => {
      console.log(res)
    })
  }

  // const jumpFilmIndex = () => {
  //   wxApi.$redirectTo({
  //     url: '/sub-packages/film/index/index'
  //   })
  // }

  const jumpUserCenter = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/mine/index'
    })
  }

  const jumpHome = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  }

  return (
    <View className='film-pay-success'>
      <CustomerHeader
        title='观影权益'
      ></CustomerHeader>
      <View style={_safe_style_(`width: 100%;height: ${navBarHeight}px;`)}></View>
      <View>
        <View className='name'>
          {
            userInfo && (
              <View>尊敬的会员：{userInfo.nickName}</View>
            )
          }
        </View>
        {/*<View className='time-box'>*/}
        {/*  <View className='t1'>到期时间：</View>*/}
        {/*  <View className='t2'>{filters.dateFormat(userInfo.movieExpireTime, 'yyyy-MM-dd hh:mm')}</View>*/}
        {/*  {*/}
        {/*    userInfo.status !== 1 && (<View className='btn' onClick={jumpFilmIndex}>续费</View>)*/}
        {/*  }*/}
        {/*</View>*/}
      </View>
      <View className='box'>
        <View className='bg'></View>
        <View className='box-t1'>会员权益使用成功</View>
        <View className='text-box'>
          <View>内容自动播放，如电视未开始播放内容，可点击</View>
          <View className='btn' onClick={terminalPass}>重试</View>
        </View>
      </View>
      <View className='btn-box'>
        <View className='btn btn-1' onClick={jumpHome}>前往首页</View>
        <View className='btn btn-2' onClick={jumpUserCenter}>个人中心</View>
      </View>
    </View>
  )
}

export default FilmPaySuccess;
