import { View, Image, Text, ScrollView, Block } from '@tarojs/components';
import { FC } from '@tarojs/taro';
import HPActionSheet from '@/wxat-common/components/HPActionSheet/index';
import { useImperativeHandle, useState } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import filters from '@/wxat-common/utils/money.wxs';

import './index.scss';
import authHelper from '@/wxat-common/utils/auth-helper';

interface IProps {
  Ref?: any;
  selectCombinationItem: any;
  payPrice: any;
  favorablePriceAmount: any;
  activityItem: any;
  addressData: any;
  confirmPayment: () => void;
}

const PaymentDetails: FC<IProps> = (
  {
    Ref,
    selectCombinationItem,
    payPrice,
    favorablePriceAmount,
    confirmPayment,
    activityItem,
    addressData,
  },
) => {
  const [show, setShow] = useState(false);
  useImperativeHandle(Ref, () => ({
    showHandle: () => {
      setShow(true);
    },
  }));

  const onHide = () => {
    setShow(false);
  };

  const jumpAddAddress = () => {
    if (!authHelper.checkAuth()) return;
    wxApi.$navigateTo({
      url: 'sub-packages/mine-package/pages/address/index',
      data: {
        isSelect: true
      }
    });
  }

  const jumpServiceAgreement = () => {
    wxApi.navigateTo({
      url: '/sub-packages/mine-package/pages/service-agreement/index'
    })
  }

  return (
    <View className='film-payment-details-common'>
      <HPActionSheet show={show} onHide={onHide} customStyle='max-height: 85%;height: auto;'>
        <View className='card-purchase-details'>
          <View className='header-top'>
            <View className='h1'>支付明细</View>
            <View className='close-btn' onClick={onHide} />
          </View>
          {
            activityItem?.find(i => i.type === 1) && (
              <View className='address-box'>
                {
                  addressData ? (
                    <Block>
                      <View className='info'>
                        <View className='name'>{addressData.consignee} {addressData.mobile}</View>
                        <View className='detailed-address'>{addressData.province}{addressData.city}{addressData.region}{addressData.address}{addressData.roomNumber}</View>
                      </View>
                      <View className='btn' onClick={jumpAddAddress}>修改</View>
                    </Block>
                  ) : (
                    <Block>
                      <View className='info'>
                        <View className='empty-status'>暂无收货地址</View>
                      </View>
                      <View className='btn' onClick={jumpAddAddress}>去添加</View>
                    </Block>
                  )
                }
              </View>
            )
          }
          <View className='flex-item payment-amount'>
            <View className='label'>支付总额</View>
            <View className='preferential'>¥{payPrice}</View>
          </View>
          <View className='h2'>商品清单</View>
          <ScrollView scrollY className='list'>
            <View className='item'>
              <Image src={selectCombinationItem?.thumbnail} className='image' mode='aspectFill' />
              <View className='name'>{selectCombinationItem?.name?.substring(0, 10)}</View>
              <View className='price'>¥{filters.floatDiv(selectCombinationItem?.activityPrice || 0, 100)} X 1</View>
            </View>
            {
              activityItem?.map((i: any, index: any) => {
                return (
                  <View className='item' key={index}>
                    <Image src={i?.thumbnail} className='image' mode='aspectFill' />
                    <View className='name'>{i?.name?.substring(0, 10)}</View>
                    <View className='price'>¥{filters.floatDiv(i?.salePrice, 100)} X 1</View>
                  </View>
                )
              })
            }
          </ScrollView>
          <View className='flex-item mb25 mt25'>
            <View className='label'>总价</View>
            <View className='val'>¥{filters.floatDiv(selectCombinationItem?.activityPrice + (activityItem[0] ? activityItem[0].salePrice : 0), 100)}</View>
          </View>
          <View className='flex-item mb25'>
            <View className='label'>优惠</View>
            <View className='val'>
              <View>优惠券 <Text className='label-red'>-¥{favorablePriceAmount}</Text></View>
            </View>
          </View>
          <View className='buy-btn' onClick={confirmPayment}>
            ¥{payPrice} 去支付
          </View>
          <View className='service_agreement_box'>
            <View className='radio-icon'></View>
            <View>购买即同意协议</View>
            <View className='service_agreement' style={_safe_style_('color: #FB822E;')} onClick={jumpServiceAgreement}>《会员服务协议》</View>
          </View>
        </View>
      </HPActionSheet>
    </View>
  );
};

export default PaymentDetails;
