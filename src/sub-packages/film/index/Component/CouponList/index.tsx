import { View, Image, Block, Text } from '@tarojs/components';
import Taro, { FC } from '@tarojs/taro';
import couponEnum from '@/wxat-common/constants/couponEnum';
import HPActionSheet from '@/wxat-common/components/HPActionSheet/index';
import filters from '@/wxat-common/utils/money.wxs';
import { useImperativeHandle, useState } from 'react';
import { _fixme_with_dataset_ } from 'wk-taro-platform';
import './index.scss';

interface IProps {
  Ref?: any;
  coupons: any;
  onConfirm: (coupon: any) => void;
}

const FilmCouponList: FC<IProps> = (
  {
    Ref,
    coupons,
    onConfirm,
  }
) => {
  const [show, setShow] = useState(false);
  useImperativeHandle(Ref, () => ({
    showHandle: () => {
      setShow(true);
    },
  }));

  const onHide = () => {
    setShow(false);
  };

  const onSelectCard = (e: any) => {
    const coupon = e.currentTarget.dataset.coupon;
    onConfirm(coupon);
    onHide();
  };

  const onNoUseCard = () => {
    onConfirm(null);
    onHide();
  };

  return (
    <View className='film-coupon-list-common'>
      <HPActionSheet show={show} onHide={onHide}>
        <View className="coupon-select">
          { coupons?.map((item: any) => {
              return (
                <View
                  className={'coupon-tab couponSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                  key={item.code}
                  onClick={_fixme_with_dataset_(onSelectCard, { coupon: item })}
                >
                  <View className="coupon-img-box">
                    {!item.status ? (
                      <Image
                        className="coupon-img"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gray.png"
                      ></Image>
                    ) : item.couponCategory === 1 ? (
                      <Image
                        className="coupon-img"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-blue.png"
                      ></Image>
                    ) : (
                      <Image
                        className="coupon-img"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gren.png"
                      ></Image>
                    )}
                  </View>
                  <View className="coupon-tab-left">
                    <View className="coupon-price">
                      {(item.couponCategory === couponEnum.TYPE.freight.value ||
                        item.couponCategory === couponEnum.TYPE.fullReduced.value) && (
                        <Block>
                          <Text className="price-logo">¥</Text>
                          <Text className="price-num">{filters.moneyFilter(item.discountFee, true)}</Text>
                        </Block>
                      )}

                      {item.couponCategory === couponEnum.TYPE.discount.value && (
                        <Block>
                          <Text className="price-num">{filters.discountFilter(item.discountFee, true)}</Text>
                          <Text className="price-logo">折</Text>
                        </Block>
                      )}
                    </View>
                    {item.minimumFee === 0 ? (
                      <View className="coupon-minimumFee">无门槛</View>
                    ) : (
                      <View className="coupon-minimumFee">
                        {`满${ filters.moneyFilter(item.minimumFee, true)}可用`}
                      </View>
                    )}

                    {item.couponCategory === 0 && <View className="coupon-type">满减券</View>}

                    {item.couponCategory === 2 && <View className="coupon-type">折扣券</View>}
                  </View>
                  <View className="coupon-tab-right">
                    <View className="coupon-name limit-line">{item.name}</View>
                    {item.endTime && (
                      <View className="coupon-validityDate">{`有效期：${filters.dateFormat(item.endTime)}`}</View>
                    )}
                  </View>
                </View>
              );
            })}
          <View className="no-use-coupon" onClick={onNoUseCard}>不使用优惠</View>
        </View>
      </HPActionSheet>
    </View>
  )
}

export default FilmCouponList;
