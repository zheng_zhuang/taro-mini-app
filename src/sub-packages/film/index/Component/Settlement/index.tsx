import { View, Image } from '@tarojs/components';
import { FC } from '@tarojs/taro';
import { useSelector } from 'react-redux';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import authHelper from '@/wxat-common/utils/auth-helper';
import wxApi from '@/wxat-common/utils/wxApi';

import './index.scss';

interface IProps {
  selectCombinationItem: any;
  favorablePriceAmount: any;
  payPrice: any;
  showCardPurchaseDetails: () => void;
  couponCount: number;
  confirmPayment: () => void;
  handleGoToCouponList: () => void;
  activityItem: any;
}

const FilmSettlement: FC<IProps> = (
  {
    selectCombinationItem,
    favorablePriceAmount,
    payPrice,
    showCardPurchaseDetails,
    couponCount,
    confirmPayment,
    handleGoToCouponList,
    activityItem
  }
) => {

  const userInfo: any = useSelector<any>((state: Record<string, any>) => state.base.userInfo);

  const login = () => {
    authHelper.checkAuth()
  }

  const jumpServiceAgreement = () => {
    wxApi.navigateTo({
      url: '/sub-packages/mine-package/pages/service-agreement/index'
    })
  }

  return (
    <View className='film-settlement-common'>
      <View className='panel checked'>
        <View className='pay-info'>
          <View className='info-box'>
            <View className='quantity' onClick={showCardPurchaseDetails}>
              <View className='t1'>已选</View>
              <View className='t2'>{ 1 + activityItem.length } 件</View>
              <View className='icon'></View>
              {
                activityItem[0] && (
                  <View className='bubble-icon'>
                    <Image className='img' src={activityItem[0].thumbnail} mode='aspectFill' />
                  </View>
                )
              }
            </View>
            <View className='price-box'>
              <View>
                <View className='price'>
                  <View className='priceSymbol'>¥ </View>
                  <View className='txt'>{payPrice}</View>
                </View>
                <View className="offer-box">
                  {
                    userInfo?.phone && (
                      <View onClick={handleGoToCouponList}>
                        {
                          couponCount !== 0 && favorablePriceAmount === 0 &&  (
                            <View className='coupons-box'>
                              <View className="t1">有可用券</View>
                              <View className='right-icon'></View>
                            </View>
                          )
                        }
                        {
                          favorablePriceAmount !== 0 && (
                            <View className='coupons-box'>
                              <View className='t1'>用券</View>
                              <View className='t2'>{ `-¥${favorablePriceAmount}` }</View>
                              <View className='right-icon'></View>
                            </View>
                          )
                        }
                      </View>
                    )
                  }
                  {
                    userInfo && !userInfo.phone && (
                      <View className='t1' onClick={login}>登录查看券</View>
                    )
                  }
                </View>
              </View>
            </View>
          </View>
          <View className='immediatelyPay'>
            <View className='pay' onClick={confirmPayment}>立即支付</View>
          </View>
        </View>
        <View className='service-agreement-box'>
          <View className='radio-icon'></View>
          <View>购买即同意协议</View>
          <View className='service-agreement' style={_safe_style_('color: #FB822E;')} onClick={jumpServiceAgreement}>《会员服务协议》</View>
        </View>
      </View>
    </View>
  )
}
export default FilmSettlement;
