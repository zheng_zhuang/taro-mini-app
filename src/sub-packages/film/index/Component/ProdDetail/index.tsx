import { ScrollView, View } from '@tarojs/components';
import Taro, { FC } from '@tarojs/taro';
import HPActionSheet from '@/wxat-common/components/HPActionSheet/index';
import { useImperativeHandle, useState } from 'react';
import imageUtils from '@/wxat-common/utils/image';
import wxParse from '@/wxat-common/components/parse-wrapper/wxParse';
import ParseWrapper from '@/wxat-common/components/parse-wrapper';

import './index.scss';

interface IProps {
  Ref?: any;
  nodes: any;
}

const FilmProDetail: FC<IProps> = (
  {
    Ref
  }
) => {
  const [show, setShow] = useState(false);
  const [detailData, setDetailData] = useState<any>({});
  useImperativeHandle(Ref, () => ({
    showHandle: (data: any) => {
      setDetailData(processRichText(data));
      setShow(true);
    },
  }));

  const processRichText = (data: any) => {
    if (data) {
      const itemDescribe = imageUtils.richTextFilterCdnImage(data)
      return wxParse('html', itemDescribe);
    } else {
      return '';
    }
  }
  const onHide = () => {
    setShow(false);
  };
  return (
    <View className="film-proDetail-common">
      <HPActionSheet show={show} onHide={onHide} customStyle='max-height: 85%;height: auto;'>
        <View className='close-btn' onClick={onHide}></View>
        <ScrollView scrollY className='scroll-view'>
          {
            show && <ParseWrapper parseContent={detailData} />
          }
        </ScrollView>
      </HPActionSheet>
    </View>
  );
};

export default FilmProDetail;
