import Taro, { FC } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import HPActionSheet from '@/wxat-common/components/HPActionSheet/index';
import AddressList from '@/wxat-common/components/address/list';
import { useImperativeHandle, useState } from 'react';

interface IProps {
  Ref?: any;
}

const FilmAddressList: FC<IProps> = (
  {
    Ref,
  }
) => {
  const [show, setShow] = useState(true);
  useImperativeHandle(Ref, () => ({
    showHandle: () => {
      setShow(true);
    },
  }));
  const onHide = () => {
    setShow(false);
  };
  const onAddressChange = (item: any) => {
    console.log(item)
  }
  return (
    <View>
      <View className='film-address-list-common'>
        <HPActionSheet show={show} onHide={onHide} customStyle='max-height: 85%;height: auto;'>
          <ScrollView scrollY className='scroll-view'>
            <AddressList onChange={onAddressChange}></AddressList>
          </ScrollView>
        </HPActionSheet>
      </View>
    </View>
  )
}
export default FilmAddressList;
