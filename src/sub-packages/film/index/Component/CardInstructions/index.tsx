import { View, Image } from '@tarojs/components';
import Taro, { FC } from '@tarojs/taro';
import HPActionSheet from '@/wxat-common/components/HPActionSheet/index';
import { useImperativeHandle, useState } from 'react';

import './index.scss';
interface IProps {
  Ref?: any;
}
const FilmCardInstructions: FC<IProps> = (
  {
    Ref
  }
) => {
  const [show, setShow] = useState(false);
  useImperativeHandle(Ref, () => ({
    showHandle: () => {
      setShow(true);
    },
  }));

  const onHide = () => {
    setShow(false);
  };
  return (
    <View className='film-card-instructions'>
      <HPActionSheet show={show} onHide={onHide} customStyle='max-height: 600px;height: 600px;'>
        <View onClick={onHide}>
          <View>
            <Image className='image' src='https://bj.bcebos.com/htrip-mp/static/app/images/film/ViewingCardInstructions.png' />
          </View>
          <View className='center'>
            <View className='btn'>
              <View className='txt'>知道了</View>
            </View>
          </View>
        </View>
      </HPActionSheet>
    </View>
)
}

export default FilmCardInstructions;
