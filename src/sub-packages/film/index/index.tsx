import { View, Image, ScrollView, Block } from '@tarojs/components';
import Taro, { FC, useDidShow, useDidHide } from '@tarojs/taro';
import { useSelector } from 'react-redux';
import { useEffect, useRef, useState } from 'react';
import filters from '@/wxat-common/utils/money.wxs';
import wxApi from '@/wxat-common/utils/wxApi';
import filmApi from '@/wxat-common/api/film';
import api from '@/wxat-common/api/index';
import util from '@/wxat-common/utils/util';
import authHelper from '@/wxat-common/utils/auth-helper';
import CustomerHeader from '@/wxat-common/components/customer-header';
import PaymentDetails from '@/sub-packages/film/index/Component/PaymentDetails';
import CouponListComponent from '@/sub-packages/film/index/Component/CouponList';
import Settlement from '@/sub-packages/film/index/Component/Settlement';
import ProdDetail from '@/sub-packages/film/index/Component/ProdDetail/index';
import CardInstructions from '@/sub-packages/film/index/Component/CardInstructions/index';
import PosterModule from '@/wxat-common/components/decorate/posterModule';
import ImageModule from '@/wxat-common/components/decorate/imageModule/index';
import TitleModule from '@/wxat-common/components/decorate/titleModule';
import AssistBlankModule from '@/wxat-common/components/decorate/assistBlankModule';
import HotAreaModule from '@/wxat-common/components/decorate/hotAreaModule';
import ProductModule from '@/wxat-common/components/base/productModule';
import classNames from 'classnames';
import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';

import './index.scss';

const FilmIndex: FC = () => {
  const initData = useRef<any>(false);
  // 活动列表数据初始化完成
  const [pageConfig, setPageConfig] = useState<any>(false);
  const [activityListInit, setActivityListInit] = useState<any>(false);
  const defaultAddress = useRef<any>(null);
  const needAddress = useRef<any>(null);
  const filmQrCodeDetail = useRef(wxApi.getStorageSync('filmQrCodeDetail'));
  const loginInfo: any = useSelector<any>((state: Record<string, any>) => state.base.loginInfo);
  const [loading, setLoading] = useState(false);
  const [tabActive] = useState(1);
  const [pagination, setPagination] = useState<any>({
    page: 1,
    pageSize: 6,
    total: 0
  })
  const [favorablePriceAmount, setFavorablePriceAmount] = useState(0);

  const [payPrice, setPayPrice] = useState(0);

  const [currentActive, setCurrentActive] = useState(0);

  const [cardList, setCardList] = useState<any>([]);

  const cardListRef: any = useRef<any>(null);

  const [couponCount, setCouponCount] = useState<any>(0);

  const selectCombinationItem = useRef<any>(null);

  const selectCouponsItem = useRef<any>(null);

  const [activityPushItemList, setActivityPushItemList] = useState<any>([]);

  const [activityList, setActivityList] = useState<any>([]);

  const [activityItem, setActivityItem] = useState<any>([]);
  const selectActivityItem = useRef<any>([]);
  const pushItemNum = useRef<number>(0);

  const paymentDetailsRef: any = useRef<any>(null);

  const [useCouponList, setUseCouponList] = useState<any>([]);

  const [widthNum, setWidthNum] = useState<number>(wxApi.getSystemInfoSync().windowWidth / 3);
  const [leftDistance, setLeftDistance] = useState<number>(wxApi.getSystemInfoSync().windowWidth / 3);
  const cardItemWidth = useRef(0)
  const [customData, setCustomData] = useState<any>([]);

  const showCardPurchaseDetails = () => {
    paymentDetailsRef.current?.showHandle();
  };

  useDidShow(() => {
    defaultAddress.current = wxApi.getStorageSync('selectedAddress');
    if (wxApi.getStorageSync('selectedAddress') && wxApi.getStorageSync('selectedAddress') === 'refresh') {
      if (addressData) {
        wxApi.removeStorageSync('selectedAddress')
        getAddressList();
      }
      return
    }
    if (defaultAddress.current) {
      wxApi.removeStorageSync('selectedAddress')
      getAddressList();
    }
  })

  useDidHide(() => {
    initData.current = false
    defaultAddress.current = null
  })

  useEffect(() => {
    if (loginInfo?.appId && !initData.current) {
      initFileConfig();
    }
  }, [loginInfo, filmQrCodeDetail]);

  const initFileConfig = () => {
    initData.current = true
    // 有useId 说明已经登录
    if (loginInfo.userId) {
      wxApi.request({
        url: `${filmApi.film.expireTime}?qrCodeId=${wxApi.getStorageSync('qrCodeId')}`,
        loading: false,
        method: 'GET',
      }).then((res: any) => {
        if (res.data.status === 1) {
          wxApi.$redirectTo({
            url: '/sub-packages/film/paySuccess/index',
          });
        } else {
          getFileConfig();
        }
      }).catch((error: any) => {
        wxApi.showToast({
          icon: 'none',
          title: error.data.errorMessage || '请重试',
          mask: true,
          duration: 2000,
        });
      });
    } else {
      getFileConfig();
    }
  }

  const getFileConfig = () => {
    let params = {}
    const storeId = filmQrCodeDetail.current.apaasStoreId || loginInfo.storeId;
    if (filmQrCodeDetail.current.apaasStoreId) {
      params = {
        appId: loginInfo.appId,
        apaasStoreId: storeId,
        isAll: false
      }
    } else {
      params = {
        appId: loginInfo.appId,
        storeId: storeId,
        isAll: false
      }
    }
    wxApi.request({
      url: api.login.getTemplateConfig,
      method: 'POST',
      loading: false,
      data: params,
    }).then((res: any) => {
      if (res) {
        if (!res.data.homePageConfig) {
          wxApi.switchTab({
            url: '/wxat-common/pages/home/index',
          });
          return false
        }
        const resData = JSON.parse(res.data.homePageConfig);
        const pageInfoModule: any = resData.find((item: any) => item.id === 'pageInfoModule');
        const virtualModule: any = resData.find((item: any) => item.id === 'virtualModule');
        setPageConfig(pageInfoModule.config);
        getCardList(virtualModule);
        setCustomData(resData);
      }
    }).catch((error: any) => {
      wxApi.showToast({
        icon: 'none',
        title: error.data?.errorMessage || '获取配置数据失败',
        mask: true,
      });
      wxApi.switchTab({
        url: '/wxat-common/pages/home/index',
      });
    });
  };

  const [addressData, setAddressData] = useState<any>(null);

  const addressDataRef: any = useRef<any>(null);

  const getAddressList = () => {
    wxApi.request({
        url: api.address.list,
        loading: false,
      })
      .then((res: any) => {
        if (res.data) {
          let findAddress: any = ''
          if (defaultAddress.current) {
            findAddress = res.data.find(i => i.id === defaultAddress.current.id) || res.data[0]
          } else {
            findAddress = res.data.find(i => i.isDefault === 1) || res.data[0]

          }
          setAddressData(findAddress);
          addressDataRef.current = findAddress;
          getCoupons().then(() => {
            calPrice().then();
          })
        } else {
          defaultAddress.current = null
          setAddressData(null)
          addressDataRef.current = null
          calculatePrice().then();
        }
      })
  }

  useEffect(() => {
    if (addressData) {
      calPrice().then();
    }
  }, [addressData]);

  const getCardList = (virtualModule: any) => {
    wxApi.request({
      url: api.classify.itemSkuList,
      method: 'GET',
      loading: false,
      data: util.formatParams({ itemNoList: virtualModule.config.itemNos }),
    }).then((res: any) => {
      setCardList(res.data);
      cardListRef.current = res.data;
      setLeftDistance(0);
      onSelectCard({ currentTarget: { dataset: { card: res.data[0].wxItem, index: 0 } } });
      getActivityByItemNos()
    });
  };



  useEffect(() => {
    if (activityListInit) {
      const percentag: number = wxApi.getSystemInfoSync().screenWidth / 375
      cardItemWidth.current = percentag * 114
      setWidthNum(percentag * 114 * cardList.length);
    }
  }, [cardList.length, activityListInit]);

  const getActivityByItemNos = () => {
    wxApi.request({
      url: filmApi.film.getActivityByItemNos,
      method: 'post',
      loading: false,
      data: { appId: loginInfo.appId, itemNos: cardListRef.current.map(i => i.wxItem.itemNo), activityType: 2 },
    }).then((res: any) => {
      const newCardList = cardListRef.current.map((item: any, index: any) => {
        if (res.data[index].activityPrice !== null) {
          item.wxItem.activityPrice = res.data[index]?.activityPrice
          if (res.data[index].itemPrice !== null) {
            item.wxItem.salePrice = res.data[index].itemPrice
          }
        } else {
          if (res.data[index].labelPrice !== null && res.data[index].itemPrice !== null){
            item.wxItem.activityPrice = res.data[index]?.itemPrice
            item.wxItem.salePrice = res.data[index].labelPrice
          } else if (res.data[index].itemPrice !== null){
            item.wxItem.activityPrice = res.data[index]?.itemPrice
            item.wxItem.salePrice = res.data[index].itemPrice
          }
        }
        item.wxItem.activityId = res.data[index]?.activityId || ''
        item.wxItem.itemHornTitle = res.data[index]?.itemHornTitle || ''
        return {
          ...item
        }
      })
      setCardList(newCardList)
    })
  }


  const onSelectCard = (item: any) => {
    const { card, index } = item.currentTarget.dataset;
    selectCombinationItem.current = card;
    util.sensorsReportData('film_card_click', {event_name: '点击观影卡', card_name: card.name, card_id: card.itemNo, activity_price: card.activityPrice, sale_price: card.salePrice})
    setCurrentActive(index);
    let width: number = 0;
    for (let i = 0; i < index; i++) {
      width += cardItemWidth.current;
    }
    if (width > wxApi.getSystemInfoSync().screenWidth / 2) {
      setLeftDistance(width + cardItemWidth.current - wxApi.getSystemInfoSync().screenWidth / 2);
    } else {
      setLeftDistance(0);
    }
    getRecentEnableActivity(card)
  };

  const getRecentEnableActivity = (card: any) => {
    wxApi.request({
      url: `${filmApi.film.queryRecentEnableActivityByItemNo}?appId=${loginInfo.appId}&itemNo=${card.itemNo}&activityType=2`,
      method: 'GET',
      loading: false,
    }).then((res: any) => {
      if (res.data) {
        wxApi.request({
          url: api.classify.itemSkuList,
          method: 'GET',
          loading: false,
          data: util.formatParams({
            itemNoList: res.data.welfarePurchaseConditionDTO.conditionItemList.map((item: any) => item.itemNo)
          }),
        }).then((itemRes: any) => {
          if (itemRes.data) {
            pushItemNum.current = res.data.welfarePurchaseConditionDTO.pushItemNum
            if (pushItemNum.current) {
              setActivityPushItemList(itemRes.data.slice(0, pushItemNum.current))
              setActivityList(itemRes.data.slice(pushItemNum.current))
              setPagination({
                page: 1,
                pageSize: itemRes.data.slice(0, pushItemNum.current).length > 0 ? 4 : 6,
                total: itemRes.data.slice(pushItemNum.current).length
              })
            } else {
              setActivityList(itemRes.data)
            }
            setActivityItem([itemRes.data[0].wxItem])
            selectActivityItem.current = [itemRes.data[0].wxItem]
            if (itemRes.data[0].wxItem.type === 1) {
              if (loginInfo.userId) {
                getAddressList();
              } else {
                calculatePrice().then();
              }
            } else {
              calculatePrice().then();
            }
          } else {
            setActivityPushItemList([])
            setActivityList([])
            setActivityItem([])
            selectActivityItem.current = []
            calculatePrice().then();
          }
          setActivityListInit(true)
        })
      } else {
        setActivityPushItemList([])
        setActivityList([])
        setActivityItem([])
        selectActivityItem.current = []
        if (loginInfo.userId) {
          getCoupons().then(() => {
            calPrice().then();
          })
        } else {
          calculateTotalPrice();
        }
        setActivityListInit(true)
      }
    })
  }

  const changeBatch = (data: any) => {
    const { location } = data.currentTarget.dataset;
    util.sensorsReportData('film_change_batch_click', {event_name: '点击换一批好物', location: location })
    if (Math.ceil(pagination.total / pagination.pageSize) === pagination.page) {
      setPagination({
        ...pagination,
        page: 1
      })
    } else {
      setPagination({
        ...pagination,
        page: pagination.page + 1
      })
    }
    wxApi.createSelectorQuery()
      .select('#main-combination-products-list')
      .boundingClientRect((rect) => {
        const percentag: number = wxApi.getSystemInfoSync().screenWidth / 375
        Taro.pageScrollTo({
          scrollTop: 400 * percentag + rect.height,
          duration: 500
        })
      })
      .exec();
  }

  const calculatePrice = async () => {
    if (loginInfo.userId) {
      getCoupons().then(() => {
        calculateTotalPrice();
      })
    } else {
      calculateTotalPrice();
    }
  };

  const calculateTotalPrice = () => {
    let needPayPrice: any = 0;
    let activityPayPrice: any = 0;
    if (selectActivityItem.current.length) {
      activityPayPrice = selectActivityItem.current.reduce((total: any, item: any) => {
        return total + item.salePrice;
      }, 0);
    }
    needPayPrice = selectCombinationItem.current.activityPrice + activityPayPrice;
    needPayPrice = needPayPrice <= 0 ? 0 : needPayPrice;
    if (selectCouponsItem.current) {
      needPayPrice = needPayPrice - selectCouponsItem.current.couponPromFee <= 0 ? 0 : needPayPrice - selectCouponsItem.current.couponPromFee;
      setFavorablePriceAmount(filters.floatDiv(selectCouponsItem.current.couponPromFee, 100));
      setPayPrice(filters.floatDiv(needPayPrice, 100));
    } else {
      setPayPrice(filters.floatDiv(needPayPrice, 100));
    }
    if (!loginInfo.userId) {
      setPayPrice(filters.floatDiv(needPayPrice, 100));
    }
  }

  const getCoupons = () => {
    const params = {
      storeId: selectCombinationItem.current.storeId,
      expressType: 0,
      itemDTOList: [
        {
          itemNo: selectCombinationItem.current.itemNo,
          itemCount: 1,
          skuId: selectCombinationItem.current.skuId || '',
          mpActivityName: selectCombinationItem.current.mpActivityName || '',
        },
      ],
    };

    params.itemDTOList = selectActivityItem.current.concat([
      {
        itemNo: selectCombinationItem.current.itemNo,
      },
    ]).map((i: any) => {
      return {
        itemNo: i.itemNo,
        itemCount: 1,
        skuId: i.skuId || '',
        mpActivityName: i.mpActivityName || '',
      };
    })
    return new Promise((resolve, reject) => {
      wxApi
        .request({
          url: api.order.coupons,
          quite: true,
          loading: false,
          data: util.formatParams(params),
        })
        .then((res: any) => {
          setUseCouponList(res.data.couponSet);
          selectCouponsItem.current = res.data.couponSet[0];
          setCouponCount(res.data.couponSet.length);
          resolve(res.data);
        })
        .catch((error: any) => {
          reject('score: error: ' + JSON.stringify(error));
        });
    });
  };

  const calPrice = () => {
    return new Promise((resolve, reject) => {
      const params = {
        wxAppId: '',
        appId: '',
        storeId: selectCombinationItem.current.storeId,
        storeName: '',
        payChannel: 1,
        channelId: 0,
        qrCodeId: '',
        orderMessage: '',
        orderRequestPromDTO: {
          couponNo: selectCouponsItem.current ? selectCouponsItem.current.code : '',
        },
        orderRequestDTOS: [],
      };
      params.orderRequestDTOS = selectActivityItem.current.concat([selectCombinationItem.current]).map((i: any) => {
        return {
          expressType: i.type === 41 ? 2 : i.type,
          userAddressId: i.type === 1 && addressDataRef.current?.id ? addressDataRef.current?.id : '',
          itemDTOList: [
            {
              itemNo: i.itemNo,
              itemCount: 1,
              itemType: i.type,
              virtualItemType: i.virtualItemType || '',
              marketActivityId: i.activityId || '',
            }
          ]
        };
      })
      try {
        wxApi.request({
          url: api.order.cal_price_new,
          quite: true,
          loading: false,
          method: 'POST',
          data: params,
        })
          .then((res: any) => {
            setFavorablePriceAmount(filters.floatDiv(res.data.couponPrice, 100));
            setPayPrice(filters.floatDiv(res.data.needPayPrice, 100));
            resolve(res.data);
          }).catch((error: any) => {
            wxApi.showToast({
              icon: 'none',
              title: error.data.errorMessage || '请重试',
              mask: true,
              duration: 2000,
            });
            reject(error)
          })
      } catch (e) {
        console.log('score: error: ' + JSON.stringify(e))
      }
    })
  };
  // 下单
  const placeOrder = async () => {
    if (loading) return;
    needAddress.current = selectActivityItem.current.concat([selectCombinationItem.current]).find((i: any) => i.type === 1);
    if (needAddress.current) {
      paymentDetailsRef.current?.showHandle();
    } else {
      payment().then();
    }
  };

  const payment = async () => {
    if (!authHelper.checkAuth()) return;
    wxApi.showLoading();
    if (needAddress.current && !addressData) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择收货地址',
        mask: true,
      });
      return false;
    }
    await calPrice()
    const params = {
      wxAppId: loginInfo.wxAppId,
      appId: loginInfo.appId,
      storeId: selectCombinationItem.current.storeId,
      outStoreId: wxApi.getStorageSync('filmQrCodeDetail').hotelContract,
      outStoreName: wxApi.getStorageSync('filmQrCodeDetail').apaasStoreName,
      roomCode: wxApi.getStorageSync('filmQrCodeDetail').roomNum,
      storeName: '',
      payChannel: 1,
      channelId: 0,
      orderMessage: '',
      orderRequestPromDTO: {
        couponNo: selectCouponsItem.current ? selectCouponsItem.current.code : null,
      },
      orderRequestDTOS: [],
    };
    params.orderRequestDTOS = selectActivityItem.current.concat([selectCombinationItem.current]).map((i: any) => {
      return {
        expressType: i.type === 41 ? 2 : i.type,
        userAddressId: i.type === 1 && addressDataRef.current.id ? addressDataRef.current.id : '',
        itemDTOList: [
          {
            itemNo: i.itemNo,
            itemCount: 1,
            itemType: i.type,
            virtualItemType: i.virtualItemType || '',
            qrCodeId: wxApi.getStorageSync('qrCodeId'),
            marketActivityId: i.activityId || '',
          }
        ]
      };
    })
    util.sensorsReportData('film_payment', {event_name: '点击观影卡支付', product_list: JSON.stringify(params.orderRequestDTOS), pay_channel: 1, coupon_no: params.orderRequestPromDTO.couponNo})
    setLoading(true);
    wxApi.request({
        url: api.order.createOrderAfterCalPrice,
        quite: true,
        method: 'POST',
        data: params,
      })
      .then((res: any) => {
        const order = res.data;
        // 判断是否需要支付
        if (order.price) {
          wxApi.requestPayment({
            timeStamp: order.timeStamp + '',
            nonceStr: order.nonceStr,
            package: `prepay_id=${order.prepayId}`,
            signType: order.signType,
            paySign: order.paySign,
            success: () => {
              util.sensorsReportData('film_pay_success', {
                event_name: '观影卡支付成功',
                need_pay_price: order.needPayPrice,
                order_no: order.orderNo
              })
              wxApi.showToast({
                icon: 'success',
                title: '支付成功',
                mask: true,
                complete: () => {
                  wxApi.$navigateTo({
                    url: '/sub-packages/film/paySuccess/index',
                  });
                },
              });
            },
            fail: () => {
              wxApi.showToast({
                icon: 'none',
                title: '支付已取消',
                mask: true,
              });
              getCoupons().then(() => {
                calPrice().then();
              })
              util.sensorsReportData('film_order_cancel', {
                event_name: '观影卡支付取消',
                need_pay_price: order.needPayPrice,
                order_no: order.orderNo
              })
            },
          });
        } else {
          wxApi.$navigateTo({
            url: '/sub-packages/film/paySuccess/index',
          });
          util.sensorsReportData('zero_film_order', {
            event_name: '0元影视订单',
            need_pay_price: order.needPayPrice,
            order_no: order.orderNo
          })
        }
        util.sensorsReportData('create_film_order_success', {
          event_name: '创建观影卡订单成功',
          need_pay_price: order.needPayPrice,
          order_no: order.orderNo
        })
      })
      .catch((error: any) => {
        wxApi.showToast({
          icon: 'none',
          title: error.data.errorMessage || '请重试',
          mask: true,
          duration: 2000,
        });

        util.sensorsReportData('create_film_order_error', {
          event_name: '创建观影卡订单失败',
          product_list: JSON.stringify(params.orderRequestDTOS),
          error_message: error.data.errorMessage || '请重试'
        })
      }).finally(() => {
      // wxApi.hideLoading();
      setLoading(false);
    });
  }

  const couponListRef: any = useRef<any>(null);
  const ProdDetailData: any = useRef<any>(null);
  const handleGoToCouponList = () => {
    couponListRef.current?.showHandle();
  };

  const chooseCoupon = async (coupon: any) => {
    selectCouponsItem.current = coupon;
    if (addressData) {
      calPrice().then();
    } else {
      calculateTotalPrice();
    }
  };

  const prodDetailRef: any = useRef<any>(null);

  const onSelectActivityItem = (target: any) => {
    if (target.currentTarget.dataset.item.itemNo === activityItem[0]?.itemNo) {
      return
    }
    setActivityItem([target.currentTarget.dataset.item]);
    selectActivityItem.current = [target.currentTarget.dataset.item];
    if (addressData) {
      getCoupons().then(() => {
        calPrice().then();
      });
    } else {
      calculatePrice().then();
    }
    util.sensorsReportData('film_combination_products_click', {
      event_name: '点击观影卡组合商品',
      is_push: target.currentTarget.dataset.isPush,
      prod_item_no: target.currentTarget.dataset.item.itemNo
    })
  }
  const onShowProDetail = (target: any) => {
    if(target.currentTarget.dataset.item.describe) {
      util.sensorsReportData('film_card_detail', {
        event_name: '观影卡商品详情',
        prod_item_no: target.currentTarget.dataset.item.itemNo
      })
      prodDetailRef.current?.showHandle(target.currentTarget.dataset.item.describe);
    }
  }

  const cardInstructionsRef: any = useRef<any>(null);
  const showViewCardInstructions = () => {
    cardInstructionsRef.current?.showHandle();
  }

  return (
    <View
      className='film-index-common'
      style={_safe_style_(
        `background: ${'url(' + pageConfig.linkUrl + ') no-repeat top left /100% '+ pageConfig?.bgColor};`,
      )}
    >
      <CustomerHeader
        title='影视'
      ></CustomerHeader>
      {
        activityListInit && customData?.map((customDataItem: any, customDataIndex: any) => {
          return (
            <View key={customDataIndex}>
              {(customDataItem.id === 'imageModule' && customDataItem.config.data && customDataItem.config.data.length) && (<ImageModule dataSource={customDataItem.config} />)}
              {customDataItem.id === 'virtualModule' && (
                <View>
                  {tabActive === 1 && (
                    <View className='tab-content'>
                      <View className='top-info'>
                        <View className='info-box'>
                          <Image className='icon' src='https://bj.bcebos.com/htrip-mp/static/app/images/film/film-title.png' />
                          <View className='info'>开通会员畅享海量高清影视资源</View>
                          <View className='watch'>10w+人在看</View>
                        </View>
                      </View>
                      {/* 卡包列表 */}
                      {
                        activityListInit && cardList.length > 0 && (
                          <Block>
                            <ScrollView scrollX scrollLeft={leftDistance} scrollWithAnimation>
                              <View className={classNames(
                                'card-list',
                                cardList.length <= 3 ? 'card-list-flex' : '',
                              )}
                                    style={_safe_style_({ width: cardList.length <= 3 ? 'auto' : `${widthNum}px` })}>
                                {
                                  cardList?.map((item: any, index: any) => {
                                    return (
                                      <View
                                        className={
                                        classNames(
                                          'boxSliding',
                                          (activityPushItemList.length > 0 || activityList.length > 0) ? 'is_bind' : 'no_bind')
                                        }
                                        style={_safe_style_(cardList.length <= 3 ? 'max-width: auto' : `max-width: 208rpx;flex: 0 0 208rpx`)}
                                        key={index}
                                      >
                                        <View
                                          onClick={_fixme_with_dataset_(onSelectCard, { card: item.wxItem, index })}
                                          className={classNames(
                                            'detailsShopping',
                                            { 'shoppingContentActive': currentActive === index },
                                          )}
                                        >
                                          {
                                            item.wxItem.itemHornTitle && (
                                              <View className='top'>
                                                <View className='tag'>{item.wxItem.itemHornTitle.substring(0,8)}</View>
                                              </View>
                                            )
                                          }
                                          <View className='sales_film_title'>{item.wxItem.name}</View>
                                          <View className='shoppingPrice'>
                                            <View className='p1'>
                                              <View className='s1'>¥</View>
                                              <View className='s2'>
                                                {filters.floatDiv(item.wxItem.activityPrice || 0, 100)}
                                              </View>
                                              {
                                                item.wxItem.activityPrice !== 0 && (item.wxItem.activityPrice) < item.wxItem.salePrice && (
                                                  <View className='tis'>优惠</View>
                                                )
                                              }
                                            </View>
                                            {
                                              (item.wxItem.activityPrice) < item.wxItem.salePrice && (<View className='p2'>原价¥{filters.floatDiv(item.wxItem.salePrice, 100)}</View>)
                                            }
                                          </View>
                                        </View>
                                        {
                                          currentActive === index && (<View className='line'></View>)
                                        }
                                      </View>
                                    );
                                  })
                                }
                              </View>
                            </ScrollView>
                            {
                              (activityPushItemList.length === 0 && activityList.length === 0) && (
                                <View className='card-tis'>
                                  <View className='text-box'>
                                    <View className='t1'>{cardList[currentActive]?.wxItem.subName}</View>
                                    <View className='t2'>全场影片通用，断电后请重新扫码登录</View>
                                  </View>
                                  <View className='info' onClick={showViewCardInstructions}>使用说明</View>
                                </View>
                              )
                            }
                          </Block>
                        )
                      }
                      {
                        (activityPushItemList.length > 0 || activityList.length > 0) && (
                          <View className='combination-products-list-box'>
                            <View className='title zero-shop'>
                              <View className='txt-box'>
                                <View className='t1'>选好物 解锁全场影片</View>
                              </View>
                              <View>
                                {
                                  activityList.length > pagination.pageSize && (
                                    <View className='change-btn' onClick={_fixme_with_dataset_(changeBatch, {location: 'top'})}>
                                      <View className='icon'></View>
                                      <View className='txt'>换一批</View>
                                    </View>
                                  )
                                }
                              </View>
                            </View>
                            <View className='product-description'>
                              <View className='txt after'>购物解锁观影</View>
                              <View className='txt after'>包邮到家</View>
                              <View className='txt t3'>无忧售后</View>
                            </View>
                            <View id='main-combination-products-list' className='main-combination-products-list'>
                              {
                                activityPushItemList?.map((item: any, index: any) => {
                                  return (
                                    <View
                                      className={classNames('main-combination-products-item', {'active': activityItem[0]?.itemNo === item.wxItem.itemNo})}
                                      key={index}
                                    >
                                      <View onClick={_fixme_with_dataset_(onShowProDetail, {item: item.wxItem})}>
                                        <Image className='img-box' src={item.wxItem.thumbnail} mode='aspectFill' />
                                        {
                                          item.wxItem.describe && (
                                            <View className='show-detail-icon' onClick={_fixme_with_dataset_(onShowProDetail, {item: item.wxItem})}></View>
                                          )
                                        }
                                      </View>
                                      <View className='info-box' onClick={_fixme_with_dataset_(onSelectActivityItem, { item: item.wxItem, index, isPush: true })}>
                                        <View className='name'>{item.wxItem.name}</View>
                                        <View className='des'>{item.wxItem.subName}</View>
                                        <View className='price-box'>
                                          <View className='price'>
                                            <View className='priceSymbol'>¥</View>
                                            <View className='txt'>{filters.floatDiv(item.wxItem.salePrice, 100)}</View>
                                            <View className='original-price-box'>
                                              <View className='preferential'>优惠{filters.floatSub(filters.floatDiv(item.wxItem.labelPrice, 100), filters.floatDiv(item.wxItem.salePrice, 100))}元</View>
                                              <View className='original-price'>¥ {filters.floatDiv(item.wxItem.labelPrice, 100)}</View>
                                            </View>
                                          </View>
                                        </View>
                                        <View className='sale'>已售 {item.wxItem.itemSalesVolume}</View>
                                        <View className='select-btn'></View>
                                      </View>
                                    </View>
                                  )
                                })
                              }
                            </View>
                            <View className='combination-products'>
                              <View className='sub-combination-products-list'>
                                {
                                  activityList.slice((pagination.page -1) * pagination.pageSize, pagination.page * pagination.pageSize).map((item: any, index: any) => {
                                    return (
                                      <View
                                        className={classNames('combination-products-box-flex')}
                                        key={index}
                                      >
                                        <View className={classNames('combination-products-box', {'active': activityItem[0]?.itemNo === item.wxItem.itemNo})}>
                                          <View onClick={_fixme_with_dataset_(onShowProDetail, {item: item.wxItem})}>
                                            <View className='single-box'>
                                              <Image className='single' src={item.wxItem.thumbnail} mode='aspectFill' />
                                              {
                                                item.wxItem.describe && (
                                                  <View className='show-detail-icon'></View>
                                                )
                                              }
                                              <View className='sale'>已售 {item.wxItem.itemSalesVolume}</View>
                                            </View>
                                          </View>
                                          <View className='prod-info' onClick={_fixme_with_dataset_(onSelectActivityItem, { item: item.wxItem, index, isPush: false })}>
                                            <View className='prod-info-top'>
                                              <View className='name'>{item.wxItem.name}</View>
                                            </View>
                                            <View className='explain'>{item.wxItem.subName}</View>
                                            <View className='buy-info'>
                                              <View className='price-box'>
                                                <View className='price'>
                                                  <View className='price-symbol'>¥</View>
                                                  <View className='price-txt'>{filters.floatDiv(item.wxItem.salePrice, 100)}</View>
                                                </View>
                                                <View className='original-price'>¥ {filters.floatDiv(item.wxItem.labelPrice, 100)}</View>
                                              </View>
                                              <View className='select-btn'></View>
                                            </View>
                                          </View>
                                          <View className='active'></View>
                                        </View>
                                      </View>
                                    )
                                  })
                                }
                              </View>
                            </View>
                            {
                              activityList.length > pagination.pageSize && (
                                <View className='change-batch' onClick={_fixme_with_dataset_(changeBatch, {location: 'bottom'})}>
                                  <View className='icon'></View>
                                  <View className='txt'>换一批好物</View>
                                </View>
                              )
                            }
                            <View className='card-instructions'>
                              <View className='text'>48小时内发货，观影后退货按9.9元/天优享价扣除</View>
                            </View>
                          </View>
                        )
                      }
                    </View>
                  )}
                </View>
              )}
              {(customDataItem.id === 'posterModule' && customDataItem.config.data && customDataItem.config.data.length) && (<PosterModule dataSource={customDataItem.config} />)}
              {customDataItem.id === 'titleModule' && (<TitleModule dataSource={customDataItem.config} padding={20} />)}
              {customDataItem.id === 'assistBlankModule' && customDataItem.config && (<AssistBlankModule dataSource={customDataItem.config} />)}
              {customDataItem.id === 'hotAreaModule' && (<HotAreaModule dataSource={customDataItem.config} />)}
              {customDataItem.id === 'productModule' && customDataItem.config.data && customDataItem.config.data.length && (<ProductModule dataSource={customDataItem.config}/>)}
            </View>
          )
        })
      }

      {/* 底部结算面板 */}
      {
        activityListInit && (
          <Settlement
            showCardPurchaseDetails={showCardPurchaseDetails}
            selectCombinationItem={selectCombinationItem.current}
            favorablePriceAmount={favorablePriceAmount}
            payPrice={payPrice}
            couponCount={couponCount}
            confirmPayment={placeOrder}
            handleGoToCouponList={handleGoToCouponList}
            activityItem={activityItem}
          />
        )
      }
      {/* 结算面板 */}
      {
        selectCombinationItem.current && (
          <PaymentDetails
            Ref={paymentDetailsRef}
            selectCombinationItem={selectCombinationItem.current}
            favorablePriceAmount={favorablePriceAmount}
            payPrice={payPrice}
            confirmPayment={payment}
            addressData={addressData}
            activityItem={activityItem}
          ></PaymentDetails>
        )
      }
      {/*  优惠券列表 */}
      {
        useCouponList.length > 0 && (
          <CouponListComponent
            Ref={couponListRef}
            coupons={useCouponList}
            onConfirm={chooseCoupon}
          >
          </CouponListComponent>
        )
      }
      {/*  商品详情 */}
      {
        ProdDetailData && (
          <ProdDetail
            Ref={prodDetailRef}
            nodes={ProdDetailData}
          ></ProdDetail>
        )
      }
      {/*  卡包使用说明 */}
      <CardInstructions Ref={cardInstructionsRef} ></CardInstructions>
    </View>
  );
};

export default FilmIndex;

