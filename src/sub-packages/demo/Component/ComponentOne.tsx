import { View, Text, Button } from '@tarojs/components';
import { FC } from '@tarojs/taro';
import {useImperativeHandle} from 'react';

interface IProps {
  childRef?: any;
  title: string;
  number: number;
  ParentTestFn: () => void;
}
const ComponentOne: FC<IProps> = (
  {
    childRef,
    title,
    number,
    ParentTestFn
  }
) => {
  useImperativeHandle(childRef, () => ({
    clickFn: clickFn,
    customData: 'customData'
  }));
  const clickFn = () => {
    console.log('clickFn')
  }

  return (
    <View>
      <Text>ComponentOne - {title} - {number}</Text>
      <View>
        <Button onClick={ParentTestFn}>ParentTestFn</Button>
      </View>
    </View>
  )
}

export default ComponentOne;
