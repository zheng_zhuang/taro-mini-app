import { View, Text, Button } from '@tarojs/components'
import Taro, {FC} from '@tarojs/taro'
import { useState, useEffect, useRef } from 'react';
import {useSelector} from 'react-redux';
import ComponentOne from '@/sub-packages/demo/Component/ComponentOne';



const DemoPage: FC = () => {
  const app = Taro.getApp();
  const [count, setCount] = useState(0);

  // useEffect 案例
  const [start, setStart] = useState(false)
  const [time, setTime] = useState(60)
  const interval = useRef<any>()

  useEffect(() => {
    setCount(count + 1)
    // effect 函数，不接受也不返回任何参数
    if (start) {
      interval.current = setInterval(() => {
        setTime((t) => t - 1) // ✅ 在 setTime 的回调函数参数里可以拿到对应 state 的最新值
      }, 1000)
    }
    return () => clearInterval(interval.current) // clean-up 函数，当前组件被注销时调用
  }, [start]) // 依赖数组，当数组中变量变化时会调用 effect 函数


  // useRef 案例
  // 初始化一个空引用
  const elementRef = useRef(null);

  Taro.nextTick(() => {
    console.log('elementRef.current', elementRef.current); // null
  })


  // useSelector 案例 获取redux中的数据
  const environment = useSelector<any>((state: Record<string, any>) => state.globalData.environment);

  const ComponentOneRef = useRef<any>(null)

  const pressFn = () => {
    console.log('ComponentOneRef.current', ComponentOneRef.current);
  }

  const sensorsFn = () => {
    app.sensors.track('sensorsClick', {
      name: 'sensorsClick',
      age: 18
    })
  }

  const TestFn = () => {
    console.log('parent-TestFn')
  }

  return (
    <View>
      <View>
        <Text ref={elementRef}>demo-{count}</Text>
      </View>
      <View>sensors---click</View>
      <Button onClick={sensorsFn}>sensors数据埋点点击</Button>
      <View>environment---{environment}</View>
      <View>
        <Button onClick={() => setStart(!start)}>{time}</Button>
      </View>
      <View>
        <Button onClick={pressFn}>获取子组件Ref</Button>
      </View>
      <View>子组件</View>
      <ComponentOne
        childRef={ComponentOneRef}
        title='title'
        number={0}
        ParentTestFn={TestFn}
      ></ComponentOne>
    </View>
  )
}

export default DemoPage;
