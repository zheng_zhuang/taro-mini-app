import React from 'react';
import { WKPage } from '@/wxat-common/utils/platform';
import { Block, View, WebView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';

import './index.scss';

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
});

const mapDispatchToProps = (dispatch) => ({});

export default
@hoc
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@WKPage
class H5VZan extends React.Component {
  config = {
    navigationBarTitleText: '直播',
    navigationStyle: 'custom',
  };

  state = {
    h5Tail: '&skipwxxcx=1',
  };

  /**
   * 生命周期函数--监听页面加载
   * **/
  componentDidMount() {
    setTimeout(() => {
      this.setState({ show: true });
    }, 5000);
  }

  onWebViewMessage = (e) => {
    console.log('onWebViewMessage  -=- ', e);
  };

  render() {
    const {
      $global: { $tmpStyle },
    } = this.props;
    const { h5Tail } = this.state;

    return (
      <WebView
        onMessage={this.onWebViewMessage}
        src={'https://wx.vzan.com/live/tvchat-1095646094?v=1608279067833' + h5Tail}
      />
    );
  }
}
