import React from 'react';
import {WKPage,_fixme_with_dataset_} from '@/wxat-common/utils/platform';
import {Block, View, Image, Navigator, Canvas} from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import filters from '../../../../wxat-common/utils/money.wxs.js';
import constants from '../../../../wxat-common/constants/index.js';
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import cdnResConfig from '../../../../wxat-common/constants/cdnResConfig.js';
import shareUtil from '../../../../wxat-common/utils/share.js';
import authHelper from '../../../../wxat-common/utils/auth-helper.js';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import ShareDialog from '../../components/share-dialog/index';
import LoadMore from '../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../wxat-common/components/error/error';
import Empty from '../../../../wxat-common/components/empty/empty';
import {connect} from 'react-redux';

import './index.scss';

const loadMoreStatus = constants.order.loadMoreStatus;

const LIVE_STATUS = [
  {value: 101, label: '正在直播'},
  {value: 102, label: '直播预告'},
  {value: 103, label: '直播回放'},
];

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
});

const mapDispatchToProps = (dispatch) => ({});

@hoc
@connect(mapStateToProps, mapDispatchToProps, undefined, {forwardRef: true})
@WKPage
class LiveList extends React.Component {
  config = {
    navigationBarTitleText: '直播列表',
    usingComponents: {
      subscribe: 'plugin-private://wx2b03c6e691cd7370/components/subscribe/subscribe',
    },
  };

  state = {
    error: false, // 是否显示错误页面
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    liveList: null, // 直播列表
    currentLive: {}, // 当前选择的直播
    pageNo: 1, // 当前列表页面数，用于加载更多
    currentType: LIVE_STATUS[0], // 当前直播列表类型
    liveTabs: LIVE_STATUS, // 列表tab分类
    likeIcon: cdnResConfig.live.like,
    likeCheckedIcon: cdnResConfig.live.likeChecked,
    viewIcon: cdnResConfig.live.view,
    uniqueShareInfoForQrCode: null,
  };

  shareDialogCMPT = React.createRef();

  /**
   * 生命周期函数--监听页面加载
   * **/
  componentDidMount() {
    Taro.hideShareMenu();
    // this.getTemplateStyle();
  }

  componentDidShow() {
    this.onPullDownRefresh(); // 向下刷新
  }

  onShareAppMessage() {
    this.getShareDialog().hide();
    const roomId = this.state.currentLive.roomId;
    const name = this.state.currentLive.name;
    const url = 'sub-packages/live-play/pages/player/index?roomId=' + roomId;
    return {
      title: name || '直播详情',
      imageUrl: this.state.currentLive.shareImg || this.state.currentLive.coverImg || '',
      path: shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.LIVE_LIST,
        bzId: roomId,
        sceneName: name,
        bzName: `直播${name ? '-' + name : ''}`,
      }),
    };
  }

  // 向下刷新
  onPullDownRefresh() {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.fetchLiveList(true);
      }
    );
  }

  // 向上拉伸刷新
  onReachBottom() {
    // 判断是否允许加载更多
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatus.LOADING,
        },

        () => {
          this.fetchLiveList();
        }
      );
    }
  }

  // 加载更多
  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatus.LOADING,
      },

      () => {
        this.fetchLiveList();
      }
    );
  };

  /**
   * 查询直播列表
   * @param {*} fromPullDown 是否为下拉刷新，即是否获取第一页的数据
   */
  fetchLiveList = (fromPullDown) => {
    this.getLiveListRequest()
      .then((res) => {
        // 判断是否为下拉刷新，即是否获取第一页的数据
        if (fromPullDown) {
          Taro.stopPullDownRefresh(); // 停止页面下拉刷新，隐藏页面顶部下来loading框
        }

        let dataList = res.data || [];
        const liveList = this.isLoadMoreRequest() && !fromPullDown ? this.state.liveList.concat(dataList) : dataList;
        const hasMore = liveList.length < res.totalCount ? true : false;
        const pageNo = hasMore ? this.state.pageNo + 1 : this.state.pageNo; // 当前列表页面数+1，用于加载下一页

        this.setState({
          error: false, // 是否显示错误页面
          liveList, // 直播列表
          pageNo, // 当前列表页面数，用于加载更多
          hasMore, // 是否允许加载更多
        });
      })
      .catch((error) => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({
            error: true, // 是否显示错误页面
          });
        }
      });
  };

  /**
   * 获取直播列表
   */
  getLiveListRequest = () => {
    const requestUrl = api.live.list; //列表查询接口

    // 接口请求参数
    const params = {
      pageNo: this.state.pageNo,
      pageSize: 10,
      liveStatus: this.state.currentType.value,
    };

    return wxApi.request({
      url: requestUrl,
      loading: true,
      data: params,
    });
  };

  // 是否为加载更多请求
  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  /**
   * 点击tab切换，获取不同状态直播列表
   * @param e
   */
  handleChangeTab = (e) => {
    const curType = e.currentTarget.dataset.item;
    console.log("curType", e)
    this.setState(
      {
        currentType: curType,
        pageNo: 1,
        hasMore: true,
        liveList: null, // 切换tab，重新查询列表之前，先清空原有的列表
      },
      () => {
        this.fetchLiveList();
      }
    );
  };

  // 处理分享
  handleShare = (e) => {
    if (!authHelper.checkAuth()) return;
    const currentLive = e.currentTarget.dataset.live;

    const uniqueShareInfoForQrCode = {
      roomId: currentLive.roomId,
      page: 'sub-packages/live-play/pages/player/index',
      refBz: shareUtil.ShareBZs.LIVE_LIST,
      refBzId: currentLive.roomId,
      refSceneName: currentLive.name,
      refBzName: `直播${currentLive.name ? '-' + currentLive.name : ''}`,
    };

    this.setState(
      {
        currentLive,
        uniqueShareInfoForQrCode,
      },

      () => {
        setTimeout(() => {
          this.getShareDialog() && this.getShareDialog().show();
        }, 100);
      }
    );
  };

  // 获取分享对话弹框
  getShareDialog = () => {
    return this.shareDialogCMPT.current;
  };

  /**
   * 调用子组件保存海报的方法
   * 因为Canvas必须放在page里，否则无法保存图片，所以只能页面重新调用子组件的方法
   */
  handleCanvas = () => {
    this.getShareDialog().savePosterImage(this);
  };

  // 点赞
  handleLike = async (e) => {
    if (!authHelper.checkAuth()) return;

    const {live, index} = e.currentTarget.dataset;
    const {liveList} = this.state;

    await wxApi.request({
      url: api.live.like,
      loading: true,
      method: 'POST',
      data: {
        likeStatus: !live.hasLiked,
        likeType: 2,
        relationId: live.roomId,
      },
    });

    liveList[index].hasLiked = !live.hasLiked;
    liveList[index].likeCount = !live.hasLiked ? live.likeCount + 1 : live.likeCount - 1;
    this.setState({
      liveList,
    });
  };

  // 订阅
  handleSubscribe = ({currentTarget}) => {
    if (!authHelper.checkAuth()) return;
    const {roomId} = currentTarget.dataset;
    wxApi.request({
      url: api.live.like,
      method: 'POST',
      loading: false,
      quite: true,
      data: {likeStatus: true, likeType: 3, relationId: roomId},
    });
  };

  // 跳转至详情页
  gotoLiveDetail = async ({currentTarget}) => {
    if (!authHelper.checkAuth()) return;

    const {roomId, isReport} = currentTarget.dataset;
    // 判断是否需上报统计，fix原小程序catchTap逻辑
    if (isReport) {
      await wxApi.request({
        url: api.live.watch,
        method: 'POST',
        loading: false,
        quite: true,
        header: {
          'content-type': 'application/x-www-form-urlencoded',
        },

        data: {roomId},
      });
    }
    wxApi.$navigateTo({
      url: 'sub-packages/live-play/pages/player/index',
      data: {roomId},
    });
  };

  reportWatch = ({currentTarget}) => {
    const {roomId} = currentTarget.dataset;

    wxApi.request({
      url: api.live.watch,
      method: 'POST',
      loading: false,
      quite: true,
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },

      data: {roomId},
    });
  };

  render() {
    const {
      $global: {$tmpStyle},
    } = this.props;
    const {
      currentType,
      liveTabs,
      error,
      liveList,
      viewIcon,
      likeCheckedIcon,
      likeIcon,
      loadMoreStatus,
      currentLive,
      uniqueShareInfoForQrCode,
    } = this.state;
    console.log("currentType", currentType)
    return (
      <Block>
        <View className='live-list-container'>
          <View className='status-box'>
            {(liveTabs || []).map((item, index) => {
              return (
                <View
                  onClick={_fixme_with_dataset_(this.handleChangeTab, {item: item})}
                  className={'status-label ' + (item.value === currentType.value ? 'active' : '')}
                  key='index'
                  data-item={item}
                  style={
                    item.value === currentType.value
                      ? 'color:' + $tmpStyle.btnColor + ';border-color:' + $tmpStyle.btnColor
                      : ''
                  }
                >
                  {item.label}
                  {item.value === currentType.value && (
                    <View
                      className='active-bottom-line'
                      style={item.value === currentType.value ? 'background-color:' + $tmpStyle.btnColor : ''}
                    />
                  )}
                </View>
              );
            })}
          </View>
          {/*  错误页面提示  */}
          {!!error ? (
            <Error/>
          ) : !!liveList && liveList.length == 0 ? (
            <Empty message='敬请期待...'/>
          ) : (
            <View className='live-list' hidden={!liveList || !liveList.length}>
              {(liveList || []).map((item, index) => {
                return (
                  <View className='tab' key={index}>
                    <View className='cover-box'>
                      <Image
                        className='cover-img'
                        src={item.coverImg}
                        mode='aspectFill'
                        data-room-id={item.roomId}
                        onClick={this.gotoLiveDetail}
                      />

                      {/*  正在直播-浏览数  */}
                      {currentType.value === 101 ? (
                        <View className='tips'>{'正在直播-观看人数 ' + item.watchLiveCount}</View>
                      ) : currentType.value === 102 ? (
                        <View className='tips'>
                          {'直播预告-播放时间 ' + filters.dateFormat(item.startTime * 1000, 'MM-dd hh:mm')}
                        </View>
                      ) : (
                        currentType.value === 103 && (
                          <View className='tips'>{'直播回放-已播放 ' + item.watchReplayCount}</View>
                        )
                      )}

                      {/*  直播预告-播放时间  */}
                      {/*  直播结束-播放数  */}
                    </View>
                    <View className='info-box'>
                      <View className='title limit-line'>{item.name}</View>
                      {/*  按钮集合  */}
                      <View className='btn-box'>
                        <View style='display: flex;'>
                          <View className='view-count-box'>
                            <Image className='icon view-icon' src={viewIcon} mode='aspectFill'/>
                            {item.browseCount}
                          </View>
                          {/*  点赞数  */}
                          <View
                            className='like-count-box'
                            onClick={this.handleLike}
                            data-index={index}
                            data-live={item}
                          >
                            {item.hasLiked ? (
                              <Image className='icon like-checked-icon' src={likeCheckedIcon} mode='aspectFill'/>
                            ) : (
                              <Image className='icon like-icon' src={likeIcon} mode='aspectFill'/>
                            )}

                            {item.likeCount}
                          </View>
                        </View>
                        <View style={{display: 'flex'}}>
                          <View
                            className='btn share-btn'
                            style={'color:' + $tmpStyle.btnColor + ';border-color:' + $tmpStyle.btnColor}
                            data-live={item}
                            onClick={this.handleShare}
                          >
                            分享
                          </View>
                          {/*  直播结束：回放  */}
                          {currentType.value === 103 ? (
                            <Navigator
                              data-room-id={item.roomId}
                              onClick={this.reportWatch}
                              url={
                                'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=' + item.roomId
                              }
                              className='btn btn-light'
                              style={'border-color:' + $tmpStyle.btnColor + ';background-color:' + $tmpStyle.btnColor}
                            >
                              回放
                            </Navigator>
                          ) : currentType.value === 101 ? (
                            <Navigator
                              data-room-id={item.roomId}
                              data-is-report={true}
                              url={
                                'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=' + item.roomId
                              }
                              className='btn btn-light'
                              style={'border-color:' + $tmpStyle.btnColor + ';background-color:' + $tmpStyle.btnColor}
                              onClick={this.gotoLiveDetail}
                            >
                              加入
                            </Navigator>
                          ) : (
                            currentType.value === 102 && (
                              <Block>
                                <subscribe
                                  roomId={item.roomId}
                                  onClick={this.handleSubscribe}
                                  data-room-id={item.roomId}
                                  color='rgba(255,255,255,0)'
                                  width={100}
                                  height={100}
                                  backgroundColor={$tmpStyle.btnColor}
                                />
                              </Block>
                            )
                          )}

                          {/*  直播中：加入  */}
                          {/*  直播预告：订阅  */}
                        </View>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          )}

          {/*  直播列表  */}
          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}/>
        </View>
        {/*  分享对话弹框  */}
        {currentLive && (
          <ShareDialog
            childRef={this.shareDialogCMPT}
            posterTips='给你分享了一场直播'
            posterName={currentLive.name}
            posterImage={currentLive.shareImg}
            uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
            onSave={this.handleCanvas}
          />
        )}

        {/*  shareCanvas必须放在page里，否则无法保存图片  */}
        <Canvas canvasId='shareCanvas' className='share-canvas'/>
        <HomeActivityDialog showPage="sub-packages/live-play/pages/live-list/index"></HomeActivityDialog>
      </Block>
    );
  }
}

export default LiveList;
