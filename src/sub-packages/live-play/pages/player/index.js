import React from 'react';
import { WKPage } from '@/wxat-common/utils/platform';
import { Block, View, Image, Navigator, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';

import filters from '../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import authHelper from '../../../../wxat-common/utils/auth-helper.js';
import api from '../../../../wxat-common/api';
import cdnResConfig from '../../../../wxat-common/constants/cdnResConfig.js';
import shareUtil from '../../../../wxat-common/utils/share.js';

import ShareDialog from '../../components/share-dialog';
import ShopGuide from '../../components/shop-guide';
import DetailParser from '../../../../wxat-common/components/detail-parser';
import { connect } from 'react-redux';
import { SkCodeComponent } from '@/decorators/SkCode';

import './index.scss';
import { qs2obj } from '../../../../wxat-common/utils/query-string.ts';

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
  tabbars: state.globalData.tabbars,
  sessionId: state.base.sessionId,
});

const mapDispatchToProps = (dispatch) => ({});

@hoc
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@WKPage
class Player extends SkCodeComponent {
  config = {
    navigationBarTitleText: '直播详情',
    usingComponents: {
      subscribe: 'plugin-private://wx2b03c6e691cd7370/components/subscribe/subscribe',
    },
  };

  state = {
    roomId: null,
    liveDetail: {},
    uniqueShareInfoForQrCode: null,
    likeIcon: cdnResConfig.live.likeLight,
    likeCheckedIcon: cdnResConfig.live.likeLightChecked,
    watchList: [],
    watchListTotal: 0,
    STATUS_MAP: { 101: '正在直播', 102: '直播预告', 103: '直播回放' },
    guideData: null,
    options: null,
  };

  // 轮询进入直播间的用户列表
  timer = null;

  // 滚动
  loopTimer = null;

  shareDialogCMPT = React.createRef();

  fld = null;

  async onWait() {
    super.onWait();
    let fld = this.formatLaunchData(this.$sk);
    if (this.$sk.scene) {
      fld = qs2obj(decodeURIComponent(this.$sk.scene));
    }
    await this.init(fld);
    this.fld = fld;
  }

  componentDidShow() {
    if (this.fld) {
      this.init(this.fld);
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
    }
    if (this.loopTimer) {
      clearInterval(this.loopTimer);
    }
  }

  onShareAppMessage() {
    const roomId = this.state.roomId;
    const name = this.state.liveDetail.name || '';
    const url = 'sub-packages/live-play/pages/player/index?roomId=' + roomId;
    return {
      title: name || '直播详情',
      imageUrl: this.state.liveDetail.shareImg || this.state.liveDetail.coverImg || '',
      path: shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.LIVE_PLAYER,
        bzId: roomId,
        sceneName: name,
        bzName: `直播${name ? '-' + name : ''}`,
      }),
    };
  }

  init = async (options) => {
    // 未授权，保存 onShow
    if (!authHelper.checkAuth()) {
      this.setState({ options });
      return;
    }

    let { roomId, refUseId } = options;

    // 绑定导购关系
    if (refUseId) {
      const data = { shareUserId: refUseId };
      await wxApi.request({
        url: api.live.bindGuide,
        loading: false,
        quite: true,
        data,
      });
    }

    roomId = +roomId;
    if (Number.isNaN(roomId)) {
      wxApi.showToast({ icon: 'none', title: '无效的直播间' });
      return;
    }

    this.setState(
      {
        roomId,
      },

      () => {
        this.fetchLiveDetail();

        this.getWatchList();
        this.queryShopGuide();

        if (this.timer) {
          clearInterval(this.timer);
        }
        if (this.loopTimer) {
          clearInterval(this.loopTimer);
        }
        this.startLoop();
        this.timer = setInterval(this.getWatchList.bind(this), 1000 * 10);
      }
    );
  };

  // 判断是否二维码分享进入
  isQrCodeMapperLaunchEnter(options) {
    if (!options || !options.scene) return false;
    return !!shareUtil.MapperQrCode.getMapperKey(options.scene);
  }

  // 从邮箱中拿启动参数
  formatLaunchData(shareObj) {
    return shareObj
      ? {
          roomId: shareObj.roomId,
          refUseId: shareObj._ref_useId,
        }
      : null;
  }

  fetchLiveDetail = async () => {
    const { roomId } = this.state;
    const { data } = await wxApi.request({
      url: api.live.detail,
      loading: true,
      data: { roomId },
    });

    this.setState({
      liveDetail: {
        ...data,
        itemInfoDTOList: data.itemInfoDTOList || [],
      },
    });
  };

  getWatchList = async () => {
    const { watchList } = this.state;
    if (!watchList && !watchList.length) {
      return;
    }

    const { roomId } = this.state;

    let {
      data: { liveUserBrowseRecordDTOList, totalBrowseCount },
    } = await wxApi.request({
      url: api.live.watchList,
      loading: false,
      quite: true,
      data: { roomId, limit: 100 },
    });

    // 去重
    liveUserBrowseRecordDTOList = (liveUserBrowseRecordDTOList || []).reduce((pre, cur) => {
      if (pre.findIndex((i) => i.userId === cur.userId) >= 0) {
        return pre;
      }
      return [...pre, cur];
    }, []);

    this.setState({
      watchList: liveUserBrowseRecordDTOList || [],
      watchListTotal: totalBrowseCount,
    });
  };

  // 处理分享
  handleShare = (e) => {
    const uniqueShareInfoForQrCode = {
      roomId: this.state.roomId,
      page: 'sub-packages/live-play/pages/player/index',
      refBz: shareUtil.ShareBZs.LIVE_PLAYER,
      refBzId: this.state.roomId,
      refSceneName: this.state.liveDetail.name,
      refBzName: `直播${this.state.liveDetail.name ? '-' + this.state.liveDetail.name : ''}`,
    };

    this.setState(
      {
        uniqueShareInfoForQrCode,
      },

      () => {
        setTimeout(() => {
          this.getShareDialog() && this.getShareDialog().show();
        }, 100);
      }
    );
  };

  // 获取分享对话弹框
  getShareDialog = () => {
    return this.shareDialogCMPT.current;
  };

  /**
   * 调用子组件保存海报的方法
   * 因为Canvas必须放在page里，否则无法保存图片，所以只能页面重新调用子组件的方法
   */
  handleCanvas = () => {
    this.getShareDialog().savePosterImage(this);
  };

  /**
   * 进入直播间上报
   */
  reportWatch = () => {
    wxApi.request({
      url: api.live.watch,
      method: 'POST',
      loading: false,
      quite: true,
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },

      data: { roomId: this.state.roomId },
    });
  };

  // 查询导购信息
  queryShopGuide = async () => {
    const { data } = await wxApi.request({
      url: api.live.queryGuide,
      loading: false,
    });

    this.setState({ guideData: data });
  };

  handleToggleLike = async () => {
    const { liveDetail } = this.state;

    await wxApi.request({
      url: api.live.like,
      loading: true,
      method: 'POST',
      data: {
        likeStatus: !liveDetail.hasLiked,
        likeType: 2,
        relationId: liveDetail.roomId,
      },
    });

    liveDetail.hasLiked = !liveDetail.hasLiked;
    liveDetail.likeCount = !liveDetail.likeCount ? liveDetail.likeCount + 1 : liveDetail.likeCount - 1;
    this.setState({
      liveDetail,
    });
  };

  handleSubscribe = async () => {
    const { roomId } = this.state.liveDetail;
    await wxApi.request({
      url: api.live.like,
      method: 'POST',
      loading: false,
      quite: true,
      data: { likeStatus: true, likeType: 3, relationId: roomId },
    });

    this.fetchLiveDetail();
  };

  startLoop = async () => {
    this.loopTimer = setInterval(() => {
      const { watchList } = this.state;

      if (!watchList && !watchList.length) return;

      this.setState({
        watchList: watchList.slice(2),
      });
    }, 4000);
  };

  render() {
    const {
      $global: { $tmpStyle },
    } = this.props;
    const {
      liveDetail,
      watchList,
      likeCheckedIcon,
      likeIcon,
      STATUS_MAP,
      watchListTotal,
      guideData,
      uniqueShareInfoForQrCode,
    } = this.state;
    return (
      <Block>
        <View className='live-detail-container'>
          <View className='poster-box'>
            <Image className='poster-img' src={liveDetail.coverImg} mode='aspectFill'></Image>
            {/*  ***用户进入  */}
            <View className='user-info-box'>
              {watchList[0] && (
                <View className='user-item loop-top'>
                  <Image className='head-img' src={watchList[0].userAvatar} mode='aspectFit'></Image>
                  <View className='user'>{watchList[0].userName + '进入了直播间'}</View>
                </View>
              )}

              {watchList[1] && (
                <View className='user-item loop'>
                  <Image className='head-img' src={watchList[1].userAvatar} mode='aspectFit'></Image>
                  <View className='user'>{watchList[1].userName + '进入了直播间'}</View>
                </View>
              )}
            </View>
          </View>
          <View className='content-container'>
            <View className='live-info-box'>
              <View className='live-info-header'>
                <View>
                  <View className='live-title limit-line'>{liveDetail.name}</View>
                </View>
                <View className='like-count-box' onTap={this.handleToggleLike}>
                  {liveDetail.hasLiked ? (
                    <Image className='icon like-checked-icon' src={likeCheckedIcon} mode='aspectFill'></Image>
                  ) : (
                    <Image className='icon like-icon' src={likeIcon} mode='aspectFill'></Image>
                  )}

                  {liveDetail.likeCount}
                </View>
              </View>
              <View className='live-info-detail'>
                <View className='live-info-detail-left'>
                  <View>{'直播状态：' + STATUS_MAP[liveDetail.liveStatus]}</View>
                  <View className='info-label'>
                    {'开始时间：' + filters.dateFormat(liveDetail.startTime * 1000, 'MM月dd日 hh:mm')}
                  </View>
                </View>
                {/*  正在直播  */}
                {liveDetail.liveStatus === 101 && (
                  <View className='live-info-detail-right'>
                    <Navigator
                      onClick={this.reportWatch}
                      url={'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=' + liveDetail.roomId}
                    >
                      <View className='live-info-btn'>进入直播</View>
                    </Navigator>
                    <View className='info-label'>{'已有' + watchListTotal + '人进入直播间'}</View>
                  </View>
                )}

                {/*  直播预告  */}
                {liveDetail.liveStatus === 102 && (
                  <View className='live-info-detail-right'>
                    <View className='live-info-btn'>
                      <subscribe onClick={this.handleSubscribe} roomId={liveDetail.roomId}></subscribe>
                    </View>
                    <View className='info-label'>{'已有' + liveDetail.subscribeCount + '人订阅直播'}</View>
                  </View>
                )}

                {/*  直播回放  */}
                {liveDetail.liveStatus === 103 && (
                  <View className='live-info-detail-right'>
                    <Navigator
                      onClick={this.reportWatch}
                      url={'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=' + liveDetail.roomId}
                    >
                      <View className='live-info-btn'>观看回放</View>
                    </Navigator>
                    <View className='info-label'>{'已有' + watchListTotal + '人观看该直播'}</View>
                  </View>
                )}
              </View>
            </View>
            {/*  活动商品  */}
            {!!liveDetail && !!liveDetail.itemInfoDTOList && liveDetail.itemInfoDTOList.length && (
              <View className='goods-box'>
                <View className='box-title'>活动商品</View>
                <View className='goods-wrapper'>
                  {liveDetail.itemInfoDTOList.map((item, index) => {
                    return (
                      <Navigator url={'/' + item.url} className='goods-item'>
                        <Image className='goods-img' src={item.coverImgUrl} mode='aspectFill'></Image>
                        <View className='goods-name limit-line line-2'>{item.goodsName}</View>
                        {item.priceType === 1 && (
                          <View className='goods-price'>{'￥' + filters.moneyFilter(item.price, true)}</View>
                        )}

                        {item.priceType === 2 && (
                          <View className='goods-price  limit-line line-2'>
                            {'￥' +
                              filters.moneyFilter(item.price, true) +
                              ' - ' +
                              filters.moneyFilter(item.price2, true)}
                          </View>
                        )}

                        {item.priceType === 3 && (
                          <View className='goods-price'>
                            {'￥' + filters.moneyFilter(item.price2, true)}
                            <View className='del'>{'￥' + filters.moneyFilter(item.price, true)}</View>
                          </View>
                        )}
                      </Navigator>
                    );
                  })}
                </View>
              </View>
            )}

            {/*  活动介绍  */}
            {!!liveDetail && !!liveDetail.activityContext && (
              <View className='detail-parse-box'>
                <View className='box-title'>活动介绍</View>
                <View className='detail-parse-content'>
                  <DetailParser
                    itemDescribe={liveDetail.activityContext}
                    showHeadline={false}
                    showHeader={false}
                  ></DetailParser>
                </View>
              </View>
            )}

            {/*  导购  */}
            {guideData && <ShopGuide source={guideData}></ShopGuide>}
            {/*  分享  */}
            <View className='bottom-box'>
              <View className='share-btn-now' onClick={this.handleShare}>
                立即分享
              </View>
            </View>
          </View>
        </View>
        {/*  分享对话弹框  */}
        {liveDetail && (
          <ShareDialog
            childRef={this.shareDialogCMPT}
            posterTips='给你分享了一场直播'
            posterName={liveDetail.name}
            posterImage={liveDetail.shareImg}
            uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
            onSave={this.handleCanvas}
          ></ShareDialog>
        )}

        {/*  shareCanvas必须放在page里，否则无法保存图片  */}
        <Canvas canvasId='shareCanvas' className='share-canvas'></Canvas>
      </Block>
    );
  }
}

export default Player;
