import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../../wxat-common/utils/wxApi';

import './index.scss';

// props的类型定义
type IProps = {
  source?: object | null;
};

let ShopGuide: FC<IProps> = ({ source }) => {
  const contact = () => {
    if (source && source.mobile) {
      wxApi.makePhoneCall({
        phoneNumber: source.mobile,
      });
    }
  };

  return (
    <View className='shopGuide'>
      <Block>
        <View className='business-card'>
          <View className='business-card-content'>
            <Image src={source.avatar ? source.avatar : '/images/noAvatar.png'} className='picture'></Image>
            <View className='content'>
              <View className='name'>{source.name}</View>
              <Text className='position'>{source.position}</Text>
            </View>
          </View>
          <View>
            <Image className='contact' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/sg-contact.png" onClick={contact}></Image>
            <Button className='image-btn' openType='contact'>
              <Image className='message' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/sg-message.png"></Image>
            </Button>
          </View>
        </View>
      </Block>
    </View>
  );
};

ShopGuide.defaultProps = {
  source: null,
};

export default ShopGuide;
