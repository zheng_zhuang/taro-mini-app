import React, { FC, useState, useImperativeHandle, useRef } from 'react'; // import { WKPage, _safe_style_ } from '@/wxat-common/utils/platform';
import '@/wxat-common/utils/platform';
import { View, Image, Button, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';

import authHelper from '../../../../wxat-common/utils/auth-helper.js';
import api from "../../../../wxat-common/api";
import wxApi from '../../../../wxat-common/utils/wxApi';
import canvasHelper from '../../../../wxat-common/utils/canvas-helper.js';
import shareUtil from '../../../../wxat-common/utils/share.js';

import AnimatDialog from "../../../../wxat-common/components/animat-dialog";
import AuthUser from '../../../../wxat-common/components/auth-user';

import './index.scss';

import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'
// props的类型定义
interface IProps {
  posterData?: Record<string, any> | null;
  visible?: boolean;
  /**
   * 海报的二维码请求参数：
   * verificationNo: itemNo,
   * verificationType: 5,
   * maPath: 'wxat-common/pages/goods-detail/index'
   */
  qrCodeParams: Record<string, any> | null;
  /**
   * 是否为新的二维码接口，新的二维码接口请求结构有变
   * 接口变更为/auth/vip/verification/getNewQRCode
   * 直接传参sence和maPath，获取时也是直接获取
   */
  isNewQrCode?: boolean;
  posterType?: string;
  /**
   * 海报上面的提示语
   */
  posterTips: string;
  /**
   * 海报主图
   */
  posterImage?: string;
  /**
   * 海报图下面的名字
   */
  posterName?: string;
  onSave: () => any;
  childRef?: any;
  /**
   * 二维码生成方式，使用映射ID的方式创建
   */
  uniqueShareInfoForQrCode?: object | null;
}

const ShareDialog: FC<IProps> = ({
  posterData,
  visible,
  isNewQrCode,
  qrCodeParams,
  posterType,
  posterTips,
  posterImage,
  posterName,
  onSave,
  childRef,
  uniqueShareInfoForQrCode,
}) => {
  const [posterDialogVisible, setPosterDialogVisible] = useState(false);
  const [qrCodeUrl, setQrCodeUrl] = useState('');
  // const posterSaveImageDataRef = useRef();
  const [user, setUser] = useState({});
  const wxUserInfo = useSelector((state) => state.base.wxUserInfo);

  const shareDialogRef = useRef<any>(null);

  // 需要暴露给外面使用的函数
  useImperativeHandle(childRef, () => ({
    hide,
    show,
    savePosterImage,
  }));

  // 创建海报
  const handleShowPosterDialog = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    // getShareDialog().hide();

    setPosterDialogVisible(false);
    setQrCodeUrl('');

    if (uniqueShareInfoForQrCode) {
      apiQRCodeByUniqueKey();
    }
  };

  const apiQRCodeByUniqueKey = () => {
    const publicArguments = shareUtil.buildSharePublicArguments({ bz: '', forServer: true });
    const scene = Object.assign({}, publicArguments, uniqueShareInfoForQrCode);
    wxApi
      .request({
        url: api.generatorMapperQrCode,
        loading: true,
        data: {
          shareParam: JSON.stringify(scene),
          urlPath: scene.page,
        },
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data);
      });
  };

  /**
   * 保存海报图片到本地
   */
  const handleSaveImage = (e) => {
    onSave && onSave();
  };
  /**
   * 直接关闭分享弹窗
   */
  const handleCloseShare = () => {
    hide();
  };
  const hide = () => {
    getShareDialog() && getShareDialog().hide(true);
  };
  const show = () => {
    getShareDialog() &&
      getShareDialog().show({
        scale: 1,
      });

    // 创建海报
    handleShowPosterDialog();
  };

  const savePosterImage = (context) => {
    saveGoodsPosterImage(context);
  };

  const saveGoodsPosterImage = (context) => {
    wxApi.showLoading({
      title: '正在保存图片...',
    });

    // const { user } = posterSaveImageDataRef.current;
    const canvasContext = wxApi.createCanvasContext('shareCanvas', context);
    const imageRequests = [
      wxApi.getImageInfo({
        src: user.headUrl,
      }),

      wxApi.getImageInfo({
        src: posterImage.replace(/^http/, 'https'),
      }),
    ];

    if (qrCodeUrl) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: qrCodeUrl,
        })
      );
    }

    Promise.all(imageRequests)
      .then((res) => {
        // 背景
        canvasHelper.drawFillRect(canvasContext, 0, 0, 260, 400, '#fff');
        // 头像
        canvasHelper.drawCircleImage(canvasContext, 42.5, 32.5, 20, res[0].path);

        // 用户名
        canvasHelper.drawText(canvasContext, user.name, 70, 26, {
          textAlign: 'start',
          fillStyle: '#000000',
          fontSize: 14,
        });

        // 红色三角
        canvasHelper.drawFillLine(
          canvasContext,
          [
            {
              x: 70,
              // y: 32
              y: 42,
            },

            {
              x: 74,
              // y: 30
              y: 40,
            },

            {
              x: 74,
              // y: 34
              y: 44,
            },
          ],

          '#f35f28'
        );

        // 红色方框
        canvasHelper.drawFillRect(canvasContext, 74, 32, 140, 20, '#f35f28');

        // 详情文案
        canvasHelper.drawText(canvasContext, posterTips, 80, 47, {
          textAlign: 'start',
          fillStyle: '#ffffff',
          fontSize: 13,
        });

        // 主图
        canvasHelper.drawImage(canvasContext, 22.5, 68, 215, 215, res[1].path);

        // 标题
        canvasHelper.drawText(canvasContext, posterName, 22.5, 320, {
          textAlign: 'start',
          fillStyle: '#373A44',
          fontSize: 14,
          maxWidth: 140,
        });

        // 二维码
        if (qrCodeUrl && res[2]) {
          canvasHelper.drawImage(canvasContext, 174, 290, 69, 69, res[2].path);
        }

        canvasContext.draw(false, () => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'shareCanvas',
              },

              context
            )
            .then((res) => {
              return wxApi.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
              });
            })
            .then(() => {
              wxApi.showToast({
                title: '已保存到相册',
              });
            })
            .catch((error) => {
              console.log('saveImage error: ', error);
              wxApi.showToast({
                title: '保存失败',
                icon: 'none',
              });
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      })
      .catch(() => {
        wxApi.hideLoading();
      });
  };

  // 获取分享对话弹框
  const getShareDialog = () => {
    return shareDialogRef.current;
  };

  const userInfoReady = () => {
    setUser({
      name: wxUserInfo ? wxUserInfo.nickName : '',
      headUrl: wxUserInfo ? wxUserInfo.avatarUrl : '',
    });
  };

  return (
    <AnimatDialog ref={shareDialogRef} animClass='share-dialog'>
      {posterDialogVisible && (
        <AuthUser onReady={userInfoReady}>
          <View className='post-container'>
            <View className='posters-box'>
              <View className='posters-nick-header'>
                <Image className='posters-head' src={user.headUrl}></Image>
                <View className='posters-right'>
                  <View className='name'>{user.name}</View>
                  <View className='posters-notes'>{posterTips}</View>
                </View>
              </View>
              {/* 图片 */}
              <Image className='posters-img' src={posterImage}></Image>
              <View className='posters-detail'>
                <View className='live-name'>
                  <View className='limit-line line-2'>{posterName}</View>
                </View>
                <Image className='qr-code' src={qrCodeUrl} mode='aspectFit'></Image>
              </View>
            </View>
          </View>
        </AuthUser>
      )}

      <View className='bottom-container'>
        <View className='share-item'>
          <Button className='share-btn' onClick={handleSaveImage}>
            <View className='image-block'>
              <Image className='share-img' src={getStaticImgUrl.group.poster_png}></Image>
            </View>
            <View>保存图片</View>
          </Button>
        </View>
        <View className='share-item'>
          <Button openType='share' className='share-btn'>
            <View className='image-block'>
              <Image className='share-img' src={getStaticImgUrl.group.share_png}></Image>
            </View>
            <Text>分享给朋友</Text>
          </Button>
        </View>
        <Button className='close' onClick={handleCloseShare}>
          取消
        </Button>
      </View>
    </AnimatDialog>
  );
};

ShareDialog.defaultProps = {
  visible: false,
  posterData: null,
  qrCodeParams: null,
  isNewQrCode: false,
  posterTips: '给你分享了一场直播',
  posterImage: '',
  posterName: '',
  uniqueShareInfoForQrCode: null,
};

export default ShareDialog;
