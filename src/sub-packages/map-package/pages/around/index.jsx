import React from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Map, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import protectedMailBox from '../../../../wxat-common/utils/protectedMailBox';
import QQMapWX from '../../plugins/qqmap-wx-jssdk.min'; // 引入wx-jssdk
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const commonImg = cdnResConfig.common;

const QQMapSecretKey = 'VVPBZ-MIQ6F-QU4JF-NN543-DM4GH-DJBZF'; // 腾讯地图秘钥
const qqmapsdk = new QQMapWX({
  key: QQMapSecretKey,
});

@hoc
class Around extends React.Component {
  state = {
    mapInfo: null, // 当前项目地点在地图的信息
    // 周边功能类型列表
    aroundTypeList: [
      {
        keyword: '交通',
        list: [],
      },

      {
        keyword: '学校',
        list: [],
      },

      {
        keyword: '医疗',
        list: [],
      },

      {
        keyword: '生活',
        list: [],
      },
    ],

    currentIndex: 0, // 当前显示的周边功能nav位置，初始值默认为第0个
    aroundList: [], // 显示的周边某种功能地点列表

    point: {}, // 地图中心点
    markers: [], // 地图上的标记点列表
    locMarker: [], // 当前项目地点的标记点
    currentLocation: null, // 当前选中的地点
  };

  /**
   * 参数 aroundInfo demo:
   * aroundInfo = {
   *  // 当前项目地点在地图的信息
   *  mapInfo: {
   *    latitude: "22.26666",
   *    longitude: "113.54342",
   *    addressName: "66路—唐家[公交站]",
   *    address: '唐家',
   *  },
   *  aroundType: ["交通", "学校", "医疗", "生活"] // 周边功能类型列表
   * };
   */
  componentDidMount() {
    const aroundInfo = protectedMailBox.read('aroundInfo') || null;
    this.formatData(aroundInfo || null); // 格式化地图数据
  }

  /**
   * 格式化地图数据
   * 格式化父级页面传过来的参数为本页面所需的格式
   * @param {*} aroundInfo
   */
  formatData(aroundInfo) {
    const mapInfo = aroundInfo.mapInfo || null; // 当前项目地点在地图的信息
    let point = null; // 为地图中心点地点的经纬度
    let marker = null; // 地图上的标记点
    let currentLocation = null; // 当前选中的地点
    if (mapInfo) {
      // 为地图中心点地点的经纬度
      point = {
        lat: mapInfo.latitude ? Number(mapInfo.latitude) : null,
        lng: mapInfo.longitude ? Number(mapInfo.longitude) : null,
      };

      // 地图上的标记点
      marker = {
        iconPath: "https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-location.png",
        id: 0,
        latitude: point.lat,
        longitude: point.lng,
        title: mapInfo.addressName || null,
        width: 30,
        height: 30,
        callout: {
          content: mapInfo.addressName || null,
          color: '#333',
          fontSize: 12,
          padding: 6,
          display: 'ALWAYS',
          textAlign: 'center',
          borderRadius: 20,
        },

        // 当前选中的地点
      };
      currentLocation = {
        latitude: point.lat,
        longitude: point.lng,
        name: mapInfo.addressName || null,
        address: mapInfo.address || null,
        scale: 18,
      };
    }

    // 将父级页面传过来的周边功能类型列表转化为本页面适用的数据格式
    const aroundType = aroundInfo.aroundType || [];
    const aroundTypeList = [];
    aroundType.forEach((item) => {
      aroundTypeList.push({
        keyword: item,
        list: [],
      });
    });

    this.setState(
      {
        mapInfo, // 当前项目地点在地图的信息
        aroundTypeList: aroundTypeList.length > 0 ? aroundTypeList : this.state.aroundTypeList, // 周边功能类型列表
        point: point, // 地图中心点
        markers: [marker], // 地图上的标记点列表
        locMarker: marker, // 当前项目地点的标记点
        currentLocation, // 当前选中的地点
      },
      () => {
        // 获取周边
        this.state.aroundTypeList.forEach((item, index) => {
          this.getAround(index);
        });
      }
    );
  }

  /**
   * 获取周边
   * @param {*} index
   */
  getAround(index) {
    const { point, aroundTypeList } = this.state;
    const location = point.lat + ',' + point.lng;
    // 搜索周边方法
    qqmapsdk.search({
      keyword: aroundTypeList[index].keyword,
      location: location,
      success: (res) => {
        const aroundList = res.data || [];

        // 周边功能点与楼盘的之间的距离转换
        aroundList.forEach((item) => {
          const _distance = item._distance + '';

          if (_distance > 1000) {
            const num = _distance / 1000 + '';
            item.distance = num.substring(0, num.indexOf('.') + 2) + 'km';
          } else {
            item.distance = (_distance.indexOf('.') != -1 ? _distance.split('.')[0] : _distance) + 'm';
          }
        });
        aroundTypeList[index].list = aroundList; // 当前选中的周边功能点类型列表

        this.initRenderMap(aroundList); // 渲染地图
        this.setState({
          aroundTypeList, // 全局赋值所有的周边功能点类型数据
          aroundList: aroundTypeList[0].list, // 全局赋值当前选中显示的周边某种功能地点列表，初始值默认为第0个
        });
      },
      fail: (res) => {},
    });
  }

  /**
   * 渲染地图
   * @param {*} aroundList 显示的周边某种功能地点列表
   */
  initRenderMap(aroundList) {
    const markers = []; // 地图上的标注点列表，重新赋值markers，为了只显示当前周边的气泡地图
    let num = 1;
    for (const item of aroundList) {
      markers.push({
        iconPath: "https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-location.png",
        id: num++,
        latitude: item.location.lat,
        longitude: item.location.lng,
        title: item.title,
        width: 1,
        height: 1,
        callout: {
          content: item.title,
          color: '#333',
          fontSize: 12,
          padding: 6,
          display: 'ALWAYS',
          textAlign: 'center',
          borderRadius: 20,
        },
      });
    }
    markers.push(this.state.locMarker); // 重新添加当前项目地点的marker

    this.setState({
      markers, // 地图上的标注点列表
    });
  }

  /**
   * 切换功能导航nav
   * @param {*} e
   */
  handleSwitchNav = (e) => {
    const index = e.currentTarget.dataset.index;
    const aroundList = this.state.aroundTypeList[index].list;

    this.initRenderMap(aroundList); // 渲染地图
    this.setState({
      currentIndex: index, // 当前显示的nav位置
      aroundList, // 显示的周边某种功能地点列表
    });
  };

  /**
   * 点击某一个周边功能地点
   * @param {*} e
   */
  handleLocation = (e) => {
    const aroundItem = e.currentTarget.dataset.item || null;
    const { aroundList } = this.state;

    aroundList.forEach((item) => {
      // 判断是否点击了某一个功能点，是则设置该功能点的经纬度为地图中心的经纬度
      if (aroundItem && aroundItem.id === item.id) {
        const newPoint = item.location;

        // 当前选中的地点
        const currentLocation = {
          latitude: item.location.lat,
          longitude: item.location.lng,
          name: item.title,
          address: item.address,
          scale: 18,
        };

        this.setState({
          point: newPoint, // 地图中心点
          currentLocation, // 当前选中的地点
        });
      }
    });
  };

  /**
   * 点击某一个标记点对应的气泡
   * @param {*} e
   */
  handleCallouttap(e) {
    const markerId = e.markerId || e.markerId === 0 ? e.markerId : null; // 点击气泡的markerId，对应markers列表中的某一个标记点的id

    const { markers } = this.state;

    markers.forEach((item) => {
      // 判断是否点击了某一个功能点，是则设置该功能点的经纬度为地图中心的经纬度
      if ((markerId || markerId === 0) && markerId === item.id) {
        const newPoint = {
          lat: item.latitude,
          lng: item.longitude,

          // 当前选中的地点
        };
        const currentLocation = {
          latitude: newPoint.lat,
          longitude: newPoint.lng,
          name: item.title,
          address: item.address,
          scale: 18,
        };

        this.setState({
          point: newPoint, // 地图中心点
          currentLocation, // 当前选中的地点
        });
      }
    });
  }

  /**
   * 打开腾讯地图
   */
  handleOpenLocation = () => {
    const currentLocation = this.state.currentLocation || null;
    if (currentLocation) {
      wxApi.openLocation(currentLocation);
    }
  };

  render() {
    const { currentLocation, point, markers, currentIndex, aroundTypeList, aroundList } = this.state;
    return (
      <View data-scoped='wk-mpa-Around' className='wk-mpa-Around around'>
        <View className='fixed-box'>
          <View className='top' onClick={this.handleOpenLocation}>
            <Image className='icon-location' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-location.png"></Image>
            <View className='location-title'>{currentLocation.name}</View>
            <Image className='icon-map' src={commonImg.iconMap}></Image>
          </View>
          {/*  地图  */}
          <Map
            id='map'
            className='map'
            longitude={point.lng}
            latitude={point.lat}
            scale='16'
            markers={markers}
            onCallouttap={this.handleCallouttap.bind(this)}
            onMarkertap={this.handleCallouttap.bind(this)}
          ></Map>
          {/*  周边功能信息切换tab  */}
          <View className='nav-box'>
            {aroundTypeList.map((item, index) => {
              return (
                <View
                  key={index}
                  className={'nav ' + (currentIndex == index ? 'active' : '')}
                  onClick={_fixme_with_dataset_(this.handleSwitchNav, { index: index })}
                >
                  {item.keyword + '(' + (item.list.length || 0) + ')'}
                </View>
              );
            })}
          </View>
        </View>
        {/*  周边功能信息列表  */}
        <View className='tab-box'>
          {aroundList.map((item, index) => {
            return (
              <View className='tab' key={item.id} onClick={_fixme_with_dataset_(this.handleLocation, { item: item })}>
                <View className='tab-left'>
                  <View className='tab-title'>{item.title}</View>
                  <View className='tab-text'>{item.address}</View>
                </View>
                <View className='tab-right'>
                  <Image className='icon-distance' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-distance.png"></Image>
                  <Text className='distance'>{item.distance}</Text>
                </View>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}

export default Around;
