import React, { FC, useState, useEffect } from 'react';
import Taro, { useShareAppMessage, useRouter, useDidShow } from '@tarojs/taro';
import '@/wxat-common/utils/platform';
import { View, Video, WebView, CoverImage, CoverView } from '@tarojs/components';
import { useSelector } from 'react-redux';
import useTemplateStyle from '@/hooks/useTemplateStyle';

import api from '@/wxat-common/api/index';
import shareUtil from '@/wxat-common/utils/share';
import authHelper from '@/wxat-common/utils/auth-helper';
import checkOptions from '@/wxat-common/utils/check-options';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';
import Error from '@/wxat-common/components/error/error';

const VIDEO_TYPE = 3;
const PDF_TYPE = 5;
const POSTER_TYPE = {
  [VIDEO_TYPE]: '视频',
  [PDF_TYPE]: 'PDF',
};

const BZ_TYPE = {
  [VIDEO_TYPE]: 'VIDEO',
  [PDF_TYPE]: 'PDF',
};

let Poster: FC = () => {
  const [posterDetail, setPosterDetail] = useState<any>({});
  const [posterType, setPosterType] = useState<any>(0);
  const [materialId, setMaterialId] = useState<any>(0);
  const [showTip, setShowTip] = useState<string | boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const { sessionId, currentStore, appId, appInfo } = useSelector((state) => state.base);
  const { params } = useRouter();
  useTemplateStyle({ autoSetNavigationBar: true });

  //获取是否了解分享操作提示
  const getKnown = () => {
    const value = wxApi.getStorageSync('knowShareWay');
    if (value === '') {
      wxApi.setStorageSync('knowShareWay', false);
    }
    setShowTip(!value);
  };

  //更新是否了解分享操作提示
  const updateKnown = () => {
    wxApi.setStorageSync('knowShareWay', true);
    setShowTip(false);
  };

  const getData = async (materialId) => {
    try {
      const { data } = await wxApi.request({
        url: api.posterConfig,
        loading: false,
        data: {
          id: materialId,
          appId,
        },
      });

      if (!data) {
        throw new Error();
      }
      const { type, id } = data;
      setMaterialId(id);
      setPosterType(type);
      setPosterDetail(data);
    } catch (error) {
      setError(true);
    } finally {
      if (params && !!params.fromGuide) {
        getKnown();
      }
      wxApi.setNavigationBarTitle({
        title: `${POSTER_TYPE[posterType] || '详情'}`,
      });
    }
  };

  useShareAppMessage(() => {
    updateKnown();
    let path = `sub-packages/guide-package/pages/poster/index?id=${materialId}`;
    if (params && !!params.fromGuide) {
      path = shareUtil.buildShareUrlPublicArguments({
        url: `sub-packages/guide-package/pages/poster/index?id=${materialId}`,
        bz: shareUtil.ShareBZs[`GUIDE_MATERIAL_${BZ_TYPE[posterType]}`],
        bzscene: shareUtil.ShareBzscene[BZ_TYPE[posterType]],
        bzId: `${materialId}`,
        sceneName: posterDetail && posterDetail.materialName,
        bzName: `${POSTER_TYPE[posterType]}-${posterDetail.materialName}`,
      });
    }
    console.log('sharePath => ', path);
    return {
      title: `您的专属导购顾问分享了${POSTER_TYPE[posterType]}给您,快来看看吧~`,
      path,
      imageUrl: `${
        posterDetail.coverUrl
          ? posterDetail.coverUrl + '?imageView2/2/w/300'
          : 'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tools/default_video.svg'
      }`,
    };
  });

  useEffect(() => {
    if (sessionId) {
      getData(params.id);
      authHelper.checkAuth(true);
    }
  }, [sessionId]);

  useDidShow(() => {
    const useShareUserLocation = appInfo && (appInfo.useShareUserLocation || appInfo.forbidChangeStoreForGuideShare);
    if (currentStore && params && useShareUserLocation && currentStore.id !== +params._ref_storeId) {
      checkOptions.changeStore(params._ref_storeId);
    }
  });

  return (
    <View className='wk-sgps-Poster'>
      {!!error ? (
        <Error message='发生了一些错误' onRetry={getData}></Error>
      ) : (
        <View className='box'>
          {posterType == VIDEO_TYPE && (
            <View>
              <View className='material-name'>{posterDetail.materialName || ''}</View>
              <Video
                src={posterDetail.materialUrl}
                controls
                autoplay={false}
                poster={
                  posterDetail.coverUrl ||
                  'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tools/default_video.svg'
                }
                initialTime={0}
                id='video'
                loop={false}
                muted={false}
              />
            </View>
          )}

          {posterType == PDF_TYPE && (
            <WebView
              src={`https://wakedata.com/viewer.html?file=${encodeURIComponent(posterDetail.materialUrl)}`}
            ></WebView>
          )}

          {showTip && posterType === VIDEO_TYPE && (
            <CoverView className='cover-tip'>
              <CoverImage
                className='line'
                src='https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tools/tip-line.png'
              ></CoverImage>
              <CoverView className='tip'>点击右上角 分享给客户哦</CoverView>
              <CoverView className='btn' onClick={updateKnown}>
                我知道了
              </CoverView>
            </CoverView>
          )}
        </View>
      )}
    </View>
  );
};

export default Poster;
