import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @scoped
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import shareUtil from '@/wxat-common/utils/share';
import dateUtil from '@/wxat-common/utils/date';
import checkOptions from '@/wxat-common/utils/check-options';
import canvasHelper from '@/wxat-common/utils/canvas-helper';
import couponEnum from '@/wxat-common/constants/couponEnum';
import Error from '@/wxat-common/components/error/error';
import './index.scss';

type StateProps = {
  sessionId: string;
  appInfo: any;
  currentStore: any;
};

interface ShareCoupons {
  props: StateProps;
}

const mapStateToProps = ({ base }) => {
  return {
    appInfo: base.appInfo,
    sessionId: base.sessionId,
    currentStore: base.currentStore,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ShareCoupons extends React.Component {
  $router = $getRouter();
  state = {
    couponId: '', //分享的优惠券ID
    couponDetail: {
      name: '',
      price: '',
      minimum: '',
      termValidity: '',
    },

    //优惠券详情
    guideId: '', //导购的员工ID
    guideAvatar: '', //导购的员工头像
    guideName: '', //导购的员工姓名
    qrCodeUrl: '', //优惠券领取路径的太阳码
    error: false,
    errorMessage: '',
    filePath: '',
  };

  async componentDidMount() {
    const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);

    if (!!formattedOptions) {
      this.init(formattedOptions);
    }
  }

  async componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);
      if (!!formattedOptions) {
        this.init(formattedOptions);
      }
    }
    if (preProps.currentStore.id && preProps.currentStore.id !== this.props.currentStore.id) {
      this.getData();
    }
  }

  init(option) {
    wxApi.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#3a8fff',
    });

    this.setState(
      {
        couponId: +option.couponId,
        guideId: +option.guideId,
      },

      () => {
        this.getData();
      }
    );
  }

  getData() {
    wxApi.showLoading({
      title: '正在生成优惠券分享图',
    });

    Promise.all([this.getCouponInfo(), this.getGuideInfo(), this.getQrcodeInfo()])
      .then((res) => {
        let [couponInfo, guideInfo, qrCodeInfo] = res;
        if (couponInfo && couponInfo.data && couponInfo.data.status !== 1) {
          this.setState({
            error: true,
            errorMessage: '该优惠券已失效或发放完毕',
          });

          return;
        }
        let guideAvatar = 'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tool/avatar.png';
        let guideName = '';
        if (guideInfo && guideInfo.data) {
          guideAvatar = guideInfo.data.avatar ? guideInfo.data.avatar.replace(/^http:/g, 'https:') : guideAvatar;
          guideName = guideInfo.data.name || '';
        }
        this.setState(
          {
            couponDetail: this.formatCouponDetail(couponInfo && couponInfo.data),
            guideAvatar,
            guideName,
            qrCodeUrl: qrCodeInfo && qrCodeInfo.data,
          },

          this.drawCanvas
        );
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          error: true,
          errorMessage: (error.data && error.data.errorMessage) || '发生了一些错误',
        });
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  getGuideInfo() {
    return wxApi.request({
      url: api.guide.guideInfo,
      loading: false,
      quite: true,
      data: {
        employeeId: this.state.guideId,
      },
    });
  }

  getQrcodeInfo() {
    let publicArguments = shareUtil.buildSharePublicArguments({
      forServer: true,
      bzscene: shareUtil.ShareBzscene.GUIDE_SHARE_COUPON,
      bzId: this.state.couponId,
      bzName: `导购专用优惠券-${this.state.couponDetail.name}`,
    });

    const couponInfo = { couponId: this.state.couponId };
    const scene = Object.assign({}, publicArguments, couponInfo);
    return wxApi.request({
      url: api.generatorMapperQrCode,
      loading: false,
      quite: true,
      data: {
        shareParam: JSON.stringify(scene),
        urlPath: 'sub-packages/marketing-package/pages/receive-coupon/index',
      },
    });
  }

  getCouponInfo() {
    return wxApi.request({
      url: api.coupon.detail,
      loading: false,
      quite: false,
      data: {
        couponInfoId: this.state.couponId,
      },
    });
  }
  formatCouponDetail(couponDetail) {
    if (!couponDetail) return null;
    const { couponCategory, discountFee, minimumFee, couponType, fixedTerm, beginTime, endTime, name } = couponDetail;
    let formattedCoupon = {
      name,
      price: '',
      minimum: '',
      termValidity: '',
    };

    if (couponCategory === couponEnum.TYPE.freight.value) {
      formattedCoupon.price = discountFee === 0 ? '免运费' : '¥' + parseFloat((discountFee / 100).toFixed(2));
    } else if (couponCategory === couponEnum.TYPE.fullReduced.value) {
      formattedCoupon.price = discountFee === 0 ? '¥0' : '¥' + parseFloat((discountFee / 100).toFixed(2));
    } else if (couponCategory === couponEnum.TYPE.discount.value) {
      formattedCoupon.price = discountFee === 0 ? '0折' : parseFloat((discountFee / 10).toFixed(2)) + '折';
    }
    // 优惠条件
    formattedCoupon.minimum = minimumFee === 0 ? '无门槛' : '满' + parseFloat((minimumFee / 100).toFixed(2)) + '可用';
    // 有效期
    formattedCoupon.termValidity =
      couponType === 0
        ? '领取后 ' + fixedTerm + ' 天有效'
        : dateUtil.format(new Date(beginTime)) + ' 至 ' + dateUtil.format(new Date(endTime));
    return formattedCoupon;
  }

  async drawCanvas() {
    const ctx = wxApi.createCanvasContext('myCanvas', this);
    try {
      //背景
      let bgData = await wxApi.getImageInfo({
        src: 'https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/coupon/share-bg.png',
      });

      canvasHelper.drawImage(ctx, 0, 0, 750, (bgData.height / bgData.width) * 750, bgData.path);

      //头像
      let avatarData = await wxApi.getImageInfo({ src: this.state.guideAvatar });
      canvasHelper.drawCircleImage(ctx, 375, 202, 43, avatarData.path);
      //提示语
      canvasHelper.drawText(ctx, `您的专属服务顾问${this.state.guideName}`, 375, 300, {
        textAlign: 'center',
        fillStyle: '#333',
        bold: true,
        fontSize: 30,
      });

      canvasHelper.drawText(ctx, '为您赠送了一张隐藏优惠券～', 375, 340, {
        textAlign: 'center',
        fillStyle: '#999999',
        fontSize: 30,
      });

      if (this.state.couponDetail) {
        //优惠券价格
        ctx.save();
        ctx.setTextAlign('left');
        ctx.setFillStyle('#FA2424');
        ctx.setFontSize(44);
        const priceWidth = ctx.measureText(`${this.state.couponDetail.price}`).width;
        ctx.setFontSize(28);
        const minimumWidth = ctx.measureText(`${this.state.couponDetail.minimum}`).width;
        const paddingLeft = (750 - priceWidth - minimumWidth - 10) / 2;
        ctx.fillText(`${this.state.couponDetail.minimum}`, paddingLeft + priceWidth + 10, 440);

        ctx.setFontSize(44);
        ctx.fillText(`${this.state.couponDetail.price}`, paddingLeft, 440);
        ctx.restore();
        //优惠券名称
        canvasHelper.drawText(ctx, `${this.state.couponDetail.name}`, 375, 490, {
          textAlign: 'center',
          fillStyle: '#333333',
          fontSize: 32,
          maxWidth: 480,
          rowLength: 1,
        });

        //优惠券有效期
        canvasHelper.drawText(ctx, `有效期：${this.state.couponDetail.termValidity}`, 375, 530, {
          textAlign: 'center',
          fillStyle: '#999999',
          fontSize: 22,
        });
      }
      //二维码
      if (this.state.qrCodeUrl) {
        let qrCodeData = await wxApi.getImageInfo({ src: this.state.qrCodeUrl });
        canvasHelper.drawImage(ctx, 314, 608, 118, 118, qrCodeData.path);
        canvasHelper.drawText(ctx, `长按识别二维码领券`, 374, 760, {
          textAlign: 'center',
          fillStyle: '#999999',
          fontSize: 22,
        });
      }

      wxApi.showLoading({ title: '正在生成名片' });
      ctx.draw(true, () => {
        wxApi.canvasToTempFilePath({
          canvasId: 'myCanvas',
          success: (res) => {
            this.setState({
              filePath: res.tempFilePath,
            });
          },
          complete: () => {
            wxApi.hideLoading();
          },
        });
      });
    } catch (err) {
      console.log(err);
      this.setState({
        error: true,
        errorMessage: err.errMsg || '发生了一些错误',
      });
    }
  }

  saveImg = () => {
    wxApi.$saveImage(this.state.filePath);
  };

  previewImg = () => {
    if (!this.state.filePath) return;
    wxApi.previewImage({
      urls: [this.state.filePath],
    });
  };

  onShareAppMessage() {
    const path = shareUtil.buildShareUrlPublicArguments({
      url: `sub-packages/marketing-package/pages/receive-coupon/index?couponId=${this.state.couponId}`,
      bz: shareUtil.ShareBZs.GUIDE_SHARE_COUPON,
      bzscene: shareUtil.ShareBzscene.GUIDE_SHARE_COUPON,
      bzId: this.state.couponId,
      sceneName: this.state.couponDetail.name,
      bzName: `导购专用优惠券${
        this.state.couponDetail && this.state.couponDetail.name ? '-' + this.state.couponDetail.name : ''
      }`,
    });

    return {
      title: `您的专属服务顾问${this.state.guideName}向您赠送了一张优惠券，快来领取呀！`,
      path,
      imageUrl: 'https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/coupon/share-url-bg.png',
    };
  }

  render() {
    const { errorMessage, error, filePath } = this.state;
    return (
      <View data-scoped='wk-sgps-ShareCoupon' className='wk-sgps-ShareCoupon share-coupon'>
        {!!error ? (
          <Error message={errorMessage}></Error>
        ) : (
          <View className='share-coupon-content'>
            <Image src={filePath} mode='widthFix' className='canvas-img' onClick={this.previewImg}></Image>
            <Canvas
              canvasId='myCanvas'
              style={_safe_style_('position:fixed;left:999%;visibility:hidden;width:750px;height:918px;')}
            ></Canvas>
            {!!filePath && (
              <View className='operation'>
                <View className='oper-item' onClick={this.saveImg}>
                  <Image
                    src='https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tool/save.png'
                    className='oper-icon'
                  ></Image>
                  <View className='oper-name'>保存图片</View>
                </View>
                <ButtonWithOpenType className='oper-item' openType='share'>
                  <Image
                    src='https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tool/share.png'
                    className='oper-icon'
                  ></Image>
                  <View className='oper-name'>发送客户</View>
                </ButtonWithOpenType>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

export default ShareCoupons;
