import React from 'react'; // @scoped
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, WebView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import './index.scss';

type StateProps = {
  wxUserInfo: Record<string, any>;
};

interface ContactWebView {
  props: StateProps;
}

const mapStateToProps = ({ base }) => {
  return {
    wxUserInfo: base.wxUserInfo,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ContactWebView extends React.Component {
  state = {
    viewSrc: 'https://chat.icsoc.net/user-iframe.html?channel_key=c7f071121fe0ea3d92f523f2ad209f04&init=1', // TATA正式客服链路，长期有效不变更
  };

  async componentDidMount() {
    console.log(this.props.wxUserInfo);
  }

  render() {
    const { wxUserInfo } = this.props;
    const { viewSrc } = this.state;
    return (
      <View data-scoped='wk-sgps-contact-webview' className='wk-sgps-contact-webview'>
        {wxUserInfo && <WebView src={`${viewSrc}&username=${wxUserInfo.nickName}&avatar=${wxUserInfo.avatarUrl}`} />}
      </View>
    );
  }
}

export default ContactWebView;
