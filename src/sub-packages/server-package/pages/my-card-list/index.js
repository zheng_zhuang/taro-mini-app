import React from 'react';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import login from '../../../../wxat-common/x-login/index.js';

import LoadMore from '../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../wxat-common/components/error/error';
import Empty from '../../../../wxat-common/components/empty/empty';
import CardModule from '../../../../wxat-common/components/cardModule/index';
import './index.scss';
const app = Taro.getApp();
const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};

@withWeapp({
  data: {
    error: false,
    productList: null,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
  },

  onLoad: function () {
    this.init();
  },

  init() {
    this.listCardList();
  },

  onPullDownRefresh() {
    this.data.pageNo = 1;
    this.data.hasMore = true;
    this.listCardList();
  },

  onReachBottom() {
    if (this.data.hasMore) {
      this.setData({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.listCardList();
    }
  },

  onRetryLoadMore() {
    this.setData({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.listCardList();
  },
  listCardList(isFromPullDown) {
    this.setData({
      error: false,
    });

    login.login().then(() => {
      wxApi
        .request({
          url: api.myCardList,
          loading: true,
        })
        .then((res) => {
          let productList = res.data || [];

          if (this.isLoadMoreRequest()) {
            productList = this.data.productList.concat(productList);
          }
          if (productList.length === res.totalCount) {
            this.data.hasMore = false;
          }
          this.setData({
            productList,
          });

          this.data.pageNo++;
        })
        .catch((error) => {
          if (this.isLoadMoreRequest()) {
            this.setData({
              loadMoreStatus: loadMoreStatus.ERROR,
            });
          } else {
            this.setData({
              error: true,
            });
          }
        })
        .finally(() => {
          wxApi.stopPullDownRefresh();
        });
    });
  },

  isLoadMoreRequest() {
    return this.data.pageNo > 1;
  },
})
class _C extends React.Component {
  config = {
    navigationBarTitleText: '我的卡券',
  };

  render() {
    const { productList, error, loadMoreStatus, loginFailStatus } = this.data;
    return (
      <View>
        <View className='cards-list'>
          {!!productList && productList.length == 0 && <Empty message='暂无卡项'></Empty>}
          {error && <Error></Error>}
          {productList && productList.length && <Cardmodule isMine dataSource={productList}></Cardmodule>}
          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        </View>
        {loginFailStatus && <Error message='配置出错，点击重试' onRetry={this.onReloadConfig}></Error>}
      </View>
    );
  }
}

export default _C;
