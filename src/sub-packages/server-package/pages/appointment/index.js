import React from 'react';
import { Block, View, Text, Image, Button, Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
// pages/appointment/index.js
import pageLinkEnum from '../../../../wxat-common/constants/pageLinkEnum.js';

const app = Taro.getApp();
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import state from '../../../../state/index.js';
import utilDate from '../../../../wxat-common/utils/date.js';
import constants from '../../../../wxat-common/constants/index.js';
import template from '../../../../wxat-common/utils/template.js';
import protectedMailBox from '../../../../wxat-common/utils/protectedMailBox.js';

import AppointmentTimeSelect from '../../../../wxat-common/components/appointment-time-select/index';
import AuthUser from '../../../../wxat-common/components/auth-user';
import Dialog from '../../../../wxat-common/components/dialog/index';
import './index.scss';
const rules = {
  reg: /^1[3|4|5|6|8|7][0-9]\d{8}$/,
  message: '手机号码格式不正确',
};

let apt_key = 'appointment';

@withWeapp({
  /**
   * 页面的初始数据
   */
  data: {
    store: null,
    ableSelectStore: false,
    servePerson: null,
    appointmentPerson: {
      name: '',
      phone: '',
    },

    goodsInfo: null,
    pageConfig: Array,
    bgColor: '#FFFFFF',
    temporarPerson: {
      name: '',
      phone: '',
    },

    appointmentTimes: null,
    activeDate: '',
    activeTime: '',
    tmpStyle: {},
    isGroupLeader: 0, //是否是团长优惠,供拼团使用
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getTemplateStyle();
    this.setData({
      goodsInfo: JSON.parse(options.goodsInfo),
      isGroupLeader: options.isGroupLeader || 0,
    });
  },

  apiLoad() {
    if (!this.data.appointmentPerson || !this.data.appointmentPerson.name) {
      return;
    }
    this.getAppointment();
    const { currentStore, storeList } = state.base;
    //当前店铺信息
    if (!!currentStore && currentStore.id) {
      if (this.data.store && this.data.store.id !== currentStore.id) {
        state.base.focusOnServePerson = null;
      }
      this.setData({
        store: {
          id: currentStore.id,
          name: currentStore.name,
        },

        ableSelectStore: storeList.length > 1,
      });
    } else {
      this.setData({
        ableSelectStore: true,
      });
    }
    //当前预约人信息
    const { focusOnServePerson } = state.base;
    if (!!focusOnServePerson) {
      this.setData({
        servePerson: {
          id: focusOnServePerson.id,
          name: focusOnServePerson.name,
        },
      });
    } else {
      this.setData({
        servePerson: null,
      });
    }

    this.getScheduled();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.apiLoad();
  },

  userInfoReady() {
    this.setData({
      'appointmentPerson.name': state.base.wxUserInfo.nickName,
      'appointmentPerson.phone': !!state.base.userInfo && !!state.base.userInfo.phone ? state.base.userInfo.phone : '',
    });

    this.apiLoad();
  },

  setAppointmentPerson() {
    wxApi.setStorage({
      key: apt_key,
      data: this.data.appointmentPerson,
    });
  },

  getAppointment() {
    if (wxApi.getStorageSync(apt_key)) {
      this.setData({
        appointmentPerson: wxApi.getStorageSync(apt_key),
      });
    } else {
      if (state.base.wxUserInfo) {
        this.setData({
          'appointmentPerson.name': state.base.wxUserInfo.nickName,
        });
      }
    }
  },

  errorTip(msg) {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  },

  handleCommitOrder(e) {
    this.setAppointmentPerson();
    const serveInfo = this.buildServeInfo();
    const { phone, name } = this.data.appointmentPerson;
    if (!phone) {
      return this.errorTip('预约人电话号码不能为空');
      if (!rules.reg.test(phone)) {
        return this.errorTip(rules.message);
      }
    }
    if (!name) {
      return this.errorTip('预约人不能为空');
    }
    if (!serveInfo.appointmentTime) {
      return this.errorTip('请选择预约时间');
    }

    const { store, servePerson, serveItem } = serveInfo;

    console.log('serveInfoserveInfoserveInfoserveInfo', serveInfo);

    let goodsInfo = [
      {
        name: serveItem && (serveItem.name || ''),
        storeName: store && (store.name || ''),
        servePerson: servePerson && (servePerson.name || ''),
        noNeedPay: serveItem.noNeedPay,
        pic: serveItem.pic,
        salePrice: serveItem.salePrice,
        skuId: serveItem.skuId,
        skuTreeNames: serveItem.skuTreeNames,
        itemCount: serveItem.itemCount,
        itemNo: serveItem.itemNo,
        itemType: constants.goods.itemType.serve,
      },
    ];

    let params = {
      scheduledTime: serveInfo.appointmentTime,
      scheduledEndTime: serveInfo.appointmentEndTime,
      subscriberName: serveInfo.appointmentPerson.name,
      subscriberMobile: serveInfo.appointmentPerson.phone,
    };

    let payDetails = [];

    //判断是否为砍价
    if (!!serveItem.bargainInitiateId) {
      goodsInfo[0].name = serveItem.activityName;
      goodsInfo[0].noNeedPay = true;
      goodsInfo[0].pic = serveItem.thumbnail;
      goodsInfo[0].skuTreeNames = serveItem.itemAttrDesc;
      goodsInfo[0].itemCount = 1;

      params.orderRequestPromDTO = {
        bargainNo: serveItem.bargainInitiateId,

        //砍价最低价
      };
      const lp = (serveItem.lowestPrice / 100).toFixed(2) * 1;
      payDetails.push({
        isCustom: true,
        label: '最低价格',
        value: `￥${lp}`,
      });
    } else {
      goodsInfo[0].name = serveItem && (serveItem.name || '');
      goodsInfo[0].noNeedPay = serveItem.noNeedPay;
      goodsInfo[0].pic = serveItem.pic;
      goodsInfo[0].skuTreeNames = serveItem.skuTreeNames;
      goodsInfo[0].itemCount = serveItem.itemCount;

      //判断是否为拼团
      if (!!serveItem.activityId || !!serveItem.groupNo) {
        params.orderRequestPromDTO = {
          minPeople: serveItem.minPeople,
        };

        if (serveItem.activityId) {
          params.orderRequestPromDTO.activityId = serveItem.activityId;
        }
        if (serveItem.groupNo) {
          params.orderRequestPromDTO.groupNo = serveItem.groupNo;
        }

        // 团长优惠金额
        if (this.data.isGroupLeader && serveItem.leaderPromFee) {
          const leaderFee = (serveItem.leaderPromFee / 100).toFixed(2) * 1;
          payDetails.push({
            isCustom: true,
            label: '团长优惠',
            value: `-￥${leaderFee}`,
          });
        }
      } else {
        payDetails.push(
          {
            name: 'coupon',
          },

          {
            name: 'redpacke',
          },

          {
            name: 'discount',
          },

          {
            name: 'card',
          }
        );
      }
    }

    if (serveInfo.servePerson) {
      params.scheduledTechnicianId = serveInfo.servePerson.id;
      params.scheduledTechnicianName = serveInfo.servePerson.name;
    }
    protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', goodsInfo);
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.payOrder,
      data: {
        payDetails: JSON.stringify(payDetails),
        params: JSON.stringify(params),
      },
    });
  },

  buildServeInfo() {
    const { activeDate, activeTime } = this.data;
    let appointmentTime = '';
    let appointmentEndTime = '';
    if (activeDate && activeTime) {
      appointmentTime = utilDate.format(new Date(activeDate.date)) + ' ' + activeTime.split('~')[0] + ':00';
      appointmentEndTime = activeTime.split('~')[1] + ':00';
    }
    return {
      store: {
        name: this.data.store ? this.data.store.name : '',
        id: this.data.store ? this.data.store.id : '',
      },

      serveItem: this.data.goodsInfo,
      servePerson: this.data.servePerson,
      appointmentPerson: this.data.appointmentPerson,
      appointmentTime: appointmentTime,
      appointmentEndTime: appointmentEndTime,
    };
  },

  handleChooseStore() {
    if (this.data.ableSelectStore) {
      wxApi.$navigateTo({
        url: pageLinkEnum.common.storeList,
      });
    }
  },

  handleChooseServePerson() {
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/serve-person-list/index',
    });
  },

  clearServePerson(e) {
    state.base.focusOnServePerson = null;
    this.setData({
      servePerson: null,
    });

    this.apiLoad();
  },

  handleOnConfirm() {
    const newValue = {
      dialogShow: true,
      title: '更改预约人',
      slotContent: 'content',
      titleTop: '0rpx',
      opacity: '0.5',
      contenMargin: '40rpx 0 50rpx 0',
      'temporarPerson.name': this.data.appointmentPerson.name,
      'temporarPerson.phone': this.data.appointmentPerson.phone,
    };

    this.setData(newValue);
  },
  nameInput(e) {
    this.setData({
      'temporarPerson.name': e.detail.value,
    });
  },
  phoneInput(e) {
    this.setData({
      'temporarPerson.phone': e.detail.value,
    });
  },
  SavePersonInfo() {
    if (rules.reg.test(this.data.temporarPerson.phone)) {
      this.setData({
        dialogShow: false,
        appointmentPerson: this.data.temporarPerson,
      });
    } else {
      wxApi.showToast({
        icon: 'none',
        title: rules.message,
      });
    }
  },
  clearPerson() {
    /*this.setData({
                   'temporarPerson.name': '',
                   'temporarPerson.phone': ''
                 });*/
  },
  onTimeSelected(e) {
    const { activeDate, activeTime } = e.detail;
    this.setData({
      activeDate,
      activeTime,
    });
  },
  getScheduled() {
    const { store, servePerson, goodsInfo } = this.data;
    if (!store || !goodsInfo) {
      return;
    }
    const params = {
      appId: app.globalData.appId,
      storeId: store.id,
      itemNo: goodsInfo.itemNo,
    };

    if (!!servePerson && !!servePerson.id) {
      params.technicianId = servePerson.id;
    }

    wxApi
      .request({
        url: api.appointment.scheduled,
        loading: true,
        data: params,
      })
      .then((res) => {
        this.setData({
          appointmentTimes: res.data,
        });
      })
      .catch((error) => {
        console.log('get scheduled error: ' + JSON.stringify(error));
      });
  },

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setData({
      tmpStyle: templateStyle,
    });
  },
})
class _C extends React.Component {
  config = {
    navigationBarTitleText: '提交订单',
  };

  render() {
    const {
      bgColor,
      store,
      ableSelectStore,
      goodsInfo,
      servePerson,
      appointmentPerson,
      appointmentTimes,
      tmpStyle,
      dialogShow,
      title,
      contenMargin,
      titleTop,
      opacity,
      temporarPerson,
    } = this.data;
    return (
      <AuthUser onReady={this.userInfoReady}>
        <View className='appointment' style={'background-color: ' + bgColor}>
          <View>
            <View className='appointment-item' onClick={this.handleChooseStore}>
              <Text className='appointment-label'>服务门店</Text>
              <View className='appointment-content'>
                <Text className='gray'>{store && store.name ? store.name : '请选择'}</Text>
                {ableSelectStore && (
                  // src={require('../../../../images/right-angle-gray.png')}
                  <Image className='right-icon' ></Image>
                )}
              </View>
            </View>
            <View className='appointment-item'>
              <Text className='appointment-label'>服务项目</Text>
              <View className='appointment-content'>
                <Text>{goodsInfo && goodsInfo.name ? goodsInfo.name : '请选择'}</Text>
                {/*<Image className='right-icon' src={require('../../../../images/right-angle-gray.png')}></Image>*/}
              </View>
            </View>
            <View className='appointment-item' onClick={this.handleChooseServePerson}>
              <Text className='appointment-label'>服务人员</Text>
              <View className='appointment-content'>
                {!!servePerson && !!servePerson.name && (
                  <View className='clear' onClick={this.clearServePerson}>
                    x
                  </View>
                )}

                <Text className='gray'>{servePerson && servePerson.name ? servePerson.name : '请选择'}</Text>
                {/*<Image className='right-icon' src={require('../../../../images/right-angle-gray.png')}></Image>*/}
              </View>
            </View>
            <View className='appointment-item' onClick={this.handleOnConfirm}>
              <Text className='appointment-label'>预约人</Text>
              <View className='appointment-content'>
                <Text className='appoint-person'>
                  {appointmentPerson && appointmentPerson.name ? appointmentPerson.name : ''}
                </Text>
                {/*<Image className='right-icon' src={require('../../../../images/right-angle-gray.png')}></Image>*/}
              </View>
            </View>
            <View className='appointment-item' onClick={this.handleOnConfirm}>
              <Text className='appointment-label'>联系电话</Text>
              <View className='appointment-content'>
                <Text className='appoint-person'>
                  {appointmentPerson && appointmentPerson.phone ? appointmentPerson.phone : '请输入'}
                </Text>
                {/*<Image className='right-icon' src={require('../../../../images/right-angle-gray.png')}></Image>*/}
              </View>
            </View>
            <View className='appointment-item'>
              <Text className='appointment-label'>预约时间</Text>
            </View>
            <AppointmentTimeSelect
              appointmentTimes={appointmentTimes}
              onChange={this.onTimeSelected}
            ></AppointmentTimeSelect>
            <Button
              className='appointment-btn'
              onClick={this.handleCommitOrder}
              style={'background:' + tmpStyle.btnColor}
            >
              提交订单
            </Button>
          </View>
        </View>
        <Dialog
          visible={dialogShow}
          onConfirm={this.SavePersonInfo}
          onCancel={this.clearPerson}
          title={title}
          contenMargin={contenMargin}
          titleTop={titleTop}
          backOpacity={opacity}
        >
          <View className='appointment-form'>
            <View className='doalog-label'>姓名</View>
            <Input
              className='appointment-input'
              placeholder='请输入姓名'
              onInput={this.nameInput}
              value={temporarPerson.name}
            ></Input>
            <View className='doalog-label'>电话</View>
            <Input
              className='appointment-input'
              type='number'
              placeholder='请输入电话'
              onInput={this.phoneInput}
              value={temporarPerson.phone}
            ></Input>
          </View>
        </Dialog>
      </AuthUser>
    );
  }
}

export default _C;
