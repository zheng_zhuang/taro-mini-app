import React from 'react';
import { Block, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import state from '../../../../../state/index.js';
import constants from '../../../../../wxat-common/constants/index.js';
import pay from '../../../../../wxat-common/utils/pay.js';
import template from '../../../../../wxat-common/utils/template.js';
import api from '../../../../../wxat-common/api/index.js';

import './index.scss';

const CARD_TYPE = constants.card.type;
const APPOINTMENT_STATUS = constants.order.appointmentStatus;
const app = Taro.getApp();

@withWeapp({
  data: {
    orderNo: 0,
    orderDetail: null,
    showHome: false,
    tmpStyle: {},
  },

  onLoad: function (options) {
    this.getTemplateStyle();
    const orderNo = options.orderNo;
    this.setData({
      orderNo: orderNo,
      showHome: !!options.showHome,
    });

    this.getOrderDetail();
  },
  getOrderDetail() {
    wxApi
      .request({
        url: api.order.detail,
        loading: true,
        data: {
          orderNo: this.data.orderNo,
        },
      })
      .then((res) => {
        if (res.data) {
          const orderDetail = res.data;
          this.assembleOrder(orderDetail);
          this.setData({
            orderDetail: orderDetail,
          });
        }
      })
      .catch((error) => {});
  },
  assembleOrder(order) {
    if (order) {
      Object.assign(order, {
        showConcat:
          order.orderStatus === APPOINTMENT_STATUS.overtime || order.orderStatus === APPOINTMENT_STATUS.refundReject,
      });

      Object.assign(order, {
        showPay: order.orderStatus === APPOINTMENT_STATUS.waitPay,
      });

      Object.assign(order, {
        showCancel:
          order.orderStatus === APPOINTMENT_STATUS.waitPay || order.orderStatus === APPOINTMENT_STATUS.waitService,
      });
    }
  },
  handlePay(e) {
    let that = this;
    let orderNo = this.data.orderNo;
    let itemType = e.currentTarget.dataset.itemType;
    if (itemType == CARD_TYPE.charge) {
      pay.payByOrder(orderNo, PAY_CHANNEL.WX_PAY, {
        wxPaySuccess() {
          that.this.getOrderDetail();
        },
        fail() {
          console.log(res);
        },
      });
    } else {
      pay.payByOrder(orderNo, null, {
        success(res) {
          that.getOrderDetail();
        },
        fail() {
          console.log(res);
        },
        wxPaySuccess() {
          that.getOrderDetail();
        },
      });
    }
  },
  handleCancelOrders(e) {
    wxApi.showModal({
      title: '确定要取消该预约吗？',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.cancel,
              loading: {
                title: '正在取消预约',
              },

              data: {
                orderNo: this.data.orderNo,
              },
            })
            .then((res) => {
              return this.getOrderDetail();
            })
            .then((res) => {
              const orderDetail = res.data;
              this.assembleOrder(orderDetail);
              this.setData({
                orderDetail: orderDetail,
              });
            })
            .catch((error) => {});
        }
      },
    });
  },
  /**
   * 联系商家
   * @param e
   */
  handleContact(e) {
    if (state.base.currentStore.tel) {
      wxApi.showModal({
        title: '联系商家',
        content: '商家电话: ' + state.base.currentStore.tel,
        success: (res) => {
          if (res.confirm) {
            wxApi.makePhoneCall({
              phoneNumber: state.base.currentStore.tel,
            });
          }
        },
      });
    }
  },
  gotoHome() {
    wxApi.switchTab({
      url: '/wxat-common/pages/home/index',
    });
  },

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setData({
      tmpStyle: templateStyle,
    });
  },
})
class _C extends React.Component {
  config = {
    navigationBarTitleText: '预约详情',
  };

  render() {
    const { tmpStyle, orderDetail, showHome } = this.data;
    return (
      <View className='detail-container'>
        <View className='order-status-box' style={'background:' + tmpStyle.btnColor}>
          {orderDetail && orderDetail.orderStatusDesc ? orderDetail.orderStatusDesc : '-'}
        </View>
        {/*  <form report-submit="true">

                                                                                                                                                                                                                       </form>  */}
        <View className='appointment-box'>
          <View className='appointment-item'>
            <Text className='appointment-name limit-line'>{orderDetail.itemList[0].itemName || ''}</Text>
          </View>
          <View className='appointment-item'>
            <Text className='appointment-adress'>{orderDetail.storeName}</Text>
          </View>
          <View className='appointment-item service'>
            <Text className='appointment-label'>服务人员：</Text>
            <Text>{orderDetail.scheduledTechnicianName || ''}</Text>
          </View>
          <View className='appointment-item'>
            <Text className='appointment-label'>预约时间：</Text>
            <Text>{orderDetail.scheduledTime || ''}</Text>
          </View>
          <View className='appointment-item'>
            <Text className='appointment-label'>商家电话：</Text>
            <Text>{orderDetail.storeTel || ''}</Text>
          </View>
        </View>
        <View className='appointment-info'>
          <View className='appointment-item'>
            <Text className='appointment-label'>预约人：</Text>
            <Text>{(orderDetail.subscriberName || '') + ' ' + (orderDetail.subscriberMobile || '')}</Text>
          </View>
          <View className='appointment-item'>
            <Text className='appointment-label'>原价：</Text>
            <Text className='red'>{'￥' + filters.moneyFilter(orderDetail.totalFee, true)}</Text>
          </View>
          <View className='appointment-item'>
            <Text className='appointment-label'>实付：</Text>
            <Text className='red'>{'￥' + filters.moneyFilter(orderDetail.payFee, true)}</Text>
          </View>
          <View className='appointment-item offset'>
            <Text className='appointment-label'>备注：</Text>
            <Text className='limit-line'>{orderDetail.userMessage}</Text>
          </View>
        </View>
        <View className='operation'>
          {showHome && (
            <View
              className={
                'home ' + (orderDetail.showPay || orderDetail.showCancel || orderDetail.showConcat ? 'home-margin' : '')
              }
              onClick={this.gotoHome}
            >
              <View style='flex: 1;height: 24rpx;'>
                {/*<Image className='home-icon' src={require('../../../../../images/tabbar/home-off.png')}></Image>*/}
              </View>
              <View className='home-text'>首页</View>
            </View>
          )}

          {orderDetail.showPay && (
            <View
              className='order-operation-box topay-btn'
              onClick={this.handlePay}
              style={'border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor}
            >
              付款
            </View>
          )}

          {orderDetail.showCancel && (
            <View className='order-operation-box' onClick={this.handleCancelOrders}>
              取消预约
            </View>
          )}

          {orderDetail.showConcat && (
            <View className='order-operation-box' onClick={this.handleContact}>
              联系商家
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default _C;
