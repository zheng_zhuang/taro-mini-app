import React from 'react';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import login from '../../../../../wxat-common/x-login/index.js';
import state from '../../../../../state/index.js';
import api from '../../../../../wxat-common/api/index.js';
import constants from '../../../../../wxat-common/constants/index.js';
import pay from '../../../../../wxat-common/utils/pay.js';
import utilDate from '../../../../../wxat-common/utils/date.js';
import template from '../../../../../wxat-common/utils/template.js';

import LoadMore from '../../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../../wxat-common/components/error/error';
import Empty from '../../../../../wxat-common/components/empty/empty';
import './index.scss';

const APPOINTMENT_STATUS = constants.order.appointmentStatus;
const loadMoreStatus = constants.order.loadMoreStatus;

// TODO: setData 迁移
export default class AppointmentList extends React.Component {
  state = {
    statusType: APPOINTMENT_STATUS.labelList,
    statusValue: APPOINTMENT_STATUS.valueList,
    payChannelWay: ['余额支付', '微信支付'],
    currentType: 0,
    tabClass: ['', '', '', '', ''],
    orderList: null,
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    tmpStyle: {},
  };

  componentDidMount() {
    this.getTemplateStyle();
  }

  componentDidShow() {
    this.listAppointments();
  }

  onPullDownRefresh() {
    // 页面相关事件处理函数--监听用户下拉动作
    this.data.pageNo = 1;
    this.data.hasMore = true;
    this.listAppointments(true);
  }

  onReachBottom() {
    // 页面上拉触底事件的处理函数
    if (this.data.hasMore) {
      this.setData({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.listAppointments();
    }
  }

  onRetryLoadMore() {
    this.setData({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.listAppointments();
  }

  listAppointments(isFromPullDown) {
    this.setData({
      error: false,
    });

    this.getListAppointmentsRequest()
      .then((res) => {
        if (this.isLoadMoreRequest()) {
          this.data.orderList = this.data.orderList.concat(res.data || []);
        } else {
          this.data.orderList = res.data || [];
        }
        if (this.data.orderList.length === res.totalCount) {
          this.data.hasMore = false;
        }
        const orderList = res.data;
        this.assembleOrderList(orderList);
        this.setData({
          orderList: this.data.orderList,
        });

        this.data.pageNo++;
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setData({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setData({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  }

  isLoadMoreRequest() {
    return this.data.pageNo > 1;
  }

  assembleOrderList(orderList) {
    const cancelList = [APPOINTMENT_STATUS.waitPay, APPOINTMENT_STATUS.waitService];
    const concatList = [APPOINTMENT_STATUS.paid, APPOINTMENT_STATUS.confirm, APPOINTMENT_STATUS.shipping];
    const now = utilDate.format(new Date());
    if (orderList && orderList.length) {
      orderList.forEach((item) => {
        item.showCancel = !!cancelList.find((c) => c === item.orderStatus);
        item.showConcat = !!concatList.find((c) => c === item.orderStatus);
        item.showPay = item.orderStatus === APPOINTMENT_STATUS.waitPay;
        // 格式化日期
        item.scheduledDate = null;
        if (item.scheduledTime) {
          const scheduledTime = new Date(item.scheduledTime.replace(/-/g, '/'));
          const date = utilDate.format(scheduledTime);
          if (date === now) {
            item.scheduledDate = '今天' + utilDate.format(scheduledTime, 'hh:mm');
          } else {
            console.log(scheduledTime);
            item.scheduledDate = utilDate.format(scheduledTime, 'yyyy-MM-dd hh:mm');
          }
        }
      });
    }
  }

  getListAppointmentsRequest() {
    let params = {
      storeId: state.base.currentStore.id,
      // itemType: constants.goods.itemType.serve,
      // itemTypeList: constants.goods.itemType.serve,
      'itemTypeList[0]': 0, //预约类型
      pageNo: this.data.pageNo,
      pageSize: 10,
    };

    const theStatus = this.data.statusValue[this.data.currentType];
    if (theStatus) {
      theStatus.forEach((item, index) => {
        params = {
          ...params,
          ...{
            ['orderStatusList[' + index + ']']: item,
          },
        };
      });
    }
    return wxApi.request({
      url: api.order.list,
      loading: true,
      data: params
    });
  }

  handleGoToDetail(e) {
    const orderNo = e.currentTarget.dataset.orderNo;
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/appointment/details/index',
      data: {
        orderNo: orderNo,
      },
    });
  }

  /**
   * 点击tab切换，获取订单列表
   * @param e
   */
  handleListAppointments(e) {
    const curType = e.currentTarget.dataset.index;
    this.setData({
      currentType: curType,
      pageNo: 1,
      hasMore: true,
    });

    this.listAppointments();
  }

  /**
   * 联系商家
   * @param e
   */
  handleContact(e) {
    if (state.base.currentStore.tel) {
      wxApi.showModal({
        title: '联系商家',
        content: '商家电话: ' + state.base.currentStore.tel,
        success: (res) => {
          if (res.confirm) {
            wxApi.makePhoneCall({
              phoneNumber: state.base.currentStore.tel,
            });
          }
        },
      });
    }
  }

  /**
   * 确认收货
   */
  handleConfirmDelivery() {
    const orderNo = e.currentTarget.dataset.orderNo;
    wxApi.showModal({
      title: '请确定已收到货物',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.confirmDelivery,
              loading: {
                title: '正在通知商家',
              },

              data: {
                orderNo,
              },
            })
            .then((res) => {
              return this.getListAppointmentsRequest();
            })
            .then((res) => {
              const orderList = res.data;
              this.assembleOrderList(orderList);
              this.setData({
                orderList: orderList,
              });
            })
            .catch((error) => {});
        }
      },
    });
  }

  /**
   * 点击取消订单
   * @param e
   */
  handleCancelOrder(e) {
    const orderNo = e.currentTarget.dataset.orderNo;
    wxApi.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.cancel,
              loading: {
                title: '正在取消订单',
              },

              data: {
                orderNo: orderNo,
              },
            })
            .then((res) => {
              this.data.pageNo = 1;
              return this.getListAppointmentsRequest();
            })
            .then((res) => {
              const orderList = res.data;
              this.assembleOrderList(orderList);
              this.setData({
                orderList: orderList,
              });
            })
            .catch((error) => {});
        }
      },
    });
  }

  /**
   * 点击付款
   * @param e
   */
  handlePay(e) {
    const that = this;
    const orderInfo = e.currentTarget.dataset.item;
    console.log('------------->', JSON.stringify(orderInfo));
    // pay.handleBuyNow(orderInfo, null, );
    pay.payByOrder(orderInfo.orderNo, null, {
      success(res) {
        that.data.pageNo = 1;
        that.listAppointments(true);
      },
      fail() {
        console.log(res);
      },
      wxPaySuccess() {
        that.data.pageNo = 1;
        that.listAppointments(true);
      },
    });
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setData({
      tmpStyle: templateStyle,
    });
  }

  config = {
    navigationBarTitleText: '我的预约',
    enablePullDownRefresh: true,
  };

  render() {
    const { currentType, tmpStyle, statusType, tabClass, orderList, error, loadMoreStatus } = this.state;

    return (
      <View className='container'>
        <View className='status-box'>
          {statusType.map((item, index) => {
            return (
              <View
                onClick={this.handleListAppointments}
                className={'status-label ' + (index == currentType ? 'active' : '')}
                key='index'
                data-index={index}
                style={
                  index == currentType
                    ? 'border-bottom:6rpx solid ' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor
                    : ''
                }
              >
                {item}
                <View className={tabClass[index]}></View>
              </View>
            );
          })}
        </View>
        {!!orderList && orderList.length == 0 && <Empty message='暂无订单'></Empty>}
        {!!error ? (
          <Error></Error>
        ) : (
          <View className='order-list' hidden={orderList && orderList.length ? false : true}>
            {orderList.map((item, index) => {
              return (
                <View className='a-order' key='index'>
                  <View className='order-info'>
                    <View className='order-no'>{'订单号：' + (item.orderNo || '-')}</View>
                    <View className='appointment-date'>{item.orderTime}</View>
                  </View>
                  <View className='a-goods' onClick={this.handleGoToDetail} data-order-no={item.orderNo}>
                    <View className='img-box'>
                      <Image src={item.itemList[0].thumbnail} className='img'></Image>
                    </View>
                    <View className='text-box'>
                      <View className='server-info'>
                        <View className='server-name'>{item.itemList[0].itemName || '-'}</View>
                        <View>
                          <Text className='user-name'>{item.subscriberName || '-'}</Text>
                          <Text className='user-phone'>{item.subscriberMobile || '-'}</Text>
                        </View>
                      </View>
                      <View className='price-info'>
                        <View className='server-status'>{item.orderStatusDesc}</View>
                        <View className='server-price'>
                          合计: ￥<Text className='total-price-fee'>{filters.moneyFilter(item.payFee, true)}</Text>
                        </View>
                        <View className='server-pay'>{item.payChannel}</View>
                      </View>
                    </View>
                  </View>
                  <View className='server-time'>
                    <View style='margin-bottom: 10rpx;'>{'预约技师 ' + (item.scheduledTechnicianName || '-')}</View>
                    <View>{'到店时间 ' + (item.scheduledDate || '-')}</View>
                  </View>
                  {(item.showCancel || item.showConcat || item.showPay) && (
                    <View className='operation-box'>
                      {item.showPay && (
                        <View
                          className='btn topay-btn'
                          onClick={this.handlePay}
                          data-item={item}
                          style={'border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor}
                        >
                          付款
                        </View>
                      )}

                      {item.showCancel && (
                        <View className='btn cancel-btn' onClick={this.handleCancelOrder} data-order-no={item.orderNo}>
                          取消预约
                        </View>
                      )}

                      {item.showConcat && (
                        <View className='btn cancel-btn' onClick={this.handleRefund} data-order-no={item.orderNo}>
                          联系商家
                        </View>
                      )}
                    </View>
                  )}
                </View>
              );
            })}
          </View>
        )}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}
