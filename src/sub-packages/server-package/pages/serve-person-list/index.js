import React from 'react';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import login from '../../../../wxat-common/x-login/index.js';
import state from '../../../../state/index.js';
import constants from '../../../../wxat-common/constants/index.js';
import template from '../../../../wxat-common/utils/template.js';
import Empty from '../../../../wxat-common/components/empty/empty';
import LoadMore from '../../../../wxat-common/components/load-more/load-more';
import './index.scss';
const loadMoreStatus = constants.order.loadMoreStatus;

@withWeapp({
  /**
   * 页面的初始数据
   */
  data: {
    //分页查询
    pageNo: 1,
    pageSize: 20,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    list: null,
    current_name: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getTemplateStyle();
    this.current_name = options.current_name;
    this.apiList();
    this.pickStore();
  },

  pickStore() {
    let { focusOnServePerson } = state.base;
    if (!!focusOnServePerson) {
      this.setData({
        current_name: focusOnServePerson.name,
      });
    }
  },

  apiList(isFromPullDown) {
    login
      .login()
      .then((res) => {
        return wxApi.request({
          url:
            api.appointment.servePerson +
            '?from=minapp&pageNo=' +
            this.data.pageNo +
            '&pageSize=' +
            this.data.pageSize +
            '&storeId=' +
            state.base.currentStore.id,
          loading: true,
        });
      })
      .then((res) => {
        let data = res.data || [];
        let list = this.data.pageNo == 1 ? data : this.data.list.concat(data);
        this.setData({
          list: list,
          pageNo: this.data.pageNo + 1,
          hasMore: data.length >= this.data.pageSize,
          loadMoreStatus: loadMoreStatus.HIDE,
        });
      })
      .catch((error) => {
        this.setData({
          loadMoreStatus: loadMoreStatus.ERROR,
        });
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  },

  onServePersonChoosed(e) {
    const perId = e.currentTarget.dataset.id;
    this.data.list.forEach((item) => {
      if (item.id == perId) {
        state.base.focusOnServePerson = {
          id: perId,
          name: item.name,
        };
      }
    });
    wxApi.navigateBack();
  },

  onPullDownRefresh() {
    this.data.pageNo = 1;
    this.data.hasMore = true;
    this.apiList(true);
  },

  onReachBottom() {
    if (this.data.hasMore && loadMoreStatus != loadMoreStatus.LOADING) {
      this.setData({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.apiList();
    }
  },

  onRetryLoadMore() {
    this.apiList();
  },

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  },
})
class _C extends React.Component {
  config = {
    navigationBarTitleText: '选择技师',
  };

  render() {
    const { current_name, list, loadMoreStatus } = this.data;
    return (
      <Block>
        <View className='serve-person-area'>
          {current_name && <View className='current-person'>{'当前：' + current_name}</View>}
          {list && (
            <View>
              {list.map((item, index) => {
                return (
                  <View className='item' onClick={this.onServePersonChoosed} key='id' data-id={item.id}>
                    <View className='name'>{item.positionName + '：' + item.name}</View>
                  </View>
                );
              })}
            </View>
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        </View>
        {!!list && list.length === 0 && <Empty message='暂无数据'></Empty>}
      </Block>
    );
  }
}

export default _C;
