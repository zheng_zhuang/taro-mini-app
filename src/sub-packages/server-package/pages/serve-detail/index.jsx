import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import { Block, View, Image, Text, Button, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../wxat-common/utils/money.wxs.js';
import api from "../../../../wxat-common/api";
import wxApi from '../../../../wxat-common/utils/wxApi';
import constants from "../../../../wxat-common/constants";
import report from "../../../../sdks/buried/report";
import template from '../../../../wxat-common/utils/template.js';
import utils from '../../../../wxat-common/utils/util.js';
import canvasHelper from '../../../../wxat-common/utils/canvas-helper.js';
import money from '../../../../wxat-common/utils/money.js';
import shareUtil from '../../../../wxat-common/utils/share.js';
import hoc from '@/hoc/index';
import CouponDialog from "../../../../wxat-common/components/coupon-dialog";
import PostersDialog from "../../../../wxat-common/components/posters-dialog";
import AnimatDialog from "../../../../wxat-common/components/animat-dialog";
import StoreModule from "../../../../wxat-common/components/decorate/storeModule";
import ChooseAttributeDialog from '../../../../wxat-common/components/choose-attribute-dialog';
import BuyNow from '../../../../wxat-common/components/buy-now';
import store from '@/store/index';
import './index.scss';

import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'

// FIXME: 重构
import WxParse from '@/wxat-common/lib/wxParse/wxParse';
import SafeAreaPlaceholder from '@/wxat-common/components/safe-area-placeholder/index';


const app = store.getState();

export default
@hoc
class ServeDetail extends React.Component {
  $router = $getRouter();
  state = {
    goodsType: constants.goods.type.serve,
    goodsImages: [],
    goodsDetail: null,
    describeHtml: null,
    dialogShow: false,
    coupon: [],
    tmpStyle: {},
    visible: false,
    qrCode: null,
  };

  itemNo = null;
  itemType = null;

  componentDidMount() {
    const options = this.$router.params;
    if (options.scene) {
      const scene = '?' + decodeURIComponent(options.scene);
      const itemNo = utils.getQueryString(scene, 'No');
      const itemType = utils.getQueryString(scene, 'Type');

      report.detailPage(itemNo);
      (this.itemNo = itemNo), (this.itemType = itemType);
    } else if (options.itemNo) {
      report.detailPage(options.itemNo);
      this.itemNo = options.itemNo;
    }

    this.getTemplateStyle();
    this.getGoodsDetail();
  }

  onSelectShareChanel() {
    this.getShareDialog().show({
      scale: 1,
    });
  }

  onCloseShare() {
    this.getShareDialog().hide();
  }

  getShareDialog() {
    if (!this._shareDialog) {
      this._shareDialog = this.selectComponent('#share-dialog');
    }
    return this._shareDialog;
  }

  onCreatePoster() {
    this.getShareDialog().hide();
    wxApi
      .request({
        url: api.verification.getQRCode,
        loading: true,
        data: {
          verificationNo: this.itemNo,
          verificationType: 5,
          maPath: '/sub-packages/server-package/pages/serve-detail/index',
        },
      })
      .then((res) => {
        this.setState({
          visible: true,
          qrCode: res.data.qrCode,
        });
      });
  }

  onSaveImage(e) {
    wxApi.showLoading();
    const { user, goods } = e;
    const canvasContext = wxApi.createCanvasContext('shareCanvas', this);
    Promise.all([
      wxApi.getImageInfo({
        src: user.headUrl,
      }),

      wxApi.getImageInfo({
        src: goods.thumbnail,
      }),

      wxApi.getImageInfo({
        src: this.state.qrCode,
      }),
    ])
      .then((res) => {
        // 背景
        canvasHelper.drawFillRect(canvasContext, 0, 0, 690, 789, '#fff');
        // 头像
        canvasHelper.drawCircleImage(canvasContext, 32, 32, 20, res[0].path);

        // 用户名
        canvasHelper.drawText(canvasContext, user.name, 62.5, 24, {
          textAlign: 'start',
          fillStyle: '#373A44',
          fontSize: 14,
        });

        // 红色三角
        canvasHelper.drawFillLine(
          canvasContext,
          [
            { x: 62.5, y: 44 },
            { x: 66.5, y: 42 },
            { x: 66.5, y: 46 },
          ],

          '#F0556C'
        );

        // 红色方框
        canvasHelper.drawFillRect(canvasContext, 66.5, 34, 130, 20, '#F0556C');

        // 拼团、砍价 商品详情文案
        canvasHelper.drawText(canvasContext, '向您推荐了这个商品', 72.5, 49, {
          textAlign: 'start',
          fillStyle: '#ffffff',
          fontSize: 13,
        });

        // 商品图片
        canvasHelper.drawImage(canvasContext, 72, 94.5, 175, 175, res[1].path);

        // 商品名
        let goodsName = '';
        if (goods.name.length <= 12) {
          goodsName = goods.name;
        } else {
          goodsName = goods.name.substr(0, 12) + '...';
        }
        canvasHelper.drawText(canvasContext, goodsName, 16, 314.5, {
          textAlign: 'start',
          fillStyle: '#373A44',
          fontSize: 14,
        });

        // 商品标价
        canvasHelper.drawText(canvasContext, '￥' + money.fen2YuanFixed(goods.salePrice), 16, 344.5, {
          textAlign: 'start',
          fillStyle: '#F0556C',
          fontSize: 18,
          bold: true,
        });

        canvasHelper.drawText(canvasContext, '￥' + money.fen2YuanFixed(goods.labelPrice), 16, 364.5, {
          textAlign: 'start',
          fillStyle: '#A2A2A4',
          fontSize: 12,
        });

        canvasHelper.drawImage(canvasContext, 209.5, 284.5, 100, 100, res[2].path);

        console.log('goods', goods);

        canvasContext.draw(false, (res) => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'shareCanvas',
              },

              this
            )
            .then((res) => {
              return wxApi.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
              });
            })
            .then((res) => {
              wxApi.showToast({
                title: '已保存到相册',
              });
            })
            .catch((error) => {
              console.log('saveImage error: ', error);
              wxApi.showToast({
                title: '保存失败',
                icon: 'none',
              });
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      })
      .catch((error) => {
        wxApi.hideLoading();
      });
  }

  /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */
  onShareAppMessage() {
    const itemName = this.state.goodsDetail.wxItem.name ? this.state.goodsDetail.wxItem.name : '';
    const path = shareUtil.buildShareUrlPublicArguments({
      url: '/sub-packages/server-package/pages/serve-detail/index?itemNo=' + this.itemNo,
      bz: shareUtil.ShareBZs.SERVE_DETAIL,
      bzName: `服务${itemName ? '-' + itemName : ''}`,
      bzId: this.itemNo,
      sceneName: itemName,
    });

    console.log('sharePath => ', path);
    return {
      title: itemName,
      path,

      success: () => {
        report.share(true);
        wxApi
          .request({
            url: api.coupon.get_coupons,
            loading: true,
            checkSession: true,
            data: {
              eventType: 0,
            },
          })
          .then((res) => {
            if (res.data && res.data.length) {
              this.setState({
                coupon: res.data || [],
                dialogShow: true,
              });
            }
          })
          .catch(() => {});
      },
      fail: () => {
        report.share(false);
      },
    };
  }

  getGoodsDetail() {
    wxApi
      .request({
        url: api.goods.detail,
        data: {
          storeId: app.base.currentStore.id,
          loading: true,
          itemNo: this.itemNo,
        },
      })
      .then((res) => {
        const data = res.data;
        if (data) {
          const describe = data.wxItem.describe;
          if (describe) {
            console.log('有富文本');
            try {
              WxParse.wxParse('describeHtml', 'html', describe, this, 5);
            } catch (e) {
              console.log('富文本转换异常', e);
            }
          } else {
            console.log('真没有富文本');
          }
          this.setState({
            goodsDetail: data,
            goodsImages: data.materialUrls || [],
          });
        } else {
          this.apiError();
        }
      })
      .catch(() => {});
  }

  handleBuyNow() {
    report.clickBuy(this.itemNo);
    this.chooseAttributeCMPT && this.chooseAttributeCMPT.showAttributDialog();
  }

  handlerChoose(info) {
    const dialog = this.chooseAttributeCMPT;
    !!dialog && dialog.hideAttributeDialog();
    const detail = JSON.parse(JSON.stringify(info));
    delete detail.wxItem.describe;
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/appointment/index',
      data: {
        goodsInfo: JSON.stringify(detail),
      },
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 组件引用
  refChooseAttributeCMPT = (node) => (this.chooseAttributeCMPT = node);
  refShareDialogCMPT = (node) => (this.shareDialogCMPT = node);

  render() {
    const {
      goodsType,
      goodsDetail,
      goodsImages,
      describeHtml,
      dialogShow,
      coupon,
      visible,
      qrCode,
      tmpStyle,
    } = this.state;

    return (
      <Block>
        {!!goodsDetail && !!goodsDetail.wxItem && (
          <View className='goods-detail-container'>
            {goodsDetail && (
              <ChooseAttributeDialog
                ref={this.refChooseAttributeCMPT}
                goodsType={goodsType}
                goodsDetail={goodsDetail}
                onChoose={this.handlerChoose}
              ></ChooseAttributeDialog>
            )}

            {/*  大图预览  */}
            {!goodsImages || !goodsImages[0] ? (
              <View className='goods-image'></View>
            ) : (
              <View
                className='goods-image'
                style={'background: transparent url(' + goodsImages[0] + ') no-repeat 50% 50%;background-size: cover;'}
              ></View>
            )}

            {/*  商品基础信息  */}
            <View className='goods-info'>
              <View className='goods-name limit-line'>{goodsDetail.wxItem && goodsDetail.wxItem.name}</View>
              <View className='goods-row'>
                <View className='goods-price'>
                  {'￥' + filters.moneyFilter(goodsDetail.wxItem && goodsDetail.wxItem.salePrice, true)}
                </View>
                {goodsDetail.wxItem.itemSalesVolume > 0 && (
                  <View className='count'>
                    {'已有' + goodsDetail.wxItem && goodsDetail.wxItem.itemSalesVolume + '人购买'}
                  </View>
                )}
              </View>
              <View className='goods-hr'></View>
              <View className='tag-box'>
                <View className='tag-item'>
                  <Image src={getStaticImgUrl.goods.authentication_png}></Image>
                  <Text>认证企业</Text>
                </View>
                <View className='tag-item'>
                  <Image src={getStaticImgUrl.goods.authentication_png}></Image>
                  <Text>支持改期</Text>
                </View>
                <View className='tag-item'>
                  <Image src={getStaticImgUrl.goods.authentication_png}></Image>
                  <Text>极速退款</Text>
                </View>
              </View>
              <View className='goods-hr'></View>
              <StoreModule></StoreModule>
              <View className='goods-detail'>图文详情</View>
              {describeHtml ? (
                <View className='goods-desc'>
                  <import src='../../../../wxat-common/lib/wxParse/wxParse.js' />
                  <template is='wxParse' data='{{wxParseData:describeHtml.nodes}}' />
                </View>
              ) : (
                <View className='goods-desc empty'>暂无内容</View>
              )}
            </View>
            <BuyNow
              operationText='已下架'
              defaultText='立即预约'
              disable={goodsDetail.wxItem.isShelf === 0}
              immediateShare={false}
              onShare={this.onSelectShareChanel}
              onBuyNow={this.handleBuyNow}
              showCart={false}
            ></BuyNow>
          </View>
        )}

        <CouponDialog visible={dialogShow} coupon={coupon} eventType='0'></CouponDialog>
        <AnimatDialog ref={this.refShareDialogCMPT} animClass='share-dialog'>
          <View className='share-item'>
            <Button openType='share' className='share-btn' onClick={this.onCloseShare}>
              <View className='image-block'>
                <Image className='share-img' src={getStaticImgUrl.group.share_png}></Image>
              </View>
              <Text>分享给朋友</Text>
            </Button>
          </View>
          {process.env.TARO_ENV === 'weapp' && (
            <View className='share-item'>
              <Button className='share-btn' onClick={this.onCreatePoster}>
                <View className='image-block'>
                  <Image className='share-img' src={getStaticImgUrl.group.poster_png}></Image>
                </View>
                <View>生成商品海报</View>
              </Button>
            </View>
          )}

          <Button className='close' onClick={this.onCloseShare}>
            关闭
            <SafeAreaPlaceholder />
          </Button>
        </AnimatDialog>
        <PostersDialog
          visible={visible}
          goods={goodsDetail && goodsDetail.wxItem}
          tips='向您推荐了这个商品'
          qrCode={qrCode}
          onSave={this.onSaveImage}
        ></PostersDialog>
        <Canvas canvasId='shareCanvas' className='testCanvas'></Canvas>
      </Block>
    );
  }
}
