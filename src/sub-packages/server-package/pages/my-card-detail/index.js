import React from 'react';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import filter from './index.wxs.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import template from '../../../../wxat-common/utils/template.js';
import QrCode from "../../../../wxat-common/components/qr-code";
import StoreModule from "../../../../wxat-common/components/decorate/storeModule";
import './index.scss';

@withWeapp({
  data: {
    dataSource: null,
    bgImage: null,
  },

  onLoad: function (option) {
    this.getTemplateStyle();
    const item = JSON.parse(option.item);
    console.log(item);
    let image = '';
    if (item.styleUrl) {
      image = item.styleUrl;
    } else {
      image = getStaticImgUrl.goods.cardMinCount_png;
    }
    this.setData({
      dataSource: item,
      bgImage: image,
    });
  },

  click(e) {
    console.log(e.currentTarget.dataset.item);
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/serve-detail/index',
      data: {
        itemNo: e.currentTarget.dataset.item.serviceItemNo,
      },
    });
  },

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  },
})
class _C extends React.Component {
  config = {
    navigationBarTitleText: '卡券详情',
  };

  render() {
    const { bgImage, dataSource } = this.data;
    return (
      <View className='my-card-detail'>
        <View className='mine-card-item'>
          <Image className='mine-card-bg' src={bgImage}></Image>
          <View className='container'>
            <View className='name'>{dataSource.cardItemName}</View>
            {/*  <View class='line'></View>  */}
            <View className='validity'>{'有效期：' + filter.getDate(dataSource.validityDate)}</View>
          </View>
        </View>
        <View className='use-address'>
          <View className='use-store'>适用门店</View>
          <Storemodule></Storemodule>
        </View>
        {dataSource && <QrCode verificationNo={dataSource.id} verificationType='3'></QrCode>}
        <View className='card-desc'>
          <View>详情</View>
          <View className='card-desc-container'>
            {dataSource.serviceDTOList.map((item, index) => {
              return (
                <View className='card-desc-item' data-item={item} onClick={this.click}>
                  <View className='left'>
                    <View>{item.serviceItemName}</View>
                    <View>{item.serviceTotalCount + '次'}</View>
                  </View>
                  <View className='right'>
                    <View>{'剩余' + item.serviceCount + '次'}</View>
                    <Image src={getStaticImgUrl.images.rightAngleGray_png)} className='right-angle'></Image>
                  </View>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    );
  }
}

export default _C;
