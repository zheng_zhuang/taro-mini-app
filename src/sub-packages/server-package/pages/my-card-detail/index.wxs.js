export default {
  getDate: function (timeStr) {
    if (timeStr) {
      return timeStr.split(' ')[0];
    } else {
      return '永久';
    }
  },
};
