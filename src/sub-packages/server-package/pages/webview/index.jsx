import React, {Component} from 'react';
import {View, WebView} from '@tarojs/components';
import Taro from "@tarojs/taro";
import wxApi from "@/wxat-common/utils/wxApi";
import H5UrlEnum from '@/wxat-common/constants/H5UrlEnum.js';
import {$getRouter} from "wk-taro-platform";
import wkApi from 'wkapi-taro';
import store from '@/store';
import utils from "@/wxat-common/utils/util";

export default class Webview extends React.Component {
  state = {
    url: '',
    title: '', /* 请尽快迁移为 componentDidMount 或 constructor */
    isStoage: 0,
    h5Path: [],
    $router: $getRouter(),
    shareConfig:null,
  }

  componentDidMount() {
    let url = decodeURIComponent(this.state.$router.params.url || '');
    const sessionId = utils.getQueryString(url, 'sessionId')
    if (!sessionId) {
      const newSeesionId = store.getState().base.sessionId
      url = url.indexOf('?') === -1 ? url + `?sessionId=${newSeesionId}` : url + `&sessionId=${newSeesionId}`
    }
    const title = decodeURIComponent(this.state.$router.params.title || '');
    const isStoage = Number(this.state.$router.params.isStoage || 0)
    this.setState({
      url,
      title,
      isStoage
    })
    Taro.setNavigationBarTitle({title});
    if (this.state.isStoage == 1) {
      const uk_hotel_url = decodeURIComponent(wxApi.getStorageSync('uk_hotel_url'))
      this.setState({
        url: uk_hotel_url
      })
    }
  }

  handleMessage = (data) =>{
    if (data.detail.data[0].type === 'share') {
      const shareConfig = data.detail.data[data.detail.data.length - 1].config;
      this.setState({
        shareConfig,
        h5Path: [{ path: shareConfig.path }]
      })
    } else {
      this.setState({
        h5Path: data.detail.data
      })
    }
  }
  
  onShareAppMessage (res) {
    let sharePath = `${H5UrlEnum.YQZ_H5_URL}pages/index/index`
    const currentH5Path = this.state.h5Path[this.state.h5Path.length - 1];
    const protectPath = ['/hotel/hotelDetail/hotelDetail', ]

    if (currentH5Path && protectPath.includes(currentH5Path.path)) {
      const fullPath = currentH5Path.fullPath.startsWith('/') ? currentH5Path.fullPath.replace('/', '') : currentH5Path.fullPath
      sharePath = `${H5UrlEnum.YQZ_H5_URL}${fullPath}`
    }
    wxApi.hideTabBar();
    if (this.state.shareConfig) {
      return {
        title: this.state.shareConfig.title,
        path: `/sub-packages/server-package/pages/webview/index?url=${encodeURIComponent(`${H5UrlEnum.YQZ_H5_URL}${this.state.shareConfig.sharePath}`)}`
      };
    } else {
      return {
        title: `酒店客房预订`,
        path: `/sub-packages/server-package/pages/webview/index?url=${encodeURIComponent(sharePath)}`
      };
    }
    
  }

  render() {

    return (
      <View>
        {this.state.url && <WebView src={this.state.url} onMessage={this.handleMessage}></WebView>}
      </View>
    );
  }
}
