import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../wxat-common/utils/money.wxs.js';
import api from "../../../../wxat-common/api";
import wxApi from '../../../../wxat-common/utils/wxApi';
import constants from "../../../../wxat-common/constants";
import pay from '../../../../wxat-common/utils/pay.js';
import report from "../../../../sdks/buried/report";
import template from '../../../../wxat-common/utils/template.js';
import utils from '../../../../wxat-common/utils/util.js';
import shareUtil from '../../../../wxat-common/utils/share.js';
import store from '@/store/index';
import BuyNow from '../../../../wxat-common/components/buy-now';
import './index.scss';
import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl.js'

const CARD_TYPE = constants.card.type;
const app = store.getState();

class CardDetail extends React.Component {
  $router = $getRouter();
  state = {
    goodsDetail: null,
    coupon: CARD_TYPE.coupon,
    charge: CARD_TYPE.charge,
    tmpStyle: {},
  };

  itemNo = null;
  itemType = null;

  componentDidMount() {
    const options = this.$router.params;
    if (options.scene) {
      const scene = '?' + decodeURIComponent(options.scene);
      const itemNo = utils.getQueryString(scene, 'No');
      const itemType = utils.getQueryString(scene, 'Type');

      report.detailPage(itemNo);
      (this.itemNo = itemNo), (this.itemType = itemType);
    } else if (options.itemNo) {
      report.detailPage(options.itemNo);
      this.itemNo = options.itemNo;
    }

    this.getTemplateStyle();
    this.getGoodsDetail();
  }

  onShow() {
    this._payInfo = null;
  }

  getGoodsDetail() {
    wxApi
      .request({
        url: api.card.detail,
        loading: true,
        checkSession: true,
        data: {
          storeId: app.base.currentStore.id,
          itemNo: this.itemNo,
        },
      })
      .then((res) => {
        this.setState({
          goodsDetail: res.data,
        });
      });
  }

  handletoServer = (event) => {
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/serve-detail/index',
      data: {
        itemNo: event.currentTarget.dataset.goods.serverItemNo,
      },
    });
  };

  /**
   * 点击付款
   * @param e
   */
  handleBuyNow = (e) => {
    const goodsDetail = this.state.goodsDetail;
    console.log(goodsDetail);
    const orderInfo = {
      itemDTOList: [
        {
          itemCount: 1,
          itemNo: goodsDetail.itemNo,
          type: goodsDetail.type,
        },
      ],
    };

    const _this = this;
    report.clickBuy(goodsDetail.itemNo);
    pay.handleBuyNow(orderInfo, null, {
      msg: '正在支付中.....',
      payInfo: this._payInfo,
      success(res, payChannel) {
        if (payChannel != 1) {
          // 1代表微信支付
          _this._payInfo = res;
          wxApi.navigateBack({});
        }
      },
      wxPaySuccess(res) {
        // 微信支付成功
        _this._payInfo = null;
        wxApi.navigateBack({});
      },
      fail(error) {
        console.log(error);
      },
      wxPayFail(fail) {
        console.log(fail);
      },
    });
  };

  /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */
  onShareAppMessage() {
    const title = this.state.goodsDetail.name ? this.state.goodsDetail.name : '';
    const path = shareUtil.buildShareUrlPublicArguments({
      url: '/sub-packages/server-package/pages/card-detail/index?itemNo=' + this.itemNo,
      bz: shareUtil.ShareBZs.CARD_DETAIL,
      bzName: `卡项${title ? '-' + title : ''}`,
      bzId: this.itemNo,
      sceneName: title,
    });

    console.log('sharePath => ', path);
    return {
      title,
      path,

      success: function (res) {
        // 转发成功
        report.share(true);
        console.log('转发成功:' + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        report.share(false);
        console.log('转发失败:' + JSON.stringify(res));
      },
    };
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { goodsDetail, coupon, charge, tmpStyle } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-spc-CardDetail' className='wk-spc-CardDetail'>
        {!!goodsDetail && (
          <View className='card-detail-container'>
            <View className='card-icon'>
              {!!(!!goodsDetail && goodsDetail.style === 0 && goodsDetail.type === coupon) && (
                <Image src={getStaticImgUrl.goods.cardMinCount_png} className='card-img'></Image>
              )}

              {!!goodsDetail && goodsDetail.style === 0 && goodsDetail.type === charge ? (
                <Image src={getStaticImgUrl.goods.rechargeCard_png} className='card-img'></Image>
              ) : (
                <Image src={goodsDetail.styleUrl} className='card-img'></Image>
              )}

              <View className='card-content-name limit-line line-2'>{goodsDetail.name}</View>
              <View className='card-content-box'>
                {goodsDetail.type === charge ? (
                  <View className='card-content-price'>
                    {'赠送￥' + filters.moneyFilter(goodsDetail.giftAmount, true)}
                  </View>
                ) : (
                  <View className='card-content-price'>
                    {'有效期：' + (goodsDetail.validityType === 0 ? '永久' : goodsDetail.validity + '天')}
                  </View>
                )}

                <View className='card-content-type'>{goodsDetail.type === coupon ? '次数卡' : '充值卡'}</View>
              </View>
            </View>
            <View className='card-desc'>
              <View className='card-name limit-line'>{goodsDetail.name}</View>
              {goodsDetail.type === charge ? (
                <View className='card-validity'>
                  {'充' +
                    filters.moneyFilter(goodsDetail.salePrice, true) +
                    '送' +
                    filters.moneyFilter(goodsDetail.giftAmount, true) +
                    '元'}
                </View>
              ) : (
                <View className='card-validity'>
                  {'有效期：' + (goodsDetail.validityType === 0 ? '永久' : goodsDetail.validity + '天')}
                </View>
              )}

              <View className='card-price'>
                <Text className='unit'>￥</Text>
                {filters.moneyFilter(goodsDetail.salePrice, true)}
              </View>
              {/*  <View class='card-store'>适用门店</View>
              </View>  */}
              <View className='goods-hr'></View>
              <View className='card-detail-label'>卡项详情</View>
              {!!(!!goodsDetail && !!goodsDetail.itemCardServerList) && (
                <Block>
                  {goodsDetail.itemCardServerList.map((serveItem, index) => {
                    return (
                      <View
                        className='card-serve-item'
                        onClick={_fixme_with_dataset_(this.handletoServer, { goods: serveItem })}
                        key={serveItem.serverItemNo}
                      >
                        <View>
                          <Image className='serve-icon' src={serveItem.serverThumbnail}></Image>
                          <View className='serve-desc'>
                            <View className='name limit-line'>{serveItem.serverItemName}</View>
                            <View className='price'>{'￥' + filters.moneyFilter(serveItem.serverSalePrice, true)}</View>
                            <View className='count'>{serveItem.serverCount + '次'}</View>
                          </View>
                        </View>
                        <Image src={getStaticImgUrl.images.rightAngleGray_png} className='right-icon'></Image>
                      </View>
                    );
                  })}
                </Block>
              )}

              <View wx-if={goodsDetail.type === charge}>
                <Text className='cardExplain'>{goodsDetail.cardExplain}</Text>
              </View>
            </View>
            <BuyNow
              operationText='立即购买'
              onBuyNow={this.handleBuyNow}
              style={'background:' + tmpStyle.btnColor}
              showCart={false}
            ></BuyNow>
          </View>
        )}
      </View>
    );
  }
}

export default CardDetail;
