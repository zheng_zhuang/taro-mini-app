import React, {Component} from 'react';
import {View, WebView} from '@tarojs/components';
import Taro from "@tarojs/taro";
import wxApi from "@/wxat-common/utils/wxApi";
import {$getRouter} from "wk-taro-platform";
import H5UrlEnum from '@/wxat-common/constants/H5UrlEnum.js';
import login from '@/wxat-common/x-login';
import {toJumpYQZ} from '@/wxat-common/utils/yqz';
export default class extends Component {

  async onLoad(options) {
    try {
      if (options.payDataStr) {
        const {cbParams, formKey, cbSuccessRouter, cbFailRouter} = JSON.parse(decodeURIComponent(options.payDataStr))
        if (formKey === '__PAY__') {
          wxApi.setNavigationBarTitle({ title: '支付页面' });
          Taro.requestPayment({
            timeStamp: String(cbParams.timeStamp),
            nonceStr: cbParams.nonceStr,
            package: `prepay_id=${cbParams.prepayId}`,
            signType: cbParams.signType,
            paySign: cbParams.paySign,
            success: function (res) {
              console.log('res: ', res);
              Taro.redirectTo({url: `/sub-packages/server-package/pages/webview/index?url=${encodeURIComponent(H5UrlEnum.YQZ_H5_URL + cbSuccessRouter)}`});
            },
            fail: function (error) {
              console.log('error: ', error);
              Taro.redirectTo({url: `/sub-packages/server-package/pages/webview/index?url=${encodeURIComponent(H5UrlEnum.YQZ_H5_URL + cbFailRouter)}`});
            }
          })
        }
      }

      if (options.loginDataStr) {
        const {formKey, cbRouter } = JSON.parse(decodeURIComponent(options.loginDataStr))
        if (formKey === '__LOGIN__') {
          await login.reLogin()
          toJumpYQZ(cbRouter)
        }
      }
    } catch (error) {
      console.log('error: ', error);
    }
  }

  render() {

    return (
      <View></View>
    );
  }
}
