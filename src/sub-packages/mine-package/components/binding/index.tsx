import React, { ComponentClass } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Text, Input, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';

import './index.scss';
import { connect } from 'react-redux';
import { ITouchEvent } from '@tarojs/components/types/common';

const CODE_INTERVAL = 60;
const phoneReg = /^1[3|4|5|6|8|7][0-9]\d{8}$/; // 手机号码正则

type BindingProps = {
  originalPhone: string;
  onChangedPhone: Function;
};

type StateProps = {
  templ: Record<string, any>;
};

type IProps = BindingProps & StateProps;

interface Binding {
  props: IProps;
}

const mapStateToProps = () => {
  return { templ: template.getTemplateStyle() };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Binding extends React.Component {
  state = {
    code: '',
    phone: '',
    countdown: 0,
  };

  setPhone = (ev: ITouchEvent) => {
    this.setState({ phone: ev.detail.value });
  };

  setCode = (ev: ITouchEvent) => {
    this.setState({ code: ev.detail.value });
  };

  sendCode = () => {
    if (this.state.countdown > 0) {
      return;
    }

    if (!this.state.phone) {
      wxApi.showToast({ title: '请输入手机号', icon: 'none' });
      return;
    }

    if (!phoneReg.test(this.state.phone)) {
      wxApi.showToast({
        title: '请输入11位手机号',
        icon: 'none',
      });

      return;
    }

    wxApi
      .request({
        url: api.userInfo.sendVCode,
        data: { phone: this.state.phone },
      })
      .then(() => {
        wxApi.showToast({ title: '验证码发送成功', icon: 'none' });
        this.setCodeInterval();
      });
  };

  setCodeInterval = () => {
    this.setState({ countdown: CODE_INTERVAL });

    this.interval = setInterval(() => {
      if (this.state.countdown <= 0) {
        clearInterval(this.interval);
        return;
      }
      this.setState({ countdown: this.state.countdown - 1 });
    }, 1000);
  };

  submit = () => {
    if (!phoneReg.test(this.state.phone)) {
      wxApi.showToast({
        title: '请输入11位手机号',
        icon: 'none',
      });

      return;
    }

    if (!this.state.phone || !this.state.code) {
      wxApi.showToast({ title: '请输入手机号和验证码', icon: 'none' });
    } else {
      wxApi
        .request({
          url: api.userInfo.changePhoneNum,
          data: {
            verifyCode: this.state.code,
            phone: this.state.phone,
          },
        })
        .then((res) => {
          if (!res.data) {
            wxApi.showToast({
              title: '验证码错误',
              icon: 'none',
            });
          } else {
            wxApi.showToast({
              title: '修改成功',
              icon: 'none',
            });

            this.props.onChangedPhone();
          }
        });
    }
  };

  interval: ReturnType<typeof setInterval>;

  render() {
    const { countdown } = this.state;
    const { templ, originalPhone } = this.props;

    return (
      <View data-scoped='wk-mcb-Binding' className='wk-mcb-Binding bind-container'>
        {!!originalPhone && (
          <View className='row'>
            <Text className='label'>原手机号:</Text>
            <Input disabled type='text' className='original-input' value={originalPhone}></Input>
          </View>
        )}

        <View className='row'>
          <Text className='label'>新手机号:</Text>
          <Input type='text' className='input' type='number' placeholder='请输入手机号' onInput={this.setPhone}></Input>
        </View>
        <View className='row'>
          <Text className='label'>验证码:</Text>
          <Input type='text' className='input' placeholder='请输入验证码' onInput={this.setCode}></Input>
          <Text className='send-code' style={_safe_style_('color: ' + templ.btnColor)} onClick={this.sendCode}>
            {countdown === 0 ? '发送验证码' : countdown + '秒后重发'}
          </Text>
        </View>
        <Button className='btn-subimt' style={_safe_style_('background: ' + templ.btnColor)} onClick={this.submit}>
          提交
        </Button>
      </View>
    );
  }
}

export default Binding as ComponentClass<BindingProps>;
