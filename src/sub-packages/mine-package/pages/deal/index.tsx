import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import './index.scss';
import imageUtils from '@/wxat-common/utils/image';

import ParseWrapper from '@/wxat-common/components/parse-wrapper';

import wxParse from '@/wxat-common/components/parse-wrapper/wxParse';
import store from '@/store';

const storeData = store.getState();
class Deal extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    tmpStyle: {},
    // 协议内容
    parseContext: null
  };

  /**
   * 生命周期函数--监听页面加载
   */
  componentDidMount() {
    const options = this.$router.params;
    if (options.title) {
      wxApi.setNavigationBarTitle({
        title: options.title || '协议内容',
      });
    }
    this.parseDescribe();
    this.getTemplateStyle();
    // const PAGE_DEAL_HAD_RELOAD = sessionStorage.getItem('PAGE_DEAL_HAD_RELOAD') || 0
    // if(PAGE_DEAL_HAD_RELOAD === 0){
    //   this.set_PAGE_DEAL_HAD_RELOAD()
    // }else if(PAGE_DEAL_HAD_RELOAD === '1'){
    //   window.sessionStorage.setItem('PAGE_DEAL_HAD_RELOAD', 2)
    // }
  }

  componentWillUnmount(){
    const PAGE_DEAL_HAD_RELOAD = sessionStorage.getItem('PAGE_DEAL_HAD_RELOAD') || 0
    if(PAGE_DEAL_HAD_RELOAD === '2'){
      window.sessionStorage.removeItem('PAGE_DEAL_HAD_RELOAD')
    }
  }

  // 要刷新一次才能显示
  set_PAGE_DEAL_HAD_RELOAD(){
    setTimeout(()=>{
      window.sessionStorage.setItem('PAGE_DEAL_HAD_RELOAD', 1)
      window.location.reload()
    }, 100)
  }

  parseDescribe() {
    // let dealContent = protectedMailBox.read('content') || '222';
    let dealContent = storeData.globalData.dealFile
    if (dealContent) {
      try {
        dealContent = imageUtils.richTextFilterCdnImage(dealContent);
        const parseContext = wxParse('html', dealContent);
        this.setState({ parseContext });
      } catch (e) {
        console.log('富文本转换异常', e);
      }
    }
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { parseContext } = this.state;
    return (
      <View data-scoped='wk-mpd-Deal' className='wk-mpd-Deal deal-container'>
        {parseContext ? <ParseWrapper parseContent={parseContext} /> : ''}
      </View>
    );
  }
}

export default Deal;
