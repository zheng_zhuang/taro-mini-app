import React, { Component } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import template from '@/wxat-common/utils/template';
import AddressList from '@/wxat-common/components/address/list/index';
import './index.scss';
import { connect } from 'react-redux';
import { $getRouter } from 'wk-taro-platform';

const mapStateToProps = (state) => ({});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class Address extends Component {
  $router = $getRouter();
  state = {
    isSelect: false
  };
  /*请尽快迁移为 componentDidMount 或 constructor*/
  componentDidMount() {
    const option = this.$router.params;
    this.setState({
      isSelect: option.isSelect
    })
    this.getTemplateStyle();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    return (
      <View data-fixme='03 add view wrapper. need more test' data-scoped='wk-mpa-Address' className='wk-mpa-Address'>
        <AddressList isSelect={this.state.isSelect} />
      </View>
    );
  }
}

export default Address;
