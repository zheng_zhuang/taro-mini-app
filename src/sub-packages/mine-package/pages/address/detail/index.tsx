import React, { Component } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class AddressDetail extends Component {
  render() {
    return (
      <View className='address-detail'>
        <View className='container'>地址详情</View>
      </View>
    );
  }
}

export default AddressDetail;
