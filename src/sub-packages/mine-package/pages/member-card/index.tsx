import React, { Component } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Input, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import template from '@/wxat-common/utils/template';
import './index.scss';

const rules = {
  username: [
    {
      required: true,
      message: '请输入实体会员卡号',
    },

    {
      min: 0,
      max: 32,
      message: '实体会员卡号最多32个字符',
    },
  ],

  credential: [
    {
      required: true,
      message: '请输入会员卡密码',
    },

    {
      min: 0,
      max: 32,
      message: '会员卡密码最多32个字符',
    },
  ],
};

const phone = /^1[3|4|5|6|8|7][0-9]\d{8}$/;
const CODE_INTERVAL = 60;

const mapStateToProps = (state) => ({
  appInfo: state.base.appInfo,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class MemberCard extends Component {
  state = {
    memberCardInfo: {},
    tmpStyle: {},
    countdown: 0,
    bindWayWithPhone: true,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    const { appInfo } = this.props;
    this.setState({
      bindWayWithPhone: appInfo && appInfo.bindWayWithPhone,
    });

    this.getTemplateStyle();
  }

  //实时绑定输入的实体会员卡号
  usernameInput = (e) => {
    const { memberCardInfo } = this.state;
    memberCardInfo.username = e.detail.value.trim();
    this.setState({
      memberCardInfo,
    });
  };

  //实时绑定输入的手机号码
  phoneInput = (e) => {
    const { memberCardInfo } = this.state;
    memberCardInfo.phone = e.detail.value.trim();
    this.setState({
      memberCardInfo,
    });
  };

  //实时绑定输入的验证码
  verifyCodeInput = (e) => {
    const { memberCardInfo } = this.state;
    memberCardInfo.verifyCode = e.detail.value.trim();
    this.setState({
      memberCardInfo,
    });
  };

  //实时绑定输入的会员卡密码
  credentialInput = (e) => {
    const { memberCardInfo } = this.state;
    memberCardInfo.credential = e.detail.value.trim();
    this.setState({
      memberCardInfo,
    });
  };

  sendCode = () => {
    if (this.state.countdown > 0) {
      return;
    }
    if (!this.state.memberCardInfo.phone || !phone.test(this.state.memberCardInfo.phone)) {
      wxApi.showToast({
        title: '请输入正确的手机号',
        icon: 'none',
      });

      return;
    }

    wxApi
      .request({
        url: api.card.sendVerifyCode,
        data: {
          phone: this.state.memberCardInfo.phone,
        },
      })
      .then(() => {
        wxApi.showToast({
          title: '验证码发送成功',
          icon: 'none',
        });

        this.setCodeInterval();
      });
  };

  setCodeInterval() {
    this.setState({
      countdown: CODE_INTERVAL,
    });

    this.interval = setInterval(() => {
      if (this.state.countdown <= 0) {
        clearInterval(this.interval);
        return;
      }
      this.setState({
        countdown: this.state.countdown - 1,
      });
    }, 1000);
  }

  /**
   * 验证输入的参数
   */
  validate() {
    const memberCardInfo = this.state.memberCardInfo;
    for (const key in rules) {
      const value = memberCardInfo[key];
      const rule = rules[key];

      let isValied = true;
      let msg = '';

      for (const r of rule) {
        if (r.required && !value) {
          isValied = false;
          msg = r.message;
          break;
        } else if (r.min || r.max) {
          const len = (value + '').length;
          if (r.min && len < r.min) {
            isValied = false;
            msg = r.message;
            break;
          }
          if (r.max && len > r.max) {
            isValied = false;
            msg = r.message;
            break;
          }
        }
      }
      if (!isValied) {
        wxApi.showToast({
          title: msg,
          icon: 'none',
        });

        return false;
      }
    }
    return true;
  }

  /**
   * 绑定实体会员卡
   */
  bindFunc = () => {
    let url = api.card.bindMemberCard;
    if (this.state.bindWayWithPhone) {
      url = api.card.bindPhoneMemberCard;
      if (!this.state.memberCardInfo.phone || !phone.test(this.state.memberCardInfo.phone)) {
        return wxApi.showToast({
          title: '请输入正确的手机号',
          icon: 'none',
        });
      }
      if (!this.state.memberCardInfo.verifyCode) {
        return wxApi.showToast({
          title: '请输入验证码',
          icon: 'none',
        });
      }
    } else {
      if (!this.validate()) {
        return;
      }
    }

    let param = {
      ...this.state.memberCardInfo,
    };

    wxApi
      .request({
        url: url,
        data: param,
        loading: true,
      })
      .then((res) => {
        if (res.data) {
          wxApi.showModal({
            title: '绑定成功',
            content: '已经成功绑定实体卡，更多权益请咨询客服。',
            showCancel: false,
            success(res) {
              if (res.confirm) {
                wxApi.$navigateTo({
                  url: '/wxat-common/pages/mine/index',
                });
              }
            },
          });
        } else {
          wxApi.showModal({
            title: '绑定失败',
            content: '请核实手机号码是否正确，再试一次',
            showCancel: false,
          });
        }
      })
      .catch((error) => {
        console.log('绑定实体会员卡失败: ' + JSON.stringify(error.data));
      });
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { memberCardInfo, bindWayWithPhone, countdown, tmpStyle } = this.state;
    return (
      <View data-scoped='wk-mpm-MemberCard' className='wk-mpm-MemberCard member-card'>
        <Image className='icon' />
        {!bindWayWithPhone ? (
          <View className='content'>
            <Text className='title'>请输入卡号及密码进行绑定</Text>
            <View className='form'>
              <View className='input-view'>
                <Text>实体会员卡号</Text>
                <Input
                  placeholderClass='placeholder'
                  placeholder='请输入'
                  value={memberCardInfo.username}
                  onBlur={this.usernameInput}
                />
              </View>
              <View className='input-view'>
                <Text>会员卡密码</Text>
                <Input
                  placeholderClass='placeholder'
                  placeholder='请输入'
                  value={memberCardInfo.credential}
                  onBlur={this.credentialInput}
                />
              </View>
            </View>
          </View>
        ) : (
          <View className='content'>
            <Text className='title'>请输入手机号及验证码进行绑定</Text>
            <View className='form'>
              <View className='input-view'>
                <Text>手机号码</Text>
                <Input
                  placeholderClass='placeholder'
                  placeholder='请输入'
                  value={memberCardInfo.phone}
                  onBlur={this.phoneInput}
                />
              </View>
              <View className='input-view'>
                <Text>验证码</Text>
                <View style={_safe_style_('position:relative')}>
                  <Input
                    placeholderClass='placeholder'
                    placeholder='请输入'
                    maxlength='6'
                    value={memberCardInfo.verifyCode}
                    onBlur={this.verifyCodeInput}
                  />

                  <Text className='code' onClick={this.sendCode}>
                    {countdown === 0 ? '获取验证码' : countdown + '秒后重发'}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        )}

        <View className='footer'>
          <Button className='second' onClick={this.bindFunc} style={_safe_style_('background:' + tmpStyle.btnColor)}>
            绑定
          </Button>
        </View>
      </View>
    );
  }
}

export default MemberCard;
