import React from 'react';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import FreeList from '@/wxat-common/components/tabbarLink/free-list/index';
import './index.scss';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
class FreeCenter extends React.Component {
  freeListRef = React.createRef<FreeList>();

  onReachBottom(): void {
    // eslint-disable-next-line
    this.freeListRef.current!.onReachBottom();
  }

  render() {
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-mpf-FreeCenter'
        className='wk-mpf-FreeCenter'
      >
        <FreeList ref={this.freeListRef} />
        <HomeActivityDialog showPage="sub-packages/mine-package/pages/free-center/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default FreeCenter;
