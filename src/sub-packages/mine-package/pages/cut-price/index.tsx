// @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import React, { ComponentClass, Component } from 'react';
import { Block, View, Image, Text, Button } from '@tarojs/components';
import Taro, { Config } from '@tarojs/taro';
import goodsTypeFilters from '@/wxat-common/utils/goodsType.wxs.js';
import filters from '@/wxat-common/utils/money.wxs.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import login from '@/wxat-common/x-login/index.js';
import constants from '@/wxat-common/constants/index.js';
import timer from '@/wxat-common/utils/timer.js';
import template from '@/wxat-common/utils/template.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import checkOption from '@/wxat-common/utils/check-options';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import { connect } from 'react-redux';
const loadMoreStatus = constants.order.loadMoreStatus;
const ONE_DAY = 24 * 3600;
// initiateStatus
/*TIME_OUT(0,"已超时"),
  ON_GOING(1, "砍价中"),
  DONE(2, "已砍到底价"),
  PAID(3, "已支付"),
  REFUNDING(-2, "退款中"),
  REFUND_COMPLETED(-3, "退款完成"),*/
//redux state的在这里定义
type PageStateProps = {};
//redux 的action方法再这里定义
type PageDispatchProps = {};
// 父子组件直接传递的props
type PageOwnProps = {
  iosSetting:any;
  isIOS:boolean
};
// 组件的state声明
type PageState = {
  activityList: Array<Record<string, any>>;
  error: boolean;
  pageNo: number;
  hasMore: boolean;
  loadMoreStatus: any;
  tmpStyle: object
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface CutPrice {
  props: IProps;
}
const mapStateToProps = (state) => {
  return {
    iosSetting:state.globalData.iosSetting,
    isIOS:state.globalData.isIOS
  };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class CutPrice extends Component {
  state = {
    activityList: [],
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    tmpStyle: {},
  };

  _thredId = null

  componentDidMount() {
    this.getTemplateStyle()
    this.listActivityList(false)
  }
  componentWillUnmount(){
    if (this._thredId) {
      timer.deleteQueue(this._thredId);
      this._thredId = null;
    }
  }

  componentDidShow = () =>{
    this._thredId = timer.addQueue(() => {
      this.updateTime(this.state.activityList);
    });
  }

  componentDidHide = () => {
    if (this._thredId) {
      timer.deleteQueue(this._thredId);
      this._thredId = null;
    }
  };
  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.listActivityList(false);
  };

  onPullDownRefresh = () => {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
        activityList: []
      },
      () => {
        this.listActivityList(true)
      }
    )

  };

  onReachBottom = () => {
    if (this.state.hasMore) {
      this.setState(
        {
          pageNo: this.state.pageNo + 1,
          loadMoreStatus: loadMoreStatus.LOADING
        },
        () => {
          this.listActivityList(false);
        }
      );
    }
  };
  listActivityList = (isFromPullDown: boolean | undefined) => {
    this.setState({
      error: false,
      loadMoreStatus: loadMoreStatus.LOADING
    });

    login.login().then(() => {
      wxApi
        .request({
          url: api.cut_price.initiateList,
          data: {
            pageNo: this.state.pageNo,
            pageSize: 10,
          },
        })
        .then((res) => {
          const resData = (res.data || []).map((item) => {
            item.timeRemaining /= 1000;
            return item;
          });

          const activityList = this.state.activityList.concat(resData)

          this.setState(
            {
              activityList
            }
          )

          if (activityList.length === res.totalCount && res.totalCount) {
            this.setState({
              hasMore: false,
              loadMoreStatus: loadMoreStatus.BASELINE,
            })
          } else {
            this.setState({
              hasMore: false,
              loadMoreStatus: loadMoreStatus.HIDE,
            })
          }

        })
        .catch(() => {
          if (this.state.activityList.length) {
            this.setState({
              loadMoreStatus: loadMoreStatus.ERROR
            })
          } else {
            this.setState({
              error: true
            })
          }
        })
        .finally(() => {
          if (isFromPullDown) {
            wxApi.stopPullDownRefresh();
          }
        });
    });
  }

  updateTime = (activityList) => {
    if (!activityList || activityList.length === 0) {
      this.setState({
        activityList,
      });

      return;
    }

    activityList.forEach((activity) => {
      /** TIME_OUT(0, "已超时"),
       ON_GOING(1, "砍价中"),
       DONE(2, "已砍到底价"),
       PAID(3, "已支付"),
       REFUNDING(-2, "退款中"),
       REFUND_COMPLETED(-3, "退款完成"),*/
      /*砍价已删除*/
      if (activity.activityStatus === -1) {
        activity.status = 5;
        activity.statusDesc = '砍价已删除';
        activity.displayTime = '';
        return;
      }
      /*砍价已结束*/
      if (activity.activityStatus === 2) {
        activity.status = 5;
        activity.statusDesc = '砍价已结束';
        activity.displayTime = '';
        return;
      }
      if (activity.initiateStatus === 0) {
        activity.status = 4;
        activity.statusDesc = '砍价已超时';
        activity.displayTime = '';
        return;
      }
      if (activity.initiateStatus === 3) {
        // 砍价成功，并已支付
        activity.status = 1;
        activity.statusDesc = '砍价成功，已购买';
        activity.displayTime = '';
        return;
      }
      if (activity.initiateStatus === 2) {
        // 砍价成功
        activity.status = 2;
        activity.statusDesc = '砍价成功，请尽快购买';
        activity.displayTime = '';
        return;
      }

      if (activity.initiateStatus === 1 && activity.timeRemaining > 0) {
        // 继续砍价中
        activity.status = 3;
        activity.statusDesc = `还差￥${(activity.salePrice - activity.totalBargainPrice - activity.lowestPrice) / 100}`;

        const timeRemaining = activity.timeRemaining;
        const day = parseInt(timeRemaining / ONE_DAY);
        let mod = timeRemaining % ONE_DAY;
        const hour = parseInt(mod / 3600);
        mod = mod % 3600;
        const minute = parseInt(mod / 60);
        const second = parseInt(mod % 60);

        const displayTime = `${day}天${hour}:${minute < 10 ? '0' : ''}${minute}:${
          second < 10 ? '0' : ''
        }${second}后结束`;
        activity.timeRemaining = timeRemaining - 1;
        activity.displayTime = displayTime;
        return;
      }
    });

    this.setState({
      activityList,
    });
  };

  handleGoJoin = (id, initiateId, e: ITouchEvent) => {
    e.stopPropagation();

    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/cut-price/join/index',
      data: {
        bargainId: id,
        bargainInitiateId: initiateId,
      },
    });
  };

  handleGoPay = (e, item) => {
    // this, item.id, item.bargainInitiateId, item
    e.stopPropagation();
    this.getDetail(item.id, item.bargainInitiateId, item)
    // checkOption.changeStore(item.initiateStoreId).then(() => {
    //   this.getDetail(item.id, item.bargainInitiateId, item);
    // });
  };

  handleGoAppointment = (goodsInfo, e: ITouchEvent) => {
    e.stopPropagation();
    goodsInfo.name = goodsInfo.itemName;
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/appointment/index',
      data: {
        goodsInfo: JSON.stringify(goodsInfo),
      },
    });
  };

  getDetail = (id, initiateId, detail) => {
    const goodsDetail = detail;
    // 显示砍价后当前价格
    goodsDetail.currentPrice = goodsDetail.salePrice - goodsDetail.totalBargainPrice;

    const params = {
      bargainId: id, // 活动id
      initiate: false, // 是否参与砍价，join界面为false
      bargainInitiateId: initiateId, // 用户发起砍价的id
      loading: true,
    };
    wxApi
      .request({
        url: api.cut_price.detail,
        data: params,
      })
      .then((res) => {
        const activity = res.data;
        const goodsInfoList = [
          {
            isCutPriceOrder: 1,
            id: activity.id,
            pic: activity.thumbnail,
            salePrice: activity.salePrice,
            itemCount: 1,
            noNeedPay: false,
            name: activity.activityName,
            itemNo: activity.itemNo,
            barcode: activity.barcode,
            skuId: activity.skuId,
            skuTreeNames: activity.itemAttrDesc,
            drugType: activity.drugType, //处方药标识
          },
        ];

        const payDetails = encodeURI(JSON.stringify([
          {
            name: 'freight',
          },
        ]));

        const urlParams = encodeURI(JSON.stringify({
          orderRequestPromDTO: {
            bargainNo: initiateId,
          },
        }));

        let toUrl = pageLinkEnum.orderPkg.payOrder

        if (detail.itemType === 41) {
          toUrl = '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index'
        }

        protectedMailBox.send(toUrl, 'goodsInfoList', goodsInfoList);
        Taro.setStorageSync('GOODS_LIST_CUT', JSON.stringify(goodsInfoList))


        wxApi.$navigateTo({
          url: toUrl,
          data: {
            payDetails: payDetails,
            params: urlParams,
            itemNo: detail.itemNo
          },
        });
      })
      .catch((error) => {
      });
  };

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { activityList, error, tmpStyle, loadMoreStatus } = this.state;
    const {iosSetting,isIOS} = this.props
    return (
      <View data-scoped='wk-mpc-CutPrice' className='wk-mpc-CutPrice serve-list'>
        {(!loadMoreStatus && !activityList.length && !error) && (
          <Empty iconClass='empty-icon' message='暂无参与砍价'></Empty>
        )}

        {error && <Error></Error>}
        {!!activityList.length && (
          <View className='group' style={_safe_style_('margin-top: 10rpx')}>
            {activityList.map((item, index) => {
              return (
                <View
                  key={'group-item-' + index}
                  className='group-item'
                  onClick={this.handleGoJoin.bind(this, item.id, item.bargainInitiateId)}
                >
                  <View>
                    <Image src={item.thumbnail} className='goods-image'></Image>
                  </View>
                  <View style={_safe_style_('flex: 1; padding-right: 30rpx')}>
                    <View className='group-name'>{item.activityName}</View>
                    <View className='group-detail'>
                      <View className='group-detail-inner'>
                        <View className='money' style={_safe_style_('margin-top: 10rpx')}>
                          最低价：
                          <Text>￥{filters.moneyFilter(item.lowestPrice, true)}</Text>
                        </View>
                        {/* <View className='stores'>
                          砍价门店：{item.initiateStoreInfo && item.initiateStoreInfo.name}
                        </View> */}
                        <View className='status-desc'>
                          <Text
                            className='status-desc-text'
                            style={_safe_style_('color: ' + (item.status === 1 ? '#FF813B' : '#FF5B51'))}
                          >
                            {item.statusDesc}
                          </Text>
                          <View className='right'>
                            {item.status === 2 && goodsTypeFilters.goodsType(item.itemNo) === 0 ? (
                              <Button
                                className='btn btn-yellow'
                                onClick={this.handleGoAppointment.bind(this, item)}
                                style={_safe_style_('background:' + tmpStyle.btnColor)}
                              >
                                立即预约
                              </Button>
                            ) : (
                              <View>
                                {item.status === 3 && (
                                  <Button
                                    className='btn btn-red'
                                    onClick={this.handleGoJoin.bind(this, item.id, item.bargainInitiateId)}
                                    style={_safe_style_('background:' + tmpStyle.btnColor)}
                                  >
                                    继续砍价
                                  </Button>
                                )}

                                {item.status === 2 && (
                                  <Button
                                    className='btn btn-yellow'
                                    onClick={(e) => {this.handleGoPay(e, item)}}
                                    style={_safe_style_('background:' + tmpStyle.btnColor)}
                                  >
                                    {isIOS && item.wxItem.itemType == 41 && iosSetting && iosSetting.buttonCopywriting ? iosSetting.buttonCopywriting:'去购买'}
                                  </Button>
                                )}
                              </View>
                            )}
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        )}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default CutPrice as ComponentClass<PageOwnProps, PageState>;
