import { $getRouter } from 'wk-taro-platform';
import React from 'react'; /* eslint-disable react/sort-comp */
import '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro, { Config } from '@tarojs/taro';
import { connect } from 'react-redux';
import hoc from '@/hoc/index';

import filters from '@/wxat-common/utils/money.wxs.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import utilDate from '@/wxat-common/utils/date.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import couponEnum from '@/wxat-common/constants/couponEnum.js';

import QrCode from '@/wxat-common/components/qr-code/index';
import './index.scss';

type StateToProps = {
  appId: string;
};

type IProps = StateToProps;

interface CouponDetailPage {
  props: IProps;
}

const mapStateToProps = (state) => ({
  appId: state.ext.appId,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class CouponDetailPage extends React.Component {
  $router = $getRouter();
  state = {
    coupon: {
      beginTime: '',
      endTime: '',
      couponCategory: null,
      discountFee: null,
      name: '',
      minimumFee: null,
      useScopeType: null,
      code: '',
      status: 1,
    },

    couponTicketId: null,
    couponEnum,
    intervalId: null,
    codeView: 'barcode',
  } as Record<string, any>;

  componentDidMount() {
    const { couponInfoId } = this.$router.params;
    if (couponInfoId) {
      const couponTicketId = JSON.parse(couponInfoId);
      this.getDetail(couponTicketId);
      this.setState({
        couponTicketId,
      });
    }
  }

  componentWillUnmount() {
    this.state.intervalId && clearTimeout(this.state.intervalId);
  }

  onPullDownRefresh() {
    this.getDetail(this.state.couponTicketId);
  }

  handleSwitch = () => {
    const { codeView } = this.state;
    this.setState({
      codeView: codeView === 'qrcode' ? 'barcode' : 'qrcode',
    });
  };

  intervalGetOrderDetail() {
    const intervalId = setTimeout(() => this.getDetail(this.state.couponTicketId, false), 2000);
    this.setState({ intervalId });
  }

  getDetail(couponTicketId, loadingList = true) {
    const reqData = {
      couponTicketId: couponTicketId,
      appId: this.props.appId,
    };

    wxApi
      .request({
        url: api.coupon.tickdetail,
        loading: loadingList,
        checkSession: true,
        data: reqData,
      })
      .then((res) => this.setState({ coupon: res.data }, this.getTime))
      .catch((err) => console.error(err))
      .finally(() => {
        wxApi.stopPullDownRefresh();
        this.intervalGetOrderDetail();
      });
  }

  getTime() {
    const { coupon } = this.state;
    let endTime: string | Date = new Date(coupon.endTime);
    let beginTime: string | Date = new Date(coupon.beginTime);
    endTime = utilDate.format(endTime, 'yyyy-MM-dd');
    beginTime = utilDate.format(beginTime, 'yyyy-MM-dd');

    this.setState({
      // eslint-disable-next-line react/no-unused-state
      'coupon.endTime': endTime,
      // eslint-disable-next-line react/no-unused-state
      'coupon.beginTime': beginTime,
    });
  }

  render() {
    const { coupon, couponEnum, codeView } = this.state;

    return (
      <View data-scoped='wk-pcd-Detail' className='wk-pcd-Detail mine-coupon-detail'>
        <Image className='bg' src={cdnResConfig.coupon.bg}></Image>
        <View className='details'>
          <View className='coupon-detail'>
            <View className='coupon-price'>
              {coupon.couponCategory === couponEnum.TYPE.discount.value ? (
                <View className='discount'>
                  <Text className='num'>{filters.discountFilter(coupon.discountFee, true)}</Text>
                  <Text className='unit'>折</Text>
                </View>
              ) : (
                <View className='discount'>
                  <Text className='num'>{filters.moneyFilter(coupon.discountFee, true)}</Text>
                  <Text className='unit'>元</Text>
                </View>
              )}
            </View>
            <View className='coupon-limit'>
              <View className='min-fee'>{'满' + filters.moneyFilter(coupon.minimumFee, true) + '可用'}</View>
              {/*  暂时不展示券的类型  */}
              {/*  <View class="coupon-type" wx:if="{{item.couponCategory===1 }}">运费券</View>
                        <View class="coupon-type" wx:else>满减券</View>  */}
            </View>
            <View className='coupon-info'>
              <View className='coupon-name limit-line line-2'>{coupon.name}</View>
              <View className='coupon-time'>
                {'有效期：' + filters.dateFormat(coupon.beginTime) + ' 至 ' + filters.dateFormat(coupon.endTime)}
              </View>
            </View>
          </View>
        </View>
        <View className='others'>
          {/* 核销成功 */}
          {coupon.status == 2 && <View className='veri-success'>优惠券已核销</View>}

          {!!(coupon.useScopeType !== 1) && (
            <View className='code-container'>
              {!!coupon.code && (
                <QrCode
                  showTip={false}
                  showBarcode={codeView === 'barcode'}
                  showQrcode={codeView === 'qrcode'}
                  verificationNo={coupon.code}
                  verificationType={2}
                  isScale
                />
              )}

              <View className='switch' onClick={this.handleSwitch}>
                <Image className='switch-icon' src={cdnResConfig.coupon.switch} />
                切换{codeView === 'qrcode' ? '条形码' : '微信扫一扫'}核销
              </View>
            </View>
          )}

          {/*  <View class='goods-hr'></View>  */}
        </View>
      </View>
    );
  }
}

export default CouponDetailPage;
