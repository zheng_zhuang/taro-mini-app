import React, { Component } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { View, ScrollView } from '@tarojs/components';
import { $getRouter, forwardPageHandler } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import hoc from '@/hoc/index';
import scan from '@/wxat-common/utils/scanH5'
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import shareUtil from '@/wxat-common/utils/share.js';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import RoomCode  from '@/wxat-common/components/room-code/index';
import { updateGlobalDataAction } from '@/redux/global-data';
import store from '@/store'
import api from '@/wxat-common/api/index';
import CouponCenter from '@/wxat-common/components/tabbarLink/coupon-center/index';
import './index.scss';

type StateToProps = {
  environment: string;
  userInfo: Record<string, any>;
};

type IProps = StateToProps;

interface CouponPage {
  props: IProps;
}

const mapStateToProps = (state) => ({
  environment: state.globalData.environment,
  userInfo: state.base.userInfo,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class CouponPage extends Component {
  $router = $getRouter()
  state = {
    directBack: false,
    codeId:null,
    updateRoomCode:false
  };

  componentDidMount() {
    const directBack = protectedMailBox.read('directBack');
    this.setState({ directBack: !!directBack });

    // 微信端禁止分享
    // if (this.props.environment !== 'wxwork') {
    //   wxApi.hideShareMenu();
    // }
    this.init()
  }
  init = async () =>{
    const options = this.$router.params
    if(process.env.WX_OA === 'true' && options.id){
      this.setState({
        codeId:options.id
      })
      const data:any = await scan.scanRoomCodeFunc(options)
      if(data && data.isScan){
        this.setState({
          updateRoomCode:data.isScan
        })
      }
    }
}
  onShareAppMessage() {
    const { userInfo } = this.props;
    let title = '发来一个优惠券礼包';
    const url = 'sub-packages/mine-package/pages/coupon/index';
    if (userInfo && userInfo.nickname) {
      title = userInfo.nickname + title;
    }
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.COUPON_CENTER,
      bzName: '领券中心',
    });

    return {
      title,
      path,
      imageUrl: cdnResConfig.wechatShare.coupon,
    };
  }
  onIsUpdateRoomCode = () =>{
    this.setState({
      updateRoomCode:false
    })
  }
  render() {
    return (
      <View className="coupon-center-wrap">
        <CouponCenter directBack={this.state.directBack} />
        <HomeActivityDialog showPage={'sub-packages/moveFile-package/pages/mine/coupon/index'}></HomeActivityDialog>
        <RoomCode onIsUpdateRoomCode={this.onIsUpdateRoomCode} codeId={this.state.codeId} updateRoomCode={this.state.updateRoomCode}></RoomCode>
      </View>
    )
  }
}

export default CouponPage;
