import React, { Component } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Input, Icon } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import filters from '@/wxat-common/utils/money.wxs';
import wxApi from '@/wxat-common/utils/wxApi';
import floatNumUtils from '@/wxat-common/utils/float-num';
import api from '@/wxat-common/api/index';
import constants from '@/wxat-common/constants/index';
import pay from '@/wxat-common/utils/pay';
import template from '@/wxat-common/utils/template';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import authHelper from '@/wxat-common/utils/auth-helper';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';
import CouponDialog from '@/wxat-common/components/coupon-dialog/index';
import './index.scss';

const PAY_CHANNEL = constants.order.payChannel;

const mapStateToProps = (state) => ({
  currentStore: state.base.currentStore,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class RechargeCenter extends Component {
  state = {
    activeCharge: null,
    rechargeList: [],
    balance: 0,
    selectIndex: -1, //0
    //现金充值
    amount: '',
    //是否是有效的现金额
    valid: false,
    tmpStyle: {},
    //是否确认协议
    dealClick: true,
    //充值协议
    dealData: null,
    dialogShow: false,
    coupon: [],
    payOrderNo: null,
    current: -1,
    isOtherPayShow: false,
    isForbidRecharge: 1,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    this.getDeal();
    this.getTemplateStyle();
    this.getBalance();
    this.fetchCards();
    this.fetchForbidStatus();
  }

  componentDidShow() {
    this.setState({
      selectIndex: -1,
    });

    this._payInfo = null;
    this._payItem = null;
  }

  fetchForbidStatus() {
    // 获取是否手动充值设置
    return wxApi
      .request({
        url: api.recharge.isForbid,
        quite: true,
        data: {},
      })
      .then((res) => {
        if (res.data) {
          // 0: 允许，1：不允许
          this.setState({
            isForbidRecharge: res.data.forbidRecharge,
          });
        }
      });
  }

  getBalance() {
    return wxApi
      .request({
        url: api.userInfo.balance,
        quite: true,
        data: {},
      })
      .then((res) => {
        this.setState({
          balance: res.data.balance,
        });
      })
      .catch((error) => {
        console.log('balance: error: ' + JSON.stringify(error));
      });
  }

  onAmountChange = (e) => {
    const amount = e.detail.value;
    const reg = /^\d+(\.\d{0,2})?$/;

    this.setState(
      {
        amount: e.detail.value,
        valid: reg.test(amount) && Number(amount) > 0 && Number(amount) < 9999.99,
      },

      () => {
        if (!this.state.valid) {
          wxApi.showToast({
            title: '请输入0~9999.99之间最多两位小数的数字',
            icon: 'none',
            duration: 2000,
          });
        }
      }
    );
  };

  handleRecharge = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    if (!this.judgeDealClick()) return;

    // 开启了手动充值 提交时做校验判断
    if (!this.state.valid && !this.state.isForbidRecharge) {
      wxApi.showToast({
        title: '请输入0~9999.99之间最多两位小数的数字',
        icon: 'none',
        duration: 2000,
      });
    }

    if (this.state.valid) {
      //充值成功的订阅消息
      let Ids = [subscribeEnum.CHARGE_SUCCESS.value, subscribeEnum.PAY_SUCCESS.value];
      //授权订阅消息
      subscribeMsg.sendMessage(Ids).then(() => {
        pay.submitPay(
          api.recharge.payOrder,
          {
            amount: floatNumUtils.floatMul(Number(this.state.amount), 100),
          },

          PAY_CHANNEL.WX_PAY,
          {
            wxPaySuccess: (res) => {
              wxApi.navigateBack({
                delta: 1,
              });
            },
            wxPayFail: (error) => {},
          }
        );
      });
    }
  };

  onCardPay = (e) => {
    if (!authHelper.checkAuth()) {
      return;
    }
    if (!this.judgeDealClick()) return;
    this.setState({
      selectIndex: e.currentTarget.dataset.index,
      isOtherPayShow: false,
    });

    const newItem = e.currentTarget.dataset.item;
    const orderInfo = {
      itemDTOList: [
        {
          itemCount: 1,
          itemNo: newItem.itemNo,
          type: newItem.type,
        },
      ],

      formId: !e.detail || e.detail.formId === 'the formId is a mock one' ? null : e.detail.formId,
    };

    if (this._payItem && this._payItem.id !== newItem.id) {
      this._payItem = null;
      this._payInfo = null;
    }
    const _this = this;
    //充值成功的订阅消息
    let Ids = [subscribeEnum.CHARGE_SUCCESS.value, subscribeEnum.PAY_SUCCESS.value];
    //授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      pay.handleBuyNow(orderInfo, PAY_CHANNEL.WX_PAY, {
        payInfo: this._payInfo,
        success(res) {
          console.log(res);
          _this._payInfo = res;
          _this._payItem = e.currentTarget.dataset.item;

          _this.setState({
            payOrderNo: res.data.orderNo,
          });
        },
        wxPaySuccess() {
          // _this.fetchSendCoupons(_this.state.payOrderNo)
          _this.autoFetchCoupons(_this.state.payOrderNo);
          _this._payInfo = null;
          _this._payItem = null;
        },
      });
    });
  };

  onOtherCardPay = (e) => {
    this.setState({
      selectIndex: e.currentTarget.dataset.index,
      isOtherPayShow: true,
    });
  };

  fetchCards() {
    const { currentStore } = this.props;
    wxApi
      .request({
        url: api.card.list,
        loading: true,
        data: {
          storeId: currentStore.id,
          isShelf: 1,
          status: 1,
          pageNo: 100,
          type: constants.card.type.charge,
        },
      })
      .then((res) => {
        if (!res.data || !res.data.length) {
          this.setState({
            isOtherPayShow: true,
          });
        }
        this.setState({
          rechargeList: res.data,
        });
      });
  }

  onFocus = () => {
    this.setState({
      selectIndex: -1,
    });
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //转到充值协议页面
  seeDeal = () => {
    protectedMailBox.send('sub-packages/mine-package/pages/deal/index', 'content', this.state.dealData.content);
    wxApi.$navigateTo({
      url: `/sub-packages/mine-package/pages/deal/index`,
      data: {
        title: '充值协议',
      },
    });
  };

  //点击确认协议
  verifyDeal = () => {
    this.setState({
      dealClick: !this.state.dealClick,
    });
  };

  //判断是否确认充值协议
  judgeDealClick() {
    if (!this.state.dealClick) {
      //点击状态
      wxApi.showToast({
        title: '请确认协议信息',
        icon: 'none',
        duration: 1500,
      });

      return false;
    }
    return true;
  }

  //查询充值协议
  getDeal() {
    wxApi
      .request({
        url: api.deal,
      })
      .then((res) => {
        this.setState({
          dealData: res.data,
        });
      });
  }

  //自动领取优惠券
  autoFetchCoupons(orderNo) {
    let reqData = {
      eventType: 5,
      orderNo: orderNo,
    };

    wxApi
      .request({
        url: api.coupon.auto_collect,
        loading: true,
        checkSession: true,
        data: reqData,
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            coupon: res.data || [],
            dialogShow: true,
          });
        }
      })
      .catch((error) => {});
  }

  // 获取可用的优惠券列表
  fetchSendCoupons(orderNo) {
    wxApi
      .request({
        url: api.coupon.get_coupons,
        loading: false,
        quite: true,
        data: { orderNo: orderNo, eventType: 5 },
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            coupon: res.data || [],
            dialogShow: true,
          });
        }
        console.log(this.state.coupon);
      })
      .catch((error) => {});
  }

  // 优惠券弹窗关闭后跳转页面
  handleCouponDialog = () => {
    wxApi.navigateBack({
      delta: 1,
    });
  };

  render() {
    const {
      tmpStyle,
      balance,
      selectIndex,
      isForbidRecharge,
      rechargeList,
      isOtherPayShow,
      dealData,
      dealClick,
      dialogShow,
      coupon,
      payOrderNo,
    } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-mpr-RechargeCenter'
        className='wk-mpr-RechargeCenter'
      >
        <View className='recharge-center'>
          <View className='header' style={_safe_style_('background:' + tmpStyle.btnColor)}>
            <View className='header__title'>账户余额(元)</View>
            <View className='header__balance'>{filters.moneyFilter(balance, true)}</View>
          </View>
          {!!(rechargeList || rechargeList.length) && (
            <View className='list-container'>
              {rechargeList.map((item, index) => {
                return (
                  <View
                    className={'item ' + (selectIndex == index ? 'active' : '')}
                    onClick={_fixme_with_dataset_(this.onCardPay, { index: index, item: item })}
                    key={index}
                    style={_safe_style_(
                      selectIndex == index
                        ? 'border-color:' +
                            tmpStyle.btnColor +
                            ';color:' +
                            tmpStyle.btnColor +
                            ';background:' +
                            tmpStyle.bgColor
                        : ''
                    )}
                  >
                    <View className='amount'>{filters.moneyFilter(item.salePrice, true) + '元'}</View>
                    {!!item.special && (
                      <View className='new-icon'>
                        <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/mine-package/new-user.png" className='new-img' />
                      </View>
                    )}

                    {item.giftAmount > 0 && (
                      <View className='giftAmount'>{'送' + filters.moneyFilter(item.giftAmount, true) + '元'}</View>
                    )}
                  </View>
                );
              })}
              {!!(!isForbidRecharge && (rechargeList || rechargeList.length)) && (
                <View
                  className={'item ' + (selectIndex == -2 ? 'active' : '')}
                  onClick={_fixme_with_dataset_(this.onOtherCardPay, { index: -2 })}
                  style={_safe_style_(
                    selectIndex == -2
                      ? 'border-color:' +
                          tmpStyle.btnColor +
                          ';color:' +
                          tmpStyle.btnColor +
                          ';background:' +
                          tmpStyle.bgColor
                      : ''
                  )}
                >
                  <View style={_safe_style_(`font-size:${Taro.pxTransform(28)};`)}>其他金额</View>
                </View>
              )}
            </View>
          )}

          {!!(rechargeList || rechargeList.length || !isForbidRecharge) && (
            <View
              className='footer'
              style={_safe_style_(!rechargeList || !rechargeList.length ? `margin-top: ${Taro.pxTransform(40)}` : '')}
            >
              {!!(!isForbidRecharge && isOtherPayShow) && (
                <View>
                  <View>手动输入金额(￥)</View>
                  <Input
                    type='digit'
                    className='input'
                    onInput={this.onAmountChange}
                    onFocus={this.onFocus}
                    placeholder='手动充值不参与余额赠送优惠活动'
                  />

                  {/*  <View class="invalid-tip" wx:if="{{!valid}}">请输入0~9999.99之间最多两位小数的数字</View>  */}
                </View>
              )}

              {!!dealData && (
                <View className='deal-option'>
                  <View className='deal-btn' onClick={this.verifyDeal}>
                    <Icon type='success' size='15' color={!dealClick ? '#f0f1f5' : tmpStyle.btnColor} />
                    同意并阅读相关协议
                  </View>
                  <View className='deal-label' onClick={this.seeDeal}>
                    《充值协议》
                  </View>
                </View>
              )}

              {!!(!isForbidRecharge && isOtherPayShow) && (
                <View
                  className='recharge-btn'
                  onClick={this.handleRecharge}
                  style={_safe_style_('background:' + tmpStyle.btnColor)}
                >
                  立即充值
                </View>
              )}
            </View>
          )}

          {!!((!rechargeList || !rechargeList.length) && isForbidRecharge) && (
            <View className='non-tip'>暂无可用的充值卡</View>
          )}
        </View>
        {!!(coupon && coupon.length) && (
          <CouponDialog
            visible={dialogShow}
            coupon={coupon}
            payOrderNo={payOrderNo}
            eventType='5'
            onHandleCouponDialog={this.handleCouponDialog}
          />
        )}
      </View>
    );
  }
}

export default RechargeCenter;
