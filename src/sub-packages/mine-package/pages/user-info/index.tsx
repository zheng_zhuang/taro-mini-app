import { $getRouter } from 'wk-taro-platform';
import React, { Component } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Text, Label, Input, Image, Picker, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import utilDate from '@/wxat-common/utils/date';
import template from '@/wxat-common/utils/template';

import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'
import './index.scss';

const rules = {
  realName: [
    {
      required: true,
      message: '请输入姓名',
    },

    {
      reg: /^[0-9A-Za-z\u4e00-\u9fa5]+$/i,
      message: '姓名由字母、数字或汉字组成',
    },

    {
      min: 0,
      max: 8,
      message: '姓名最多8个字符',
    },
  ],

  phone: [
    {
      required: true,
      message: '请输入手机号码',
    },

    {
      reg: /^1[3|4|5|6|8|7][0-9]\d{8}$/,
      message: '手机号码无效',
    },
  ],

  birthday: [
    {
      required: true,
      message: '请选择生日',
    },
  ],

  gender: [
    {
      required: true,
      message: '请选择性别',
    },
  ],
};

const mapStateToProps = (state) => ({});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class UserInfo extends Component {
  $router = $getRouter();
  state = {
    userInfo: {},
    hiddenGenderSelect: true,
    end: utilDate.format(new Date()),
    tmpStyle: {},
    soure: null,
  }; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    const options = this.$router.params;
    if (options.soure) {
      this.setState({
        soure: options.soure,
      });
    }
    this.getTemplateStyle();
  }

  componentDidShow() {
    // 获取用户的资料详情
    wxApi
      .request({
        url: api.userInfo.detail,
        loading: true,
      })
      .then((res) => {
        const data = res.data;
        console.log(res);
        data.birthday = data.birthday || '';
        this.setState({
          userInfo: data,
        });
      })
      .catch((error) => {
        console.log('获取用户信息失败: ' + JSON.stringify(error));
      });
  }

  validate() {
    const userInfo = this.state.userInfo;
    for (const key in rules) {
      const value = userInfo[key];
      const rule = rules[key];

      let isValied = true;
      let msg = '';

      for (const r of rule) {
        if (r.required && !value) {
          isValied = false;
          msg = r.message;
          break;
        } else if (r.reg && !r.reg.test(value)) {
          isValied = false;
          msg = r.message;
          break;
        } else if (r.min || r.max) {
          const len = (value + '').length;
          if (r.min && len < r.min) {
            isValied = false;
            msg = r.message;
            break;
          }
          if (r.max && len > r.max) {
            isValied = false;
            msg = r.message;
            break;
          }
        }
      }
      if (!isValied) {
        wxApi.showToast({
          title: msg,
          icon: 'none',
        });

        return false;
      }
    }
    return true;
  }

  save = () => {
    if (!this.validate()) {
      return;
    }
    // userId wxNickname realName gender birthday phone
    const param = {
      ...this.state.userInfo,
    };

    // 头像不要上传
    delete param.avatarImgUrl;
    wxApi
      .request({
        url: api.userInfo.update,
        data: param,
        loading: true,
      })
      .then((res) => {
        wxApi.showToast({
          title: '修改成功',
        });
      })
      .catch((error) => {
        console.log('获取用户信息失败: ' + JSON.stringify(error));
      });
  };

  nameInput = (e) => {
    const { userInfo } = this.state;
    userInfo.realName = e.detail.value.trim();
    this.setState({
      userInfo,
    });
  };

  phoneInput = (e) => {
    const { userInfo } = this.state;
    userInfo.phone = e.detail.value.trim();
    this.setState({
      userInfo,
    });
  };

  changePhoneNum = (e) => {
    wxApi.$navigateTo({
      url: '/sub-packages/mine-package/pages/change-phone/index',
      data: {
        phone: this.state.userInfo.phone,
      },
    });
  };

  bindDateChange = (e) => {
    const { userInfo } = this.state;
    userInfo.birthday = e.detail.value;
    this.setState({
      userInfo,
    });
  };

  onGenderShow = () => {
    this.setState({
      hiddenGenderSelect: false,
    });
  };

  onGenderHidden = () => {
    this.setState({
      hiddenGenderSelect: true,
    });
  };

  onMaleTap = () => {
    this.updateGender(1);
  };

  onFemaleTap = () => {
    this.updateGender(2);
  };

  updateGender(value) {
    const { userInfo } = this.state;
    userInfo.gender = value;
    this.setState({
      userInfo,
      hiddenGenderSelect: true,
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { userInfo, end, tmpStyle, hiddenGenderSelect } = this.state;
    return (
      <View data-scoped='wk-mpu-UserInfo' className='wk-mpu-UserInfo user-info'>
        <View className='form'>
          <View className='input-view'>
            <Text className='input-view__text'>微信：</Text>
            <Label className='input-view__label' style={_safe_style_('color:rgba(162,162,164,1);')}>
              {userInfo.wxNickname || ''}
            </Label>
          </View>
          <View className='input-view'>
            <Text className='input-view__text'>姓名：</Text>
            <Input
              className='input-view__input'
              placeholderClass='placeholder'
              placeholder='请输入姓名'
              value={userInfo.realName}
              onBlur={this.nameInput}
            />
          </View>
          <View className='input-view' onClick={this.onGenderShow}>
            <Text className='input-view__text'>性别：</Text>
            {!userInfo.gender ? (
              <Label className='input-view__label' style={_safe_style_('color:#A2A2A4')}>
                请选择性别
              </Label>
            ) : (
              <Label className='input-view__label'>{userInfo.gender === 1 ? '男' : '女'}</Label>
            )}

            <Image className='right-icon' src={getStaticImgUrl.images.rightAngleGray_png} />
          </View>
          <View className='input-view'>
            <Picker mode='date' className='picker' end={end} value={userInfo.birthday} onChange={this.bindDateChange}>
              <Text className='input-view__text placeholder'>生日：</Text>
              {!userInfo.birthday ? (
                <Label className='input-view__label' style={_safe_style_('color:#A2A2A4')}>
                  请选择生日
                </Label>
              ) : (
                <Label className='input-view__label'>{userInfo.birthday}</Label>
              )}

              <Image className='right-icon' src={getStaticImgUrl.images.rightAngleGray_png} />
            </Picker>
          </View>
          <View className='input-view' onClick={this.changePhoneNum}>
            <Text className='input-view__text'>手机号：</Text>
            <Input
              placeholderClass='placeholder'
              className='phone input-view__input '
              disabled
              value={userInfo.phone}
              onBlur={this.phoneInput}
              type='number'
              placeholder='请输入手机号码'
            />

            <Image className='right-icon' src={getStaticImgUrl.images.rightAngleGray_png} />
          </View>
        </View>
        <View className='footer'>
          <Button className='second' onClick={this.save} style={_safe_style_('background:' + tmpStyle.btnColor)}>
            保存
          </Button>
        </View>
        <View className={(hiddenGenderSelect ? 'global-hidden ' : '') + 'mask'} onClick={this.onGenderHidden}>
          <View className='mask-body'>
            <View className='gender-item male' onClick={this.onMaleTap}>
              男
            </View>
            <View className='gender-item' onClick={this.onFemaleTap}>
              女
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default UserInfo;
