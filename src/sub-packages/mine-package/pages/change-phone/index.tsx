import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';

import Binding from '../../components/binding/index';
import hoc from '@/hoc';

@hoc
class ChangePhone extends React.Component {
  $router = $getRouter();
  onPhoneChanged = () => {
    wxApi.navigateBack({ delta: 1 });
  };

  render() {
    const { phone } = this.$router.params;
    return (
      <View className='change-phone'>
        <Binding onChangedPhone={this.onPhoneChanged} originalPhone={phone}></Binding>
      </View>
    );
  }
}

export default ChangePhone;
