import Taro from '@tarojs/taro';
import { Component } from 'react';
import { Image, View } from '@tarojs/components';
import './index.scss';
class PrizeDetailPage extends Component {
  state = {
    id: '',
  }

  render() {
    const { id } = this.state;
    console.log(id)
    return (
      <View className='prizeDetailCommon'>
        <View className='displayInformation'>
          <View className='top'>
            <View className='left'>
              <View className='name'>1天观影卡</View>
              <View className='time t1'>2020-12-12 12:12:12</View>
              <view className='info t1'>全国通用</view>
            </View>
            <View className='right'>
              <Image className='img' src='https://material-source.ctlife.tv/d0d53a56de914daca1a707e517256300.jpg' mode='aspectFill'></Image>
            </View>
          </View>
          <View className='instructions'>
            <View className='title'>使用说明</View>
            <View className='textBox'>
              用户参与活动即可获得兑换码1个：
              用户获得兑换码后，复制兑换码至【喜茶G0】小程序→点击”喜贵宾“→”兑换中心“→”喜茶券“→粘贴兑换码即可完成兑换；优惠券使用期限：2023年4月30日前。
            </View>
          </View>
        </View>
        <View className='infoBox'>
          <View className='title'>领取信息</View>
          <View className='item'>领取时间：2023-03-2221:56:45</View>
          <View className='item'>领取数量：1</View>
          <View className='item'>权益价值：¥19.90</View>
          <View className='item'>领取方式：任务奖励</View>
        </View>
      </View>
    );
  }
}

export default PrizeDetailPage;
