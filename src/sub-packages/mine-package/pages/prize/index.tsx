
import Taro from '@tarojs/taro';
import { View, ScrollView, Image } from '@tarojs/components'
import { Component } from 'react'
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { connect } from 'react-redux';


const mapStateToProps = (state) => ({
  themeConfig: state.globalData.themeConfig || null,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })

class PrizePage extends Component {
  state = {
    pageNo: 1,
    hasMore: true,
    list: [
      {id: 1, name: '1天观影卡', state: 1, time: '2020-12-12 12:12:12'},
    ],
    error: false,
    loadMoreStatus: 'LOADING',
    statusType: [
      {label: '全部', value: 0},
      {label: '待使用', value: 1},
      {label: '已使用', value: 2},
      {label: '已过期', value: 3},
    ],
    currentActive: 0,
  }

  handlerScroll = () => {
    console.log('handlerScroll')
    if (!this.state.hasMore) return false
    this.setState(
      {
        pageNo: this.state.pageNo + 1
      },
      () => {
        this.getList()
      }
    )
  }

  onRetryLoadMore = () => {
    console.log('onRetryLoadMore')
  }

  getList = () => {
    console.log(this.state.list, this.state.error, this.state.loadMoreStatus)
    wxApi
      .request({
        url: api.prize.list,
        loading: false,
        checkSession: true,
        data: {
          pageSize: 10,
          pageNo: this.state.pageNo
        }
      })
  }

  // 跳转到详情
  handleGoToDetail = (e: any) => {
    const item = e.currentTarget.dataset.order;
    // const index = e.currentTarget.dataset.index;
    const params = {
      id: item.id,
    };
    wxApi.$navigateTo({
      url: `/sub-packages/mine-package/pages/prize-detail/index?id=${item.id}`,
      data: params
    })
  }

  render() {
    const { list, error, loadMoreStatus, statusType, currentActive } = this.state;
    let {themeConfig}  = this.props;

    if (!themeConfig) {
      themeConfig = {template: '#FF7000'}
    }
    return (
      <View className='prizePageCommon'>
        <View className='status-box'>
          {statusType.map((item, index) => {
            return (
              <View
                className={'status-label ' + (item.value === currentActive ? 'active' : '')}
                key={index}
              >
                {item.label}
                {item.value === currentActive ? (<View className='line' style={'background-color:' + themeConfig.template.themeColor}></View>) : ''}
              </View>
            );
          })}
        </View>
        <ScrollView
          scrollY
          scrollWithAnimation
          scrollAnchoring
          onScrollToLower={this.handlerScroll}>
          <View>
            {!error && !list.length && !loadMoreStatus &&  <Empty message='暂无数据'></Empty>}
            {error && <Error></Error>}
            <View className='list'>
              {!error && list.map((item, index) => {
                return (
                  <View className='item' key={index} onClick={_fixme_with_dataset_(this.handleGoToDetail, { target: item, index: index })}>
                    <View className='top'>
                      <View className='left'>
                        <View>
                          <Image className='img' src='https://material-source.ctlife.tv/d0d53a56de914daca1a707e517256300.jpg' mode='aspectFill'></Image>
                          <View className='txt'>无门槛</View>
                        </View>
                      </View>
                      <View className='right'>
                        <View className='name'>{item.name}</View>
                        <View className='tag-box'>
                          <View className='tag'>兑换券</View>
                        </View>
                        <View className='time'>{item.time}</View>
                        <View className='btn'>去使用</View>
                      </View>
                    </View>
                    <View className='bottom'>使用条件：仅支持首次入住亚朵酒店用户</View>
                  </View>
                )
              })}
              <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}
export default PrizePage;
