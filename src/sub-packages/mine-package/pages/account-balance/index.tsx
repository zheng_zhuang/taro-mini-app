import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
// pages/mine/account-balance/index
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import utilDate from '@/wxat-common/utils/date';
import template from '@/wxat-common/utils/template';

import AuthUser from '@/wxat-common/components/auth-user';
import WaterBillDetail from '@/wxat-common/components/water-bill-detail/index';
import Error from '@/wxat-common/components/error/error';
import './index.scss';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';

type StateProps = {
  templ: any;
};

type IProps = StateProps;

interface AccountBalance {
  props: IProps;
}

const mapStateToProps = () => {
  return { templ: template.getTemplateStyle() };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class AccountBalance extends React.Component {
  state = {
    balance: 0,
    pageNo: 1,
    hasMore: true,
    userReady: false,
    billList: [],
    error: false,
    dateTime: { startTime: '', endTime: '' },
  };

  componentDidShow() {
    if (this.state.userReady) {
      if (!this._reload) {
        this._reload = true;
        return;
      }
      this.getBalance();
      this.getBill();
    }
  } /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    const { templ } = this.props;
    if (templ.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff',
        backgroundColor: templ.titleColor,
      });
    }
  }

  private _reload = false;

  clickRecharge = () => {
    wxApi.$navigateTo({ url: '/sub-packages/mine-package/pages/recharge-center/index' });
  };

  clickGo = () => {
    wxApi.$navigateTo({ url: '/sub-packages/mine-package/pages/recharge-center/index' });
  };

  getBalance = async () => {
    try {
      const { data } = await wxApi.request({ url: api.userInfo.balance, data: {} });
      this.setState({ balance: data.balance });
    } catch (e) {
      console.log('balance: error: ' + JSON.stringify(e));
    }
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  onDateChange = (value) => {
    const time = new Date(value);
    const startTime = utilDate.format(time);

    time.setMonth(time.getMonth() + 1);
    const endTime = utilDate.format(time);

    this.setState(
      {
        pageNo: 1,
        dateTime: { startTime, endTime },
      },

      () => {
        this.getBill();
      }
    );
  };

  /**
   * @func 获取流水账单
   * @param {Boolean} isPullDownRefresh - 是否通过下拉刷新操作
   */
  getBill = async (isPullDownRefresh?: boolean) => {
    this.setState({ error: false });

    try {
      const requestData = { ...this.state.dateTime, page: this.state.pageNo, pageSize: 10 };

      const res = await wxApi.request({ url: api.bill.query, loading: true, data: requestData });
      const data = res.data || [];

      let billList: any[] = [];
      if (this.isLoadMoreRequest()) {
        billList = [...this.state.billList, ...data];
      } else {
        billList = data || [];
      }

      let hasMore = true;
      if (billList.length === res.totalCount) {
        hasMore = false;
      }
      this.setState({ billList, hasMore, pageNo: this.state.pageNo + 1 });

      if (isPullDownRefresh) {
        wxApi.stopPullDownRefresh();
      }
    } catch (e) {
      this.setState({ error: true });
      if (isPullDownRefresh) {
        wxApi.stopPullDownRefresh();
      }
    }
  };

  userInfoReady = () => {
    this.setState({ userReady: true });
    this.getBalance();
    this.getBill();
  };

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh = () => {
    // 页面相关事件处理函数--监听用户下拉动作
    this.setState({ pageNo: 1, hasMore: true }, () => {
      this.getBalance();
      this.getBill(true);
    });
  };

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom = () => {
    // 页面上拉触底事件的处理函数
    if (this.state.hasMore) {
      this.getBill();
    }
  };

  getGroupBills = () => {
    const billList = this.state.billList;
    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth();
    let dispplayMonth = '';
    let monthList: any = null;
    const list: any[] = [];
    billList.forEach((item: any) => {
      const billDate = new Date(item.createTime);
      const billYear = billDate.getFullYear();
      const billMonth = billDate.getMonth();

      let display = '';

      if (billYear === year) {
        if (month === billMonth) {
          display = '本月';
        } else {
          display = `${billMonth + 1}月`;
        }
      } else {
        display = utilDate.format(billDate, 'yyyy年MM月');
      }

      if (display !== dispplayMonth) {
        dispplayMonth = display;
        monthList = {
          display,
          bills: [],
        };

        list.push(monthList);
      }
      monthList.bills.push({
        createTime: utilDate.format(billDate, 'yyyy/MM/dd hh:mm:ss'),
        amount: item.amount,
        desc: item.orderDesc,
      });
    });
    return list;
  };

  render() {
    const { balance, error } = this.state;
    const { templ } = this.props;
    const groupBills = this.getGroupBills();

    return (
      <View data-scoped='wk-mpa-AccountBalance' className='wk-mpa-AccountBalance account-balance'>
        <AuthUser onReady={this.userInfoReady}>
          <View className='header' style={{ background: templ.btnColor }}>
            <View className='header__title'>账户余额(元)</View>
            <View className='header__balance'>{filters.moneyFilter(balance, true)}</View>
            <View className='header__btn' onClick={this.clickRecharge} style={{ color: templ.btnColor }}>
              充值
            </View>
          </View>
          {!!error && <Error></Error>}
          {!error && (
            <WaterBillDetail isMoney onDateChangeEvent={this.onDateChange} dataSource={groupBills}></WaterBillDetail>
          )}
        </AuthUser>
      </View>
    );
  }
}

export default AccountBalance;
