import React, { Component } from 'react';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import constant from '@/wxat-common/constants/index';
import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl.js'
// 获取会员码的定时器
import './index.scss';

const memCodeCheck = {
  loopId: -1,
  intervalTime: 30000,
};

const mapStateToProps = (state) => ({});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class MemberCode extends Component {
  state = {
    membershipCode: {
      barCode: '',
      error: false,
    },
  }; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    this.startGettingBarCode();
  }

  componentWillUnmount() {
    this._stopMemCodeInterval();
  }

  // 开始循环获取会员码
  startGettingBarCode = () => {
    this._getBarCodePath();
    this._stopMemCodeInterval();
    memCodeCheck.loopId = setInterval(() => {
      this._getBarCodePath();
    }, memCodeCheck.intervalTime);
  };

  // 获取会员码
  _getBarCodePath() {
    wxApi
      .request({
        url: api.membershipCode.getBarCode,
        loading: true,
        checkSession: true,
        // data: reqParam
      })
      .then((res) => {
        const { membershipCode } = this.state;
        membershipCode.barCode = res.data ? constant.PREFIX_BASE64 + res.data : '';
        membershipCode.error = false;
        this.setState({
          membershipCode,
        });
      })
      .catch((err) => {
        const { membershipCode } = this.state;
        membershipCode.error = true;
        this._stopMemCodeInterval();
        this.setState({
          membershipCode,
        });
      });
  }

  _stopMemCodeInterval() {
    if (memCodeCheck.loopId) {
      clearInterval(memCodeCheck.loopId);
    }
  }

  handleShowLargeImage = (e) => {
    wxApi.previewImage({
      current: '', // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.src], // 需要预览的图片http链接列表
    });
  };

  render() {
    const { membershipCode } = this.state;
    return (
      <View data-scoped='wk-mpm-MemberCode' className='wk-mpm-MemberCode membership-code'>
        <View className='membership-header'>
          <Image className='membership-header__image' src={getStaticImgUrl.mine.group_png}></Image>
          <Text className='membership-header__text'>会员码</Text>
        </View>
        <View className='mem-code-container'>
          {/* {membershipCode.error && <Error />} */}
          {!membershipCode.error && (
            <View className='mem-code-content'>
              <View>
                <View className='item mem-code-area'>
                  <Image
                    className='bar-code'
                    mode='aspectFit'
                    src={membershipCode.barCode}
                    onClick={_fixme_with_dataset_(this.handleShowLargeImage, { src: membershipCode.barCode })}
                  />
                </View>
                <View className='item mem-code-info' onClick={this.startGettingBarCode}>
                  <Image className='mem-code-info__image' src={getStaticImgUrl.mine.rectanglePath_png} />
                  <View className='mem-code-info__view'>支付码每30秒自动更新，请在店内出示使用</View>
                </View>
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default MemberCode;
