import React, { Component } from 'react';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template';
import IntegralList from '@/wxat-common/components/tabbarLink/integral-list/index';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';

class IntegralMall extends Component {
  state = {}; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    this.getTemplateStyle();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
  }

  render() {
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-mpi-IntegralMall'
        className='wk-mpi-IntegralMall'
      >
        <IntegralList id='rooms-list' />
      </View>
    );
  }
}

export default IntegralMall;
