import { View } from '@tarojs/components';
import { FC } from '@tarojs/taro';

import './index.scss'

const ServiceAgreement: FC = () => {
  return (
    <View className='service-agreement-common'>
      <View className="boxService">
        <View className="boxServiceTitle">携旅VIP会员服务协议</View>
        <View className="boxServiceTime">本版本更新时间：2020年9月1日</View>
        <View>欢迎您使用携旅会员服务！</View>
        <View className="boxServiceTop"
        >携旅会员服务由广州携旅信息科技有限公司（以下亦称“携旅”）向您提供，注册地址为：广州高新技术产业开发区科珠路203号303。
        </View
        >
        <View className="boxServiceTop boxServiceBlod"
        >《携旅会员服务协议》（以下亦称“本协议）由您（以下亦称“会员”或“VIP会员”）和携旅进行缔结，对双方具有同等法律效力。携旅建议您仔细阅读本协议的全部内容，尤其是以加粗形式展示的，与您的权益（可能）存在重大关系的条款（包括相关免除携旅责任的条款、限制您权利的条款、约定的争议解决方式及司法管辖条款等），请您留意重点阅读。若您认为前述加粗条款可能会导致您的部分或全部权利或利益受损，请您务必再次仔细阅读，在确保您已经理解、接受了前述加粗条款的前提下，继续使用携旅会员服务。
        </View
        >
        <View className="boxServiceTop boxServiceBlod"
        >如果您不同意本协议的任一或全部条款内容，请不要以确认形式（包括但不限于支付行为/接受赠与、或完成了成为会员的全部程序而在此过程中未向携旅提出关于本协议的任何异议、或使用VIP会员服务）进行下一步操作或使用携旅会员服务。当您以确认形式进行下一步操作或使用携旅会员服务时，即表示您与携旅已达成协议关系，您自愿接受本协议并遵守本协议项下的全部约定。
        </View
        >
        <View className="boxServiceTop boxServiceBlod"
        >携旅有权变更本协议内容，一旦本协议内容发生变更的，携旅将在相应页面、站内信或以其他合理方式进行通知，请您仔细阅读。如果您不同意变更的内容的，您可以选择停止使用VIP会员服务。如您继续使用VIP会员服务的，则视为您已经同意变更的全部内容。更新后的协议自发布之日起生效。
        </View
        >
        <View className="boxServiceTop"
        >若您是未成年人，请您在监护人监护、指导下阅读并决定是否同意本协议。
        </View
        >
        <View className="boxServiceTop">一、服务说明</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View>
            <View className="boxServiceBlod">【携旅会员】
            </View
            >
            是指完成了成为会员的所有程序，且在遵守本协议的前提下，在会员时长有效期内享受携旅提供的VIP会员服务的自然人。
            <View
            >携旅会员目前分为携旅VIP会员及携旅会员2种类型。具体规则以携旅在相关页面的说明为准。</View
            >
            <View>
              <View className="boxServiceBlod"
              >携旅会员与携旅VIP会员等是不同的会员类型，
              </View
              >
              请您根据您的需求，开通您所需的会员，进而享受相应的服务。
            </View>
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>2.</View>
          <View>
            <View className="boxServiceBlod">【携旅会员服务】
            </View
            >
            是由携旅向您提供的专享的会员权益，具体权益内容说明请您查看本协议第四节。同时，携旅在此善意提醒您，本协议中还有需要进行额外付费的服务，包括但不限于本协议第四节第5条。
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>3.</View>
          <View>
            <View className="boxServiceBlod">【携旅会员服务协议】
            </View
            >
            是《携旅服务协议》、《携旅隐私政策》的补充协议，是其不可分割的一部分，与其具有同等法律效力；本协议如与前述协议发生冲突的，以本协议为准。
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>4.</View>
          <View>
            <View className="boxServiceBlod">【携旅会员服务规则】
            </View
            >
            是携旅在携旅会员中心已经或未来不时发布的与VIP会员服务相关的协议、公告、页面说明、通知、FAQ等内容。前述内容一经发布即生效，均构成本协议不可分割的一部分。
          </View>
        </View>
        <View className="boxServiceTop">二、使用服务</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View>服务获取</View>
        </View>
        <View className="leftBoxServiceFlex">
          <View className="boxServiceFlex">
            <View>1.1</View>
            <View
            >携旅会员服务为收费服务，您可以通过支付相应的服务费用购买；同时，您亦可以通过营销活动、接受礼品卡、接受好友代开会员服务等携旅认可的途径获取服务。
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>1.2</View>
            <View>
              <View
              >您在获取携旅会员服务时，应当遵守法律法规、本协议约定，不侵犯第三方或携旅的合法权益。您不得自行（或协助他人）通过以下方式获取VIP会员服务：
              </View
              >
              <View>（1） 以商业性或其他非个人使用等目的；</View>
              <View
              >（2） 通过机器人软件、蜘蛛软件、爬虫软件等任何自动程序、脚本、软件等方式；
              </View
              >
              <View
              >（3） 未经携旅允许通过转移、赠与、借用、租用、销售、转让VIP会员服务的方式；
              </View
              >
              <View
              >（4） 通过不正当手段或以违反诚实信用原则的方式（如利用规则漏洞、利用系统漏洞、滥用会员身份、黑色产业、投机等）；
              </View
              >
              <View>（5） 通过利用或破坏携旅会员活动规则的方式。</View>
              <View className="boxServiceBlod"
              >携旅在此声明：任何未经携旅明示授权而销售、转让携旅会员资格的行为属于非法销售、非法转让，携旅有权追究其法律责任。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>2.</View>
          <View>服务使用的基本原则</View>
        </View>
        <View className="leftBoxServiceFlex">
          <View className="boxServiceFlex">
            <View>2.1</View>
            <View
            >您在使用携旅会员服务的过程中，应遵守法律法规及其他规范性文件，包括但不限于《中华人民共和国保守国家秘密法》、《中华人民共和国著作权法》、《中华人民共和国计算机信息系统安全保护条例》、《计算机软件保护条例》、《互联网电子公告服务管理规定》、《中华人民共和国网络安全法》、《信息网络传播权保护条例》等；应遵守公共秩序，尊重社会公德，不得危害网络安全，不得利用网络从事危害国家安全、荣誉和利益，煽动颠覆国家政权、推翻社会主义制度，煽动分裂国家、破坏国家统一，宣扬恐怖主义、极端主义，宣扬民族仇恨、民族歧视，传播暴力、淫秽色情信息，编造、传播虚假信息扰乱经济秩序和社会秩序，以及侵害他人名誉、隐私、知识产权和其他合法权益等活动。在任何情况下，携旅一旦合理地认为您存在上述行为的，可以在任何时候，不经事先通知终止向您提供携旅会员服务。
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>2.2</View>
            <View>
              <View
              >携旅授予您对携旅会员服务一项个人的、非独家的、不可转让的、非商业用途的、可撤销的、有期限的使用许可。
              </View
              >
              <View className="boxServiceBlod"
              >即：您仅可出于个人、非商业的目的使用VIP会员服务。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>3.</View>
          <View
          > 服务内容的变更
            <View
            >携旅有权根据法律法规及政策变更、版权方要求、自身运营策略变更对VIP会员服务内容（包括但不限于VIP会员权益细则、收费标准、收费方式）进行部分或全部变更。就前述变更，携旅将通过相应服务页面、站内信通知或以其他合理方式进行发布，并于发布之日起生效。发布后，您继续操作的行为（包括但不限于点击同意、或继续购买、或完成支付行为、或使用VIP会员服务等），均视为您已经接受前述变更。</View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>4.</View>
          <View>第三方服务内容</View>
        </View>
        <View className="leftBoxServiceFlex">
          <View className="boxServiceFlex boxServiceBlod">
            <View>4.1</View>
            <View
            >若您在携旅平台的视频搜索结果中展示的视频播放源为非携旅的，在您点击播放后，将跳转至第三方视频页面为您提供服务。该类视频的服务由第三方提供与负责，而非携旅提供的VIP会员服务。携旅在此善意提醒您，第三方会依据其平台规则（包括会员规则，如涉及）向您提供服务并收取服务费（如有）。
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>4.2</View>
            <View>
              若您购买联合会员服务（如：同时购买携旅VIP会员服务+龙腾会员服务）或因购买携旅会员服务而获赠第三方会员服务的，携旅仅提供携旅会员服务，不对第三方的会员服务负责。第三方会员服务的权益、使用、收费规则等，将由第三方执行和向您解释，您亦可以前往第三方平台查询相关规则。
            </View>
          </View>
        </View>
        <View className="boxServiceTop">三、您的账号</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View>
            <View>账号获得</View>
            <View
            >在您的VIP会员服务的有效期限内，您享有VIP会员权益的携旅账号即为您的携旅会员账号（即：与您的VIP会员服务绑定的携旅账号，以下亦称“VIP账号”或“会员账号”）。
            </View
            >
            <View
            >同时，携旅在此善意提醒您，您应当在遵守携旅账号使用规则的前提下，使用您的会员账号。携旅建议您查阅《携旅服务协议》及相关子协议、各类公告、页面说明和规范流程、FAQ等以获得更多关于账号使用规则的内容。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>2.</View>
          <View>
            <View>登录</View>
            <View
            >除您以游客模式（具体说明见第三节第8条）购买携旅会员服务的，携旅会员服务需要您登录您的携旅会员账号方可使用。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>3.</View>
          <View>账号管理及安全</View>
        </View>
        <View className="leftBoxServiceFlex">
          <View className="boxServiceFlex boxServiceBlod">
            <View>3.1</View>
            <View
            >您应自行负责并妥善、正确地保管、使用、维护您的VIP会员账号和密码，并对您的账号和密码采取必要和有效的保密措施。
            </View
            >
            <View className="boxServiceBlod"
            >非携旅法定过错导致的任何遗失、泄露、被篡改、被盗以及其他因保管、使用、维护不当而造成的损失，您应自行承担。
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>3.2</View>
            <View>
              <View
              >如果您发现有人未经授权使用了您的账号或您的账号存在其他异常情况导致无法正常登录使用的，则您需要按照携旅官方公布的账号找回流程进行账号找回。在找回过程中，携旅可能会要求您提供相应信息及/或证明资料，
              </View
              >
              <View className="boxServiceBlod"
              >请确保您所提供的内容真实有效，否则将可能无法通过携旅的验证而导致找回失败。
              </View
              >
            </View>
          </View>
          <View className="boxServiceFlex">
            <View>3.3</View>
            <View>
              <View
              >为保护账号安全，防止账号被盗等情况发生，携旅可能会不时或定期采用一种或多种方式对账号使用者进行用户身份验证（如短信验证、邮件认证等），
              </View
              >
              <View className="boxServiceBlod"
              >如未成功通过验证的，携旅有合理理由怀疑该账号出现被盗等不安全情况，并视情节严重情况而决定是否中止向该账号继续提供会员服务及/或采取进一步措施。
              </View
              >
            </View>
          </View>
          <View className="boxServiceFlex">
            <View>3.4</View>
            <View>
              <View>请您特别注意，</View>
              <View className="boxServiceBlod"
              >您的VIP会员账号下的行为视为您本人的行为，您应对您的VIP账号下发生的活动或通过该账号进行的活动负责。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>4.</View>
          <View>
            <View> 信息查询</View>
            <View
            >您可以通过登录携旅会员中心免费查询您的账号信息详情，包括已开通的VIP会员服务内容、服务期限、消费记录等。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>5.</View>
          <View> 登录限制</View>
        </View>
        <View className="leftBoxServiceFlex">
          <View className="boxServiceFlex boxServiceBlod">
            <View>5.1</View>
            <View>
              <View
              >携旅会员账号可在已装有携旅的电视系统以及特别说明的终端使用，具体适用的设备终端以携旅实际提供为准。
              </View
              >
              <View>原则上：</View>
              <View>（1） 同一VIP账号最多可在一个设备终端登录；</View>
              <View>（2） 同一VIP账号一天最多可在三个设备终端上使用；</View>
              <View>（3） 同一VIP账号同一时间最多可在一个设备终端上使用。</View>
              <View
              >如您超出上述使用限制而造成您的损失，将由您自行承担，且携旅有权视您的超出使用情况对您作出限制登录、限制使用、中断或终止服务等处理措施。
              </View
              >
            </View>
          </View>
          <View className="boxServiceFlex">
            <View>5.2</View>
            <View
            >为了您顺利使用携旅会员服务，携旅在此建议您，若您超过上述限制，应当立刻采取（1）关闭其他设备终端的视频播放；（2）及时修改账号的密码并重新进行登录。您可以在产品中查阅、管理您的VIP账号的设备终端登录情况。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>6.</View>
          <View>
            <View>账号独立</View>
            <View className="boxServiceBlod"
            >您的VIP账号仅限您本人出于非商业目的进行使用。会员权益不可在不同账号之间进行转移、迁徙、转让、赠与、售卖、租借、分享，即使前述不同账号由同一人拥有使用权。
            </View
            >
            <View
            >携旅在此善意提醒您，您在进行会员购买/参与活动时请注意区分，以免造成不必要的损失。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>7.</View>
          <View>
            <View>权利归属</View>
            <View
            >携旅会员账号、VIP会员账号下的VIP会员服务、虚拟产品（商品）及其涉及的产品及/或服务的所有权及相关知识产权归携旅所有或经过授权使用，您仅拥有前述产品及/或服务的有限使用权。但您经合法渠道取得的实体产品所有权、账号下归属于您的个人财产、携旅另有说明的其他权益除外。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>8.</View>
          <View>
            <View>游客模式</View>
          </View>
        </View>
        <View className="leftBoxServiceFlex">
          <View className="boxServiceFlex boxServiceBlod">
            <View>8.1</View>
            <View
            >因苹果应用商店渠道允许非注册用户通过游客方式购买VIP会员服务，会员服务将会绑定您购买会员时所用的设备，VIP会员服务也将仅可在绑定设备上使用，且部分会员权益可能会因为您使用游客模式而无法享受（如活动参与）。若您设备丢失、损坏、进行系统升级、还原、清空或其他类似行为的，将会导致您无法继续使用绑定于此设备的VIP会员权益，由此造成的您的损失，您应当自行承担。
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>8.2</View>
            <View
            >携旅在此建议您按照页面操作指引绑定您的携旅注册账号，以便可在您所拥有的其他设备上使用会员权益及更好的享受携旅所提供给您的会员权益。
            </View
            >
          </View>
        </View>

        <View className="boxServiceTop">四、会员权益及额外付费相关</View>
        <View className="boxServiceFlex">
          <View className="boxServiceBlod">1.</View>
          <View>
            <View className="boxServiceBlod">VIP会员权益</View>
            <View
            >携旅会员权益具体以携旅平台上VIP特权页面的说明或携旅实际提供为准，本协议项下的会员权益名称与页面的说明不一致的，不影响您所实际享受的会员权益的内容。
            </View
            >
            <View
            >请您理解，VIP会员类型多样，不同类型的VIP会员的会员权益存在差异；同一类型的VIP会员分为不同等级，不同等级之间的会员权益亦可能存在差异。您在VIP会员服务项下可享受的具体会员权益以携旅的页面说明或携旅实际提供为准。
            </View
            >
            <View className="boxServiceBlod"
            >携旅可能会根据法律法规及政策变更、版权状态变化、自身运营策略对前述会员权益作出部分修改，包括但不限于调整视频播出进度、停更部分视频内容或下线部分视频内容；若涉及内容下线，即使您已经将该等内容下载或缓存至本地，您仍可能无法进行观看。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>2.</View>
          <View>
            <View>设备及系统差异</View>
            <View
            >您成为VIP会员后，可能会因您使用的软件版本、设备、操作系统等不同以及其他第三方原因导致实际可使用的具体权益或服务内容有差别，由此可能给您带来的不便，您表示理解且不予追究或豁免携旅的相关责任。携旅建议您可以通过升级应用程序或操作系统版本、更换使用设备等方式来尝试解决，或者直接通过本协议提供的方式与携旅联系进行解决
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>3.</View>
          <View>
            <View>广告特权</View>
          </View>
        </View>
        <View className="leftBoxServiceFlex boxServiceBlod">
          <View className="boxServiceFlex">
            <View>3.1</View>
            <View
            >您在使用VIP会员服务的过程中，仍将（可能）接触到以各种方式投放的商业性广告或其他类型的商业信息，包括但不限于贴片广告、开机广告、创意中插广告、跑马灯广告、片尾广告、植入广告、弹窗广告、暂停广告、原创大头贴、口播标板、前情提要广告、创口贴等；
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>3.2</View>
            <View
            >携旅提供的“广告特权”仅指可自动为您减免部分视频播放前的贴片广告的时长。出于版权方或其他特殊要求，部分视频的片头仍会有其他形式的广告呈现，您在此表示理解，且同意上述呈现不视为携旅侵权或违约。携旅承诺，会尽量降低对您的视频观看体验的影响；
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>3.3</View>
            <View
            >关于会员定向推荐内容：主要是由携旅向VIP会员用户推荐影视内容、会员福利等，目的是让会员用户了解携旅更多的精彩内容及更好的享有会员权益及服务，且推荐内容可手动关闭；
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>3.4</View>
            <View
            >您知悉并同意，携旅可以通过电子邮件、站内信、手机短信、网站公告或其他方式向您发送促销信息或其他相关商业信息。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>4.</View>
          <View>
            <View>电影点播券（以下亦称“点播券”）</View>
            <View
            >部分影片可以使用电影点播券进行观看，在您的VIP会员服务的有效期间内，您获得携旅赠送的一定数量的电影点播券，具体赠送和使用规则（包括但不限于领取方式、有效期等）以携旅的相关页面说明或携旅实际提供为准。同时，请您理解，携旅可能会根据版权方要求、自身运营策略变更等原因而调整前述点播券具体使用规则，且部分影片仍需额外付费，具体说明见第四节第5条。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>5.</View>
          <View>
            <View> 额外付费</View>
          </View>
        </View>
        <View className="leftBoxServiceFlex boxServiceBlod">
          <View className="boxServiceFlex">
            <View>5.1</View>
            <View>
              <View
              >在您的携旅会员有效期内，您可以观看到携旅平台VIP专享的视频内容，但部分视频需要您额外付费后方可观看，目前主要有如下几种：
              </View
              >
              <View className="boxServiceFlex">
                <View>（1）</View>
                <View
                >付费影片：该类影片在支付点播费用后您便可直接观看，但请注意此类影片不可使用电影点播券。若您是携旅VIP会员，就付费影片中的部分影片，携旅将提供您免费观看的特权，但携旅有权根据版权方的要求对上述影片恢复收费。
                </View
                >
                <View
                >同时，携旅在此善意提醒您，携旅有权根据版权方要求及/或携旅实际运营需要，调整向VIP会员提供的可免费观看的影片的内容、数量等。
                </View
                >
              </View>
              <View className="boxServiceFlex">
                <View>（2）</View>
                <View
                >用券可观看的影片（且您没有点播券或选择不使用点播券的）：若您在观看该类影片时无点播券或您选择不使用点播券，则您需要额外付费进行观看。
                </View
                >
              </View>
              <View className="boxServiceFlex">
                <View>（3）</View>
                <View>
                  <View>超前点播：</View>
                  <View
                  >“超前点播”是携旅为满足广大用户的对部分电视剧、综艺视频内容超前观看需求，就携旅平台上部分定期更新的视频内容，在保证普通用户和VIP会员所享受的原本视频内容更新节奏不变的前提下，向VIP会员提供的剧集的超前点播的服务模式。
                  </View
                  >
                  <View
                  >通过“超前点播”服务，您可选择通过额外付费，提前观看前述视频内容的更多剧集，具体的点播规则以携旅平台实际说明或提供为准。
                  </View
                  >
                  <View
                  >同时，携旅向您承诺，该“超前点播”的服务模式，不影响您享受既有的VIP会员权益。
                  </View
                  >
                </View>
              </View>
              <View className="boxServiceFlex boxServiceNormal">
                <View>（4）</View>
                <View>其他需要您额外付费后方可享受的内容，如知识课程等。</View>
              </View>
            </View>
          </View>
          <View className="boxServiceFlex">
            <View>5.2</View>
            <View>
              <View
              >前述需要额外付费的服务内容，携旅会以显著标注向您作出提示。请您仔细阅读相关提示，并自主决定是否进行购买。
              </View
              >
              <View className="boxServiceBlod"
              >携旅在此善意提醒您，服务内容在您付费成功后有一定的使用有效期（具体以购买页面说明为准），请您记得及时使用，在有效期满后，无论您是否使用完毕所购的服务内容，携旅不因此向您退还您已支付的费用且不予进行任何形式的补偿/赔偿。同时，您理解并同意，就前述额外付费的部分或全部视频内容，携旅有权根据自身运营政策之需要，在一段时间后，免费提供给用户观看。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>6.</View>
          <View>
            <View> 虚拟产品</View>
            <View>
              您在使用VIP会员服务过程中可能获得由我们提供的各种积分、卡券、优惠券、代金券、福利券、成长值等虚拟产品。前述虚拟产品仅能用于携旅官方渠道公布的指定用途，不能退货、兑换现金或转让、买卖、置换、抵押等，在使用后亦不会恢复。
            </View
            >
            <View>
              同时，请您理解，携旅有权根据实际运营策略变更，变更其使用规则（包括使用有效期）。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>7.</View>
          <View>
            <View> 活动参与</View>
            <View>
              在VIP会员服务期限内，您有权选择参加由携旅组织的VIP活动并享有由携旅提供的各项优惠。具体活动及优惠的内容及规则以携旅实际提供为准。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>8.</View>
          <View>
            <View> 优惠政策</View>
            <View>
              携旅可能会根据实际运营情况对不同阶段已经开通且持续有效的VIP会员给予延期、权益升级等方面的优惠，具体优惠政策以携旅在相关服务页面通知的内容为准。若您是符合前述条件的VIP会员，则您有权对该等优惠进行享受。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>9.</View>
          <View>
            <View> 活动规范</View>
            <View>
              您在获取、使用携旅会员服务(包括参与活动)时，应遵循携旅官方渠道公布的流程、平台规范等：
            </View
            >
            <View className="boxServiceFlex">
              <View>9.1</View>
              <View
              >不得通过未经携旅授权或公布的途径、方式、渠道非法获取携旅会员服务（包括但不限于部分或全部会员权益、VIP会员账号）及/或参与活动获取福利（包括但不限于会员权益、虚拟产品、实体商品、会员服务兑换码，本条下同）；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>9.2</View>
              <View
              >不得通过不正当手段或以违反诚实信用原则的方式（如利用规则漏洞、利用系统漏洞、滥用会员身份、黑色产业、投机等违背携旅提供服务/举办活动的初衷的方式）参与活动或者获取福利。
              </View
              >
            </View>
            <View className="boxServiceBlod">
              若您违反上述约定，则携旅有权作出删除、取消、清零等处理，且有权终止向您提供服务，由此造成的全部损失由您自行承担。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>10.</View>
          <View>
            <View> 服务期限</View>
            <View className="boxServiceFlex">
              <View>10.1</View>
              <View
              > 您的VIP会员服务的服务期限以您自行选择并支付相应会员费用对应的服务期限为准，自您成为VIP会员之时起算，您可以通过登录携旅会员中心免费查询。该期限不因您未使用或对视频的暂停、倒回等情况而延长。当会员服务期限到期后，携旅将停止继续向您提供VIP会员服务；但如您开通自动续费服务且您在服务期限到期前续费成功的，会员服务期限将在原服务期限的基础上顺延。
              </View
              >
            </View>
            <View className="boxServiceFlex boxServiceBlod">
              <View>10.2</View>
              <View
              >请您理解，VIP会员服务期限中包含携旅解决故障、服务器维修、调整、升级等或因第三方侵权处理所需用的合理时间，对上述情况所需用的时间或造成的任何损失（如有），携旅不予任何形式的赔偿/补偿，但携旅会尽可能将影响降至最低。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceTop">五、收费及退订</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View>
            <View> 收费方式</View>
            <View>
              携旅会员服务为收费服务，您可通过携旅实际接受的付费方式完成VIP会员费用的支付，如银行卡支付、第三方支付等。请您注意，若您使用您的苹果账户或与您的会员账号绑定的通信账户进行支付，此付费方式为代收费运营商托收的付款方式，您通过此种付费方式付费可能存在一定的商业风险（如不法分子利用您账户或银行卡等有价卡等进行违法活动），该等风险可能会给您造成相应的经济损失，您应自行承担全部损失。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>2.</View>
          <View>
            <View> 费用退还</View>
            <View>
              VIP会员服务系网络商品和虚拟商品，采用先收费后服务的方式，会员费用是您所购买的会员服务所对应的网络商品价格，而非预付款或者存款、定金、储蓄卡等性质，
              <View className="boxServiceBlod"
              >VIP会员服务一经开通后不可转让或退款（如因VIP会员服务存在重大瑕疵导致您完全无法使用等携旅违约情形、本协议另有约定、法律法规要求必须退款的或经携旅判断后认为可以退款等除外）。</View
              >
            </View>
            <View
            >携旅在此特别提醒您，您在购买VIP会员服务（包括自动续费服务）之前应仔细核对账号信息、购买的服务内容、价格、服务期限等信息。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>3.</View>
          <View>
            <View> 收费标准、方式的变更</View>
            <View>
              VIP会员服务（包括自动续费）、“付费影片”、“超前点播”及其他付费点播等服务的收费方式、收费标准由携旅根据公司的运营成本、运营策略等综合考虑后独立决定（调整包括但不限于促销、涨价等），并在相关的产品服务宣传及支付页面向您展示；若您在购买和续费时，相关收费方式发生变化的，以携旅实际接受的收费方式为准；相关服务的价格发生了调整的，应以携旅平台上公示的现时有效的价格为准（但携旅与您另有约定的情形除外）。您同意您继续操作的行为（包括但不限于点击同意、或继续购买、或完成支付行为、或使用VIP会员服务、或进行额外付费等），即视为您知悉并同意变更后的收费方式、收费标准。
            </View
            >
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>4.</View>
          <View>
            <View> 自动续费</View>
            <View>
              自动续费服务是指在会员已开通VIP会员服务的前提下，出于会员对于自动续费的需求，避免会员因疏忽或其他原因导致未能及时续费而推出的服务。如会员选择开通自动续费服务的，即会员授权携旅可在会员服务期限即将过期时或会员服务的订阅周期即将到期时，委托支付渠道从与您的自有充值账户、绑定的第三方支付账户、银行卡、通信账户等（以下统称“账户”）余额中代扣下一个计费周期的费用。该服务实现的前提是您已开通自动续费及绑定相关账户，且可成功从其上述账户中足额扣款。
            </View
            >
            <View
            >4.1 计费周期：如月度、季度、年度等（具体以携旅提供的为准），会员可自行选择。
            </View
            >
            <View className="boxServiceFlex">
              <View>4.2</View>
              <View>
                <View>自动扣款规则：</View>
                <View className="boxServiceFlex">
                  <View>（1）</View>
                  <View
                  >会员选择开通自动续费的，则视为同意授权携旅可在会员服务期限即将过期时，依据支付渠道的扣款规则发出扣款指令，并同意支付渠道可以根据携旅发出的扣款指令，在不验证会员账户密码、支付密码、短信校验码等信息的情况下从账户中扣划下一个计费周期的费用。
                  </View
                  >
                </View>
                <View className="boxServiceFlex">
                  <View>（2）</View>
                  <View
                  >除非会员主动明确地取消了自动续费，否则，会员对携旅的自动扣款委托长期有效、不受次数限制。
                  </View
                  >
                </View>
                <View className="boxServiceFlex boxServiceBlod">
                  <View>（3）</View>
                  <View
                  >由于自动续费服务需要您主动操作取消，如果您未主动明确地取消自动续费，则将视为您同意携旅可依据支付渠道的扣款规则在会员服务期限到期后的一定期限内进行不时的扣款尝试（即使您账户内金额不足）。
                  </View
                  >
                </View>
                <View className="boxServiceFlex boxServiceBlod">
                  <View>（4）</View>
                  <View>
                    <View className="boxServiceNormal">一旦扣款成功，携旅将为您开通本次计费周期对应的VIP会员服务；
                    </View
                    >
                    携旅在此特别提醒您，您所支付的本次计费周期对应的VIP会员服务费用的金额以您在开通自动续费服务时，与携旅约定的自动续费价格为准：但在平台开展活动或其他携旅与您另有约定的情形下，前述金额以携旅与您的相关约定为准。同时，在自动续费服务取消后，再次开通自动续费服务的，您所支付的本次计费周期对应的VIP会员服务费用的金额以再次开通时，与携旅约定的自动续费价格为准。
                  </View>
                </View>
              </View>
            </View>
            <View className="boxServiceFlex">
              <View>4.3</View>
              <View>
                <View>取消方式：</View>
                <View>请联系携旅客服，400 0077988</View>
              </View>
            </View>
          </View>
        </View>
        <View className="boxServiceTop">六、您的行为规范和违约处理</View>
        <View className="boxServiceFlex">
          <View className="boxServiceBlod">1.</View>
          <View>
            <View className="boxServiceBlod">
              在您使用携旅会员服务的过程中，不得存在以下行为：
            </View
            >
            <View className="boxServiceFlex">
              <View>1.1</View>
              <View
              >未经携旅明确授权，通过技术手段对服务内容、服务期限、消费金额、交易状态等信息进行修改；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.2</View>
              <View
              >将VIP会员服务通过非携旅明确授权的方式（包括但不限于转移、赠与、借用、租用、销售、转让）提供他人使用；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.3</View>
              <View
              >未经携旅明确授权，将VIP会员服务提供的视频的全部或部分进行复制、下载、上传、修改、编目排序、翻译、发行、开发、转让、销售、展示、传播、合成、嵌套、链接、创作衍生作品、进行商业开发或推广等；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.4</View>
              <View
              >对携旅用于保护VIP会员服务的任何安全措施技术进行破解、更改、反操作、篡改或其他破坏；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.5</View>
              <View
              >未经携旅事先书面同意，删除VIP会员服务内容上的任何所有权或知识产权声明或标签；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.6</View>
              <View
              > 未经携旅明确授权，采用收费或免费的方式，在任何公开场合全部或部分展示VIP会员服务内容（但如您的上述行为不构成侵权的除外）；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.7</View>
              <View>
                其他未经携旅明示授权许可或违反本协议、法律法规或监管政策、侵犯第三方或携旅合法权益的行为。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>2.</View>
          <View>
            <View>
              您知悉并同意，如您存在或携旅经独立判断后认为您存在任何违反国家法律法规或监管政策、违反本协议或有损携旅或/及其关联公司的声誉、利益的行为的，携旅有权独立决定采取以下一项或多项处理措施：
            </View
            >
            <View className="boxServiceFlex">
              <View>2.1</View>
              <View
              >如本协议对此行为有单独条款约定处理方式的，按照该条款处理；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.2</View>
              <View
              >无需通知您而采取一种或多种措施制止您的行为及行为产生的后果，如删除/屏蔽相关链接或内容、限制/取消您的账号/账户使用权限等；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.3</View>
              <View
              >无需通知您而中断或终止部分或全部VIP会员服务，且您已交纳的VIP会员服务费用将不予退还且不获得任何形式的补偿/赔偿；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.4</View>
              <View
              >如您的行为使携旅或/及其关联公司遭受任何损失的，您应当承担全部损失赔偿责任并在携旅要求的时限内完成费用支付。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceTop">七、服务的中断和终止</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View>
            <View> 本服务的中断或终止包括如下情况：</View>
            <View className="boxServiceFlex">
              <View>1.1</View>
              <View> 您主动提出要求的；</View>
            </View>
            <View className="boxServiceFlex">
              <View>1.2</View>
              <View
              >您存在或携旅经独立判断后认为您存在任何违反国家法律法规或监管政策、违反本协议或有损携旅或/及其关联公司的声誉、利益的行为的；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.3</View>
              <View>携旅根据法律法规、监管政策的规定或有权机关的要求；</View>
            </View>
            <View className="boxServiceFlex">
              <View>1.4</View>
              <View>携旅为维护账号与系统安全等紧急情况之需要；</View>
            </View>
            <View className="boxServiceFlex">
              <View>1.5</View>
              <View
              >不可抗力（鉴于互联网之特殊性质，不可抗力亦包括黑客攻击、电信部门技术调整导致之重大影响、因政府管制而造成之暂时关闭、病毒侵袭等影响互联网正常运行之情形）；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>1.6</View>
              <View>其他携旅无法抗拒的情况。</View>
            </View>
          </View>
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>2.</View>
          <View>
            <View> 当发生前述终止的情况时，您与携旅均认可如下处理方式：</View>
            <View className="boxServiceFlex">
              <View>2.1</View>
              <View>已经产生但未使用的VIP会员权益自动清除且不折现；</View>
            </View>
            <View className="boxServiceFlex">
              <View>2.2</View>
              <View
              >如您在携旅平台内有正在进行中的交易，携旅届时将视情况进行合理处理；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.3</View>
              <View
              >除法律法规另有规定或携旅另有说明外，携旅已收取的会员费用不予退还；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.4</View>
              <View
              >如因您违反本协议导致终止的，携旅有权视情况要求您承担相应的违约责任；
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.5</View>
              <View
              > 除法律法规另有规定或携旅另有说明外，携旅无需向您和第三人承担任何责任。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceTop">八、您的隐私保护</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View
          >携旅深知个人信息对您的重要性，因此携旅非常重视保护您的隐私和个人信息，亦将您的个人信息以高度审慎的义务对待和处理。在您使用携旅会员服务的过程中，携旅将采用相关技术措施及其他安全措施来保护您的个人信息。
          </View
          >
        </View>
        <View className="boxServiceFlex">
          <View>2.</View>
          <View>
            <View>未成年人保护</View>
            <View>携旅非常注重未成年人的保护。</View>
            <View className="boxServiceFlex">
              <View>2.1</View>
              <View
              >若您为未成年人，应在监护人监护、指导下阅读本协议，并且使用VIP会员服务已经得到监护人的同意。
              </View
              >
            </View>
            <View className="boxServiceFlex boxServiceBlod">
              <View>2.2</View>
              <View
              >监护人应指导子女上网应该注意的安全问题，防患于未然。若监护人同意未成年人使用VIP会员服务，必须以监护人名义申请消费，并对未成年人使用VIP会员服务进行正确引导、监督。未成年人使用VIP会员服务，以及行使和履行本协议项下的权利和义务即视为已获得了监护人的认可。
              </View
              >
            </View>
            <View className="boxServiceFlex">
              <View>2.3</View>
              <View
              >携旅提醒未成年人在使用会员服务时，要善于网上学习，认清网络世界与现实世界的区别，避免沉迷于网络，影响日常的学习生活。
              </View
              >
            </View>
          </View>
        </View>
        <View className="boxServiceFlex">
          <View>3.</View>
          <View
          >更多关于个人信息处理和保护规则、用户对个人信息的控制权等内容，请您至携旅平台上查阅《携旅隐私政策》的全文。
          </View
          >
        </View>
        <View className="boxServiceTop">九、通知</View>
        <View className="leftBoxServiceFlex">
          为便于您获知与本协议和VIP会员服务相关的信息，您同意携旅有权通过网页公示、页面提示、弹窗、消息通知、公众号通知、站内信、您预留的联系方式（如手机短信、电子邮件等）等一种或多种方式进行通知，该通知自携旅发送之时视为已成功送达您。如多种通知方式并存的，则送达时间以上述方式中最早发送之时为准。
          此类通知的内容或将对您产生重大有利或不利影响，请您务必确保联系方式为有效并请及时关注相应通知。
        </View>
        <View className="boxServiceTop">十、联系携旅</View>
        <View className="leftBoxServiceFlex">
          <View
          >如您对本协议或使用VIP会员服务的过程中有任何问题（包括问题咨询、投诉等），携旅专门为您提供了多种反馈通道，希望为您提供满意的解决方案：
          </View
          >
          <View className="boxServiceFlex">
            <View>
              <View className="tBoxServiceFlexDoT"></View>
            </View>
            <View
            > 在线客服/其他在线意见反馈通道：您可与携旅平台上产品功能页面的在线客服联系或者在线提交意见反馈；
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>
              <View className="tBoxServiceFlexDoT"></View>
            </View>
            <View>人工客服通道：您可以拨打携旅的任何一部客服电话与携旅联系；</View>
          </View>
          <View className="boxServiceFlex">
            <View>
              <View className="tBoxServiceFlexDoT"></View>
            </View>
            <View
            >专设的邮件通道：携旅专门设立了fan@htrip.tv邮箱，您可以通过该邮箱与携旅联系；
            </View
            >
          </View>
          <View className="boxServiceFlex">
            <View>
              <View className="tBoxServiceFlexDoT"></View>
            </View>
            <View>其他方式：携旅提供的其他反馈通道。</View>
          </View>
          <View>我们会在收到您的反馈后尽快向您答复。</View>
        </View>

        <View className="boxServiceTop">十一、 隐私政策</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View>为了向您提供更优质的服务，我们与包括友盟+在内第三方数据服务商合作预测您的特征偏好。为了保障您的数据安全，该种预测以指数分值的方式提供，数据分析在严格去标识化安全措施前提下进行。</View>
        </View>
        <View className="boxServiceTop">十二、 其他</View>
        <View className="boxServiceFlex">
          <View>1.</View>
          <View
          >本协议的生效、履行、解释及争议的解决均适用中华人民共和国法律。
          </View
          >
        </View>
        <View className="boxServiceFlex boxServiceBlod">
          <View>2.</View>
          <View
          > 如就本协议的签订、履行等发生任何争议的，双方应尽量友好协商解决；协商不成时，任何一方均可向被告住所地享有管辖权的人民法院提起诉讼。
          </View
          >
        </View>
        <View className="boxServiceFlex">
          <View>3.</View>
          <View
          >如本协议因与中华人民共和国现行法律相抵触而导致部分无效的，不影响协议的其他部分的效力。
          </View
          >
        </View>
        <View className="boxServiceFlex">
          <View>4.</View>
          <View
          >本协议的标题仅为方便及阅读而设，并不影响正文中任何条款的含义或解释。
          </View
          >
        </View>

        <View className="boxServiceTime">广州携旅信息科技有限公司</View>
      </View>
    </View>
  )
}

export default ServiceAgreement;
