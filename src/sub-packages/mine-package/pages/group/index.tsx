import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import login from '@/wxat-common/x-login/index.js';
import constants from '@/wxat-common/constants/index.js';
import template from '@/wxat-common/utils/template.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';

import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';

const loadMoreStatus = constants.order.loadMoreStatus;
const GROUP_STATUS = {
  '-1': '拼团中',
  '1': '拼团成功',
  '0': '拼团失败',
  '-2': '退款中',
  '-3': '退款完成',
};

type StateProps = {
  templ: any;
};

interface Group {
  props: StateProps;
}

const mapStateToProps = () => {
  return {
    templ: template.getTemplateStyle(),
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class Group extends React.Component {
  state = {
    activityList: null,
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    const { templ } = this.props;

    if (templ.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templ.titleColor, // 必写项
      });
    }
  }

  componentDidMount() {
    this.listActivityList();
  }

  // onPullDownRefresh = () => {
  //   this.setState({ pageNo: 1, hasMore: true }, () => {
  //     this.listActivityList(true);
  //   });
  // };

  onReachBottom = () => {
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatus.LOADING,
        },

        this.listActivityList
      );
    }
  };

  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatus.LOADING,
      },

      this.listActivityList
    );
  };

  listActivityList = async (isFromPullDown = false) => {
    this.setState({ error: false });
    await login.login();
    try {
      const res = await wxApi.request({
        url: api.group.queryOwn,
        loading: true,
        data: { pageNo: this.state.pageNo, pageSize: 10 },
      });

      let activityList = res.data || [];
      // 如果不是第一页, 请求回来的数据进行拼接展示到页面中
      if (this.isLoadMoreRequest()) {
        activityList = ((this.state.activityList as unknown) as Array<any>).concat(activityList);
      }

      // 上拉刷新请求的数据 length 达到最大数量, 禁止掉上拉对数据的请求
      // 同时展示加载到底部了
      if (activityList.length === res.totalCount) {
        this.state.hasMore = false;
        this.setState({ loadMoreStatus: loadMoreStatus.BASELINE });
      }
      activityList.forEach((activity) => {
        activity.statusName = GROUP_STATUS[activity.status + ''];
        // 如果拼团状态为“拼团中”，但是拼团活动的状态为“已结束”的话，活动状态描述应该也是“拼团失败”
        if (activity.status === -1 && activity.activityStatus === 0) {
          activity.statusName = GROUP_STATUS[0 + ''];
        }
      });
      this.setState({ activityList: [...activityList], pageNo: this.state.pageNo + 1 });
    } catch (err) {
      // 请求失败的情况下, 不是第一页, 展示加载失败状态
      if (this.isLoadMoreRequest()) {
        this.setState({ loadMoreStatus: loadMoreStatus.ERROR });
      } else {
        // 否则展示错误
        this.setState({ error: true });
      }
    } finally {
      // 如果当前状态是在下拉刷新, 就停止当前页面下拉刷新。
      if (isFromPullDown) {
        wxApi.stopPullDownRefresh();
      }
    }
  };

  onGoJoin = (ev: ITouchEvent) => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/group/join/index',
      data: { groupNo: ev.currentTarget.dataset.num },
    });
  };

  onGoOrder = (ev) => {
    const item = ev.currentTarget.dataset.item;
    let url = pageLinkEnum.orderPkg.orderDetail;
    wxApi.$navigateTo({
      url: url,
      data: { orderNo: item.orderNo },
    });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  render() {
    const { activityList, error, loadMoreStatus } = this.state as Record<string, any>;
    const { templ } = this.props;

    return (
      <View data-scoped='wk-mpg-Group' className='wk-mpg-Group serve-list'>
        {!!(!!activityList && activityList.length === 0 && !error) && (
          <Empty iconClass='empty-icon' message='暂无参与拼团'></Empty>
        )}

        {!!error && <Error></Error>}
        {!!(!!activityList && !!activityList.length && !error) && (
          <View className='group'>
            {activityList.map((item) => {
              return (
                <View key={item.groupNo} className='group-item'>
                  <View>
                    <Image src={item.thumbnail} className='goods-image'></Image>
                  </View>
                  <View className='group-detail'>
                    <View className='info-block level-1'>
                      <View className='group-name text-ellipsis'>{item.itemName}</View>
                      <View className='group-status'>{item.statusName}</View>
                    </View>
                    <View className='info-block level-2'>
                      {!!item.itemAttribute && <View>{item.itemAttribute}</View>}
                      <View>{'x' + item.itemCount}</View>
                      <View>{'合计：' + filters.moneyFilter(item.payFee, true)}</View>
                    </View>
                    <View className='info-block level-3'>
                      {item.status === 1 && (
                        <View
                          className='btn'
                          style={_safe_style_('background:' + templ.btnColor)}
                          onClick={_fixme_with_dataset_(this.onGoOrder, { item: item })}
                        >
                          订单详情
                        </View>
                      )}

                      <View
                        className='btn'
                        onClick={_fixme_with_dataset_(this.onGoJoin, { num: item.groupNo })}
                        style={_safe_style_('background:' + templ.btnColor)}
                      >
                        拼团详情
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        )}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default Group;
