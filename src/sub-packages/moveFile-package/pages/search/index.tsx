import { _fixme_with_dataset_ } from 'wk-taro-platform';
import { View, Input } from '@tarojs/components';
import React, { Component } from 'react';
import Taro from '@tarojs/taro';
// pages/search/index.js
import wxApi from '../../../../wxat-common/utils/wxApi';
import api from "../../../../wxat-common/api";
import report from "../../../../sdks/buried/report";
import template from '../../../../wxat-common/utils/template';
import reportConstants from '../../../../sdks/buried/report/report-constants';
import goodsTypeEnum from '../../../../wxat-common/constants/goodsTypeEnum';
import industryEnum from '../../../../wxat-common/constants/industryEnum';
import util from '../../../../wxat-common/utils/util';

import Error from '../../../../wxat-common/components/error/error';
import Empty from '../../../../wxat-common/components/empty/empty';
import HoverCart from "../../../../wxat-common/components/cart/hover-cart";
import ProductModule from "../../../../wxat-common/components/base/productModule";
import './index.scss';
const app = Taro.getApp();

class Search extends Component {

  /**
   * 页面的初始数据
   */
  state = {
    focus: true,
    dataSource: [],
    historyKeyWords: [],
    keyWords: '',
    industry: app.globalData.industry, // 当前行业类型
    reportSource: reportConstants.SOURCE_TYPE.search.key,
    __isPageShow: false
  }

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {
    this.setState({
      industry: app.globalData.industry,
    });

    this.getTemplateStyle();
    Taro.getStorage({
      key: 'historyKeyWords',
      success: (res) => {
        if (res.data && res.data.length) {
          this.setState({
            historyKeyWords: res.data,
          });
        }
      },
    });
  }

  enterFocus = () => {
    this.setState({
      focus: true,
    });
  }

  loseFocus = () => {
    // fixme 需要加延迟操作，否则无法执行历史记录的点击事件
    setTimeout(() => {
      this.setState({
        focus: false,
      });
    }, 300);
  }

  confrim = (options) => {
    this.setState({
      focus: false,
    });

    const keyWords = options.detail.value;
    this.getResultFromKeyWrods(keyWords);
  }

  clickHistoryItem = (options) => {
    const keyWords = options.currentTarget.dataset.item;
    this.getResultFromKeyWrods(keyWords);
    // 1、修改input value
    this.setState({
      keyWords: keyWords,
      focus: false,
    });
  }

  getResultFromKeyWrods = (keyWords) => {
    // 搜索埋点上报
    report.search(keyWords);

    const index = this.state.historyKeyWords.indexOf(keyWords);
    if (index === -1) {
      // 不存在
      // 对于全部空格的不错处理
      if (!keyWords.match(/^\s*$/)) {
        this.state.historyKeyWords.unshift(keyWords);
      }
    } else {
      // 修改历史记录排序
      const str = this.state.historyKeyWords.splice(index, 1);
      this.state.historyKeyWords.unshift(str[0]);
    }
    this.setState({
      historyKeyWords: this.state.historyKeyWords,
    });

    const params = {
      itemName: keyWords,
      status: 1,
      type: 1,
      typeList: [],
      pageNo: 1,
      pageSize: 10,

      // 判断是否为地产行业，是则设置type为楼盘类型，以及在typeList中增加楼盘商品类型为请求参数
    };
    if (this.state.industry === industryEnum.type.estate.value) {
      params.type = goodsTypeEnum.ESTATE.value;
      params.typeList.push(goodsTypeEnum.ESTATE.value);
    }

    // 判断请求的商品类型为产品时，则删除请求参数中的type，改用typeList包含产品类型及组合商品类型
    if (params.type === goodsTypeEnum.PRODUCT.value) {
      params.typeList.push(goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value);
    }

    wxApi
      .request({
        url: api.classify.itemSkuList,
        loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true && !!res.data) {
          let itemid = '';
          const len = res.data.length;
          if (len) {
            res.data.forEach((item, i) => {
              const wxItem = item.wxItem || item;
              itemid = `${itemid}${wxItem.itemNo}`;
              if (i < len - 1) {
                itemid = `${itemid}${wxItem.itemNo},`;
              }
            });
          }
          report.searchResult(keyWords, itemid, len);
        }
        this.setState({
          dataSource: res.data || [],
        });
      })
      .catch(() => {
        this.setState({
          dataSource: null,
        });

      });
  }

  onItemClick = (e) => {
    const goodsItem = e.detail;
    if (goodsItem && goodsItem.wxItem) {
      report.clickSearchItem(this.state.keyWords, goodsItem.wxItem.itemNo, goodsItem.wxItem.name);
    }
  }

  // 获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      Taro.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload () {
    Taro.setStorage({
      key: 'historyKeyWords',
      data: this.state.historyKeyWords,
    });
  }

  render () {
    const { focus, keyWords, historyKeyWords, dataSource, reportSource, __isPageShow } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-smps-Search" className="wk-smps-Search">
        <View className="search-view">
          <View className="input-view">
            <Input
              className="input-search"
              focus={focus}
              placeholderClass="input-search-placeholder"
              placeholder="关键词搜索"
              onConfirm={this.confrim}
              value={keyWords}
              confirmType="search"
              onFocus={this.enterFocus}
              onBlur={this.loseFocus}
            ></Input>
          </View>
          {/*  聚焦显示  */}
          {focus && (
            <View className="cover">
              {historyKeyWords.length && <View className="tip">最近搜索</View>}
              <View className="history-container">
                {historyKeyWords &&
                  historyKeyWords.map((item, index) => {
                    return (
                      <View
                        className="history-item"
                        key={item.index}
                        onClick={_fixme_with_dataset_(this.clickHistoryItem, { item: item })}
                      >
                        {item}
                      </View>
                    );
                  })}
              </View>
            </View>
          )}

          {/*  结果显示  */}
          <View className="result-view">
            {!!dataSource && dataSource.length === 0 && <Empty message="暂无内容"></Empty>}

            {!dataSource && <Error></Error>}
            {!!dataSource && dataSource.length && (
              <ProductModule list={dataSource} reportSource={reportSource} onClick={this.onItemClick}></ProductModule>
            )}
          </View>
        </View>
        <HoverCart isPageShow={__isPageShow}></HoverCart>
      </View>
    );
  }
}

export default Search;
