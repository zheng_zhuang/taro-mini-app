import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import api from "../../../../wxat-common/api";
import wxApi from '../../../../wxat-common/utils/wxApi';
import constants from "../../../../wxat-common/constants";
import goodsTypeEnum from '../../../../wxat-common/constants/goodsTypeEnum.js';
import template from '../../../../wxat-common/utils/template.js';
import util from '../../../../wxat-common/utils/util.js';
import {  WithPageLifecycles } from 'wk-taro-platform';

import HomeActivityDialog from "../../../../wxat-common/components/home-activity-dialog";
import HoverCart from "../../../../wxat-common/components/cart/hover-cart";
import LoadMore from '../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../wxat-common/components/error/error';
import Empty from '../../../../wxat-common/components/empty/empty';
import CardModule from "../../../../wxat-common/components/cardModule";
import EstateModule from "../../../../wxat-common/components/base/estateModule";
import ProductModule from "../../../../wxat-common/components/base/productModule";
import ServeModule from "../../../../wxat-common/components/base/serveModule";
import { connect } from 'react-redux';
import './index.scss';

const loadMoreStatus = constants.order.loadMoreStatus;

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@WithPageLifecycles
class More extends React.Component {

  state = {
    sourceType: null, // 0、更多服务 1、更多产品 2、更多卡项
    dataSource: [],
    pageNo: null,
    url: null,
    error: false,
    loadMoreStatus: loadMoreStatus.HIDE,
    hasMore: true,
    industry: this.props.industry,
    showType: null,
    isSelfSupport: false,
    __isPageShow: false
  }


  /**
   * 生命周期函数--监听页面加载
   */
   onLoad (options) {
    const showType = 'oneRowTwo'; // display是商品类的展示字段，showType是营销组件类的展示字段
    this.getTemplateStyle();
    const x = parseInt(options.type);
    if (options.name) {
      Taro.setNavigationBarTitle({
        title: options.name,
      });
    } else {
      if (x == 0) {
        Taro.setNavigationBarTitle({
          title: '服务列表',
        });
      } else if (x == 1) {
        Taro.setNavigationBarTitle({
          title: '产品列表',
        });
      } else if (x == 100) {
        Taro.setNavigationBarTitle({
          title: '自营商品',
        });
      } else if (x == 32) {
        Taro.setNavigationBarTitle({
          title: '楼盘列表',
        });
      } else {
        Taro.setNavigationBarTitle({
          title: '卡项列表',
        });
      }
    }
    let links;
    if (x === 0) {
      links = api.goods.list;
    } else if (x === 1 || x === 32 || x === 100) {
      links = api.classify.itemSkuList;
    } else {
      links = api.card.list;
    }
    this.setState({
      industry: this.props.industry,
      sourceType: x === 100 ? 1 : x,
      url: links,
      pageNo: 1,
      showType: showType,
      isSelfSupport: x === 100,
    }, ()=>{
      this.getList(false);
    });

  }

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
   onPullDownRefresh () {
    this.setState({
      pageNo: 1,
      hasMore: true,
    }, ()=>{
      this.getList(true);
    });
  }

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {
    if (this.state.hasMore) {
      this.setState({
        pageNo: ++this.state.pageNo,
      }, ()=>{
        this.getList(false);
      });
    }
  }

  getList(isFromPullDown) {
    const _this = this
    this.setState({
      error: false,
    });

    const params = {
      status: 1,
      type: this.state.sourceType === 2 ? goodsTypeEnum.TIME_CARD.value : this.state.sourceType,
      isShelf: 1,
      sourceTypeList: this.state.isSelfSupport ? [1, 4] : null,
      pageNo: this.state.pageNo,
      pageSize: 10,
    };

    // 判断请 求的商品类型为产品时，则删除请求参数中的type，改用typeList包含产品类型及组合商品类型
    if (params.type === goodsTypeEnum.PRODUCT.value) {
      params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];
    }

    wxApi
      .request({
        url: this.state.url,
        loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true) {
          if (this.isLoadMoreRequest()) {
            _this.setState(
              {
                dataSource: _this.state.dataSource.concat(res.data || [])
              }
            )
          } else {
            _this.setState(
              {
                dataSource: res.data || []
              }
            )
          }
          if (_this.state.dataSource.length === res.totalCount) {
            _this.setState(
              {
                hasMore: false
              }
            )
          }
          _this.setState({
            dataSource: this.state.dataSource,
            hasMore: this.state.hasMore,
          });

        }
      })
      .catch((error) => {
        if (_this.isLoadMoreRequest()) {
          _this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          _this.setState({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          Taro.stopPullDownRefresh();
        }
      });
  }

  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.getList(this.state.url, this.state.sourceType);
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      Taro.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { dataSource, error, showType, loadMoreStatus, sourceType, __isPageShow } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-smpm-More" className="wk-smpm-More">
        <View>
          {sourceType === 0 && (
            <View className="serve-list">
              {!dataSource.length && <Empty message="敬请期待..."></Empty>}

              {error && <Error></Error>}
              { !!dataSource.length && <ServeModule list={dataSource} display={showType}></ServeModule>}

              <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
            </View>
          )}

          {sourceType === 1 && (
            <View className="goods-list">
              {!dataSource.length && <Empty message="敬请期待..."></Empty>}

              {error ? (
                <Error></Error>
              ) : (
                <View>
                  { !!dataSource.length && (
                    <ProductModule list={dataSource} display={showType}></ProductModule>
                  )}

                  <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
                </View>
              )}
            </View>
          )}

          {sourceType === 32 && (
            <View className="goods-list">
              {!dataSource.length && <Empty message="敬请期待..."></Empty>}

              {error ? (
                <Error></Error>
              ) : (
                <View>
                  {!!dataSource.length && (
                    <EstateModule list={dataSource} display={showType}></EstateModule>
                  )}

                  <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
                </View>
              )}
            </View>
          )}

          {(sourceType === 2 || sourceType === 3 || sourceType === 4) && (
            <View className="cards-list">
              {!dataSource.length && <Empty message="敬请期待..."></Empty>}

              {error && <Error></Error>}
              {!!dataSource.length && <CardModule dataSource={dataSource}></CardModule>}

              <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
            </View>
          )}
        </View>
        {sourceType === 1 && <HoverCart isPageShow={__isPageShow}></HoverCart>}
        <HomeActivityDialog
          showPage={'sub-packages/moveFile-package/pages/more/index?type=' + sourceType}
        ></HomeActivityDialog>
      </View>
    );
  }
}

export default More;
