import { _fixme_with_dataset_, _safe_style_ , $getRouter} from 'wk-taro-platform';
import { Block, View, Text, Button, Image } from '@tarojs/components';

import React from 'react';
import Taro from '@tarojs/taro';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import state from '@/state/index';
import store from '@/store/index';
import wxParse from '@/wxat-common/components/parse-wrapper/wxParse';
import ParseWrapper from '@/wxat-common/components/parse-wrapper/index';
import authHelper from '@/wxat-common/utils/auth-helper';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import shareUtil from '@/wxat-common/utils/share';
import template from '@/wxat-common/utils/template';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import Popover from '@/wxat-common/components/popover/index';
import CouponDialog from '@/wxat-common/components/coupon-dialog/index';
import CommentArticle from '@/wxat-common/components/comment-article/index';
import ShareDialog from "@/wxat-common/components/share-dialog";
import { connect } from 'react-redux';
import { setShareAppMessage } from '@/wxat-common/utils/platform/official-account.h5'
import './index.scss';

const app = store.getState();

interface DetailProps {
  sessionId: string
}

interface DetailState {
  orderPopoverVisible: boolean,
  commentOrder: string,
  title: any,
  updateTime: any,
  author: any,
  content: any,
  id: any,
  commentCount: any,
  likeCount: any,
  liked: boolean,
  getCoupon: boolean,
  // 优惠券列表
  coupon: any[],
  // 优惠券弹窗
  couponDialogShow: boolean,
  dataLoad: boolean,
  isFullScreen: boolean,
  isOverHeight: boolean,
  isAll: boolean,
  tmpStyle: any,
  currentVideoIndex: any,
  coverUrl: any,
}

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Detail extends React.Component<DetailProps, DetailState> {
  $router = $getRouter();

  state = {
    orderPopoverVisible: false,
    commentOrder: 'like',
    title: null,
    updateTime: null,
    author: null,
    content: null,
    id: null,
    commentCount: null,
    likeCount: null,
    liked: false,
    getCoupon: false,
    // 优惠券列表
    coupon: [],
    // 优惠券弹窗
    couponDialogShow: false,
    dataLoad: false,
    isFullScreen: !!app.globalData.phoneInfo && app.globalData.phoneInfo.isFullScreenDevice,
    isOverHeight: true,
    isAll: false,
    tmpStyle: null,
    coverUrl: '',
    currentVideoIndex: null,
    refShareDialogCMTP: React.createRef<any>()
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.initRenderData();
    }
  }

  commentModdule = React.createRef();

  refShareDialogCMTP = React.createRef();

  onLoad (options) {    
    this.setState({
      id: options.id,
      toAddComment: false,
    
    }, () =>{
      if (!!state.base.sessionId || !!this.props.sessionId) {
        this.initRenderData();
      }
      this.getTemplateStyle();
    });
    console.log('传进来的id', this.state.id);
  }

  initRenderData() {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({ dataLoad: true });

    if (!authHelper.checkAuth(false)) {
      this.setState({
        toAddComment: true
      });
    }
    this.getArticle();
  }

  // 获取文章详情
  getArticle() {
    wxApi
      .request({
        url: api.article.detail,
        data: {
          id: this.state.id,
        },

        loading: true,
      })
      .then((res) => {
        wxApi.stopPullDownRefresh();
        if (res.success) {
          this.setState({
            title: res.data.title,
            coverUrl: res.data.coverUrl,
            updateTime: res.data.updateTime,
            author: res.data.author,
            content: res.data.content,
            commentCount: res.data.commentCount || 0,
            likeCount: res.data.virtualLikeCount || 0,
            liked: res.data.liked || false, // TODO:
          });
          try {
            this.setState({
              content: wxParse('html', res.data.content)
            })
          } catch (err) {
            console.log(err);
          }
          Taro.setNavigationBarTitle({ title: res.data.title || '' });
          Taro.nextTick(() => {
            this.getContentHeight();
            setShareAppMessage(this.onShareAppMessage())
            console.log(this.onShareAppMessage());
          });
        }
      })
      .catch((err) => {
        wxApi.stopPullDownRefresh();
        console.error('获取图文详情err', err);
      });
  }

  // 文章
  onChangeLike = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    wxApi
      .request({
        url: api.article.changeLike,
        method: 'POST',
        data: {
          generalId: +this.state.id,
          likeType: 1,
          status: +!this.state.liked,
        },

        loading: true,
      })
      .then((res) => {
        if (res.success) {
          this.setState({
            liked: !this.state.liked,
            likeCount: this.state.liked ? this.state.likeCount - 1 : this.state.likeCount + 1
          });
        }
      });
  }

  // 文章评论
  toCommentPage = () => {
    if (!authHelper.checkAuth(true, false)) {
      return;
    }
    if (!this.state.id) {
      Taro.showToast({
        title: '当前文章ID为空',
        icon: 'none',
      });

      return;
    }
    this.setState({
      toAddComment: true,
    });

    wxApi.$navigateTo({
      url: '/wxat-common/pages/evaluation/index',
      // url: '/sub-packages/moveFile-package/pages/evaluation/index',
      data: {
        type: 'ARTICLE',
        orderNo: this.state.id,
      },
    });
  }

  // 删除评论
  deleteComment() {
    this.setState({
      commentCount: this.state.commentCount - 1,
    });
  }

  // 文章分享
  onShareAppMessage() {
    const { id, title, coverUrl } = this.state;
    console.log(coverUrl);
    if (id) {
      const url = `sub-packages/moveFile-package/pages/graph/detail/index?id=${id}`
      const path = shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.GRAPH_DETAIL,
        bzName: `分享文章${title ? '-' + title : ''}`,
      });
      return {
        title: title || '',
        path,
        imageUrl:coverUrl
      };
    } else {
      return {};
    }
  }

  onPullDownRefresh() {
    this.setState({ pageNo: 1 });
    this.getArticle(true);
    if (this.commentModdule.current) {
      this.commentModdule.current.init(true);
    }
  }

/*  onShareAppMessage = (e) => {
    return {
      title: this.state.title,
      path: shareUtil.buildShareUrlPublicArguments({
        url: `sub-packages/moveFile-package/pages/graph/detail/index?id=${this.state.id}`,
        bz: shareUtil.ShareBZs.GRAPH_DETAIL,
        bzName: '图文详情',
      })
    };
  }*/

  // 自动领取优惠券
  autoFetchCoupons() {
    if (!authHelper.checkAuth(false, false)) {
      return;
    }

    wxApi
      .request({
        url: api.coupon.auto_collect,
        loading: true,
        checkSession: true,
        data: {
          eventType: 6,
          articleId: this.state.id,
        },
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            coupon: res.data || [],
            couponDialogShow: true,
          });
        }
      })
      .catch(() => {});
  }

  // 查看是否有评论自动发放优惠券
  fetchSendCoupons() {
    if (!authHelper.checkAuth(false, false)) {
      return;
    }
    wxApi
      .request({
        url: api.coupon.get_coupons,
        loading: false,
        quite: true,
        data: { eventType: 6 },
      })
      .then((res) => {
        if (res.data && res.data.length) {
          const status = res.data[0].status;
          if (status === 3) return false;
          this.setState({
            coupon: res.data[0],
            couponDialogShow: true,
          });
        }
        console.log(this.state.coupon);
      })
      .catch(() => {});
  }
  // componentDidShow (){
  //   console.log("11111111111111111111111111111111111111111")
  // }
  componentDidShow() {    
    console.log("11111111111111111111111111111111111111111")
    // 从别的页返回时刷新评论列表
    let hasCommentOrderValue = false;
    if(!!wxApi.getStorageSync('commentOrder')){
      hasCommentOrderValue = true;
    }
    console.log(wxApi.getStorageSync('commentOrder'));
    wxApi.removeStorageSync('commentOrder');
    this.setState({
      commentOrder:hasCommentOrderValue ? wxApi.getStorageSync('commentOrder'):'like',
    })
    if (this.state.toAddComment) {
      this.setState({
        toAddComment: false,
      });

      this.onPullDownRefresh();
      if (protectedMailBox.read('getCoupon')) {
        // this.fetchSendCoupons();
        this.autoFetchCoupons();
        protectedMailBox.send('sub-packages/moveFile-package/pages/graph/detail/index', 'getCoupon', false);
      }
    }
  }

  onReachBottom() {
    // 页面到达底部加载更多评论列表
    if (this.commentModdule.current) {
      this.commentModdule.current.onReachBottom();
    }
  }

  toggleOrderVisible = () => {
    this.setState({ orderPopoverVisible: !this.state.orderPopoverVisible });
  }

  changeOrder = (e) => {
    const orderType = e.currentTarget.dataset.orderType;
    this.setState({ commentOrder: orderType, orderPopoverVisible: false });
    if (this.commentModdule.current) {
      this.commentModdule.current.getCommentList(orderType);
    }
  }

  // 点击小程序卡片或文字和图片跳转到页面
  onNavigateToDataMiniprogramPath(e) {
    const url = e.currentTarget.dataset.path;
    const classifyPath = 'wxat-common/pages/classify/index';
    const cartPath = 'sub-packages/moveFile-package/pages/shopping-cart/index';
    // 分类和购物车页面跳转
    {
      url === classifyPath ? app.jumpToClassify() : url === cartPath ? app.jumpToCart() : wxApi.$navigateTo({ url });
    }
  }

  // 获取文章内容 dom 高度
  getContentHeight() {
    Taro.createSelectorQuery()
      .select('#detailContent')
      .boundingClientRect( (rect) => {
        const isOverHeight = rect.height > 800;
        this.setState({
          isOverHeight,
          isAll: !isOverHeight,
        });
        console.log(rect, this.state.isOverHeight, this.state.isAll);
      })
      .exec();
  }

  onChangeShowAll = () => {
    this.setState({
      isAll: !this.state.isAll,
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // fix多视频同时播放
  onVideoPlay(e) {
    const curIdx = e.currentTarget.id;
    if (!this.state.currentVideoIndex) {
      this.setState({
        currentVideoIndex: curIdx,
      });

      const videoContext = Taro.createVideoContext(curIdx, this); // 这里对应的视频id
      videoContext.play();
    } else {
      // 有播放时先将prev暂停，再播放当前点击的current
      const videoContextPrev = Taro.createVideoContext(this.state.currentVideoIndex, this);
      // this是在自定义组件下，当前组件实例的this，以操作组件内 video 组件（在自定义组件中药加上this，如果是普通页面即不需要加）
      if (this.state.currentVideoIndex != curIdx) {
        videoContextPrev.pause();
        this.setState({
          currentVideoIndex: curIdx,
        });

        const videoContextCurrent = Taro.createVideoContext(curIdx, this);
        videoContextCurrent.play();
      }
    }
  }

  handleGoVR(e) {
    const vrLink = e.currentTarget.dataset.vrlink;
    protectedMailBox.send('sub-packages/moveFile-package/pages/receipt/index', 'receiptURL', vrLink);

    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/receipt/index',
      data: {},
    });
  }

  /**
   * 分享
   */
   handleSelectChanel() {
    if (this.state.refShareDialogCMTP.current) {
      this.state.refShareDialogCMTP.current.show();
    }
  }

  render() {
    const {
      isAll,
      title,
      updateTime,
      author,
      content,
      isOverHeight,
      commentCount,
      orderPopoverVisible,
      commentOrder,
      id,
      isFullScreen,
      tmpStyle,
      liked,
      likeCount,
      couponDialogShow,
      coupon,
    } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpgd-Detail" className="wk-mpgd-Detail">
        <AuthPuop></AuthPuop>
        {!!content && (
          <View className="graph-detail">
            <View className={'view-box ' + (isAll ? '' : 'view-hide')}>
              <View className="detail-content" id="detailContent">
                <View className="title">{title}</View>
                <View>
                  <Text className="date">{updateTime}</Text>
                  <Text className="author">{author}</Text>
                </View>
                <View className="content">
                  <ParseWrapper parseContent={content} />
                  {/* <TaroParseTmpl data={{ wxParseData: content.nodes }}></TaroParseTmpl> */}
                </View>
              </View>
              {!!isOverHeight && (
                <View className={'all-article ' + (isAll ? '' : 'all-article-hide')} onClick={this.onChangeShowAll}>
                  {isAll ? '收起全文' : '展开全文'}
                </View>
              )}
            </View>
            {/*  评论标题 排序项  */}
            {!!commentCount && (
              <View className="detail-count">
                <Text className="comment">评论</Text>
                <Popover
                  visible={orderPopoverVisible}
                  className="order"
                  position="bottom"
                  renderOutlet={
                    <Block>
                      <View onClick={this.toggleOrderVisible}>
                        <View className="order-outlet">
                          <Text>{commentOrder === 'like' ? '按点赞' : '按时间'}</Text>
                          <Text className="outlet-logo"></Text>
                        </View>
                      </View>
                    </Block>
                  }
                  renderContent={
                    <Block>
                      <View>
                        <View
                          className={'order-option ' + (commentOrder === 'like' ? 'active' : '')}
                          onClick={_fixme_with_dataset_(this.changeOrder, { orderType: 'like' })}
                        >
                          <Text>按点赞</Text>
                        </View>
                        <View
                          className={'order-option ' + (commentOrder === 'time' ? 'active' : '')}
                          onClick={_fixme_with_dataset_(this.changeOrder, { orderType: 'time' })}
                        >
                          <Text>按时间</Text>
                        </View>
                      </View>
                    </Block>
                  }
                ></Popover>
                {/*  <text class="like">赞 {{likeCount}}</text>  */}
              </View>
            )}

            {/*  评论列表  */}
            {!!commentCount && (
              <CommentArticle
                type="ARTICLE"
                order={commentOrder}
                itemNo={id}
                ref={this.commentModdule}
                onDeleteComment={this.deleteComment}
              ></CommentArticle>
            )}

            {/*  兼容IOS底部遮盖问题  */}
            <View className="blank"></View>
            {/*  文章操作  */}
            <View className={'footer ' + (isFullScreen ? 'fix-full-screen' : '')}>
              {process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true' ? (<View className="footer-item share-btn" onClick={this.handleSelectChanel.bind(this)}>
                <Image
                  className="footer-logo"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/share-new.png"
                ></Image>
                <View className="footer-label">分享</View>
              </View>) : (<Button className="footer-item share-btn" openType="share">
                <Image
                  className="footer-logo"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/share-new.png"
                ></Image>
                <View className="footer-label">分享</View>
              </Button>)}
              {!!commentCount && (
                <View className="footer-item" onClick={this.toCommentPage}>
                  <Image
                    className="footer-logo"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/comment-new.png"
                  ></Image>
                  {!!commentCount && <View className="badge-number">{commentCount <= 99 ? commentCount : '99+'}</View>}

                  <View className="footer-label">评论</View>
                </View>
              )}

              <View className="footer-item" onClick={this.onChangeLike}>
                {liked ? (
                  <Image
                    className="footer-logo active-like"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/active-like-new.png"
                    style={_safe_style_('background:' + tmpStyle.btnColor)}
                  ></Image>
                ) : (
                  <Image
                    className="footer-logo"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/like-new.png"
                  ></Image>
                )}

                {!!likeCount && <View className="badge-number">{likeCount < 1000 ? likeCount : '999+'}</View>}

                <View className="footer-label" style={_safe_style_('color: ' + (liked ? tmpStyle.btnColor : '#333'))}>
                  点赞
                </View>
              </View>
            </View>
            <CouponDialog
              title="恭喜获得优惠券"
              visible={couponDialogShow}
              coupon={coupon}
              eventType="6"
            ></CouponDialog>
            <HomeActivityDialog
              showPage={'sub-packages/moveFile-package/pages/group/detail/index?id=' + id}
            ></HomeActivityDialog>
          </View>
        )}
        {/*  分享对话弹框  */}
        <ShareDialog
          childRef={this.state.refShareDialogCMTP}
          qrCodeParams={null}
          showLabelPrice
          posterTips="向您推荐了这篇图文"
        ></ShareDialog>
      </View>
    );
  }
}

Detail.enableShareAppMessage = true
export default Detail;
