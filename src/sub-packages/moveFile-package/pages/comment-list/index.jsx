import React from 'react'; // @scoped
import { forwardPageHandler } from '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import CommentModule from '@/wxat-common/components/comment-module';
import './index.scss';

@hoc
class CommentList extends React.Component {
  /**
   * 页面的初始数据
   */
  state = {
    commentId: null,
  };

  commentTitle = null;

  /**
   * 生命周期函数--监听页面加载
   */
  componentDidMount() {
    const commentData = protectedMailBox.read('comment-data');
    if (commentData) {
      this.setState({
        commentId: commentData.commentId || null,
      });

      this.commentTitle = commentData.commentTitle || null;
    }
    wxApi.setNavigationBarTitle({
      title: this.commentTitle || '评论列表',
    });
  }

  render() {
    const { commentId } = this.state;
    return (
      !!commentId && (
        <View
          data-fixme='03 add view wrapper. need more test'
          data-scoped='wk-wpc-CommentList'
          className='wk-wpc-CommentList'
        >
          <CommentModule itemNo={commentId}></CommentModule>
        </View>
      )
    );
  }
}

export default CommentList;
