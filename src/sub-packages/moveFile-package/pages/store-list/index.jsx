import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import api from '../../../../wxat-common/api/index';
import wxApi from '../../../../wxat-common/utils/wxApi';
import ProtectedMailBox from '../../../../wxat-common/utils/protectedMailBox';
import template from '../../../../wxat-common/utils/template';
import industryEnum from '../../../../wxat-common/constants/industryEnum';
import PickStore from '../../../../wxat-common/components/pick-store/index';
import { connect } from 'react-redux';
import { notifyStoreDecorateSwitch } from '../../../../wxat-common/x-login/decorate-configuration-store.ts';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import { updateGlobalDataAction } from '@/redux/global-data';
import storeData from '@/store'
const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class StoreList extends React.Component {
  $router = $getRouter();
  state = {
    storeIds: null, // 门店过滤列表id字符串
  };

  mailKey = '';

  componentDidMount() {
    const options = this.$router.params;
    this.mailKey = options && !!options.mailKey ? options.mailKey : '';

    // 门店过滤列表id字符串
    const storeIds = ProtectedMailBox.read('storeIds');
    this.setState({
      storeIds: storeIds || null,
    });

    this.getTemplateStyle();

    this.initRender();
  }
  
  componentWillUnmount = () => {
    this.setState = (state,callback)=>{
      return;
    };
  }

  onPickStore(store) {
    wxApi.removeStorageSync('storeId');
    wxApi.setStorageSync('storeId',store.id);
    if (!!this.mailKey) {
      //当前页面信息
      const pages = Taro.getCurrentPages();
      if (!!pages && pages.length > 1) {
        const lastru = pages[pages.length - 2]['route'];
        ProtectedMailBox.send(lastru, this.mailKey, store);
      }
      wxApi.$navigateBack();
    } else {
      wxApi
        .request({
          url: api.store.choose + '?storeId=' + store.id,
          loading: true,
        })
        .then(() => {
          //清除购物车缓存
          let storageList = ['serviceTreeData', 'serviceOrderList', 'storageList', 'storageOrderList', 'sceneCartList', 'sceneCartSelectList']
          storageList.forEach(item => {
            wxApi.removeStorageSync(item)
          })
          notifyStoreDecorateSwitch(store).then(() => {
            wxApi.navigateBack();
          });
          storeData.dispatch(updateGlobalDataAction({
            roomCode:'',
            roomCodeTypeId:'',
            roomCodeTypeName:''
          }))
        });
    }
  }

  initRender() {
    this.pickStoreCOMPT && this.pickStoreCOMPT.initRender();
  }

  onPullDownRefresh() {
    this.pickStoreCOMPT && this.pickStoreCOMPT.refreshList();
  }

  onReachBottom() {
    this.pickStoreCOMPT && this.pickStoreCOMPT.loadMore();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  refPickStoreCOMPT = (node) => (this.pickStoreCOMPT = node);

  render() {
    const { storeIds } = this.state;
    return (
      <Block>
      <PickStore ref={this.refPickStoreCOMPT} storeIds={storeIds} onPickStore={this.onPickStore.bind(this)}/>
      <home-activity-dialog showPage="sub-packages/moveFile-package/pages/store-list/index"></home-activity-dialog>
      </Block>
    );
  }
}

export default StoreList;
