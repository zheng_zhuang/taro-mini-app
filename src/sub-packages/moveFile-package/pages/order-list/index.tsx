import React from 'react';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi/index';
import api from '@/wxat-common/api/index';
import scan from '@/wxat-common/utils/scanH5'
import OrderList from '@/wxat-common/components/tabbarLink/order-list/index';
import CouponDialog from '@/wxat-common/components/coupon-dialog/index';
import classNames from 'classnames';
import RoomCode from '@/wxat-common/components/room-code/index'
import { $getRouter } from '@/wxat-common/utils/platform';
import styles from './index.module.scss';

interface State {
  component: {
    [key: string]: any
  },
  sendCoupon: number,
  coupon: any[],
  dialogShow: boolean,
}

class OrderList1 extends React.Component<State> {
  $router = $getRouter()
  state = {
    component: null,
    // 是否发放优惠券，下完单之后跳转到订单列表，发放优惠券
    sendCoupon: 0,
    coupon: null,
    dialogShow: false,
    updateRoomCode:false,
    codeId:''
  };

  componentFn = (node) => {
    this.setState({
      component: node
    })
    console.log(this.state.component);
  };

  onLoad(options: any) {
    this.setState({
      sendCoupon: options.sendCoupon || 0,
    });

    if (options.sendCoupon) {
      this.fetchSendCoupons();
    }
    this.init().then()
  }

  init = async () =>{
    const options = this.$router.params
    if(process.env.WX_OA === 'true' && options.id){
      this.setState({
        codeId:options.id
      })
      const data: any = await scan.scanRoomCodeFunc(options)
      if(data?.isScan){
        this.setState({
          updateRoomCode:data.isScan
        })
      }
    }
  }

  fetchSendCoupons() {
    wxApi
      .request({
        url: api.coupon.get_coupons,
        loading: false,
        quite: true,
        data: { eventType: 1 },
      })
      .then((res: any) => {
        if (res.data) {
          this.setState({
            coupon: res.data || [],
            dialogShow: true,
          });
        }
      })
      .catch((err: any) => {
        console.log(err);
      })
  }

  onIsUpdateRoomCode = () =>{
    this.setState({
      updateRoomCode:false
    })
  }

  render() {
    const { dialogShow, coupon = [], sendCoupon } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-smpo-OrderList'
        className={classNames(styles['wk-smpo-OrderList'])}
      >
        <OrderList ref={this.componentFn}></OrderList>
        {/*  优惠券对话弹框  */}
        {!!(sendCoupon && coupon && coupon.length) && (
          <CouponDialog visible={dialogShow} coupon={coupon} eventType='1'></CouponDialog>
        )}
         <RoomCode onIsUpdateRoomCode={this.onIsUpdateRoomCode} codeId={this.state.codeId} updateRoomCode={this.state.updateRoomCode}></RoomCode>
      </View>
    );
  }
}

export default OrderList1;
