import {$getRouter} from 'wk-taro-platform';
import React, {Component} from 'react';
import Taro from '@tarojs/taro';
import {View, WebView} from '@tarojs/components';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi'

export default class extends Component {
  $router = $getRouter();
  url = '';
  title = ''; /*请尽快迁移为 componentDidMount 或 constructor*/
  isStoage = 0

  UNSAFE_componentWillMount() {
    this.url = decodeURIComponent(this.$router.params.url || '');
    this.title = decodeURIComponent(this.$router.params.title || '');
    this.isStoage = Number(this.$router.params.isStoage || 0)
    Taro.setNavigationBarTitle({title: this.title});
    if (this.isStoage == 1) {
      this.url = decodeURIComponent(wxApi.getStorageSync('uk_hotel_url'))
    }
  }

  render() {
    return (
      <View className='webview'>
        <WebView src={this.url}/>
      </View>
    );
  }
}
