import { _safe_style_ } from 'wk-taro-platform';
import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
// pages/mine/account-balance/index.js
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import utilDate from '../../../../../wxat-common/utils/date.js';
import template from '../../../../../wxat-common/utils/template.js';

// import AuthPuop from '../../../../../wxat-common/components/authorize-puop/index';
import AuthUser from '../../../../../wxat-common/components/auth-user/index';
import WaterBillDetail from '../../../../../wxat-common/components/water-bill-detail/index';
import Error from '../../../../../wxat-common/components/error/error';
import './index.scss';

class AccountBalance extends React.Component {

  state = {
    userReady: false,
    balance: 0,
    pageNo: 1,
    hasMore: true,
    billList: [],
    error: false,
    dateTime: {
      startTime: null,
      endTime: null,
    },
    _reload: false,
    tmpStyle: {},
  }

  groupBills = () => {
    const billList = this.state.billList;
    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth();
    let dispplayMonth = null;
    let monthList = null;
    const list = [];
    billList.forEach((item) => {
      const billDate = new Date(item.createTime);
      const billYear = billDate.getFullYear();
      const billMonth = billDate.getMonth();

      let display = '';

      if (billYear === year) {
        if (month === billMonth) {
          display = '本月';
        } else {
          display = `${billMonth + 1}月`;
        }
      } else {
        display = utilDate.format(billDate, 'yyyy年MM月');
      }

      if (display !== dispplayMonth) {
        dispplayMonth = display;
        monthList = {
          display,
          bills: [],
        };

        list.push(monthList);
      }
      monthList.bills.push({
        createTime: utilDate.format(billDate, 'yyyy/MM/dd hh:mm:ss'),
        amount: item.amount,
        desc: item.orderDesc,
      })
    })
    return list;
  }

  /**
  * 生命周期函数--监听页面显示
  */
   componentDidShow() {
   this.getTemplateStyle();
   if (this.state.userReady) {
     if (!this.state._reload) {
       this.setState(
         {
          _reload: true
         }
       )
       return;
     }
     this.getBalance();
     this.getBill(false);
   }
  }

      /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {
    // 页面相关事件处理函数--监听用户下拉动作
    this.state.pageNo = 1;
    this.state.hasMore = true;
    this.getBalance();
    this.getBill(true);
  }

  /**
  * 页面上拉触底事件的处理函数
  */
  onReachBottom () {
   // 页面上拉触底事件的处理函数
   if (this.state.hasMore) {
     this.getBill(false)
   }
  }

  clickRecharge = (e) => {
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/mine/recharge-center/index',
    });
  }

  clickGo = (e) => {
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/mine/recharge-center/index',
    });
  }

  userInfoReady = () => {
    this.setState({
      userReady: true,
    });

    this.getBalance();
    this.getBill();
  }

  getBalance() {
    return wxApi
      .request({
        url: api.userInfo.balance,
        data: {},
      })
      .then((res) => {
        this.setState({
          balance: res.data.balance,
        });
      })
  }

  /**
  * @func 获取流水账单
  * @param {Boolean} isPullDownRefresh - 是否通过下拉刷新操作
  */
  getBill(isPullDownRefresh) {
   this.setState({
     error: false,
   });

   let requestData = {
     page: this.state.pageNo,
     pageSize: 10,
   };

   if (this.state.dateTime.startTime != null) {
     requestData.startTime = this.state.dateTime.startTime;
     requestData.endTime = this.state.dateTime.endTime;
   }

   return wxApi
     .request({
       url: api.bill.query,
       loading: true,
       data: requestData,
     })
     .then((res) => {
       if (this.isLoadMoreRequest()) {
         this.state.billList = this.state.billList.concat(res.data || []);
       } else {
         this.state.billList = res.data || [];
       }

       if (this.state.billList.length === res.totalCount) {
         this.state.hasMore = false;
       }
       this.setState({
          pageNo: this.state.pageNo + 1,
         billList: this.state.billList
       });

       if (isPullDownRefresh) {
         Taro.stopPullDownRefresh();
       }
     })
     .catch(() => {
       this.setState({
         error: true,
       });

       if (isPullDownRefresh) {
         Taro.stopPullDownRefresh();
       }
     })
  }

  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  onDateChange = (value) => {
    value = value.replace(/-/, '/') + '/01'
    let startTime = new Date(value);
    let endTime = new Date(value);
    endTime.setMonth(endTime.getMonth() + 1);
    this.setState(
      {
        pageNo: 1,
        dateTime: {
          startTime: utilDate.format(startTime),
          endTime: utilDate.format(endTime)
        }
      },
      () => {
        this.getBill(false)
      }
    )
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      Taro.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    },()=>{
    });
  }

  render() {
    const { tmpStyle, balance, error } = this.state;
    return (
      <View data-scoped="wk-mpma-AccountBalance" className="wk-mpma-AccountBalance account-balance">
        <AuthUser onReady={this.userInfoReady}>
          <View className="header" style={_safe_style_('background:' + tmpStyle.btnColor)}>
            <View className="view">账户余额(元)</View>
            <View className="view">{filters.moneyFilter(balance, true)}</View>
            <View
              className="recharge-btn"
              onClick={this.clickRecharge}
              style={_safe_style_('color:' + tmpStyle.btnColor)}
            >
              充值
            </View>
          </View>
          {/*  view class='tip'>
              <View class='tip-content'>
                <View>充300送100</View>
                <View>显示优惠，多充多送</View>
              </View>
              <View class='go-btn' bindtap='clickGo'>GO</View>
            </view  */}
          {error && <Error></Error>}
          {!error && <WaterBillDetail onDateChangeEvent={this.onDateChange} isMoney={true} dataSource={this.groupBills()}></WaterBillDetail>}
        </AuthUser>
      </View>
    );
  }
}

export default AccountBalance;
