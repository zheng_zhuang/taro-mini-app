import React, { Component } from 'react';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template';
import IntegralList from '@/wxat-common/components/tabbarLink/integral-list/index';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
interface IState {}
interface IProps {
  scoreName: string;
}
const mapStateToProps = (state) => {
  return { scoreName: state.globalData.scoreName || '积分' };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class IntegralMall extends Component<IProps, IState> {
  state = {}; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    this.getTemplateStyle();
    wxApi.setNavigationBarTitle({
      title: `${this.props.scoreName}商城`,
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
  }

  render() {
    const { scoreName } = this.props;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-mpi-IntegralMall'
        className='wk-mpi-IntegralMall'
      >
        <IntegralList id='rooms-list' scoreName={scoreName} />
        <HomeActivityDialog showPage='sub-packages/moveFile-package/pages/mine/integral-mall/index'></HomeActivityDialog>
      </View>
    );
  }
}

export default IntegralMall;
