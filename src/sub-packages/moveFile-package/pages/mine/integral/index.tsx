import React, { Component } from 'react'; // @externalClassesConvered(AnimatDialog)
import { $getRouter, _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
// pages/mine/account-balance/index.tsx
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import uitlDate from '@/wxat-common/utils/date';
import template from '@/wxat-common/utils/template';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import WaterBillDetail from '@/wxat-common/components/water-bill-detail/index';
import { NOOP_OBJECT } from '@/wxat-common/utils/noop';
import './index.scss';
import RoomCode from '@/wxat-common/components/room-code/index'
import { updateGlobalDataAction } from '@/redux/global-data';
import scan from '@/wxat-common/utils/scanH5'
import store from '@/store'
const mapStateToProps = (state) => ({
  wxUserInfo: state.base.wxUserInfo,
  scoreName: state.globalData.scoreName || '积分',
});
interface IProps {
  wxUserInfo: any;
  scoreName: string;
}
interface IState {
  score: number;
  pageNo: number;
  hasMore: boolean;
  billList: any[];
  scoreIntroduction: any;
  error: boolean;
  dateTime: {
    startTime: any;
    endTime: any;
  };
  avatarImgUrl: any;
}
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class Integral extends Component<IProps, IState> {
  $router=$getRouter()
  state = {
    score: 0,
    pageNo: 1,
    hasMore: true,
    billList: [],
    scoreIntroduction: '',
    error: false,
    dateTime: {
      startTime: null,
      endTime: null,
    },

    updateRoomCode:false,
    codeId:'',

    avatarImgUrl: null,
  }; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    const { wxUserInfo, scoreName } = this.props;
    this.setState({
      avatarImgUrl: wxUserInfo?.avatarUrl,
    });

    wxApi.setNavigationBarTitle({
      title: `${scoreName}中心`,
    });

    this.getTemplateStyle();
  }

  componentDidMount() {
    this.getBalance();
    this.getBill();
    this.getScoreIntroduction();
    this.init()
  }
  init = async () =>{
    const options = this.$router.params
    if(process.env.WX_OA === 'true' && options.id){
      this.setState({
        codeId:options.id
      })
      const data:any = await scan.scanRoomCodeFunc(options)
      if(data && data.isScan){
        this.setState({
          updateRoomCode:data.isScan
        })
      }
    }
  }
  componentDidShow() {
    if (!this._reload) {
      this._reload = true;
      return;
    }

    this.getBalance();
    this.getBill();
    this.getScoreIntroduction();
  }

  onPullDownRefresh() {
    // 页面相关事件处理函数--监听用户下拉动作
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      this.getBill
    );
  }

  onReachBottom() {
    // 页面上拉触底事件的处理函数
    if (this.state.hasMore) {
      this.getBill();
    }
  }

  groupBills(arrlist) {
    const list = [];
    if (arrlist && arrlist.length > 0) {
      const billList = arrlist;
      const now = new Date();
      const year = now.getFullYear();
      const month = now.getMonth();
      let dispplayMonth = null;
      let monthList = null;
      billList.forEach((item) => {
        const billDate = new Date(item.createTime);
        const billYear = billDate.getFullYear();
        const billMonth = billDate.getMonth();

        let display = '';

        if (billYear === year) {
          if (month === billMonth) {
            display = '本月';
          } else {
            display = `${billMonth + 1}月`;
          }
        } else {
          display = uitlDate.format(billDate, 'yyyy年MM月');
        }

        if (display !== dispplayMonth) {
          dispplayMonth = display;
          monthList = {
            display,
            bills: [],
          };

          list.push(monthList);
        }
        monthList.bills.push({
          createTime: uitlDate.format(billDate, 'yyyy/MM/dd hh:mm:ss'),
          amount: item.amount,
          desc: item.orderDesc,
        });
      });
    }
    return list;
  }

  showInstruction = () => {
    this.animatDialogCOMPT.show({
      scale: true,
    });
  };

  hideInstruction = () => {
    this.animatDialogCOMPT.hide();
  };

  getBalance() {
    return wxApi
      .request({
        url: api.userInfo.balance,
        data: {},
      })
      .then((res) => {
        this.setState({
          score: res.data.score,
        });
      })
      .catch((error) => {
        console.log('score: error: ' + JSON.stringify(error));
      });
  }

  getScoreIntroduction() {
    return wxApi
      .request({
        url: api.user.scoreIntroduction,
        data: {},
      })
      .then((res) => {
        this.setState({
          scoreIntroduction: res.data || '无',
        });
      })
      .catch((error) => {});
  }

  getBill = () => {
    this.setState({
      error: false,
    });

    const { startTime, endTime } = this.state.dateTime;
    const requestData = {
      page: this.state.pageNo,
      pageSize: 10,
      ...(startTime != null ? { startTime, endTime } : NOOP_OBJECT),
    };

    return wxApi
      .request({
        url: api.user.waterScore,
        loading: true,
        data: requestData,
      })
      .then((res) => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            billList: this.groupBills(this.state.billList.concat(res.data || [])),
          });
        } else {
          this.setState({
            billList: this.groupBills(res.data) || [],
          });
        }
        if (this.state.billList.length === res.totalCount) {
          this.setState({
            hasMore: false,
          });
        }
        // this.setState({
        //   billList: that.groupBills(this.state.billList),
        //   pageNo: this.state.pageNo + 1
        // })
      });
    /* .catch(error => {
                                    this.setState({
                                      error: true
                                    })
                                    console.log('balance: error: ' + JSON.stringify(error))
                                  }) */
  };

  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  onDateChange = (date: string) => {
    const time = new Date(date);
    const startTime = uitlDate.format(time);
    time.setMonth(time.getMonth() + 1);
    const endTime = uitlDate.format(time);

    this.setState(
      {
        dateTime: {
          startTime,
          endTime,
        },

        pageNo: 1,
      },

      this.getBill
    );
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  refAnimatDialogCOMPT = (node) => (this.animatDialogCOMPT = node);
  onIsUpdateRoomCode = () =>{
    this.setState({
      updateRoomCode:false
    })
  }
  render() {
    const { avatarImgUrl, score, scoreIntroduction, billList, error,codeId,updateRoomCode } = this.state;
    const { scoreName } = this.props;
    return (
      <View data-scoped='wk-mpi-Integral' className='wk-mpi-Integral'>
        <View className='detail'>
          {!avatarImgUrl && <View className='head-icon' />}
          {!!avatarImgUrl && <Image className='head-icon' src={avatarImgUrl} />}
          <View className='detail-abs' style={_safe_style_('z-index:1;')}>
            <View className='title'>我的{scoreName}</View>
            <View className='score'>{score}</View>
          </View>
          <View className='bottom'>
            <View onClick={this.showInstruction} className='showInstruction'>
              {scoreName}规则
            </View>
          </View>
        </View>
        <AnimatDialog ref={this.refAnimatDialogCOMPT} animClass='instruction'>
          <View className='instruction-body'>
            <View className='instruction-body__title'>{scoreName}规则</View>
            <Text className='instruction-body__view'>{scoreIntroduction}</Text>
            <View className='instruction-body__btn' onClick={this.hideInstruction}>
              确定
            </View>
          </View>
        </AnimatDialog>
        {!error && <WaterBillDetail dataSource={billList} isMoney={false} onDateChangeEvent={this.onDateChange} />}
        <RoomCode onIsUpdateRoomCode={this.onIsUpdateRoomCode} codeId={this.state.codeId} updateRoomCode={this.state.updateRoomCode}></RoomCode>
      </View>
    );
  }
}

export default Integral;
