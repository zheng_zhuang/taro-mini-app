import { _fixme_with_dataset_ } from 'wk-taro-platform';
import { View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import constant from '../../../../../wxat-common/constants/index.js';
//获取会员码的定时器
import HomeActivityDialog from '../../../../../wxat-common/components/home-activity-dialog/index';
import Error from '../../../../../wxat-common/components/error/error';
import './index.scss';
const memCodeCheck = {
  loopId: -1,
  intervalTime: 30000,
};
class MemberCode extends React.Component {

  state = {
    showImg: false,
    membershipCode: {
      barCode: '',
      error: false,
    },
  }

  /**
  * 生命周期函数--监听页面加载
  */
  onLoad() {
    this.startGettingBarCode();
  }
  
  componentWillUnmount() {
    this._stopMemCodeInterval();
  }

  // 开始循环获取会员码
  startGettingBarCode = () => {
    this._getBarCodePath();
    this._stopMemCodeInterval();
    memCodeCheck.loopId = setInterval(() => {
      this._getBarCodePath();
    }, memCodeCheck.intervalTime);
  }

  // 获取会员码
  _getBarCodePath = () => {
    wxApi
      .request({
        url: api.membershipCode.getBarCode,
        loading: true,
        checkSession: true
      })
      .then((res) => {
        this.setState(
          {
            membershipCode: {
              barCode: res.data ? constant.PREFIX_BASE64 + res.data : '',
              error: false
            }
          }
        )
      })
      .catch(() => {
        this._stopMemCodeInterval();
        this.setState({
          membershipCode: {
            error: true
          }
        });
      });
  }

  _stopMemCodeInterval = () => {
    if (memCodeCheck.loopId) {
      clearInterval(memCodeCheck.loopId);
    }
  }

  handleShowLargeImage = (flag) => {
    this.setState(
      {
        showImg: flag
      }
    )
  }

  render() {
    const { membershipCode, showImg } = this.state
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mpmm-MemberCode"
        className="wk-mpmm-MemberCode"
      >
        <View className="membership-code">
          <View className="membership-header">
            <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/images/mine/group.png"></Image>
            <Text className="head-title">会员码</Text>
          </View>
          <View className="mem-code-container">
            {membershipCode.error && <Error></Error>}
            {!membershipCode.error && (
              <View className="mem-code-content">
                <View>
                  <View className="item mem-code-area">
                    <Image
                      className="bar-code"
                      mode="aspectFit"
                      src={membershipCode.barCode}
                      onClick={() => {this.handleShowLargeImage(true)}}
                    ></Image>
                  </View>
                  <View className="item mem-code-info" onClick={this.startGettingBarCode}>
                    <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/images/mine/rectangle-path.png"></Image>
                    <View>支付码每30秒自动更新，请在店内出示使用</View>
                  </View>
                </View>
              </View>
            )}
          </View>
        </View>
        <HomeActivityDialog showPage="sub-packages/moveFile-package/pages/mine/member-code/index"></HomeActivityDialog>

        {/* 展示图片 */}
        {
          showImg && (
            <View className="preview-image" onClick={() => {this.handleShowLargeImage(false)}}>
              <Image src={membershipCode.barCode}></Image>
            </View>
          )
        }
      </View>
    );
  }
}

export default MemberCode;
