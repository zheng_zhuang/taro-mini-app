import { Block, View } from '@tarojs/components';
import { Component } from 'react';
import Taro from '@tarojs/taro';
import wxClass from '../../../../../wxat-common/utils/wxClass.js';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import cdnResConfig from '../../../../../wxat-common/constants/cdnResConfig.js';
import state from '../../../../../state/index.js';
import shareUtil from '../../../../../wxat-common/utils/share.js';

import AuthPuop from '../../../../../wxat-common/components/authorize-puop/index';
import HomeActivityDialog from '../../../../../wxat-common/components/home-activity-dialog/index';
import CouponCenter from '../../../../../wxat-common/components/tabbarLink/coupon-center/index';
import './index.scss';
const app = Taro.getApp();

class Coupon extends Component {

  state = {
    component: null
  }

  onLoad () {
    const component = this.selectComponent('#coupon-center');
    if (!!component) {
      this.setState({
        component
      });

      const directBack = protectedMailBox.read('directBack');
      component.onLoad({ directBack });
    }
    // 微信端禁止分享
    if (app.globalData.environment !== 'wxwork') {
      Taro.hideShareMenu();
    }
  }

  onPullDownRefresh () {
    if (this.state.component) {
      this.state.component.onPullDownRefresh();
    }
  }

  onReachBottom () {
    if (this.state.component) {
      this.state.component.onReachBottom();
    }
  }

  onRetryLoadMore () {
    if (this.state.component) {
      this.state.component.onRetryLoadMore();
    }
  }

  onShareAppMessage (res) {
    let title = '发来一个优惠券礼包';
    let url = 'sub-packages/moveFile-package/pages/mine/coupon/index';
    if (state.base.userInfo && state.base.userInfo.nickname) {
      title = state.base.userInfo.nickname + title;
    }
    return {
      title,
      path: shareUtil.buildShareUrlPublicArguments(url, shareUtil.ShareBZs.COUPON_CENTER),

      imageUrl: cdnResConfig.wechatShare.coupon,
    };
  }

  render () {
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpmc-Coupon" className="wk-mpmc-Coupon">
        <CouponCenter id="coupon-center"></CouponCenter>
        <HomeActivityDialog showPage="sub-packages/moveFile-package/pages/mine/coupon/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default Coupon;
