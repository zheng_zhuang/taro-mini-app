import { View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../../wxat-common/utils/money.wxs.js';
import api from "../../../../../../wxat-common/api";
import wxApi from '../../../../../../wxat-common/utils/wxApi';
import utilDate from '../../../../../../wxat-common/utils/date.js';
import cdnResConfig from '../../../../../../wxat-common/constants/cdnResConfig.js';
import couponEnum from '../../../../../../wxat-common/constants/couponEnum.js';
import pageLinkEnum from '../../../../../../wxat-common/constants/pageLinkEnum.js';
import QrCode from "../../../../../../wxat-common/components/qr-code";
import { connect } from 'react-redux';
import './index.scss';

const mapStateToProps = (state) => ({
  globalData: state.globalData,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Detail extends React.Component {

  state = {
    coupon: null,
    pageNo: 1,
    couponTicketId: null,
    detailImg: cdnResConfig.coupon.detailBg,
    couponEnum,
    pageLinkEnum,
    intervalId: null,
    __isPageShow: true
  }

  onLoad(options) {
    if (options.couponInfoId) {
      const couponTicketId = JSON.parse(options.couponInfoId);
      this.getDetail(couponTicketId);
      this.setState({
        couponTicketId: couponTicketId,
      })
    }
  }

  componentWillUnmount() {
    clearTimeout(this.state.intervalId)
  }

  intervalGetOrderDetail () {
    let timeId = setTimeout(() => {
      this.getDetail(this.state.couponTicketId, false);
    }, 2000)
    this.setState(
      {
        intervalId: timeId
      }
    )
  }

  onPullDownRefresh () {
    this.getDetail(this.state.couponTicketId);
  }

  getDetail (couponTicketId, loadingList = true) {
    const reqData = {
      couponTicketId: couponTicketId,
      appId: this.props.globalData.currentStore.appId,
    };

    wxApi
      .request({
        url: api.coupon.tickdetail,
        loading: loadingList,
        checkSession: true,
        data: reqData,
      })
      .then((res) => {
        const data = res.data || [];
        this.getTime(data);
        this.setState({
          coupon: data,
        });
      })
      .finally(() => {
        const pages = Taro.getCurrentPages();
        const currentPage = pages[pages.length - 1];
        const _url = currentPage.route; // 当前页面url
        if (_url === pageLinkEnum.common.couponDetail) {
          this.intervalGetOrderDetail();
        }
      });
  }

  getTime (data) {
    const endTime = new Date(data.endTime);
    const beginTime = new Date(data.beginTime);
    data.endTime = utilDate.format(endTime, 'yyyy-MM-dd');
    data.beginTime = utilDate.format(beginTime, 'yyyy-MM-dd');
    return data;
  }

  onChooseCoupon (e) {
    const coupon = e.target.dataset.item;
    const pages = Taro.getCurrentPages();
    const prevPage = pages[pages.length - 2]; // 上一个页面
    prevPage.setData({
      coupon: {
        cardItemName: coupon.cardItemName,
        userCardServiceId: coupon.userCardServiceId,
      },
    });

    Taro.navigateBack({
      delta: 1,
    });
  }

  
  render () {
    const { detailImg, coupon, couponEnum, __isPageShow } = this.state

    return (
      <>
        {
          !!coupon && (
            <View data-scoped="wk-pmcd-Detail" className="wk-pmcd-Detail mine-coupon-detail">
              <Image className="bg" src={detailImg}></Image>

              <View className="coupon-content">
              <View className="details">
                <View className="coupon-detail">
                  <View className="coupon-price">
                    {coupon.couponCategory === couponEnum.TYPE.discount.value ? (
                      <View className="discount">
                        <Text className="num">{filters.discountFilter(coupon.discountFee, true)}</Text>
                        <Text className="unit">折</Text>
                      </View>
                    ) : (
                      <View className="discount">
                        <Text className="num">{filters.moneyFilter(coupon.discountFee, true)}</Text>
                        <Text className="unit">元</Text>
                      </View>
                    )}
                  </View>
                  <View className="coupon-limit">
                    <View className="min-fee">{'满' + filters.moneyFilter(coupon.minimumFee, true) + '可用'}</View>
                    {/*  暂时不展示券的类型  */}
                    {/*  <View class="coupon-type" wx:if="{{item.couponCategory === 1}}">运费券</View>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <View class="coupon-type" wx:else>满减券</View>  */}
                  </View>
                  <View className="coupon-info">
                    <View className="coupon-name">{coupon.name}</View>
                    <View className="coupon-time">{'有效期：' + coupon.beginTime + ' 至 ' + coupon.endTime}</View>
                  </View>
                </View>
              </View>
              <View className="others">
                  {coupon != null && coupon.useScopeType !== 1 && coupon.status != 2 ? (
                    <View>
                      <QrCode isPageShow={__isPageShow} verificationNo={coupon.code} verificationType="2"></QrCode>
                    </View>
                  ) : (
                    coupon.status == 2 && (
                      <View className="veri-success">
                        <Text className="tip-success">优惠券已核销</Text>
                      </View>
                    )
                  )}
        
                  {/*  核销成功  */}
                  {/*  <View class='goods-hr'></View>  */}
                  {/*  <View class='card-desc'>
                      <View>{{coupon.suitItemType === 2 ? '可用商品类目' : '可用商品'}}
                        <text wx:if="{{coupon.suitItemType===0}}">：所有商品</text>
                      </View>
                      <View class='card-desc-container' wx:if="{{coupon.suitItemType===1}}">
                        <View class='card-desc-item' wx:for="{{coupon.itemBriefs}}" data-item='{{item}}' bindtap=''>
                          <View class='left'>
                            <View>●{{item.name}}</View>
                          </View>
                        </View>
                      </View>
                      <View class='card-desc-container' wx:if="{{coupon.suitItemType===2}}">
                        <View class='card-desc-item' wx:for="{{coupon.categoryBriefs}}" data-item='{{item}}' bindtap=''>
                          <View class='left'>
                            <View>●{{item.name}}</View>
                          </View>
                        </View>
                      </View>
                  </View>  */}
              </View>
              </View>

            </View>
          )
        }
        {
          false && (
            <View data-scoped="wk-pmcd-Detail" className="wk-pmcd-Detail mine-coupon-detail">
            <Image className="bg" src={detailImg}></Image>
            <View className="details">
              <View className="coupon-detail">
                <View className="coupon-price">
                  {coupon.couponCategory === couponEnum.TYPE.discount.value ? (
                    <View className="discount">
                      <Text className="num">{filters.discountFilter(coupon.discountFee, true)}</Text>
                      <Text className="unit">折</Text>
                    </View>
                  ) : (
                    <View className="discount">
                      <Text className="num">{filters.moneyFilter(coupon.discountFee, true)}</Text>
                      <Text className="unit">元</Text>
                    </View>
                  )}
                </View>
                <View className="coupon-limit">
                  <View className="min-fee">{'满' + filters.moneyFilter(coupon.minimumFee, true) + '可用'}</View>
                  {/*  暂时不展示券的类型  */}
                  {/*  <View class="coupon-type" wx:if="{{item.couponCategory === 1}}">运费券</View>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <View class="coupon-type" wx:else>满减券</View>  */}
                </View>
                <View className="coupon-info">
                  <View className="coupon-name">{coupon.name}</View>
                  <View className="coupon-time">{'有效期：' + coupon.beginTime + ' 至 ' + coupon.endTime}</View>
                </View>
              </View>
            </View>
            <View className="others">
              {coupon != null && coupon.useScopeType !== 1 && coupon.status != 2 ? (
                <View>
                  <QrCode isPageShow={__isPageShow} verificationNo={coupon.code} verificationType="2"></QrCode>
                </View>
              ) : (
                coupon.status == 2 && (
                  <View className="veri-success">
                    <Text className="tip-success">优惠券已核销</Text>
                  </View>
                )
              )}
    
              {/*  核销成功  */}
              {/*  <View class='goods-hr'></View>  */}
              {/*  <View class='card-desc'>
                  <View>{{coupon.suitItemType === 2 ? '可用商品类目' : '可用商品'}}
                    <text wx:if="{{coupon.suitItemType===0}}">：所有商品</text>
                  </View>
                  <View class='card-desc-container' wx:if="{{coupon.suitItemType===1}}">
                    <View class='card-desc-item' wx:for="{{coupon.itemBriefs}}" data-item='{{item}}' bindtap=''>
                      <View class='left'>
                        <View>●{{item.name}}</View>
                      </View>
                    </View>
                  </View>
                  <View class='card-desc-container' wx:if="{{coupon.suitItemType===2}}">
                    <View class='card-desc-item' wx:for="{{coupon.categoryBriefs}}" data-item='{{item}}' bindtap=''>
                      <View class='left'>
                        <View>●{{item.name}}</View>
                      </View>
                    </View>
                  </View>
              </View>  */}
            </View>
          </View>
          )
        }
      </>
    );
  }
}

export default Detail;
