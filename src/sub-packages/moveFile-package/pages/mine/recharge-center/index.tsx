import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, Image, Input, Icon } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
// pages/mine/recharge-center/index.js
import wxApi from '@/wxat-common/utils/wxApi';
import floatNumUtils from '@/wxat-common/utils/float-num';
import api from '@/wxat-common/api/index';
import constants from '@/wxat-common/constants/index';
import state from '@/state/index';
import pay from '@/wxat-common/utils/pay';
import template from '@/wxat-common/utils/template';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import authHelper from '@/wxat-common/utils/auth-helper';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';

// import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import CouponDialog from '@/wxat-common/components/coupon-dialog/index';
import './index.scss';
import store from '@/store';
import { updateGlobalDataAction } from '@/redux/global-data';
const PAY_CHANNEL = constants.order.payChannel;
class RechargeCenter extends React.Component {

  state = {
    activeCharge: null,
    rechargeList: [],
    balance: 0,
    selectIndex: -1, //0

    //现金充值
    amount: '',
    //是否是有效的现金额
    valid: false,
    tmpStyle: {},
    //是否确认协议
    dealClick: true,
    //充值协议
    dealData: null,
    dialogShow: false,
    coupon: [],
    payOrderNo: null,
    current: -1,
    isOtherPayShow: false,
    isForbidRecharge: 1,
    _payInfo: null,
    _payItem: null
  }

  onLoad () {
    this.getDeal();
    this.getTemplateStyle();
    this.getBalance();
    this.fetchCards();
    this.fetchForbidStatus();
  }

  /**
  * 生命周期函数--监听页面加载
  */
   componentDidShow() {
   this.setState({
     selectIndex: -1,
   })
   this.setState(
     {
      _payInfo: null,
      _payItem: null
     }
   )
  }

  fetchForbidStatus() {
    // 获取是否手动充值设置
    return wxApi
      .request({
        url: api.recharge.isForbid,
        quite: true,
        data: {},
      })
      .then((res) => {
        if (res.data) {
          // 0: 允许，1：不允许
          this.setState({
            isForbidRecharge: res.data.forbidRecharge,
          });
        }
      });
  }

  getBalance() {
    return wxApi
      .request({
        url: api.userInfo.balance,
        quite: true,
        data: {},
      })
      .then((res) => {
        this.setState({
          balance: res.data.balance,
        });
      })
  }

  onAmountChange = (e) => {
    const amount = e.detail.value;
    const reg = /^\d+(\.\d{0,2})?$/;

    this.setState({
      amount: e.detail.value,
      valid: reg.test(amount) && Number(amount) > 0 && Number(amount) < 9999.99,
    },()=>{
      if (!this.state.valid) {
        Taro.showToast({
          title: '请输入0~9999.99之间最多两位小数的数字',
          icon: 'none',
          duration: 2000,
        });
      }
    });


  }

  handleRecharge = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    if (!this.judgeDealClick()) return;
    // 开启了手动充值 提交时做校验判断
    if (!this.state.valid && !this.state.isForbidRecharge) {
      Taro.showToast({
        title: '请输入0~9999.99之间最多两位小数的数字',
        icon: 'none',
        duration: 2000,
      });
    }

    if (this.state.valid) {
      //充值成功的订阅消息
      let Ids = [subscribeEnum.CHARGE_SUCCESS.value, subscribeEnum.PAY_SUCCESS.value];

      //授权订阅消息
      subscribeMsg.sendMessage(Ids).then(() => {
        pay.submitPay(
          api.recharge.payOrder,
          {
            amount: floatNumUtils.floatMul(Number(this.state.amount), 100),
          },

          PAY_CHANNEL.WX_PAY,
          {
            wxPaySuccess: (res) => {
              Taro.navigateBack({
                delta: 1,
              });
            },
            wxPayFail: (error) => {},
          }
        );
      });
    }
  }

  onCardPay = (e) => {
    if (!authHelper.checkAuth()) {
      return;
    }
    if (!this.judgeDealClick()) return;
    this.setState({
      selectIndex: e.currentTarget ? e.currentTarget.dataset.index : 0,
      isOtherPayShow: false,
    });

    const newItem = e.currentTarget ? e.currentTarget.dataset.item : {};
    const orderInfo = {
      itemDTOList: [
        {
          itemCount: 1,
          itemNo: newItem.itemNo,
          type: newItem.type,
        },
      ],

      formId: !e.detail || e.detail.formId === 'the formId is a mock one' ? null : e.detail.formId,
    };

    if (this.state._payItem && this.state._payItem.id !== newItem.id) {
      this.setState(
        {
         _payInfo: null,
         _payItem: null
        }
      )
    }
    const _this = this;
    //充值成功的订阅消息
    let Ids = [subscribeEnum.CHARGE_SUCCESS.value, subscribeEnum.PAY_SUCCESS.value];
    //授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      pay.handleBuyNow(orderInfo, PAY_CHANNEL.WX_PAY, {
        payInfo: this.state._payInfo,
        success(res) {
          _this.setState(
            {
             _payInfo: res,
             _payItem: e.currentTarget ? e.currentTarget.dataset.item : {}
            }
          )

          _this.setState({
            payOrderNo: res.data.orderNo,
          });
        },
        wxPaySuccess() {
          _this.autoFetchCoupons(_this.state.payOrderNo);
          _this.setState(
            {
             _payInfo: null,
             _payItem: null
            }
          )
        },
      });
    })
  }

  onOtherCardPay = (e) => {
    this.setState({
      selectIndex: e.currentTarget ? e.currentTarget.dataset.index : 0,
      isOtherPayShow: true,
    });
  }

  fetchCards() {
    wxApi
      .request({
        url: api.card.list,
        loading: true,
        data: {
          storeId: state.base.currentStore.id,
          isShelf: 1,
          status: 1,
          pageNo: 100,
          type: constants.card.type.charge,
        },
      })
      .then((res) => {
        this.setState({
          rechargeList: res.data || []
        });
        if (!res.data) {
          this.setState(
            {
              isOtherPayShow: true
            }
          )
        }
      });
  }

  onFocus = () => {
    this.setState({
      selectIndex: -1,
    });
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      Taro.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //转到充值协议页面
  seeDeal () {
    protectedMailBox.send('sub-packages/mine-package/pages/deal/index', 'content', this.state.dealData.content);

    wxApi.$navigateTo({
      url: `/sub-packages/mine-package/pages/deal/index`,
      data: {
        title: '充值协议',
      },
    });
  }

  //点击确认协议
  verifyDeal = () => {
    this.setState({
      dealClick: !this.state.dealClick,
    });
  }

  //判断是否确认充值协议
  judgeDealClick() {
    if (!this.state.dealClick) {
      //点击状态
      Taro.showToast({
        title: '请确认协议信息',
        icon: 'none',
        duration: 1500,
      });

      return false;
    }
    return true;
  }

  //查询充值协议
  getDeal() {
    wxApi
      .request({
        url: api.deal,
      })
      .then((res) => {
        this.setState({
          dealData: res.data,
        });
        store.dispatch(updateGlobalDataAction({
          dealFile: res.data.content
        }))
      });
  }

  //自动领取优惠券
  autoFetchCoupons(orderNo) {
    let reqData = {
      eventType: 5,
      orderNo: orderNo,
    };

    wxApi
      .request({
        url: api.coupon.auto_collect,
        loading: true,
        checkSession: true,
        data: reqData,
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            coupon: res.data || [],
            dialogShow: true,
          });
        }
      })
  }

  // 获取可用的优惠券列表
  fetchSendCoupons(orderNo) {
    wxApi
      .request({
        url: api.coupon.get_coupons,
        loading: false,
        quite: true,
        data: { orderNo: orderNo, eventType: 5 },
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            coupon: res.data || [],
            dialogShow: true,
          });
        }
      })
  }

  // 优惠券弹窗关闭后跳转页面
  handleCouponDialog = () => {
    Taro.navigateBack({
      delta: 1,
    });
  }

  render() {
    const {
      tmpStyle,
      balance,
      selectIndex,
      isForbidRecharge,
      rechargeList,
      isOtherPayShow,
      dealData,
      dealClick,
      dialogShow,
      coupon,
      payOrderNo,
    } = this.state;
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mpmr-RechargeCenter"
        className="wk-mpmr-RechargeCenter"
      >
        <View className="recharge-center">
          <View className="header" style={_safe_style_('background:' + tmpStyle.btnColor)}>
            <View className="view">账户余额(元)</View>
            <View className="view">{filters.moneyFilter(balance, true)}</View>
          </View>
          {/*  <View class='title'>
                                                                                                                                                                                                          <View>充值金额</View>
                                                                                                                                                                                                        </View>  */}
          
            <View className="list-container">
              {!!rechargeList.length && rechargeList.map((item, index) => {
                  return (
                    <View
                      className={'item ' + (selectIndex == index ? 'active' : '')}
                      onClick={_fixme_with_dataset_(this.onCardPay, { index: index, item: item })}
                      key={item.id}
                      style={_safe_style_(
                        selectIndex == index
                          ? 'border-color:' +
                              tmpStyle.btnColor +
                              ';color:' +
                              tmpStyle.btnColor +
                              ';background:' +
                              tmpStyle.bgColor
                          : ''
                      )}
                    >
                      <View className="amount">{filters.moneyFilter(item.salePrice, true) + '元'}</View>
                      {
                        !!item.special && (
                          <View className="new-icon">
                            <Image
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/new-user.png"
                              className="new-img"
                            ></Image>
                          </View>
                        )
                      }
                      {item.giftAmount > 0 && (
                        <View className="giftAmount">{'送' + filters.moneyFilter(item.giftAmount, true) + '元'}</View>
                      )}
                    </View>
                  );
                })}
                {!isForbidRecharge && !!rechargeList.length && (
                <View
                  className={'item ' + (selectIndex == -2 ? 'active' : '')}
                  onClick={_fixme_with_dataset_(this.onOtherCardPay, { index: -2 })}
                  style={_safe_style_(
                    selectIndex == -2
                      ? 'border-color:' +
                          tmpStyle.btnColor +
                          ';color:' +
                          tmpStyle.btnColor +
                          ';background:' +
                          tmpStyle.bgColor
                      : ''
                  )}
                >
                  <View style={_safe_style_('font-size:28rpx;')}>其他金额</View>
                </View>
              )}
            </View>

          {(!rechargeList.length || !isForbidRecharge) && (
            <View
              className="footer"
              style={_safe_style_(!!rechargeList.length ? 'margin-top: 40rpx' : '')}
            >
              {!isForbidRecharge && isOtherPayShow && (
                <View>
                  <View>手动输入金额(￥)</View>
                  <Input
                    type="digit"
                    onInput={this.onAmountChange}
                    onFocus={this.onFocus}
                    placeholder="手动充值不参与余额赠送优惠活动"
                  ></Input>
                  {/*  <View class="invalid-tip" wx:if="{{!valid}}">请输入0~9999.99之间最多两位小数的数字</View>  */}
                </View>
              )}

              {!!dealData && (
                <View className="deal-option">
                  <View className="deal-btn" onClick={this.verifyDeal.bind(this)}>
                    <Icon type="success" size="15" color={!dealClick ? '#f0f1f5' : tmpStyle.btnColor}></Icon>
                    同意并阅读相关协议
                  </View>
                  <View className="deal-label" onClick={this.seeDeal.bind(this)}>
                    《充值协议》
                  </View>
                </View>
              )}

              {!isForbidRecharge && isOtherPayShow && (
                <View
                  className="recharge-btn"
                  onClick={this.handleRecharge}
                  style={_safe_style_('background:' + tmpStyle.btnColor)}
                >
                  立即充值
                </View>
              )}
            </View>
          )}

          {(!rechargeList.length) && !!isForbidRecharge && (
            <View className="non-tip">暂无可用的充值卡</View>
          )}
        </View>
        {
          !!coupon.length && (
            <CouponDialog
              visible={dialogShow}
              coupon={coupon}
              payOrderNo={payOrderNo}
              eventType="5"
              onHandleCouponDialog={this.handleCouponDialog}
            ></CouponDialog>
          )
        }
        <HomeActivityDialog showPage="sub-packages/moveFile-package/pages/mine/recharge-center/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default RechargeCenter;
