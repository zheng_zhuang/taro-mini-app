import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, ScrollView } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import constants from '../../../../wxat-common/constants/index.js';
import cdnResConfig from '../../../../wxat-common/constants/cdnResConfig.js';
import state from '../../../../state/index.js';
import shareUtil from '../../../../wxat-common/utils/share.js';
import AuthPuop from '../../../../wxat-common/components/authorize-puop/index';
import AuthUser from '../../../../wxat-common/components/auth-user/index';
import CouponModule from '../../../../wxat-common/components/couponModule/index';
import LoadMore from '../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../wxat-common/components/error/error';
import Empty from '../../../../wxat-common/components/empty/empty';
import template from '@/wxat-common/utils/template.js';
import { connect } from 'react-redux';
import './index.scss';
const loadMoreStatus = constants.order.loadMoreStatus;
const COUPON_STATUS = constants.coupon.labelList;

const mapStateToProps = (state) => ({
  globalData: state.globalData,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class CouponList extends React.Component {

  state = {
    tmpStyle: {},
    couponList: [],
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.LOADING,
    statusType: COUPON_STATUS,
    status: 1,
    tabClass: ['', '', ''], // tab的class集合
  }

  onLoad() {
    this.getTemplateStyle()
  }

  // 用户信息准备-获取优惠券列表
  userInfoReady = () => {
    this.listCouponList()
  }

  handlerScroll = () => {
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatus.LOADING,
          pageNo: this.state.pageNo + 1,
        },
        () => {
          this.listCouponList()
        }
      )
    }
  }

  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatus.LOADING,
      },
      () => {
        this.listCouponList()
      }
    )
  }

  listCouponList = () => {
    this.setState(
      { 
        hasMore: false,
        error: false,
        loadMoreStatus: loadMoreStatus.LOADING
      },
      () => {
        wxApi
        .request({
          url: api.coupon.ticketlist,
          loading: false,
          checkSession: true,
          data: {
            pageNo: this.state.pageNo,
            pageSize: 10,
            appId: this.props.globalData.appId,
            status: this.state.status,
          },
        })
        .then((res) => {
          let data = res.data || [];
          let couponList = this.state.couponList.concat(data)
          let hasMore = couponList.length < res.totalCount;
          if (couponList.length === res.totalCount && !!res.data) {
            this.setState({
              loadMoreStatus: loadMoreStatus.BASELINE
            })
          }
          if (res.data === null) {
            this.setState({
              loadMoreStatus: loadMoreStatus.HIDE
            })
          }
          this.setState({
            couponList,
            hasMore,
          });
  
          // data.length && this.selectComponent('#coupon').getCouponList();
        })
        .catch(() => {
          this.setState({
            loadMoreStatus: loadMoreStatus.HIDE,
            error: true,
          })
        })
        .finally(() => {
          Taro.stopPullDownRefresh();
        });
      }
    )
  }

  handleRemove = (couponNo) => {
    let reqData = {
      couponTicketId: couponNo,
    };

    Taro.showModal({
      title: '确定要删除吗？',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.coupon.del,
              loading: true,
              checkSession: true,
              data: reqData,
            })
            .then(() => {
              this.setState(
                {
                  couponList: [],
                  hasMore: true,
                  loadMoreStatus: loadMoreStatus.LOADING,
                  pageNo: 1,
                },
                () => {
                  this.listCouponList();
                }
              )
              Taro.showToast({
                title: '删除成功'
              })
            })
            .catch(() => {
              Taro.showToast({
                title: '删除失败'
              })
            })
        }
      }
    })
  }

  onChooseCoupon = (e) => {
    const coupon = e.target.dataset.item;
    const pages = Taro.getCurrentPages();
    const prevPage = pages[pages.length - 2]; //上一个页面
    prevPage.setData({
      coupon: {
        cardItemName: coupon.cardItemName,
        userCardServiceId: coupon.userCardServiceId,
      },
    });

    Taro.navigateBack({
      delta: 1,
    })
  }

  handleListOrders = (index) => {

    this.setState(
      {
        status: index + 1,
        pageNo: 1,
        hasMore: true,
        couponList: []
      },
      () => {
        this.listCouponList()
      }
    )
  }

  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle() || {};
    let btnColor = null;
    if (this.props.activityDetail && this.props.activityDetail.buttonColor !== 'default') {
      btnColor = this.props.activityDetail.buttonColor;
      this.setState({
        tmpStyle: Object.assign({}, templateStyle, { btnColor: btnColor }),
      });
    } else {
      this.setState({
        tmpStyle: templateStyle,
      });
    }
  }

  onShareAppMessage() {
    // 企业微信内分享
    if (this.props.globalData.environment === 'wxwork') {
      const url = '/sub-packages/moveFile-package/pages/coupon-list/index';
      return {
        title: `${state.base.wxUserInfo.nickName}给你发来一个优惠券大礼包`,
        path: shareUtil.buildShareUrlPublicArguments(url, shareUtil.ShareBZs.COUPON_LIST),

        imageUrl: cdnResConfig.share.coupon,
      };
    }
  }

  getIntegralCoupon = (couponNo) => {
    let reqData = {
      couponId: couponNo,
    };

    Taro.showModal({
      title: `是否消耗${this.props.globalData.scoreName}兑换？`,
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.coupon.integral,
              loading: true,
              checkSession: true,
              data: reqData,
            })
            .then(() => {
              Taro.showToast({
                title: '领取成功',
              })
              this.setState(
                {
                  pageNo: 1
                },
                () => {
                  this.listCouponList()
                }
              )
            })
        }
      },
    });
  }

  getFreeCoupon = (couponNo) => {
    let reqData = {
      couponId: couponNo,
    };

    wxApi
      .request({
        url: api.coupon.collect,
        loading: true,
        checkSession: true,
        data: reqData,
      })
      .then(() => {
        this.setState(
          {
            pageNo: 1
          },
          () => {
            this.listCouponList()
          }
        )
        Taro.showToast({
          title: '领取成功',
        });
      })
  }

  handleGetCoupon = (e) => {
    if (state.base.registered) {
      let couponNo = e.detail.couponNo;
      let receiveMethod = e.detail.receiveMethod;
      if (receiveMethod) {
        this.getIntegralCoupon(couponNo);
      } else {
        this.getFreeCoupon(couponNo);
      }
    } else {
      wxApi.$navigateTo({
        url: '/sub-packages/moveFile-package/pages/wx-auth/index',
      });
    }
  }

  render() {
    const { status, tmpStyle, statusType, tabClass, couponList, error, loadMoreStatus } = this.state
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-smpc-CouponList"
        className="wk-smpc-CouponList"
        
      >
        <ScrollView scrollY={true} scrollWithAnimation scrollAnchoring style={{'height': '100%', 'overflow-anchor': 'auto', '-webkit-overflow-scrolling': 'touch'}} onScrollToLower={this.handlerScroll}>
        <AuthPuop></AuthPuop>
        <AuthUser onReady={this.userInfoReady}></AuthUser>
          <View className="couponList">
            <View className="status-box">
              {statusType &&
                statusType.map((item, index) => {
                  return (
                    <View
                      onClick={() => {this.handleListOrders(index)}}
                      className={'status-label ' + (index == status - 1 ? 'active' : '')}
                      key={'status-label-' + index}
                      style={_safe_style_(
                        index == status - 1 ? 'color:' + tmpStyle.btnColor + ';border-color:' + tmpStyle.btnColor : ''
                      )}
                    >
                      {item}
                      <View className={tabClass[index]}></View>
                    </View>
                  );
                })}
            </View>
            {
              !error && !!couponList.length && <View className="notes">小提示：按住券左滑可以删除券哦~</View>
            }
            {
             !error && !loadMoreStatus && !couponList.length && <Empty message="暂无优惠劵"></Empty>
            }
            {error && <Error></Error>}
            <View>
              {!!couponList.length && (
                <CouponModule
                  list={couponList}
                  id="coupon"
                  isMine={true}
                  onRemove={this.handleRemove}
                  onCouponEvent={this.handleGetCoupon}
                ></CouponModule>
              )}

              <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
            </View>
          </View>
        </ScrollView>

      </View>
    );
  }
}

export default CouponList;
