import { $getRouter, _safe_style_ } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text,Button,Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import constants from '@/wxat-common/constants/index';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Empty from '@/wxat-common/components/empty/empty';
import WxOpenWeapp from '@/wxat-common/components/wx-open-launch-weapp'
import './index.scss';
import hoc from '@/hoc';
import AnimatDialog from "@/wxat-common/components/animat-dialog";
import template from '@/wxat-common/utils/template.js';
import { connect } from 'react-redux';

const loadMoreStatus = constants.order.loadMoreStatus;

const rules = {
  mobile: [
    {
      required: true,
      message: '请输入手机号码',
    },

    {
      reg: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
      message: '手机号码无效',
    },
  ],
};
interface StateProps {
  userInfo:any,
  currentStore:any
}

interface LuckyRecord {
  props: StateProps;
}
const mapStateToProps = (state) => {
  return { userInfo: state.base.userInfo.id ? state.base.userInfo : state.base.loginInfo, currentStore:state.base.currentStore};
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class LuckyRecord extends React.Component {
  $router = $getRouter();
  state = {
    record: [] as any[],
    pageNo: 0,
    pageSize: 10,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    activityId: 0,
    form: {
      mobile: null,
    },
    formValidated:{
      isValied:false,
      msg:''
    },
    _ruleDialog:null,
    selectLucky: {
      luckyTurningId:null,
      id:null,
      luckyItemNo:null,
      luckyGiftName:null
    },
    tmpStyle: null,
    shareDialogRef:React.createRef<any>()
  };

  componentDidMount() {
    this.getTemplateStyle()
    this.setState({
      activityId:this.$router.params.id
    })
    this.getLuckyRecordList();
    if (this.props.userInfo) {
      const form = {
        mobile: this.props.userInfo.phone
      }
      this.setState({
        form:form
      });
    }
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  getLuckyRecordList = async () => {
    this.setState({ loadMoreStatus: loadMoreStatus.LOADING });
    const apiUrl = api.luckyDial.getUserRecords;

    const { pageNo, pageSize } = this.state;
    try {
      const res = await wxApi.request({
        url: apiUrl,
        loading: true,
        data: {
          luckyTurningId: this.$router.params.id,
          pageNo: pageNo + 1,
          pageSize,
        },
      });

      const xData = res.data || [];
   
      this.setState({
        record: this.state.record.concat(xData),
        pageNo: res.pageNo,
        loadMoreStatus: loadMoreStatus.HIDE,
        hasMore: xData.length === 10,
      });
    } catch (err) {
      this.setState({
        loadMoreStatus: loadMoreStatus.ERROR,
      });
    }
  };

  onRetryLoadMore = () => {
    this.onReachBottom();
  };

  onReachBottom = () => {
    if (this.state.loadMoreStatus !== loadMoreStatus.LOADING && this.state.hasMore) {
      this.getLuckyRecordList();
    }
  };

  handleToUse=()=> {
    Taro.navigateToMiniProgram({
      appId: 'wx2560348cdcdd57fe',
      // appId: 'wxed6c99dc0cd41de7',
      path: 'pages/menber/menber1',
    });
  }

  getRuleDialog = () =>{
    return this.state.shareDialogRef.current;
  }

  handleViewRule = (item) =>{
    this.setState({
      selectLucky: item,
    });

    this.getRuleDialog().show({
      scale: 1,
    });
  }

  validate= ()=> {
    for (const key in rules) {
      const value = this.state.form[key];
      const rule = rules[key];

      let isValied = true;
      let msg = '';
      for (const r of rule) {
        if (r.required && !value) {
          isValied = false;
          msg = r.message;
          break;
        } else if (r.reg && !r.reg.test(value)) {
          isValied = false;
          msg = r.message;
          break;
        }
      }
      if (!isValied) {
        this.setState({
          formValidated: {
            isValied: false,
            msg,
          },
        },()=>{
          this.errorTip(this.state.formValidated.msg);
        });
        return null
      }
    }
    this.setState({
      formValidated: {
        isValied: true,
        msg: '验证通过',
      },
    });
    this.handleSelectLucky()
    return true;
  }

  errorTip=(msg)=> {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }

  handleImmediatelyLucky=()=> {
    this.validate();
  }

  handleSelectLucky=()=>{
    wxApi
      .request({
        url: `${api.virtualGoods.priceUnifiedVirtualOrder}?recordId=${this.state.selectLucky.id}&luckyTurningId=${this.state.selectLucky.luckyTurningId}`,
        method: 'POST',
        data: {
          itemNo: this.state.selectLucky.luckyItemNo,
          virtualItemOrderVO: {
            phone: this.state.form.mobile,
          },

          itemDTOList: [
            {
              itemNo: this.state.selectLucky.luckyItemNo,
              itemCount: 1,
            },
          ],

          storeId: this.props.currentStore.id,
          storeName: this.props.currentStore.name,
        },

        loading: true,
      })
      .then((res) => {
        if (res.success) {
          this.setState({
            pageNo: 0,
            record: [],
          });

          this.errorTip('兑奖成功');
          this.getLuckyRecordList();
          this.getRuleDialog().hide();
        }
      })
      .catch((res) => {
        this.errorTip('兑奖失败');
      });
  }

  handleClose=() =>{
    this.getRuleDialog().hide();
  }

  bindInputMobile=(e)=> {
    const form = {
      mobile:e.target.value
    }
    this.setState({
      form:form
    });
  }

  render() {
    const { record, hasMore, loadMoreStatus,form,selectLucky,shareDialogRef, tmpStyle } = this.state;
    const transPx = document.documentElement.clientWidth / (Taro.config.designWidth || 750);
    const btnStyles = `.navBtn {
      padding: 0px ${37 * transPx}px;
      opacity: 1;
      border-radius: ${45 * transPx}px;
      background: #14b99a;
      color: #fff;
      line-height: ${50 * transPx}px;
      margin-top: 0;
      border-width: 0;
      outline: 0;
      font-size: ${24 * transPx}px;
    }`;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-pll-LuckyRecord'
        className='wk-pll-LuckyRecord'
      >
        {record.length > 0 && (
          <View className='lucky-record'>
            <View className='list'>
              {record.map((item) => {
                return (
                  <View className='list-item' key={item.id}>
                    <Image className='list-item__img' mode="aspectFit" src={item.luckyGiftPhoto}></Image>
                    <View className='list-item__content'>
                      <Text className='list-item__content-name'>{item.luckyGiftName}</Text>
                      <View className="list-item_comtent_item">
                      <Text className='list-item__content-time'>{'中奖时间：' + item.luckyTime}</Text>
                      {item.luckyItemNo != 0 && (
                            <Block>
                              {item.itemFillInStatus == 0 ? (
                                <Button
                                  className="close-btn"
                                  onClick={()=>this.handleViewRule(item) }
                                >
                                  立即兑奖
                                </Button>
                              ) : (
                                Taro.getEnv() === Taro.ENV_TYPE.WEB ? (
                                  // 正式环境 携旅院线
                                  // username="gh_24e933b6a7b2"
                                  <WxOpenWeapp
                                    username="gh_73c309d26824"
                                    path="pages/menber/menber1"
                                    styles={btnStyles}
                                    slotTel={
                                    <Button className="navBtn" style={_safe_style_('background: ' + tmpStyle.btnColor )}>
                                      去使用
                                    </Button>}>
                                  </WxOpenWeapp>
                                ) : (
                                  <Button className="close-btn" onClick={this.handleToUse}>
                                    去使用
                                  </Button>
                                )
                              )}
                            </Block>
                          )}
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        )}

        {!!(record.length === 0 && !hasMore) && <Empty message='暂无中奖记录'></Empty>}
        <AnimatDialog ref={shareDialogRef} animClass="lucky-dialog">
          <View className="lucky-container">
            <View className="title">兑奖</View>
            <View className="lucky-name">
              奖品:<Text>{selectLucky.luckyGiftName}</Text>
            </View>
            <View className="prize-title-phone">奖品手机号</View>
            <Input
              className="input-phone"
              onInput={(e)=>this.bindInputMobile(e)}
              value={form.mobile}
              placeholder="请输入"
            ></Input>
            <View className="btn-container">
              <Button className="btn" onClick={this.handleClose}>
                取消
              </Button>
              <Button className="btn" onClick={this.handleImmediatelyLucky}>
                立即兑奖
              </Button>
            </View>
          </View>
        </AnimatDialog>
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default LuckyRecord;
