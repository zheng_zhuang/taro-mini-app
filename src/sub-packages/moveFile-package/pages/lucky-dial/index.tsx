import { $getRouter,_safe_style_ } from 'wk-taro-platform';
import React from 'react'; /* eslint-disable react/sort-comp */
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import luckyDialWxs from './index.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import shareUtil from '@/wxat-common/utils/share.js';
import PrizeDialog from "./components/prize-dialog";
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import CustomerHeader from '@/wxat-common/components/customer-header/index'
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';

interface StateProps {
  sessionId: string;
  globalData:any;
  scoreName:string
}

interface LuckyDial {
  props: StateProps;
}

const mapStateToProps = (state) => {
  return { sessionId: state.base.sessionId,
    globalData:state.globalData,
    scoreName:state.globalData.scoreName || '积分'
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class LuckyDial extends React.Component {
  $router = $getRouter();
  state = {
    // 顺时针方向，每个格子对应的实际渲染下标，没有4是因为4是抽奖按钮
    boxArr: [0, 1, 2, 5, 8, 7, 6, 3, ''],
    // 礼物列表
    prize: [] as any[],
    // 选中的格子在礼物列表中的下标
    chooseItem: 0,
    // 当前高亮格子的下标boxArr的下标
    index: 0,
    // 是否开始游戏
    playing: false,
    // 中奖格子的下标
    luckyNum: 0,
    // 规则
    rule: [],
    // 转盘参数
    options: {
      lap: 5, // 圈数
      speed: 50, // 每转一格的时间(ms)
      ease: true, // 先慢后快，结束时变慢
      ratio: 4, // 变速时，速度的倍数
    },
    // 展示中奖的弹框
    showDialog: false,
    // 光标移动计时器
    timeout: 0,
    // 中奖的具体数据
    luckyData: {},
    // 参与类型(0:Free, 1:Points)
    participationType: 0,
    // 用户剩余可抽奖次数
    remainTimes: 0,
    /**
     * 未中奖提示语
     */
    unluckyTips: '很遗憾，未中奖，下次加油~',
    // 每次消耗积分数
    perPoints: 0,
    // 用户积分数
    userScore: 0,
    // 活动状态，已结束为4，需要拦截
    activityStatus: -1,
    // 活动名称
    activityName: '',
    // 还能不能通过分享获得抽奖次数
    getMoreByShare: false,
    // 数据是否加载完成
    dataLoad: false,
    /**
     * 是否可以继续游戏，结果弹窗在 转盘结束后 100ms 延迟显示
     * 在 100ms 内不可再次进行开始转盘操作
     * 只与弹窗状态有关 */
    continueGame: true,

    navOpacity: 0,
    navBgColor: null,
    selectLuckyId: null,
    activityId:null,
  };

  asyncSetState(state) {
    return new Promise<void>((resolve) => {
      this.setState(state, resolve);
    });
  }

  async componentDidMount() {
    if (this.props.sessionId) {
      this.initRenderData();
    }
    this.setState({
      navBgColor:this.props.globalData.tabbars.list[0].navBackgroundColor,
    })
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.sessionId != this.props.sessionId) {
      this.initRenderData();
    }

    if (!!nextProps.userInfo && !!this.props.userInfo) { 
    // fixme 再获取数据之后跳转页面
      this.handleAuth()
    }
  }

  handleAuth = () => {
    const _authCheck = authHelper.checkAuth()
    if (!_authCheck) return;

  }

  componentDidShow = () => {

    if (!this._reload) {
      this._reload = true;
      return;
    }

    this.getUserActivityMessage();
  };

  private _reload: boolean;

  initRenderData = async () => {
    if (this.state.dataLoad) return;

    await this.asyncSetState({ dataLoad: true });
    this._reload = true;
    this.getActivityDetails(true);
  };

  getActivityDetails = async (loading: boolean) => {
    const apiUrl = api.luckyDial.getActivityDetails;
    const luckyTurningId = this.$router.params.id;
    const res = await wxApi.request({
      url: apiUrl,
      loading,
      data: { luckyTurningId },
    });

    const prize = [] as any[];
    const giftsList = res.data.giftsList;
    for (let i = 0; i < 8; i++) {
      const index = giftsList.findIndex((item) => {
        // 这里要减个1是因为后端礼物位置是从1开始计算的
        const giftIndex = item.giftIndex - 1;
        // 因为没有4，4是抽奖按钮，所以大于3需要减1
        const boxIndex = this.state.boxArr[giftIndex] as number;
        const index = this.state.boxArr[giftIndex] > 3 ? boxIndex - 1 : boxIndex;

        return index === i;
      });
      if (index === -1) {
        prize.push({
          giftName: res.data.unluckyName,
          giftPhotoUrl: res.data.unluckyPhotoUrl,
        });
      } else {
        prize.push(giftsList[index]);
      }
    }
    await this.asyncSetState({
      rule: res.data.ruleDescription,
      prize: prize,
      activityStatus: res.data.activityStatus,
      activityName: res.data.activityName,
      participationType: res.data.participationType,
      unluckyTips: res.data.unluckyTips,
      perPoints: res.data.perPoints,
    });

    this.getUserActivityMessage();
  };

  getUserActivityMessage = async () => {
    const url = api.luckyDial.getUserActivityMessage;
    const luckyTurningId = this.$router.params.id;

    const { data } = await wxApi.request({
      url,
      data: { luckyTurningId },
    });

    await this.asyncSetState({
      remainTimes: data.remainTimes,
      userScore: data.userScore || 0,
      getMoreByShare: data.getMoreByShare,
    });
  };

  goRecord = () => {
    const { id } = this.$router.params;
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/lucky-dial/lucky-record/index',
      data: { id },
    });
  };

  getLuckyId = async () => {
    const { remainTimes, timeout } = this.state;
    const { id } = this.$router.params;

    try {
      const { data } = await wxApi.request({
        url: `${api.luckyDial.startGame}?luckyTurningId=${id}`,
        method: 'POST',
      });

      const luckyNum = data.giftIndex === 8 ? 0 : data.giftIndex;

      await this.asyncSetState({ remainTimes: remainTimes - 1 });
      await this.asyncSetState({ luckyNum: luckyNum,selectLuckyId: data.id, });
    } catch (err) {
      clearInterval(timeout);
      await this.asyncSetState({ playing: false });
      throw err;
    }
  };

  dialogHide = async () => {
    await this.asyncSetState({ showDialog: false, continueGame: true });
  };

  clickShare = async () => {
    const { getMoreByShare } = this.state;
    const activityId = this.$router.params.id;
    const apiUrl = api.luckyDial.addLuckTimes;
    if (!getMoreByShare) return false

    await wxApi.request({
      url: apiUrl,
      data: { luckyTurningId: activityId, byWay: 'Share' },
      quite: true,
    });

    this.getUserActivityMessage();
  };

  onShareAppMessage = () => {
    const { id } = this.$router.params;
    const activityName = this.state.activityName;
    const path = shareUtil.buildShareUrlPublicArguments({
      // url: 'sub-packages/marketing-package/pages/lucky-dial/index?id=' + id,
      url: 'sub-packages/moveFile-package/pages/lucky-dial/index?id=' + id,
      bz: shareUtil.ShareBZs.LUCKY_DIAL,
      bzName: `幸运大转盘${activityName ? '-' + activityName : ''}`,
      bzId: id,
      sceneName: activityName,
    });
    return {
      path,
    };
  };

  startGame = async (e: ITouchEvent) => {
    if (!authHelper.checkAuth()) return false;
    const index = +e.currentTarget.dataset.index;
    // index为4是点击开始按钮，游戏如果已经开始或者点击的不是开始按钮就不执行。
    // 弹窗弹起的一瞬间，开始按钮不可点击
    if (index !== 4 || this.state.playing || !this.state.continueGame) return false;
    if (this.state.remainTimes < 1) {
      const content = this.state.getMoreByShare ? '次数用光啦，可通过分享的方式增加抽奖次数' : '次数用光啦~';
      wxApi.showModal({
        content: content,
        showCancel: false,
        confirmText: '好的',
        confirmColor: '#666666',
      });

      return false;
    }
    if (this.state.perPoints > this.state.userScore) {
      wxApi.showModal({
        content: '积分不足',
        showCancel: false,
        confirmText: '好的',
        confirmColor: '#666666',
      });

      return false;
    }
    if (this.state.activityStatus === 4) {
      wxApi.showToast({
        title: '活动已结束',
        icon: 'none',
      });

      return false;
    }
    await this.asyncSetState({ playing: true });
    // 发请求获取中奖id
    this.getLuckyId().then(async () => {
      let finish = false;
        // 转的圈数
        let lap = 0;
        // 当前转过的格子数,计算圈数使用
        let round = 0;
        // 速度
        let speed = this.state.options.speed;
        let slow = true;
        let slwostop = true;
      if (this.state.options.ease) {
        speed = this.state.options.speed * this.state.options.ratio;
      }
      const game = async () => {
        // 变换高亮的格子
        await this.running();
        // 转过的格子加1
        round++;
        const dialItemNum = this.state.boxArr.length;
        // 当round等于格子数的时候，圈数加一，dialItemNum - 1 是因为格子占了个位置
        if (round === dialItemNum - 1) {
          lap++;
          round = 0;
        }

        if (this.state.options.ease && round === Math.floor(dialItemNum / 2) && slow) {
          clearInterval(this.state.timeout);
          speed = this.state.options.speed;
          slow = false;
          await this.asyncSetState({
            timeout: setInterval(game, speed),
          });
        }
        if (lap > this.state.options.lap) {
          if (this.state.options.ease && slwostop) {
            clearInterval(this.state.timeout);
            speed = this.state.options.speed * this.state.options.ratio;
            slwostop = false;
            await this.asyncSetState({ timeout: setInterval(game, speed) });
          }

          if (this.state.index === this.state.luckyNum && finish) {
            const luckyData = this.state.prize[
              this.state.chooseItem > 3 ? this.state.chooseItem - 1 : this.state.chooseItem
            ];

            const isLucky = !!luckyData.id;
            clearInterval(this.state.timeout);
            await this.asyncSetState({
              playing: false,
            });

            // 延迟100ms展示获奖信息
            isLucky && (await this.asyncSetState({ continueGame: false }));
            setTimeout(async () => {
              if (isLucky) {
                await this.asyncSetState({
                  luckyData,
                  showDialog: true,
                });
              } else {
                wxApi.showToast({
                  title: this.state.unluckyTips,
                  icon: 'none',
                });
              }
            }, 100);
            this.getUserActivityMessage();
          }
          finish = true;
        }
      };
      await this.asyncSetState({
        timeout: setInterval(game, speed),
      });
    });
  };

  running = async () => {
    const index = this.state.index >= this.state.boxArr.length - 2 ? 0 : this.state.index + 1;
    const chooseItem = this.state.boxArr[this.state.index];
    await this.asyncSetState({ index, chooseItem });
  };

  onPageScroll(e) {
    this.setState({
      navOpacity: e.scrollTop / 50,
    });
  }

  render() {
    const {
      navOpacity,
      navBgColor,
      // scoreName,

      userScore,
      chooseItem,
      boxArr,
      prize,
      participationType,
      perPoints,
      remainTimes,
      rule,
      luckyData,
      showDialog,
      selectLuckyId,
      activityId,
    } = this.state;
    const {scoreName} = this.props
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-mpl-LuckyDial' className='wk-mpl-LuckyDial'>
        {/* @TODO */}
        <AuthPuop></AuthPuop>
       {
         process.env.TARO_ENV === 'weapp' ? <CustomerHeader isTitle={true} navOpacity={navOpacity} title="幸运大转盘" bgColor={navBgColor}></CustomerHeader> : null
       }
        <View className='dial-box'>
          {/* <Image src={cdnResConfig.lucky_dial.background} className='bg-float'></Image> */}
          <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/word.png" className='dial-logo'></Image>
          <Image
            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/gift-bag-new.png"
            className="dial-gift-bag"
          ></Image>
          <ButtonWithOpenType openType='share' className='share-btn' onClick={this.clickShare}>
            <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/share.svg" className='share-img'></Image>
          </ButtonWithOpenType>
          <View className="dial">
            <View className="integral">
              {'我的' + scoreName + '：'}
              <Text style={_safe_style_('color:#fe7500;')}>{userScore}</Text>
            </View>
            <View className="rule-record" onClick={this.goRecord}>
              我的中奖记录
            </View>
            <Image
              src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/big-turntable.svg"
              className="bg-dial-float"
            ></Image>
            {boxArr?.map((item, index) => {
                return (
                  <View
                    className={
                      'dial-item ' + luckyDialWxs.getDialItemClass(index) + ' ' + (index === chooseItem ? 'choose' : '')
                    }
                    key={index}
                    onClick={_fixme_with_dataset_(this.startGame, { index: index })}
                  >
                    {index !== 4 && (
                      <Image
                        className="prize-img"
                        src={
                          prize[index > 4 ? index - 1 : index] ? prize[index > 4 ? index - 1 : index].giftPhotoUrl : ''
                        }
                      ></Image>
                    )}

                    {index !== 4 && (
                      <View className="prize-text">
                        {prize[index > 4 ? index - 1 : index] ? prize[index > 4 ? index - 1 : index].giftName : ''}
                      </View>
                    )}

                    {index === 4 && (
                      <View className="start-btn">{participationType === 0 ? '免费抽奖' : scoreName + '抽奖'}</View>
                    )}

                    {/*  <auth-user wx:if="{{index === 4}}" scene="custom_btn" custom_style="{{btnStyle}}" custom_btn_desc="{{participationType === 0 ? '免费抽奖' : '积分抽奖'}}">
                         <View class="start-btn">{{participationType === 0 ? '免费抽奖' : '积分抽奖'}}</View>
                       </auth-user>  */}
                  </View>
                );
              })}
            <View className="lucky-tips">
              <View className="user-points">{'每次抽奖将消耗' + perPoints + scoreName}</View>
              <Text className="user-time">{'剩余次数：' + remainTimes + '次'}</Text>
            </View>
          </View>
          <View className="rule">
            <View className="rule-item">
              <View className="rule-header">
                <Image
                  className="rule-image"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/Slash.svg"
                ></Image>
                <View className="rule-title">规则说明</View>
                <Image
                  className="rule-image"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/Slash.svg"
                ></Image>
              </View>
              <View className="rule-text">{rule}</View>
            </View>
          </View>
          {showDialog && (
            <PrizeDialog selectLuckyId={selectLuckyId} onHide={this.dialogHide} luckyData={luckyData}></PrizeDialog>
          )}

          <HomeActivityDialog
            showPage={'sub-packages/moveFile-package/pages/lucky-dial/index?id=' + activityId}
          ></HomeActivityDialog>
        </View>
      </View>
    );
  }
}
LuckyDial.enableShareAppMessage = true
export default LuckyDial;
