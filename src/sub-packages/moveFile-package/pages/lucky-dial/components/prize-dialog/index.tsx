import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image ,Block,Text,Input} from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import { connect } from 'react-redux';
// const commonImg = cdnResConfig.common;
import api from '@/wxat-common/api/index';
import hoc from '@/hoc';
// giftType 为0是积分，跳转积分中心，giftType为1是优惠券跳转优惠券中心。
const giftTypeForUrl = ['/sub-packages/mine-package/pages/integral/index', '/wxat-common/pages/coupon-list/index'];
const rules = {
  mobile: [
    {
      required: true,
      message: '请输入手机号码',
    },

    {
      reg: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
      message: '手机号码无效',
    },
  ],
};
interface PrizeDialogProps {
  luckyData: any;
  onHide: Function;
  selectLuckyId:string | null,
  userInfo?:any,
  currentStore?:any
}

interface PrizeDialogState{
  form:{
    mobile:string | undefined
  },
  cdnResConfig:any,
  formValidated:{
    isValied:boolean,
    msg:null | string
  }
}
interface PrizeDialog {
  props: PrizeDialogProps;
  state:PrizeDialogState
}

const mapStateToProps = (state) => {
  return { userInfo: state.base.userInfo.id ? state.base.userInfo : state.base.loginInfo, currentStore:state.base.currentStore};
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class PrizeDialog extends React.Component {
  state={
    cdnResConfig: cdnResConfig.lucky_dial,
    form: {
      mobile: null,
    },
    formValidated:{
      isValied:false,
      msg:''
    },
  }
  componentDidMount(){
    if (this.props.userInfo) {
      const form = {
        mobile: this.props.userInfo.phone
      }
      this.setState({
        form:form
      });
    }
  }
  hide = (ev: ITouchEvent) => {
    const { onHide, luckyData } = this.props;
    const tap = ev.target.dataset.tap;
    if (tap == 1) {
      onHide();
    } else if (tap == 2) {
      onHide();
      const url = giftTypeForUrl[luckyData.giftType] || '';
      if (url) {
        wxApi.$navigateTo({ url });
      }
    } else if(tap == 3){
      this.validate()
    }
  };
  validate = ()=> {
    for (const key in rules) {
      const value = this.state.form[key];
      const rule = rules[key];
      let isValied = true;
      let msg = '';
      for (const r of rule) {
        if (r.required && !value) {
          isValied = false;
          msg = r.message;
          break;
        } else if (r.reg && !r.reg.test(value)) {
          isValied = false;
          msg = r.message;
          break;
        }
      }
      if (!isValied) {
        this.setState({
          formValidated: {
            isValied: false,
            msg,
          },
        },()=>{
          this.errorTip(this.state.formValidated.msg);
        });
        return false;
      }
    }
    this.setState({
      formValidated: {
        isValied: true,
        msg: '验证通过',
      },
    });
    this.handleSelectLucky()
    return true;
  }
  errorTip(msg:string | null) {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }
  bindInputMobile(e) {
    const form = {
      mobile:e.target.value
    }
    this.setState({
      form:form
    });
  }
  handleSelectLucky(){
    wxApi
      .request({
        url: `${api.virtualGoods.priceUnifiedVirtualOrder}?recordId=${this.props.selectLuckyId}&luckyTurningId=${this.props.luckyData.luckyTurningId}`,
        method: 'POST',
        data: {
          itemNo: this.props.luckyData.virtualItemNo,
          virtualItemOrderVO: {
            phone: this.state.form.mobile,
          },

          itemDTOList: [
            {
              itemNo: this.props.luckyData.virtualItemNo,
              itemCount: 1,
            },
          ],

          recordId: this.props.luckyData.id,
          storeId: this.props.currentStore.id,
          storeName: this.props.currentStore.name,
        },

        loading: true,
      })
      .then((res) => {
        if (res.success) {
          this.errorTip('兑奖成功,去中奖记录中去使用');
          this.props.onHide();
        }
      })
      .catch((res) => {
        this.errorTip('兑奖失败');
      });
  }
  render() {
    const { luckyData } = this.props;
    const {cdnResConfig, form} = this.state
    return (
      <View
        data-scoped='wk-lcp-PrizeDialog'
        className='wk-lcp-PrizeDialog prize-dialog'
        // onClick={_fixme_with_dataset_(this.hide, { tap: '1' })}
      >
        <View className='prize-dialog-container'>
          <Image
            onClick={_fixme_with_dataset_(this.hide, { tap: '1' })}
            // src={commonImg.close}
            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/close.png"
            className='close-icon'
          ></Image>
          <View className='header'>
            恭喜您！
            <Image src={cdnResConfig.dialog_header} className='header-bg'></Image>
          </View>
          <View className='prize-message'>
            <View className='prize-name'>
              获得<View style={_safe_style_('color: #F52636;')}>{luckyData.giftName}</View>
            </View>
            <Image src={luckyData.giftPhotoUrl} className='prize-img'></Image>
            <View className='tip'>运气真的这么好，再来一次？</View>
            {luckyData.giftType === 2 && (
              <View className="prize-phone">
                <Text className="text">兑奖手机号</Text>
                <Input onInput={(e) => this.bindInputMobile(e)} placeholder="请输入" value={form.mobile}></Input>
              </View>
            )}
            <View className='btn-wrapper'>
            {luckyData.giftType !== 2 ? (
                <Block>
                  <View className="use-btn" data-tap="2" onClick={_fixme_with_dataset_(this.hide, { tap: '2' })}>
                    去查看
                  </View>
                  <View className="close-btn" data-tap="1" onClick={_fixme_with_dataset_(this.hide, { tap: '1' })}>
                    继续抽奖
                  </View>
                </Block>
              ) : (
                <Block>
                  <View className="use-btn" data-tap="1" onClick={_fixme_with_dataset_(this.hide, { tap: '1' })}>
                    继续抽奖
                  </View>
                  <View className="close-btn" data-tap="3" onClick={_fixme_with_dataset_(this.hide, { tap: '3' })}>
                    立即兑奖
                  </View>
                </Block>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default PrizeDialog;
