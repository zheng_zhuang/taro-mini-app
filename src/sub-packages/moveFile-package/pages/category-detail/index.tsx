import { $getRouter } from 'wk-taro-platform'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import React, { ComponentClass, Component } from 'react';
import { Block, View } from '@tarojs/components';
import { connect } from 'react-redux';
import Taro, { Config } from '@tarojs/taro';
import constants from '@/wxat-common/constants/index.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import util from '@/wxat-common/utils/util.js';
import imageUtils from '@/wxat-common/utils/image.js';

import HoverCart from '@/wxat-common/components/cart/hover-cart/index';
import GoodsListFilter from '@/wxat-common/components/goods-list-filter/index';
import GoodsItem from '@/wxat-common/components/industry-decorate-style/goods-item';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import Search from '@/wxat-common/components/search/index';
import './index.scss';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import report from '@/sdks/buried/report/index';
import shareUtil from '@/wxat-common/utils/share.js';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
const loadMoreStatusEnum = constants.order.loadMoreStatus;

type PageStateProps = {};

type PageDispatchProps = {};

type PageOwnProps = {};

type PageState = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface Index {
  props: IProps;
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class Index extends Component {
  $router = $getRouter();
  state = {
    goodsTypeEnum,
    //商品类型
    type: 1,
    categoryId: null,
    error: false,
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    goodsList: [],
    reportSource: '',
    title: '',
    view: 'vertical',
  };

  hasMore = true;
  queryParams = {
    pageNo: 1,
    pageSize: 10,
    sortField: null,
    sortType: null, // true：降序， false: 升序
    inStock: null, // 0: 没货，1: 有货
  };

  componentDidMount() {
    this.getTemplateStyle();
    const options = this.$router.params;
    const type = options.type ? parseInt(options.type) : 1;
    const categoryId = options.id;
    const reportSource = options.source || '';
    const title = options.name || '分类详情';
    wxApi.setNavigationBarTitle({
      title,
    });

    this.setState(
      {
        type,
        categoryId,
        reportSource,
        title,
      },

      () => {
        this.api();
      }
    );
  }

  onShareAppMessage() {
    report.share(true);
    const { type, categoryId, reportSource, title } = this.state;
    const url = `/sub-packages/marketing-package/pages/category-detail/index?type=${type}&id=${categoryId}&source=${reportSource}&name=${title}`;
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.CATEGORY_DETAIL,
      bzName: title,
    });

    console.log('sharePath => ', path);
    return {
      title,
      path,
    };
  }

  onFilterChange = (detail) => {
    const { sortField, sortType } = detail;
    this.queryParams.sortType = sortType;
    this.queryParams.sortField = sortField;
    this.fresh();
  };

  fresh = () => {
    this.queryParams.pageNo = 1;
    this.hasMore = true;
    this.api();
  };

  /**
   * 查询分类中的商品
   */
  api = () => {
    const params: any = {
      status: 1,
      type: this.state.type,
      categoryId: this.state.categoryId,
      pageNo: this.queryParams.pageNo,
      pageSize: this.queryParams.pageSize,
    };

    if (this.queryParams.sortField !== null) {
      params.sortField = this.queryParams.sortField;
    }

    if (this.queryParams.sortType != null) {
      params.sortType = this.queryParams.sortType;
    }

    // 判断请求的商品类型为产品时，则删除请求参数中的type，改用typeList包含产品类型及组合商品类型
    if (params.type === goodsTypeEnum.PRODUCT.value) {
      params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];
    }

    this.setState({
      loadMoreStatus: loadMoreStatusEnum.LOADING,
    });

    wxApi
      .request({
        url: api.classify.itemSkuList,
        //loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true) {
          // 处理带sku快速添加
          let goodsList,
            resData = res.data || [];
          resData.forEach((item) => {
            const thumbnail = item.thumbnail || (!!item.wxItem ? item.wxItem.thumbnail : '');
            item.thumbnailMin = imageUtils.thumbnailMin(thumbnail);
            item.thumbnailMid = imageUtils.thumbnailMid(thumbnail);
            item.thumbnailOneRowTwo = imageUtils.thumbnailOneRowTwo(thumbnail);
          });
          if (this.isLoadMoreRequest()) {
            goodsList = this.state.goodsList.concat(resData);
          } else {
            goodsList = resData;
          }

          this.hasMore = goodsList.length !== res.totalCount;
          this.queryParams.pageNo += 1;
          this.setState({
            goodsList,
          });
        }
        this.setState({
          loadMoreStatus: loadMoreStatusEnum.HIDE,
        });
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatusEnum.ERROR,
          });
        }
      });
  };

  isLoadMoreRequest = () => {
    return this.queryParams.pageNo > 1;
  };

  onReachBottom = () => {
    if (this.state.loadMoreStatus === loadMoreStatusEnum.HIDE && this.hasMore) {
      this.api();
    }
  };

  onRetryLoadMore = () => {
    this.api();
  };

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  };

  handleViewChange = ({ view }) => {
    this.setState({ view });
  };

  render() {
    const { type, goodsTypeEnum, goodsList, reportSource, loadMoreStatus, error, view } = this.state;

    let $content: JSX.Element | null = null;
    if (view === 'oneRowTwo') {
      const leftList = goodsList.filter((_, ind) => ind % 2 === 0);
      const rightList = goodsList.filter((_, ind) => ind % 2 !== 0);

      $content = (
        <View className='wrapper'>
          <View className='left'>
            {leftList.map((item: any, index) => {
              return (
                <GoodsItem
                  key={item && item.wxItem ? item.wxItem.itemNo : index}
                  goodsItem={item}
                  display={view}
                  reportSource={reportSource}
                  showDivider={index !== goodsList.length - 1}
                ></GoodsItem>
              );
            })}
          </View>
          <View className='right'>
            {rightList.map((item: any, index) => {
              return (
                <GoodsItem
                  key={item && item.wxItem ? item.wxItem.itemNo : index}
                  goodsItem={item}
                  display={view}
                  reportSource={reportSource}
                  showDivider={index !== goodsList.length - 1}
                ></GoodsItem>
              );
            })}
          </View>
        </View>
      );
    } else {
      $content = (
        <View>
          {goodsList.map((item: any, index) => {
            return (
              <GoodsItem
                key={item && item.wxItem ? item.wxItem.itemNo : index}
                goodsItem={item}
                display={view}
                reportSource={reportSource}
                showDivider={index !== goodsList.length - 1}
              ></GoodsItem>
            );
          })}
        </View>
      );
    }

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-mpc-CategoryDetail'
        className='wk-mpc-CategoryDetail'
      >
        <View className='cg__container'>
          {type === goodsTypeEnum.PRODUCT.value && (
            <View className='cg__search'>
              <Search></Search>
            </View>
          )}

          {type === goodsTypeEnum.PRODUCT.value && (
            <View className='cg__goods-filter'>
              <GoodsListFilter
                onFilterChange={this.onFilterChange}
                showViewIcon
                view={view}
                onViewChange={this.handleViewChange}
              ></GoodsListFilter>
            </View>
          )}

          {!!goodsList && (
            <View className={'cg__content ' + (type === goodsTypeEnum.SERVER.value ? 'cg__content-no-margin' : '')}>
              {$content}
            </View>
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore} />
          {!!(loadMoreStatus !== loadMoreStatusEnum.LOADING && !!goodsList && goodsList.length == 0) && (
            <Empty message='暂无产品' />
          )}

          {!!error && <Error />}
        </View>
        <HoverCart />
        <AuthPuop />
        <HomeActivityDialog showPage="sub-packages/moveFile-package/pages/category-detail/index"></HomeActivityDialog>
      </View>
    );
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default Index as ComponentClass<PageOwnProps, PageState>;
