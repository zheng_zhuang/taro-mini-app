export default {
  navigationBarBackgroundColor: '#ffffff',
  navigationBarTextStyle: 'black',
  backgroundColor: '#eeeeee',
  backgroundTextStyle: 'light',
};
