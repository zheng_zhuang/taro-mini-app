import { _safe_style_ } from 'wk-taro-platform';
import { Block, View, Swiper, SwiperItem, Text, Image, Canvas, ScrollView } from '@tarojs/components';
import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import constants from "../../../../../wxat-common/constants";
import api from "../../../../../wxat-common/api";
import wxApi from '../../../../../wxat-common/utils/wxApi';
import state from "../../../../../state";
import timer from '../../../../../wxat-common/utils/timer.js';
import report from '@/sdks/buried/report/index.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import template from '../../../../../wxat-common/utils/template.js';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import industryEnum from '../../../../../wxat-common/constants/industryEnum.js';
import deliveryWay from '../../../../../wxat-common/constants/delivery-way.js';
import goodsTypeEnum from '../../../../../wxat-common/constants/goodsTypeEnum.js';
import login from "../../../../../wxat-common/x-login";
import pageLinkEnum from '../../../../../wxat-common/constants/pageLinkEnum.js';
import shareUtil from '../../../../../wxat-common/utils/share.js';
import checkOptions from '../../../../../wxat-common/utils/check-options.js';
import store from '@/store/index'
import AuthPuop from "../../../../../wxat-common/components/authorize-puop";
import DrugsInfo from "../../../../../wxat-common/components/goods-detail/drugs-info";
import TaskError from "../../../../../wxat-common/components/task-error";
import FrontMoneyItem from "../../../../../wxat-common/components/front-money-item";
import ShareDialog from "../../../../../wxat-common/components/share-dialog";
import CombinationItemDetail from "../../../../../wxat-common/components/combination-item-detail";
import DetailParser from "../../../../../wxat-common/components/detail-parser";
import CommentModule from "../../../../../wxat-common/components/comment-module";
import ListGroup from "../components/list-group";
import RuleDesc from "../components/rule-desc";
import AnimatDialog from "../../../../../wxat-common/components/animat-dialog";
import ChooseAttributeDialog from "../../../../../wxat-common/components/choose-attribute-dialog";
import BuyNow from "../../../../../wxat-common/components/buy-now";
import './index.scss';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';

class Detail extends Component {
  state = {
    dataLoad: false,
    loading: false,
    industryEnum, // 行业类型常量
    industry: null, // 行业类型

    itemNo: null, // 商品单号
    activityId: null, // 活动id

    goodsImages: [], // 商品图片
    goodsDetail: null, // 商品详情

    ptActivityItemDTO: {}, // 拼团活动详情
    groupActivityStatus: constants.groupActivity.status, // 拼团活动状态常量

    groups: [], // 正在凑团的拼团信息top 5
    goodType: constants.goods.type.group, // 商品类型
    sessionId: '',

    limitAmountTitle: null, // 限购类型标题，用户选择商品属性的时候，展示限购信息
    isBuyOfGroup: true, // 是否为拼团购买订单，用于下单时区分是拼团购买订单还是原价购买

    tmpStyle: {}, // 主题模板配置
    uniqueShareInfoForQrCode: null,
    // 拼团规则
    groupRule: {},

    _thredId: null,
    _shareDialog: null,
    _ruleDialog: null,
    ruleDialog: null,
    groupeDialog: null,
    chooseAttributeDialogNode: null,
    shareDialogNode: React.createRef(),
  }

  // ------------原computed属性

  // 是否显示提货时间
  isShowVerificationTimeBox = () => {
    // 判断当前品牌是否支持自提，以及是否有核销时间
    const ptActivityItemDTO = this.state.ptActivityItemDTO;
    if (
      state.base.appInfo &&
      state.base.appInfo.expressType != deliveryWay.EXPRESS.value &&
      ptActivityItemDTO &&
      ptActivityItemDTO.verificationStartTime &&
      ptActivityItemDTO.verificationEndTime
    ) {
      return true;
    }
    return false;
  }

  // 获取整个正在凑团的前两位显示在页面当中
  groupTop2 = () => {
    return this.state.groups.slice(0, 2)
  }

  // 是否不可编辑
  isDisable = () => {
    // fixme 保留库存不足的逻辑
    if (this.state.goodsDetail) {
      const wxItem = this.state.goodsDetail.wxItem;
      if (wxItem.isShelf === 0) {
        return true;
      } else if (
        wxItem.type !== goodsTypeEnum.SERVER.value &&
        wxItem.type !== goodsTypeEnum.SLEEVE_SYSTEM.value &&
        wxItem.type !== goodsTypeEnum.TICKETS.value &&
        wxItem.itemStock <= 0 &&
        wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value
      ) {
        return true;
      } else if (this.state.ptActivityItemDTO.currentStatus === this.state.groupActivityStatus.NOT_SHELF) {
        return true;
      } else if (this.state.ptActivityItemDTO.currentStatus === this.state.groupActivityStatus.NOT_START) {
        return true;
      }
    }
    return false;
  }

  // 底部操作按钮文字
  operationText = () => {
    // fixme 保留库存不足的逻辑
    if (this.state.goodsDetail) {
      const wxItem = this.state.goodsDetail.wxItem;
      if (wxItem.isShelf === 0) {
        return '商品已下架';
      } else if (
        wxItem.type !== goodsTypeEnum.SERVER.value &&
        wxItem.type !== goodsTypeEnum.SLEEVE_SYSTEM.value &&
        wxItem.type !== goodsTypeEnum.TICKETS.value &&
        wxItem.itemStock <= 0 &&
        wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value
      ) {
        return '库存不足';
      } else if (this.state.ptActivityItemDTO.currentStatus === this.state.groupActivityStatus.NOT_SHELF) {
        return '活动已结束';
      } else if (this.state.ptActivityItemDTO.currentStatus === this.state.groupActivityStatus.NOT_START) {
        return '活动未开始';
      }
      return '立即购买';
    }
  }

  // 底部按钮默认文字
  defaultText = () => {
    if (this.state.goodsDetail && this.state.goodsDetail.wxItem.type === goodsTypeEnum.SERVER.value) {
      return '立即预约';
    }
    return '发起拼团';
  }

  buyDirectText = () => {
    if (this.state.goodsDetail && this.state.goodsDetail.wxItem.frontMoneyItem) {
      return '原价预定';
    }
    return '原价购买';
  }

  // 是否渐变
  isGradualChange = () => {
    if (this.state.goodsDetail && this.state.goodsDetail.wxItem.type !== goodsTypeEnum.SERVER.value) {
      return true;
    }
    return false;
  }

  // 是否可以直接购买
  isCanBuyDirect = () => {
    if (this.state.goodsDetail) {
      const wxItem = this.state.goodsDetail.wxItem;
      if (wxItem.isShelf === 0) {
        return false;
      } else if (
        wxItem.type !== goodsTypeEnum.SERVER.value &&
        wxItem.type !== goodsTypeEnum.SLEEVE_SYSTEM.value &&
        wxItem.type !== goodsTypeEnum.TICKETS.value &&
        wxItem.itemStock <= 0 &&
        wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value
      ) {
        return false;
      }
      return true;
    }
  }

  // ^^^^^^^^^^^^^^^原computed属性

  /**
   * 生命周期函数--监听页面加载
   */
  // activityId 活动id, itemNo商品编号
  onLoad () {
    const { activityId, itemNo } = Taro.getCurrentInstance().router.params
    this.setState(
      {
        activityId,
        itemNo,
        _thredId: timer.addQueue(() => {
          // 定时器格式化时间
          this.updateActivityTime(this.state.ptActivityItemDTO || {}); // 格式化拼团活动时间
          this.updateJoinTime(this.state.groups || []); // 格式化参团时间
        })
      }
    )

    login.login().then(() => {
      checkOptions.checkOnLoadOptions({activityId, itemNo}).then(res => {
        this.init(res);
      })
    })

  }



  init (options) {
    const sessionId = report.getBrowseItemSessionId(options.itemNo)
    this.setState(
      {
        sessionId,
        industry: store.getState().globalData,
        uniqueShareInfoForQrCode: {
          activityId: options.activityId,
          page: 'sub-packages/moveFile-package/pages/group/detail/index',
        }
      },
      () => {
        if (!!this.state.sessionId && options.activityId) {
          this.initRenderData(); // 初始化渲染数据
        }
      }
    )

  }

  /**
   * 生命周期函数--监听页面卸载
   */
   onUnload () {
    // 判断定时器是否存在，删除定时器
    if (this.state._thredId) {
      timer.deleteQueue(this.state._thredId);
      this.setState(
        {
          _thredId: null
        }
      )
    }
  }

  /**
   * 初始化渲染数据
   */
   initRenderData() {
    this.getTemplateStyle(); // 获取主题模板
    if (this.state.dataLoad) {
      return;
    }
    this.setState({
      dataLoad: true,
    });
    if (this.state.activityId) {
      this.getGoodsDetail(); // 获取商品详情
      this.getGroups();
    }
  }

  onRuleLoad = (e) => {
    this.setState({
      groupRule: e.detail || {},
    });
  }

  /**
   * 获取模板配置
   */
   getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

    /**
   * 获取商品详情
   */
     getGoodsDetail = () => {
      Taro.showLoading(
        {
          title: '加载中...',
          mask: true
        }
      )
      this.setState(
        {
          loading: false
        }
      )
      const { activityId, itemNo } = this.state;
      if (!activityId && !itemNo) return;

      const params = {};
      if (itemNo) {
        params.itemNo = itemNo;
      }

      // 判断是否为拼团购买订单，是则添加activityId为请求商品及活动详情接口的请求参数
      if (this.state.isBuyOfGroup) {
        params.activityId = activityId;
        params.type = goodsTypeEnum.GROUP.value;
      }
      wxApi
        .request({
          url: api.goods.detail,
          data: params,
        })
        .then(
          (res) => {
            const data = res.data;
            if (data) {
              let title = '商品详情';
              if (data.wxItem && data.wxItem.name) {
                title = data.wxItem.name;
              }

              // 判断是否为拼团购买订单，是则重新渲染活动详情
              if (this.state.isBuyOfGroup) {
                // 拼团详情内容
                let ptActivityItemDTO = null;
                if (data.ptActivityItemDTO) {
                  ptActivityItemDTO = data.ptActivityItemDTO;
                }

                this.updateActivityTime(ptActivityItemDTO || {}); // 格式化拼团活动时间
                this.setState({
                  ptActivityItemDTO: ptActivityItemDTO || {},
                });
              }

              Taro.setNavigationBarTitle({
                title,
              });
              this.setState(
                {
                  loading: true,
                  itemNo: data.wxItem ? data.wxItem.itemNo : '',
                  goodsDetail: data,
                  goodsImages: data.materialUrls || [],
                }
              )

              report.browseItem({
                itemNo: this.state.itemNo,
                name: data.wxItem.name,
                salePrice: data.wxItem.salePrice,
                thumbnail: this.state.goodsImages[0],
                source: reportConstants.SOURCE_TYPE.group.key,
                sessionId: this.state.sessionId,
              })

              Taro.hideLoading()
            }
          }
        ).catch(() => {
          Taro.hideLoading()
        })
    }

    /**
   * 获取正在凑团的拼团列表top 5
   */
  getGroups() {
    wxApi
      .request({
        url: api.activity.groupList,
        data: {
          pageSize: 5,
          activityId: this.state.activityId,
          status: -1, // 状态是拼团中
        },
      })
      .then((res) => {
        this.updateJoinTime(res.data || []); // 格式化参团时间
      })
  }

   /**
   * 判断活动状态
   */
    judgeActivityStatus(startTime, endTime) {
      const now = new Date().getTime();

      if (now < startTime) {
        return this.state.groupActivityStatus.NOT_START; // 未开始
      } else if (startTime < now < endTime) {
        return this.state.groupActivityStatus.SHELF; // 进行中
      } else {
        return this.state.groupActivityStatus.NOT_SHELF; // 已结束
      }
    }

    /**
   * 格式化拼团活动时间
   * @param {*} ptActivityItemDTO
   */
     updateActivityTime(ptActivityItemDTO) {
      if (ptActivityItemDTO && Object.keys(ptActivityItemDTO).length > 0) {
        this.updateTime(ptActivityItemDTO, 'Object'); // 格式化倒计时

        this.setState({
          ptActivityItemDTO: this.state.ptActivityItemDTO,
        });
      }
    }

      /**
   * 格式化参团时间
   * @param {*} groups
   */
  updateJoinTime(groups) {
    if (groups && groups.length) {
      groups.forEach((g, index) => {
        // 当活动结束后，把所有凑团列表的参团时间置为0
        if (this.state.ptActivityItemDTO.currentStatus === this.state.groupActivityStatus.NOT_SHELF) {
          g.remainTime = 0;
        } else if (g.remainTime < 1) {
          // 判断当前团的参数时间小于1是，则在所有凑团列表中删除该团
          groups.splice(index, 1);
        }
        this.updateTime(g, 'String'); // 格式化倒计时
      });

      this.setState({
        groups: groups,
      });
    }
  }

    /**
   * 格式化倒计时
   * @param {*} obj
   */
     updateTime(obj, type = 'String') {
      const now = new Date().getTime();

      if (!obj.remainTime || obj.remainTime <= 0) {
        if (type === 'String') {
          obj.displayTime = '00:00:00';
        } else {
          obj.displayTime = {
            day: '00',
            hour: '00',
            minute: '00',
            second: '00',
          };
        }

        obj.currentStatus = this.state.groupActivityStatus.NOT_SHELF; // 将拼团状态设为已结束
        return;
      }

      obj.currentStatus = this.judgeActivityStatus(obj.activityStartTime, obj.activityEndTime);

      // 根据拼团的活动状态判断时间
      if (obj.activityEndTime) {
        if (obj.currentStatus === this.state.groupActivityStatus.NOT_START) {
          obj.remainTime = parseInt((obj.activityStartTime - now) / 1000);
        } else {
          obj.remainTime = parseInt((obj.activityEndTime - now) / 1000);
        }
      }

      obj.remainTime -= 1;

      const day = parseInt(obj.remainTime / (24 * 3600));

      const hourS = parseInt(obj.remainTime % (24 * 3600));
      const hourInt = parseInt(hourS / 3600);
      const hour = hourInt < 10 ? '0' + hourInt : hourInt;

      const minuteS = hourS % 3600;
      const minuteInt = parseInt(minuteS / 60);
      const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

      const secondS = parseInt(minuteS % 60);
      const second = secondS < 10 ? '0' + secondS : secondS;

      if (type === 'String') {
        obj.displayTime = day ? `${day}天${hour}:${minute}:${second}` : `${hour}:${minute}:${second}`;
      } else {
        obj.displayTime = {
          day: day < 10 ? '0' + day : day,
          hour: hour + '',
          minute: minute + '',
          second: second + '',
        };
      }
      // obj.currentStatus = this.data.groupActivityStatus.SHELF; // 将拼团状态设为进行中
    }

      /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */
  onShareAppMessage() {
    report.share(true);
    const { itemNo, activityId, goodsDetail, goodsImages } = this.state;
    const imageUrl = goodsImages.length ? goodsImages[0] : '';
    if (goodsDetail) {
      const url = `/sub-packages/moveFile-package/pages/group/detail/index?activityId=${activityId}&itemNo=${itemNo}`;
      const activityName = goodsDetail.ptActivityItemDTO.activityName ? goodsDetail.ptActivityItemDTO.activityName : '';
      const path = shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.GROUP_DETAIL,
        bzName: `拼团活动${activityName ? '-' + activityName : ''}`,
      });
      return {
        title: activityName,
        imageUrl,
        path,
      };
    } else {
      return {};
    }
  }

    /**
   * 打开邀请好友对话弹框
   */
     handleSelectChanel = () => {
       this.state.shareDialogNode.current.show();
    }

    // 保存图片
    onSavePosterImage = (e) => {
      this.state.shareDialogNode.current.savePosterImage(this);
    }

    refRule = (node) => {
      this.setState(
        {
          ruleDialog: node
        }
      )
    }

    groupRule = (node) => {
      this.setState(
        {
          groupeDialog: node
        }
      )
    }

    /**
   * 打开活动规则
   */
  handleViewRule = () => {
    this.state.ruleDialog.show({ scale: 1 })
  }


  /**
   * 关闭活动规则
   */
   handleCloseRule = () => {
    this.state.ruleDialog.hide();
  }

    /**
   * 跳转参与拼团
   * @param {*} e
   */
     onJoinGroup(e) {
      wxApi.$navigateTo({
        url: '/sub-packages/moveFile-package/pages/group/join/index',
        data: {
          groupNo: e.currentTarget.dataset.no,
        },
      });
    }

    /**
     * 查看更多拼团
     */
    handleViewMoreGroup = () => {
      this.state.groupeDialog.show({
        scale: 1,
      });
    }


  /**
   * 关闭拼团列表对话弹框
   */
  handleCloseGroup = () => {
    this.state.groupeDialog.hide();
  }

    /**
   * 直接购买
   */
     handleBuyDirect = () => {
      if (!this.state.goodsDetail || !this.state.goodsDetail.wxItem) {
        Taro.showToast({
          title: `系统繁忙，请稍后重试`,
          icon: 'none',
        });

        return;
      }

      this.setState({
        limitAmountTitle: null, // 设置限购类型标题为弄，以便选择商品属性时，不展示展示拼团限购信息
        isBuyOfGroup: false, // 是原价购买订单
      });

      const goodsDetail = this.state.goodsDetail;
      const goodsInfo = {
        itemCount: 1,
        wxItem: goodsDetail.wxItem,
      };

      if (goodsDetail.wxItem.type === goodsTypeEnum.ROOMS.value) {
        // 判断是否为客房直接购买订单 客房类型为18
        protectedMailBox.send('sub-packages/tourism-package/pages/rooms-info/index', 'goodsInfo', goodsInfo);

        wxApi.$navigateTo({
          url: '/sub-packages/tourism-package/pages/rooms-info/index',
        });
      } else if (goodsDetail.wxItem.type === goodsTypeEnum.TICKETS.value) {
        // 判断是否为票务直接购买订单 票务类型为19
        protectedMailBox.send('sub-packages/tourism-package/pages/tickets-info/index', 'goodsInfo', goodsInfo);

        wxApi.$navigateTo({
          url: '/sub-packages/tourism-package/pages/tickets-info/index',
        });
      } else {
        this.state.chooseAttributeDialogNode.showAttributDialog();
      }
    }

    chooseAttributeDialogRef = (node) => {
      this.setState(
        {
          chooseAttributeDialogNode: node
        }
      )
    }

      /**
   * 底部购买按钮
   */
  handleBuyNow = () => {
    if (!this.state.goodsDetail || !this.state.goodsDetail.wxItem) {
      Taro.showToast({
        title: `系统繁忙，请稍后重试`,
        icon: 'none',
      });
      return;
    }

    this.setState({
      limitAmountTitle: '拼团', // 设置限购类型标题，以便展示拼团限购信息
      isBuyOfGroup: true, // 是拼团购买订单
    });
    report.clickBuy(this.state.activityId, 2);
    this.state.chooseAttributeDialogNode.showAttributDialog();
  }

    /**
   * 选定购买的商品属性
   * @param {*} info
   */
     handlerChooseAttribute = (info) => {
      const { activityId, goodsDetail } = this.state;
      const detail = info; // 选择的商品详情

      if (goodsDetail.ptActivityItemDTO && goodsDetail.ptActivityItemDTO.limitItemCount) {
        const limitItemCount = goodsDetail.ptActivityItemDTO.limitItemCount;
        // 判断是否为拼团订单，及购买数量是否大于限购数量
        if (this.state.isBuyOfGroup && detail.itemCount > limitItemCount) {
          Taro.showToast({
            title: `购买数量最多只能${limitItemCount}`,
            icon: 'none',
          });

          return;
        }
      }
      this.state.chooseAttributeDialogNode.hideAttributeDialog();

      const goodsInfoList = info.detail; // 购买的商品信息列表

      if (goodsDetail.wxItem.type === goodsTypeEnum.SERVER.value) {
        // 判断是否为服务拼团订单 服务类型为0
        wxApi.$navigateTo({
          url: '/sub-packages/server-package/pages/appointment/index',
          data: {
            goodsInfo: JSON.stringify(goodsInfoList) + '&isGroupLeader=1', // isGroupLeader=1表示是团长优惠
          },
        });
      } else if (goodsDetail.wxItem.type === goodsTypeEnum.ROOMS.value) {
        // 判断是否为客房拼团订单 客房类型为18
        protectedMailBox.send('sub-packages/tourism-package/pages/rooms-info/index', 'goodsInfo', goodsInfoList);

        wxApi.$navigateTo({
          url: '/sub-packages/tourism-package/pages/rooms-info/index',
          data: {
            isGroupLeader: 1, // isGroupLeader=1表示是团长优惠
          },
        });
      } else if (goodsDetail.wxItem.type === goodsTypeEnum.TICKETS.value) {
        // 判断是否为票务拼团订单 票务类型为19
        protectedMailBox.send('sub-packages/tourism-package/pages/tickets-info/index', 'goodsInfo', goodsInfoList);

        wxApi.$navigateTo({
          url: '/sub-packages/tourism-package/pages/tickets-info/index',
          data: {
            isGroupLeader: 1, // isGroupLeader=1表示是团长优惠
          },
        });
      } else if (goodsDetail.wxItem.type === goodsTypeEnum.SLEEVE_SYSTEM.value) {
        // 判断是否为套系拼团订单 类型为29
        protectedMailBox.send(pageLinkEnum.photographyPkg.appointment, 'goodsInfo', goodsInfoList);

        wxApi.$navigateTo({
          url: pageLinkEnum.photographyPkg.appointment,
          data: {
            isGroupLeader: 1, // isGroupLeader=1表示是团长优惠
          },
        });
      } else {
        // 购买的商品信息列表
        const goodsInfoList = [
          {
            pic: detail.pic,
            salePrice: detail.salePrice,
            itemCount: detail.itemCount,
            noNeedPay: detail.noNeedPay,
            name: detail.activityName || detail.name,
            itemNo: detail.itemNo,
            barcode: detail.barcode,
            skuId: detail.skuId,
            skuTreeIds: detail.skuTreeIds,
            skuTreeNames: detail.skuTreeNames,
            wxItem: detail.wxItem,
            reportExt: JSON.stringify({
              sessionId: this.state.sessionId,
            }),

            drugType: detail.wxItem.drugType,
          },
        ];

        // 判断是否有核销时间，有则添加核销时间跳转付款页面显示
        if (
          goodsDetail.ptActivityItemDTO &&
          goodsDetail.ptActivityItemDTO.verificationStartTime &&
          goodsDetail.ptActivityItemDTO.verificationEndTime
        ) {
          goodsInfoList[0].verificationStartTime = goodsDetail.ptActivityItemDTO.verificationStartTime;
          goodsInfoList[0].verificationEndTime = goodsDetail.ptActivityItemDTO.verificationEndTime;
        }

        const orderRequestPromDTO = {}; // 订单请求参数
        const navigateToData = {}; // 跳转页面所需的参数
        // 判断是拼团购买订单的话，则添加订单请求参数及优惠信息参数
        if (this.state.isBuyOfGroup) {
          orderRequestPromDTO.minPeople = detail.minPeople;
          orderRequestPromDTO.activityId = activityId;

          const payDetails = [
            {
              name: 'freight',
            },
          ];

          let leaderFee = 0;
          if (goodsDetail.ptActivityItemDTO && goodsDetail.ptActivityItemDTO.leaderPromFee) {
            leaderFee = (goodsDetail.ptActivityItemDTO.leaderPromFee / 100).toFixed(2) * 1;
          }
          if (leaderFee) {
            payDetails.push({
              isCustom: true,
              label: '团长优惠',
              value: `-￥${leaderFee}`,
            });
          }
          navigateToData.payDetails = JSON.stringify(payDetails);

          navigateToData.params = JSON.stringify({
            orderRequestPromDTO,
          });
        }

        report.startGroup(activityId); // 统计上报
        report.clickBuy(activityId, 2);

        protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', goodsInfoList);

        if (this.state.goodsDetail.wxItem.type === goodsTypeEnum.VIRTUAL_GOODS.value) {
          navigateToData.itemNo = this.state.itemNo;
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index',
            data: navigateToData,
          });
        } else {
          if(!!navigateToData && Object.keys(navigateToData).length > 0){
            navigateToData.params = encodeURI(navigateToData.params) || ''
            navigateToData.payDetails = encodeURI(navigateToData.payDetails) || ''
            wxApi.$navigateTo({
              url: pageLinkEnum.orderPkg.payOrder,
              data: navigateToData,
            });
          } else {
            wxApi.$navigateTo({
              url: pageLinkEnum.orderPkg.payOrder
            });
          }

        }
      }
    }

  render() {
    const {
      loading,
      goodsImages,
      tmpStyle,
      ptActivityItemDTO,
      groupActivityStatus,
      goodsDetail,
      dataLoad,
      groups,
      itemNo,
      industry,
      industryEnum,
      goodType,
      limitAmountTitle,
      groupRule,
      uniqueShareInfoForQrCode,
    } = this.state
    return (
      <>
        {
          loading && (
            <ScrollView
              scrollY
              data-fixme="02 block to view. need more test" data-scoped="wk-mpgd-Detail" className="wk-mpgd-Detail">
              <AuthPuop></AuthPuop>
              <Block>
                {
                  goodsDetail
                  ? (
                    <View className="goods-detail-container">

                      {/* 商品轮播图 */}
                      <Swiper className="swiper" indicatorDots={goodsImages.length > 1}>
                        {goodsImages &&
                          goodsImages.map((item) => {
                            return (
                              <Block key={item}>
                                <SwiperItem>
                                  <View
                                    className="goods-image"
                                    style={_safe_style_(
                                      'background: transparent url(' + item + ') no-repeat 50% 50%;background-size: cover;'
                                    )}
                                  ></View>
                                </SwiperItem>
                              </Block>
                            );
                          })}
                      </Swiper>

                      {/*  商品基础信息  */}
                      <View className="goods-info">
                        <View className="card">
                          <View className="sale-info-box" style={_safe_style_('background:' + tmpStyle.bgGradualChange)}>
                            <View className="sale-info">
                              <View className="desc-box">限时拼团</View>
                            </View>

                            {/*  活动剩余时间  */}
                            {
                              ptActivityItemDTO.displayTime && (
                                <View className="remain-time-box">
                                  <View className="remain-time-title">
                                    {'距离' +
                                      (ptActivityItemDTO.currentStatus === groupActivityStatus.NOT_START ? '开始' : '结束') +
                                      '还剩：'}
                                  </View>
                                  <View className="remain-time">
                                    <Text className="time-block">{ptActivityItemDTO.displayTime.day}</Text>
                                    <Text className="symbol">天</Text>
                                    <Text className="time-block">{ptActivityItemDTO.displayTime.hour}</Text>
                                    <Text className="symbol">:</Text>
                                    <Text className="time-block">{ptActivityItemDTO.displayTime.minute}</Text>
                                    <Text className="symbol">:</Text>
                                    <Text className="time-block">{ptActivityItemDTO.displayTime.second}</Text>
                                  </View>
                                </View>
                              )
                            }
                          </View>

                          <View className="goods-price">
                            <View className="sale-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                              <Text className="unit">￥</Text>
                              <Text>{ptActivityItemDTO.priceRange || 0}</Text>
                            </View>
                            {/*  原售价  */}
                            <View className="label-price">
                              {'￥' + filters.moneyFilter(goodsDetail.wxItem.salePrice, true)}
                            </View>
                        </View>

                          {
                            ptActivityItemDTO.activityName && (
                              <View className="order-info">
                                <View className="goods-name limit-line line-5">
                                  {
                                    goodsDetail.wxItem.drugType ? (
                                      <Text className="drug-tag" style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                                        处方药
                                      </Text>
                                    ) : ''
                                  }

                                  { ptActivityItemDTO.activityName }
                                </View>
                                <View className="activity-info">
                                  <View className="count">{'已拼' + (goodsDetail.wxItem.itemSalesVolume || 0) + '单'}</View>
                                  <View
                                    className="save-money"
                                    style={_safe_style_('color: ' + tmpStyle.btnColor + ';background: ' + tmpStyle.bgColor)}
                                  >
                                    {'立减' + filters.moneyFilter(goodsDetail.wxItem.salePrice - ptActivityItemDTO.salePrice, true) + '元'}
                                  </View>
                                </View>
                              </View>
                            )
                          }

                          {/*  规则详情入口  */}
                          <View className="tag-box rule-info">
                            <View className="tag-item">规则详情</View>
                            <View className="more-rule" onClick={this.handleViewRule}>
                              <Text>支付开团，邀请好友参与，成功发货，失败退款</Text>
                              <Image
                                className="img-box"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                              ></Image>
                            </View>
                          </View>

                          {/* 定金模式 */}
                          {
                            goodsDetail.wxItem.frontMoneyItem && (
                              <FrontMoneyItem
                                leftRightMargin={0}
                                leftRightPadding={30}
                                frontMoney={goodsDetail.wxItem.frontMoney}></FrontMoneyItem>
                            )
                          }

                          {/*  提货时间  */}
                        {
                          this.isShowVerificationTimeBox() && (
                            <View className="tag-box verification-time-box">
                              <View className="tag-item">核销时间</View>
                              <View className="verification-time">
                                {filters.dateFormat(ptActivityItemDTO.verificationStartTime, 'yyyy-MM-dd hh:mm') +
                                  '\r\n          至\r\n          ' +
                                  filters.dateFormat(ptActivityItemDTO.verificationEndTime, 'yyyy-MM-dd hh:mm')}
                              </View>
                            </View>
                          )
                        }

                        </View>

                        {/*  正在凑团的拼团信息  */}
                        {
                          dataLoad && groups && groups.length > 0 && (
                            <View className="card more-group">
                              <View className="title">
                                <View className="line" style={_safe_style_('background: ' + tmpStyle.btnColor)}></View>
                                <View className="line1" style={_safe_style_('background: ' + tmpStyle.btnColor)}></View>
                                <Text style={_safe_style_('margin: 0 24rpx 0 42rpx;color: ' + tmpStyle.btnColor)}>正在凑团</Text>
                                <View className="line" style={_safe_style_('background: ' + tmpStyle.btnColor)}></View>
                                <View className="line1" style={_safe_style_('background: ' + tmpStyle.btnColor)}></View>
                              </View>
                              <View className="tag-box" style={_safe_style_('margin-bottom: 22rpx;')}>
                                <View className="tag-item">以下小伙伴正在发起拼团，您可直接参与</View>
                                <View className="view-more" onClick={this.handleViewMoreGroup}>
                                  更多&gt;&gt;
                                </View>
                              </View>
                              {/*  拼团列表  */}
                              {
                                dataLoad && (
                                  <ListGroup
                                    groups={this.groupTop2()}
                                    enableOldWithNew={ptActivityItemDTO.enableOldWithNew}
                                    disable={ptActivityItemDTO.currentStatus === groupActivityStatus.NOT_SHELF}></ListGroup>
                                )
                              }
                            </View>
                          )
                        }

                        {/*  组合商品明细  */}
                        {
                          goodsDetail.wxItem.combinationDTOS && goodsDetail.wxItem.combinationDTOS.length > 0 && (
                            <CombinationItemDetail
                              combinationDtos={goodsDetail.wxItem.combinationDTOS}
                              style="display: block;border-bottom: 20rpx solid rgba(241, 244, 249, 1);"></CombinationItemDetail>
                          )
                        }

                        {/*  商品评论  */}
                        {
                          dataLoad && itemNo && industry !== industryEnum.type.beauty.value && (
                            <CommentModule
                              id="comment"
                              showHeader
                              isBrief
                              commentTitle={goodsDetail.wxItem.name}
                              itemNo={itemNo}></CommentModule>
                          )
                        }

                        {/*   药品说明   */}
                        { industryEnum.type.medicine && (industry === industryEnum.type.medicine.value) && (<DrugsInfo goodsDetail={goodsDetail}></DrugsInfo>) }

                        {/*  商品详情  */}
                        <DetailParser itemDescribe={goodsDetail.wxItem.describe}></DetailParser>

                      </View>
                        {/*  底部购买按钮栏  */}
                      {
                        dataLoad && (
                          <BuyNow
                            style="z-index: 55;"
                            disable={this.isDisable()}
                            operationText={this.operationText()}
                            defaultText={this.defaultText()}
                            buyDirectText={this.buyDirectText()}
                            isGradualChange={this.isGradualChange()}
                            goodsDetail={goodsDetail}
                            showCart={false}
                            showBuyDirect={this.isCanBuyDirect()}
                            onBuyDirect={this.handleBuyDirect}
                            immediateShare={false}
                            onShare={this.handleSelectChanel}
                            onBuyNow={this.handleBuyNow}></BuyNow>
                        )
                      }
                    </View>
                  )
                  : (
                    <TaskError storeName=""  onCallSomeFun={this.getGoodsDetail}></TaskError>
                  )
                }
              </Block>
              {/*  选择商品属性弹框  */}
              {
                goodsDetail && (
                  <ChooseAttributeDialog
                    id="choose-attribute-dialog"
                    goodsType={goodType}
                    goodsDetail={goodsDetail}
                    limitAmountTitle={limitAmountTitle}
                    ref={this.chooseAttributeDialogRef}
                    onChoose={this.handlerChooseAttribute}></ChooseAttributeDialog>
                )
              }

              {/*  规则详情  */}
              <AnimatDialog id="rule-dialog" ref={this.refRule} key="rule-dialog" animClass="rule-dialog">
                {dataLoad && <RuleDesc onRule={this.onRuleLoad}></RuleDesc>}
                <Image
                  className="icon-close"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/close.png"
                  onClick={this.handleCloseRule}
                ></Image>
              </AnimatDialog>

              {/*  更多已发起拼团的小伙伴  */}
              <AnimatDialog id="group-dialog" ref={this.groupRule} key="group-dialog" animClass="group-dialog">
                <View className="more-title">正在拼团</View>
                {/*  拼团列表  */}
                <ListGroup
                  className="list-group"
                  size="small"
                  groups={groups}
                  enableOldWithNew={ptActivityItemDTO.enableOldWithNew}
                  disable={ptActivityItemDTO.currentStatus === groupActivityStatus.NOT_SHELF}></ListGroup>
                <Image
                  className="icon-close"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/close.png"
                  onClick={this.handleCloseGroup}
                ></Image>
              </AnimatDialog>

                {/*  分享对话弹框  */}
                <ShareDialog
                  id="share-dialog"
                  childRef={this.state.shareDialogNode}
                  posterHeaderType={groupRule.posterType || 1}
                  posterTips={groupRule.posterContent || '一起来拼团呀'}
                  posterLogo={groupRule.posterLogo}
                  posterSalePriceLabel="拼团价"
                  posterLabelPriceLabel="活动结束价"
                  posterName={goodsDetail.wxItem.name}
                  posterImage={goodsDetail.wxItem.thumbnail}
                  salePrice={goodsDetail.ptActivityItemDTO.priceRange}
                  labelPrice={filters.moneyFilter(goodsDetail.wxItem.salePrice, true)}
                  onSave={this.onSavePosterImage}
                  uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}></ShareDialog>
                  <HomeActivityDialog showPage={'sub-packages/moveFile-package/pages/group/detail/index?itemNo=' + itemNo}></HomeActivityDialog>
            </ScrollView>

          )
        }
      </>
    )
  }
}

Detail.enableShareAppMessage = true

export default Detail;
