import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import report from '@/sdks/buried/report/index.js';
import shareUtil from '../../../../../wxat-common/utils/share.js';

import AuthPuop from '../../../../../wxat-common/components/authorize-puop/index';
import HomeActivityDialog from '../../../../../wxat-common/components/home-activity-dialog/index';
import GroupList from '../../../../../wxat-common/components/tabbarLink/group-list/index';
import './index.scss';

class List extends React.Component {

  state = {
    component: null,
    showGroupType: 0, // 拼团子组件展示类型，0：上图下文(默认展示的类型)，1：左图右文
    groupListRef: React.createRef()
  }

  onLoad (options) {
    // 判断是否有拼团子组件展示类型参数
    if (options.showGroupType || options.showGroupType === 0) {
      this.setState({
        showGroupType: options.showGroupType,
      });
    }
  }

  onPullDownRefresh () {
    this.state.groupListRef.current.onPullDownRefresh();
  }

  onReachBottom () {
    this.state.groupListRef.current.onReachBottom();
  }

  onRetryLoadMore () {
    this.state.groupListRef.current.onRetryLoadMore();
  }

  onShareAppMessage () {
    report.share(true);
    const url = '/sub-packages/moveFile-package/pages/group/list/index';
    return {
      title: '',
      path: shareUtil.buildShareUrlPublicArguments(url, shareUtil.ShareBZs.GROUP_LIST),
      imageUrl: '',
    };
  }

  render () {
    const { showGroupType } = this.state
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpgl-List" className="wk-mpgl-List">
        <GroupList id="group-list" ref={this.state.groupListRef} showGroupType={showGroupType}></GroupList>
        <AuthPuop></AuthPuop>
        <HomeActivityDialog showPage="sub-packages/moveFile-package/pages/group/list/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default List;
