import { Block, ScrollView, View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import wxClass from '../../../../../../wxat-common/utils/wxClass.js';
import api from '../../../../../../wxat-common/api/index.js';
import wxApi from '../../../../../../wxat-common/utils/wxApi';

import AnimatDialog from '../../../../../../wxat-common/components/animat-dialog/index';
import './index.scss';

const Title = '拼团规则说明';
const Setions = [
        '1、必须根据拼团要求的最低人数，才能拼团成功。',
        '2、发起拼团先要支付，如果最终拼团不成功，支付的费用自动原路退回。',
        '3、拼团设置的时限内，例如24小时，必须找够人数，否则拼团不成功，退款。',
        '4、参与阶梯团定价的商品，越多人参与团购，价格越便宜。',
      ];

class RuleDesc extends React.Component {

  state = {
    title: '',
    setions: [],
  }

  componentDidMount() {
    wxApi
      .request({
        url: api.activity.querySetting,
      })
      .then((res) => {
        console.log('api.activity.querySetting===', res);
        // this.$scope.triggerEvent('rule', res.data);
        this.props.onRule(res.data);
        const content = res.data.content || '';
        const array = content.split('↵');
        let setions = [];
        array.forEach((item) => {
          if (item === '') {
            return;
          }
          setions = setions.concat(item.split('\n').filter((section) => section !== ''));
        });
        this.setState({
          title: res.data.title || Title,
          setions: setions.length ? setions : Setions
        });
      })
  }

  render () {
    const { title, setions } = this.state;
    return (
      <ScrollView data-scoped="wk-pgcr-RuleDesc" className="wk-pgcr-RuleDesc scroll-view_H" scrollY className="scroll">
        <View className="rule">
          <View className="title">{title}</View>
          {setions &&
            setions.map((item, index) => {
              return (
                <View className="block" key={'block-' + index}>
                  {item}
                </View>
              );
            })}
        </View>
      </ScrollView>
    );
  }
}

export default RuleDesc;
