import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import api from '../../../../../../wxat-common/api/index.js';
import wxApi from '../../../../../../wxat-common/utils/wxApi';
import template from '../../../../../../wxat-common/utils/template.js';
import { connect } from 'react-redux'
import './index.scss';


const mapStateToProps = (state) => {
  return {
    userInfo: state.base.userInfo
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ListGroup extends React.Component {

  state = {
    tmpStyle: {},
    isNewUser: false, // 是否为新用户
  }

  componentDidMount () {
    this.getTemplateStyle(); // 获取模板配置
    this.queryIsNewUser(); // 查询是否为新用户
  }

  /**
  * 获取模板配置
  */
  getTemplateStyle () {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
  * 查询是否为新用户
  */
  queryIsNewUser () {
    wxApi
      .request({
        url: api.user.isNewUser,
        data: {},
      })
      .then(
        (res) => {
          const data = res.data;
          if (!!data) {
            this.setState({
              isNewUser: true,
            });
          }
        }
      )
  }

  /**
  * 去参团
  * @param {*} e
  */
  onJoinGroup = (e) => {
    const userId = this.props.userInfo.id || null; // 当前用户id
    const leaderUserId = e.currentTarget.dataset.leaderUserId || null; // 当前点击的团长id
    if (this.props.disable) {
      // 判断是否禁止操作
      return;
    } else if (
      userId &&
      leaderUserId &&
      userId !== leaderUserId && // 判断点击的是否为自己发起的团
      !!this.props.enableOldWithNew && // 判断该团是否为老带新拼团
      !this.state.isNewUser
    ) {
      // 判断当前用户是否为老用户
      Taro.showToast({
        icon: 'none',
        title: '该团仅限新用户可以参与哦，老用户可发起此拼团活动，邀请新用户共同参团',
      });

      return;
    }

    // 跳转参团页面
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/group/join/index',
      data: {
        groupNo: e.currentTarget.dataset.no,
      },
    });
  }

  render () {
    const { tmpStyle } = this.state
    return (
      <View data-fixme="03 add view wrapper. need more test"
        data-scoped="wk-pgcl-ListGroup"
        className="wk-pgcl-ListGroup">
        {this.props.groups &&
          this.props.groups.map((item, index) => {
            return (
              <View
                className="group-item"
                key={item.groupNo}
                onClick={_fixme_with_dataset_(this.onJoinGroup, { no: item.groupNo, leaderUserId: item.leaderUserId })}
              >
                <Image src={item.leaderAvatar} className="group-item-image avatar"></Image>
                <View className={'group-item-view ' + ('user-name name_' + this.props.size)}>{item.leaderNickName || '******'}</View>
                <View className="group-item-view group-time" style={_safe_style_('flex: 1')}>
                  <View className="group-item-view remain-people" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    {'还差' + (item.requirePeople - item.currentCount) + '人成团'}
                  </View>
                  <View className="group-item-view remain-time" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    {'剩余：' + item.displayTime}
                  </View>
                </View>
                <View
                  className={'group-item-view ' + ('join join-' + this.props.size) + ' ' + (this.props.disable ? 'disabled ' : '')}
                  style={_safe_style_('background:' + tmpStyle.btnColor)}
                >
                  去参团
                </View>
              </View>
            )
          })}
      </View>
    )
  }
}

export default ListGroup;
