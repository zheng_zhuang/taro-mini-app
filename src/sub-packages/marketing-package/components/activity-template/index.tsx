import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Image, Text, ScrollView, RichText } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import './index.scss';
import { connect } from 'react-redux';
import iosPayTypeEnum from "@/wxat-common/constants/iosPayTypeEnum";
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'
interface ComponentProps {
  activityDetail: Object;
  iosSetting:any,
  isIOS:boolean
}

interface ComponentState {
  type: number;
  shopType: number;
  choose: string;
  backGroundImage: string;
  goodsList: any;
  topicImgUrl: string;
  isIos: number;
  partId: any;
  rechargeTypeList:any;
  itemBackGroud: string;
  topicDesc: any;
}
const mapStateToProps = (state) => {
  return {
    iosSetting:state.globalData.iosSetting,
    isIOS:state.globalData.isIOS
  };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ActivityTemplate extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    activityDetail: null,
    iosSetting:{},
    isIOS:false
  };
  iosPayLimitDialog = React.createRef<any>()
  state = {
    partId:null,
    type: 5,
    shopType: 1,
    choose: '1',
    backGroundImage: 'https://front-end-1302979015.file.myqcloud.com/images/c/images/template3.png',
    goodsList: [],
    topicImgUrl: '',
    isIos: 1,
    rechargeTypeList: [],
    itemBackGroud:'',
    topicDesc:null,
  }

  componentDidMount() {
    const { activityDetail } = this.props;
    this.getModel();
    this.setState({
      backGroundImage: `https://front-end-1302979015.file.myqcloud.com/images/c/images/template${this.state.type}.png`,
      topicImgUrl: activityDetail["topicImgUrl"],
      partId: activityDetail['topicPartitions'][0] && activityDetail['topicPartitions'][0]['partId'],
      type: Number(activityDetail["styleNo"]),
    },() =>{
      this.format(activityDetail)
    });

  }
  format(obj) {
    let topicDesc = null;
    let shopType = 1;
    switch (obj.display) {
      case 'leftRightSlide':
        shopType = 1;
        break;
      case 'OneRowTwo':
        shopType = 2;
        break;
      case 'OneRowThree':
        shopType = 3;
        break;
      default:
        shopType = 1;
        break;
    }

    if (obj.topicDesc) {
      topicDesc = obj.topicDesc.replace(/<img /g, '<img class="rich_img" ');
    }
    this.setState({
      rechargeTypeList: obj.topicPartitions,
      type: obj.styleNo,
      shopType,
      partId: obj.topicPartitions[0] && obj.topicPartitions[0]['partId'],
      itemBackGroud: obj.itemBackGroud && obj.backGroudType != 0 ? `url('${obj.itemBackGroud}')` : '',
      topicImgUrl: obj.topicImgUrl ? obj.topicImgUrl : '',
      // 如果是<p><br></p>，则是在pc设置描述后，又再次删除描述的空白返回值。
      topicDesc: topicDesc == '<p><br></p>' ? null : topicDesc,
    },() =>{
      this.getList();
    });

  }


  componentDidUpdate(preProps) {
    // 会在更新后会被立即调用。首次渲染不会执行此方法
    if (preProps.activityDetail !== this.props.activityDetail) {
      const obj = preProps.activityDetail
      this.format(obj)
    }
  }

  /* 获取设备 */
  getModel() {
    const that = this;
    Taro.getSystemInfo({
      success: function (res) {
        // console.log(res.screenHeight,res.screenWidth,res.screenHeight/res.screenWidth)
        if (res.platform == 'ios') {
          that.setState({
            isIos: 1,
          });
        } else if (res.platform == 'android') {
          that.setState({
            isIos: 0,
          });
        }
      },
    });
  }

  choose=(item)=>{
    console.log(item)
    this.setState({
      choose: item.index,
      partId: item.partId,
    },()=>{
      this.getList();
    });
  }

  getList() {
    wxApi
      .request({
        url: api.specialActivity.detail,
        loading: true,
        data: {
          partId: this.state.partId,
        },
      })
      .then((res) => {
        const goodsList = res.data || [];
        this.setState({
          goodsList,
        });
      })
      .catch((error) => {
        // this.apiError(error);
      });
  }

  onClickGoodsItem = (e) => {
    let url = '/wxat-common/pages/goods-detail/index';
    let data = {};

    const goodsItem = e.currentTarget.dataset.detail;
    const goodsType = goodsItem.wxItem ? goodsItem.wxItem.type : goodsItem.type;
    switch (goodsType) {
      case goodsTypeEnum.PRODUCT.value:
        // 产品类型
        url = '/wxat-common/pages/goods-detail/index';
        break;
      case goodsTypeEnum.ROOMS.value:
        // 客房类型
        url = '/wxat-common/pages/goods-detail/index';
        break;
      case goodsTypeEnum.TICKETS.value:
        // 票务类型
        url = '/wxat-common/pages/goods-detail/index';
        break;
      case goodsTypeEnum.COMBINATIONITEM.value:
        // 组合商品类型
        url = '/wxat-common/pages/goods-detail/index';
        break;
      case goodsTypeEnum.SERVER.value:
        // 服务类型
        url = '/sub-packages/server-package/pages/serve-detail/index';
        break;
      case goodsTypeEnum.TIME_CARD.value || goodsTypeEnum.CHARGE_CARD.value:
        // 卡项或充值卡类型
        url = '/sub-packages/server-package/pages/card-detail/index';
        break;
    }

    data = {
      itemNo: goodsItem.itemNo || goodsItem.wxItem.itemNo,
      source: reportConstants.SOURCE_TYPE.activity.key,
    };

    wxApi.$navigateTo({
      url: url,
      data: data,
    });
  }

  async buyVirtualCommodity(e) {
    const { data } = await wxApi.request({
      url: api.virtualGoods.unifiedVirtualOrder,
      method: 'POST',
      data: {
        virtualItemOrderVO: {},
        itemNo: e.currentTarget.dataset.detail.wxItem.itemNo,
        distributorId: e.currentTarget.dataset.detail.wxItem.distributorId,
      },

      loading: true,
    });

    data.timeStamp = data.timeStamp + '';
    data.package = 'prepay_id=' + data.prepayId;
    data.success = () => {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/pay-success/index',
        data: {
          costPrice: e.currentTarget.dataset.detail.wxItem.salePrice,
        },

        success() {},
        fail() {},
      });
    };
    data.fail = ({ errMsg }) => {
      if (errMsg !== 'requestPayment:fail cancel') {
        Taro.showToast({ title: errMsg });
      }
    };
    Taro.requestPayment(data);
  }

   handleBuyNow = async (e) =>{
    if (!authHelper.checkAuth()) {
      return;
    }
    if (e.currentTarget.dataset.detail.wxItem.type === 41) {
      // this.buyVirtualCommodity(e)
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index',
        data: {
          itemNo: e.currentTarget.dataset.detail.wxItem.itemNo,
        },
      });
    } else {
      this.onClickGoodsItem(e);
    }
  }
  // ios下单
  handleIosBuyNow=(e)=>{
    if (!authHelper.checkAuth()) {
      return;
    }
    if (!this.IosPayControl(e)) {
      return;
    }
    if(e.currentTarget.dataset.detail.wxItem.type === 41 ){
      wxApi.$navigateTo({
        url:"/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index",
        data:{
          itemNo: e.currentTarget.dataset.detail.wxItem.itemNo,
          iosPayNodes: this.props.iosSetting ? encodeURIComponent(this.props.iosSetting.text) : '',
          jumpType: this.props.iosSetting ? this.props.iosSetting.jumpType: '1'
        }
      })
    }else {
      this.onClickGoodsItem(e)
    }
  }

  // 根据后台设置配置支付
  IosPayControl=(e)=>{
    if (this.props.iosSetting.payType == iosPayTypeEnum.payType.JUST_LIKE_ANDROID.value) {
      this.handleBuyNow(e)
      return false
    } else if (this.props.iosSetting.payType == iosPayTypeEnum.payType.H5_PAY.value) {
      return true
    } else if (this.props.iosSetting.payType == iosPayTypeEnum.payType.IOS_PAY_NONSUPPORT.value) {
      // 做弹窗操作
      this.showIospayLimitDialog()
      return false
    }
  }

  /**
     * 打开提示弹窗
     */
   showIospayLimitDialog = () =>{
    this.iosPayLimitDialog.current.showIospayLimitDialog();
  }

  render() {
    const {
      topicImgUrl,
      backGroundImage,
      isIos,
      type,
      choose,
      rechargeTypeList,
      shopType,
      itemBackGroud,
      goodsList,
      topicDesc,

    } = this.state;
    const {iosSetting,isIOS} = this.props
    return (
      <View data-scoped="wk-smca-ActivityTemplate" className="wk-smca-ActivityTemplate module-wrap">
        <Image
          src={topicImgUrl || backGroundImage}
          mode="widthFix"
          style={_safe_style_('width: 100%; min-height: 100vh; display: block')}
        ></Image>
        <View className="activity-box">
          <View className={isIos == 1 ? 'isIos' : 'android'}>
            {(type == 1 || type === 2) && rechargeTypeList.length > 1 && (
              <Text
                className={
                  'content-tab ' +
                  ('content-tab' + type) +
                  ' ' +
                  (choose == '1' ? 'choose' + type : '') +
                  ' ' +
                  (rechargeTypeList.length > 1 ? 'has-two' + type : '')
                }
                onClick={()=>this.choose({ index: '1', partId: rechargeTypeList[0]?.partId })}
              >
                {rechargeTypeList[0]?.partName}
              </Text>
            )}

            {(type == 1 || type === 2) && rechargeTypeList.length > 1 && (
              <Text
                className={
                  'content-tab ' +
                  ('content-tab' + type) +
                  ' ' +
                  (choose == '2' ? 'choose' + type : '') +
                  ' ' +
                  (rechargeTypeList.length > 1 ? 'has-two' + type : '')
                }
                onClick={()=>this.choose({ partId: rechargeTypeList[1]?.partId, index: '2' })}
              >
                {rechargeTypeList[1]?.partName}
              </Text>
            )}

            <View
              className={
                'midden-content ' +
                ('midden-content' + type + '-' + shopType) +
                ' ' +
                ('midden-content' + type) +
                ' ' +
                (choose == '2' ? 'border-right' : '')
              }
            >
              <View className={'template-box3 ' + ('template-box' + type + '-' + shopType)}>
                {shopType === 1 ? (
                  <Block>
                    <ScrollView scrollX>
                      {goodsList &&
                        goodsList.map((item, index) => {
                          return (
                            <View
                              className={'top ' + ('type' + type + '-' + shopType)}
                              key={index}
                              style={_safe_style_('background-image: ' + (itemBackGroud || ''))}
                            >
                              {item.wxItem.labelName && (
                                <Text className={'tip ' + ('tip' + type)}>{item.wxItem.labelName}</Text>
                              )}

                              <Text className="title">{item.wxItem.name}</Text>
                              <View className={'money ' + ('money' + type)}>
                                <Text>￥</Text>
                                <Text>{item.wxItem.salePrice / 100}</Text>
                              </View>
                              <View className="init-money flex-JC-center">
                                <Text className='text1 text'>官方价</Text>
                                <Text className='text2 text'>{'￥' + item.wxItem.labelPrice / 100}</Text>
                              </View>
                              <View className="btnBox">
                                {
                                  isIOS && item.wxItem.type ===41 ?
                                  <View className={'btn '  + ('btn' + type + '-' + shopType ) + ' ' + ('btn' + type)} onClick={_fixme_with_dataset_(this.handleIosBuyNow, { detail: item })}>{iosSetting&&iosSetting.buttonCopywriting?iosSetting.buttonCopywriting:'去购买'}</View> : <View
                                  className={'btn ' + ('btn' + type + '-' + shopType) + ' ' + ('btn' + type)}
                                  onClick={_fixme_with_dataset_(this.handleBuyNow, { detail: item })}
                                >
                                  去购买
                                </View>
                                }

                              </View>
                            </View>
                          );
                        })}
                    </ScrollView>
                  </Block>
                ) : (
                  <Block>
                    {goodsList &&
                      goodsList.map((item, index) => {
                        return (
                          <View
                            className={'top ' + ('type' + type + '-' + shopType)}
                            key={index}
                            style={_safe_style_('background-image: ' + (itemBackGroud || ''))}
                          >
                            {item.wxItem.labelName && (
                              <Text className={'tip ' + ('tip' + type)}>{item.wxItem.labelName}</Text>
                            )}

                            <Text className="title">{item.wxItem.name}</Text>
                            <View className={'money ' + ('money' + type)}>
                              <Text>￥</Text>
                              <Text>{item.wxItem.salePrice / 100}</Text>
                            </View>
                            <View className="init-money">
                              <Text className='text1 text'>官方价</Text>
                              <Text className='text2 text'>{'￥' + item.wxItem.labelPrice / 100}</Text>
                            </View>
                            <View className="btnBox">
                              {
                                isIOS && item.wxItem.type ===41 ?
                                <View className={'btn '  + ('btn' + type + '-' + shopType ) + ' ' + ('btn' + type)} onClick={_fixme_with_dataset_(this.handleIosBuyNow, { detail: item })}>{iosSetting&&iosSetting.buttonCopywriting?iosSetting.buttonCopywriting:'去购买'}</View> :
                                <View
                                className={'btn ' + ('btn' + type + '-' + shopType) + ' ' + ('btn' + type)}
                                onClick={_fixme_with_dataset_(this.handleBuyNow, { detail: item })}
                                >
                                  去购买
                                </View>

                              }
                            </View>
                          </View>
                        );
                      })}
                  </Block>
                )}
              </View>
            </View>
          </View>
          {topicDesc && (
            <View className="bottom-box">
              <RichText nodes={topicDesc}></RichText>
            </View>
          )}
        </View>
        <IosPayLimitDialog tipsCopywriting={iosSetting?.tipsCopywriting} ref={this.iosPayLimitDialog}></IosPayLimitDialog>
      </View>
    );
  }
}

export default ActivityTemplate;
