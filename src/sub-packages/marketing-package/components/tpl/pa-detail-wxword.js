import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Button } from '@tarojs/components';
import filters from '@/wxat-common/utils/money.wxs';
import Taro from '@tarojs/taro';
import './index.scss';


class PaDetailWxworkTmpl extends React.Component {
  static defaultProps = {
    isFullScreen: false,
    goodsDetail: {},
    tmpStyle: {},
    extraData: {},
  };

  static options = {
    addGlobalClass: true,
  };

  render() {
    const { isFullScreen, goodsDetail, tmpStyle, extraData } = this.props;
    return (
      <Block>
        {goodsDetail.itemStock <= 0 && <View style={_safe_style_('height:77px')}></View>}
        <View className={'promotion-amb-foot ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {goodsDetail.itemStock <= 0 && <View className='no-stock'>库存不足</View>}
          <View className='btn-box-wxwork'>
            <View className='wxwork-tips'>
              <View>大使赚{'¥' + filters.moneyFilter(extraData.promoterCommission, true)}</View>
              我赚{'¥' + filters.moneyFilter(extraData.guideCommission, true)}
            </View>
            {extraData.activityStatus == 1 ? (
              <ButtonWithOpenType
                openType='share'
                className='btn wxwork-btn'
                style={_safe_style_('background: ' + tmpStyle.btnColor)}
              >
                分享
              </ButtonWithOpenType>
            ) : (
              <Button className={'btn wxwork-btn ' + (extraData.activityStatus == 0 ? 'activeBtn' : 'defaultBtn')}>
                {extraData.activityStatus == 0 ? '即将开始' : '已结束'}
              </Button>
            )}
          </View>
        </View>
      </Block>
    );
  }
}

export default PaDetailWxworkTmpl;
