import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Button } from '@tarojs/components';
import filters from '@/wxat-common/utils/money.wxs';
import Taro from '@tarojs/taro';
import './index.scss';


class PaDetailTmpl extends React.Component {
  static defaultProps = {
    isFullScreen: false,
    goodsDetail: {},
    tmpStyle: {},
    promoterCommission: '',
    onShare: null,
    activeStatus: false,
  };

  static options = {
    addGlobalClass: true,
  };

  handleShare = () => {
    this.props.onShare();
  };

  render() {
    const { isFullScreen, goodsDetail, tmpStyle, promoterCommission, activeStatus } = this.props;

    return (
      <Block>
        {goodsDetail.itemStock <= 0 && <View style={_safe_style_('height:77px')}></View>}
        <View className={'promotion-amb-foot ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {goodsDetail.itemStock <= 0 && <View className='no-stock'>库存不足</View>}
          <View className='btn-box'>
            <View className='tips'>
              客户购买，立赚
              <Text className='money' style={_safe_style_('color: ' + tmpStyle.btnColor)}>
                {'¥' + filters.moneyFilter(promoterCommission, true)}
              </Text>
            </View>
            {activeStatus ? (
              <Button className='btn defaultBtn'>已结束</Button>
            ) : (
              <Button
                className='btn'
                style={_safe_style_('background: ' + tmpStyle.btnColor)}
                onClick={this.handleShare}
              >
                立即推广
              </Button>
            )}
          </View>
        </View>
      </Block>
    );
  }
}

export default PaDetailTmpl;
