import React, { FC, useRef, useState, useImperativeHandle, useEffect } from 'react'; // @externalClassesConvered(AnimatDialog)
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';

import authHelper from '@/wxat-common/utils/auth-helper';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import canvasHelper from '@/wxat-common/utils/canvas-helper';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import shareUtil from '@/wxat-common/utils/share';
import AuthUser from '@/wxat-common/components/auth-user';
import { useSelector } from 'react-redux';

import './index.scss';

const cdnImg = cdnResConfig.promotion_amb;
let rpx = 1;

// props的类型定义
type IProps = {
  /**
   * 海报图下面的名字
   */
  posterName: string;
  /**
   * 海报图片
   */
  posterImage: string;
  /**
   * 海报提示语
   */
  posterTips?: string;
  /**
   * 二维码生成方式，使用映射ID的方式创建
   */
  uniqueShareInfoForQrCode?: object | null;

  childRef?: any;
};

let PostersDialog: FC<IProps> = ({ posterTips, posterImage, posterName, uniqueShareInfoForQrCode, childRef }) => {
  const [posterDialogVisible, setPosterDialogVisible] = useState(false);
  const [user, setUser] = useState<Record<string, any>>({});
  const [qrCodeUrl, setQrCodeUrl] = useState('');

  const shareDialogRef = useRef<any>(null);

  const wxUserInfo = useSelector((state: Record<string, any>) => state.base.wxUserInfo);
  const contextRef = useRef<any>();
  const filePath = useRef<string>();

  useEffect(() => {
    contextRef.current && uniqueShareInfoForQrCode && show();
  }, [uniqueShareInfoForQrCode]);

  // 需要暴露给外面使用的函数
  useImperativeHandle(childRef, () => ({
    setContext,
    show,
  }));

  const setContext = (context) => {
    contextRef.current = context;
  };

  const userInfoReady = () => {
    setUser({
      name: wxUserInfo ? wxUserInfo.nickName : '',
      avatarUrl: wxUserInfo ? wxUserInfo.avatarUrl : '',
    });
  };

  const initParams = () => {
    setPosterDialogVisible(false);
    setQrCodeUrl('');
    filePath.current = '';

    if (!authHelper.checkAuth()) {
      return;
    }

    apiQRCodeByUniqueKey();
  };

  // 保存
  const handleSave = () => {
    if (filePath.current) {
      checkImageAuth();
    } else {
      initCanvas(checkImageAuth);
    }
  };

  // 预览分享
  const handleShare = () => {
    if (filePath.current) {
      previewImg();
    } else {
      initCanvas(previewImg);
    }
  };

  /** 初始化画布 */
  const initCanvas = (action) => {
    rpx = (wxApi.getSystemInfoSync().windowWidth / 750) * 2;

    const ctx = wxApi.createCanvasContext('shareCanvas', contextRef.current);
    drawCarte(ctx, action);
  };

  const drawCarte = (ctx, action) => {
    Promise.all([
      wxApi.getImageInfo({ src: user.avatarUrl }),
      wxApi.getImageInfo({ src: posterImage }),
      wxApi.getImageInfo({ src: qrCodeUrl }),
    ])
      .then((res) => {
        // 背景
        canvasHelper.drawFillRect(ctx, 0, 0, 275 * rpx, 383 * rpx, '#fff');

        // 圆形头像
        canvasHelper.drawCircleImage(ctx, 45 * rpx, 35 * rpx, 20 * rpx, res[0].path);

        //用户名
        canvasHelper.drawText(ctx, user.name, 75 * rpx, 28 * rpx, {
          textAlign: 'start',
          fillStyle: '#000',
          fontSize: 14 * rpx,
        });

        //红色三角
        canvasHelper.drawFillLine(
          ctx,
          [
            {
              x: 70 * rpx,
              y: 42 * rpx,
            },

            {
              x: 74 * rpx,
              y: 40 * rpx,
            },

            {
              x: 74 * rpx,
              y: 44 * rpx,
            },
          ],

          '#f35f28'
        );

        //红色方框
        canvasHelper.drawFillRect(ctx, 74 * rpx, 33 * rpx, 170 * rpx, 18 * rpx, '#f35f28');

        //海报提示语
        canvasHelper.drawText(ctx, posterTips, 80 * rpx, 46 * rpx, {
          textAlign: 'start',
          fillStyle: '#fff',
          fontSize: 12,
        });

        // 主图
        canvasHelper.drawImage(ctx, 25 * rpx, 70 * rpx, 225 * rpx, 225 * rpx, res[1].path);

        // 二维码
        canvasHelper.drawImage(ctx, 186 * rpx, 305 * rpx, 64 * rpx, 64 * rpx, res[2].path);

        // 海报名称
        canvasHelper.drawText(ctx, posterName, 25 * rpx, 335 * rpx, {
          textAlign: 'start',
          fillStyle: '#000',
          fontSize: 15 * rpx,
          maxWidth: 150 * rpx,
          lineHeight: 20 * rpx,
          oneLineTop: 345 * rpx,
        });

        wxApi.showLoading({ title: '正在生成海报' });
        ctx.draw(true, () => {
          wxApi.canvasToTempFilePath({
            canvasId: 'shareCanvas',
            success: (res) => {
              filePath.current = res.tempFilePath;
              action.call(this);
            },
            complete: () => {
              wxApi.hideLoading();
            },
          });
        });
      })
      .catch((err) => {
        wxApi.showToast({
          title: '加载海报失败，' + err.errMsg,
          icon: 'none',
        });
      });
  };
  /**
   * 保存图片前检查访问相机的权限
   */
  const checkImageAuth = () => {
    wxApi.$saveImage(filePath.current);
  };

  /**
   * 发送给客户（预览图片）
   */
  const previewImg = () => {
    wxApi.previewImage({
      urls: [filePath.current || ''],
    });
  };

  /**
   * 获取二维码
   */
  const apiQRCodeByUniqueKey = () => {
    const publicArguments = shareUtil.buildSharePublicArguments({ bz: '', forServer: true });
    const scene: Record<string, any> = Object.assign({}, publicArguments, uniqueShareInfoForQrCode);
    console.log(scene, 'scene');
    let path = `${scene.page}?activityId=${scene.activityId}&itemNo=${scene.itemNo}&promoterActivityId=${scene.promoterActivityId}&refBz=${scene.refBz}&refBzName=${scene.refBzName}&refStoreId=${scene.refStoreId}&refUseId=${scene.refUseId}&refWxOpenId=${scene.refWxOpenId}&source=${scene.source}`;
    console.log('path=>', path);
    wxApi
      .request({
        url: api.generatorMapperQrCode,
        loading: true,
        data: {
          shareParam: JSON.stringify(scene),
          urlPath: scene.page,
          //type: null
        },
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data);
      });
  };

  const hide = () => {
    shareDialogRef.current && shareDialogRef.current.hide(true);
  };

  const show = (context?) => {
    context && (contextRef.current = context);
    shareDialogRef.current &&
      shareDialogRef.current.show({
        scale: 1,
      });

    initParams();
  };

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-wcp-marketingPostersDialog'
      className='wk-wcp-marketingPostersDialog'
    >
      <AnimatDialog ref={shareDialogRef} animClass='share-dialog'>
        <View className='share-dialog'>
          <AuthUser onReady={userInfoReady}>
            {!!posterDialogVisible && (
              <View className='preview'>
                <View className='user'>
                  <Image className='user-face' src={user.avatarUrl}></Image>
                  <View className='text-content'>
                    <View className='user-name'>{user.name}</View>
                    <View className='tips'>{posterTips}</View>
                  </View>
                </View>
                <Image className='thumbnail' src={posterImage}></Image>
                <View className='wrap'>
                  <View className='product-name limit-line line-2'>{posterName}</View>
                  <Image className='qr-code' src={qrCodeUrl}></Image>
                </View>
              </View>
            )}

            {/*  操作按钮  */}
            <View className='operation'>
              <View className='oper-item' onClick={handleSave}>
                <Image src={cdnImg.save} className='oper-icon'></Image>
                <View className='oper-name'>保存图片</View>
              </View>
              <View className='oper-item' onClick={handleShare}>
                <Image src={cdnImg.share} className='oper-icon'></Image>
                <View className='oper-name'>发送客户</View>
              </View>
            </View>
            <View className='close' onClick={hide}>
              取消
            </View>
          </AuthUser>
        </View>
      </AnimatDialog>
    </View>
  );
};

PostersDialog.defaultProps = {
  posterTips: '给你分享了一个隐蔽的优惠包',
  posterImage: '',
  posterName: '',
  uniqueShareInfoForQrCode: null,
};

export default PostersDialog;
