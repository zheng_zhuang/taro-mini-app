import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Image, Input, ScrollView, Text, RichText } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import { connect } from 'react-redux';
import './index.scss';
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'
import iosPayTypeEnum from "@/wxat-common/constants/iosPayTypeEnum";
const rules = {
  mobile: [
    {
      required: true,
      message: '请输入手机号码',
    },

    {
      reg: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
      message: '手机号码无效',
    },
  ],
};
interface ComponentProps {
  activityDetail: Object;
  iosSetting:any;
  isIOS:boolean
}

interface ComponentState {
  form: { mobile: string; };
  formValidated: { isValied: boolean;msg: string };
  rechargeTypeList: never[];
  rechargeType: number;
  rechargeList: never[];
  selectRechargeItem: null;
  scrollViewWidth: number;
  tmpStyle: null;
  display: string;
  topicDesc: any;
}

const mapStateToProps = (state) => ({
  userInfo: state.base.loginInfo,
  iosSetting:state.globalData.iosSetting,
  isIOS:state.globalData.isIOS
});
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class RechargeVersion extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    activityDetail: null,
  };
  iosPayLimitDialog = React.createRef<any>()
  state= {
    form: {
      mobile: this.props.userInfo.phone || '',
    },

    rechargeTypeList: [],

    rechargeType: 0, //充值类型下标

    rechargeList: [],

    selectRechargeItem: null, //选中的充值

    scrollViewWidth: 0,

    tmpStyle: null,

    display: 'LeftRightSlide', //布局方式

    formValidated: {
      isValied: false,
      msg: '请填写完整信息',
    },
    topicDesc: null,

  }
  componentDidMount():void{
    this.getTemplateStyle();
    const obj = this.props.activityDetail
    let topicDesc = null;
    if (obj.topicDesc) {
      topicDesc = obj.topicDesc.replace(/<img /g, '<img class="rich_img" ');
    }
    this.setState({
      rechargeTypeList: obj.topicPartitions,
      display: obj.display,
      topicDesc: topicDesc,
    },() =>{
      this.getList();
    });
  }
  isIOSFun() {
    const res = wxApi.getSystemInfoSync()
    const model = 'iPhone|iPad|iPod|iOS'
    return model.includes(res.model);
  }
/*  componentWillReceiveProps(nextProps: Readonly<ComponentProps>, nextContext: any): void {
    const obj = nextProps.activityDetail
    let topicDesc = null;
    if (obj.topicDesc) {
      topicDesc = obj.topicDesc.replace(/<img /g, '<img class="rich_img" ');
    }
    this.setState({
      rechargeTypeList: obj.topicPartitions,
      display: obj.display,
      topicDesc: topicDesc,
    },() =>{
      this.getList();
    });

  }*/

/*  componentDidUpdate(preProps) {
    if (preProps.activityDetail !== this.props.activityDetail ) {
      const obj = preProps.activityDetail
      let topicDesc = null;
      if (obj.topicDesc) {
        topicDesc = obj.topicDesc.replace(/<img /g, '<img class="rich_img" ');
      }
      this.setState({
        rechargeTypeList: obj.topicPartitions,
        display: obj.display,
        topicDesc: topicDesc,
      });
      this.getList();
    }
  }*/

  onSetPhone(e) {
    const form = this.state.form;
    form.mobile = e.target.value;
    this.setState({form});
  }
  onRechargePhone() {
    if (!authHelper.checkAuth()) return;
    const form = this.state.form

    // form.mobile = this.state.base.loginInfo ? this.state.base.loginInfo.phone : null
    this.setState({form});
    this.validate();
  }
  onChooseContact() {
    return false
    Taro.chooseContact({
      success: ({ phoneNumber }) => {
        const form = this.state.form
        form.mobile = phoneNumber
        this.setState({form});
      },
    });
  }
  onSelectRechargeTypeItem = (e) => {
    if (e.currentTarget.dataset.index === this.state.rechargeType) {
      return;
    }
    this.setState({
      rechargeType: e.currentTarget.dataset.index,
    },() =>{
      this.getList();
    });


  }
  onClickRechargeItem = (e) => {
    const {selectRechargeItem} =this.state
    if (
      selectRechargeItem && selectRechargeItem.wxItem &&
      e.currentTarget.dataset.item.wxItem.id === selectRechargeItem.wxItem.id
    ) {
      return;
    }
    this.setState({
      selectRechargeItem: e.currentTarget.dataset.item,
    });
  }
  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    let btnColor = null;
    if (this.props.activityDetail.buttonColor !== 'default') {
      btnColor = this.props.activityDetail.buttonColor;
      this.setState({
        tmpStyle: Object.assign({}, templateStyle, { btnColor: btnColor }),
      });
    } else {
      this.setState({
        tmpStyle: templateStyle,
      });
    }
  }

  //立即充值
  onRechargeImmediately = async () =>  {
    if (!authHelper.checkAuth()) return;
    await this.validate();
    if (!this.state.formValidated.isValied) {
      Taro.showToast({
        title: this.state.formValidated.msg,
        icon: 'none',
        duration: 2000,
      });
      return;
    } else if (!this.state.selectRechargeItem) {
      Taro.showToast({
        title: '请选中权益卡',
        icon: 'none',
        duration: 2000,
      });
      return;
    }
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index',
      data: {
        itemNo: this.state.selectRechargeItem.wxItem.itemNo,
        mobile: this.state.form.mobile,
      },
    });
  }

  getList() {
    if(!this.state.rechargeList) return;
    wxApi
      .request({
        url: api.specialActivity.detail,
        loading: true,
        data: {
          partId: this.state.rechargeTypeList[this.state.rechargeType].partId,
        },
      })
      .then((res) => {
        this.setState({
          rechargeList: res.data,
          selectRechargeItem: res.data ? res.data[0] : null,
        });
      });
  }

  validate =() => {
    const _that = this
    return new Promise((resolve) =>{
      let isValied = true;
      let msg = '';
      for (const key in rules) {
        const value = this.state.form[key];
        const rule = rules[key];

        for (const r of rule) {
          if (r.required && !value) {
            isValied = false;
            msg = r.message;
            break;
          } else if (r.reg && !r.reg.test(value)) {
            isValied = false;
            msg = r.message;
            break;
          }
        }
      }
      if (!isValied) {
        _that.setState({
          formValidated: {
            isValied: false,
            msg,
          },
        },() =>{
          resolve(false) ;
        });
        return
      }
      this.setState({
        formValidated: {
          isValied: true,
          msg: '验证通过',
        },
      },() =>{
        resolve(true)
      });
    })

  }
  // 根据后台设置配置支付
  IosPayControl=()=>{
    if (this.props.iosSetting.payType == iosPayTypeEnum.payType.JUST_LIKE_ANDROID.value) {
      this.onRechargeImmediately()
      return false
    } else if (this.props.iosSetting.payType == iosPayTypeEnum.payType.H5_PAY.value) {
      return true
    } else if (this.props.iosSetting.payType == iosPayTypeEnum.payType.IOS_PAY_NONSUPPORT.value) {
      // 做弹窗操作
      this.showIospayLimitDialog()
      return false
    }
  }

  showIospayLimitDialog=()=>{
    this.iosPayLimitDialog.current.showIospayLimitDialog();
  }

  //立即下单
   onIosRechargeImmediately=async ()=>{
    if (!authHelper.checkAuth()) return
    if (!this.IosPayControl()) {
      return;
    }
    await this.validate()
    if(!this.state.formValidated.isValied){
      wxApi.showToast({
        title: this.state.formValidated.msg,
        icon: 'none',
        duration: 2000
      })
      return
    }else if(!this.state.selectRechargeItem ){
      wxApi.showToast({
        title: '请选中权益卡',
        icon: 'none',
        duration: 2000
      })
      return
    }
    wxApi.$navigateTo({
      url:"/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index",
      data:{
        itemNo:this.state.selectRechargeItem.wxItem.itemNo,
        mobile:this.state.form.mobile,
        iosPayNodes: this.props.iosSetting ? encodeURIComponent(this.props.iosSetting.text) : '',
        jumpType: this.props.iosSetting ? this.props.iosSetting.jumpType: '1'
      }
    })
  }

  render() {
    const {
      form,
      index,
      rechargeType,
      rechargeTypeList,
      tmpStyle,
      display,
      selectRechargeItem,
      rechargeList,
      tem,
      topicDesc,

    } = this.state;

    const {
      activityDetail,
      isIOS,
      iosSetting
    } =this.props;
    return (
      <View data-scoped="wk-pacr-RechargeVersion" className="wk-pacr-RechargeVersion recharge-version">
        <Image
          style={_safe_style_('display:block;width:100%')}
          mode="widthFix"
          src={activityDetail.topicImgUrl}
        ></Image>
        <Image
          style={_safe_style_('display:block;width:100%')}
          mode="widthFix"
          src={activityDetail.backGroudUrl}
        ></Image>
        <View className="body">
          <View className="recharge-mode-box">
            <View className="title">
              充值手机号
            </View>
            <View className="input-box">
              <Input
                onInput={this.onSetPhone.bind(this)}
                type='number'
                value={form.mobile}
                placeholderClass="input-placeholder-style"
                placeholder="请输入充值手机号"
                className="input"
              ></Input>
              <View
                onClick={this.onChooseContact}
                className="input-icon"
                style={_safe_style_(
                  "background-image:url('https://front-end-1302979015.file.myqcloud.com/images/c/images/tongxunluintouch01.svg');"
                )}
              ></View>
            </View>
          </View>
          <View className="recharge-type-box">
            <View className="title">充值类型</View>
            <View className="main">
              <ScrollView className="recharge-type-scroll-view" scrollX>
                {rechargeTypeList &&
                  rechargeTypeList.map((item, index) => {
                    return (
                      <View
                        onClick={_fixme_with_dataset_(this.onSelectRechargeTypeItem, { index: index })}
                        className={
                          rechargeType === index ? 'select-recharge-type-item recharge-type-item' : 'recharge-type-item'
                        }
                        key={index}
                      >
                        {item.partName}
                        {/*               <text style="border-bottom-color:{{tmpStyle.btnColor}};"  wx:if="{{rechargeType === index}}" class="indicator-block"></text> */}
                        {rechargeType === index && (
                          <Text
                            className="indicator-block"
                            style={_safe_style_('background-color:' + tmpStyle.btnColor + ';')}
                          ></Text>
                        )}

                        {/*               <image src="https://front-end-1302979015.file.myqcloud.com/images/c/images/sence-flag.png"></image> */}
                      </View>
                    );
                  })}
              </ScrollView>
              {rechargeList.length > 0 ? (
                <Block>
                  {display === 'leftRightSlide' ? (
                    <ScrollView scrollX className="left-right-slide">
                      <View style={_safe_style_('margin-top: 34rpx')}>
                        {rechargeList &&
                          rechargeList.map((item, index) => {
                            return (
                              <View
                                style={_safe_style_(
                                  activityDetail.itemBackGroud && activityDetail.backGroudType != 0
                                    ? 'background-image: url(' +
                                        activityDetail.itemBackGroud +
                                        ');background-position: center;background-size: auto;'
                                    : null
                                )}
                                onClick={_fixme_with_dataset_(this.onClickRechargeItem, { item: item })}
                                className={
                                  selectRechargeItem&& (selectRechargeItem.wxItem.id === item.wxItem.id)
                                    ? 'content-item select-content-item'
                                    : 'content-item'
                                }
                                key={index}
                              >
                                <Text style={_safe_style_('display: block;')} className="text">
                                  {item.wxItem.name}
                                </Text>
                                {item.wxItem.subName && (
                                  <Text
                                    style={_safe_style_(
                                      item.wxItem.subName
                                        ? 'background: linear-gradient(90deg,#ffb847, #f6d881);'
                                        : 'width:60rpx'
                                    )}
                                    className="subName"
                                  >
                                    {item.wxItem.subName || ''}
                                  </Text>
                                )}

                                <View className="price-box">
                                  <Text>￥</Text>
                                  <Text>{item.wxItem.salePrice / 100}</Text>
                                </View>
                                <Text className="text-line">{'￥' + item.wxItem.labelPrice / 100}</Text>
                                {item.wxItem.labelName && (
                                  <View className="snap-up">
                                    <View className='child'>{item.wxItem.labelName || '限时抢购'}</View>
                                  </View>
                                )}
                              </View>
                            );
                          })}
                      </View>
                    </ScrollView>
                  ) : display === 'OneRowThree' ? (
                    <View className="one-row-three">
                      {rechargeList &&
                        rechargeList.map((item, index) => {
                          return (
                            <View
                              style={_safe_style_(
                                activityDetail.itemBackGroud && activityDetail.backGroudType != 0
                                  ? 'background-image: url(' +
                                      activityDetail.itemBackGroud +
                                      ');background-position: center;background-size: auto;'
                                  : null
                              )}
                              onClick={_fixme_with_dataset_(this.onClickRechargeItem, { item: item })}
                              className={
                                selectRechargeItem.wxItem.id === item.wxItem.id
                                  ? 'content-item select-content-item'
                                  : 'content-item'
                              }
                              key={index}
                            >
                              <Text className="text">{item.wxItem.name}</Text>
                              {item.wxItem.subName && (
                                <Text
                                  style={_safe_style_(
                                    item.wxItem.subName
                                      ? 'background: linear-gradient(90deg,#ffb847, #f6d881);'
                                      : 'width:60rpx'
                                  )}
                                  className="subName"
                                >
                                  {item.wxItem.subName || ''}
                                </Text>
                              )}

                              <View className="price-box">
                                <Text>￥</Text>
                                <Text>{item.wxItem.salePrice / 100}</Text>
                              </View>
                              <Text className="text-line">{'￥' + item.wxItem.labelPrice / 100}</Text>
                              {item.wxItem.labelName && (
                                <View className="snap-up">
                                  <View className='child'>{item.wxItem.labelName || '限时抢购'}</View>
                                </View>
                              )}
                            </View>
                          );
                        })}
                    </View>
                  ) : (
                    display === 'OneRowTwo' && (
                      <View className="one-row-two">
                        {rechargeList &&
                          rechargeList.map((item, index) => {
                            return (
                              <View
                                style={_safe_style_(
                                  activityDetail.itemBackGroud && activityDetail.backGroudType != 0
                                    ? 'background-image: url(' +
                                        activityDetail.itemBackGroud +
                                        ');background-position: center;background-size: auto;'
                                    : null
                                )}
                                onClick={_fixme_with_dataset_(this.onClickRechargeItem, { item: item })}
                                className={
                                  selectRechargeItem.wxItem.id === item.wxItem.id
                                    ? 'content-item select-content-item'
                                    : 'content-item'
                                }
                                key={index}
                              >
                                <Text className="text">{item.wxItem.name}</Text>
                                {item.wxItem.subName && (
                                  <Text
                                    style={_safe_style_(
                                      item.wxItem.subName
                                        ? 'background: linear-gradient(90deg,#ffb847, #f6d881);'
                                        : 'width:60rpx'
                                    )}
                                    className="subName"
                                  >
                                    {item.wxItem.subName || ''}
                                  </Text>
                                )}

                                <View className="price-box">
                                  <Text>￥</Text>
                                  <Text>{item.wxItem.salePrice / 100}</Text>
                                </View>
                                <Text className="text-line">{'￥' + item.wxItem.labelPrice / 100}</Text>
                                {item.wxItem.labelName && (
                                  <View className="snap-up">
                                    <View className='child'>{item.wxItem.labelName || '限时抢购'}</View>
                                  </View>
                                )}
                              </View>
                            );
                          })}
                      </View>
                    )
                  )}
                </Block>
              ) : (
                <View className="null-data-box">
                  <Image
                    className="non-existent"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/non-existent.svg"
                    mode="scaleToFill"
                  ></Image>
                </View>
              )}
            </View>
          </View>
          {topicDesc && (
            <View className="info">
              <RichText
                nodes={topicDesc}
                style={_safe_style_('color:#666;text-align: justify')}
              ></RichText>
            </View>
          )}
        </View>
        {
          !isIOS ? <View
          onClick={this.onRechargeImmediately}
          style={tmpStyle ? _safe_style_(
            'background:' +
              tmpStyle.btnColor +
              ';box-shadow: 0rpx 16rpx 40rpx 0rpx ' +
              filters.handleOpacity(tmpStyle.btnColor, '0.48') +
              ';'
          ) :null}
          className="btn"
        >
          立即充值
          <Image
            className="btn-image"
            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon-backLink.svg"
          ></Image>
          </View> : <View onClick={this.onIosRechargeImmediately} style={tmpStyle ? _safe_style_('background:' + tmpStyle.btnColor +';box-shadow:0rpx 16rpx 40rpx 0rpx' + filters.handleOpacity(tmpStyle.btnColor,'0.48') + ';') : null} className="btn">
            {iosSetting && iosSetting.buttonCopywriting ? iosSetting.buttonCopywriting:'去购买'}
            <Image className="btn-image" src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon-backLink.svg"></Image>
        </View>
        }
        <IosPayLimitDialog tipsCopywriting={iosSetting?.tipsCopywriting} ref={this.iosPayLimitDialog}></IosPayLimitDialog>
      </View>
    );
  }
}

export default RechargeVersion;
