import { $getRouter } from 'wk-taro-platform'; /* eslint-disable react/sort-comp */
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import React, { ComponentClass } from 'react';
import Taro from '@tarojs/taro';
import {Fragment} from 'react'
import { connect } from 'react-redux';
import store from '@/store/index';
import hoc from '@/hoc';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import imageUtils from '@/wxat-common/utils/image.js';
import report from '@/sdks/buried/report';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import displayEnum from '@/wxat-common/constants/displayEnum';
import Empty from '@/wxat-common/components/empty/empty';
import HoverCart from '@/wxat-common/components/cart/hover-cart/index';
import GoodsItem from '@/wxat-common/components/industry-decorate-style/goods-item';
import TopMenu from '@/wxat-common/components/top-menu/index';
import ActivityTemplate from '@/sub-packages/marketing-package/components/activity-template';
import CustomerHeader from '@/wxat-common/components/customer-header/index';
import RechargeVersion from '@/sub-packages/marketing-package/pages/activity/components/recharge-version';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog';

import './index.scss';

const app = store.getState();
const reportSource = reportConstants.SOURCE_TYPE.activity.key;

type PageStateProps = {
  industry: string;
};

type PageDispatchProps = {};

type IProps = PageStateProps & PageDispatchProps;

interface ActivityDetail {
  fontColor: string;
  topicColor: string;
  topicImgUrl: string;
  topicName: string;
  topicId: number;
  topicPartitions: Array<Record<string, any>>;
}

type PageState = {
  activityDetail: ActivityDetail;
  background: string;
  topicId: number;
  goodsList: Array<Record<string, any>>;
  isTop: boolean;
  display: string;
  itemClass: string | undefined;
  containerClass: string | undefined;
  navOpacity:number | 0;
  navBgColor: any | null;
  isEmpty: Boolean
};

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class ActivityPage extends React.Component<IProps, PageState> {
  $router = $getRouter();
  state = {
    background: '#fff',
    topicId: 0,
    activityDetail: {
      fontColor: '',
      topicColor: '',
      topicImgUrl: '',
      topicName: '',
      topicId: 0,
      topicPartitions: [],
    },
    isEmpty: false,
    goodsList: [],
    isTop: false,
    display: '',
    itemClass: '',
    containerClass: '',
    navOpacity:0,
    navBgColor:null,
    topicType:0,//专题活动类型  0 -默认 1-充值 2-模板活动
  };

  onPageScroll(res) {
    let height = (app.globalData.homeConfig && app.globalData.homeConfig[1].config.height / 2 )|| 60;
    this.setState({
      navOpacity: res.scrollTop / height
    });

    if (res.scrollTop > 300) {
      this.setState({
        isTop: true,
      });
    } else {
      this.setState({
        isTop: false,
      });
    }
  }

  componentDidShow() {
    const { activityDetail } = this.state;
    const { topicName } = activityDetail || {};
    !!topicName && Taro.setNavigationBarTitle({ title: topicName || '充值版' });
  }

  componentDidMount() {
    const { topicId, display } = this.$router.params;
    if (!!topicId) {
      report.detailPage(topicId);
    }

    const _display = display || 'vertical';

    const itemClass = displayEnum.getClassByShowType('item', _display);
    const containerClass = displayEnum.getClassByShowType('container', _display);
    this.setState(
      {
        itemClass,
        containerClass,
        topicId: topicId ? Number(topicId) : 6,
        display: _display,
      },

      () => {
        this.getpartitions();
      }
    );
  }

  getpartitions() {
    wxApi
      .request({
        url: api.specialActivity.partitions,
        loading: true,
        data: {
          topicId: this.state.topicId,
        },
      })
      .then((res) => {
        const { data } = res;
        if (data === null) {
          this.setState(
            {
              isEmpty: true
            }
            )
            return false
          }
        if (!!data && !!data.topicPartitions && data.topicPartitions.length > 0) {
          this.getList(data.topicPartitions[0].partId);
        }
        this.setState(
          {
            activityDetail: data,
            navBgColor: app.globalData.tabbars.list[0].navBackgroundColor,
            topicType: data.topicType
          },

          () => {
            this.getPageConfig();
          }
        );
      })
  }

  getPageConfig() {
    const { activityDetail } = this.state;
    const { topicName, fontColor, topicColor } = activityDetail || {};
    Taro.setNavigationBarTitle({ title: topicName || '充值版' });
    if (!!topicColor)
      wxApi.setNavigationBarColor({
        frontColor: fontColor === "#000000" ? "#000000" : '#ffffff', // 必写项
        backgroundColor: topicColor ? topicColor :'#ffffff' // 必写项
      });
  }

  getList = (partId) => {
    this.setState(
      {
        goodsList: []
      },
      () => {
        wxApi
        .request({
          url: api.specialActivity.detail,
          loading: true,
          data: {
            partId,
          },
        })
        .then((res) => {
          const goodsList = res.data || [];
          goodsList.forEach((item) => {
            item.thumbnailMin = imageUtils.thumbnailMid(item.thumbnail || (item.wxItem ? item.wxItem.thumbnail : ''));
          });
          this.setState({
            goodsList,
          });
        })
      }
    )

  };

  fakeData(goodsList) {
    goodsList.forEach((item) => {
      item.activityTags = [
        {
          name: '折扣',
          tagCss: 'background:white;color:red;border:1rpx solid red',
        },

        {
          name: '实惠',
          tagCss: '',
        },

        {
          name: '满减',
          tagCss: '',
        },
      ];

      item.goodsTags = [
        {
          name: '5元/片',
          tagCss: 'background:white;color:rgba(163,163,163,1);border:1rpx solid rgba(163,163,163,1)',
        },

        {
          name: '5g/包',
          tagCss: 'background:white;color:red;border:1rpx solid red',
        },

        {
          name: '超级优惠',
          tagCss: 'background:white;color:rgba(163,163,163,1);border:1rpx solid rgba(163,163,163,1)',
        },
      ];
    });
  }

  render() {
    const { isTop, activityDetail, containerClass, itemClass, display, goodsList, navOpacity, navBgColor,topicType, topicId, isEmpty } = this.state;

    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-smpa-Activity" className="wk-smpa-Activity">
        <CustomerHeader navOpacity={navOpacity} title={activityDetail.topicName} bgColor={navBgColor}></CustomerHeader>
        <View className="activitiy">
          {topicType === 0 && (
            <Fragment>
              <View className={'fixed ' + (isTop ? 'top' : '')}>
                {activityDetail.topicImgUrl && (
                  <View className="activitiy-img">
                    <Image src={activityDetail.topicImgUrl}></Image>
                  </View>
                )}

                {activityDetail.topicPartitions && (
                  <TopMenu
                    color={activityDetail.topicColor}
                    list={activityDetail.topicPartitions}
                    onTabClick={this.getList}
                  ></TopMenu>
                )}
              </View>
              <View className={'goods-list ' + containerClass}>
                {goodsList &&
                  goodsList.map((item, index) => {
                    return (
                      <GoodsItem
                        className={itemClass}
                        display={display}
                        reportSource={reportSource}
                        goodsItem={item}
                        key={'goods-item-' + index}
                        showDivider={index !== goodsList.length - 1}
                      ></GoodsItem>
                    );
                  })}
              </View>
            </Fragment>
          )}

          {
            isEmpty && <Empty  message='暂无活动~' />
          }

          {topicType === 1 && (
            <RechargeVersion activityDetail={activityDetail}/>
          )}
        </View>
        {topicType === 2 && <ActivityTemplate activityDetail={activityDetail}/>}

        {topicType === 0 && <HoverCart isPageShow={true}></HoverCart>}

        {
          !!topicId && <HomeActivityDialog
          showPage={'sub-packages/marketing-package/pages/activity/index?topicId=' + topicId}
          ></HomeActivityDialog>
        }
      </View>
    );
  }
}

export default ActivityPage as ComponentClass;
