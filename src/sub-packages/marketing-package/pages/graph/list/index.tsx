import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Image, Text, ScrollView} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';

import store from '@/store/index';
import constants from '@/wxat-common/constants/index';
import graphEnum from '@/wxat-common/constants/graphEnum';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import utils from '@/wxat-common/utils/util';
import state from '@/state/index';
import report from '@/sdks/buried/report/index';

import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
const loadMoreStatus = constants.order.loadMoreStatus;
const app = store.getState();

interface listProps {

}

interface listState {
  graphicStyle: string,
  categroyId: string,
  graphList: any[],
  pageNo: number | string,
  pageSize: number | string,
  hasMore: boolean,
  loadMoreStatus: any,
  dialogShow: boolean,
  isFullScreen: boolean,
}

class List extends React.Component<listProps, listState> {
  state = {
    graphicStyle: 'title',
    categroyId: '',
    graphList: [],
    pageNo: 1,
    pageSize: 6,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    dialogShow: false,
    isFullScreen: !!app.globalData.phoneInfo && app.globalData.phoneInfo.isFullScreenDevice,
  }

  getList() {
    this.setState({ loadMoreStatus: loadMoreStatus.LOADING });
    const params = {
      // per_appId: state.base.currentStore.appId,
      categoryId: this.state.categoryId,
      pageNo: this.state.pageNo,
      pageSize: this.state.pageSize,
      deleteFlag: 'N',
      status: graphEnum.STATUS.ON_SHELVE.value,
    };

    return wxApi
      .request({
        url: api.article.list,
        data: params,
        loading: true,
      })
      .then((res) => {
        const { data } = res || {};
        const xData = res.data || [];
        xData.forEach((element) => {
          report.videoExposure(element);
        });
        this.setState({
          loadMoreStatus: loadMoreStatus.HIDE,
          graphList: this.state.graphList.concat(xData),
          hasMore: data.length === 6,
        });
      })
      .catch(() => {
        this.setState({
          loadMoreStatus: loadMoreStatus.ERROR,
        });
      });
  }

  onLoad(options) {
    const title = (options || {}).title || '营销图文';
    let categroy = (options || {}).categroy || '';
    console.log(categroy, '---------------');

    const graphicStyle = (options || {}).graphicStyle || 'title';
    Taro.setNavigationBarTitle({
      title: title,
    });

    this.setState({
      categoryId: categroy,
      graphicStyle,
    });

    // 品牌代言人相关
    if (options.promotion) {
      this.taskCenterFunction(options);
    } else if (options.scene) {
      const scene = '?' + decodeURIComponent(options.scene);
      const taskReferId = utils.getQueryString(scene, 'r');
      const shareId = utils.getQueryString(scene, 's');
      const taskType = utils.getQueryString(scene, 't');
      categroy = utils.getQueryString(scene, 'c');
      if (categroy) {
        this.setState({
          categoryId: categroy,
        });
      }
      this.taskCenterFunction({
        promotion: 1,
        targetType: 1,
        taskReferId,
        taskType,
        shareId,
      });
    }
    // 刷新现在获取的 所有图文列表（修改哎pages）
    // 还原 pages
    // 修复返回重新请求页面
    const { pageNo, pageSize, graphList } = this.state;
    this.setState({
      pageNo: 1,
      pageSize: (graphList || []).length || pageSize,
      graphList: [],
    }, () =>{
      this.getList().then(() => {
        this.setState({ pageNo, pageSize });
      });
    });
  }

  jump = (e) => {
    // 上报营销图文的对应的图文点击量
    report.clickVideoGraphic(e.currentTarget.dataset);
    // if (!authHelper.checkAuth()) return;
    const id = e.currentTarget.dataset.id;
    const type = e.currentTarget.dataset.type;
    const index = e.currentTarget.dataset.index;
    if (type == 2) {
      wxApi.$navigateTo({
        url: `/sub-packages/marketing-package/pages/graph/video-detail/index`,
        data: {
          id,
          pageNo: Math.ceil((index + 1) / 6),
          categoryId: this.state.categoryId,
          deleteFlag: 'N',
          status: graphEnum.STATUS.ON_SHELVE.value,
        },
      });
    } else {
      wxApi.$navigateTo({
        url: `/sub-packages/moveFile-package/pages/graph/detail/index?id=${id}`,
      });
    }
  }

  onPullDownRefresh() {
    Taro.stopPullDownRefresh();
    this.setState({ pageNo: 1, pageSize: 6, graphList: [] }, () => {
      this.getList();
    });
  }

  onReachBottom() {
    if (!this.state.hasMore) {
      Taro.showToast({
        title: '没有更多数据了！',
        icon: 'none',
        duration: 1500,
      });
    } else {
      // 上报上拉刷新
      report.pullDown();
    }
    if (this.state.loadMoreStatus !== loadMoreStatus.LOADING && this.state.hasMore) {
      this.setState(
        {
          data: ++this.state.pageNo,
        },

        () => {
          this.getList();
        }
      );
    }
  }

  onRetryLoadMore() {
    this.onReachBottom();
  }

  onShareAppMessage() {
    return { title: '营销图文' };
  }

  // 任务中心相关功能
  taskCenterFunction(options) {
    const { promotion, targetType, taskType, shareId } = options;
    const newUser = options.taskType == 1;
    state.base.newUser = newUser;
    state.base.promotion = promotion;
    state.base.taskType = taskType;
    state.base.targetType = targetType;
    state.base.shareId = shareId;
    state.base.isFirst = true;
  }

  render() {
    const { isFullScreen, graphList, hasMore, loadMoreStatus, categroyId } = this.state;
    return (
      <ScrollView
        scrollY
        data-fixme="02 block to view. need more test" data-scoped="wk-mpgl-List" className="wk-mpgl-List">
        {graphList.length > 0 && (
          <View className={'graph-list ' + (isFullScreen ? 'fix-full-screen' : '')}>
            <View className="waterfallBox">
              <View className="waterfall">
                <View className="waterfall-list" style={_safe_style_('padding-left:10rpx;')}>
                  {!!graphList &&
                    graphList.map((item, index) => {
                      return (
                        <View key={item.index} data-item={item} className="waterfall-list-item">
                          {index % 2 == 0 && (
                            <View
                              className="waterfall-item"
                              onClick={_fixme_with_dataset_(this.jump, {
                                id: item.id,
                                index: index,
                                type: item.articleType,
                                mediaType: item.mediaType ? item.mediaType : '',
                              })}
                            >
                              <View className="waterfall-content">
                                <View className="waterfall-imgBox">
                                  <Image
                                    className="waterfall-item-image"
                                    mode="widthFix"
                                    src={item.coverUrl}
                                  ></Image>
                                  {item.articleType == 2 && (
                                    <View className="waterfall-video">
                                      <Image
                                        className="waterfall-video-img"
                                        mode="widthFix"
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_video.svg"
                                      ></Image>
                                    </View>
                                  )}

                                  {item.labelPosition && (
                                    <View className="waterfall-location">
                                      <Image
                                        className="location-img"
                                        mode="widthFix"
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/dingwei.svg"
                                      ></Image>
                                      <Text className="txt">{item.labelPosition}</Text>
                                    </View>
                                  )}
                                </View>
                                <View className="waterfall-text-content">
                                  <View className="waterfall-item-title">{item.title}</View>
                                  {item.labelOrdinary && (
                                    <View className="waterfall-item-tag">
                                      {filters.imgArrFormat(item.labelOrdinary) &&
                                        filters.imgArrFormat(item.labelOrdinary).map((item, index) => {
                                          return (
                                            <View key={item.index} data-item={item} className="waterfall-item-tag-v">
                                              <Text className="waterfall-item-tag-t">{item}</Text>
                                            </View>
                                          );
                                        })}
                                    </View>
                                  )}

                                  <View className="waterfall-item-comment">
                                    {item.labelPrice && (
                                      <View className="price">
                                        <Text className="priceNumber">{item.labelPrice}</Text>
                                      </View>
                                    )}

                                    {item.totalLikeCount && (
                                      <View className="totalLikeCount">
                                        <Image
                                          className="img"
                                          mode="widthFix"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_ready.svg"
                                        ></Image>
                                        <Text className="txt">{item.totalLikeCount}</Text>
                                      </View>
                                    )}
                                  </View>
                                </View>
                              </View>
                            </View>
                          )}
                        </View>
                      );
                    })}
                </View>
                <View className="waterfall-list" style={_safe_style_('padding-right:10rpx;')}>
                  {!!graphList &&
                    graphList.map((item, index) => {
                      return (
                        <View key={item.index} data-item={item} className="waterfall-list-item">
                          {index % 2 == 1 && (
                            <View
                              className="waterfall-item"
                              onClick={_fixme_with_dataset_(this.jump, {
                                id: item.id,
                                type: item.articleType,
                                mediaType: item.mediaType ? item.mediaType : '',
                                index: index,
                              })}
                            >
                              <View className="waterfall-content">
                                <View className="waterfall-imgBox">
                                  <Image
                                    className="waterfall-item-image"
                                    mode="widthFix"
                                    src={item.coverUrl}
                                  ></Image>
                                  {item.articleType == 2 && (
                                    <View className="waterfall-video">
                                      <Image
                                        className="waterfall-video-img"
                                        mode="widthFix"
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_video.svg"
                                      ></Image>
                                    </View>
                                  )}

                                  {!!item.labelPosition && (
                                    <View className="waterfall-location">
                                      <Image
                                        className="location-img"
                                        mode="widthFix"
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/dingwei.svg"
                                      ></Image>
                                      <Text>{item.labelPosition}</Text>
                                    </View>
                                  )}
                                </View>
                                <View className="waterfall-text-content">
                                  <View className="waterfall-item-title">{item.title}</View>
                                  {item.labelOrdinary && (
                                    <View className="waterfall-item-tag">
                                      {filters.imgArrFormat(item.labelOrdinary) &&
                                        filters.imgArrFormat(item.labelOrdinary).map((item, index) => {
                                          return (
                                            <View key={item.index} data-item={item} className="waterfall-item-tag-v">
                                              <Text className="waterfall-item-tag-t">{item}</Text>
                                            </View>
                                          );
                                        })}
                                    </View>
                                  )}

                                  <View className="waterfall-item-comment">
                                    {item.labelPrice && (
                                      <View className="price">
                                        <Text className="priceNumber">{item.labelPrice}</Text>
                                      </View>
                                    )}

                                    {item.totalLikeCount && (
                                      <View className="totalLikeCount">
                                        <Image
                                          className="img"
                                          mode="widthFix"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_ready.svg"
                                        ></Image>
                                        <Text className="txt">{item.totalLikeCount}</Text>
                                      </View>
                                    )}
                                  </View>
                                </View>
                              </View>
                            </View>
                          )}
                        </View>
                      );
                    })}
                </View>
              </View>
            </View>
          </View>
        )}

        {graphList.length === 0 && !hasMore && <Empty message="敬请期待..."></Empty>}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        <HomeActivityDialog showPage="sub-packages/moveFile-package/pages/graph/list/index"></HomeActivityDialog>
        <HomeActivityDialog
          showPage={'sub-packages/moveFile-package/pages/graph/list/index?categroy=' + categroyId}
        ></HomeActivityDialog>
      </ScrollView>
    );
  }
}

export default List;
