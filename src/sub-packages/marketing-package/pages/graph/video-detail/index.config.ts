export default {
  navigationBarTitleText: ' ',
  navigationBarTextStyle: 'white',
  enableShareAppMessage: true,
  backgroundColor: '#F2F6F7',
  navigationStyle: 'custom',
};
