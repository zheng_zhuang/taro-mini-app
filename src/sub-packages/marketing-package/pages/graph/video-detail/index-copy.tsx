import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Swiper, SwiperItem, Video, CoverImage, CoverView, Button, Image } from '@tarojs/components';
import React, { ComponentClass, createRef }  from 'react';
import Taro from '@tarojs/taro';
// import { $getRouter } from 'wk-taro-platform';
import filters from '@/wxat-common/utils/money.wxs';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import state from '@/state/index';
import authHelper from '@/wxat-common/utils/auth-helper';
import shareUtil from '@/wxat-common/utils/share';
import template from '@/wxat-common/utils/template';
import report from '@/sdks/buried/report/index';
import graphEnum from '@/wxat-common/constants/graphEnum';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import ShareDialog from '@/wxat-common/components/share-dialog/index';
import fn from '@/imports/fn0';
import { connect } from 'react-redux';
import './index.scss';

interface VideoDetailProps {
  sessionId: string
}

interface VideoDetailState {
  id: any, // 传进来的id
  detailList: any[],
  dataLoad: boolean,
  tmpStyle: any,
  play: boolean, // 是否播放
  current: any, // 当前视频位置
  duration: any, // 视频总时长
  currentTime: any, // 当前播放时长
  palyCount: any, // 循环次数
  statusBarHeight: any, // 返回按钮top距离
  showDialog: boolean,
  totalList: any[],
  isFirst: boolean,
  isAndroid: boolean,
  toAddComment: boolean,
  pageNo: number,
  categoryId: any,
  deleteFlag: any,
  status: any,
  ids: any,
}

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class VideoDetail extends React.Component<VideoDetailProps, VideoDetailState> {
  // $router = $getRouter();
  state = {
    id: null, // 传进来的id
    detailList: [],
    dataLoad: false,
    tmpStyle: null,
    play: true, // 是否播放
    current: 0, // 当前视频位置
    duration: 0, // 视频总时长
    currentTime: 0, // 当前播放时长
    palyCount: 0, // 循环次数
    statusBarHeight: 0, // 返回按钮top距离
    showDialog: false,
    totalList: [],
    isFirst: true,
    isAndroid: false,
    toAddComment: true,
    pageNo: 1,
    categoryId: null,
    deleteFlag: null,
    status: null,
    ids: null,
    refShareDialogCMTP: React.createRef(),
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.initRenderData();
    }
  }
  componentDidCatch(error, errorInfo) {
    console.log('==============',error,errorInfo);
  }
  /**
   * 生命周期函数--监听页面卸载
   */
  componentWillUnmount() {
  // onUnload() {
    report.leaveCurrentVideo(this.state);
  }

  onLoad(options) {
    this.setState({
      id: options.id,
      toAddComment: false,
      pageNo: options.pageNo,
      categoryId: options.categoryId,
      deleteFlag: options.deleteFlag,
      status: options.status,
      ids: options.ids ? JSON.parse(options.ids) : null
    }, () =>{
      if (!!state.base.sessionId || !!this.props.sessionId) {
        this.initRenderData();
      }
    });

    // 胶囊按钮位置信息
    const menuButtonInfo = process.env.TARO_ENV === 'weapp' ? Taro.getMenuButtonBoundingClientRect() : {top: 10};
    const statusBarHeight = menuButtonInfo.top + 5;
    this.setState({
      statusBarHeight,
      isAndroid: Taro.getSystemInfoSync().platform === 'android',
    });
  }

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // 上报视频播放时长和总时间
    report.leaveCurrentVideo(this.state);
  }


  initRenderData() {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({ dataLoad: true });
    // this.getArticle();

    if (this.state.ids) {
      this.fetchData();
    }
    if (this.state.pageNo) {
      this.getMoreList();
    }
    if (this.state.id && !this.state.ids && !this.state.pageNo) {
      this.getArticle();
    }
  }

  // 获取文章详情
  getArticle() {
    wxApi
      .request({
        url: api.article.detail,
        data: {
          id: this.state.id,
        },

        loading: true,
      })
      .then((res) => {
        // wxApi.stopPullDownRefresh();
        if (res.success) {
          const detailObj = {
            id: res.data.id,
            title: res.data.title,
            content: res.data.content,
            likeCount: res.data.totalLikeCount || 0,
            virtualLikeCount: res.data.virtualLikeCount || 0,
            liked: res.data.liked || false, // 是否点赞:
            goodsList: res.data.articleItemList || [], // 商品列表
            digest: res.data.digest, // 摘要
            shortDigest: '', // 短摘要
            showToggle: false, // 是否显示展开全文按钮
            isAll: false, // 是否显示全文
            showGood: true, // 是否显示第一个商品

            // 商品曝光
          };
          if (!!detailObj.goodsList && detailObj.goodsList.length > 0) {
            report.commodityExposure(detailObj.goodsList[0]);
          }
          // 判断商品标题超出两行显示。。。
          detailObj.goodsList.forEach((item) => {
            if (item.fitemName.length > 54) {
              item.fitemName = item.fitemName.slice(0, 50) + '...';
            }
          });

          // 判断是否缩略摘要
          if (res.data.digest.length > 54) {
            detailObj.shortDigest = res.data.digest.slice(0, 50) + '...';
            detailObj.showToggle = true;
          }
          const detailList: any[] = this.state.detailList;
          detailList.push(detailObj);

          this.setState({
            detailList,
          });

          this.getArticleStatistics(this.state.id);
          // 获取video
          Taro.nextTick(() => {
            this.palyVideo();
          });
        }
      })
  }

  setShortDigest() {
    if (this.state.digest.length > 54) {
      const shortDigest = this.state.digest.slice(0, 50) + '...';
      this.setState({
        shortDigest,
        showToggle: true,
      });
    }
  }

  // 点赞
  onChangeLike = (e) => {
    e.preventDefault();
    e.stopPropagation();
    report.likes(this.state);
    if (!authHelper.checkAuth()) {
      return;
    }
    const current = this.state.current;
    wxApi
      .request({
        url: api.article.changeLike,
        method: 'POST',
        data: {
          generalId: +this.state.detailList[current].id,
          likeType: 1,
          status: +!this.state.detailList[current].liked,
        },

        loading: true,
      })
      .then((res) => {
        if (res.success) {
          const detailList = this.state.detailList;
          detailList[current].likeCount = detailList[current].liked
            ? detailList[current].likeCount - 1
            : detailList[current].likeCount + 1;
          detailList[current].virtualLikeCount = detailList[current].liked
            ? detailList[current].virtualLikeCount - 1
            : detailList[current].virtualLikeCount + 1;
          detailList[current].liked = !detailList[current].liked;
          this.setState({
            detailList,
          });
        }
      });
  }

  // 文章分享
  setShareConfig=(e)=>{
    e.preventDefault();
    e.stopPropagation();
    console.log(this.state.refShareDialogCMTP)
    this.state.refShareDialogCMTP.current ? this.state.refShareDialogCMTP.current.show() : null;
  }

  onShareAppMessage () {
    report.share(true, this.state);
    const { current, detailList } = this.state;
    const title =  detailList[current].title;
    const imgUrl = detailList[current].coverUrl;
    const path = shareUtil.buildShareUrlPublicArguments({
        url: `sub-packages/marketing-package/pages/graph/video-detail/index?id=${this.state.detailList[current].id}`,
        bz: shareUtil.ShareBZs.GRAPH_DETAIL
    });
    console.log('sharePath => ', path);
    return { title, path,imgUrl };
  };

  onChangeShowAll = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const current = this.state.current;
    const detailList = this.state.detailList;
    detailList[current].isAll = !detailList[current].isAll;
    this.setState({
      detailList,
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 点击暂停视频
  videoTap = () => {
    this.toggleVideoPlay()
  }

  toggleVideoPlay () {
    if (this.state.play) {
      this.pauseVideo()
    } else {
      this.palyVideo()
    }
  }

  toggleVideo=(e)=>{
    console.log(123)
    e.preventDefault();
    e.stopPropagation();
    this.toggleVideoPlay()
  }

  // 开始播放
  palyVideo(){
    let videoContext;
    if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
       videoContext = document.getElementById('myVideo' + this.state.current);
    } else {
       videoContext = Taro.createVideoContext('myVideo' + this.state.current);
    }
    videoContext.play();
    this.setState({
      play: true,
    });
  }
  // 暂停播放
  pauseVideo(){
    let videoContext;
    if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
       videoContext = document.getElementById('myVideo' + this.state.current);
    } else {
       videoContext = Taro.createVideoContext('myVideo' + this.state.current);
    }
    videoContext.pause();
    this.setState({
      play: false,
    });
  }

  // 切换视频
  changeSwiper = (e) => {
    if (!this.state.detailList.length) return false
    if (!this.state.detailList[e.detail.current]) return false
    // if (e.type !== 'autoplay' && e.type != 'touch') return false;
    console.log('changeSwiper==', e.detail.current,this.state.current);
    this.setState({
      play: true,
    });
    if(e.detail.current == this.state.current ||  e.detail.current == this.state.detailList.length) {
      this.setState({
        play: true,
        duration: 0, // 视频总时长
        currentTime: 0, // 当前播放时长
        palyCount: 0, // 循环次数
      });
      this.getArticleStatistics(this.state.detailList[e.detail.current].id);
      if (!this.state.pageNo) return;
      if (e.detail.current + 1 >= this.state.detailList.length) {
        this.setState({
          pageNo: ++this.state.pageNo,
        });

        this.getMoreList();
      }
      return
    }
    // 暂停上一个视频
    /*   const videoContext = wx.createVideoContext('myVideo' + this.state.current);
       videoContext.pause()//暂停播放 */
    this.setState({
      current: e.detail.current,
    },()=>{
      Taro.nextTick(() => {
        this.palyVideo();
      });
    });
  }

  bindtimeupdate = (e) => {
    const currentTime = Math.floor(e.detail.currentTime); // 当前播放时长
    const duration = Math.floor(e.detail.duration); // 视频总时长
    this.setState({
      currentTime,
      duration,
    });
  }

  // 播放结束
  bindended = ()=> {
    this.setState({
      palyCount: this.state.palyCount + 1,
    });
  }

  clickGoods = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const current = this.state.current;
    if (!this.state.detailList[current].goodsList.length) return;
    // 商品曝光量
    this.state.detailList[current].goodsList.forEach((element) => {
      report.commodityExposure(element);
    });
    // 弹出商品列表弹窗
    // this.selectComponent("#animat-dialog").show({
    //   translateY: 300
    // })
    this.setState({
      showDialog: true,
    });
  }

  onClickMaskHide = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      showDialog: false,
    });
  }

  //
  goToGoodDetail = (e) => {
    const index = e.currentTarget.dataset.index;
    const current = this.state.current;
    const item = this.state.detailList[current].goodsList[index];
    // 商品点击量
    report.clickCommodity(item);
    switch (item.fitemType) {
      case 1:
        // 普通商品
        wxApi.$navigateTo({
          url: 'wxat-common/pages/goods-detail/index',
          data: {
            itemNo: item.fitemNo,
            source: 'classify',
          },
        });

        break;
      case 2:
        // 组合商品
        wxApi.$navigateTo({
          url: 'wxat-common/pages/goods-detail/index',
          data: {
            itemNo: item.fitemNo,
            source: 'classify',
          },
        });

        break;
      case 41:
        // 虚拟权益卡
        wxApi.$navigateTo({
          url: 'sub-packages/marketing-package/pages/virtual-goods/detail/index',
          data: {
            itemNo: item.fitemNo,
          },
        });

        break;
      case 48:
        // MMS商品
        Taro.navigateToMiniProgram({
          appId: item.fwxAppId,
          path: item.fjumpUrl + item.fitemNo,
        });

        break;
    }
  }

  bindtransition = (e) => {
    /*
      if(e.detail.dy > 200) {
        let index = this.state.totalList.findIndex(e => e.id === this.state.id );
        this.setState({
          id: this.state.totalList[index + 1].id
        })
        this.getArticle()
      } */
  }

  bindanimationfinish = (e) => {
    /* if(e.detail.current + 1 === this.state.detailList.length ) {
       wx.showToast({
         icon: 'none',
         title: '到底了'
       })
     } */
  }

  handleBack = () => {
    if (Taro.getCurrentPages().length > 1) {
      Taro.navigateBack();
    } else {
      Taro.switchTab({
        url: '/wxat-common/pages/home/index',
      });
    }
  }

  handleClose = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const current = this.state.current;
    const detailList = this.state.detailList;
    detailList[current].showGood = false;
    this.setState({
      detailList,
    });
  }

  getList(ids) {
    if (!ids.length) {
      this.setState({ graphList: [] });
      return;
    }
    // Request URL: https://wkb.wakedata.com/cs/auth/market_article/query_available?ids=123,234,567
    // Response:  与 /market_article/query/list的一致， 屏蔽掉content字端，屏蔽已删除已下架的文章
    wxApi
      .request({
        url: api.article.availableList,
        quite: true,
        data: {
          ids,
        },
      })
      .then((res) => {
        let { success, data } = res;
        if (!res.success) {
          return;
        }
        const index = data.findIndex((e) => e.id == this.state.id);
        data.splice(0, index);
        data = data.filter((e) => e.articleType === 2);
        data.forEach((item) => {
          const detailObj = {
            id: item.id,
            title: item.title,
            content: item.content,
            likeCount: item.totalLikeCount || 0,
            virtualLikeCount: item.virtualLikeCount || 0,
            liked: item.liked || false, // 是否点赞:
            goodsList: item.articleItemList || [], // 商品列表
            digest: item.digest, // 摘要
            shortDigest: '', // 短摘要
            showToggle: false, // 是否显示展开全文按钮
            isAll: false, // 是否显示全文
            showGood: true, // 是否显示第一个商品

            // 商品曝光
          };
          if (!!detailObj.goodsList && detailObj.goodsList.length > 0) {
            report.commodityExposure(detailObj.goodsList[0]);
          }
          // 判断商品标题超出两行显示。。。
          detailObj.goodsList.forEach((item) => {
            if (item.fitemName.length > 54) {
              item.fitemName = item.fitemName.slice(0, 50) + '...';
            }
          });

          // 判断是否缩略摘要
          if (item.digest.length > 54) {
            detailObj.shortDigest = item.digest.slice(0, 50) + '...';
            detailObj.showToggle = true;
          }
          const detailList = this.state.detailList;
          detailList.push(detailObj);
          this.setState({
            detailList,
          });
        });
        this.getArticleStatistics(this.state.detailList[this.state.current].id);
        // 获取video
        Taro.nextTick(() => {
          this.palyVideo();
        });
      })
  }

  fetchData() {
    if (this.state.ids) {
      this.getList(this.state.ids.toString());
    }
  }

  getMoreList(isStatistics = null) {
    const params = {
      // per_appId: state.base.currentStore.appId,
      categoryId: this.state.categoryId,
      pageNo: this.state.pageNo,
      pageSize: 6,
      deleteFlag: 'N',
      status: graphEnum.STATUS.ON_SHELVE.value,
    };

    return wxApi
      .request({
        url: api.article.list,
        data: params,
        loading: true,
      })
      .then((res) => {
        let { success, data } = res;
        if (!success) {
          return;
        }
        // 初次进来进行删减
        if (this.state.isFirst) {
          const index = data.findIndex((e) => e.id == this.state.id);
          data.splice(0, index);
        }

        data = data.filter((e) => e.articleType === 2);
        // data没有长度无法下滑，所以需要继续请求直到最后一页  直到最后一页，才停止
        if (!data.length && res.data.length !== 0) {
          this.setState({
            pageNo: ++this.state.pageNo,
          });

          this.getMoreList();
          return;
        }
        const detailList = [];
        data.forEach((item) => {
          const detailObj = {
            id: item.id,
            title: item.title,
            content: item.content,
            likeCount: item.totalLikeCount || 0,
            virtualLikeCount: item.virtualLikeCount || 0,
            liked: item.liked || false, // 是否点赞:
            goodsList: item.articleItemList || [], // 商品列表
            digest: item.digest, // 摘要
            shortDigest: '', // 短摘要
            showToggle: false, // 是否显示展开全文按钮
            isAll: false, // 是否显示全文
            showGood: true, // 是否显示第一个商品

            // 商品曝光
          };
          if (!!detailObj.goodsList && detailObj.goodsList.length > 0) {
            report.commodityExposure(detailObj.goodsList[0]);
          }
          // 判断商品标题超出两行显示。。。
          detailObj.goodsList.forEach((item) => {
            if (item.fitemName.length > 54) {
              item.fitemName = item.fitemName.slice(0, 50) + '...';
            }
          });

          // 判断是否缩略摘要
          if (item.digest.length > 54) {
            detailObj.shortDigest = item.digest.slice(0, 50) + '...';
            detailObj.showToggle = true;
          }
          detailList.push(detailObj);
        });
        this.setState(
          {
            detailList: [...this.state.detailList, ...detailList],
          },

          () => {
            if (this.state.detailList.length === 1 && !!res.data.length) {
              this.setState({
                pageNo: ++this.state.pageNo,
              });

              this.getMoreList(true);
            }
          }
        );
        // 获取video
        if (this.state.isFirst) {
          this.getArticleStatistics(this.state.detailList[this.state.current].id);
          Taro.nextTick(() => {
            this.palyVideo();
          });
          this.setState({
            isFirst: false,
          });
        }
      })
      .catch((error) => {});
  }

  // 请求统计浏览接口
  getArticleStatistics(StatisticsId) {
    wxApi
      .request({
        url: api.article.detail,
        data: {
          id: StatisticsId,
        },

        loading: true,
      })
      .then((res) => {
      });
  }

  render() {
    const { detailList, isAndroid, play, current, showDialog, id, statusBarHeight } = this.state;
    const currentItem = detailList[current]
    const WEAPP_RENDER = (
      <View data-scoped="wk-mpgv-VideoDetail" className="wk-mpgv-VideoDetail videoBox">
        <Swiper
          vertical
          className="swiper"
          onChange={this.changeSwiper}
          onTransition={this.bindtransition}
          onAnimationFinish={this.bindanimationfinish}
        >
          {
          !!detailList &&
            detailList.map((item, index) => {
              return (
                <Block key={'block-' + index}>
                  <SwiperItem className="swiper-item">
                    {index === current ? (
                      <Video
                        key={'myVideo-' + index}
                        id={'myVideo' + index}
                        autoplay
                        src={item.content}
                        enableProgressGesture={false}
                        showCenterPlayBtn={false}
                        controls={false}
                        loop
                        onClick={this.videoTap}
                        onTimeUpdate={this.bindtimeupdate}
                        onEnded={this.bindended}
                        customCache={false}
                      >
                        {isAndroid && (
                          <Block key={'ggg' + index}>
                            {!play && (
                              <CoverImage
                                onClick={this.videoTap}
                                className="play"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/play.png"
                              ></CoverImage>
                            )}

                            <CoverView className="content">
                              {(item.showGood && item.goodsList.length > 0 ) && (
                                <CoverView className="goodsItem">
                                  <CoverImage className="goodsImg" src={item.goodsList[0].fcoverUrl}></CoverImage>
                                  <CoverView className="goodsRight">
                                    <CoverView className="title">{item.goodsList[0].fitemName}</CoverView>
                                    <CoverView>
                                      {fn.split(item.goodsList[0].labelOrdinary, ',') &&
                                        fn.split(item.goodsList[0].labelOrdinary, ',').map((tag, index) => {
                                          return (
                                            <CoverView className="tag" key={'cover-view-' + index }>
                                              {tag}
                                            </CoverView>
                                          );
                                        })}
                                    </CoverView>
                                    {item.goodsList[0].fitemType === 48 ? (
                                      <CoverView className="money">
                                        <CoverView className="number">{item.goodsList[0].fitemSalePrice}</CoverView>
                                      </CoverView>
                                    ) : (
                                      <CoverView className="money">
                                        <CoverView className="symbol">￥</CoverView>
                                        <CoverView className="number">
                                          {filters.moneyFilter(item.goodsList[0].fitemSalePrice, true)}
                                        </CoverView>
                                      </CoverView>
                                    )}

                                    <CoverView
                                      className="btn"
                                      onClick={_fixme_with_dataset_(this.goToGoodDetail, { index: 0 })}
                                    >
                                      去看看
                                    </CoverView>
                                  </CoverView>
                                  <CoverImage
                                    className="close"
                                    onClick={this.handleClose}
                                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/close.png"
                                  ></CoverImage>
                                </CoverView>
                              )}

                              <CoverView className="digest" id="digest">
                                {!!item.showToggle && !item.isAll ? item.shortDigest : item.digest}
                                {!!item.showToggle && (
                                  <CoverView className="isAll" onClick={this.onChangeShowAll}>
                                    {item.isAll ? '收起' : '展开'}
                                  </CoverView>
                                )}
                              </CoverView>
                            </CoverView>
                            {/*  右侧按钮  */}
                            <CoverView className="bottomRight">
                              <CoverView className="like" onClick={this.onChangeLike}>
                                {item.liked ? (
                                  <CoverImage src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like1.png"></CoverImage>
                                ) : (
                                  <CoverImage src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like.png"></CoverImage>
                                )}

                                <CoverView>{item.virtualLikeCount < 1000 ? item.virtualLikeCount : '999+'}</CoverView>
                              </CoverView>
                              <Button className="share" openType="share">
                                <CoverImage src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-share.png"></CoverImage>
                                <CoverView>分享</CoverView>
                              </Button>
                              <CoverView className="goods" onClick={this.clickGoods}>
                                <CoverImage className="goods-icon" src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-goods.png"></CoverImage>
                                <CoverView className="goods-txt">{item.goodsList.length}</CoverView>
                              </CoverView>
                            </CoverView>
                          </Block>
                        )}
                      </Video>
                    ) : (
                      <Image
                        className="play"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/play.png"
                      ></Image>
                    )}

                    {!isAndroid && (
                      <Block>
                        {!play && (
                          <Image
                            onClick={this.videoTap}
                            className="play"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/play.png"
                          ></Image>
                        )}

                        <View className="content">
                          {(item.showGood && item.goodsList.length > 0) && (
                            <View className="goodsItem">
                              <Image className="goodsImg" src={item.goodsList[0].fcoverUrl}></Image>
                              <View className="goodsRight">
                                <View className="title">{item.goodsList[0].fitemName}</View>
                                <View className="tagBox">
                                  {fn.split(item.goodsList[0].labelOrdinary, ',') &&
                                    fn.split(item.goodsList[0].labelOrdinary, ',').map((tag, index) => {
                                      return (
                                        <View className="tag" key={'tag-' + index}>
                                          {tag}
                                        </View>
                                      );
                                    })}
                                </View>
                                {item.goodsList[0].fitemType === 48 ? (
                                  <View className="money">
                                    <View className="number">{item.goodsList[0].fitemSalePrice}</View>
                                  </View>
                                ) : (
                                  <View className="money">
                                    <View className="symbol">￥</View>
                                    <View className="number">
                                      {filters.moneyFilter(item.goodsList[0].fitemSalePrice, true)}
                                    </View>
                                  </View>
                                )}

                                <View className="btn" onClick={_fixme_with_dataset_(this.goToGoodDetail, { index: 0 })}>
                                  去看看
                                </View>
                              </View>
                              <Image
                                className="close"
                                onClick={this.handleClose}
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/close.png"
                              ></Image>
                            </View>
                          )}

                          <View className="digest" id="digest">
                            {item.showToggle && !item.isAll ? item.shortDigest : item.digest}
                            {item.showToggle && (
                              <View className="isAll" onClick={this.onChangeShowAll}>
                                {item.isAll ? '收起' : '展开'}
                              </View>
                            )}
                          </View>
                        </View>
                        {/*  右侧按钮  */}
                        <View className="bottomRight">
                          <View className="like" onClick={this.onChangeLike}>
                            {item.liked ? (
                              <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like1.png"></Image>
                            ) : (
                              <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like.png"></Image>
                            )}

                            <View>{item.virtualLikeCount < 1000 ? item.virtualLikeCount : '999+'}</View>
                          </View>
                          <Button className="share" openType="share">
                            <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-share.png"></Image>
                            <View>分享</View>
                          </Button>
                          <View className="goods" onClick={this.clickGoods}>
                            <Image className="goods-icon" src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-goods.png"></Image>
                            <View className="goods-txt">{item.goodsList.length}</View>
                          </View>
                        </View>
                      </Block>
                    )}
                  </SwiperItem>
                </Block>
              );
            })}
        </Swiper>
        {/*  商品列表  */}
        {/*  <animat-dialog
                                         id="animat-dialog"
                                         anim-class="goods-dialog">  */}
        {showDialog && <CoverView className="commodity_screen" onClick={this.onClickMaskHide}></CoverView>}

        {showDialog && (
          <CoverView className="goods-dialog">
            <CoverView className="headerTitle">商品列表</CoverView>
            {/*  <scroll-view scroll-y="true" scroll-y="{{true}}"class="goodsBox">  */}
            <CoverView className="goodsBox">
              {!!detailList[current].goodsList &&
                detailList[current].goodsList.map((item, index) => {
                  return (
                    <CoverView className="goodsItem" key={'goodsItem-' + index}>
                      {Taro.getEnv() === Taro.ENV_TYPE.WEB && <Image className="goodsImg" src={item.fcoverUrl}></Image>}
                      {Taro.getEnv() !== Taro.ENV_TYPE.WEB && <CoverImage className="goodsImg" src={item.fcoverUrl}></CoverImage>}
                      <CoverView className="goodsRight">
                        <CoverView className="title">{item.fitemName}</CoverView>
                        <CoverView className="tagBox">
                          {fn.split(item.labelOrdinary, ',') &&
                            fn.split(item.labelOrdinary, ',').map((tag, index) => {
                              return (
                                <CoverView className="tag" key={'tag2-' + index}>
                                  {tag}
                                </CoverView>
                              );
                            })}
                        </CoverView>
                        {item.fitemType === 48 ? (
                          <CoverView className="money">
                            <CoverView className="number">{item.fitemSalePrice}</CoverView>
                          </CoverView>
                        ) : (
                          <CoverView className="money">
                            <CoverView className="symbol">￥</CoverView>
                            <CoverView className="number">{filters.moneyFilter(item.fitemSalePrice, true)}</CoverView>
                          </CoverView>
                        )}

                        <CoverView
                          className="btn"
                          onClick={_fixme_with_dataset_(this.goToGoodDetail, { index: index })}
                        >
                          去看看
                        </CoverView>
                      </CoverView>
                    </CoverView>
                  );
                })}
            </CoverView>
            {/*  </scroll-view>  */}
          </CoverView>
        )}

        {/*  </animat-dialog>  */}
        <HomeActivityDialog
          showPage={'sub-packages/moveFile-package/pages/graph/detail/index?id=' + id}
        ></HomeActivityDialog>
        <CoverImage
          onClick={this.handleBack}
          className="backIcon"
          style={_safe_style_('margin-top:' + statusBarHeight + 'px;')}
          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/goToBack.png"
        ></CoverImage>
      </View>
    )
    const H5_RENDER = (
      <View data-scoped="wk-mpgv-VideoDetail" className="wk-mpgv-VideoDetail videoBox">
        {/* {(!!detailList && detailList.length > 0)
        && (
        <Video
          key={'myVideo-' + current}
          id={'myVideo' + current}
          src={currentItem.content}
          enableProgressGesture={false}
          showCenterPlayBtn={false}
          controls={false}
          loop
          onClick={this.videoTap}
          onTimeUpdate={this.bindtimeupdate}
          onEnded={this.bindended}
          customCache={false}
          />
        )} */}
        <View onClick={this.toggleVideo}>
          <Swiper
            vertical
            className="swiper"
            onChange={this.changeSwiper}
            onTransition={this.bindtransition}
            onAnimationFinish={this.bindanimationfinish}
          >
            {
            (!!detailList && detailList.length > 0) &&
              detailList.map((item, index) => {
                return (
                  <SwiperItem className={"swiper-item" + ' index'+ index + ' current'+current} key={'SwiperItem' + index} onClick={this.toggleVideo}>
                    {
                      index === current &&
                      (
                      <Video
                        key={'myVideo-' + current}
                        id={'myVideo' + current}
                        src={currentItem.content}
                        enableProgressGesture={false}
                        showCenterPlayBtn={false}
                        controls={false}
                        loop
                        onClick={this.videoTap}
                        onTimeUpdate={this.bindtimeupdate}
                        onEnded={this.bindended}
                        customCache={false}
                        />
                      )
                    }

                    {isAndroid && (
                      <Block key={'asd' + index}>
                        {!play && (
                          <Image
                            onClick={this.videoTap}
                            className={"play " + index + current}
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/play.png"
                          ></Image>
                        )}

                        <View className="content">
                          {(item.showGood && item.goodsList.length > 0) && (
                            <View className="goodsItem">
                              <Image className="goodsImg" src={item.goodsList[0].fcoverUrl}></Image>
                              <View className="goodsRight">
                                <View className="title">{item.goodsList[0].fitemName}</View>
                                <View>
                                  {fn.split(item.goodsList[0].labelOrdinary, ',') &&
                                    fn.split(item.goodsList[0].labelOrdinary, ',').map((tag, index) => {
                                      return (
                                        <View className="tag" key={'cover-view-' + index }>
                                          {tag}
                                        </View>
                                      );
                                    })}
                                </View>
                                {item.goodsList[0].fitemType === 48 ? (
                                  <View className="money">
                                    <View className="number">{item.goodsList[0].fitemSalePrice}</View>
                                  </View>
                                ) : (
                                  <View className="money">
                                    <View className="symbol">￥</View>
                                    <View className="number">
                                      {filters.moneyFilter(item.goodsList[0].fitemSalePrice, true)}
                                    </View>
                                  </View>
                                )}

                                <View
                                  className="btn"
                                  onClick={_fixme_with_dataset_(this.goToGoodDetail, { index: 0 })}
                                >
                                  去看看
                                </View>
                              </View>
                              <Image
                                className="close"
                                onClick={this.handleClose}
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/close.png"
                              ></Image>
                            </View>
                          )}

                          <View className="digest" id="digest">
                            {!!item.showToggle && !item.isAll ? item.shortDigest : item.digest}
                            {!!item.showToggle && (
                              <View className="isAll" onClick={this.onChangeShowAll}>
                                {item.isAll ? '收起' : '展开'}
                              </View>
                            )}
                          </View>
                        </View>
                        {/*  右侧按钮  */}
                        <View className="bottomRight">
                          <View className="like" onClick={this.onChangeLike}>
                            {item.liked ? (
                              <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like1.png"></Image>
                            ) : (
                              <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like.png"></Image>
                            )}

                            <View>{item.virtualLikeCount < 1000 ? item.virtualLikeCount : '999+'}</View>
                          </View>
                          <Button className="share" openType="share" onClick={this.setShareConfig}>
                            <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-share.png"></Image>
                            <View>分享</View>
                          </Button>
                          <View className="goods" onClick={this.clickGoods}>
                            <Image className="goods-icon" src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-goods.png"></Image>
                            <View className="goods-txt">{item.goodsList.length}</View>
                          </View>
                        </View>
                      </Block>
                    )}

                    {!isAndroid && (
                      <Block>
                        {!play && (
                          <Image
                            onClick={this.videoTap}
                            className={"play " + index + current}
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/play.png"
                          ></Image>
                        )}

                        <View className="content">
                          {(item.showGood && item.goodsList.length > 0) && (
                            <View className="goodsItem">
                              <Image className="goodsImg" src={item.goodsList[0].fcoverUrl}></Image>
                              <View className="goodsRight">
                                <View className="title">{item.goodsList[0].fitemName}</View>
                                <View className="tagBox">
                                  {fn.split(item.goodsList[0].labelOrdinary, ',') &&
                                    fn.split(item.goodsList[0].labelOrdinary, ',').map((tag, index) => {
                                      return (
                                        <View className="tag" key={'tag-' + index}>
                                          {tag}
                                        </View>
                                      );
                                    })}
                                </View>
                                {item.goodsList[0].fitemType === 48 ? (
                                  <View className="money">
                                    <View className="number">{item.goodsList[0].fitemSalePrice}</View>
                                  </View>
                                ) : (
                                  <View className="money">
                                    <View className="symbol">￥</View>
                                    <View className="number">
                                      {filters.moneyFilter(item.goodsList[0].fitemSalePrice, true)}
                                    </View>
                                  </View>
                                )}

                                <View className="btn" onClick={_fixme_with_dataset_(this.goToGoodDetail, { index: 0 })}>
                                  去看看
                                </View>
                              </View>
                              <Image
                                className="close"
                                onClick={this.handleClose}
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/close.png"
                              ></Image>
                            </View>
                          )}

                          <View className="digest" id="digest">
                            {item.showToggle && !item.isAll ? item.shortDigest : item.digest}
                            {item.showToggle && (
                              <View className="isAll" onClick={this.onChangeShowAll}>
                                {item.isAll ? '收起' : '展开'}
                              </View>
                            )}
                          </View>
                        </View>
                        {/*  右侧按钮  */}
                        <View className="bottomRight">
                          <View className="like" onClick={this.onChangeLike}>
                            {item.liked ? (
                              <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like1.png"></Image>
                            ) : (
                              <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-like.png"></Image>
                            )}

                            <View>{item.virtualLikeCount < 1000 ? item.virtualLikeCount : '999+'}</View>
                          </View>
                          <Button className="share" openType="share" onClick={this.setShareConfig}>
                            <Image src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-share.png"></Image>
                            <View>分享</View>
                          </Button>
                          <View className="goods" onClick={this.clickGoods}>
                            <Image className="goods-icon" src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/video-goods.png"></Image>
                            <View className="goods-txt">{item.goodsList.length}</View>
                          </View>
                        </View>
                      </Block>
                    )}
                  </SwiperItem>
                );
              })}
          </Swiper>
        </View>
        {/*  商品列表  */}
        {showDialog && <View className="commodity_screen" onClick={this.onClickMaskHide}></View>}

        {showDialog && (
          <View className="goods-dialog">
            <View className="headerTitle">商品列表</View>
            {/*  <scroll-view scroll-y="true" scroll-y="{{true}}"class="goodsBox">  */}
            <View className="goodsBox">
              {!!detailList[current].goodsList &&
                detailList[current].goodsList.map((item, index) => {
                  return (
                    <View className="goodsItem" key={'goodsItem-' + index}>
                      {Taro.getEnv() === Taro.ENV_TYPE.WEB && <Image className="goodsImg" src={item.fcoverUrl}></Image>}
                      {Taro.getEnv() !== Taro.ENV_TYPE.WEB && <Image className="goodsImg" src={item.fcoverUrl}></Image>}
                      <View className="goodsRight">
                        <View className="title">{item.fitemName}</View>
                        <View className="tagBox">
                          {fn.split(item.labelOrdinary, ',') &&
                            fn.split(item.labelOrdinary, ',').map((tag, index) => {
                              return (
                                <View className="tag" key={'tag2-' + index}>
                                  {tag}
                                </View>
                              );
                            })}
                        </View>
                        {item.fitemType === 48 ? (
                          <View className="money">
                            <View className="number">{item.fitemSalePrice}</View>
                          </View>
                        ) : (
                          <View className="money">
                            <View className="symbol">￥</View>
                            <View className="number">{filters.moneyFilter(item.fitemSalePrice, true)}</View>
                          </View>
                        )}

                        <View
                          className="btn"
                          onClick={_fixme_with_dataset_(this.goToGoodDetail, { index: index })}
                        >
                          去看看
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>
            {/*  </scroll-view>  */}
          </View>
        )}

        {/*  </animat-dialog>  */}
        <HomeActivityDialog
          showPage={'sub-packages/moveFile-package/pages/graph/detail/index?id=' + id}
        ></HomeActivityDialog>
        <Image
          onClick={this.handleBack}
          className="backIcon"
          style={_safe_style_('margin-top:' + statusBarHeight + 'px;')}
          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/goToBack.png"
        ></Image>



          <ShareDialog
            childRef={this.state.refShareDialogCMTP}
          />

      </View>
    )
    return process.env.TARO_ENV === 'weapp' ? WEAPP_RENDER : H5_RENDER;
  }
}

export default VideoDetail;
