import React, { useEffect, useState, FC } from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import wxApi from '@/wxat-common/utils/wxApi';
import Empty from '@/wxat-common/components/empty/empty';
import api from '@/wxat-common/api/index.js';
import './index.scss';
import { $getRouter } from 'wk-taro-platform';

interface storeListState {
  hotelContract: string | number | undefined;
  name: String;
  address: String
}

let StoreList: FC = () => {
  const { itemNo } = $getRouter().params;
  const [storeList, setStoreList] = useState<Array<storeListState>>([]);

  const getStoreData = async () => {
    const { data = [] } = await wxApi.request({
      url: api.virtualGoods.ticketHotel,
      data: {
        itemNo
      },
    })
    setStoreList(data)
  }
  useEffect(() => {
    getStoreData()
  }, [itemNo]);

  return (
    <View className="store-container">
      {
        storeList.length > 0 ? (
          storeList.map(item => {
            return (
              <View className="store-item" key={item.hotelContract}>
                  <View>{item.name}</View>
                  <View>{item.address}</View>
                </View>
            )
          })
        ) : (
          <Empty message='暂无数据...'></Empty>
        )
      }
    </View>
  );
};

export default StoreList;
