// @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import React, { Component } from 'react';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '@/wxat-common/api';
import wxApi from '@/wxat-common/utils/wxApi';
import constants from '@/wxat-common/constants';
import IntegralModule from '@/wxat-common/components/decorate/integralModule';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import { connect } from 'react-redux';
// const app = Taro.getApp();
const loadMoreStatusList = constants.order.loadMoreStatus;
// redux state的在这里定义
interface PageStateProps {
  scoreName: string;
}
// redux 的action方法再这里定义
interface PageDispatchProps {}
// 父子组件直接传递的props
interface PageOwnProps {}
// 组件的state声明
interface PageState {
  dataList: Array<Record<string, any>>;
  error: boolean;
  pageNo: number;
  hasMore: boolean;
  score: number;
  loadMoreStatus: any;
  dataSource:any
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const mapStateToProps = (state) => ({
  scoreName: state.globalData.scoreName || '积分',
});
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class IntergralMall extends Component<IProps, PageState> {
  state = {
    dataList: [],
    dataSource: {},
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatusList.HIDE,
    score: 0,
  };

  onPullDownRefresh = () => {
    this.getList(true);
  };

  onReachBottom = () => {
    if (this.state.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
        pageNo: this.state.pageNo + 1,
      });

      this.getList();
    }
  };
  componentDidMount() {
    this.getBalance();
    this.getList();
    wxApi.setNavigationBarTitle({
      title: `${this.props.scoreName}商城`,
    });
  }

  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.getList();
  };
  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  /**
   *  获取积分
   */
  getBalance = () => {
    return wxApi
      .request({
        url: api.userInfo.balance,
        data: {},
      })
      .then((res) => {
        this.setState({
          score: res.data.score,
        });
      })
      .catch((error) => {
        console.log('score: error: ' + JSON.stringify(error));
      });
  };

  /**
   * 获取积分商品列表
   */
  getList = (isStart?) => {
    if (isStart) {
      this.setState({
        pageNo: 1,
        hasMore: true,
      });
    }
    this.setState({ error: false });
    const params = {
      type: 22,
      pageNo: this.state.pageNo,
      pageSize: 10,
    };

    wxApi
      .request({
        url: api.goods.integralQuery,
        data: params,
      })
      .then((res) => {
        const data = res.data || [];
        let dataList;
        if (this.isLoadMoreRequest()) {
          dataList = this.state.dataList.concat(data);
        } else {
          dataList = data;
        }
        const hasMore = dataList.length < res.totalCount;
        this.setState({
          dataList,
          hasMore,
          dataSource: res || {},
        });
      });
  };

  render() {
    const { score, dataList, error, loadMoreStatus, dataSource } = this.state;
    const { scoreName } = this.props;
    return (
      <View data-scoped='wk-pig-Goods' className='wk-pig-Goods integral'>
        <View className='top-box'>
          <Image
            className='bg-img'
            src='https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/integral/Rectangle.png'
          ></Image>
          <View className='integralNum'>{score}</View>
          <View className='pig-Goods-title'>我的{scoreName}</View>
        </View>
        <View className='goods-list'>
          {!!(!!dataList && dataList.length == 0) && <Empty message='敬请期待...'></Empty>}
          {!!error && <Error></Error>}
          {!!(!!dataList && dataList.length > 0) && (
            <IntegralModule scoreName={scoreName} dataSource={dataSource} _renderList={dataList} showNumberType={1} />
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        </View>
      </View>
    );
  }
}

export default IntergralMall;
