import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import date from '@/wxat-common/utils/date';
import shareUtils from '@/wxat-common/utils/share';
import { connect } from 'react-redux';

import PostersDialog from '@/sub-packages/marketing-package/components/posters-dialog';
import TaskError from '@/wxat-common/components/task-error/index';
import ActivityItem from "./components/activity-item";
import Empty from '@/wxat-common/components/empty/empty';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import { contentTypeObj, couponpocket, joinGroup } from "./constant";
import './index.scss';

const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};

interface StateProps {
  sessionId: string;
  currentStore: Record<string, any>;
  userInfo: Record<string, any>;
}

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    userInfo: state.base.userInfo,
  };
};

interface PromotionAmb {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class PromotionAmb extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    error: false,
    activityList: [],
    hasMore: true,
    nothing: false,
    loadMoreStatus: loadMoreStatus.HIDE,

    sameStore: true,
    shareParams: {},
    uniqueShareInfoForQrCode: null,
    storeId: null,
    storeName: null,
    commission: {},
    tmpStyle: {},
  };

  pageNo = 1;
  refShareDialogCMPT = React.createRef<any>();

  componentDidMount() {
    const { sessionId } = this.props;
    if (sessionId) {
      this.initRenderData();
    }
    this.getTemplateStyle();
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.initRenderData();
    }
    // 比较门店
    if (preProps.currentStore.id !== this.props.currentStore.id) {
      this.checkSameStore();
    }
  }

  // 初始化
  initRenderData() {
    this.getStoreInfo();
  }

  // 比较门店是否一致
  checkSameStore() {
    const { currentStore } = this.props;
    const { storeId } = this.state;

    this.setState({
      sameStore: currentStore.id === storeId,
    });

    if (currentStore.id === storeId) {
      this.getDetail();
    }
  }

  // 获取大使信息
  getStoreInfo() {
    const { userInfo, currentStore } = this.props;

    wxApi
      .request({
        url: api.ambassador.detail,
        loading: true,
        data: {
          auditStatus: 1, // 审核通过
          memberId: userInfo.id,
        },
      })
      .then((res) => {
        if (res.data) {
          const detail = res.data;
          console.log(detail.storeName, 'detail.storeName');
          this.setState({
            storeId: detail.storeId,
            storeName: detail.storeName,
            sameStore: currentStore.id === detail.storeId,
          });

          if (currentStore.id === detail.storeId) {
            this.getDetail();
          }
        } else {
          wxApi.showToast({
            title: '您还未成为推广大使',
            icon: 'none',
          });

          setTimeout(() => {
            wxApi.$navigateTo({
              url: '/wxat-common/pages/home/index',
            });
          }, 15e2);
        }
      });
  }

  getDetail() {
    this.fetchActivities();
    this.sumcommission();
  }

  onPullDownRefresh() {
    this.pageNo = 1;
    this.setState({
      hasMore: true,
    });

    this.fetchActivities(true);
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.fetchActivities();
    }
  }

  onRetryLoadMore() {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.fetchActivities();
  }

  // 获取佣金累计
  sumcommission() {
    const now = new Date();

    wxApi
      .request({
        url: api.ambassador.sumcommission,
        loading: true,
        data: {
          startDate: date.format(now),
          endDate: date.format(date.addDays(now, 1)),
        },
      })
      .then((res) => {
        this.setState({
          commission: res.data || {},
        });
      });
  }

  // 门店活动列表
  fetchActivities(isFromPullDown?) {
    const { userInfo } = this.props;

    this.setState({
      error: false,
    });

    wxApi
      .request({
        url: api.ambassador.activityList,
        loading: true,
        data: {
          sortByBeginTime: 1,
          activityStatus: 1, // 正在进行中
          promoterId: userInfo.id,
          pageNo: this.pageNo,
          pageSize: 5,
        },
      })
      .then((res) => {
        let { activityList } = this.state;
        const result = res.data || [];

        result.map((item) => {
          item.salePrice = item.contentActivityAmount || 0;
          item.money = item.promoterCommission || 0;
          item.thumbnail = item.contentInfo.contentThumbnail;
          // 真实库存：provNum(总量) - cardSalesVolume(已售出) provNum为0时不限量
          item.itemStock = 1;
          // item.itemStock =
          //   item.pocketInfo.provNum === 0 ? 9999999999 : item.provNum - item.pocketInfo.cardSalesVolume;

          return item;
        });

        activityList = activityList.concat(result);

        this.pageNo++;
        this.setState({
          nothing: !activityList.length,
          hasMore: activityList.length !== res.totalCount,
          activityList,
        });
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  }

  isLoadMoreRequest() {
    return this.pageNo > 1;
  }

  /**
   * 打开分享
   */
  handleShare(item) {
    const { currentStore } = this.props;
    const path = contentTypeObj[item.contentType].detailUrl;
    const uniqueShareInfoForQrCode = {
      page: path,
      refStoreId: currentStore.id,
      itemNo: item.contentInfo.detailId,
      activityId: item.contentActivityId, // 活动 id
      activityStatus: item.activityStatus,
      promoterActivityId: item.id, // 推广大使活动 id
      refBz: shareUtils.ShareBZs.PROMOTION_AMBASSADOR,
      refBzName: `推广大使${
        item.contentInfo && item.contentInfo.contentName ? '-' + item.contentInfo.contentName : ''
      }`,
    };

    this.refShareDialogCMPT.current.setContext(this);
    this.setState({
      shareParams: {
        name: item.name,
        thumbnail: item.contentInfo.contentThumbnail,
      },

      uniqueShareInfoForQrCode,
    });
  }

  // 业绩列表
  toPerfList = () => {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/promotion-amb/perf-list/index',
    });
  };

  // 跳转详情
  toCardPackDetail = (item) => {
    const url = contentTypeObj[item.contentType].detailUrl;
    wxApi.$navigateTo({
      url: url,
      data: {
        itemNo: item.contentInfo.detailId,
        isPaDetail: 1,
        promoterCommission: item.promoterCommission,
        guideCommission: item.guideCommission,
        activityId: item.contentActivityId,
        promoterActivityId: item.id,
        activityStatus: item.activityStatus,
      },
    });
  };

  // 获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    this.setState({
      tmpStyle,
    });

    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const {
      storeId,
      storeName,
      sameStore,
      commission,
      activityList,
      nothing,
      error,
      shareParams,
      uniqueShareInfoForQrCode,
      loadMoreStatus,
      hasMore,
      tmpStyle,
    } = this.state as Record<string, any>;

    const { userInfo } = this.props;

    return (
      !!storeId && (
        <View
          data-fixme='02 block to view. need more test'
          data-scoped='wk-mpp-PromotionAmb'
          className='wk-mpp-PromotionAmb'
        >
          {!sameStore ? (
            <TaskError storeName={storeName}></TaskError>
          ) : (
            <Block>
              <View className='header' style={_safe_style_('background:' + tmpStyle.btnColor)}>
                <View className='outline'>
                  <View className='user'>
                    <View className='user-info'>
                      <Image className='user-face' src={userInfo.avatarImgUrl}></Image>
                      {userInfo.nickname}
                    </View>
                    <View className='total-incomes' onClick={this.toPerfList}>
                      累计奖励金
                      <Text className='incomes'>{'￥' + filters.moneyFilter(commission.totalCommissiion, true)}</Text>
                      <Image
                        className='icon-right'
                        src={getStaticImgUrl.images.rightAngleGray_png}
                        mode='widthFix'
                      ></Image>
                    </View>
                  </View>
                  <View className='today-data'>
                    <View>
                      奖励金(今日)
                      <View className='num'>{filters.moneyFilter(commission.sumCommissiion, true)}</View>
                    </View>
                    <View>
                      推广订单 (今日)
                      <View className='num'>{commission.sumItemCount}</View>
                    </View>
                  </View>
                </View>
              </View>
              <View className='title'>门店活动</View>
              {activityList.map((item) => {
                return (
                  <ActivityItem
                    key={item.id}
                    item={item}
                    from='index'
                    onCardPackDetail={this.toCardPackDetail}
                    onShare={this.handleShare.bind(this)}
                  ></ActivityItem>
                );
              })}
              {!!nothing && <Empty message='敬请期待...'></Empty>}
              {!!error && <Error></Error>}
              {!!hasMore && <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>}
              <View className='cushion'></View>
              {/*  分享海报  */}
              <PostersDialog
                childRef={this.refShareDialogCMPT}
                posterName={shareParams.name}
                posterImage={shareParams.thumbnail}
                uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
              ></PostersDialog>

              <Canvas canvasId='shareCanvas' className='promotion-canvas'></Canvas>
            </Block>
          )}
        </View>
      )
    );
  }
}

export default PromotionAmb;
