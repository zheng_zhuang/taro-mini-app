import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import paFilters from '../../wxs/filters.wxs';
import filters from '@/wxat-common/utils/money.wxs';
import template from '@/wxat-common/utils/template';
import { connect } from 'react-redux';
import React, { ComponentClass } from 'react';
import { contentTypeObj } from "../../constant";

import './index.scss';

interface StateProps {
  tmpStyle: any;
}

interface ActivityItemProps {
  from?: string;
  item: Record<string, any>;
  onShare?: Function;
  onCardPackDetail?: Function;
  onPerfDetail?: Function;
}

const mapStateToProps = () => {
  return {
    tmpStyle: template.getTemplateStyle(),
  };
};

interface ActivityItem {
  props: StateProps & ActivityItemProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ActivityItem extends React.Component {
  state = {};

  // 分享
  handleShare = (e) => {
    const { item, onShare } = this.props;

    onShare && onShare(item);
    e.stopPropagation();
  };

  // 卡包详情
  toCardPackDetail = () => {
    const { item, onCardPackDetail } = this.props;

    onCardPackDetail && onCardPackDetail(item);
  };

  // 业绩明细
  toPerfDetail = () => {
    const { item, onPerfDetail } = this.props;

    onPerfDetail && onPerfDetail(item);
  };

  render() {
    const { item, tmpStyle, from } = this.props;

    return (
      !!item && (
        <View
          data-scoped='wk-pca-ActivityItem'
          className='wk-pca-ActivityItem activity-item'
          onClick={this.toCardPackDetail}
        >
          <View className='thumbnail'>
            <Image className='img' src={item.contentInfo.contentThumbnail}></Image>
            {item.itemStock === 0 && <View className='tips'>库存不足</View>}
          </View>
          <View className='content'>
            <View className='activity-header'>
              <View className='activity-name limit-line'>{item.name}</View>
              <View className='activity-type'>{contentTypeObj[item.contentType].name}</View>
            </View>
            <View className='activity-date'>
              {paFilters.dateSplit(item.beginTime) + '～' + paFilters.dateSplit(item.endTime)}
            </View>
            <View className='price'>
              {contentTypeObj[item.contentType].label}
              {'：￥' + filters.moneyFilter(item.contentInfo.contentAmount, true)}
            </View>
            <View className='income' style={_safe_style_('color: ' + tmpStyle.btnColor)}>
              {from === 'index'
                ? `推广赚 ¥${filters.moneyFilter(item.money, true)}`
                : `总获益 ¥${filters.moneyFilter(item.activityCommission, true)}`}
            </View>
            {from === 'index' && (
              <Button
                className='share-btn'
                onClick={this.handleShare}
                style={_safe_style_('background: ' + tmpStyle.btnColor)}
              >
                去分享
              </Button>
            )}

            {from === 'perf-list' && (
              <Button className='detail-btn' onClick={this.toPerfDetail}>
                明细
                <Image
                  className='icon-right'
                  src={getStaticImgUrl.images.rightAngleGray_png}
                  mode='widthFix'
                ></Image>
              </Button>
            )}
          </View>
        </View>
      )
    );
  }
}

export default ActivityItem as ComponentClass<ActivityItemProps>;
