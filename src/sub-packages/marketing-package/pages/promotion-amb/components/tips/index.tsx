import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, Text, View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template';
import { connect } from 'react-redux';
import React, { ComponentClass } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import './index.scss';

type StateProps = {
  tmpStyle: any;
};

type TipsProps = {
  action: string;
  storeName?: string;
};

const mapStateToProps = () => {
  return {
    tmpStyle: template.getTemplateStyle(),
  };
};

interface Tips {
  props: StateProps & TipsProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Tips extends React.Component {
  state = {};

  backHomepage = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  render() {
    const { action, tmpStyle, storeName } = this.props;
    const withoutAuth = cdnResConfig.auth.withoutAuth;
    const successImg = cdnResConfig.promotion_amb.success;

    return (
      <View data-scoped='wk-pct-Tips' className='wk-pct-Tips container'>
        {action === 'isAmbassador' && (
          <Block>
            <View className='isAmbassador'>
              <Image className='img' src={withoutAuth}></Image>
              <View className='tips-text'>
                <Text>{'您已成为' + storeName + '的推广大使，\n暂不支持重复申请'}</Text>
              </View>
              <Button
                className='common-btn'
                onClick={this.backHomepage}
                style={_safe_style_('background:' + tmpStyle.btnColor)}
              >
                知道了
              </Button>
            </View>
          </Block>
        )}

        {action === 'isGuider' ? (
          <Block>
            <View className='isAmbassador'>
              <Image className='img' src={withoutAuth}></Image>
              <View className='tips-text'>
                <Text>您已是导购，不能成为别人的推广大使，\n请在商家小程序点击推广大使，开启推广大使身份</Text>
              </View>
              <Button
                className='common-btn'
                onClick={this.backHomepage}
                style={_safe_style_('background:' + tmpStyle.btnColor)}
              >
                知道了
              </Button>
            </View>
          </Block>
        ) : (
          action === 'applySuccess' && (
            <Block>
              <View className='applySuccess'>
                <Image className='img' src={successImg}></Image>
                <View className='title'>提交申请成功</View>
                <View className='tips-text'>
                  <Text>服务顾问审核通过将会给您推送消息</Text>
                </View>
                <Button
                  className='common-btn'
                  onClick={this.backHomepage}
                  style={_safe_style_('background:' + tmpStyle.btnColor)}
                >
                  返回首页
                </Button>
              </View>
            </Block>
          )
        )}
      </View>
    );
  }
}

export default Tips as ComponentClass<TipsProps>;
