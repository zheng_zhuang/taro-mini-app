import React from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { Block, View, Picker, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
import paFilters from '../wxs/filters.wxs';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import date from '@/wxat-common/utils/date';
import { connect } from 'react-redux';

import ActivityItem from '../components/activity-item/index';
import Empty from '@/wxat-common/components/empty/empty';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import { contentTypeObj } from '../constant/index';

import './index.scss';

const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};

type StateProps = {};

type IProps = StateProps;

const mapStateToProps = () => {
  return {};
};

interface PerfList {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class PerfList extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    error: false,
    activityList: [],
    nothing: false,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,

    month: '',
    monthEnd: '',
    commission: {},
  };

  pageNo = 1;

  componentDidMount() {
    const now = date.format(new Date(), 'yyyy-MM');

    this.setState(
      {
        month: now,
        monthEnd: now,
      },

      () => {
        this.reLoad();
      }
    );

    this.getTemplateStyle();
  }

  onPullDownRefresh() {
    this.pageNo = 1;
    this.setState({
      hasMore: true,
    });

    this.fetchActivities(true);
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.fetchActivities();
    }
  }

  onRetryLoadMore() {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.fetchActivities();
  }

  // 业绩列表
  fetchActivities(isFromPullDown?) {
    this.setState({
      error: false,
    });

    const chooseDate = this.getChooseDate(1),
      searchStartTime = chooseDate.split('|')[0],
      searchEndTime = chooseDate.split('|')[1];

    wxApi
      .request({
        url: api.ambassador.activityList,
        loading: true,
        data: {
          sortByBeginTime: 1,
          searchStartTime,
          searchEndTime,
          getCommission: 1,
          pageNo: this.pageNo,
          pageSize: 1000, // 后端反馈暂无法实现分页
        },
      })
      .then((res) => {
        let activityList = res.data || [];

        activityList.map((item) => {
          item.salePrice = item.contentActivityAmount || 0;
          item.money = item.activityCommission || 0;
          item.thumbnail = item.contentActivityThumbnail;
          // 真实库存：provNum(总量) - cardSalesVolume(已售出) provNum为0时不限量
          item.itemStock = 1;
          // item.itemStock =
          //   item.pocketInfo.provNum === 0 ? 9999999999 : item.pocketInfo.provNum - item.pocketInfo.cardSalesVolume;

          return item;
        });

        if (this.pageNo !== 1) {
          activityList = this.state.activityList.concat(activityList);
        }

        this.pageNo++;
        this.setState({
          hasMore: activityList.length !== res.totalCount,
          nothing: activityList.length ? false : true,
          activityList,
        });
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  }

  // 获取累计奖励金
  fetchMonthCommission() {
    const chooseDate = this.getChooseDate(1),
      searchStartTime = chooseDate.split('|')[0],
      searchEndTime = chooseDate.split('|')[1];

    wxApi
      .request({
        url: api.ambassador.sumcommission,
        data: {
          startDate: searchStartTime,
          endDate: searchEndTime,
        },
      })
      .then((res) => {
        this.setState({
          commission: res.data || {},
        });
      });
  }

  isLoadMoreRequest() {
    return this.pageNo > 1;
  }

  reLoad() {
    this.pageNo = 1;
    this.setState({
      hasMore: true,
    });

    this.fetchActivities();
    this.fetchMonthCommission();
  }

  bindDateChange = (e) => {
    this.setState(
      {
        month: e.detail.value,
      },

      () => {
        this.reLoad();
      }
    );
  };

  // 业绩明细
  toPerfDetail(item) {
    const chooseDate = this.getChooseDate();

    item.pocketInfo = null;
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/promotion-amb/perf-detail/index',
      data: {
        chooseDate,
        id: item.id,
        activityCommission: item.activityCommission || 0,
        activityId: item.contentActivityId,
      },
    });
  }

  // 计算后端所需的月起始天
  getChooseDate(hms?) {
    const month = this.state.month as any,
      beginHms = hms ? ' 00:00:01' : '',
      endHms = hms ? ' 23:59:59' : '';

    return `${month}-01${beginHms}|${month}-${new Date(
      month.split('-')[0],
      month.split('-')[1],
      0
    ).getDate()}${endHms}`;
  }

  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { month, monthEnd, commission, activityList, hasMore, nothing, error, loadMoreStatus } = this.state as Record<
      string,
      any
    >;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-ppp-PerfList' className='wk-ppp-PerfList'>
        <View className='header'>
          <Picker
            mode='date'
            value={month}
            onChange={this.bindDateChange}
            fields='month'
            start='2018-08'
            end={monthEnd}
          >
            <View className='picker'>{paFilters.dateFormat(month)}</View>
          </Picker>
          {commission !== null && (
            <View>
              总累计奖励金
              <Text className='incomes mr'>{'¥' + filters.moneyFilter(commission.totalCommissiion, true)}</Text>
              月累计奖励金
              <Text className='incomes'>{'¥' + filters.moneyFilter(commission.sumCommissiion, true)}</Text>
            </View>
          )}
        </View>
        {activityList.map((item) => {
          return (
            <ActivityItem
              key={item.id}
              item={item}
              onPerfDetail={this.toPerfDetail.bind(this)}
              from='perf-list'
            ></ActivityItem>
          );
        })}
        {!!nothing && <Empty message='暂无业绩'></Empty>}
        {!!error && <Error></Error>}
        {!!hasMore && <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>}
        <View className='cushion'></View>
      </View>
    );
  }
}

export default PerfList;
