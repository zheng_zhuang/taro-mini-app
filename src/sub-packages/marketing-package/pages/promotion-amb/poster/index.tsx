import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(AnimatDialog)
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Button, Label, Radio, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import checkOptions from '@/wxat-common/utils/check-options';
import { connect } from 'react-redux';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import shareUtil from '@/wxat-common/utils/share';

import AuthUser from '@/wxat-common/components/auth-user/index';
import Tips from '../components/tips/index';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';

const commonImg = cdnResConfig.common;

import './index.scss';

type StateProps = {
  userInfo: Record<string, any>;
  sessionId: string;
  appInfo: Record<string, any>;
};

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    sessionId: state.base.sessionId,
    userInfo: state.base.userInfo,
    appInfo: state.base.appInfo,
  };
};

interface Poster {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Poster extends React.Component {
  $router = $getRouter();
  constructor(props) {
    super(props);
  }

  state = {
    cdnImg: cdnResConfig.promotion_amb,
    isAmbassador: false,
    checked: false,
    guideInfo: null,
    qrCodeUrl: '',
    agreement: '',
    storeName: '',
    action: 'isAmbassador',
  };

  initializing = false;
  optionsData: Record<string, any>;
  refAgreementDialogCMPT: Taro.RefObject<any> = React.createRef();

  async componentDidMount() {
    const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);

    if (!!formattedOptions) {
      this.init(formattedOptions);
    }
    this.getTemplateStyle();
  }

  async componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);
      if (!!formattedOptions) {
        this.init(formattedOptions);
      }
    }
  }

  // 强制登录
  userInfoReady = async () => {
    const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);

    if (!!formattedOptions) {
      this.init(formattedOptions);
    }
  };

  init(options) {
    const { userInfo } = this.props;

    if (this.initializing || !Object.keys(options).length || !userInfo || !userInfo.id) {
      return false;
    }
    this.initializing = true;

    let params = options;

    // 短链分享
    if (options.scene) {
      params = decodeURIComponent(options.scene)
        .split('&')
        .map((kv) => {
          const [key, val] = kv.split('=');
          return { [key]: val };
        })
        .reduce((pre, cur) => ({ ...pre, ...cur }), {});
    }

    this.checkIdentity(params);
    this.getAgreement();
  }

  // 获取导购信息
  async getGuideInfo(params) {
    return await wxApi.request({
      url: api.ambassador.guideInfo,
      loading: true,
      data: {
        userId: params.guideId,
        storeId: params.storeId,
      },
    });
  }

  // 是否为大使身份
  async checkIdentity(guideInfo) {
    const { appInfo, userInfo } = this.props;

    if (appInfo.isPromoter) {
      // 获取门店名称
      wxApi
        .request({
          url: api.ambassador.detail,
          loading: true,
          data: {
            auditStatus: 1, // 审核通过
            memberId: userInfo.id,
          },
        })
        .then((res) => {
          if (res.data) {
            this.setState({
              storeName: res.data.storeName,
              isAmbassador: true,
            });

            // 企微推送招募失败消息
            this.sendWrongMsg(guideInfo);
          }
        });
    } else {
      const isGuider = await this.checkMemberIsGuider();

      // 导购默认大使
      if (isGuider.data) {
        this.setState({
          action: 'isGuider',
          isAmbassador: true,
        });

        // 企微推送招募失败消息
        this.sendWrongMsg(guideInfo);
        return false;
      }

      // 进入申请流程
      const { data } = await this.getGuideInfo(guideInfo);

      this.setState(
        {
          guideInfo: {
            storeName: data.storeName,
            name: data.employeeName,
            ...guideInfo,
          },
        },

        () => {
          this.apiQRCodeByUniqueKey();
        }
      );
    }
  }

  // 读取缓存二维码
  getStoredQrode() {
    const { guideInfo } = this.state as Record<string, any>;

    return wxApi.request({
      url: api.ambassador.findGuideQrcodeUrl,
      quite: true,
      loading: true,
      data: {
        userId: guideInfo.guideId,
      },
    });
  }

  // 确认客户是否为导购
  checkMemberIsGuider() {
    const { userInfo } = this.props;

    return wxApi.request({
      url: api.ambassador.checkMemberIsGuider,
      loading: true,
      data: {
        memberId: userInfo.id,
      },
    });
  }

  // 企微提醒导购，对方已是其他门店大使
  sendWrongMsg(guideInfo) {
    const { userInfo } = this.props;

    wxApi.request({
      url: api.ambassador.sendMemberBindMsg,
      quite: true,
      method: 'POST',
      data: {
        employeeId: guideInfo.guideId,
        memberId: userInfo.id,
      },
    });
  }

  // 获取协议
  getAgreement() {
    wxApi
      .request({
        url: api.ambassador.agreement,
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            agreement: res.data.content,
          });
        }
      });
  }

  // 再次生成二维码(视觉展示)
  async apiQRCodeByUniqueKey() {
    const { guideInfo } = this.state as Record<string, any>;

    // 读取缓存二维码 {门店id: 二维码地址}
    try {
      const qrResult = await this.getStoredQrode();
      let qrStorage = qrResult.data;

      if (qrStorage) {
        qrStorage = JSON.parse(qrStorage);

        if (qrStorage.guideFace) {
          guideInfo.guideFace = qrStorage.guideFace;

          this.setState({ guideInfo });
        }

        if (qrStorage[guideInfo.storeId]) {
          this.setState({
            qrCodeUrl: qrStorage[guideInfo.storeId],
          });

          return false;
        }
      }
    } catch (e) {}

    // 如果没有二维码，重新生成
    const params = { ...guideInfo };

    params.name = undefined;
    params.storeName = undefined;
    params.refBzName = '招募推广大使';

    const publicArguments = shareUtil.buildSharePublicArguments({ bz: '', forServer: true });
    const scene = Object.assign({}, publicArguments, params);

    wxApi
      .request({
        url: api.generatorMapperQrCode,
        loading: true,
        data: {
          shareParam: JSON.stringify(scene),
          urlPath: scene.page,
          //type: null
        },
      })
      .then((res) => {
        this.setState({
          qrCodeUrl: res.data,
        });
      });
  }

  radioChange = () => {
    this.setState({
      checked: !this.state.checked,
    });
  };

  // 申请流程
  onSubmit = () => {
    const { checked, guideInfo } = this.state as Record<string, any>;

    if (!checked) {
      return wxApi.showToast({
        title: '请阅读并同意推广大使服务协议',
        icon: 'none',
      });
    }

    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/promotion-amb/apply/index',
      data: {
        ...guideInfo,
      },
    });
  };

  /**
   * 打开协议会话
   */
  showAgreementDialog = (e) => {
    e.stopPropagation();
    this.refAgreementDialogCMPT.current.show({
      scale: 1,
    });
  };

  /**
   * 关闭协议会话
   */
  hideAgreementDialog = () => {
    this.refAgreementDialogCMPT.current.hide();
  };

  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    this.setState({
      tmpStyle,
    });

    console.log(tmpStyle, 'tmpStyle');
    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { action, storeName, isAmbassador, qrCodeUrl, cdnImg, guideInfo, checked, agreement, tmpStyle } = this
      .state as Record<string, any>;

    return (
      <View data-fixme='03 add view wrapper. need more test' data-scoped='wk-ppp-Poster' className='wk-ppp-Poster'>
        <AuthUser onReady={this.userInfoReady}>
          {isAmbassador ? (
            <Block>
              <Tips action={action} storeName={storeName}></Tips>
            </Block>
          ) : (
            qrCodeUrl && (
              <Block>
                <View className='poster-bg'>
                  <Image className='bg-img' src={cdnImg.shareBg02}></Image>
                  <View className='poster-content'>
                    <Image className='guide-face' src={guideInfo.guideFace}></Image>
                    <View className='guide-name'>{guideInfo.name}</View>
                    <View className='store-name'>{guideInfo.storeName}</View>
                    <View className='tips'>邀请你成为本店的推广大使</View>
                    <Image className='qr-code' src={qrCodeUrl}></Image>
                  </View>
                </View>
                <View className='bottom-layer'>
                  <View className='agreement'>
                    <Label onClick={this.radioChange}>
                      <Radio className='radio' color={tmpStyle.btnColor} value={checked} checked={checked}></Radio>
                      我已阅读并同意
                      <Text className='agreement-text' onClick={this.showAgreementDialog}>
                        《推广大使服务协议》
                      </Text>
                    </Label>
                  </View>
                  <Button className='apply-btn' onClick={this.onSubmit}>
                    我要成为推广大使
                  </Button>
                </View>
                {/*  规则详情  */}
                <AnimatDialog ref={this.refAgreementDialogCMPT} animClass='agreement-dialog'>
                  <View className='agreement-dialog'>
                    <View className='content'>
                      <View className='title'>推广大使服务协议</View>
                      <ScrollView scrollY className='scroll'>
                        <Text>{agreement}</Text>
                      </ScrollView>
                    </View>
                    <Image className='icon-close' src={commonImg.close} onClick={this.hideAgreementDialog}></Image>
                  </View>
                </AnimatDialog>
              </Block>
            )
          )}
        </AuthUser>
      </View>
    );
  }
}

export default Poster;
