export const joinGroup = 2;
export const bargain = 3;
export const couponpocket = 1;

export const contentTypeList = [
  {
    name: '代金卡包',
    key: couponpocket
  },
  {
    name: '拼团活动',
    key: joinGroup
  },
  {
    name: '砍价活动',
    key: bargain
  }
];

export const contentTypeObj = {
  [couponpocket]: {
    name: '代金卡包',
    formLabel: '选择代金卡包',
    value: 'cardPackage',
    detailUrl: 'sub-packages/marketing-package/pages/card-pack/detail/index',
    label: '券包价',
    indicators: [
      {
        name: '券包支付成功',
        value: 1
      },
      {
        name: '券核销成功',
        value: 2
      }
    ]
  },
  [joinGroup]: {
    name: '拼团活动',
    formLabel: '选择拼团',
    value: 'joinGroup',
    detailUrl: 'sub-packages/marketing-package/pages/group/detail/index',
    label: '团购价',
    indicators: [
      {
        name: '拼团成功',
        value: 3
      },
      {
        name: '超过订单退款时间',
        value: 4
      }
    ]
  },
  [bargain]: {
    name: '砍价活动',
    formLabel: '选择砍价',
    value: 'bargain',
    detailUrl: 'sub-packages/marketing-package/pages/cut-price/detail/index',
    label: '可砍最低价',
    indicators: [
      {
        name: '砍价订单支付成功',
        value: 5
      },
      {
        name: '超过订单退款时间',
        value: 6
      }
    ]
  }
};

export const profitShardingMode = [
  {
    name: '券包支付成功',
    value: 1
  },
  {
    name: '券核销成功',
    value: 2
  },
  {
    name: '拼团成功',
    value: 3
  },
  {
    name: '超过订单退款时间',
    value: 4
  },
  {
    name: '砍价订单支付成功',
    value: 5
  },
  {
    name: '超过订单退款时间',
    value: 6
  }
];
