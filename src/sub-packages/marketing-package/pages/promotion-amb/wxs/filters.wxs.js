export default {
  dateFormat: function (dateStr) {
    return dateStr.replace('-', '年') + '月';
  },

  dateSplit: function (dateStr) {
    return dateStr.split(' ')[0];
  },
};
