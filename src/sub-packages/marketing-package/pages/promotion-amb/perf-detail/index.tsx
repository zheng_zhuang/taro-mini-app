import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { connect } from 'react-redux';

import ActivityItem from '../components/activity-item/index';
import Empty from '@/wxat-common/components/empty/empty';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';

import './index.scss';

const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};

type StateProps = {};

type IProps = StateProps;

const mapStateToProps = () => {};

interface PerfDetail {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class PerfDetail extends React.Component {
  $router = $getRouter();
  constructor(props) {
    super(props);
  }

  state = {
    error: false,
    orderList: [],
    nothing: false,
    loadMoreStatus: loadMoreStatus.HIDE,
    tmpStyle: {},
    activityDetailItem: {},
  };

  pageNo = 1;
  hasMore = true;
  item: any;

  componentDidMount() {
    this.item = this.$router.params;
    this.queryDetailById(this.item.id);
    this.fetchOrderList();
    this.getTemplateStyle();
  }

  onPullDownRefresh() {
    this.pageNo = 1;
    this.hasMore = true;
    this.fetchOrderList(true);
  }

  onReachBottom() {
    if (this.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.fetchOrderList();
    }
  }

  onRetryLoadMore() {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.fetchOrderList();
  }

  queryDetailById(id) {
    wxApi
      .request({
        url: api.ambassador.queryDetailById,
        loading: true,
        data: {
          id,
        },
      })
      .then((res) => {
        res.data.activityCommission = this.item.activityCommission;
        this.setState({
          activityDetailItem: res.data,
        });
      });
  }

  // 门店活动列表
  fetchOrderList(isFromPullDown?) {
    const item = this.item;

    this.setState({
      error: false,
    });

    wxApi
      .request({
        url: api.ambassador.querycommissionlist,
        loading: true,
        data: {
          pageNo: this.pageNo,
          pageSize: 10,
          activityId: item.id,
          startDate: item.chooseDate.split('|')[0],
          endDate: item.chooseDate.split('|')[1],
        },
      })
      .then((res) => {
        let orderList = res.data || [];

        if (this.pageNo !== 1) {
          orderList = this.state.orderList.concat(orderList);
        }

        this.pageNo++;
        this.setState({
          hasMore: orderList.length !== res.totalCount,
          nothing: orderList.length ? false : true,
          orderList,
        });
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  }

  isLoadMoreRequest() {
    return this.pageNo > 1;
  }

  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    this.setState({
      tmpStyle,
    });

    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { orderList, hasMore, nothing, error, loadMoreStatus, tmpStyle, activityDetailItem } = this.state as Record<
      string,
      any
    >;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-ppp-PerfDetail' className='wk-ppp-PerfDetail'>
        <ActivityItem item={activityDetailItem}></ActivityItem>
        {/*  订单列表  */}
        <View className='list'>
          {orderList.map((item) => {
            return (
              <View className='incomes-item' key={item.id}>
                <View className='info'>
                  <View className='info-item'>
                    <Text className='label'>订单编号：</Text>
                    {item.orderNo}
                  </View>
                  <View className='info-item'>
                    <Text className='label'>下单客户：</Text>
                    {!!item.orderUserAvatar && <Image className='user-face' src={item.orderUserAvatar}></Image>}
                    {item.orderUserName || item.orderUserNickname}
                  </View>
                  <View className='info-item'>
                    <Text className='label'>下单时间：</Text>
                    {filters.dateFormat(item.createTime, 'yyyy-MM-dd hh:mm')}
                  </View>
                </View>
                <View className='incomes'>
                  <View className='label'>已入账</View>
                  <View className='money' style={_safe_style_('color: ' + tmpStyle.btnColor)}>
                    {'+¥' + filters.moneyFilter(item.commission, true)}
                  </View>
                </View>
              </View>
            );
          })}
        </View>
        {!!nothing && <Empty message='暂无明细'></Empty>}
        {!!error && <Error></Error>}
        {!!hasMore && <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>}
        <View className='cushion'></View>
        {/*  列表模板  */}
      </View>
    );
  }
}

export default PerfDetail;
