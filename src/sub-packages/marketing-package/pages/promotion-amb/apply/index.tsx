import { $getRouter } from 'wk-taro-platform';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template';
import { connect } from 'react-redux';
import React, { ComponentClass } from 'react';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';

import Tips from '../components/tips/index';

import './index.scss';

type StateProps = {
  userInfo: any;
};

const mapStateToProps = (state) => {
  return {
    userInfo: state.base.userInfo,
  };
};

interface Apply {
  props: StateProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Apply extends React.Component {
  $router = $getRouter();
  state = {
    guideInfo: {},
    tmpStyle: {},
  };

  componentDidMount() {
    this.setState({
      guideInfo: this.$router.params,
    });

    this.getTemplateStyle();
  }

  onSubmit = () => {
    const { userInfo } = this.props;
    const { guideInfo } = this.state as Record<string, any>;
    const params = {
      employeeId: guideInfo.guideId,
      memberId: userInfo.id,
      storeId: guideInfo.storeId,
      phone: userInfo.phone,
    };

    // 强制订阅
    const ids = [subscribeEnum.AMB_APPLY_RESULT.value];

    subscribeMsg.sendMessage(ids).then((res) => {
      if (!Object.values(res || {}).includes('accept')) {
        return wxApi.showToast({
          title: '请先订阅消息',
          icon: 'none',
        });
      }

      wxApi
        .request({
          url: api.ambassador.apply,
          method: 'POST',
          loading: true,
          data: params,
        })
        .then((res) => {
          this.setState({
            applySuccess: true,
          });
        })
        .catch((error) => {
          console.log('ERROR: ', error);
        });
    });
  };

  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    this.setState({
      tmpStyle,
    });

    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { userInfo } = this.props;
    const { applySuccess, guideInfo, tmpStyle } = this.state as Record<string, any>;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-ppa-Apply' className='wk-ppa-Apply'>
        {applySuccess ? (
          <Tips action='applySuccess'></Tips>
        ) : (
          <Block>
            <View className='form'>
              <View className='form-item'>
                <View className='label'>微信信息</View>
                <View className='content'>
                  <Image className='user-face' src={userInfo.avatarImgUrl}></Image>
                  {userInfo.nickname}
                </View>
              </View>
              <View className='form-item'>
                <View className='label'>手机号</View>
                <View className='content'>{userInfo.phone}</View>
              </View>
              <View className='form-item'>
                <View className='label'>推广门店</View>
                <View className='content'>{guideInfo.storeName}</View>
              </View>
              <View className='form-item'>
                <View className='label'>服务顾问</View>
                <View className='content'>{guideInfo.name}</View>
              </View>
            </View>
            <Button
              className='common-btn'
              style={_safe_style_('background:' + tmpStyle.btnColor)}
              onClick={this.onSubmit}
            >
              确认并提交
            </Button>
          </Block>
        )}
      </View>
    );
  }
}

export default Apply as ComponentClass<StateProps>;
