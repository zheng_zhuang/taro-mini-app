import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { connect } from 'react-redux';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import shareUtil from '@/wxat-common/utils/share';
import canvasHelper from '@/wxat-common/utils/canvas-helper';
import template from '@/wxat-common/utils/template';

import './index.scss';

let rpx = 1;

type StateProps = {
  userInfo: Record<string, any>;
  sessionId: string;
};

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    sessionId: state.base.sessionId,
    userInfo: state.base.userInfo,
  };
};

interface Share {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Share extends React.Component {
  $router = $getRouter();
  state = {
    cdnImg: cdnResConfig.promotion_amb,
    guideInfo: null,
    qrCodeUrl: '',
    filePath: '',
  };

  componentDidMount() {
    const { sessionId } = this.props;

    if (sessionId) {
      this.apiQRCodeByUniqueKey();
    }
    this.getTemplateStyle();
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.apiQRCodeByUniqueKey();
    }
  }

  // 获取二维码
  async apiQRCodeByUniqueKey() {
    const guideInfo = this.$router.params;
    const { userInfo } = this.props;

    if (!guideInfo.guideId) {
      return false;
    }

    await this.setState({
      guideInfo,
    });

    const params = {
      page: 'sub-packages/marketing-package/pages/promotion-amb/poster/index',
      guideFace: userInfo.avatarImgUrl,
      guideId: guideInfo.guideId,
      storeId: guideInfo.storeId,
      refStoreId: guideInfo.storeId,
      refBzName: '招募推广大使',
    };

    const publicArguments = shareUtil.buildSharePublicArguments({ bz: '', forServer: true });
    const scene = Object.assign({}, publicArguments, params);

    wxApi
      .request({
        url: api.generatorMapperQrCode,
        loading: true,
        data: {
          shareParam: JSON.stringify(scene),
          urlPath: scene.page,
          //type: null
        },
      })
      .then((res) => {
        this.setState(
          {
            qrCodeUrl: res.data,
          },

          () => {
            // 绘制海报
            this.initCanvas();
          }
        );

        //缓存二维码
        this.handleStoredQrode(guideInfo.storeId, res.data);
      });
  }

  // 缓存二维码
  async handleStoredQrode(storeId, value) {
    const { guideInfo } = this.state as Record<string, any>;

    const { data } = await wxApi.request({
      url: api.ambassador.findGuideQrcodeUrl,
      quite: true,
      data: {
        userId: guideInfo.guideId,
      },

      loading: true,
    });

    const qrStorage = JSON.parse(data || null) || {};
    qrStorage[storeId] = value;
    qrStorage.guideFace = this.props.userInfo.avatarImgUrl;

    wxApi.request({
      url: api.ambassador.addGuideQrcodeUrl,
      method: 'POST',
      quite: true,
      data: {
        userId: guideInfo.guideId,
        qrcodeUrl: JSON.stringify(qrStorage),
      },
    });
  }

  /** 初始化画布 */
  initCanvas() {
    rpx = (wxApi.getSystemInfoSync().windowWidth / 750) * 2;

    const ctx = wxApi.createCanvasContext('shareCanvas', this);
    this.drawCarte(ctx);
  }

  drawCarte(ctx) {
    const { cdnImg, qrCodeUrl, guideInfo } = this.state as Record<string, any>;
    const { userInfo } = this.props;

    Promise.all([
      wxApi.getImageInfo({ src: cdnImg.shareBg }),
      wxApi.getImageInfo({ src: userInfo.avatarImgUrl }),
      wxApi.getImageInfo({ src: qrCodeUrl }),
    ])
      .then((res) => {
        // 背景
        canvasHelper.drawImage(ctx, 0, 0, 282 * rpx, 453 * rpx, res[0].path);

        // 圆形头像
        canvasHelper.drawCircleImage(ctx, 140 * rpx, 285 * rpx, 18 * rpx, res[1].path);

        // 姓名
        canvasHelper.drawText(ctx, guideInfo.name, 140 * rpx, 318 * rpx, {
          textAlign: 'center',
          fillStyle: '#000',
          font: 12 * rpx,
          blod: true,
        });

        // 门店
        canvasHelper.drawText(ctx, guideInfo.storeName, 140 * rpx, 335 * rpx, {
          textAlign: 'center',
          fillStyle: '#888',
          font: 9 * rpx,
        });

        // 邀请提示
        canvasHelper.drawText(ctx, '邀请你成为本店的推广大使', 140 * rpx, 353 * rpx, {
          textAlign: 'center',
          fillStyle: '#FF753D',
          font: 9 * rpx,
        });

        // 二维码
        canvasHelper.drawImage(ctx, 108 * rpx, 360 * rpx, 64 * rpx, 64 * rpx, res[2].path);

        wxApi.showLoading({ title: '正在生成海报' });
        ctx.draw(true, () => {
          wxApi.canvasToTempFilePath({
            canvasId: 'shareCanvas',
            success: (res) => {
              this.setState({
                filePath: res.tempFilePath,
              });
            },
            complete: () => {
              wxApi.hideLoading();
            },
          });
        });
      })
      .catch((err) => {
        wxApi.showToast({
          title: '生成海报失败，' + err.errMsg,
          icon: 'none',
        });
      });
  }

  /**
   * 保存图片前检查访问相机的权限
   */
  handleSaveImage = () => {
    if (!this.state.filePath) return;
    wxApi.$saveImage(this.state.filePath);
  };

  /**
   * 发送给客户（预览图片）
   */
  handlePreviewImg = () => {
    wxApi.previewImage({
      urls: [this.state.filePath],
    });
  };

  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { cdnImg, filePath } = this.state as Record<string, any>;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pps-Share' className='wk-pps-Share'>
        {!!filePath && (
          <Block>
            <View className='preview'>
              <Image className='preview-img' src={filePath}></Image>
            </View>
            <View className='operation'>
              <View className='oper-item' onClick={this.handleSaveImage}>
                <Image src={cdnImg.save} className='oper-icon'></Image>
                <View className='oper-name'>保存图片</View>
              </View>
              <View className='oper-item' onClick={this.handlePreviewImg}>
                <Image src={cdnImg.share} className='oper-icon'></Image>
                <View className='oper-name'>发送客户</View>
              </View>
            </View>
          </Block>
        )}

        <Canvas canvasId='shareCanvas' className='promotion-canvas'></Canvas>
      </View>
    );
  }
}

export default Share;
