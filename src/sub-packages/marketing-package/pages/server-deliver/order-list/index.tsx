import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, Text, ScrollView, Image, Input } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import state from '@/state/index.js';
import imageUtils from '@/wxat-common/utils/image.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import util from '@/wxat-common/utils/util.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import report from '@/sdks/buried/report/index.js'
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import ChooseAttributeDialog from '@/wxat-common/components/choose-attribute-dialog/index';
import Empty from '@/wxat-common/components/empty/empty';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import './index.scss';
import store from '@/store';
const storeData = store.getState();
const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};
class OrderList extends React.Component {
  state = {
    tmpStyle: {}, // 主题模板配置
    fircurrentIndex: 0, //默认选中一级分类的第一个
    seccurrentIndex: 0, //默认选中二级分类的第一个
    firstLevelList: [], //一级分类列表
    serviceCateGoryId: null, //一级列表的id
    secondid: null, //二级列表的id
    categoryLevel2: [], //二级分类列表
    dataSource: [], //商品列表
    pageNo: 1, //商品分页
    hasMore: true, //是否有更多商品数据
    totalCount: 0,
    goodsDetail: null,
    serviceCateGoryIdList: '',
    skuDialogNode: React.createRef(),
    chooseAttributeDialogNode: React.createRef(),
  }

  /**
  * 生命周期函数--监听页面加载
  */
  onLoad() {
    this.getTemplateStyle();
    this.getPcProductList();
  }

  /**
  * 生命周期函数--监听页面显示
  */
  componentDidShow () {
    this.computedCount();
    this.setState(
      {
        fircurrentIndex: 0,
        seccurrentIndex: 0,
      },
      () => {
        this.getFirstLevelList();
      }
    )

  }

  /**
  * 页面上拉触底事件的处理函数
  */
  onReachBottom () {
    if (this.state.hasMore) {
      let pageNo = this.state.pageNo++;
      this.setState({
        pageNo,
      },()=>{
        // this.getLevel2Data();
      });
    }
  }

  //获取pc配置的商品id
  getPcProductList = () => {
    const router = Taro.getCurrentInstance().router
    let serviceCateGoryIdList = !!decodeURIComponent(router.params.id)
      ? JSON.parse(decodeURIComponent(router.params.id))
      : [];
    if (serviceCateGoryIdList.length > 0) {
      const idsList = [];
      const parentIdList = [];
      let resList = serviceCateGoryIdList.filter((num) => {
        return parseInt(num.parentId) > 0;
      });

      serviceCateGoryIdList.forEach((item) => {
        parentIdList.push(item.id);
        idsList.push(item.id);
        if (resList && resList.length != serviceCateGoryIdList.length) {
          if (item.parentId != 0) {
            idsList.push(item.parentId);
          }
        }
      });
      let newidsList = idsList.join(',');
      let newparentIdList = parentIdList.join(',');

      this.setState({
        serviceCateGoryIdList: newidsList,
        parentIdList: newparentIdList,
      });
    } else {
      this.setState({
        serviceCateGoryIdList: '',
        parentIdList: '',
      });
    }
  }

  //计算初始count
  computedCount = () => {
    let current = Taro.getStorageSync('storageList') || [];
    let count = 0;
    current.forEach((item) => {
      count = item.count + count;
    });
    this.setState({
      totalCount: count,
    });
  }

  minTotalCount = () => {
    this.setState({
      totalCount: this.state.totalCount - 1,
    });
  }

  addTotalCount = () => {
    this.setState({
      totalCount: this.state.totalCount + 1,
    });
  }

  //sku 商品
  gosku = (e) => {
    let item = e.currentTarget.dataset.item;
    item.categoryType = 5;
    this.state.skuDialogNode.current.show(item);
  }

  //获取主题色
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //获取配送列表
  getFirstLevelList = () => {
    wxApi
      .request({
        url: api.livingService.getNewFir,
        method: 'GET',
        loading: true,
        data: {
          serviceCateGoryIdList: this.state.serviceCateGoryIdList,
          categoryType: 5,
        },
      })
      .then((res) => {
        let firstLevelList = res.data || [];
        if (firstLevelList.length == 0) return;
        let serviceCateGoryId = res.data && res.data[0].id;
        this.setState({
          firstLevelList,
          serviceCateGoryId,
        },()=>{
          this.getLevel2Data();
        });
      });
  }

  //查询二级分类
  getLevel2Data () {
    wxApi
      .request({
        url: api.livingService.getNewSec,
        method: 'GET',
        loading: true,
        data: {
          // serviceCateGoryIdList:"",
          parentId: this.state.serviceCateGoryId,
          storeId: state.base.currentStore.id,
          categoryType: 5,
        },
      })
      .then((res) => {
        let categoryLevel2 = res.data || [];
        if (categoryLevel2.length && categoryLevel2) {
          let secondid = categoryLevel2[0].id;
          this.queryItemSku(res.data[0].id);
          this.setState({
            categoryLevel2: this.flatterArray(categoryLevel2),
            secondid,
          });
        } else {
          this.setState(
            {
              categoryLevel2,
            },
            () => {
              // 没有子分类  展示商品数据
              this.queryItemSku();
            }
          )
        }
      })
  }

  flatterArray = (arr) => {
    let x = [];
    arr.forEach((item) => {
      if (item.childrenCategory && item.childrenCategory.length) {
        x = x.concat(this.flatterArray(item.childrenCategory));
      } else {
        x.push(item);
      }
    });
    return x;
  }

  /*
  *查询分类中的子数据
  */
  queryItemSku = (id) => {
    const categoryId = id || this.state.firstLevelList[this.state.fircurrentIndex].id;
    const params = {
      status: 1,
      deliveryCategoryId: categoryId,
      sourceType: 1,
    };

    params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];

    wxApi
      .request({
        url: api.classify.itemSkuList,
        loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true) {
          let hasMore = true;
          let pageNo = this.state.pageNo;
          let dataSource;
          let resData = res.data || [];
          resData.forEach((item) => {
            const thumbnail = item.thumbnail || (!!item.wxItem ? item.wxItem.thumbnail : '');
            item.thumbnailMin = imageUtils.thumbnailMin(thumbnail);
            item.thumbnailMid = imageUtils.thumbnailMid(thumbnail);
            item.count = 0;
          });

          if (this.isLoadMoreRequest()) {
            dataSource = this.state.dataSource.concat(resData);
          } else {
            dataSource = resData;
          }
          if (dataSource.length === res.totalCount) {
            hasMore = false;
          } else {
            pageNo += 1;
          }
          this.calcCartNumber(dataSource);

          this.setState({
            dataSource,
            showType: 0,
            hasMore,
            pageNo,
          });

          const goodsExposure = [];
          resData.forEach((item) => {
            let itemNo;
            if (!!(itemNo = item.wxItem) && !!(itemNo = itemNo.itemNo)) {
              goodsExposure.push(itemNo);
            }
          });
          if (!!goodsExposure && !!goodsExposure.length) {
            report.goodsExposure(goodsExposure.join(','));
          }
        }
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR || '',
          });
        }
      });
  }

  // 计算商品在购物车添加的数量
  calcCartNumber = (data) => {
    let dataSource = data ? data : this.state.dataSource;
    let list = Taro.getStorageSync('storageList') || [];
    if (list.length > 0) {
      dataSource.forEach((item) => {
        list.forEach((value) => {
          if (item.wxItem.itemNo == value.wxItem.itemNo) {
            item.count += value.count;
          }
        })
      })
    }
    this.setState({
      dataSource,
    })
  }

  // 计算多规格商品在购物车添加的数量
  skucalcCartNumber = (data) => {
    let dataSource = data ? data : this.state.dataSource;
    let list = Taro.getStorageSync('storageList') || [];
    if (list.length > 0) {
      dataSource.forEach((item) => {
        item.count = 0;
        list.forEach((value) => {
          if (item.wxItem.itemNo == value.wxItem.itemNo) {
            item.count += value.count;
          }
        });
      });
    }
    this.setState({
      dataSource,
    })
  }

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  }

  //购物车添加
  clickPlus = (e, index) => {
    e.stopPropagation()
    let dataSource = this.state.dataSource;
    let itemCount = dataSource[index].count;
    itemCount = itemCount + 1;

    dataSource[index].count = itemCount;
    this.setState({
      dataSource: dataSource,
    });

    this.addTotalCount();

    //  this.setProduct();
    //优先获取判断再缓存
    let data = {
      ...this.state.dataSource[index],
      count: 1,
    };

    let current = Taro.getStorageSync('storageList');
    let newArr = [];
    if (current && current.length) {
      let item = null;
      if (this.state.dataSource[index].skuId) {
        item = current.find((i) => i.skuId === this.state.dataSource[index].skuId);
      } else {
        item = current.find((i) => i.wxItem.itemNo === this.state.dataSource[index].wxItem.itemNo);
      }
      if (!!item) {
        item.count = item.count + 1;
        newArr = current;
      } else {
        newArr = [data, ...current];
      }
    } else {
      newArr = [data, ...current];
    }

    Taro.setStorageSync('storageList', newArr);
    Taro.setStorageSync('storageOrderList', newArr); //下单缓存
  }

  clickSkuPlus = (e, item) => {
    e.stopPropagation()
    item.wxItem.skuType = 'skuList';
    item.wxItem.thumbnail = item.thumbnailMin
    item.wxItem.tenantPrice = item.tenantPrice;
    item.categoryType = 5;
    this.setState(
      {
        goodsDetail: item,
      },
      () => {
        this.showAttrDialog(); // 显示商品属性对话弹框
      }
    )
  
  }

  //购物车减少
  clickMinus = (e, index) => {
    e.stopPropagation()
    this.minTotalCount();
    let dataSource = this.state.dataSource;
    let itemCount = dataSource[index].count;

    itemCount = itemCount - 1;
    dataSource[index].count = itemCount;
    this.setState({
      dataSource: dataSource,
    });

    let current = Taro.getStorageSync('storageList');
    let newArr = [];
    if (current && current.length) {
      let item = null;
      let currentIdx = null; //缓存商品的当前点击id 的index
      if (this.state.dataSource[index].skuId) {
        item = current.find((i, iIndex) => {
          currentIdx = iIndex;
          return i.skuId === this.state.dataSource[index].skuId;
        });
      } else {
        item = current.find((i, iIndex) => {
          currentIdx = iIndex;
          return i.wxItem.itemNo === this.state.dataSource[index].wxItem.itemNo;
        });
      }
      if (!!item) {
        item.count = item.count - 1;
        current.splice(currentIdx, 1);
        newArr = current;
      } else {
        newArr = [...current];
      }
    } else {
      newArr = [...current];
    }

    Taro.setStorageSync('storageList', newArr);
    Taro.setStorageSync('storageOrderList', newArr); //下单缓存
  }

  /**
  * 点击左边的分类按钮
  */
    clickClassify = (index, value) => {
    if (this.state.fircurrentIndex === index) return;
    this.setState(
      {
        fircurrentIndex: index,
        seccurrentIndex: 0,
        serviceCateGoryId: value.id,
        pageNo: 1,
        hasMore: true,
      },
      () => {
        this.getLevel2Data()
      }
    )
  }

  //点击二级分类按钮
  clicksecondClassify = (index, value) => {
    if (this.state.seccurrentIndex === index) return;
    this.setState({
      seccurrentIndex: index,
      pageNo: 1,
    },()=>{
      this.queryItemSku(value.secondId);
    });
  }

    //存数据
  // setProduct(){
  //   wx.setStorageSync('storageList', storageList)
  // },
  //跳转去购物清单
  goServerList = () => {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/living-service/service-reserve-list/index',
    });
  }

  /**
  * 跳转到配送服务下单页面
  * 2021-04-21 加入新逻辑，通过scan字段判断是否需要扫描二维码
  */
  async goServerDeliverDetail() {
    //未进行认证则优先认证
    if (!authHelper.checkAuth()) return;
    //将缓存中的storageList优先提取出来，避免重复提取，以优化代码
    let storageList = Taro.getStorageSync('storageList');
    console.log(storageList, 'storageList----storageList')
    //需要有商品才可以下单
    if (!storageList || storageList.length === 0) {
      Taro.showToast({
        title: '请选择商品',
        icon: 'none',
        duration: 2000, //持续的时间
      });
      return;
    }
    //判断是否包含需要扫码的商品
    const flag = await util.checkNeedScan(storageList);
    if (!flag) {
      wxApi.$navigateTo({
        url: '/sub-packages/order-package/pages/pay-order/index',
        data: {
          categoryType: 5,
          title: '确认订单',
        },
      });
      return;
    }
    // 循环生成格式缓存，然后扫码
    const serverGoodsList = storageList.map(item => {
      if (item.itemNo) return item
      const { wxItem } = item
      const _item = {
        itemNo: wxItem.itemNo,
        barcode: wxItem.barcode,
        pic: wxItem.thumbnail,
        name: wxItem.name,
        noNeedPay: wxItem.noNeedPay,
        freight: wxItem.freight,
        wxItem: wxItem,
        itemCount: item.count,
        count: item.count,
        salePrice: wxItem.salePrice,
      }
      return _item
    })

    // 获取从商品详情添加的商品
    protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', serverGoodsList);
    Taro.setStorageSync('GOODS_LIST_CUT', JSON.stringify(serverGoodsList))
    window.sessionStorage.setItem('GOODS_LIST_CUT', JSON.stringify(serverGoodsList))
    if (storeData.globalData.roomCode) {
      wxApi.$navigateTo({
        url: '/sub-packages/order-package/pages/pay-order/index',
        data: {
          roomCode: state.roomCode,
          roomCodeTypeId: state.roomCodeTypeId,
          roomCodeTypeName: state.roomCodeTypeName,
          trueCode: state.roomCodeTypeName + '-' + state.roomCode,
          categoryType: 5,
          title: '确认订单',
        },
      });
    } else {
      // 转到扫码页面
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
        data: {
          isDistributionServices: true
        },
      })
    }
  }

  /*
  *跳转到配送服务商品订单详情
  */
  goGoodsDetail = (value) => {
    if (!authHelper.checkAuth()) return;
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: value.wxItem.itemNo,
        categoryType: 5,
      },
    });
  }

  //多规格加入缓存
  handlerChooseSku = (info) => {
    const thumbnail = info.thumbnail || (!!info.wxItem ? info.wxItem.thumbnail : '');
    let data = {
      ...info,
      count: 1,
      scan: this.state.goodsDetail.scan,
      thumbnailMin: imageUtils.thumbnailMin(thumbnail),
      thumbnailMid: imageUtils.thumbnailMin(thumbnail),
      skuInfo: info.skuTreeNames,
    };

    let current = Taro.getStorageSync('storageList');
    let newArr = [];
    if (current && current.length) {
      let item = null;
      if (info.skuId) {
        item = current.find((i) => i.skuId === info.skuId);
      } else {
        item = current.find((i) => i.wxItem.itemNo === info.wxItem.itemNo);
      }
      if (!!item) {
        item.count = item.count += info.itemCount;
        newArr = current;
      } else {
        data.count = info.itemCount;
        newArr = [data, ...current];
      }
    } else {
      data.count = info.itemCount;
      newArr = [data, ...current];
    }

    Taro.setStorageSync('storageList', newArr);
    Taro.setStorageSync('storageOrderList', newArr); //下单缓存
    this.computedCount();
    this.skucalcCartNumber();
    this.hideAttrDialog();
  }

  /**
  * 显示商品属性对话弹框
  */
  showAttrDialog = () => {
    this.state.chooseAttributeDialogNode.current.showAttributDialog();
  }

  /**
   * 隐藏商品属性对话弹框
   */
  hideAttrDialog = () => {
    this.state.chooseAttributeDialogNode.current.hideAttributeDialog();
  }

  render() {
    const {
      fircurrentIndex,
      tmpStyle,
      firstLevelList,
      seccurrentIndex,
      categoryLevel2,
      dataSource,
      totalCount,
      limitAmountTitle,
      goodsDetail,
    } = this.state
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpso-OrderList" className="wk-mpso-OrderList">
        <View className="classify">
          <View className="content">
            <View className="left">
              {firstLevelList &&
                firstLevelList.map((item, index) => {
                  return (
                    <View
                      key={'left-item-' + index}
                      onClick={() => {this.clickClassify(index, item)}}
                      style={_safe_style_(
                        'color:' +
                          (fircurrentIndex === index ? tmpStyle.btnColor : 'rgba(162,162,164,1)') +
                          ';background-color:' +
                          (fircurrentIndex === index ? 'rgba(255,255,255,1)' : 'rgba(240,241,245,1)') +
                          ';padding: 40rpx 0'
                      )}
                    >
                      <Text
                        className="category-item limit-line line-2"
                        style={_safe_style_(
                          'color:' +
                            (fircurrentIndex === index ? tmpStyle.btnColor : '') +
                            '; font-weight:' +
                            (fircurrentIndex === index ? 600 : '')
                        )}
                      >
                        {item.categoryName}
                      </Text>
                    </View>
                  );
                })}
            </View>
            <View className="right">
              <View className="content-container">
                {!!categoryLevel2 && categoryLevel2.length > 0 && (
                  <ScrollView className="secondary-classification" scrollX={true}>
                    {categoryLevel2 &&
                      categoryLevel2.map((item, index) => {
                        return (
                          <View
                            className={'secondary-classification-item ' + (seccurrentIndex === index ? 'cur' : '')}
                            key={'secondary-classification-item-' + index}
                            onClick={() => {this.clicksecondClassify(index, item)}}
                            style={_safe_style_(
                              seccurrentIndex === index
                                ? 'color:' + tmpStyle.btnColor + ';border-color:' + tmpStyle.btnColor
                                : ''
                            )}
                          >
                            {item.categoryName}
                          </View>
                        );
                      })}
                  </ScrollView>
                )}
                {/* 服务事项展示模式  */}
                {!!dataSource.length &&
                  dataSource.map((item, index) => {
                    return (
                      <View key={'child-item' + index} className="child-item">
                        <View
                          className="service-item"
                          onClick={() => {this.goGoodsDetail(item)}}
                        >
                          <Image className="service-itemimg" src={item.thumbnailMin} mode="aspectFill"></Image>
                          <View className="serviceBox">
                            <View className="titlebox">
                              <Text className="serviceNname">{item.wxItem.name}</Text>
                            </View>
                            {/*  <text class="subtitle">配送服务的事项东西订单的地方的副标题</text>  */}
                            <View className="bottom">
                              <View className="bottomBox">
                                <View>
                                  <View className="servicePrice">
                                    ￥<Text>{filters.moneyFilter(item.wxItem.salePrice || 0, true)}</Text>
                                    <Text className="label-discount">
                                      {'￥' + filters.moneyFilter(item.wxItem.labelPrice || 0, true)}
                                    </Text>
                                  </View>
                                  <View className="h-goods-desc">
                                    {(item.itemSalesVolume || item.wxItem.itemSalesVolume || 0) + '人已购买'}
                                  </View>
                                </View>
                                {!item.skuInfoList.length ? (
                                  <View>
                                    {item.count > 0 && (
                                      <View className="count-box">
                                        <View
                                          className="minus left-radius"
                                          onClick={(e) => {this.clickMinus(e, index)}}
                                        >
                                          -
                                        </View>
                                        <View className="num">
                                          {item.count}
                                        </View>
                                        <View
                                          className="plus right-radius"
                                          onClick={(e) => {this.clickPlus(e, index)}}
                                        >
                                          +
                                        </View>
                                      </View>
                                    )}

                                    {/* fixme 保留库存不足的逻辑  {{(formatSpuItem.stock < 1) ? 'disabled' : ''}} */}
                                    {item.count == 0 && (
                                      <View
                                        className="add-img"
                                        onClick={(e) => {this.clickPlus(e, index)}}
                                      >
                                        <Image
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                                          className="add-cart-icon"
                                          mode="aspectFill"
                                        ></Image>
                                        {/*  <text>添加</text>  */}
                                      </View>
                                    )}
                                  </View>
                                ) : (
                                  <View>
                                    <View
                                      className="add-img"
                                      onClick={(e) => {this.clickSkuPlus(e, item)}}
                                    >
                                      <Image
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                                        className="add-cart-icon"
                                        mode="aspectFill"
                                      ></Image>
                                      {item.count > 0 && <View className="cartCount">{item.count}</View>}
                                    </View>
                                  </View>
                                )}
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  })}
                {!!dataSource && dataSource.length == 0 && (
                  <Empty emptyStyle={'width:auto;'} message="敬请期待..."></Empty>
                )}
              </View>
            </View>
            <View className="bottom_box">
              <View className="iconBox" onClick={this.goServerList}>
                <View className="cart-img-box" style={_safe_style_('background:' + tmpStyle.btnColor)}>
                  <Image
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                    className="cart-imgiocn"
                  ></Image>
                </View>
                {totalCount > 0 && <View className="cartCount">{totalCount}</View>}
              </View>
              <View
                className="bottom_submite"
                style={_safe_style_('background:' + tmpStyle.btnColor + ';')}
                onClick={this.goServerDeliverDetail.bind(this)}
              >
                立即下单
              </View>
            </View>
          </View>
          {/*  选择商品属性对话弹框  */}
          {goodsDetail && (
            <ChooseAttributeDialog
              id="choose-attribute-dialog"
              ref={this.state.chooseAttributeDialogNode}
              isDistributionServices={true}
              onChoose={this.handlerChooseSku}
              limitAmountTitle={limitAmountTitle}
              goodsDetail={goodsDetail}
            ></ChooseAttributeDialog>
          )}

          {/*  <sku-dialog id="sku-dialog" ref={this.state.skuDialogNode}
          bind:addTotalCount="addTotalCount"
          bind:minTotalCount="minTotalCount"></sku-dialog>  */}
        </View>
        <HomeActivityDialog showPage="sub-packages/marketing-package/pages/server-deliver/order-list/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default OrderList;
