import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Button, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index';
import authHelper from '../../../../../wxat-common/utils/auth-helper';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox';
import template from '../../../../../wxat-common/utils/template';
import shareUtil from '../../../../../wxat-common/utils/share';
import imageUtils from '../../../../../wxat-common/utils/image';
import DetailParser from '../../../../../wxat-common/components/detail-parser/index';
import './index.scss';

import hoc from '@/hoc';
import { connect } from 'react-redux';
import screen from '@/wxat-common/utils/screen';

type StateProps = {
  tmpStyle: any;
  sessionId: string;
};

interface MicroDecorateDetail {
  props: StateProps;
}

const mapStateToProps = (state) => {
  return {
    tmpStyle: template.getTemplateStyle(),
    sessionId: state.base.sessionId,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class MicroDecorateDetail extends React.Component {
  $router = $getRouter();
  state = {
    orderDetail: {},
    recommendList: [],
  } as Record<string, any>;

  componentDidMount() {
    if (this.props.sessionId) {
      this.init();
    }
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.init();
    }
  }

  stateChange = (data) => {
    if (data && data.key === 'sessionId') {
      this.init();
    }
  };

  init = () => {
    this.getOrderDetail();
    this.getRecommendList();
  };

  openVrPreview = ({ currentTarget }) => {
    const vrLink = currentTarget.dataset.url;
    protectedMailBox.send('sub-packages/marketing-package/pages/receipt/index', 'receiptURL', vrLink);
    wxApi.$navigateTo({ url: '/sub-packages/marketing-package/pages/receipt/index', data: {} });
  };

  handleShowLargeImage = ({ currentTarget }) => {
    wxApi.previewImage({
      current: '', // 当前显示图片的http链接
      urls: [currentTarget.dataset.src], // 需要预览的图片http链接列表
    });
  };

  getRecommendList = async () => {
    const url = authHelper.checkAuth(false) ? api.micro_decorate.recommend_auth : api.micro_decorate.recommend;
    const res = await wxApi.request({
      url,
      loading: true,
      data: { id: this.$router.params.id },
    });

    const data = res.data || [];
    data.forEach((item: any) => {
      item.thumbnail = imageUtils.thumbnailOneRowTwo(item.thumbnail || '');
    });

    this.setState({ recommendList: data });
  };

  handleGoToDetail = ({ currentTarget }) => {
    const item = currentTarget.dataset.item;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/micro-decorate/detail/index',
      data: { id: item.id },
    });
  };

  getOrderDetail = async () => {
    const url = authHelper.checkAuth(false) ? api.micro_decorate.detail_auth : api.micro_decorate.detail;
    try {
      const { data } = await wxApi.request({
        url,
        data: { id: this.$router.params.id },
      });

      data.thumbnail = imageUtils.thumbnailOneRowTwo(data.thumbnail || '');

      this.setState({ orderDetail: data });
    } catch (res) {
      return null;
    }
  };

  onShareAppMessage = () => {
    const name = this.state.orderDetail.name;
    const id = this.state.orderDetail.id;
    const path = shareUtil.buildShareUrlPublicArguments({
      url: `/sub-packages/marketing-package/pages/micro-decorate/detail/index?id=${id}`,
      bz: shareUtil.ShareBZs.MICRO_DECORATE,
      bzName: `微装方案${name ? '-' + name : ''}`,
      bzId: id,
      sceneName: name,
    });

    console.log('sharePath => ', path);
    return {
      title: name,
      imageUrl: this.state.orderDetail.thumbnail,
      path,
    };
  };

  handleOrderNow = () => {
    if (!authHelper.checkAuth()) return false;
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/form-tool/index',
      data: { formId: this.state.orderDetail.formId, id: this.state.orderDetail.id },
    });
  };

  onChangeLike = async ({ currentTarget }) => {
    if (!authHelper.checkAuth()) return false;
    const item = currentTarget.dataset.item;
    const log = currentTarget.dataset.log;
    const data = {
      sceneModelId: item.id,
      likeStatus: item.isLiked ? 0 : 1,
    };

    await wxApi.request({
      url: api.micro_decorate.like,
      loading: true,
      data,
    });

    if (!log) {
      const orderDetail = this.state.orderDetail;
      orderDetail.isLiked = orderDetail.isLiked || orderDetail.isLiked != 0 ? 0 : 1;
      orderDetail.approvalNum = orderDetail.isLiked ? orderDetail.approvalNum + 1 : orderDetail.approvalNum - 1;
      this.setState({ orderDetail });
      Taro.eventCenter.trigger('midecorate-FetchData', {});
    } else {
      await this.getRecommendList();
    }
  };

  render() {
    const { orderDetail, recommendList } = this.state;
    const { tmpStyle } = this.props;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pmd-Detail' className='wk-pmd-Detail'>
        <View style={_safe_style_('padding-bottom: 110rpx;')}>
          {!!orderDetail.name && (
            <View className='decorate-header'>
              <Image
                className='item-img'
                onClick={_fixme_with_dataset_(this.handleShowLargeImage, { src: orderDetail.thumbnail })}
                src={orderDetail.thumbnail}
                mode='aspectFill'
              ></Image>
              {!!orderDetail.vrUrl && (
                <Image
                  className='vr-btn'
                  onClick={_fixme_with_dataset_(this.openVrPreview, { url: orderDetail.vrUrl })}
                  src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/view.png'
                ></Image>
              )}

              <View className='item-info'>
                <View>
                  <View className='item-tit'>{orderDetail.name}</View>
                  <View className='bottom-info'>
                    <View className='item-icon'>
                      <Image
                        className='icon-img'
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/order.png'
                        mode='aspectFill'
                      ></Image>
                      {orderDetail.appointmentNum + '人预约'}
                    </View>
                    <View className='item-icon' style={_safe_style_('margin-left:40rpx')}>
                      <Image
                        className='icon-img'
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/look.png'
                        mode='aspectFill'
                      ></Image>
                      {orderDetail.viewNum}
                    </View>
                  </View>
                </View>
              </View>
              <View className='bottom-label'>
                {orderDetail.labels.map((i, index) => {
                  return (
                    <View className='item-label' key={index}>
                      {i.label}
                    </View>
                  );
                })}
              </View>
            </View>
          )}

          {!!orderDetail.desc && <DetailParser itemDescribe={orderDetail.desc} showHeader={false}></DetailParser>}

          {!!(!!recommendList && !!recommendList.length) && (
            <View className='recommend-container'>
              <View className='recommend-tit'>推荐方案</View>
              {recommendList.map((item, index) => {
                return (
                  <View className='decorate-item' key={index}>
                    <Image
                      className='item-img'
                      onClick={_fixme_with_dataset_(this.handleGoToDetail, { item: item })}
                      src={item.thumbnail}
                      mode='aspectFill'
                    ></Image>
                    {!!item.vrUrl && (
                      <Image
                        className='vr-btn'
                        onClick={_fixme_with_dataset_(this.openVrPreview, { url: item.vrUrl })}
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/view.png'
                      ></Image>
                    )}

                    <View className='item-tit'>{item.name}</View>
                    <View className='bottom-info'>
                      <View className='item-icon'>
                        <Image
                          className='icon-img'
                          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/order.png'
                          mode='aspectFill'
                        ></Image>
                        {item.appointmentNum + '人预约'}
                      </View>
                      <View className='item-icon' style={_safe_style_('margin-left:40rpx')}>
                        <Image
                          className='icon-img'
                          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/look.png'
                          mode='aspectFill'
                        ></Image>
                        {item.viewNum}
                      </View>
                      <View
                        style={_safe_style_('flex:1;text-align:right')}
                        className='item-icon icon-like'
                        onClick={_fixme_with_dataset_(this.onChangeLike, { log: 'recommend', item: item })}
                      >
                        {item.isLiked ? (
                          <Image
                            className='icon-img icon-like'
                            src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like-checked.png'
                          ></Image>
                        ) : (
                          <Image
                            className='icon-img'
                            src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like.png'
                            mode='aspectFill'
                          ></Image>
                        )}

                        {item.approvalNum}
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          )}
        </View>
        <View className={'bottom-btn-container' + (screen.isFullScreenPhone ? ' fix-full-screen' : '')}>
          <View className='image-btn-box'>
            <View className='image-btn'>
              <ButtonWithOpenType openType='share' className='btn-share'>
                <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-share.png"></Image>
                <Text className='btn-share-text'>分享</Text>
              </ButtonWithOpenType>
            </View>
            <View className='image-btn'>
              <Button className='btn-share' onClick={_fixme_with_dataset_(this.onChangeLike, { item: orderDetail })}>
                {orderDetail.isLiked ? (
                  <Image
                    className='btn-icon'
                    src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like-btn-ischecked.png'
                  ></Image>
                ) : (
                  <Image
                    className='btn-icon'
                    src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like-btn.png'
                    mode='aspectFill'
                  ></Image>
                )}

                <Text className='btn-share-text'>{orderDetail.approvalNum}</Text>
              </Button>
            </View>
          </View>
          <View className='btn-box'>
            <View
              className='btn'
              onClick={this.handleOrderNow}
              style={_safe_style_('background-color: ' + tmpStyle.btnColor)}
            >
              <Text>预约</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default MicroDecorateDetail;
