import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Block, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index';
import Empty from '../../../../../wxat-common/components/empty/empty';
import template from '@/wxat-common/utils/template';
import './index.scss';
import hoc from '@/hoc';

@hoc
class MicroDecorateOrder extends React.Component {
  state = {
    orderList: [],
    pageNo: 1,
    hasMore: true, // 是否可以加载更多
  };

  componentDidMount() {
    this.getTemplateStyle();
  }

  componentDidShow() {
    this.getOrderList();
  }

  onReachBottom = () => {
    if (this.state.hasMore) {
      this.setState({ pageNo: this.state.pageNo + 1 }, () => this.getOrderList());
    }
  };

  // 获取分组列表
  getOrderList = async () => {
    const res = await wxApi.request({
      url: api.micro_decorate.formList,
      loading: true,
      data: { pageNo: this.state.pageNo, pageSize: 10 },
    });

    const data = res.data || [];
    let renderList = [];
    if (this.isLoadMoreRequest()) {
      renderList = this.state.orderList.concat(data);
    } else {
      renderList = data;
    }
    const hasMore = renderList.length < res.totalCount;
    this.setState({ orderList: renderList, hasMore });
  };

  handleGoToPlan = ({ currentTarget }) => {
    const id = currentTarget.dataset.item.sceneModelId;
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/micro-decorate/detail/index',
      data: { id },
    });
  };

  handleGoToOrder = ({ currentTarget }) => {
    const id = currentTarget.dataset.item.submitFormId;
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/micro-decorate/order-detail/index',
      data: { id },
    });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();

    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { orderList } = this.state;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pmo-Order' className='wk-pmo-Order'>
        {orderList && orderList.length ? (
          <View className='page-container'>
            {orderList.map((item: any, index) => {
              return (
                <View className='order-item' key={index}>
                  <View className='order-info'>
                    <Image className='item-img' src={item.cover} mode='aspectFill'></Image>
                    <View className='item-time'>{filters.dateFormat(item.submitDate, 'yyyy-MM-dd hh:mm')}</View>
                  </View>
                  <View className='order-operation'>
                    <View className='item-tit'>{item.name}</View>
                    <View className='btn-box'>
                      <Text
                        style={_safe_style_('margin-right: 20rpx;')}
                        onClick={_fixme_with_dataset_(this.handleGoToPlan, { item: item })}
                        className='order-btn'
                      >
                        方案详情
                      </Text>
                      <Text onClick={_fixme_with_dataset_(this.handleGoToOrder, { item: item })} className='order-btn'>
                        预约详情
                      </Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        ) : (
          <Empty iconClass='empty-icon' message='敬请期待...'></Empty>
        )}
      </View>
    );
  }
}

export default MicroDecorateOrder;
