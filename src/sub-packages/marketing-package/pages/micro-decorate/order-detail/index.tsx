import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index';
import './index.scss';

class MicroDecorateOrderDetail extends React.Component {
  $router = $getRouter();
  state = {
    orderDetail: {},
  } as Record<string, any>;

  componentDidMount() {
    this.getOrderDetail();
  }

  //获取分组列表
  getOrderDetail = async () => {
    const res = await wxApi.request({
      url: api.micro_decorate.formDetail,
      loading: true,
      data: { submitFormId: this.$router.params.id },
    });

    const data = res.data || {};
    //根据uuid标志获取地址
    const submitFormBody = JSON.parse(res.data.submitFormBody);
    const formBody = JSON.parse(res.data.clientFormInfoDTO.formBody);
    let node: any = {};
    formBody.forEach((item) => {
      if (item.type == 'address') {
        node = submitFormBody.find((_item) => item.uuid == _item.uuid);
        data.address = node.value;
      }
    });
    this.setState({ orderDetail: data });
  };

  render() {
    const { orderDetail } = this.state;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-pmo-OrderDetail'
        className='wk-pmo-OrderDetail'
      >
        <View className='detail-header'>
          <Image className='item-img' src={orderDetail.activityCover} mode='aspectFill'></Image>
          {!!orderDetail.name && <View className='item-title'>{orderDetail.activityName}</View>}
        </View>
        <View className='order-detail'>
          <View className='order-info'>预约信息</View>
          {!!orderDetail.name && <View className='item-info'>{'姓名：' + orderDetail.name}</View>}
          {!!orderDetail.phone && <View className='item-info'>{'手机号：' + orderDetail.phone}</View>}
          {!!orderDetail.createTime && (
            <View className='item-info'>
              {'预约时间：' + filters.dateFormat(orderDetail.createTime, 'yyyy-MM-dd hh:mm')}
            </View>
          )}

          {!!orderDetail.address && <View className='item-info'>{'服务地址：' + orderDetail.address}</View>}
        </View>
      </View>
    );
  }
}

export default MicroDecorateOrderDetail;
