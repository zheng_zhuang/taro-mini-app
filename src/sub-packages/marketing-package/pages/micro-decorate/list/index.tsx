import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View, ScrollView, Text, Image } from '@tarojs/components';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index';
import authHelper from '../../../../../wxat-common/utils/auth-helper';
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox';
import template from '../../../../../wxat-common/utils/template';
import imageUtils from '../../../../../wxat-common/utils/image';
import Empty from '../../../../../wxat-common/components/empty/empty';
import './index.scss';
import hoc from '@/hoc';
import { connect } from 'react-redux';

type StateProps = {
  templ: any;
};

interface MicroDecorateList {
  props: StateProps;
}

const mapStateToProps = () => {
  return {
    templ: template.getTemplateStyle(),
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class MicroDecorateList extends React.Component {
  state = {
    labelList: [],
    decorateList: [],
    pageNo: 1,
    pageSize: 10,
    hasMore: true, // 是否可以加载更多
    selectList: '',
  } as Record<string, any>;

  componentDidMount() {
    this.initRenderData();
  }

  initRenderData = () => {
    this.getLabelList();
    this.fetchData();
  };

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState({ pageNo: this.state.pageNo + 1 });
      this.fetchData();
    }
  }

  // 获取分组列表
  getLabelList = async () => {
    const res = await wxApi.request({
      url: api.micro_decorate.labelList,
      loading: true,
    });

    const data = res.data || [];
    this.setState({ labelList: data });
  };

  openVrPreview = ({ currentTarget }) => {
    const vrLink = currentTarget.dataset.url;
    protectedMailBox.send('sub-packages/marketing-package/pages/receipt/index', 'receiptURL', vrLink);
    wxApi.$navigateTo({ url: '/sub-packages/marketing-package/pages/receipt/index', data: {} });
  };

  onChangeLike = async ({ currentTarget }) => {
    if (!authHelper.checkAuth()) return false;
    const item = currentTarget.dataset.item;
    const data = { sceneModelId: item.id, likeStatus: item.isLiked ? 0 : 1 };

    await wxApi.request({ url: api.micro_decorate.like, loading: true, data });

    const { pageNo, pageSize, decorateList } = this.state;

    this.setState({ pageNo: 1, pageSize: decorateList.length });

    await this.fetchData();
    this.setState({ pageNo, pageSize });
    Taro.eventCenter.trigger('midecorate-FetchData');
  };

  //根据标签获取列表
  fetchData = async () => {
    const params = {
      pageNo: this.state.pageNo,
      pageSize: this.state.pageSize,
      isShelf: 1,
    };

    if (this.state.selectList && this.state.selectList.length) {
      params.labels = this.state.selectList.join(',');
    }

    const res = await wxApi.request({
      url: api.micro_decorate.decorateList,
      loading: true,
      data: params,
    });

    const data = res.data || [];

    data.forEach((item) => {
      item.thumbnail = imageUtils.thumbnailOneRowTwo(item.thumbnail);
    });
    let renderList;
    if (this.isLoadMoreRequest()) {
      renderList = this.state.decorateList.concat(data);
    } else {
      renderList = data;
    }
    const hasMore = renderList.length < res.totalCount;
    renderList = renderList.filter((item) => item.isShelf);
    this.setState({ decorateList: renderList, hasMore });
  };

  handleGoToDetail = ({ currentTarget }) => {
    const item = currentTarget.dataset.item;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/micro-decorate/detail/index',
      data: { id: item.id },
    });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  handleChangeLabel = ({ currentTarget }) => {
    const index = currentTarget.dataset.index;
    const item = this.state.labelList[index];
    item.isChecked = !item.isChecked;
    const ids: any[] = [];
    this.state.labelList.forEach((item: any) => {
      if (item.isChecked) {
        ids.push(item.id);
      }
    });

    this.setState(
      {
        labelList: this.state.labelList,
        selectList: ids,
      },

      () => {
        this.fetchData();
      }
    );
  };

  render() {
    const { labelList, decorateList } = this.state;
    const tmpColor = this.props.templ.btnColor;

    return (
      <View data-scoped='wk-pml-List' className='wk-pml-List page-container'>
        <View className='label-box'>
          <ScrollView className='scroll-nav' scrollX style={_safe_style_('width: 100%')}>
            {labelList.map((item, index) => {
              return (
                <View
                  key={index}
                  onClick={_fixme_with_dataset_(this.handleChangeLabel, { id: item.id, index: index })}
                  className='item-nav'
                >
                  <Text
                    className={'label-text ' + (item.isChecked ? 'nav-click' : '')}
                    style={_safe_style_('background-color:' + (item.isChecked ? tmpColor : ''))}
                  >
                    {item.label}
                  </Text>
                </View>
              );
            })}
          </ScrollView>
        </View>
        {decorateList && decorateList.length ? (
          <View className='micro-decorate-container'>
            {decorateList.map((item, index) => {
              return (
                <View className='decorate-item' key={index}>
                  <Image
                    className='item-img'
                    onClick={_fixme_with_dataset_(this.handleGoToDetail, { item: item })}
                    src={item.thumbnail}
                    mode='aspectFill'
                  ></Image>
                  {!!item.vrUrl && (
                    <Image
                      className='vr-btn'
                      onClick={_fixme_with_dataset_(this.openVrPreview, { url: item.vrUrl })}
                      src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/view.png'
                    ></Image>
                  )}

                  <View className='item-tit'>{item.name}</View>
                  <View className='bottom-info'>
                    <View className='item-icon'>
                      <Image
                        className='icon-img'
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/order.png'
                        mode='aspectFill'
                      ></Image>
                      {item.appointmentNum + '人预约'}
                    </View>
                    <View className='item-icon' style={_safe_style_('margin-left:40rpx')}>
                      <Image
                        className='icon-img'
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/look.png'
                        mode='aspectFill'
                      ></Image>
                      {item.viewNum}
                    </View>
                    <View
                      style={_safe_style_('flex:1;text-align:right')}
                      className='item-icon icon-like'
                      onClick={_fixme_with_dataset_(this.onChangeLike, { item: item })}
                    >
                      {item.isLiked ? (
                        <Image
                          className='icon-img'
                          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like-checked.png'
                        ></Image>
                      ) : (
                        <Image
                          className='icon-img'
                          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like.png'
                          mode='aspectFill'
                        ></Image>
                      )}

                      {item.approvalNum}
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        ) : (
          <Empty style={_safe_style_('margin-top:120rpx;')} message='敬请期待...'></Empty>
        )}
      </View>
    );
  }
}

export default MicroDecorateList;
