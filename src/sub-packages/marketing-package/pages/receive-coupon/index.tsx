import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @scoped
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import checkOptions from '@/wxat-common/utils/check-options';
import authHelper from '@/wxat-common/utils/auth-helper';
import dateUtil from '@/wxat-common/utils/date';
import couponEnum from '@/wxat-common/constants/couponEnum';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg.js';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum.js';
import report from '@/sdks/buried/report/index.js';
import RecommendGoodsList from '@/wxat-common//components/recommend-goods-list/index';
import Error from '@/wxat-common/components/error/error';
import utils from '@/wxat-common/utils/util';

import template from '../../../../wxat-common/utils/template';
import './index.scss';

const IMG_PREFIX = 'https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/coupon/';
const IMG_SRC = {
  bg: IMG_PREFIX + 'receive-bg.png',
  icon: IMG_PREFIX + 'receive-icon.png',
  tip: IMG_PREFIX + 'receive-tip.png',
};

type StateProps = {
  sessionId: string;
  appInfo: any;
};

interface ReceiveCoupon {
  props: StateProps;
}

const mapStateToProps = ({ base }) => {
  return {
    appInfo: base.appInfo,
    sessionId: base.sessionId,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ReceiveCoupon extends React.Component {
  $router = $getRouter();
  state = {
    refUserId: '', //分享者ID
    couponId: '', //分享的优惠券ID
    couponDetail: null, //优惠券详情
    formattedCoupon: {
      name: '',
      price: '',
      minimum: '',
      termValidity: '',
      labels: [],
    },

    goodsList: [],
    received: false, //是否已经领取
    invalidTip: '',
    errorMessage: '',
  };

  async componentDidMount() {
    await this.checkInit();
  }

  async componentDidUpdate() {
    await this.checkInit();
  }

  async checkInit() {
    const option =
      !this.state.couponId && !!this.props.sessionId && (await checkOptions.checkOnLoadOptions(this.$router.params));
    if (!option) return;
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    const scene = option.scene ? '?' + decodeURIComponent(option.scene) : '';
    const refUserId = scene ? +utils.getQueryString(scene, 'refUseId') : +option['_ref_useId'];
    const couponId = scene ? +utils.getQueryString(scene, 'couponId') : +option.couponId;
    this.setState({ couponId, refUserId }, () => {
      this.getCouponInfo();
      this.getRecommands();
    });
  }

  getCouponInfo() {
    wxApi
      .request({
        url: api.coupon.queryDetail,
        loading: false,
        data: {
          couponInfoId: this.state.couponId,
        },
      })
      .then((res) => {
        if (res.data) {
          const { status, userCollectCount, claimNumber } = res.data;
          let invalidTip = '';
          if (status !== 1) {
            invalidTip = '亲来晚了一步哦，优惠券已失效或发放完毕～';
          } else if (claimNumber !== 0 && userCollectCount >= claimNumber) {
            invalidTip = '亲已达到优惠券领取上限哦～';
          }
          this.setState({
            couponDetail: res.data,
            formattedCoupon: this.formatCouponDetail(res.data),
            invalidTip,
          });
        } else {
          this.setState({
            errorMessage: '优惠券信息不存在',
          });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          errorMessage: (error.data && error.data.errorMessage) || '出现了一些错误',
        });
      });
  }

  formatCouponDetail(couponDetail) {
    if (!couponDetail) return null;
    const {
      couponCategory,
      discountFee,
      minimumFee,
      couponType,
      fixedTerm,
      beginTime,
      endTime,
      name,
      useScopeType,
      memberRestrictType,
      memberLevelDTOS,
    } = couponDetail;
    const formattedCoupon = {
      name,
      price: '',
      minimum: '',
      termValidity: '',
      labels: [] as string[],
    };

    if (couponCategory === couponEnum.TYPE.freight.value) {
      formattedCoupon.price = discountFee === 0 ? '免运费' : '¥' + parseFloat((discountFee / 100).toFixed(2));
    } else if (couponCategory === couponEnum.TYPE.fullReduced.value) {
      formattedCoupon.price = discountFee === 0 ? '¥0' : '¥' + parseFloat((discountFee / 100).toFixed(2));
    } else if (couponCategory === couponEnum.TYPE.discount.value) {
      formattedCoupon.price = discountFee === 0 ? '0折' : parseFloat((discountFee / 10).toFixed(2)) + '折';
    }
    // 优惠条件
    formattedCoupon.minimum = minimumFee === 0 ? '无门槛' : '满' + parseFloat((minimumFee / 100).toFixed(2)) + '可用';
    // 有效期
    formattedCoupon.termValidity =
      couponType === 0
        ? '领取后 ' + fixedTerm + ' 天有效'
        : dateUtil.format(new Date(beginTime)) + ' 至 ' + dateUtil.format(new Date(endTime));
    // 标签
    const type = {
      [couponEnum.TYPE.fullReduced.value]: couponEnum.TYPE.fullReduced.label,
      [couponEnum.TYPE.freight.value]: couponEnum.TYPE.freight.label,
      [couponEnum.TYPE.discount.value]: couponEnum.TYPE.discount.label,
    };

    const scope = {
      [couponEnum.SCOPE.Universal.value]: couponEnum.SCOPE.Universal.label,
      [couponEnum.SCOPE.Online.value]: couponEnum.SCOPE.Online.label,
      [couponEnum.SCOPE.offline.value]: couponEnum.SCOPE.offline.label,
    };

    formattedCoupon.labels.push(type[couponCategory]);
    formattedCoupon.labels.push(scope[useScopeType]);
    if (memberRestrictType === 0) {
      formattedCoupon.labels.push('新人券');
    }
    (memberLevelDTOS || []).forEach((item) => {
      formattedCoupon.labels.push(item.levelName);
    });
    return formattedCoupon;
  }

  getRecommands() {
    wxApi
      .request({
        url: api.goods.recommendGoods,
        loading: false,
        quite: true,
        data: {
          couponId: this.state.couponId,
          pageSize: 10,
        },
      })
      .then((res) => {
        this.setState({
          goodsList: (res.data && res.data.wxItemDetailList) || [],
        });
      });
  }

  handleReceive = async () => {
    if (!authHelper.checkAuth(true) || this.state.invalidTip) return;
    // 授权订阅消息
    const Ids = [subscribeEnum.COUPONS_ARRIVE.value];
    await subscribeMsg.sendMessage(Ids);
    wxApi
      .request({
        url: api.coupon.guideCouponCollect,
        loading: true,
        data: {
          couponId: this.state.couponId,
          refUserId: this.state.refUserId,
        },
      })
      .then((_) => {
        report.clickGetCoupon(2, this.state.couponId);
        this.setState({
          received: true,
        });
      });
  };

  render() {
    const { errorMessage, couponDetail, formattedCoupon, invalidTip, received, goodsList } = this.state;
    const { appInfo } = this.props;
    return (
      <View data-scoped='wk-smpr-ReceiveCoupon' className='wk-smpr-ReceiveCoupon share-coupon'>
        {!!errorMessage && <Error message={errorMessage} />}
        {!!couponDetail && (
          <View>
            <Image mode='widthFix' src={IMG_SRC.bg} className='coupon-bg' />
            <View className='coupon-detail'>
              <View className='price-box'>
                <Text className='price'>{formattedCoupon.price}</Text>
                <Text>{formattedCoupon.minimum}</Text>
              </View>
              <View className='name limit-line line-1'>{formattedCoupon.name}</View>
              <View className='label'>
                {formattedCoupon.labels.map((item) => {
                  return <Text key={item}>{item}</Text>;
                })}
              </View>
              <View className='termValidity'>{'有效期：' + formattedCoupon.termValidity}</View>
            </View>
            <View className='coupon-option'>
              {invalidTip ? (
                <Block>
                  <View className='receive-tip limit-line line-1'>{invalidTip}</View>
                </Block>
              ) : (
                !invalidTip &&
                !received && (
                  <Block>
                    <View className='receive-btn' onClick={this.handleReceive}>
                      立即领取
                    </View>
                  </Block>
                )
              )}

              {/*  立即领取按钮  */}
              {/*  当前已领取说明  */}
              {!!received && (
                <Block>
                  <View className='receive-tip limit-line line-1'>
                    {'请悄咪咪的在“' + (appInfo && appInfo.appName) + '”使用哦～'}
                  </View>
                  <Image className='receive-icon' src={IMG_SRC.icon} />
                </Block>
              )}
            </View>
          </View>
        )}

        {!!(goodsList && goodsList.length) && (
          <View className='recommend-goods'>
            <Image mode='widthFix' src={IMG_SRC.tip} className='recommend-tip' />
            <View className='coupon-recommend-goods'>
              <RecommendGoodsList label='' itemNo='' list={goodsList} />
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default ReceiveCoupon;
