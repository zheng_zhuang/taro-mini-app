import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, ScrollView} from '@tarojs/components';
import Taro from '@tarojs/taro';

import report from '@/sdks/buried/report/index.js';
import shareUtil from '@/wxat-common/utils/share.js';
import GroupLists from '@/wxat-common/components/tabbarLink/group-list/index';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import './index.scss';

class ListInGroup extends React.Component {
  $router = $getRouter();
  state = {
    showGroupType: 0, // 拼团子组件展示类型，0：上图下文(默认展示的类型)，1：左图右文
  };

  onLoad = (options) => {
    // 判断是否有拼团子组件展示类型参数
    if (options.showGroupType || options.showGroupType === 0) {
      this.setState({
        showGroupType: options.showGroupType,
      });
    }
  };

  onPullDownRefresh() {
    if (this.RefGroupListsCMTP.current) {
      this.RefGroupListsCMTP.current.onPullDownRefresh();
    }
  }

  onReachBottom() {
    if (this.RefGroupListsCMTP.current) {
      this.RefGroupListsCMTP.current.onReachBottom();
    }
  }

  onRetryLoadMore() {
    if (this.RefGroupListsCMTP.current) {
      this.RefGroupListsCMTP.current.onRetryLoadMore();
    }
  }

  onShareAppMessage = () => {
    report.share(true);
    const url = '/sub-packages/moveFile-package/pages/group/list/index';
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.GROUP_LIST,
      bzName: `精选拼团`,
    });

    console.log('sharePath => ', path);
    return {
      title: '',
      path,
      imageUrl: '',
    };
  };

  componentDidMount() {
    const parmas = this.$router.params;
    this.onLoad(parmas);
  }

  RefGroupListsCMTP: Taro.RefObject<any> = React.createRef();

  render() {
    const { showGroupType } = this.state;
    return (
      <ScrollView
        scrollY
        data-fixme='02 block to view. need more test' data-scoped='wk-mpgl-List' className='wk-mpgl-List'>
        <GroupLists showGroupType={Number(showGroupType)} ref={this.RefGroupListsCMTP} />
        <AuthPuop></AuthPuop>
      </ScrollView>
    );
  }
}

export default ListInGroup;
