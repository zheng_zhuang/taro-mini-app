import { $getRouter } from 'wk-taro-platform';
import React, { Component, createRef } from 'react'; /* eslint-disable react/sort-comp */
// @externalClassesConvered(AnimatDialog)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Button, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import constants from '@/wxat-common/constants/index.js';
import api from '@/wxat-common/api/index.js';
import timer from '@/wxat-common/utils/timer.js';
import wxApi from '@/wxat-common/utils/wxApi';
import report from '@/sdks/buried/report/index.js';
import utils from '@/wxat-common/utils/util.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import template from '@/wxat-common/utils/template.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import shareUtil from '@/wxat-common/utils/share.js';

import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import ChooseAttributeDialog from '@/wxat-common/components/choose-attribute-dialog';
import ShareDialog from '@/wxat-common/components/share-dialog/index';
// import ShareDialog from '@/wxat-common/components/share-dialog-new/index';

import RuleDesc from "../rule-desc";
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';

import Error from '@/wxat-common/components/error/error';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import './index.scss';

import ListActivity from '@/wxat-common/components/tabbarLink/group-list/list-activity';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import getStaticImgUrl from '../../../../../wxat-common/constants/frontEndImgUrl'


const defaultImg = getStaticImgUrl.wxatCommon.defaultImg_png;
const commonImg = cdnResConfig.common;
const mapStateToProps = (state) => ({
  base: state.base,
  globalData: state.globalData,
  sessionId: state.base.sessionId,
});

interface GroupJoinProps { base: any; globalData: any }

interface GroupJoin {
  props: GroupJoinProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class GroupJoin extends Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    tmpStyle: {}, // 获取模板配置
    isNewUser: false, // 是否为新用户

    groupNo: null, // 拼团单号
    groupActivityStatus: constants.groupActivity.status, // 当前拼团活动状态常量
    detail: {}, // 获取拼团详情
    remainTime: 0, // 该拼团允许参团剩余时间

    error: false, // 是否展示错误页面

    activityList: [], // 更多拼团活动列表

    visible: false, // 是否展示生成分享海报对话弹框
    qrCode: null, // 分享海报二维码

    goodType: constants.goods.type.group, // 商品类型常量，用于选择商品属性对话弹框

    currentGroupStore: null, // 当前拼团门店
    storeIds: null, // 适用拼团的门店id列表字符串
    allowCurrentGroupStore: true, // 当前门店是否允许参加拼团

    bgGroupFullImg: cdnResConfig.group.bgGroupFullImg, // 参团人员已满对话弹框背景图片
    bgOldWithNewImg: cdnResConfig.group.bgGroupFullImg, // 老带新拼团对话弹框背景图片

    sessionId: null,
    dataLoad: false,
    qrCodeParams: null,
    // 拼团规则
    groupRule: {},
    displayTime: {},
    members: [],
  };

  private timerId?: number;
  private chooseAttributeDialogRef = React.createRef<ChooseAttributeDialog>();
  private detailShareDialogRef = React.createRef<ShareDialog>();
  private groupDialogRef = React.createRef<AnimatDialog>();
  private groupFullDialogRef = React.createRef<AnimatDialog>();
  private oldWithDialogRef = React.createRef<AnimatDialog>();

  // 是否已经参团
  isJoined = () => {
    let joined = false;
    const detailVOList = this.state.detail && this.state.detail.detailVOList ? this.state.detail.detailVOList : []; // 已参团人数集合
    if (this.props.base.loginInfo && detailVOList.length > 0) {
      joined = !!detailVOList.find((m) => {
        return m.userId === this.props.base.loginInfo.userId;
      });
    }
    return joined;
  };

  // 还差几人成团
  stillCount = () => {
    const count = 0;
    const requirePeople = this.state.detail && this.state.detail.requirePeople ? this.state.detail.requirePeople : 0; // 最小参团人数
    const detailVOList = this.state.detail && this.state.detail.detailVOList ? this.state.detail.detailVOList : []; // 已参团人数集合
    if (requirePeople - detailVOList.length > 0) {
      return requirePeople - detailVOList.length;
    }
    return count;
  };

  // 判断该拼团活动是否已结束
  isActivityEnd = () => {
    if (this.state.detail) {
      const activityStatus = this.state.detail.activityStatus; // 拼团活动状态
      const groupActivityStatus = constants.groupActivity.status; // 拼团活动状态常量
      if (
        activityStatus === groupActivityStatus.NOT_SHELF || // 拼团活动状态为已结束
        activityStatus === groupActivityStatus.DELETED
      ) {
        // 拼团活动状态为已删除
        return true;
      }
    }
    return false;
  };

  // 判断是否拼团成功
  isSuccess = () => {
    if (this.state.detail) {
      const status = this.state.detail.status; // 拼团状态
      const groupStatus = constants.group.status; // 拼团状态常量
      if (status === groupStatus.SUCCESS) {
        // 拼团成功
        return true;
      }
    }
    return false;
  };

  // 判断该团是否还允许参团
  isCanJoin = () => {
    if (
      this.state.detail &&
      this.stillCount() && // 还差拼团成员
      !this.isActivityEnd() && // 判断该拼团活动未结束
      !this.isSuccess() && // 当前拼团未成功
      !this.isFailed() && // 当前拼团未失败
      !this.isJoinEnd() // 当前拼团是否未结束参团
    ) {
      return true;
    }
    return false;
  };

  // 拼团结果描述
  groupResultDesc = () => {
    let desc = null;
    if (this.isSuccess()) {
      // 拼团成功
      desc = '拼团成功';
    } else if (this.isFailed()) {
      // 拼团失败
      desc = '拼团失败';
      if (this.isActivityEnd()) {
        // 判断该拼团活动是否已结束
        desc = '活动已结束';
      }
    }
    return desc;
  };

  // 判断是否为老带新拼团，默认为0：普通拼团、1：老带新拼团
  isEnableOldWithNew = () => {
    if (this.state.detail && this.state.detail.enableOldWithNew) {
      return true;
    }
    return false;
  };

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad = (options) => {
    if (options.scene) {
      const scene = '?' + decodeURIComponent(options.scene);
      const groupNo = utils.getQueryString(scene, 'No');
      this.setState(
        {
          groupNo: groupNo,
        },

        () => {
          this.initRender();
        }
      );
    } else {
      this.setState(
        {
          groupNo: options.groupNo,
        },

        () => {
          this.initRender();
        }
      );
    }

    // 获取当前拼团门店
    // this.setState({
    //   currentGroupStore: this.props.base.currentStore,
    //   qrCodeParams: {
    //     verificationNo: this.state.groupNo,
    //     verificationType: 5,
    //     maPath: 'sub-packages/marketing-package/pages/group/join/index',
    //   },
    // });

    // if (!!this.props.base.sessionId) {
    //   this.initDetail(); // 初始化获取详情
    // }
  };

  initRender() {
    // 获取当前拼团门店
    this.setState({
      currentGroupStore: this.props.base.currentStore,
      qrCodeParams: {
        verificationNo: this.state.groupNo,
        verificationType: 5,
        maPath: 'sub-packages/marketing-package/pages/group/join/index',
      },
    });

    if (this.props.base.sessionId) {
      this.initDetail(); // 初始化获取详情
    }
  }

  async componentDidMount() {
    // const parms = this.$router.params;
    // if (parms) {
    //   this.setState(
    //     {
    //       groupNo: parms.groupNo,
    //     },
    //     () => {
    //       this.onLoad(parms);
    //       this.queryIsNewUser(); // 查询是否为新用户
    //       this.getDetail(); // 获取拼团详情
    //     }
    //   );
    // }


    const params = this.$router.params;
    const { sessionId } = this.props.base;
    if (params && !!sessionId) {
      const { groupNo = null } = params;
      await new Promise((resolve) => this.setState({ groupNo }, resolve));
      this.onLoad(params);
    }
  }

  async componentDidUpdate() {
    if (!this.state.groupNo) {
      const params = this.$router.params;
      const { sessionId } = this.props.base;
      if (params && !!sessionId) {
        const { groupNo = null } = params;
        await new Promise((resolve) => this.setState({ groupNo }, resolve));
        this.onLoad(params);
      }
    }
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { base } = this.props;
    if (base !== nextProps.base) {
      this.setState({ currentGroupStore: nextProps.base.currentStore });
    }

    if (!!nextProps.sessionId && !this.props.sessionId) {
    // fixme 由于H5刷新，再获取模板之前渲染页面
      this.initDetail()
    }

  }

  componentDidShow() {
    this.queryIsNewUser(); // 查询是否为新用户
    if (this.timerId != null) {
      this.timerId = timer.addQueue(this.updateTime);
    }
  }

  /**
   * 生命周期函数--监听页面隐藏
   */
  componentDidHide() {
    // 判断定时器是否存在，存在则在页面隐藏时删除定时器
    if (this.timerId != null) {
      timer.deleteQueue(this.timerId);
      this.timerId = undefined;
    }
  }

  onRuleLoad = (e) => {
    this.setState({
      groupRule: e || {},
    });
  };

  /**
   * 用户点击右上角分享
   */
   onShareAppMessage = () => {
    report.share(true);
    this.getShareDialog().hide();
    const detail = this.state.detail;
    const title = detail.activityName ? detail.activityName : '';
    const imageUrl = detail.thumbnail ? detail.thumbnail : '';
    const path = shareUtil.buildShareUrlPublicArguments({
      url: '/sub-packages/marketing-package/pages/group/join/index?groupNo=' + detail.groupNo,
      bz: shareUtil.ShareBZs.GROUP_JOIN,
      bzName: `拼团活动${detail.activityName ? '-' + detail.activityName : ''}`,
      bzId: detail && detail.groupNo,
      sceneName: detail && detail.activityName,
    });

    return {
      title,
      path,
      imageUrl: imageUrl,
      success: (res) => {
        this.setState({
          visible: false,
        });
      },
      fail: (res) => {
        report.share(false);
        // 转发失败
      },
    };
  };

  // 判断是否拼团失败（该处改用方法而不用计算属性的原因是为了避免定时器的计算属性获取不到最新的返回值）
  isFailed = () => {
    if (this.state.detail) {
      const status = this.state.detail.status; // 拼团状态
      const groupStatus = constants.group.status; // 拼团状态常量
      if (
        status === groupStatus.FAILED || // 拼团失败
        status === groupStatus.REFUNDING || // 退款中
        status === groupStatus.REFUND_COMPLETED || // 退款完成
        (status === groupStatus.ON_PROGRESS && // 拼团中
          (this.isJoinEnd() || // 参团时间剩余截止
            this.isActivityEnd())) // 判断该拼团活动是否已结束
      ) {
        return true;
      }
    }
    return false;
  };

  // 判断该拼团是否已结束参团（该处用改方法而不用计算属性的原因是为了避免定时器的计算属性获取不到最新的返回值）
  isJoinEnd = () => {
    if (this.state.detail && this.state.remainTime < 1) {
      // 倒计时小于1
      return true;
    }
    return false;
  };

  /**
   * 初始化获取详情
   */
  initDetail = () => {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({
      dataLoad: true,
    });

    this.getTemplateStyle(); // 获取模板配置
    this.getDetail(); // 获取拼团详情
    this.getActivity(); // 获取更多拼团活动列表
    this.queryIsNewUser();
  };

  /**
   * 获取模板配置
   */
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  /**
   * 判断是否已授权
   */
  checkAuth = () => {
    const isRegistered = !!this.props.base.registered;
    if (!isRegistered) {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/wx-auth/index',
      });
    }
    return isRegistered;
  };

  /**
   * 查询是否为新用户
   */
  queryIsNewUser = () => {
    wxApi
      .request({
        url: api.user.isNewUser,
        loading: true,
        quite: true,
        data: {},
      })
      .then(
        (res) => {
          const data = res.data;
          if (data) {
            this.setState({
              isNewUser: true,
            });
          }
        },
        (e) => {}
      )
      .catch((error) => {});
  };

  /**
   * 获取拼团详情
   */
  getDetail = () => {
    wxApi
      .request({
        url: api.group.detail,
        data: {
          groupNo: this.state.groupNo,
        },
      })
      .then((res) => {
        const members: Object[] = []; // 参团人员数组（包括未参团的，以便页面渲染）
        if (res.data) {
          const { requirePeople, detailVOList = [], storeIds, remainTime } = res.data;
          const len = requirePeople - detailVOList.length
          console.log(len, 'len--------------------------len')
          members.push(...detailVOList)
          for (let i = 0; i < len; i++) {
            // 参团人员数组添加未参团的，以便页面渲染
            members.push({
              userId: 'virtal' + i,
              unJoin: true
            })
          }

          this.setState({
            storeIds: storeIds || null, // 适用拼团的门店id列表字符串
            remainTime, // 该拼团允许参团剩余时间
          });

          this.handlerIsAllow(); // 判断当前门店是否允许拼团
        }

        this.setState(
          {
            detail: res.data,
            members,
            sessionId: report.getBrowseItemSessionId(res.data.itemNo),
          },

          this.updateTime
        );

        // 初始化分享数据
        this.onShareAppMessage()

      })
      .catch((error) => {
        this.apiError(error);
      });
  };

  /**
   * 显示错误页面
   * @param {*} error
   */
  apiError = (error) => {
    // this.setState({
    //   error: true,
    // });
    Taro.showToast({
      title: '查询失败，请稍后重试',
      icon: 'none'
    })
  };

  /**
   * 获取更多拼团活动列表
   */
  getActivity = () => {
    wxApi
      .request({
        url: api.activity.list,
        loading: true,
        data: {
          pageNo: 1,
          pageSize: 10,
        },
      })
      .then((res) => {
        this.setState({
          activityList: res.data || [],
        })
      })
  };

  /**
   * 格式化参团倒计时
   */
  updateTime = () => {
    let remainTime = this.state.remainTime; // 该拼团允许参团剩余时间
    if (remainTime == null || remainTime <= 0) {
      // 判断定时器是否存在，存在则在页面隐藏时删除定时器
      if (this.timerId != null) {
        timer.deleteQueue(this.timerId);
        this.timerId = undefined;
      }
      this.setState({
        displayTime: {
          hour: '00',
          minute: '00',
          second: '00',
        },

        remainTime: 0,
      });

      return;
    } else if (this.timerId == null) {
      this.timerId = timer.addQueue(this.updateTime);
    }

    remainTime -= 1;

    const day = parseInt(remainTime / (24 * 3600));

    const hourS = parseInt(remainTime % (24 * 3600));
    const hourInt = parseInt(hourS / 3600);
    const hour = hourInt < 10 ? '0' + hourInt : hourInt;

    const minuteS = hourS % 3600;
    const minuteInt = parseInt(minuteS / 60);
    const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

    const secondS = parseInt(minuteS % 60);
    const second = secondS < 10 ? '0' + secondS : secondS;

    const displayTime = {
      day: day < 10 ? '0' + day : day,
      hour: hour + '',
      minute: minute + '',
      second: second + '',
    };

    this.setState({
      displayTime,
      remainTime,
    });
  };

  /**
   * 判断当前门店是否允许拼团
   */
  handlerIsAllow = () => {
    const currentGroupStoreId = this.state.currentGroupStore.id; // 当前拼团的门店id
    const storeIdList = this.state.storeIds ? this.state.storeIds.split(',') : []; // 适用拼团的门店id列表数组
    if (storeIdList.length > 0 && storeIdList.indexOf('' + currentGroupStoreId) === -1) {
      this.setState({
        allowCurrentGroupStore: false, // 当前门店不允许拼团
      });
    } else {
      this.setState({
        allowCurrentGroupStore: true, // 当前门店允许拼团
      });
    }
  };

  /**
   * 切换拼团门店
   */
  handlerSwitchStore = () => {
    protectedMailBox.send('wxat-common/pages/store-list/index', 'storeIds', this.state.storeIds);
    wxApi.$navigateTo({
      url: '/wxat-common/pages/store-list/index',
    });
  };

  /**
   * 打开拼团活动规则对话弹框
   */
  handleViewRule = () => {
    this.getRuleDialog().show({
      scale: 1,
    });
  };

  /**
   * 关闭拼团活动规则对话弹框
   */
  handleCloseRule = () => {
    this.getRuleDialog().hide();
  };

  /**
   * 获取活动规则对话弹框
   */
  getRuleDialog = () => {
    return this.groupDialogRef.current;
  };

  /**
   * 参加拼团操作
   */
  handleJoinGroup = async () => {
    await this.queryIsNewUser()
    if (!this.checkAuth()) {      // 判断是否已授权
      return;
    } else if (this.isEnableOldWithNew() && !this.state.isNewUser) {
      // 判断当前用户是否为老用户
      this.handleOldWithNewDialog(); // 打开老带新拼团提示对话弹框
      return;
    }
    Taro.showLoading(
      {
        title: '加载中',
        mask: true
      }
    )
    // 根据groupNo查询拼团详情

    wxApi
      .request({
        url: api.group.queryActivityByGroupNo,
        data: {
          groupNo: this.state.groupNo,
        },
      })
      .then((res) => {
        // 打开参团人数已满提示对话弹框
        if (res.data && res.data.status !== -1) {
          this.handleGroupFullDialog();
        } else {
          const { groupNo, goodsDetail, detail } = this.state;
          report.joinGroup({
            groupNo,
            activityId: detail && detail.activityId,
          });

          // 判断当前门店是否允许拼团
          if (!this.state.allowCurrentGroupStore) {
            wxApi.showToast({
              title: `当前门店不适用于该拼团活动，请切换门店`,
              icon: 'none',
            });

            return;
          }

          if (goodsDetail) {
            this.handleChooseAttribute(); // 展示选择商品属性的弹框
          } else {
            this.getGoodsDetail(); // 获取商品详情
          }
        }
      }).finally(() => {
        Taro.hideLoading()
      })
  }

  /**
   * 打开参团人数已满提示对话弹框
   */
  handleGroupFullDialog = () => {
    this.groupFullDialogRef.current &&
      this.groupFullDialogRef.current.show({
        scale: 1,
      });
  };

  /**
   * 关闭参团人数已满提示对话弹框
   */
  handleCloseGroupFullDialog = () => {
    this.groupFullDialogRef.current && this.groupFullDialogRef.current.hide();

    // 关闭参团人数已满提示对话弹框，刷新参团详情
    this.getDetail(); // 获取拼团详情
  };

  /**
   * 点击发起拼团，跳转拼团列表页面
   */
  handleGoToGroupList = () => {
    wxApi.$redirectTo({
      url: '/sub-packages/moveFile-package/pages/group/list/index',
    });
  };

  /**
   * 打开老带新拼团提示对话弹框
   */
  handleOldWithNewDialog = () => {
    this.oldWithDialogRef.current &&
      this.oldWithDialogRef.current.show({
        scale: 1,
      });
  };

  /**
   * 关闭老带新拼团提示对话弹框
   */
  handleCloseOldWithNewDialog = () => {
    this.oldWithDialogRef.current && this.oldWithDialogRef.current.hide();

    // 关闭老带新拼团提示对话弹框，刷新参团详情
    this.getDetail(); // 获取拼团详情
  };

  /**
   * 点击发起拼团，跳转该拼团活动的详情页面
   */
  handleGoToGroupDetail = () => {
    this.oldWithDialogRef.current && this.oldWithDialogRef.current.hide(); // 关闭老带新拼团提示对话弹框，避免用户返回该页面时会继续显示该弹框

    const { activityId, itemNo } = this.state.detail;
    if (activityId && itemNo) {
      wxApi.$navigateTo({
        url: '/sub-packages/moveFile-package/pages/group/detail/index',
        data: {
          activityId: activityId,
          itemNo: itemNo,
        },
      });
    }
  };

  /**
   * 获取商品详情
   */
  getGoodsDetail = () => {
    const { groupNo, itemNo } = this.state.detail;
    return wxApi
      .request({
        url: api.goods.detail,
        data: {
          groupNo,
          itemNo,
          type: goodsTypeEnum.GROUP.value,
        },
      })
      .then(
        (res) => {
          const data = res.data;
          // 把活动id,拼团编号附加到data（即添加到goodsDetail)
          data.groupNo = this.state.groupNo;
          if (data) {
            this.setState(
              {
                goodsDetail: data,
              },

              () => {
                this.handleChooseAttribute(); // 展示选择商品属性的弹框
              }
            );

            if (data) {
              report.browseItem({
                itemNo: data.wxItem.itemNo,
                name: data.wxItem.name,
                salePrice: data.wxItem.salePrice,
                thumbnail: data.materialUrls ? data.materialUrls[0] : '',
                source: reportConstants.SOURCE_TYPE.group.key,
                sessionId: this.state.sessionId,
              });
            }
          } else {
            this.apiError();
          }
        },
        (e) => {}
      )
      .catch((error) => {
        this.apiError(error);
      });
  };

  /**
   * 展示选择商品属性的弹框
   */
  handleChooseAttribute = () => {
    const dialog = this.chooseAttributeDialogRef.current;
    if (dialog) {
      dialog.showAttributDialog();
    }
  };

  /**
   * 选定购买的商品属性
   */
  handlerChooseAttribute = (info) => {
    const dialog = this.chooseAttributeDialogRef.current;

    const detail = info;
    const { groupNo, goodsDetail } = this.state;

    if (goodsDetail.ptActivityItemDTO && goodsDetail.ptActivityItemDTO.limitItemCount) {
      const limitItemCount = goodsDetail.ptActivityItemDTO.limitItemCount;
      if (detail.itemCount > limitItemCount) {
        wxApi.showToast({
          title: `购买数量最多只能${limitItemCount}`,
          icon: 'none',
        });

        return;
      }
    }

    !!dialog && dialog.hideAttributeDialog();

    const goodsInfoList = info;

    // 判断是否为服务拼团 服务类型为0
    if (goodsDetail.wxItem.type === 0) {
      // isGroupLeader=1表示是团长优惠
      wxApi.$navigateTo({
        url: '/pages/appointment/index',
        data: {
          goodsInfo: JSON.stringify(goodsInfoList),
        },
      });
    } else {
      const goodsInfoList = [
        {
          pic: detail.pic,
          salePrice: detail.salePrice,
          itemCount: detail.itemCount,
          noNeedPay: detail.noNeedPay,
          name: detail.activityName,
          itemNo: detail.itemNo,
          skuId: detail.skuId,
          barcode: detail.barcode,
          skuTreeIds: detail.skuTreeIds,
          skuTreeNames: detail.skuTreeNames,
          reportExt: JSON.stringify({
            sessionId: this.state.sessionId,
          }),

          drugType: detail.wxItem.drugType, // 处方药
        },
      ];

      const payDetails = [
        {
          name: 'freight',
        },
      ];

      const orderRequestPromDTO = {
        minPeople: detail.minPeople,
      };
      orderRequestPromDTO.groupNo = groupNo;
      const params = JSON.stringify({
        orderRequestPromDTO,
      });
      report.clickBuy(groupNo, 3);
      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', goodsInfoList);
      if(goodsDetail.wxItem.type === goodsTypeEnum.VIRTUAL_GOODS.value){
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index',
          data: {
            payDetails:encodeURI(JSON.stringify(payDetails)),
            params: encodeURI(params),
            itemNo: detail.itemNo
          }
        });
      }else{
        wxApi.$redirectTo({
          url: pageLinkEnum.orderPkg.payOrder,
          data: {
            payDetails:encodeURI(JSON.stringify(payDetails)),
            params: encodeURI(params),
            itemNo: detail.itemNo
          }
        })
      }
    }
  };

  /**
   * 点击进入订单详情
   */
  handleGoToOrderDetail = () => {
    const params = {
      orderNo: this.state.detail.orderNo,
    };

    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.orderDetail,
      data: params,
    });
  };

  /**
   * 更多拼团，跳转拼团列表
   */
  handleMoreGroups = () => {
    wxApi.$navigateTo({
      url: `/sub-packages/marketing-package/pages/group/list/index`,
    });
  };

  /**
   * 跳转当前拼团活动详情页面
   * @param {*} e
   */
  handleGotoDetail = (e) => {
    // 判断当前门店是否允许拼团
    if (!this.state.allowCurrentGroupStore) {
      wxApi.showToast({
        title: `当前门店不适用于该拼团活动，请切换门店`,
        icon: 'none',
      });

      return;
    }

    const { activityId, itemNo } = e.currentTarget.dataset;
    if (activityId && itemNo) {
      wxApi.$navigateTo({
        url: '/sub-packages/moveFile-package/pages/group/detail/index',
        data: {
          activityId: activityId,
          itemNo: itemNo,
        },
      });
    } else {
      wxApi.showToast({
        title: '无法获取商品详情信息',
        icon: 'none',
      });
    }
  };

  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel = () => {
    this.getShareDialog().show();
  };

  /**
   * 获取邀请好友对话弹框
   */
  getShareDialog = () => {
    if (this.detailShareDialogRef.current) {
      this._shareDialog = this.detailShareDialogRef.current;
    }
    return this._shareDialog;
  };

  // 保存图片
  onSavePosterImage = (e) => {
    this.getShareDialog().savePosterImage(this);
  };

  render() {
    const {
      error,
      detail,
      tmpStyle,
      currentGroupStore,
      displayTime,
      members,
      activityList,
      dataLoad,
      bgGroupFullImg,
      bgOldWithNewImg,
      groupRule,
      qrCodeParams,
      goodType,
      goodsDetail,
    } = this.state;
    console.log(this.state.members, 'this.state.members')
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pgj-Join' className='wk-pgj-Join'>
        {error ? (
          <Error></Error>
        ) : (
          <View className='group-moudle'>
            <View className='group-detail'>
              <View
                className='group-tab group-activity'
                onClick={_fixme_with_dataset_(this.handleGotoDetail, {
                  activityId: detail.activityId,
                  itemNo: detail.itemNo,
                })}
                style={_safe_style_('background:' + tmpStyle.bgGradualChange)}
              >
                <View className='activity-detail-box'>
                  <Image src={detail.thumbnail} className='goods-img'></Image>
                  <View className='right-content'>
                    <View className='group-title limit-line line-3'>
                      <Text
                        className='group-minPeople'
                        style={_safe_style_(
                          'background:' +
                            tmpStyle.bgColor +
                            ';color:' +
                            tmpStyle.btnColor +
                            ';border-color:' +
                            tmpStyle.btnColor
                        )}
                      >
                        {detail.requirePeople + '人团'}
                      </Text>
                      {detail.activityName}
                    </View>
                    <View className='activity-info'>
                      <View className='real-sale money' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {'拼团价：￥' + filters.moneyFilter(detail.salePrice, true)}
                      </View>
                      <View className='sale-amount'>
                        <Text className='origin-sale money'>
                          {'￥' + filters.moneyFilter(detail.salePrice + detail.groupPromFee, true)}
                        </Text>
                        <Text>{'已拼' + detail.currentCount + '单'}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              {/*  门店选择  */}
              <View className='group-tab group-store' onClick={this.handlerSwitchStore}>
                <View className='store-title'>当前门店</View>
                <View className='current-store'>
                  {!!currentGroupStore && (currentGroupStore.abbreviation || currentGroupStore.name)}
                </View>
                <Image className='right-angle-gray' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
              </View>
              {/*  拼团规则  */}
              <View className='group-tab group-rule' onClick={this.handleViewRule}>
                <View className='rule-title'>规则详情</View>
                <View className='rule-info'>
                  <Text className='rule-text'>支付开团，邀请好友参与，成功发货，失败退款</Text>
                  <Image className='right-angle-gray' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
                </View>
              </View>
              {/*  参团信息  */}
              <View className='group-tab group-info'>
                {!!(!this.isActivityEnd() && !this.isSuccess()) && (
                  <View className='remain-time-box'>
                    <View className='remain-time'>
                      {!!(displayTime.day && displayTime.day !== '00') && (
                        <Text className='time-block' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                          {displayTime.day}
                        </Text>
                      )}

                      {!!(displayTime.day && displayTime.day !== '00') && (
                        <Text className='symbol' style={_safe_style_('line-height: 70rpx;')}>
                          天
                        </Text>
                      )}

                      <Text className='time-block' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                        {displayTime.hour}
                      </Text>
                      <Text className='symbol'>:</Text>
                      <Text className='time-block' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                        {displayTime.minute}
                      </Text>
                      <Text className='symbol'>:</Text>
                      <Text className='time-block' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                        {displayTime.second}
                      </Text>
                    </View>
                    <View className='remain-time-text'>距拼团结束还剩</View>
                  </View>
                )}

                {/*  参团成员  */}
                <View className='group-users'>
                  <View className='user-leader'>
                    <Block>
                      {!!members &&
                        members.map((item, index) => {
                          return index === 0 ? (
                            <View key={'user-item' + index} className='user-item'>
                              <Image
                                className={item.userAvatar ? 'avatar' : 'default-avatar'}
                                src={item.userAvatar ? item.userAvatar : defaultImg}
                              />

                              {index === 0 && (
                                <View className='leader' style={_safe_style_('background-color:' + tmpStyle.btnColor)}>
                                  团长
                                </View>
                              )}
                            </View>
                          ) : null;
                        })}
                    </Block>
                  </View>
                  {/*  团员  */}
                  <View className='user-members'>
                    <Block>
                      {!!members &&
                        members.map((item, index) => {
                          return (
                            index > 0 && (
                              <View key={'user-item' + index} className='user-item'>
                                <Image
                                  className={item.userAvatar ? 'avatar' : 'default-avatar'}
                                  src={item.userAvatar ? item.userAvatar : defaultImg}
                                ></Image>
                              </View>
                            )
                          );
                        })}
                    </Block>
                  </View>
                </View>
                {/*  参团人数提示  */}
                {this.isCanJoin() && (
                  <View className='join-num-tips'>
                    还差
                    <Text className='num' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      {this.stillCount()}
                    </Text>
                    人，即可拼团成功！
                  </View>
                )}

                {/*  拼团结果  */}
                {this.groupResultDesc() && (
                  <View
                    className={'group-result ' + (detail.status === 1 ? 'group-success' : 'group-failed')}
                    style={_safe_style_(
                      'color:' + (detail.status === 1 ? 'tmpStyle.btnColor' : 'rgba(153, 153, 153, 1)')
                    )}
                  >
                    {this.groupResultDesc()}
                  </View>
                )}

                {/*  参团详情按钮  */}
                <View className='btn-box'>
                  {!this.isJoined() ? (
                    <Block>
                      {this.isCanJoin() && (
                        <Button
                          className='btn btn-join'
                          style={_safe_style_('background:' + tmpStyle.bgGradualChange)}
                          onClick={this.handleJoinGroup}
                        >
                          我要参团
                        </Button>
                      )}
                    </Block>
                  ) : (
                    this.isJoined() && (
                      <Block>
                        {this.isCanJoin() ? (
                          <Button
                            className='btn btn-join'
                            style={_safe_style_('background:' + tmpStyle.bgGradualChange)}
                            onClick={this.handleSelectChanel}
                          >
                            邀请好友
                          </Button>
                        ) : (
                          !this.isCanJoin() && (
                            <Button
                              className='btn btn-join'
                              style={_safe_style_('background:' + tmpStyle.bgGradualChange)}
                              onClick={_fixme_with_dataset_(this.handleGoToOrderDetail, {
                                activityId: detail.activityId,
                                itemNo: detail.itemNo,
                              })}
                            >
                              查看订单详情
                            </Button>
                          )
                        )}
                      </Block>
                    )
                  )}

                  {/*  已参团  */}
                  {/*  拿更多拼团商品  */}
                  <Button
                    className='btn btn-more'
                    style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color: ' + tmpStyle.btnColor)}
                    onClick={_fixme_with_dataset_(this.handleMoreGroups, {
                      activityId: detail.activityId,
                      itemNo: detail.itemNo,
                    })}
                  >
                    拿更多拼团商品
                  </Button>
                </View>
                {/*  分享提示  */}
                {!!(this.isJoined() && this.isCanJoin()) && <View className='share-tips'>分享好友越多，成团越快</View>}
              </View>
            </View>
            {/*  更多拼团模块 */}
            {!!dataLoad && (
              <View className='activity-list'>
                <View className='activity-title' onClick={this.handleMoreGroups}>
                  大家都在拼
                </View>
                <ListActivity activityList={activityList} showGroupType={1} />
              </View>
            )}
          </View>
        )}

        {/*  规则对话弹框  */}
        <AnimatDialog id='rule-dialog' animClass='rule-dialog' ref={this.groupDialogRef}>
          {!!dataLoad && <RuleDesc onRule={this.onRuleLoad}></RuleDesc>}
          <Image className='icon-close' src={commonImg.close} onClick={this.handleCloseRule}></Image>
        </AnimatDialog>
        {/*  参团人数已满提示对话弹框  */}
        <AnimatDialog id='group-full-dialog' animClass='group-full-dialog' ref={this.groupFullDialogRef}>
          <View className='group-full'>
            <Image className='bg-group-full' src={bgGroupFullImg}></Image>
            <View className='group-full-content'>
              <View className='group-full-title'>参团人数已满</View>
              <View className='group-full-text'>快去发起自己的拼团吧</View>
            </View>
            <View className='btn-group-full' onClick={this.handleGoToGroupList}>
              发起拼团
            </View>
          </View>
          <Image
            className='icon-group-full-close'
            src={commonImg.close}
            onClick={this.handleCloseGroupFullDialog}
          ></Image>
        </AnimatDialog>
        {/*  老带新拼团提示对话弹框  */}
        <AnimatDialog id='old-with-new-dialog' animClass='old-with-new-dialog' ref={this.oldWithDialogRef}>
          <View className='old-with-new'>
            <Image className='bg-old-with-new' src={bgOldWithNewImg}></Image>
            <View className='old-with-new-content'>
              <View className='old-with-new-title'>仅限新用户参与</View>
              <View className='old-with-new-text'>老用户可发起此拼团活动</View>
            </View>
            <View className='btn-old-with-new' onClick={this.handleGoToGroupDetail}>
              发起拼团
            </View>
          </View>
          <Image
            className='icon-old-with-new-close'
            src={commonImg.close}
            onClick={this.handleCloseOldWithNewDialog}
          ></Image>
        </AnimatDialog>
        {/*  分享对话弹框  */}
        <ShareDialog
          id='share-dialog'
          posterHeaderType={groupRule.posterType || 1}
          posterTips={groupRule.posterContent || '物美价廉的好货，赶紧来拼团哟！'}
          posterLogo={groupRule.posterLogo}
          posterSalePriceLabel='拼团价'
          posterLabelPriceLabel='活动结束价'
          posterName={detail.activityName}
          posterImage={detail.thumbnail}
          salePrice={filters.moneyFilter(detail.salePrice, true)}
          labelPrice={filters.moneyFilter(detail.salePrice + detail.groupPromFee, true)}
          onSave={this.onSavePosterImage}
          qrCodeParams={qrCodeParams}
          childRef={this.detailShareDialogRef}
        ></ShareDialog>
        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='share-canvas'></Canvas>
        {/*  选择拼团商品属性对话弹框  */}
        {!!goodsDetail && (
          <ChooseAttributeDialog
            ref={this.chooseAttributeDialogRef}
            goodsType={goodType}
            onChoose={this.handlerChooseAttribute}
            goodsDetail={goodsDetail}
          ></ChooseAttributeDialog>
        )}

        <AuthPuop></AuthPuop>
      </View>
    );
  }
}
GroupJoin.enableShareAppMessage = true
export default GroupJoin;
