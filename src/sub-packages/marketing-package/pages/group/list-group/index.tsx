import React, { Component } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';
import './index.scss';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
  base: state.base,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class ListGroup extends Component {
  static defaultProps = {
    size: 'normal',
    // 参团列表
    groups: [],
    // 是否为老带新拼团
    enableOldWithNew: false,
    // 是否不允许点击
    disable: false,
  };

  state = {
    tmpStyle: {},
    isNewUser: false, // 是否为新用户
  }; /*请尽快迁移为 componentDidMount 或 constructor*/
  UNSAFE_componentWillMount() {
    this.getTemplateStyle(); // 获取模板配置
    this.queryIsNewUser(); // 查询是否为新用户
  }
  /**
   * 获取模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }
  /**
   * 查询是否为新用户
   */
  queryIsNewUser() {
    wxApi
      .request({
        url: api.user.isNewUser,
        data: {},
      })
      .then(
        (res) => {
          const data = res.data;
          if (!!data) {
            this.setState({
              isNewUser: true,
            });
          }
        },
        (e) => {}
      )
      .catch((error) => {});
  }

  /**
   * 去参团
   * @param {*} e
   */
  onJoinGroup = (e) => {
    const { base } = this.props;
    const userId = base.userInfo.id || null; // 当前用户id
    const leaderUserId = e.currentTarget.dataset.leaderUserId || null; // 当前点击的团长id

    if (this.props.disable) {
      // 判断是否禁止操作
      return;
    } else if (
      userId &&
      leaderUserId &&
      userId !== leaderUserId && // 判断点击的是否为自己发起的团
      !!this.props.enableOldWithNew && // 判断该团是否为老带新拼团
      !this.state.isNewUser
    ) {
      // 判断当前用户是否为老用户
      wxApi.showToast({
        icon: 'none',
        title: '该团仅限新用户可以参与哦，老用户可发起此拼团活动，邀请新用户共同参团',
      });

      return;
    }
    // 跳转参团页面
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/group/join/index',
      data: {
        groupNo: e.currentTarget.dataset.no,
      },
    });
  };

  render() {
    const { groups, size, disable } = this.props;
    const { tmpStyle } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pgl-ListGroup' className='wk-pgl-ListGroup'>
        {!!groups &&
          groups.map((item, index) => {
            return (
              <View
                className='group-item'
                key={item.groupNo}
                onClick={_fixme_with_dataset_(this.onJoinGroup, { no: item.groupNo, leaderUserId: item.leaderUserId })}
              >
                <Image src={item.leaderAvatar} className='group-item-image avatar'></Image>
                <View className={'group-item-view ' + ('user-name name_' + size)}>
                  {item.leaderNickName || '******'}
                </View>
                <View className='group-item-view group-time' style={_safe_style_('flex: 1')}>
                  <View className='group-item-view remain-people' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    {'还差' + (item.requirePeople - item.currentCount) + '人成团'}
                  </View>
                  <View className='group-item-view remain-time' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    {'剩余：' + item.displayTime}
                  </View>
                </View>
                <View
                  className={'group-item-view ' + ('join join-' + size) + ' ' + (disable ? 'disabled ' : '')}
                  style={_safe_style_('background:' + tmpStyle.btnColor)}
                >
                  去参团
                </View>
              </View>
            );
          })}
      </View>
    );
  }
}

export default ListGroup;
