import React, { Component } from 'react';
import '@/wxat-common/utils/platform';
import { ScrollView, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';

interface RuleDesc {
  props: { onRule: Function; activityId: string };
}

class RuleDesc extends Component {
  static defaultProps = {
    activityList: {
      type: Array,
      value: [],
    },

    activityId: '',
  };

  state = {
    defaultRule:{
      title: '拼团规则说明',
      setions: [
        '1、必须根据拼团要求的最低人数，才能拼团成功。',
        '2、发起拼团先要支付，如果最终拼团不成功，支付的费用自动原路退回。',
        '3、拼团设置的时限内，例如24小时，必须找够人数，否则拼团不成功，退款。',
        '4、参与阶梯团定价的商品，越多人参与团购，价格越便宜。',
      ],
    },
    title: '',
    setions: [],
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    this.attached();
  }
  attached() {
    const { activityId, defaultRule } = this.props;

    wxApi
      .request({
        url: api.activity.querySetting,
        data: {
          activityId,
        },
      })
      .then((res) => {
        this.props.onRule && this.props.onRule(res.data);
        const content = res.data.content || '';
        const array = content.split('↵');
        let setions = [];
        array.forEach((item) => {
          if (item === '') {
            return;
          }
          setions = setions.concat(item.split('\n').filter((section) => section !== ''));
        });
        this.setState({
          title: res.data.title,
          setions: setions,
        });
      })
      .catch((e) => {
        console.log(e);
        this.setState({
          title: defaultRule.title,
          setions: defaultRule.setions,
        });
      });
  }

  render() {
    const { title, setions } = this.state;
    return (
      <ScrollView data-scoped='wk-pgr-RuleDesc' className='wk-pgr-RuleDesc scroll-view_H scroll' scrollY>
        <View className='rule'>
          <View className='title'>{title}</View>
          {setions.map((item, index) => {
            return (
              <View className='block' key={index}>
                {item}
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

export default RuleDesc;
