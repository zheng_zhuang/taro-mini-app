import { _fixme_with_dataset_, _safe_style_, $getRouter } from 'wk-taro-platform';
import { View, Radio, Text, Image, Input } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import state from '@/state/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import utils from '@/wxat-common/utils/util.js';
import api from '@/wxat-common/api/index.js';
import './index.scss';
import {getFreeLabel} from '@/wxat-common/utils/service'
import store from '@/store';
const storeData = store.getState();
class GoodsReserveList extends React.Component {

  /**
   * 页面的初始数据
   */
  state = {
    goodsList: [],
    totalPrice: 0,
    allChecked: false,
    goodsEdit: false,
    editAllChecked: false,
    count: '',
    stockList:[]
  }

  $router = $getRouter()

  /**
   * 生命周期函数--监听页面加载
   */
   componentDidMount () {
    let that = this;
    wxApi.getStorage({
      key: 'serviceTreeData',
      success(res) {
        // 处理表单数据
        const ids = []
        let arr = res.data.map((item) => {
          if(!ids.includes(JSON.parse(item).id)){
            ids.push(JSON.parse(item).id)
          }
          return {
            ...JSON.parse(item),
            checked: true,
            editChecked: false,
          };
        });
        that.getServerItemStock(ids)
        that.setState({
          goodsList: arr,
        },() => {
          that.calcPrice();
          that.judgeAllCheck();
          that.count();
        });
      }
    });
    this.getTemplateStyle();
  }

  count() {
    let serviceTreeData = this.state.goodsList;
    let count = 0;
    let totalPrice = 0;
    serviceTreeData.forEach((item) => {
      count += item.checked ? item.count : 0;
      totalPrice += parseInt(item.salePrice * item.count) + parseInt(item.deposit * item.count);
    });
    this.setState({
      count,
      totalPrice: totalPrice / 100 || 0,
    });
  }

  // 选择商品
  handleCheck = (e) => {
    console.log('选择', this.state.goodsList);
    let index = e.currentTarget.dataset.index;
    let list = this.state.goodsList;
    if (this.state.goodsEdit) {
      list[index].editChecked = !list[index].editChecked;
    } else {
      list[index].checked = !list[index].checked;
    }
    this.setState({
      goodsList: list,
    },() => {
      this.judgeAllCheck();
      this.calcPrice();
    });
    this.count();
  }

  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 判断是否全选
  judgeAllCheck() {
    let list = this.state.goodsList;
    for (let i = 0; i < list.length; i++) {
      if (this.state.goodsEdit) {
        if (!list[i].editChecked) {
          this.setState({
            editAllChecked: false,
          });
          break;
        }
        this.setState({
          editAllChecked: true,
        });
      } else {
        if (!list[i].checked) {
          this.setState({
            allChecked: false,
          });
          break;
        }
        this.setState({
          allChecked: true,
        });
      }
    }
  }

  // 计算选择商品的价格
  calcPrice() {
    let list = this.state.goodsList;
    let count = 0;
    list.forEach((item) => {
      if (item.checked) {
        count += parseInt(item.salePrice * item.count) + parseInt(item.deposit * item.count);
      }
    });
    this.setState({
      totalPrice: count / 100 || 0,
    });
  }

  // 全选
  handleAllCheck = () => {
    let list = this.state.goodsList;
    let allCheck = this.state.allChecked;
    let editAllChecked = this.state.editAllChecked;
    let goodsEdit = this.state.goodsEdit;
    if (goodsEdit) {
      if (editAllChecked) {
        list = list.map((item) => {
          return {
            ...item,
            editChecked: false,
          };
        });
      } else {
        list = list.map((item) => {
          return {
            ...item,
            editChecked: true,
          };
        });
      }
    } else {
      if (allCheck) {
        list = list.map((item) => {
          return {
            ...item,
            checked: false,
          };
        });
      } else {
        list = list.map((item) => {
          return {
            ...item,
            checked: true,
          };
        });
      }
    }
    this.setState({
      goodsList: list,
      allChecked: goodsEdit ? allCheck : !allCheck,
      editAllChecked: goodsEdit ? !editAllChecked : editAllChecked,
    },() => {
      this.calcPrice();
      this.count();
    });
  }

  // 编辑
  handleEdit = () => {
    let list = this.state.goodsList;
    if (!this.state.goodsEdit) {
      list = list.map((item) => {
        return {
          ...item,
          editChecked: false,
        };
      });
    }
    this.setState({
      goodsEdit: !this.state.goodsEdit,
      goodsList: list,
    });
  }

  // 立即下单或删除
  async handleFoot() {
    if (!authHelper.checkAuth()) return;
    let goodsEdit = this.state.goodsEdit;
    let list = this.state.goodsList;
    console.log(list);
    if (goodsEdit) {
      let ishasDeleteList = list.find((item) => {
        return item.editChecked == true;
      });
      if (!ishasDeleteList) {
        wxApi.showToast({
          icon: 'none',
          title: '请选择要删除的服务',
        });
        return;
      }
      let filterList = list.filter((item) => {
        return !item.editChecked;
      });
      console.log(filterList);
      this.setState({
        goodsEdit: !this.state.goodsEdit,
        goodsList: filterList,
      },()=>{
        this.calcPrice();
        // 缓存购物车数据
        let stringifyList = filterList.map((item) => {
          return JSON.stringify(item);
        });
        wxApi.setStorageSync('serviceTreeData', stringifyList);
        wxApi.setStorageSync('serviceOrderList', stringifyList);
        this.count();
      });
    } else {
      // 下单数据
      let orderList = this.state.goodsList.filter((item) => {
        return item.checked;
      });
      let stringifyOrderList = orderList.map((item) => {
        return JSON.stringify(item);
      });
      wxApi.setStorageSync('serviceOrderList', stringifyOrderList);

      // 判断是否有服务不在服务时间内
      let nowTime;
      await wxApi.request({
        url: api.livingService.getNowDate,
      }).then(res=>{
        nowTime = new Date(res.data)
      })
      const noServiceTime = stringifyOrderList.filter(item=>{
        return !utils.time_range(JSON.parse(item).serverStartTime,JSON.parse(item).serverEndTime,nowTime)
      })
      if (noServiceTime.length) {
        wxApi.showToast({
          title: `${JSON.parse(noServiceTime[0]).serverName}不在服务时间`,//提示文字
          duration: 2000,//显示时长
          icon: 'none', //图标，支持"success"、"loading"
        })
        return
      }

      //判断是否包含需要扫码的商品
      const flag = await utils.checkNeedScan(stringifyOrderList.map((e) => JSON.parse(e)));
      if (!flag) {
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
          data: {},
        });

        return;
      }

      if (storeData.globalData.roomCode) {
        if (orderList.length == 0) {
          wxApi.showToast({
            icon: 'none',
            title: '请选择要下单的服务',
          });
          return;
        }
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
          data: {
            roomCode: state.roomCode,
            roomCodeTypeId: state.roomCodeTypeId,
            roomCodeTypeName: state.roomCodeTypeName,
            trueCode: state.roomCodeTypeName ? state.roomCodeTypeName + '-' + state.roomCode : state.roomCode,
          },
        });
      } else {
        // if (process.env.WX_OA === 'true') {
        //   // 公众号
        //   wxApi.$navigateTo({
        //     url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
        //     data: {
        //       needScan: true
        //     },
        //   });
        // } else {
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/living-service/scan-code/index'
          });
        // }
      }
    }
  }

  //购物车添加
  clickPlus = (e) => {
    console.log(e);
     //判断库存
     if(this.judgeStock(this.state.goodsList,e.currentTarget.dataset.item,this.state.stockList) == 1 && e.currentTarget.dataset.item.useStock == 1){
      return
    }
    const index = e.currentTarget.dataset.index;
    let goodsList = this.state.goodsList;
    let count = goodsList[index].count;
    count = count + 1;
    goodsList[index].count = count;
    this.setState({
      goodsList: goodsList,
    });
    this.calcPrice();
    goodsList = goodsList.map((item) => {
      return JSON.stringify(item);
    });
    wxApi.setStorageSync('serviceTreeData', goodsList);
    wxApi.setStorageSync('serviceOrderList', goodsList);
    this.count();
  }

  //购物车减少
  clickMinus = (e) => {
    console.log(e);
    const index = e.currentTarget.dataset.index;
    let goodsList = this.state.goodsList;
    let count = goodsList[index].count;
    if (count <= 1) {
      return false;
    }
    count = count - 1;
    goodsList[index].count = count;
    this.setState({
      goodsList: goodsList,
    });
    this.calcPrice();
    goodsList = goodsList.map((item) => {
      return JSON.stringify(item);
    });
    console.log('asdf333333333333333333', goodsList);
    wxApi.setStorageSync('serviceTreeData', goodsList);
    wxApi.setStorageSync('serviceOrderList', goodsList);
    this.count();
  }

  // 查看上传的图片
  previewUploadImage(previewUrls){
    wxApi.previewImage({
      urls: previewUrls
    });
  }

  // 查询库存
  getServerItemStock = (params = [])=>{
    if(!params.length){
      return
    }
    wxApi.request({
      url:api.livingService.getServerItemStock,
      loading: true,
      method: 'POST',
      data:params
    }).then((res)=>{
      this.setState({
        stockList:res.data || []
      })
    })
  }

  judgeStock=(list,item,stockList)=>{
    let itemList = []
    let count = 0
    let stock = 0
    for(let i = 0;i<list.length;i++){
      if(list[i].id == item.id){
        itemList.push(list[i])
      }
    }
    for(let j = 0;j<itemList.length;j++){
      count = count + itemList[j].count
    }
    for(let i = 0;i<list.length;i++){
      if(list[i].id == item.id){
        itemList.push(list[i])
      }
    }
    for(let c = 0;c<stockList.length;c++){
      if(stockList[c].id == item.id){
        stock = stockList[c].itemStock
      }
    }
    if(count >= stock){
      return 1
    }else{
      return 0
    }
  }

  render() {
    const { goodsEdit, editAllChecked, allChecked, tmpStyle, goodsList, totalPrice, count,stockList } = this.state;
    return (
      <View data-scoped="wk-mplg-GoodsReserveList" className="wk-mplg-GoodsReserveList">
        {goodsList.length !== 0 && (
          <View className="header">
            <View>
              <Radio
                value={goodsEdit ? editAllChecked : allChecked}
                color={tmpStyle.btnColor}
                checked={goodsEdit ? editAllChecked : allChecked}
                onChange={this.handleAllCheck}
              ></Radio>
              <Text>全选</Text>
            </View>
            <Text onClick={this.handleEdit}>{goodsEdit ? '完成' : '编辑'}</Text>
          </View>
        )}

        <View className="container">
          {goodsList.length !== 0 ? (
            <View>
              {
                goodsList?.map((item, index) => {
                  return (
                    <View key={item.serverId} className="goods-item">
                      <View className="goods-item-info">
                        <View className="radio-image-box">
                          <Radio
                            value={goodsEdit ? item.editChecked : item.checked}
                            color={tmpStyle.btnColor}
                            checked={goodsEdit ? item.editChecked : item.checked}
                            onChange={_fixme_with_dataset_(this.handleCheck, { index: index })}
                          ></Radio>
                          <Image src={filters.imgListFormat(item.img)} className="goods-item-info-img"></Image>
                        </View>
                        <View className="goods-item-box">
                          <View className="goods-item-info-name">
                            <View>{item.serverName}</View>
                            <View className="cash">{'押金￥' + item.deposit / 100}</View>
                          </View>
                          <View className="goods-item-info-form">
                            {
                              item.widgetValue?.map((el, index) => {
                                return (
                                  // <View key={el.label}>
                                  //   <View>{el.label + '：' + el.value}</View>
                                  // </View>
                                  el.type == 'uploadImage' && el.value.length?
                                  <View>{el.label}: 共{el.value.length}张图片 <Text style={_safe_style_(`color:${tmpStyle.btnColor};`)} onClick={this.previewUploadImage.bind(this,el.value)}>查看</Text>
                                  </View>:
                                  <View key={el.uuid}>
                                    <View>{el.label + ': ' + el.value}</View>
                                  </View>
                                );
                              })}
                            <View className="count-wrap">
                              <View style={_safe_style_('color:' + tmpStyle.btnColor + ';font-size:32rpx')}>
                                {item.needPayFee == 1 ? '￥' + item.salePrice / 100 : getFreeLabel(item.showType)}
                              </View>
                              {item.count > 1 && (
                                <View>
                                  <View
                                    className="minus left-radius"
                                    onClick={_fixme_with_dataset_(this.clickMinus, {
                                      item: item,
                                      index: index,
                                    })}
                                  >
                                    -
                                  </View>
                                  <View className="num">
                                    <Input className="input-comp" type="number" value={item.count} disabled></Input>
                                  </View>
                                  <View
                                    className="plus right-radius"
                                    style={_safe_style_('background-color:' + tmpStyle.btnColor + (this.judgeStock(goodsList,item,stockList) == 1 && item.useStock == 1 ? ';opacity:0.3':''))}
                                    onClick={_fixme_with_dataset_(this.clickPlus, {
                                      item: item,
                                      index: index,
                                    })}
                                  >
                                    +
                                  </View>
                                </View>
                              )}
                            </View>
                          </View>
                        </View>
                        {!goodsEdit && item.count == 1 && (
                          <View className="goods-item-info-right">
                            <View>{'x' + item.count}</View>
                          </View>
                        )}
                      </View>
                    </View>
                  );
                })}
            </View>
          ) : (
            <View className="empty">暂无预订清单</View>
          )}
        </View>
        {goodsList.length !== 0 && (
          <View className="goods-all-info">
            <View className="goods-all-info-price">
              <View>合计：</View>
              <Text className="font-color">{'￥' + totalPrice}</Text>
            </View>
            {goodsEdit && (
              <View
                className="mini-btn"
                type="warn"
                size="mini"
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleFoot.bind(this)}
              >
                删除
              </View>
            )}

            {!goodsEdit && (
              <View
                className="mini-btn"
                type="warn"
                size="mini"
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.handleFoot.bind(this)}
              >
                {'立即下单\r\n      ' + (count ? '(' : '') + (count ? count : '') + (count ? ')' : '')}
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

export default GoodsReserveList;
