import { $getRouter } from 'wk-taro-platform';
import {  View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';
import { connect } from 'react-redux';
import ShareDialog from "@/wxat-common/components/share-dialog";
import shareUtil from '@/wxat-common/utils/share';
import { showShareMenu,hideShareMenu,hideShareTimelineMenu,showShareTimelineMenu, setShareAppMessage } from '@/wxat-common/utils/platform/official-account.h5'


const app = Taro.getApp();

interface StateProps {
  templ: any;
  industry: string;
  sessionId: string;
  currentStore: any;
  environment: any;
  appInfo: any;
}

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
  };
};

interface HotelReservationShare {
  props: IProps;
}


@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class HotelReservationShare extends React.Component {

  $router = $getRouter();
  state = {
    tmpStyle: {}, // 主题模板配置
    sessionId: null,
    companyName:null,
    company_code:null,
    shareTitle:null,
    shareimageUrl:'https://front-end-1302979015.file.myqcloud.com/images/c/images/plugin/ukShareImg.png',
    refShareDialogCMTP: React.createRef<any>(),
  };
  onLoad(options){
    Taro.nextTick(() => {
    setShareAppMessage(this.onShareAppMessage())
  })
 }

  onShareAppMessage = (e) => {
    const { companyName, company_code,shareTitle, shareimageUrl} = this.state;
    const imageUrl = shareimageUrl;
    const url =`sub-packages/marketing-package/pages/living-service/hotel-reservation-H5/index?companyName=${companyName}&company_code=${company_code}`;
    const title = shareTitle;
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.UKEMPLOYEE_INVITATION,
      bzName: `产品${title ? '-' + title : ''}`,
    });
    console.log('sharePath => ', path);
    return { title, path, imageUrl };
  };


  // async componentDidMount() {

  // }
  componentDidMount() {
    wxApi.showLoading({
      title: '正在加载信息...',
    });


    hideShareMenu();
    hideShareTimelineMenu();

    console.log("++++++++++++++1",this.$router)
    const params = this.$router.params;
    console.log("++++++++++++++2",params.title)
    this.setState({
      companyName: params.companyName,
      company_code: params.company_code,
      shareTitle:params.title
    },()=>{
      setTimeout(()=>{
        wxApi.hideLoading;
        this.handleSelectChanel()
      },2000)
    })

	}
  async componentDidUpdate(preProps) {

  }
  /**
   * 获取邀请好友对话弹框
   */
  //  handleSelectChanel = () => {
  //   this.refShareDialogCMPT.current.show();
  // };

   handleSelectChanel = ()=>{
    this.state.refShareDialogCMTP.current.show("shareYouke");
    Taro.nextTick(() => {
      setShareAppMessage(this.onShareAppMessage(),()=>{
        showShareMenu();
        showShareTimelineMenu();
      })
    });
  };

  operationText = () => {

  }

  apiError = () => {
    this.setState({
      error: true,
    });
  };


  init = async () => {



  };

  initRenderData() {
    this.getTemplateStyle(); // 获取主题模板配置


  };

  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };








  render() {

    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpvd-Detail" className="wk-mpvd-Detail">
      {/*  分享对话弹框  */}
        <ShareDialog
          childRef={this.state.refShareDialogCMTP}
        ></ShareDialog>
      </View>
    );
  }
}

HotelReservationShare.enableShareAppMessage = true

export default HotelReservationShare;
