import { _safe_style_ } from 'wk-taro-platform';
import { View, Image, Text, ScrollView } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import template from '../../../../../wxat-common/utils/template.js';
import ImageModule from '../../../../../wxat-common/components/decorate/imageModule/index';
import './index.scss';
class OrderListDetail extends React.Component {

  state = {
    isshowTc: false, //默认关闭付款明细弹窗
    orderNo: '',
    orderDetail: {},
    pageConfig: [], //广告位
    imgUrlList: null,
    detDayAll: [],
    formExt: [],
    getDayAll: [],
    tmpStyle: {}, //主题
    loading: false
  }

  /**
  * 生命周期函数--监听页面加载
  */
    onLoad(options) {
    this.setState(
      {
        orderNo: options.orderNo,
      },
      () => {
        this.getPlatformOrderDetail();
        this.getSpace();
        this.getTemplateStyle();
      }
    )
  }

  //打开付款明细弹窗
  showTc = () => {
    let isshowTc = !this.state.isshowTc;
    this.setState({
      isshowTc
    })
  }

  //关闭付款明细弹窗
  closeTc = () => {
    this.setState({
      isshowTc: false
    })
  }

  //获取主题模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  getPlatformOrderDetail = () => {
    Taro.showLoading(
      {
        title: '正在加载',
        mask: true
      }
    )
    this.setState(
      {
        loading: false
      }
    )
    wxApi
      .request({
        url: api.livingService.getplatformDetail,
        method: 'GET',
        loading: true,
        data: {
          orderNo: this.state.orderNo,
        },
      })
      .then((res) => {
        (res.data.orderPlatform.imgList = (res.data.orderPlatform.img || '').split(',')[0]),
          (res.data.orderPlatform.enterTime = this.MonthDayformat(res.data.orderPlatform.timeSlot.split(',')[0]));

        res.data.orderPlatform.leaveTime = this.MonthDayformat(res.data.orderPlatform.timeSlot.split(',')[1]);

        let getDayAll = this.getDayAll(
          res.data.orderPlatform.timeSlot.split(',')[0],
          res.data.orderPlatform.timeSlot.split(',')[1]
        )

        res.data.actualFee = res.data.itemList[0].actualFee / 100;
        res.data.orderPlatform.deposit = res.data.orderPlatform.deposit / 100;
        res.data.orderPlatform.salePrice = (res.data.orderPlatform.salePrice / 100).toFixed(2);
        let formExt = JSON.parse(res.data.orderPlatform.formExt);
        this.setState({
          orderDetail: res.data,
          getDayAll,
          formExt
        })
      }).finally(() => {
        Taro.hideLoading()
        this.setState(
          {
            loading: true
          }
        )
      })
  }

  //获取广告位
  getSpace = () => {
    wxApi
      .request({
        url: api.livingService.getSpace,
        data: {
          type: 2,
        },
      })
      .then((res) => {
        if (!res.data) return false
        let pageConfig = res.data
        let imgUrlList = JSON.parse(pageConfig.imgUrl);
        imgUrlList.height = 240;
        imgUrlList.margin = 0;
        imgUrlList.radius = 6;
        this.setState({
          pageConfig,
          imgUrlList,
        });
      })
  }

  //格式化日期，只显示月份和日
  MonthDayformat = (date) => {
    let Dtime = new Date(date);
    var Dtimemonth = Dtime.getMonth() + 1;
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}月${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }日`
  }

  //获取时间段
  getDayAll = (starDay, endDay) => {
    var arr = [];
    var dates = [];
    var db = new Date(starDay);
    var de = new Date(endDay);
    var s = db.getTime() - 24 * 60 * 60 * 1000;
    var d = de.getTime() - 24 * 60 * 60 * 1000 * 2;
    for (var i = s; i <= d; ) {
      i = i + 24 * 60 * 60 * 1000;
      arr.push(parseInt(i));
    }
    for (var j in arr) {
      var time = new Date(arr[j]);
      var year = time.getFullYear(time);
      var mouth = time.getMonth() + 1 >= 10 ? time.getMonth() + 1 : '0' + (time.getMonth() + 1);
      var day = time.getDate() >= 10 ? time.getDate() : '0' + time.getDate();
      var YYMMDD = year + '-' + mouth + '-' + day;
      dates.push(YYMMDD);
    }
    return dates;
  }

  render() {
    const { tmpStyle, orderDetail, imgUrlList, formExt, getDayAll, isshowTc, loading } = this.state
    return (
      <>
      {
        loading && (
          <View data-scoped="wk-mplo-OrderListDetail" className="wk-mplo-OrderListDetail order-list-detail">
            <View className="order-list-box" style={_safe_style_('background: ' + tmpStyle.bgGradualChange + ';')}>
              <View className="orderBox-head">
                <View>
                  <Image
                    className="order-head-img"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-time.png"
                    mode="aspectFit"
                  ></Image>
                </View>
                <View className="order-head-text">{filters.showOrderDesc(orderDetail.orderStatus)}</View>
              </View>
            </View>
            <View className="order-list-content">
              <View className="order-list-item">
                <View className="order-hotel">
                  <View className="order-list-left">
                    <Image className="listLeftImg" src={orderDetail.orderPlatform.imgList} mode="aspectFill"></Image>
                  </View>
                  <View className="order-list-right">
                    <View className="hotel-name">
                      {orderDetail.orderPlatform.categoryType == 3
                        ? orderDetail.storeName
                        : orderDetail.orderPlatform.categoryName}
                    </View>
                    <View className="hotle-hui">{orderDetail.orderPlatform.serverName}</View>
                    {orderDetail.orderPlatform.categoryType == 3 && (
                      <View className="hotle-hui">
                        <Text>{orderDetail.orderPlatform.enterTime}</Text>（入住）/
                        <Text>{orderDetail.orderPlatform.leaveTime}</Text>（退房）共
                        <Text>{orderDetail.itemList[0].itemAttribute}</Text>
                        {orderDetail.itemList[0].itemUnit}
                      </View>
                    )}

                    <View className="hotle-hui hotle-bottom">
                      <Text>
                        <Image
                          className="location-icon"
                          src="https://front-end-1302979015.file.myqcloud.com/images/location-green.svg"
                          mode="aspectFit"
                        ></Image>
                      </Text>
                      <Text className="hotle-hui">{orderDetail.storeAddress}</Text>
                    </View>
                  </View>
                </View>
              </View>
              {!!imgUrlList && (
                <View className="hotel-list-carousel">
                  <ImageModule dataSource={imgUrlList}></ImageModule>
                </View>
              )}

              <View className="order-list-item">
                <View className="order-infomation">
                  <View className="orderInfomation-item" onClick={this.showTc}>
                    <View className="paytext">付款</View>
                    <View>
                      <Image
                        className="right-icon"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        mode="aspectFit"
                      ></Image>
                    </View>
                  </View>
                  {orderDetail.actualFee && (
                    <View className="orderInfomation-item">
                      <View>{'房费：￥' + orderDetail.actualFee}</View>
                    </View>
                  )}

                  {!!orderDetail.orderPlatform.deposit && (
                    <View className="orderInfomation-item">
                      <View>
                        {'押金：￥' + orderDetail.orderPlatform.deposit}
                        {orderDetail.orderStatus == -20 && <Text>（已退全额）</Text>}
                      </View>
                    </View>
                  )}
                </View>
              </View>
              <View className="order-list-item">
                <View className="order-infomation">
                  <View className="orderInfomation-item">
                    <View className="paytext">订单信息</View>
                  </View>
                  <View className="orderInfomation-item">
                    <View>{'订单号：' + orderDetail.orderPlatform.orderNo}</View>
                  </View>
                  <View className="orderInfomation-item">
                    <View>{'下单时间：' + orderDetail.orderTime}</View>
                  </View>
                  {orderDetail.roomCode && (
                    <View className="orderInfomation-item">
                      <View>{'房间号：' + orderDetail.roomCode}</View>
                    </View>
                  )}

                  {formExt &&
                    formExt.map((item, index) => {
                      return (
                        <View className="orderInfomation-item">
                          <View>{item.label + '：' + item.value}</View>
                        </View>
                      );
                    })}
                </View>
              </View>
            </View>
            {/* ----------底部弹窗----------- */}
            {isshowTc && (
              <View className="tc-box">
                <View className="tc-hui" onClick={this.closeTc}></View>
                <View className="tc-content">
                  <View className="tc-title">付款明细</View>
                  <View className="scroll-content">
                    <View className="scroll-item-head">
                      <Text className="scroll-title">合计</Text>
                      <Text className="scroll-price right">{'¥' + orderDetail.actualFee}</Text>
                    </View>
                    <ScrollView className="scroll-item-list" scrollY={true}>
                      {
                        !!getDayAll.length &&
                          getDayAll.map((item, index) => {
                            return (
                              <View className="scrollList-item" key={'scrollList-item-' + index}>
                                <Text>{item}</Text>
                                <Text className="right">
                                  {'¥' + orderDetail.orderPlatform.salePrice + ' × ' + orderDetail.itemList[0].itemCount}
                                </Text>
                              </View>
                            );
                          })
                        }
                    </ScrollView>
                  </View>
                </View>
              </View>
            )}
          </View>
        )
      }
      </>
    )
  }
}

export default OrderListDetail;
