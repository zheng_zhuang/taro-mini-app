import { _fixme_with_dataset_, _safe_style_, $getRouter } from 'wk-taro-platform';
import { View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import template from '@/wxat-common/utils/template';
import ImageModule from '@/wxat-common/components/decorate/imageModule/index';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import './index.scss';
import {getFreeLabel} from '@/wxat-common/utils/service'
class ServiceorderDetails extends React.Component {

  state = {
    isshowTc: false, // 默认关闭付款明细弹窗
    orderNo: '',
    orderDetail: {},
    pageConfig: [], // 广告位
    imgUrlList: null,
    detDayAll: [],
    formExt: [], // 动态表单
    tmpStyle: {
      btnColor1: ''
    }, // 主题
    newOrderDetail: {},
    detailList: []
  }

  $router = $getRouter()
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {
    const options = this.$router.params
    this.setState({
      orderNo: options.orderNo,
    }, () => {
      this.getPlatformOrderDetail();
      this.getSpace();
      this.getTemplateStyle();
    });
  }

  // 获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 打开付款明细弹窗
  showTc () {
    const isshowTc = !this.state.isshowTc;
    this.setState({
      isshowTc,
    });
  }

  // 关闭付款明细弹窗
  closeTc () {
    this.setState({
      isshowTc: false,
    });
  }

  getPlatformOrderDetail () {
    console.log('this.state.orderNo', this.state.orderNo);
    wxApi
      .request({
        url: api.livingService.getplatformDetailNew,
        method: 'GET',
        loading: true,
        data: {
          orderNo: this.state.orderNo,
        },
      })
      .then((res) => {
        console.log('res', res);
        const detailList = res.data.serverPlatformItemList.map((item) => {
          return {
            ...item,
            parseFormExt: item.formExt ? JSON.parse(item.formExt) : [],
            amountText: (item.amount / 100).toFixed(2),
          };
        });
        console.log(detailList);
        this.setState({
          detailList, // 服务项列表
          newOrderDetail: res.data, // 订单详情
        });
        (res.data.orderPlatform.imgList = (res.data.orderPlatform.img || '').split(',')[0]),
          (res.data.orderPlatform.enterTime = this.MonthDayformat(res.data.orderPlatform.timeSlot.split(',')[0]));

        res.data.orderPlatform.leaveTime = this.MonthDayformat(res.data.orderPlatform.timeSlot.split(',')[1]);

        const getDayAll = this.getDayAll(
          res.data.orderPlatform.timeSlot.split(',')[0],
          res.data.orderPlatform.timeSlot.split(',')[1]
        );

        res.data.actualFee = res.data.itemList[0].actualFee / 100;
        res.data.orderPlatform.deposit = res.data.orderPlatform.deposit / 100;
        res.data.orderPlatform.salePrice = (res.data.orderPlatform.salePrice / 100).toFixed(2);
        console.log('res', res.data);
        const formExt = JSON.parse(res.data.orderPlatform.formExt);
        this.setState({
          orderDetail: res.data,
          getDayAll,
          formExt,
        });
      });
  }

  // 获取广告位
  getSpace() {
    wxApi
      .request({
        url: api.livingService.getSpace,
        data: {
          type: 2,
        },
      })
      .then((res) => {
        const pageConfig = res.data || [];
        const imgUrlList = JSON.parse(pageConfig.imgUrl);
        imgUrlList.height = 240;
        imgUrlList.margin = 0;
        imgUrlList.radius = 6;
        console.log(imgUrlList);
        this.setState({
          pageConfig,
          imgUrlList,
        });
      })
      .catch((error) => {});
  }

  // 格式化日期，只显示月份和日
  MonthDayformat(date) {
    const Dtime = new Date(date);
    const Dtimemonth = Dtime.getMonth() + 1;
    return `${Dtime.getMonth() + 1 < 10 ? `0${Dtimemonth}` : Dtimemonth}月${
      Dtime.getDate() < 10 ? `0${Dtime.getDate()}` : Dtime.getDate()
    }日`;
  }

  // 获取时间段
  getDayAll(starDay, endDay) {
    const arr: number[] = [];
    const dates: string[] = [];
    const db = new Date(starDay);
    const de = new Date(endDay);
    const s = db.getTime() - 24 * 60 * 60 * 1000;
    const d = de.getTime() - 24 * 60 * 60 * 1000 * 2;
    for (let i = s; i <= d; ) {
      i = i + 24 * 60 * 60 * 1000;
      arr.push(parseInt(i));
    }
    for (const j of arr) {
      const time = new Date(arr[j]);
      const year = time.getFullYear(time);
      const mouth = time.getMonth() + 1 >= 10 ? time.getMonth() + 1 : `0${time.getMonth() + 1}`;
      const day = time.getDate() >= 10 ? time.getDate() : `0${time.getDate()}`;
      const YYMMDD = `${year}-${mouth}-${day}`;
      dates.push(YYMMDD);
    }
    return dates;
  }

  /**
   * 复制文字
   * @param {*} e
   */
  copyText(e) {
    wxApi.setClipboardData({
      data: e.currentTarget.dataset.text,
      success: function () {
        wxApi.getClipboardData({
          success: function () {
            wxApi.showToast({
              title: '复制成功',
            });
          },
        });
      },
    });
  }

  // 查看上传的图片
  previewUploadImage(previewUrls,e){
    e.stopPropagation()
    wxApi.previewImage({
      urls: previewUrls
    });
  }

  render() {
    const { tmpStyle, newOrderDetail, detailList, imgUrlList } = this.state;
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mpls-ServiceorderDetails"
        className="wk-mpls-ServiceorderDetails"
      >
        {/*  订单状态  */}
        <View className="order-status-box" style={_safe_style_(`background: ${tmpStyle.btnColor1}`)}>
          <Image
            className="icon-message"
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-message.png"
          ></Image>
          <View className="order-status-desc">
            <View>{filters.showOrderDesc(newOrderDetail.orderStatus)}</View>
          </View>
        </View>
        {/*  订单详情  */}
        <View className="detail-container">
          {detailList ?
            detailList.map((item, index) => {
              return (
                <View key={index}>
                  <View className="item-box">
                    <View className="item-tab">
                      <View className="item-icon-box">
                        {item.img && <Image className="item-icon" src={filters.imgListFormat(item.img)}></Image>}
                      </View>
                      {/*  商品详情  */}
                      <View className="item-info">
                        {/*  商品名称  */}
                        <View className="item-name limit-line">{item.serverName || ''}</View>
                        {item.parseFormExt ?
                          item.parseFormExt.map(el => {
                            return (
                              el.type === 'uploadImage'&& el.value.length ? (
                                <View>{el.label}: 共{el.value.length}张图片 <Text style={_safe_style_(`color:${tmpStyle.btnColor};`)} onClick={this.previewUploadImage.bind(this,el.value)}>查看</Text>
                                </View>
                              ) : (<View className="item-attribute">{`${el.label}:${el.value || ''}`}</View>)
                            );
                          }) : ''}
                        {/* ------取件码----- */}
                        {!!item.robotFetchCode && (
                          <View className="pickUpCode">
                            取件码：<Text>{item.robotFetchCode}</Text>
                          </View>
                        )}

                        <View
                          className="item-info-bottom"
                          style={_safe_style_('margin-top: ' + (item.parseFormExt.length ? '28rpx' : '75rpx'))}
                        >
                          <View className="item-info-price-count price">{item.needPayFee == 1 ? ('￥' + item.amountText) : getFreeLabel(item.showType)}</View>
                          <View className="item-info-price-count">{'x' + item.itemCount}</View>
                        </View>
                        {/*  </View>   */}
                      </View>
                      {/* <View class="right-info">
                            <View>x{{item.itemCount}}</View>
                            <View>押金：￥{{ item.deposit / 100 }}</View>
                            <View>售价：￥{{ item.amount / 100 }}</View>
                          </View>  */}
                    </View>
                  </View>
                </View>
              );
            }) : ''}
          {!!imgUrlList && (
            <View className="hotel-list-carousel">
              <ImageModule dataSource={imgUrlList}></ImageModule>
            </View>
          )}

          {/*  订单支付价格信息  */}
          <View className="info-box">
            <View className="info-title">订单信息</View>
            <View className="info-tab">
              <Text className="goods-label">{`订单号： ${newOrderDetail.orderNo}`}</Text>
              <Text className="copy" onClick={_fixme_with_dataset_(this.copyText, { text: newOrderDetail.orderNo })}>
                复制
              </Text>
            </View>
            <View className="info-tab">
              <Text className="goods-label">
                {`下单时间： ${filters.dateFormat(newOrderDetail.createTime, 'yyyy-MM-dd hh:mm:ss')}`}
              </Text>
            </View>
            {newOrderDetail.deposit === true && (
              <View className="info-tab">
                <Text className="goods-label">押金：</Text>
                <Text className="goods-price-light">{`￥${filters.moneyFilter(newOrderDetail.deposit, true)}`}</Text>
              </View>
            )}

            {newOrderDetail.actualFee ? (
              <View className="info-tab">
                <Text className="goods-label">服务费用：</Text>
                <Text className="goods-price-light">{`￥${filters.moneyFilter(newOrderDetail.actualFee, true)}`}</Text>
              </View>
            ) : ''}

            <View className="info-tab">
              <Text className="goods-label">配送位置：</Text>
              <Text className="goods-price-light">{newOrderDetail.location || '--'}</Text>
            </View>
            {newOrderDetail.payFee ? (
              <View className="info-tab">
                <Text className="goods-label">付款方式：</Text>
                <Text>{newOrderDetail.payChannel || '微信'}</Text>
              </View>
            ) : ''}

            <View className="info-tab" style={_safe_style_('display: flex;')}>
              <Text className="goods-label">备注：</Text>
              <Text style={_safe_style_('flex: 1;')}>{newOrderDetail.orderMessage || '--'}</Text>
            </View>
          </View>
          {/*  订单表单信息  */}
          {/* <View class="info-box">
                <View class="info-title">表单信息</View>
                <View class="info-tab"  wx:for="{{formExt}}">
                <text class='goods-label'> {{item.label}}：{{item.value}}</text>
                </View>
              </View>  */}
        </View>
      </View>
    );
  }
}

export default ServiceorderDetails;
