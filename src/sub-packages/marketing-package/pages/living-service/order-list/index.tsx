import { View } from '@tarojs/components';
import { $getRouter } from 'wk-taro-platform';
import React from 'react';
// import wxApi from '@/wxat-common/utils/wxApi';
// import api from '@/wxat-common/api/index';
import OrderList from '@/wxat-common/components/tabbarLink/serviceorder-list/index';
import './index.scss';

class OrderList1 extends React.Component {
  state = {
    // 是否发放优惠券，下完单之后跳转到订单列表，发放优惠券
    coupon: null
  }

  $router = $getRouter()
  orderListRef = React.createRef()

  onLoad() {
  }

  render() {
    return (
      <View
        data-fixme="03 add view wrapper. need more test"
        data-scoped="wk-mplo-OrderList"
        className="wk-mplo-OrderList"
      >
        <OrderList fromPage="serveOrder"></OrderList>
      </View>
    );
  }
}

export default OrderList1;
