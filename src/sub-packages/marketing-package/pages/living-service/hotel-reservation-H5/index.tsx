import { $getRouter} from 'wk-taro-platform';

import {  View, } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import './index.scss';
import { connect } from 'react-redux';
import authHelper from '@/wxat-common/utils/auth-helper';
import H5UrlEnum from '@/wxat-common/constants/H5UrlEnum.js';
import wxApi from "@/wxat-common/utils/wxApi";
interface StateProps {
  templ: any;
  industry: string;
  sessionId: string;
  currentStore: any;
  environment: any;
  appInfo: any;
  userInfo: any;
  loginInfo: { wxAppId: any, appId: any },
}

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
    userInfo: state.base.userInfo,
    loginInfo: state.base.loginInfo,

  };
};

interface hotelReservationH5 {
  props: IProps;
}


@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class hotelReservationH5 extends React.Component {

  $router = $getRouter();
  state = {
    tmpStyle: {}, // 主题模板配置
    sessionId: null,
    companyName:null,
    company_code:null

  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!!nextProps.userInfo && !!this.props.userInfo) {
    // fixme 再获取数据之后跳转页面
      this.isLoginAndTiaozhuan()
    }
  }

  componentDidMount() {

    const params = this.$router.query;

    if( params.companyName){
      // localStorage.setItem('companyName', params.companyName)
      Taro.setStorageSync('companyName', params.companyName)
    };
    if( params.company_code){
      // localStorage.setItem('company_code', params.company_code)
      Taro.setStorageSync('company_code', params.company_code)
    };

    this.setState({
      // companyName: params.companyName?params.companyName:localStorage.getItem('companyName'),
      companyName: params.companyName?params.companyName:Taro.getStorageSync('companyName'),
      // company_code: params.company_code? params.company_code:localStorage.getItem('company_code')
      company_code: params.company_code? params.company_code:Taro.getStorageSync('company_code')
    },()=>{
    })
	}

  isLoginAndTiaozhuan() {

    if (!authHelper.checkAuth()) return;
    this.tiaozhaung()
  }

  tiaozhaung(){
    const { companyName, company_code} = this.state;
    const { appInfo, userInfo,loginInfo } = this.props;
    const openId = userInfo?.wxOpenId || loginInfo?.openId;
    if(!!userInfo && !!appInfo) {
      const target = `${H5UrlEnum.H5_URL}${H5UrlEnum.REGISTERSTAFFPAGE.url}?openid=${openId}&name=${userInfo.nickname}&phone=${userInfo.phone}&avatar=${userInfo.avatarImgUrl}&env=H5&companyName=${companyName}&company_code=${company_code}&user_id=${userInfo.id}&aid=${loginInfo.wxAppId}&brand_id=${loginInfo.appId}`
      // window.location.href = target
      wxApi.$locationTo(target)
    }
  }

  apiError = () => {
    this.setState({
      error: true,
    });
  };


  init = async () => {



  };

  initRenderData() {

  };





  render() {

    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpvd-Detail" className="wk-mpvd-Detail">
      </View>
    );
  }
}



export default hotelReservationH5;
