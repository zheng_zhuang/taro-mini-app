import { _fixme_with_dataset_, _safe_style_, $getRouter } from 'wk-taro-platform';
import {
  Block,
  View,
  Image,
  Text,
  Input,
  Button
} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import store from '@/store';
import filters from '@/wxat-common/utils/money.wxs.js';
import couponEnum from '@/wxat-common/constants/couponEnum';
import BottomDialog from '@/wxat-common/components/bottom-dialog/index';
import wxApi from '@/wxat-common/utils/wxApi';
import constants from '@/wxat-common/constants/index';
import api from '@/wxat-common/api/index';
import utils from '@/wxat-common/utils/util';
import template from '@/wxat-common/utils/template';
import checkOptions from '@/wxat-common/utils/check-options';
import priceHandler from '../../../utils/priceHandler';
import payUtils from '@/wxat-common/utils/pay';
import './index.scss';
import { connect } from 'react-redux';
import {getFreeLabel} from '@/wxat-common/utils/service'
const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};

const storeData = store.getState();

interface PageStateProps {
  currentStore: any
}

type IProps = PageStateProps
interface GoodsRentalPendingpay {
  props: IProps;
}

const mapStateToProps = (state) => ({
  currentStore: state.base.currentStore
})

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class GoodsRentalPendingpay extends React.Component {

  state = {
    payLoading: false, // 支付提交中
    tmpStyle: {}, // 主题模板配置
    itemNo: null, // 商品单号
    goodsImagesOne: '', // 第一张商品图片
    totalPrice: null, // 合计价钱
    trueCode: '',
    data: {},
    roomCode: '', // 房间号
    qrCodeTypeId: null,
    coupons: {
      couponSet: []
    },
    optionServiceDetail: {
      formExt: [{
        uuid: '',
        label: '',
        value: ''
      }],
      count: '',
      deposit: '',
      serverName: '',

    },
    isCoupon: 1,
    inTime: '',
    outTime: '',
    curCoupon: {
      code: ''
    },
    price: {},
    payDetails: [],
    curGiftCard: {},
    seckillNo: '',
    serveCard: {},
    curRedPacke: {},
    scoreName: '',
    goodsIntegral: '',
    goodsInternal: '',
    goodsInfoList: [{
      wxItem: {}
    }],
    goodsTotalPrice: '',
    isEstateType: false,
    isGiftActivity: false,
    isFrontMoneyItem: false,
    isFullScreen: false,
    isError: false,
    needScan: false
  }

  chooseCard = React.createRef()
  $router = $getRouter()

  /**
   * 生命周期函数--监听页面加载
   */
  async componentDidMount() {
    const options = this.$router.params
    this.setState({
      needScan: !!options.needScan
    })
    const formattedOptions = await checkOptions.checkOnLoadOptions(options);
    if (formattedOptions) {
      this.init(formattedOptions);
    }
    wxApi.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline'],
    });
    if (storeData.globalData.roomCode) {
      this.setState({
        roomCode: storeData.globalData.roomCode,
        trueCode: `${storeData.globalData.roomCodeTypeName}-${storeData.globalData.roomCode}`,
        qrCodeTypeId: storeData.globalData.roomCodeTypeId,
      });
    } else {
      this.setState({
        roomCode: options.roomCode,
        trueCode: options.trueCode,
        qrCodeTypeId: options.roomCodeTypeId,
      });
    }
  }

  async init(options) {
    const itemNo = options.itemNo;
    const optionServiceDetail = wxApi.getStorageSync('serviceDetail');
    // const optionServiceDetail = JSON.parse(wxApi.getStorageSync('serviceDetail'));
    optionServiceDetail.salePrice = parseFloat(optionServiceDetail.salePrice) * parseFloat(optionServiceDetail.count);
    optionServiceDetail.deposit = parseFloat(optionServiceDetail.deposit) * parseFloat(optionServiceDetail.count);
    const goodsImages: any = [];
    goodsImages.push(...optionServiceDetail.img.split(','));
    const goodsImagesOne = goodsImages[0];
    optionServiceDetail.formExt.forEach((item, index) => {
      if (item.type === 'address' && item.value) {
        optionServiceDetail.formExt.splice(Number(index) + 1, 1);
      }
    });
    console.log(options)
    this.setState({
      itemNo,
      roomCode: options.roomCode,
      trueCode: options.trueCode,
      optionServiceDetail,
      goodsImagesOne,
    }, () => {
      this.handleGoodsInfoChange();
    });
    this.getTemplateStyle(); // 获取主题模板配置
    this.totalPrice();
  }

  // 错误提示方法
  errorTip(msg) {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }

  handleGoToCouponList = () => {
    const couponSet = this.state.coupons.couponSet;
    if (!couponSet || couponSet.length < 1) {
      return this.errorTip('无可用优惠券');
    }

    this.setState({
      isCoupon: 1,
    });

    this.getChoseDialog().showModal();
  }

  // 获取优惠对话框
  getChoseDialog() {
    return this.chooseCard.current
  }

  // 计算优惠劵的价格
  handleGoodsInfoChange() {
    let data: any = {
      expressType: 0,
      storeId: this.props.currentStore.id,
      orderServerPlatformDTO: {
        platformDay: 1,
      },
      itemDTOList: []
    };
    console.log(this.state.optionServiceDetail)
    data.itemDTOList = [this.state.optionServiceDetail].map((item: any) => {
      const p = {
        itemNo: item.itemNo,
        skuId: item.skuId,
        itemCount: item.count || 1,
        itemType: 44,
        mpActivityType: '',
        mpActivityId: '',
        mpActivityName: '',
        activityType: '',
        activityId: ''
      };

      // 促销相关的请求参数
      if (item.mpActivityType) {
        p.mpActivityType = item.mpActivityType;
      }
      if (item.mpActivityId) {
        p.mpActivityId = item.mpActivityId;
      }
      if (item.mpActivityName) {
        p.mpActivityName = item.mpActivityName;
      }

      // 赠品相关的请求参数
      if (item.activityType) {
        p.activityType = item.activityType;
      }
      if (item.activityId) {
        p.activityId = item.activityId;
      }
      return p;
    });
    data = utils.formatParams(data);
    this.setState({
      data
    });
    priceHandler
      .fetchPreferentialList(data)
      .then((res) => {
        if (res) {
          const { curRedPacke, curCoupon, coupons } = res;
          if (curCoupon.code) {
            this.setState({
              curRedPacke,
              curCoupon,
              coupons,
            });

            data['orderRequestPromDTO.couponNo'] = curCoupon.code;
          } else {
            this.setState({
              curRedPacke,
              coupons,
              curCoupon,
            });
          }
        }
        // 获取订单价格

        return priceHandler.getPrice(data);
      })
      .then((res) => {
        const { price } = res;
        this.setState({
          price
        });
      })
      .catch((error) => {
        if (!error.quite) {
          // 未选择收货地址时报错
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  // 获取主题
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 计算合计价格
  totalPrice() {
    const optionServiceDetail: any = this.state.optionServiceDetail;
    const totalPrice = parseFloat(optionServiceDetail.salePrice) + parseFloat(optionServiceDetail.deposit);
    this.setState({
      totalPrice,
    });
  }

  // 不使用优惠
  onNoUseCard = () => {
    const data = this.state.data;
    switch (this.state.isCoupon) {
      case 1:
        delete data['orderRequestPromDTO.couponNo'];
        this.setState({
          curCoupon: {},
          data: data,
        });
        break;
      case 2:
        this.setState({
          curRedPacke: {},
        });
        break;
      case 3:
        this.setState({
          serveCard: {},
        });
        break;
      case 4:
        this.setState({
          curGiftCard: {},
        });
        break;
    }
    this.getChoseDialog().hideModal();
    this.handlePreferentialChange();
  }

  onSelectCard = (e) => {
    const coupon = e.currentTarget.dataset.coupon;
    // 优惠券或红包状态是可用的或者优惠类型为礼品卡或次数卡（次数后端返回都是可用的）
    if (
      (coupon.status && coupon.status === 1) ||
      coupon.cardType === constants.card.type.gift ||
      coupon.cardType === constants.card.type.coupon
    ) {
      const data = this.state.data;
      switch (this.state.isCoupon) {
        case 1:
          data['orderRequestPromDTO.couponNo'] = coupon.code;
          this.setState({
            curCoupon: coupon,
            data,
          });
          break;
        case 2:
          this.setState({
            curRedPacke: coupon,
          });
          break;
        case 3:
          this.setState({
            serveCard: coupon,
          });
          break;
        case 4:
          this.setState({
            curGiftCard: coupon,
          });
          break;
      }
      this.getChoseDialog().hideModal();
      this.handlePreferentialChange();
    } else {
      wxApi.showToast({
        title: coupon.rejReason || '该优惠不能使用。',
        icon: 'none',
      });
    }
  }

  /**
   * 优惠信息变化时的handler，
   * 如：优惠券选择，礼品卡选择、红包选择等
   */
  handlePreferentialChange() {
    wxApi.showLoading({
      title: '请稍等',
    });
    priceHandler
      .getPrice(this.state.data)
      .then((res) => {
        const { price } = res;
        this.setState({
          price
        });
      })
      .catch((error) => {
        if (!error.quite) {
          // 未选择收货地址时报错
          wxApi.showToast({
            title: error.msg,
            icon: 'none',
          });
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  // 支付
  handlePay = () => {
    if(this.state.payLoading){
      return
    }
    // 判断是否达到起送金额
    const list = JSON.stringify(this.state.optionServiceDetail.formExt);
    const timeSlot = `${this.state.inTime},${this.state.outTime}`;
    if (this.state.needScan && !this.state.roomCode) {
      wxApi.showToast({
        icon: 'none',
        title: '请输入位置信息'
      })
      return
    }
    this.setState({
      payLoading:true
    })
      wxApi
      .request({
        url: api.livingService.unifiedOrder,
        method: 'POST',
        data: {
          formExt: list,
          itemNo: this.state.itemNo,
          itemCount: this.state.optionServiceDetail.count,
          deposit: this.state.optionServiceDetail.deposit,
          platformDay: 1,
          timeSlot: timeSlot,
          roomCode: this.state.roomCode,
          qrCodeTypeId: this.state.qrCodeTypeId,
          orderRequestPromDTO: {
            couponNo: this.state.curCoupon.code,
          },
          payTradeType: payUtils.returnPayTradeType()
        },
      })
      .then((res) => {
        const payInfo = res.data;
        if (this.state.optionServiceDetail.deposit == 0 && this.state.optionServiceDetail.salePrice == 0) {
          wxApi.$navigateTo({
            url: '/wxat-common/pages/tabbar-order/index',
            data: {
              tabType: 'service_order',
            },
          });
          return
        }
        if(process.env.WX_OA === 'true'){
          wx.chooseWXPay({
            timestamp: payInfo.timeStamp,
            nonceStr: payInfo.nonceStr,
            package: `prepay_id=${payInfo.prepayId}`,
            signType: 'RSA',
            paySign: payInfo.paySign,
            success(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
            cancel(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
          });
        } else {
          wxApi.requestPayment({
            timeStamp: String(payInfo.timeStamp),
            nonceStr: payInfo.nonceStr,
            package: `prepay_id=${payInfo.prepayId}`,
            signType: 'RSA',
            paySign: payInfo.paySign,
            success(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
            fail(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
          });
        }
      })
      .catch((error) => {
        console.log(error)
      }).finally((res) => {
        this.setState({
          payLoading:true
        })
      })
  }

  // 扫码获取房间号
  scanCode = () => {
    Taro.scanCode({
      success: (res) => {
        if (!res.path) {
          return wxApi.showToast({
            title: '无法识别房间信息',
            icon: 'none',
          });
        }
        let path:any = decodeURIComponent(res.path);
        // 取url后面的参数 无需关注
        // let vars = res.path.split('=');
        // vars.splice(0, 1);
        // vars = vars.join('=');
        // const query = vars.split('&');
        // const params: any = {};
        // for (let i = 0; i < query.length; i++) {
        //   const q = query[i].split('=');
        //   if (q.length === 2) {
        //     params[q[0]] = q[1];
        //   }
        // }

        path = path.substring(path.lastIndexOf('?') + 1)
        path = path.split('&')
        const params = {}
        for(let i=0;i<path.length;i++){
          const val = path[i].split('=')
          params[val[0]] = val[1]
        }

        if (params.id) {
          wxApi
            .request({
              url: api.queryQrCodeScenes,
              isLoginRequest: true,
              method: 'GET',
              data: {
                id: params.id,
              },
            })
            .then(({ data }) => {
              const dataObj = {};
              data.split('&').forEach((item) => {
                const key = item.split('=')[0];
                const value = item.split('=')[1];
                dataObj[key] = value;
              });
              return wxApi.request({
                url: api.selectQrCodeNumber,
                isLoginRequest: true,
                method: 'POST',
                data: {
                  id: params.id,
                  hotelId: params.pId || params.hId || params.storeId,
                  qrCodeNumber: params.qrcodeNo,
                },
              });
            })
            .then(({ data: { qrCodeNumber, roomCode, roomNumber, qrCodeTypeName, qrCodeTypeId,storeId } }) => {
              if (storeData.base.userInfo.storeId !== storeId) {
                wxApi.showToast({
                  title: '小程序门店与二维码门店不匹配',
                  icon: 'none'
                });
                return
              }
              if (!roomCode && (params.tp == 1 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              console.log(qrCodeNumber, roomCode, roomNumber, qrCodeTypeName, qrCodeTypeId,params)
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? `${qrCodeTypeName}-${roomNumber}`
                      : `${qrCodeTypeName}-${params.id ? roomCode : qrCodeNumber}`
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
                qrCodeTypeId,
              });
            });
        } else if (params.qrcodeNo) {
          wxApi
            .request({
              url: api.selectQrCodeNumber,
              isLoginRequest: true,
              method: 'POST',
              data: {
                id: params.id,
                hotelId: params.pId || params.hId || params.storeId,
                qrCodeNumber: params.qrcodeNo,
              },
            })
            .then(({ data: { qrCodeNumber, roomCode, roomNumber, qrCodeTypeName, qrCodeTypeId } }) => {
              if ((!roomCode || !qrCodeNumber) && (params.tp === 1 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp === 2 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp === 2
                      ? `${qrCodeTypeName}-${roomNumber}`
                      : `${qrCodeTypeName}-${params.id ? roomCode : qrCodeNumber}`
                    : '',
                roomCode: params.tp === 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
                qrCodeTypeId,
              });
            });
        }
      },
    });
  }

  // 输入位置信息
  inputRoomCode = (e) => {
    const value = e.detail.value
    this.setState({
      roomCode: value
    })
  }

  // 查看上传的图片
  previewUploadImage(previewUrls){
    wxApi.previewImage({
      urls: previewUrls
    });
  }

  render() {
    const {
      payLoading,
      goodsImagesOne,
      optionServiceDetail,
      roomCode,
      trueCode,
      tmpStyle,
      curCoupon,
      price,
      coupons,
      payDetails,
      curGiftCard,
      seckillNo,
      serveCard,
      curRedPacke,
      scoreName,
      goodsIntegral,
      goodsInternal,
      goodsInfoList,
      goodsTotalPrice,
      isEstateType,
      isGiftActivity,
      isFrontMoneyItem,
      isCoupon,
      isFullScreen,
      totalPrice,
      isError,
      needScan
    } = this.state;
    return (
      <View data-scoped="wk-mplg-GoodsRentalPendingpay" className="wk-mplg-GoodsRentalPendingpay goods-rental-details">
        <View className="goods-rental-details-item">
          <View className="goods-rental-information">
            <View className="information-img">
              <Image className="information-img" src={goodsImagesOne} mode="aspectFill"></Image>
            </View>
            <View className="information-text">
              <View className="informationtext-title">{optionServiceDetail.serverName}</View>
              <View className="information-info">
                {optionServiceDetail.formExt ?
                  optionServiceDetail.formExt.map((item) => {
                    return (
                      item.type == 'uploadImage' && item.value.length?
                      <View>{item.label}: 共{item.value.length}张图片 <Text style={_safe_style_(`color:${tmpStyle.btnColor};`)} onClick={this.previewUploadImage.bind(this,item.value)}>查看</Text>
                      </View>:
                      <View key={item.uuid}>
                        <View>{item.label + ': ' + item.value}</View>
                      </View>
                    );
                  }) : ''
                }
                <View className="information-info-count">{`x${optionServiceDetail.count}`}</View>
              </View>
              <View className="informationtext-price" style={_safe_style_(`color:${tmpStyle.btnColor};`)}>
                  { optionServiceDetail.needPayFee == 1 ? <Text>￥</Text> : ''}
                  { optionServiceDetail.needPayFee == 1 ? filters.moneyFilter(optionServiceDetail.salePrice, true) : getFreeLabel(optionServiceDetail.showType) }</View>
            </View>
          </View>
        </View>
        <View className="goods-rental-details-item">
          <View className="information-itemlist">
            {optionServiceDetail.deposit ? (
              <View className="information-items-item">
                <View className="itemsItem-left">应押押金</View>
                <View className="itemsItem-right">
                  {`￥${filters.moneyFilter(optionServiceDetail.deposit, true)}`}
                </View>
              </View>
            ) : ''}

            {optionServiceDetail.salePrice ? (
              <View className="information-items-item">
                <View className="itemsItem-left">应收费用</View>
                <View className="itemsItem-right">
                  {`￥${filters.moneyFilter(optionServiceDetail.salePrice, true)}`}
                </View>
              </View>
            ) : ''}

            {optionServiceDetail.categoryType === 1 ? (trueCode ? (
              <View className="information-items-item">
                <View className="itemsItem-left">
                  配送位置
                  <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>
                </View>
                <View className="itemsItem-right" onClick={this.scanCode} style={_safe_style_('width:500rpx;')}>
                  <Text style={_safe_style_('text-align:right;flex:1;color:#8893a6')}>{trueCode}</Text>
                  {!trueCode && (
                    <View
                      style={_safe_style_(
                        `background-color: ${tmpStyle.btnColor || 'rgba(243, 95, 40, 0.8'};
                        background-image: url('https://front-end-1302979015.file.myqcloud.com/images/scan.svg')`
                      )}
                      className="mask-img"
                    ></View>
                  )}
                </View>
              </View>
            ) : (needScan ? (
              <View className="information-items-item">
                <View className="itemsItem-left">
                  配送位置
                  <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>
                </View>
                <View className="itemsItem-right" style={_safe_style_('width:500rpx;')}>
                  <Input
                    placeholder="请输入位置信息"
                    value={roomCode == 'null' ? '' : trueCode}
                    style={_safe_style_('width:100%')}
                    placeholderStyle="text-align: left;color:#8893A6"
                    onBlur={_fixme_with_dataset_(this.inputRoomCode)}
                  ></Input>
                </View>
              </View>
            ) : '')) : ''}
          </View>
        </View>
        <View className="row-box" onClick={this.handleGoToCouponList}>
          <View className="row-label">优惠劵</View>
          {curCoupon && curCoupon.name ? (
            <View className="right-text">
              {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                <Text>{`${filters.discountFilter(curCoupon.discountFee, true)}折`}</Text>
              )}
              {`减￥${filters.moneyFilter(price.couponPrice, true)}`}
            </View>
          ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
            <View className="right-text">{`可用${coupons.couponSet.length}张`}</View>
          ) : (
            <View className="right-text">无可用</View>
          )}

          <Image
            className="right-icon"
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
          ></Image>
        </View>
        {/*  优惠信息  */}
        <View className="shipping-method">
          {price.fullDecrementPromFee && (
            <View className="row-box">
              <View className="row-label">满减</View>
              <View className="right-text">{`减￥${filters.moneyFilter(price.fullDecrementPromFee, true)}`}</View>
            </View>
          )}

          {payDetails &&
            payDetails.map((item, index) => {
              return (
                <Block key={item.index}>
                  {item.isCustom ? (
                    <View className="row-box">
                      <View className="row-label">{item.label}</View>
                      <View className="right-text">{item.value}</View>
                    </View>
                  ) : (
                    <Block>
                      {item.name === 'gift-card' && (
                        <View className="row-box" onClick={this.handleGoToGiftCardList}>
                          <View className="row-label">礼品卡</View>
                          {curGiftCard && curGiftCard.cardItemName ? (
                            <View className="right-text">
                              {`抵扣￥${filters.moneyFilter(price.giftCardPrice, true)}`}
                            </View>
                          ) : coupons && coupons.userGiftCardDTOSet && coupons.userGiftCardDTOSet.length ? (
                            <View className="right-text">{`可用${coupons.userGiftCardDTOSet.length}张`}</View>
                          ) : (
                            <View className="right-text">无可用</View>
                          )}

                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'coupon' && !seckillNo && (
                        <View className="row-box" onClick={this.handleGoToCouponList}>
                          <View className="row-label">优惠劵</View>
                          {curCoupon && curCoupon.name ? (
                            <View className="right-text">
                              {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                <Text>{`${filters.discountFilter(curCoupon.discountFee, true)}折`}</Text>
                              )}

                              {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                            </View>
                          ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                            <View className="right-text">{`可用${coupons.couponSet.length}张`}</View>
                          ) : (
                            <View className="right-text">无可用</View>
                          )}

                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'card' && (
                        <View className="row-box" onClick={this.handleGoToCard}>
                          <View className="row-label">次数卡</View>
                          <View className="right-text">{serveCard.cardItemName}</View>
                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'redpacke' && (
                        <View className="row-box" onClick={this.handleRedPackeList}>
                          <View className="row-label">红包</View>
                          {curRedPacke && curRedPacke.code ? (
                            <View className="right-text">
                              {`减￥${filters.moneyFilter(price.redPacketPrice, true)}`}
                            </View>
                          ) : coupons && coupons.redPacketSet && coupons.redPacketSet.length ? (
                            <View className="right-text">{`可用${coupons.redPacketSet.length}个`}</View>
                          ) : (
                            <View className="right-text">无可用</View>
                          )}

                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'discount' && (
                        <View className="row-box">
                          <View className="row-label">会员折扣</View>
                          <View className="right-text">
                            {(coupons.memberDiscount && coupons.memberDiscount < 100
                              ? coupons.memberDiscount / 10 + '折'
                              : '无') +
                              '\r\n            减￥' +
                              filters.moneyFilter(price.memberPrice, true)}
                          </View>
                        </View>
                      )}

                      {item.name === 'integral' && (
                        <View className="row-box">
                          <View className="row-label">{scoreName + '抵扣'}</View>
                          <View className="right-text">
                            {goodsIntegral + scoreName + '减￥' + filters.moneyFilter(price.integralPromFee, true)}
                          </View>
                        </View>
                      )}

                      {item.name === 'innerBuy' && (
                        <View className="row-box">
                          <View className="row-label">内购券</View>
                          <View className="right-text">
                            {goodsInternal + '张减￥' + filters.moneyFilter(price.innerBuyPromFee, true)}
                          </View>
                        </View>
                      )}

                      {item.name === 'seckill' && (
                        <View className="row-box">
                          <View className="row-label">秒杀优惠</View>
                          <View className="right-text">{'减￥' + filters.moneyFilter(price.secKillPromFee, true)}</View>
                        </View>
                      )}
                    </Block>
                  )}
                </Block>
              );
            })}
          {price.groupPromFee && (
            <View className="row-box">
              <View className="row-label">拼团优惠</View>
              <View className="right-text">{'-¥ ' + filters.moneyFilter(price.groupPromFee, true)}</View>
            </View>
          )}

          <Block>
            {isEstateType ? (
              <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                <View className="row-label">认筹金</View>
                <View className="right-text">
                  {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                </View>
              </View>
            ) : (
              <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                <View className="row-label">商品原价</View>
                {/*  <View wx:if="{{goodsInfoList[0].seckillPrice > (price.totalPrice||goodsTotalPrice)}}" class="right-text">¥ {{filters.moneyFilter(goodsInfoList[0].seckillPrice,true)}}</View>  */}
                <View className="right-text">
                  {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                </View>
              </View>
            )}
          </Block>

          <View className="row-box">
            <View className="row-label">共优惠</View>
            <View className="right-text">
              {'- ¥ ' +
                (isGiftActivity
                  ? filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)
                  : filters.moneyFilter(price.totalFreePrice || 0, true) < 0
                  ? 0
                  : filters.moneyFilter(price.totalFreePrice || 0, true))}
            </View>
          </View>
          {price && isFrontMoneyItem && (
            <View className="row-box">
              <View className="row-label">尾款</View>
              <View className="right-text">{'¥ ' + filters.moneyFilter(price.restMoney, true)}</View>
            </View>
          )}
        </View>
        {/*  选择优惠对话框  */}
        <BottomDialog ref={this.chooseCard} id="choose-card" customClass="bottom-dialog-custom-class">
          {isCoupon === 4 && (
            <View className="coupon-select">
              {coupons.userGiftCardDTOSet &&
                coupons.userGiftCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name limit-line">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用礼品卡
              </View>
            </View>
          )}

          {/*  优惠券优惠类型  */}
          {isCoupon === 1 && (
            <View className="coupon-select">
              {coupons.couponSet &&
                coupons.couponSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab couponSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-img-box">
                        {!item.status ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gray.png"
                          ></Image>
                        ) : item.couponCategory === 1 ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-blue.png"
                          ></Image>
                        ) : (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gren.png"
                          ></Image>
                        )}
                      </View>
                      <View className="coupon-tab-left">
                        {item.discountFee === 0 ? (
                          <View className="coupon-freeFreight">免运费</View>
                        ) : (
                          <View className="coupon-price">
                            {(item.couponCategory === couponEnum.TYPE.freight.value ||
                              item.couponCategory === couponEnum.TYPE.fullReduced.value) && (
                              <Block>
                                <Text className="price-logo">￥</Text>
                                <Text className="price-num">{filters.moneyFilter(item.discountFee, true)}</Text>
                              </Block>
                            )}

                            {item.couponCategory === couponEnum.TYPE.discount.value && (
                              <Block>
                                <Text className="price-num">{filters.discountFilter(item.discountFee, true)}</Text>
                                <Text className="price-logo">折</Text>
                              </Block>
                            )}
                          </View>
                        )}

                        {item.minimumFee === 0 ? (
                          <View className="coupon-minimumFee">无门槛</View>
                        ) : (
                          <View className="coupon-minimumFee">
                            {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                          </View>
                        )}

                        {item.couponCategory === 1 && <View className="coupon-type">运费券</View>}

                        {item.couponCategory === 2 && <View className="coupon-type">折扣券</View>}

                        {item.couponCategory === 0 && <View className="coupon-type">满减券</View>}
                      </View>
                      <View className="coupon-tab-right">
                        <View className="coupon-name limit-line">{item.name}</View>
                        {item.endTime && (
                          <View className="coupon-validityDate">{'有效期：' + filters.dateFormat(item.endTime)}</View>
                        )}
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}

          {/*  红包优惠类型  */}
          {isCoupon === 2 && (
            <View className="coupon-select">
              {coupons.redPacketSet &&
                coupons.redPacketSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab redPacketSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        {item.thresholdFee === 0 ? (
                          <View className="coupon-name limit-line">无门槛</View>
                        ) : (
                          <View className="coupon-name limit-line">
                            {'满' + filters.moneyFilter(item.thresholdFee, true) + '可用'}
                          </View>
                        )}

                        {item.endTime && (
                          <View className="coupon-validityDate">
                            {'有效期：' + filters.dateFormat(item.endTime, 'yyyy-MM-dd hh:mm:ss')}
                          </View>
                        )}
                      </View>
                      <View className="coupon-tab-bottom">
                        金额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.luckMoneyFee, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用红包
              </View>
            </View>
          )}

          {/*  卡项优惠类型  */}
          {isCoupon === 3 && (
            <View className="coupon-select">
              {coupons.userCardDTOSet &&
                coupons.userCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}
        </BottomDialog>
        {/*  支付按钮  */}
        <View className={'pay-box ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {totalPrice ? (
            <View className="total-price">
              <Text>合计：</Text>
              <Text className="price-logo">¥</Text>
              <Text className="price-num">{filters.moneyFilter(price.needPayPrice, true)}</Text>
              {optionServiceDetail.deposit ? <Text className="deposit">（含押金）</Text> : ''}
            </View>
          ) : ''}

          <Button
            className="to-pay-btn"
            style={_safe_style_('background:' + (isError ? '#cccccc' : tmpStyle.btnColor))}
            onClick={this.handlePay}
            disabled={payLoading}
          >
            {totalPrice ? '确定支付' : '确定提交'}
          </Button>
        </View>
      </View>
    );
  }
}

export default GoodsRentalPendingpay;
