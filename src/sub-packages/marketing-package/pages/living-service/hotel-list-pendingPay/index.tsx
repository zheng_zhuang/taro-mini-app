import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import {
  Block,
  View,
  Text,
  Image,
  Input,
  Form,
  RadioGroup,
  Label,
  Radio,
  CheckboxGroup,
  Checkbox,
  Picker,
  Textarea,
  Button,
  ScrollView,
} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import utils from '../../../../../wxat-common/utils/util.js';
import template from '../../../../../wxat-common/utils/template.js';
import priceHandler from '../../../utils/priceHandler.js';
import constants from '../../../../../wxat-common/constants/index.js';
import couponEnum from '../../../../../wxat-common/constants/couponEnum.js';
import BottomDialog from '../../../../../wxat-common/components/bottom-dialog/index';
import deliveryWay from '@/wxat-common/constants/delivery-way';
import pay from '@/wxat-common/utils/pay'
import './index.scss';
const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};

class HotelListPendingPay extends React.Component {

  state = {
    isGiftActivity: false,
    itemNo: '',
    inTime: '',
    outTime: '',
    address: '',
    showinTime: '',
    showoutTime: '',
    DaysBetween: '',
    serviceName: '',
    salePrice: '',
    isshowTc: false,
    payAble: false,
    totalPrice: '',
    getDayAll: [],
    num: 1,
    totaldeposit: '', //统计押金
    totalsalePrice: '', //统计单价
    tmpStyle: {}, // 主题模板配置
    name: null, //表单名称
    buttonName: '提交表单', //按钮名称
    hideAvatar: false, //是否隐藏头图
    avatar: null, //头图url
    avatarHeight: 300, //头图高度
    isCoupon: 1,
    formBody: [], //表单内容
    couponEnum,
    curCoupon: {},
    price: {},
    coupons: {},
    curGiftCard: {},
    serveCard: {},
    curRedPacke: {},
    deposit: {},
    data: {},
    item: {},
    chooseCardNode: React.createRef()
  }

  /**
  * 生命周期函数--监听页面加载
  */
  onLoad(options) {
    if (process.env.TARO_ENV === 'weapp') {
      Taro.hideShareMenu({
        menus: ['shareAppMessage', 'shareTimeline'],
      })
    }
    let itemNo = options.itemNo;
    let inTime = options.inTime;
    let outTime = options.outTime;
    let showinTime = this.MonthDayformat(inTime);
    let showoutTime = this.MonthDayformat(outTime);
    let serviceName = options.serviceName;
    let DaysBetween = options.DaysBetween;
    let salePrice = options.salePrice;
    let deposit = options.deposit;
    let totalsalePrice = Number(salePrice) * Number(DaysBetween);
    let totalPrice = Number(salePrice) * Number(DaysBetween) + Number(deposit);
    let getDayAll = this.getDayAll(inTime, outTime);
    let storeId = options.storeId;
    this.setState(
      {
        itemNo,
        serviceName,
        salePrice,
        deposit,
        inTime,
        outTime,
        showinTime,
        showoutTime,
        DaysBetween,
        totalPrice,
        getDayAll,
        totalsalePrice,
        storeId,
      },
      () => {
        this.handleGoodsInfoChange()
      }
    )

    let formExt = JSON.parse(options.formExt)
    const formBody = formExt ? formExt : [];
    let newFormBody: Array<object> = [];
    formBody.forEach((item) => {
      if (!item.value) {
        item['value'] = '';
      }
      newFormBody.push(item);
      if (item.type === 'address') {
        newFormBody.push({
          label: '详细地址',
          type: 'detail-address',
          require: false,
          value: '',
        });
      }
    });
    this.setState({
      formBody: newFormBody,
    });

    this.getTemplateStyle();
  }

  handleGoToCouponList = () => {
    const couponSet = this.state.coupons.couponSet;
    if (!couponSet || couponSet.length < 1) {
      return this.errorTip('无可用优惠券');
    }

    this.setState({
      isCoupon: 1,
    });

    this.state.chooseCardNode.current.showModal();
  }

  onSelectCard = (e) => {
    let coupon = e.currentTarget.dataset.coupon;
    // 优惠券或红包状态是可用的或者优惠类型为礼品卡或次数卡（次数后端返回都是可用的）
    if (
      (coupon.status && coupon.status === 1) ||
      coupon.cardType === constants.card.type.gift ||
      coupon.cardType === constants.card.type.coupon
    ) {
      switch (this.state.isCoupon) {
        case 1:
          const data = this.state.data;
          data['orderRequestPromDTO.couponNo'] = coupon.code;
          this.setState({
            curCoupon: coupon,
            data: data,
          });

          break;
        case 2:
          this.setState({
            curRedPacke: coupon,
          });

          break;
        case 3:
          this.setState({
            serveCard: coupon,
          });

          break;
        case 4:
          this.setState({
            curGiftCard: coupon,
          });

          break;
      }

      this.state.chooseCardNode.current.hideModal();
      this.handlePreferentialChange();
    } else {
      Taro.showToast({
        title: coupon.rejReason || '该优惠不能使用。',
        icon: 'none',
      });
    }
  }

  /**
  * 优惠信息变化时的handler，
  * 如：优惠券选择，礼品卡选择、红包选择等
  */
    handlePreferentialChange = () => {
    this.setState({
      payAble: false,
    });

    Taro.showLoading({
      title: '请稍等',
    });

    priceHandler
      .getPrice(this.state.data)
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
          payAble: true,
        });
      })
      .catch((error) => {
        if (!error.quite) {
          //未选择收货地址时报错
          Taro.showToast({
            title: error.msg,
            icon: 'none',
          });
        }
      })
      .finally(() => {
        Taro.hideLoading();
      });
  }

  //计算优惠劵的价格
  handleGoodsInfoChange = () => {
    /*const denyPay = checkStockAndShefHandler.checkGoodsDenyPay(this.data);
    if (denyPay.deny) {
      wx.showToast({
        icon: "none",
        title: denyPay.reason,
      });
      return;
    }
    this.setData({
      payAble: false,
    });
    wx.showLoading({
      title: "请稍等",
    });*/

    Taro.showLoading(
      {
        title: '正在加载',
        mask: true
      }
    )

    let data = {
      expressType: 0,
      itemDTOList: [
        {
          itemCount: this.state.num,
          itemNo: this.state.itemNo,
        },
      ],

      orderServerPlatformDTO: {
        platformDay: this.state.DaysBetween,
      },

      // storeId: state.base.currentStore.id,
      storeId: this.state.storeId,
      // 'orderRequestPromDTO.couponNo' : ''
    };

    data.itemDTOList[0].itemType = 47;
    data = utils.formatParams(data);
    this.setState({
      data,
    });
    priceHandler
      .fetchPreferentialList(data)
      .then((res) => {
        if (res) {
          const { curRedPacke, curCoupon, coupons } = res;
          if (this.state.salePrice == 0) {
            this.setState({
              curRedPacke,
              coupons,
              curCoupon: {},
            });
          } else {
            this.setState({
              curRedPacke,
              curCoupon,
              coupons,
            });

            if (curCoupon.code) {
              data['orderRequestPromDTO.couponNo'] = curCoupon.code;
            }
          }
        }
        //获取订单价格
        return priceHandler.getPrice(data);
      })
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
          payAble: true,
        });
      })
      .catch((error) => {
        if (!error.quite) {
          //未选择收货地址时报错
        }
      })
      .finally(() => {
        Taro.hideLoading();
      });
  }

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //明细
  tcbtn = () => {
    let isshowTc = !this.state.isshowTc;
    this.setState({
      isshowTc,
    });
  }

  //关闭
  closeTc = () => {
    this.setState({
      isshowTc: false,
    })
  }

  //统计金额
  totalPrice = () => {
    let totalPrice = Number(this.state.totalsalePrice) * this.state.num + Number(this.state.deposit) * this.state.num;
    this.setState({
      totalPrice,
    });
  }

  //押金金额
  depositPrice() {
    let totaldeposit = Number(this.state.deposit) * this.state.num;
    this.setState({
      totaldeposit,
    });
  }

  //格式化日期，只显示月份和日
  MonthDayformat = (date) => {
    let Dtime = new Date(date);
    var Dtimemonth = Dtime.getMonth() + 1;

    // Dtimemonth < 10 ? '0' + Dtimemonth : Dtimemonth
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}月${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }日`;
  }

  //错误提示方法
  errorTip = (msg) => {
    Taro.showToast({
      icon: 'none',
      title: msg,
    });
  }

  //获取时间段
  getDayAll = (starDay, endDay) => {
    var arr = [];
    var dates = [];
    var db = new Date(starDay);
    var de = new Date(endDay);
    var s = db.getTime() - 24 * 60 * 60 * 1000;
    var d = de.getTime() - 24 * 60 * 60 * 1000 * 2;
    for (var i = s; i <= d; ) {
      i = i + 24 * 60 * 60 * 1000;
      arr.push(parseInt(i));
    }
    for (var j in arr) {
      var time = new Date(arr[j]);
      var year = time.getFullYear(time);
      var mouth = time.getMonth() + 1 >= 10 ? time.getMonth() + 1 : '0' + (time.getMonth() + 1);
      var day = time.getDate() >= 10 ? time.getDate() : '0' + time.getDate();
      var YYMMDD = year + '-' + mouth + '-' + day;
      dates.push(YYMMDD);
    }
    return dates;
  }

  // 不使用优惠
  onNoUseCard = () => {
    switch (this.state.isCoupon) {
      case 1:
        const data = this.state.data;
        delete data['orderRequestPromDTO.couponNo'];
        this.setState({
          curCoupon: {},
          data: data,
        });

        break;
      case 2:
        this.setState({
          curRedPacke: {},
        });

        break;
      case 3:
        this.setState({
          serveCard: {},
        });

        break;
      case 4:
        this.setState({
          curGiftCard: {},
        });

        break;
    }

    this.state.chooseCardNode.current.hideModal();
    this.handlePreferentialChange();
  }

  /* 点击减号 */
  bindMinus = () => {
    var num = this.state.num;
    // 如果大于1时，才可以减
    if (num > 1) {
      num--;
    }
    // 将数值与状态写回
    const data = this.state.data;
    data['itemDTOList[0].itemCount'] = num;
    this.setState({
      num: num,
      data: data,
    });

    this.totalPrice();
    this.depositPrice();
    this.handlePreferentialChange();
  }

  /* 点击加号 */
  bindPlus = () => {
    var num = this.state.num;
    // 不作过多考虑自增1
    num++;
    // 将数值与状态写回
    const data = this.state.data;
    data['itemDTOList[0].itemCount'] = num;
    this.setState({
      num: num,
      data: data,
    });

    this.totalPrice();
    this.depositPrice();
    this.handlePreferentialChange();
  }

  inputValue = (e) => {
    const index = e.currentTarget.dataset.index;
    const value = e.detail.value;
    const formBody = this.state.formBody;
    formBody[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({ formBody });
  }

  chooseGender = ({
    currentTarget: {
      dataset: { value, index },
    },
  }) => {
    const formBody = this.state.formBody;
    formBody[index].value = value;
    this.setState({
      formBody: formBody,
    });
  }

  checkForm = (dataList) => {
    let error = '';
    dataList.forEach((item) => {
      if (error) return;
      if (item.required && !item.value) {
        error = item.label + '不能为空';
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });
    return error;
  }

  /**
  * 支付
  * @param {*} e
  */
  handlePay = () => {
    
    let { formBody, payAble } = this.state;
    const error = this.checkForm(formBody);
    if (error) {
      Taro.showToast({ icon: 'none', title: error });
      return;
    }
    if (!payAble) {
      Taro.showToast({ icon: 'none', title: '价格计算失败' });
      return;
    }
    //地址栏数据处理
    const list = [];
    formBody.forEach((item, index) => {
      let value = item.value || '';
      if (item.type === 'address') {
        const isNotEmptyArray = !!value && !!value.push;
        if (isNotEmptyArray) {
          if (value[0] === value[1]) {
            value = value.concat().splice(1).join('');
          } else {
            value = value.join('');
          }
        }
        value += formBody[index + 1].value || '';
      } else if (item.type === 'detail-address') {
        return;
      }
      list.push({
        label: item.label,
        uuid: item.uuid,
        value,
      });
    });
    Taro.showLoading(
      {
        title: '正在加载',
        mask: true
      }
    )
    let timeSlot = `${this.state.inTime},${this.state.outTime}`;

    wxApi
      .request({
        url: api.livingService.unifiedOrder,
        method: 'POST',
        data: {
          itemNo: this.state.itemNo,
          itemCount: this.state.num,
          deposit: this.state.deposit,
          platformDay: this.state.DaysBetween,
          formExt: JSON.stringify(list),
          timeSlot: timeSlot,
          orderRequestPromDTO: {
            couponNo: this.state.curCoupon.code,
          },
          itemType: 47,
          payTradeType: pay.returnPayTradeType()
        },
      })
      .then((res) => {
        const { orderNo } = res.data
        pay.payByOrder(
          orderNo,
          null, 
          {
            wxPaySuccess () {
              wxApi.$navigateTo({
                url: '/sub-packages/marketing-package/pages/living-service/order-list/index',
                data: {
                  orderNo,
                },
              })
            },
            wxPayFail () {
              wxApi.$navigateTo({
                url: '/sub-packages/marketing-package/pages/living-service/order-list/index',
                data: {
                  orderNo,
                },
              })
            }
          }
         )
      }).finally(() => {
        Taro.hideLoading();
      })
  }

  render() {
    const {
      serviceName,
      showinTime,
      showoutTime,
      DaysBetween,
      curCoupon,
      couponEnum,
      price,
      coupons,
      num,
      formBody,
      tmpStyle,
      curGiftCard,
      serveCard,
      curRedPacke,
      isGiftActivity,
      getDayAll,
      salePrice,
      deposit,
      isshowTc,
      isCoupon,
      item,
    } = this.state
    const { 
      payDetails,
      seckillNo, 
      useLogisticsCoupon, 
      choosedDelivery, 
      scoreName,
      goodsIntegral,
      goodsInternal,
      isCardpackType,
      goodsInfoList,
      goodsTotalPrice,
      isEstateType,
      allFreightCollect,
      isFrontMoneyItem,
      isError,
    } = this.props
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mplh-HotelListPendingPay"
        className="wk-mplh-HotelListPendingPay"
      >
        <View className="hotel-list-pendingPay">
          <View className="hotel-list-box">
            <View className="hotel-list-head">
              <View className="hotel-name">{serviceName}</View>
              <View className="hotel-time">
                <Text className="hotel-time-item">{'入住：' + showinTime}</Text>
                <Text className="hotel-time-item">{'退房：' + showoutTime}</Text>
                <Text className="countDate">{'共' + DaysBetween + '晚'}</Text>
              </View>
            </View>
            <View className="row-box" onClick={this.handleGoToCouponList}>
              <View className="row-label">优惠劵</View>
              {curCoupon && curCoupon.name ? (
                <View className="right-text">
                  {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                    <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                  )}

                  {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                </View>
              ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
              ) : (
                <View className="right-text">无可用</View>
              )}

              <Image
                className="right-icon"
                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
              ></Image>
            </View>
            <View className="hotel-list-content">
              <View className="hotel-content-item">
                <View className="hotel-lable">房间数</View>
                <View className="hotle-count">
                  <Text className="hotleBtn" onClick={this.bindMinus}>
                    -
                  </Text>
                  <Text>
                    <Input className="input-cpt" type="number" value={num} disabled></Input>
                  </Text>
                  <Text className="hotleBtn" onClick={this.bindPlus}>
                    +
                  </Text>
                </View>
              </View>
            </View>
            <Form>
              <View className="form-body">
                <View className="white-box">
                  {formBody &&
                    formBody.map((item, index) => {
                      return (
                        <View className={'form-item ' + item.type + '-item'} key={item.uuid}>
                          {item.type !== 'comment' && (
                            <View className="form-item-name">
                              <Text>{item.label}</Text>
                              {item.required && (
                                <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>
                              )}
                            </View>
                          )}

                          {/*  性别  */}
                          {item.type === 'sex' && (
                            <View className="form-item-content form-sex">
                              <View
                                onClick={_fixme_with_dataset_(this.chooseGender, { value: '男', index: index })}
                                className={item.value === '男' ? 'selected' : ''}
                              >
                                <Image
                                  mode="aspectFit"
                                  src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/male.png"
                                ></Image>
                                <View>男</View>
                              </View>
                              <View
                                onClick={_fixme_with_dataset_(this.chooseGender, { value: '女', index: index })}
                                className={item.value === '女' ? 'selected' : ''}
                              >
                                <Image
                                  mode="aspectFit"
                                  src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/female.png"
                                ></Image>
                                <View>女</View>
                              </View>
                            </View>
                          )}

                          {/*  单选框  */}
                          {item.type === 'radio' && (
                            <RadioGroup
                              className="form-item-content form-radio"
                              name="radio-group"
                              onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                            >
                              {item.options &&
                                item.options.map((optionItem, index) => {
                                  return (
                                    <Label key={optionItem.value}>
                                      <Radio value={optionItem} color={tmpStyle.btnColor}></Radio>
                                      {optionItem}
                                    </Label>
                                  );
                                })}
                            </RadioGroup>
                          )}

                          {/*  多选框  */}
                          {item.type === 'checkbox' && (
                            <CheckboxGroup
                              className="form-item-content form-radio"
                              name="radio-group"
                              onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                            >
                              {item.options &&
                                item.options.map((optionItem, index) => {
                                  return (
                                    <Label key={optionItem.value}>
                                      <Checkbox value={optionItem} color={tmpStyle.btnColor}></Checkbox>
                                      {optionItem}
                                    </Label>
                                  );
                                })}
                            </CheckboxGroup>
                          )}

                          {/*  时间选择器  */}
                          {item.type === 'date' && (
                            <Picker
                              className="form-item-content form-date"
                              mode="date"
                              start={item.dateRange[0]}
                              end={item.dateRange[1]}
                              onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                            >
                              {item.value ? (
                                <Text className="picker">{item.value}</Text>
                              ) : (
                                <Text style={_safe_style_('color: #888;')}>请选择</Text>
                              )}

                              <Image
                                className="picker-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                              ></Image>
                            </Picker>
                          )}

                          {/*  文本类型单行输入框  */}
                          {(item.type === 'text' || item.type === 'email') && (
                            <Input
                              className="form-item-content form-input"
                              placeholder="请输入"
                              value={item.value}
                              onBlur={_fixme_with_dataset_(this.inputValue, { index: index })}
                            ></Input>
                          )}

                          {/*  数字类型单行输入框  */}
                          {item.type === 'phone' && (
                            <Input
                              className="form-item-content form-input"
                              placeholder="请输入"
                              type="number"
                              onBlur={_fixme_with_dataset_(this.inputValue, { index: index })}
                            ></Input>
                          )}

                          {/*  多行输入框  */}
                          {item.type === 'textarea' && (
                            <View className="form-textarea">
                              <Textarea
                                className="form-item-content"
                                maxlength="-1"
                                placeholder="请输入留言内容"
                                onInput={_fixme_with_dataset_(this.inputValue, { index: index })}
                              ></Textarea>
                              {/*  <view class="num-tip">5/50</view>  */}
                            </View>
                          )}

                          {/*  地址  */}
                          {item.type === 'address' && (
                            <Picker
                              className="form-item-content form-address"
                              mode="region"
                              onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                            >
                              {item.value ? (
                                <Text className="picker">{item.value}</Text>
                              ) : (
                                <Text style={_safe_style_('color: #888;')}>请选择</Text>
                              )}

                              <Image
                                className="picker-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                              ></Image>
                            </Picker>
                          )}

                          {/* 详细地址  */}
                          {item.type === 'detail-address' && (
                            <Textarea
                              className="form-item-content form-detail-address"
                              disableDefaultPadding
                              autoHeight
                              placeholder="请输入详细地址"
                              onInput={_fixme_with_dataset_(this.inputValue, { index: index })}
                            ></Textarea>
                          )}

                          {item.type === 'comment' && (
                            <View className="comment">
                              <Text>{item.value}</Text>
                            </View>
                          )}
                        </View>
                      );
                    })}
                </View>
              </View>
            </Form>
            {/*  优惠信息  */}
            <View className="shipping-method">
              {price.fullDecrementPromFee && (
                <View className="row-box">
                  <View className="row-label">满减</View>
                  <View className="right-text">{'减￥' + filters.moneyFilter(price.fullDecrementPromFee, true)}</View>
                </View>
              )}

              {payDetails &&
                payDetails.map((item, index) => {
                  return (
                    <Block key={item.index}>
                      {item.isCustom ? (
                        <View className="row-box">
                          <View className="row-label">{item.label}</View>
                          <View className="right-text">{item.value}</View>
                        </View>
                      ) : (
                        <Block>
                          {item.name === 'gift-card' && (
                            <View className="row-box" onClick={this.handleGoToGiftCardList}>
                              <View className="row-label">礼品卡</View>
                              {curGiftCard && curGiftCard.cardItemName ? (
                                <View className="right-text">
                                  {'抵扣￥' + filters.moneyFilter(price.giftCardPrice, true)}
                                </View>
                              ) : coupons && coupons.userGiftCardDTOSet && coupons.userGiftCardDTOSet.length ? (
                                <View className="right-text">{'可用' + coupons.userGiftCardDTOSet.length + '张'}</View>
                              ) : (
                                <View className="right-text">无可用</View>
                              )}

                              <Image
                                className="right-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                              ></Image>
                            </View>
                          )}

                          {item.name === 'coupon' && !seckillNo && (
                            <View className="row-box" onClick={this.handleGoToCouponList}>
                              <View className="row-label">优惠劵</View>
                              {curCoupon && curCoupon.name ? (
                                <View className="right-text">
                                  {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                    <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                  )}

                                  {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                                </View>
                              ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                                <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                              ) : (
                                <View className="right-text">无可用</View>
                              )}

                              <Image
                                className="right-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                              ></Image>
                            </View>
                          )}

                          {item.name === 'coupon' &&
                            useLogisticsCoupon == 1 &&
                            seckillNo &&
                            choosedDelivery !== deliveryWay.SELF_DELIVERY.value && (
                              <View className="row-box" onClick={this.handleGoToCouponList}>
                                <View className="row-label">运费券</View>
                                {curCoupon && curCoupon.name ? (
                                  <View className="right-text">
                                    {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                      <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                    )}

                                    {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                                  </View>
                                ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                                  <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                                ) : (
                                  <View className="right-text">无可用</View>
                                )}

                                <Image
                                  className="right-icon"
                                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                                ></Image>
                              </View>
                            )}

                          {item.name === 'card' && (
                            <View className="row-box" onClick={this.handleGoToCard}>
                              <View className="row-label">次数卡</View>
                              <View className="right-text">{serveCard.cardItemName}</View>
                              <Image
                                className="right-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                              ></Image>
                            </View>
                          )}

                          {item.name === 'redpacke' && (
                            <View className="row-box" onClick={this.handleRedPackeList}>
                              <View className="row-label">红包</View>
                              {curRedPacke && curRedPacke.code ? (
                                <View className="right-text">
                                  {'减￥' + filters.moneyFilter(price.redPacketPrice, true)}
                                </View>
                              ) : coupons && coupons.redPacketSet && coupons.redPacketSet.length ? (
                                <View className="right-text">{'可用' + coupons.redPacketSet.length + '个'}</View>
                              ) : (
                                <View className="right-text">无可用</View>
                              )}

                              <Image
                                className="right-icon"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                              ></Image>
                            </View>
                          )}

                          {item.name === 'discount' && (
                            <View className="row-box">
                              <View className="row-label">会员折扣</View>
                              <View className="right-text">
                                {(coupons.memberDiscount && coupons.memberDiscount < 100
                                  ? coupons.memberDiscount / 10 + '折'
                                  : '无') +
                                  '\r\n              减￥' +
                                  filters.moneyFilter(price.memberPrice, true)}
                              </View>
                            </View>
                          )}

                          {item.name === 'integral' && (
                            <View className="row-box">
                              <View className="row-label">{scoreName + '抵扣'}</View>
                              <View className="right-text">
                                {goodsIntegral + scoreName + '减￥' + filters.moneyFilter(price.integralPromFee, true)}
                              </View>
                            </View>
                          )}

                          {item.name === 'innerBuy' && (
                            <View className="row-box">
                              <View className="row-label">内购券</View>
                              <View className="right-text">
                                {goodsInternal + '张减￥' + filters.moneyFilter(price.innerBuyPromFee, true)}
                              </View>
                            </View>
                          )}

                          {item.name === 'seckill' && (
                            <View className="row-box">
                              <View className="row-label">秒杀优惠</View>
                              <View className="right-text">
                                {'减￥' + filters.moneyFilter(price.secKillPromFee, true)}
                              </View>
                            </View>
                          )}
                        </Block>
                      )}
                    </Block>
                  );
                })}
              {price.groupPromFee && (
                <View className="row-box">
                  <View className="row-label">拼团优惠</View>
                  <View className="right-text">{'-¥ ' + filters.moneyFilter(price.groupPromFee, true)}</View>
                </View>
              )}

              {!!isCardpackType && (
                <Block>
                  <View className="row-box">
                    <View className="row-label">卡包明细</View>
                  </View>
                  {goodsInfoList[0].wxItem.couponInfos &&
                    goodsInfoList[0].wxItem.couponInfos.map((item, index) => {
                      return (
                        <Block key={item.index}>
                          <View className="row-box card-pack-box">
                            <View className="label">{item.name}</View>
                            <View className="right-text">{(item.couponNum || 0) + '张'}</View>
                          </View>
                        </Block>
                      );
                    })}
                </Block>
              )}

              <Block>
                {isEstateType ? (
                  <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                    <View className="row-label">认筹金</View>
                    <View className="right-text">
                      {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                    </View>
                  </View>
                ) : (
                  <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                    <View className="row-label row-label-deposit">商品原价(含押金)</View>
                    {/*  <view wx:if="{{goodsInfoList[0].seckillPrice > (price.totalPrice||goodsTotalPrice)}}" class="right-text">¥ {{filters.moneyFilter(goodsInfoList[0].seckillPrice,true)}}</view>  */}
                    <View className="right-text">
                      {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                    </View>
                  </View>
                )}
              </Block>
              {item.name === 'freight' && choosedDelivery === deliveryWay.EXPRESS.value && (
                <Block>
                  {payDetails &&
                    payDetails.map((item, index) => {
                      return (
                        <View className="row-box" key={item.index}>
                          <View className="row-label">运费</View>
                          {price.freight && price.freight > 0 ? (
                            <View className="right-text">{'￥' + filters.moneyFilter(price.freight, true)}</View>
                          ) : (
                            <View className="right-text">{allFreightCollect ? '到付' : '包邮'}</View>
                          )}
                        </View>
                      );
                    })}
                </Block>
              )}

              {choosedDelivery === deliveryWay.CITY_DELIVERY.value && (
                <View className="row-box">
                  <View className="row-label">配送费</View>
                  {price.freight && price.freight > 0 ? (
                    <View className="right-text">{'￥' + filters.moneyFilter(price.freight, true)}</View>
                  ) : (
                    <View className="right-text">免配送费</View>
                  )}
                </View>
              )}

              <View className="row-box">
                <View className="row-label">共优惠</View>
                <View className="right-text">
                  {'- ¥ ' +
                    (isGiftActivity
                      ? filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)
                      : filters.moneyFilter(price.totalFreePrice || 0, true) < 0
                      ? 0
                      : filters.moneyFilter(price.totalFreePrice || 0, true))}
                </View>
              </View>
              {price && isFrontMoneyItem && (
                <View className="row-box">
                  <View className="row-label">尾款</View>
                  <View className="right-text">{'¥ ' + filters.moneyFilter(price.restMoney, true)}</View>
                </View>
              )}
            </View>
            {/*  <view class="hotel-list-content">
                      <view class="hotel-content-item">
                          <view class="hotel-lable">
                              房间数
                          </view>
                          <view class="hotle-count">
                              <Text class="hotleBtn" >-</Text>
                              <Text><input class="input-cpt" type="number" value="{{num}}" disabled/></Text>
                              <Text class="hotleBtn">+</Text>
                          </view>
                      </view>
                      <view class="hotel-content-item">
                          <view class="hotel-lable">
                              入住人
                          </view>
                          <view class="hotle-input">
                              <input placeholder-class="input-placeholder" placeholder="请填写实际入住人姓名" />
                          </view>
                      </view>
                      <view class="hotel-content-item">
                          <view class="hotel-lable">
                              手机号
                          </view>
                          <view class="hotle-input">
                              <input placeholder-class="input-placeholder" placeholder="用于接收预订信息" />
                          </view>
                      </view>
                  </view>  */}
          </View>
        </View>
        {/*  支付按钮  */}
        <View className="pay-box">
          <View className="total-price">
            <Text>合计：</Text>
            <Text className="price-logo">¥</Text>
            <Text className="price-num">{filters.moneyFilter(price.needPayPrice, true)}</Text>
            <Text className="deposit" onClick={this.tcbtn}>
              <Text>明细</Text>
              <Image
                mode="aspectFill"
                className="down-angle1"
                src="https://front-end-1302979015.file.myqcloud.com/images/c/images/down-angle.png"
              ></Image>
            </Text>
          </View>
          <Button
            className="to-pay-btn"
            style={_safe_style_('background:' + (isError ? '#cccccc' : tmpStyle.btnColor))}
            onClick={this.handlePay}
          >
            确定支付
          </Button>
        </View>
        {/* ----------底部弹窗----------- */}
        {isshowTc && (
          <View className="tc-box">
            <View className="tc-hui" onClick={this.closeTc}></View>
            <View className="tc-content">
              <View className="tc-title">
                <View className="tc-titleName">{serviceName}</View>
                <View className="tc-titletext">{DaysBetween + '晚 ' + num + ' 间'}</View>
              </View>
              <View className="scroll-content">
                <View className="scroll-item-head">
                  <Text className="scroll-title">合计</Text>
                  <Text className="scroll-price right">{'¥' + filters.moneyFilter(price.needPayPrice, true)}</Text>
                </View>
                <ScrollView className="scroll-item-list" scrollY="true">
                  <View className="scrollList-item">
                    <Text>优惠</Text>
                    <Text className="right">{'¥' + filters.moneyFilter(price.totalFreePrice, true)}</Text>
                  </View>
                  {getDayAll &&
                    getDayAll.map((item, index) => {
                      return (
                        <View className="scrollList-item" key={'scrollList-item-' + index}>
                          <Text>{item}</Text>
                          <Text className="right">{'¥' + filters.moneyFilter(salePrice, true) + ' × ' + num}</Text>
                        </View>
                      );
                    })}
                  <View className="scrollList-item">
                    <Text>押金</Text>
                    <Text className="right">{'¥' + filters.moneyFilter(deposit, true) + ' × ' + num}</Text>
                  </View>
                </ScrollView>
              </View>
            </View>
          </View>
        )}

        {/*  选择优惠对话框  */}
        <BottomDialog id="choose-card" ref={this.state.chooseCardNode} customClass="bottom-dialog-custom-class">
          {isCoupon === 4 && (
            <View className="coupon-select">
              {coupons.userGiftCardDTOSet &&
                coupons.userGiftCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name limit-line">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (!!item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用礼品卡
              </View>
            </View>
          )}

          {/*  优惠券优惠类型  */}
          {isCoupon === 1 && (
            <View className="coupon-select">
              {coupons.couponSet &&
                coupons.couponSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab couponSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-img-box">
                        {!item.status ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gray.png"
                          ></Image>
                        ) : item.couponCategory === 1 ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-blue.png"
                          ></Image>
                        ) : (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gren.png"
                          ></Image>
                        )}
                      </View>
                      <View className="coupon-tab-left">
                        {item.discountFee === 0 ? (
                          <View className="coupon-freeFreight">免运费</View>
                        ) : (
                          <View className="coupon-price">
                            {(item.couponCategory === couponEnum.TYPE.freight.value ||
                              item.couponCategory === couponEnum.TYPE.fullReduced.value) && (
                              <Block>
                                <Text className="price-logo">￥</Text>
                                <Text className="price-num">{filters.moneyFilter(item.discountFee, true)}</Text>
                              </Block>
                            )}

                            {item.couponCategory === couponEnum.TYPE.discount.value && (
                              <Block>
                                <Text className="price-num">{filters.discountFilter(item.discountFee, true)}</Text>
                                <Text className="price-logo">折</Text>
                              </Block>
                            )}
                          </View>
                        )}

                        {item.minimumFee === 0 ? (
                          <View className="coupon-minimumFee">无门槛</View>
                        ) : (
                          <View className="coupon-minimumFee">
                            {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                          </View>
                        )}

                        {item.couponCategory === 1 && <View className="coupon-type">运费券</View>}

                        {item.couponCategory === 2 && <View className="coupon-type">折扣券</View>}

                        {item.couponCategory === 0 && <View className="coupon-type">满减券</View>}
                      </View>
                      <View className="coupon-tab-right">
                        <View className="coupon-name limit-line">{item.name}</View>
                        {item.endTime && (
                          <View className="coupon-validityDate">{'有效期：' + filters.dateFormat(item.endTime)}</View>
                        )}
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}

          {/*  红包优惠类型  */}
          {isCoupon === 2 && (
            <View className="coupon-select">
              {coupons.redPacketSet &&
                coupons.redPacketSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab redPacketSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        {item.thresholdFee === 0 ? (
                          <View className="coupon-name limit-line">无门槛</View>
                        ) : (
                          <View className="coupon-name limit-line">
                            {'满' + filters.moneyFilter(item.thresholdFee, true) + '可用'}
                          </View>
                        )}

                        {item.endTime && (
                          <View className="coupon-validityDate">
                            {'有效期：' + filters.dateFormat(item.endTime, 'yyyy-MM-dd hh:mm:ss')}
                          </View>
                        )}
                      </View>
                      <View className="coupon-tab-bottom">
                        金额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.luckMoneyFee, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用红包
              </View>
            </View>
          )}

          {/*  卡项优惠类型  */}
          {isCoupon === 3 && (
            <View className="coupon-select">
              {coupons.userCardDTOSet &&
                coupons.userCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (!!item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}
        </BottomDialog>
      </View>
    );
  }
}

export default HotelListPendingPay;
