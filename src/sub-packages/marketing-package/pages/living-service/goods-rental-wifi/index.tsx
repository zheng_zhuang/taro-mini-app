import { _fixme_with_dataset_ } from 'wk-taro-platform'
import { View, Image } from '@tarojs/components'
import React from 'react'
import Taro from '@tarojs/taro'
import { connect } from 'react-redux'
import wxApi from '../../../../../wxat-common/utils/wxApi'
import api from '../../../../../wxat-common/api/index.js'
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog'
import './index.scss';

const mapStateToProps = (state) => ({
  globalData: state.globalData,
  currentStore: state.base.currentStore
})

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class GoodsRentalWifi extends React.Component {

  state = {
    wifiList: null,
    address: null
  }

  /**
  * 生命周期函数--监听页面加载
  */
  onLoad() {
    this.setState({
      address: this.props.currentStore.address,
    });
    this.selectByRoomCodeWifi();
  }

  //点击复制文本
  copyText = (e) => {
    Taro.setClipboardData({
      data: e.currentTarget.dataset.text,
      success: function () {
        Taro.getClipboardData({
          success: function () {
            Taro.showToast({
              icon: 'succees',
              title: '复制成功',
            });
          },
        });
      },
    });
  }

  selectByRoomCodeWifi() {
    wxApi
      .request({
        url: api.livingService.selectByRoomCodeWifi,
        method: 'POST',
        data: {
          hotelId: this.props.currentStore.id,
          roomCode: this.props.globalData.roomCode,
        },
      })
      .then((res) => {
        let wifiList = res.data || [];
        this.setState({
          wifiList,
        });
      })
      .catch((error) => {
        let message = error.data.errorMessage;
        Taro.showToast({
          title: message,
          icon: 'none', //如果要纯文本，不要icon，将值设为'none'
          duration: 4000,
        });
      });
  }

  render() {
    const { address, wifiList } = this.state
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mplg-GoodsRentalWifi"
        className="wk-mplg-GoodsRentalWifi"
      >
        <View className="goods-rental-details">
          <View className="goods-rental-details-item">
            {address && (
              <View className="goods-rental-information">
                <View className="information-img">
                  <Image
                    className="store-location-icon"
                    src="https://front-end-1302979015.file.myqcloud.com/images/location-green.svg"
                    mode="aspectFit"
                  ></Image>
                </View>
                <View className="information-text">
                  <View className="informationtext-title">{address}</View>
                </View>
              </View>
            )}
          </View>
          {wifiList && (
            <View className="goods-rental-details-item">
              <View className="information-itemlist">
                <View className="information-items-item">
                  <View className="itemsItem-left">wifi账号</View>
                  <View className="itemsItem-right">{wifiList.wifiName}</View>
                </View>
                <View className="information-items-item">
                  <View className="itemsItem-left">wifi密码</View>
                  <View className="itemsItem-right">{wifiList.wifiPwd}</View>
                  <View
                    className="itemsItem-button"
                    onClick={_fixme_with_dataset_(this.copyText, { text: wifiList.wifiPwd })}
                  >
                    复制
                  </View>
                </View>
              </View>
            </View>
          )}
        </View>
        <HomeActivityDialog showPage="sub-packages/marketing-package/pages/living-service/goods-rental-wifi/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default GoodsRentalWifi;
