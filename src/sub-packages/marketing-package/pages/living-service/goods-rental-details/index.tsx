import {_fixme_with_dataset_, _safe_style_, $getRouter} from 'wk-taro-platform';
import {
  Block,
  View,
  Swiper,
  SwiperItem,
  Text,
  Form,
  Image,
  RadioGroup,
  Label,
  Radio,
  CheckboxGroup,
  Checkbox,
  Picker,
  Input,
  Textarea,
} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import store from '@/store';
import filters from '@/wxat-common/utils/money.wxs.js';
import industryEnum from '@/wxat-common/constants/industryEnum.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import template from '@/wxat-common/utils/template.js';
import checkOptions from '@/wxat-common/utils/check-options.js';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import ContactHousekeeper from '@/wxat-common/components/contact-housekeeper/index';
import CustomerHeader from '@/wxat-common/components/customer-header/index';
import BuyNow from '@/wxat-common/components/buy-now/index';
import DetailParser from '@/wxat-common/components/detail-parser/index';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import {connect} from 'react-redux';
import getStaticImgUrl from '../../../../../wxat-common/constants/frontEndImgUrl'
import {updateGlobalDataAction} from '@/redux/global-data';
import RegionPicker from '@/sub-packages/scene-package/components/region-picker/index'
import UploadImg from "@/wxat-common/components/upload-img";
import utils from '@/wxat-common/utils/util.js';
import {getFreeLabel} from '@/wxat-common/utils/service'
// import Img from '../../img/close.png'

const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};

const storeData = store.getState();

interface PageStateProps {
  currentStore: any
}

type IProps = PageStateProps

interface GoodsRentalDetails {
  props: IProps;
}

const mapStateToProps = (state) => ({
  currentStore: state.base.currentStore
})

@connect(mapStateToProps, undefined, undefined, {forwardRef: true})
class GoodsRentalDetails extends React.Component {
  $router = $getRouter();

  state = {
    tmpStyle: {}, // 主题模板配置
    industryEnum, // 行业类型常量
    industry: storeData.globalData.industry, // 行业类型
    id: null, // 商品单号
    sourceType: null, // 服务类型
    goodsImages: [], // 商品图片
    serviceDetail: [], // 服务商品详情
    dataLoad: false,
    sessionId: null,
    error: false,
    isExpand: false,
    extraData: null,
    _optionsData: null,
    _storeId: null,
    formExt: [], // 表单提交
    disable: false, // 默认不禁止点击提交按钮
    num: 1,
    navOpacity: 0,
    navBgColor: null,
    realPath: '',
    isStock: true,  //是否有库存
    changeType: null, //门店设置类型,1:不配置 2:客服图片 3:客服对话,
    imgeUrl: null, //二维码还是链接
    dataTotal: null, //总数据
    postType: null, //等于2就弹框
  }

  onPageScroll(res) {
    this.setState({
      navOpacity: res.scrollTop / 50,
    });
  }

  stateChange = (data) => {
    // 是否已经登录，存在sessionId
    if (data && data.key === 'sessionId') {
      const formattedOptions = checkOptions.checkOnLoadOptions(this.state._optionsData);

      if (formattedOptions) {
        this.init(formattedOptions);
      }
    }
  };

  /**
   * 生命周期函数--监听页面加载
   */
  async componentDidMount() {

  }

  async componentDidShow() {
    const options = this.$router.params
    console.log(options, '---------options');
    console.log(storeData.base.sessionId, '---------------------sessionId');
    if (storeData.base.sessionId) {
      // localStorage.setItem('sessionId',storeData.base.sessionId);
      Taro.setStorageSync('sessionId', storeData.base.sessionId)
    } else {
      // const sessionIdData = localStorage.getItem('sessionId');
      const sessionIdData = Taro.getStorageSync('sessionId');
      console.log(sessionIdData, '------------------sessionIdData')
      if (sessionIdData) {
        this.setState({
          sessionId: sessionIdData,
        });
      }
    }
    this.setState({
      _optionsData: options,
      num: 1
    });
    Taro.removeStorageSync('serviceDetail'); // 移除单个服务详情缓存
    const formattedOptions = await checkOptions.checkOnLoadOptions(options);
    console.log(formattedOptions, '-------------formattedOptions')
    if (formattedOptions) {
      this.init(formattedOptions);
    } else {
      // wxApi.$navigateTo({
      //   url: '/wxat-common/pages/home/index',
      // });
      Taro.reLaunch({
        url: '/wxat-common/pages/home/index'
      });

    }
    if (storeData.globalData.tabbars && storeData.globalData.tabbars.list) {
      this.setState({
        navBgColor: storeData.globalData.tabbars.list[0].navBackgroundColor,
      });
    }
  }

  async init(options) {
    Taro.removeStorageSync('serviceDetail'); // 移除单个服务详情缓存

    const id = options.id;
    const sourceType = options.sourceType;
    const title = options.title ? options.title : '服务订单详情';
    const realpath = `sub-packages/marketing-package/pages/living-service/goods-rental-details/index?id=${id}&sourceType=${sourceType}`;
    Taro.setNavigationBarTitle({
      title,
    });

    this.setState({
      id,
      sourceType,
      realpath,
    });

    this.initRenderData(); // 初始化渲染的数据
  }

  initRenderData() {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({
      dataLoad: true,
    });

    this.getTemplateStyle(); // 获取主题模板配置
    this.getServiceDetail(); // 获取商品详情
    this.getType();//获取门店客服设置类型
  }

  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  getType() {
    wxApi
      .request({
        url: api.livingService.getType,
      })
      .then((res) => {
        console.log(res, '---------res');
        this.setState({
          changeType: res.data.customerType || null,
          imgeUrl: res.data.imgUrl || null,
          dataTotal: res.data || null,
        });
      });
  }

  getServiceDetail() {
    Taro.showLoading(
      {
        title: '加载中...',
        mask: true
      }
    )
    wxApi
      .request({
        url: api.livingService.getPlatformDetails,
        data: {
          id: this.state.id,
          sourceType: this.state.sourceType,
          storeId: this.props.currentStore.id,
        },
      })
      .then((res) => {
        const goodsImages: any[] = [];
        if (goodsImages) {
          goodsImages.push(...res.data.img.split(','));
        }
        const serviceDetail = res.data || [];

        //判断是否有库存
        let isStock = true
        if (serviceDetail.useStock == 1 && !serviceDetail.itemStock) {
          isStock = false
        }

        let formExt: any[] = [];

        if (serviceDetail.formExt) {
          formExt = JSON.parse(res.data.formExt).widgetValue;
        }
        formExt.forEach((item, index) => {
          // 地址栏多添加一行详细地址
          if (item.type === 'address') {
            formExt.splice(index + 1, 0, {
              label: '详细地址',
              type: 'detail-address',
              required: false,
              value: '',
            });
          }
          // 单选
          if (item.type === 'radio') {
            item.options = item.options.map((el) => {
              return {
                value: el,
                text: el,
                checked: false
              }
            })
          }
          // 多选
          if (item.type === 'checkbox') {
            item.options = item.options.map((el) => {
              return {
                value: el,
                checked: false,
              };
            })
          }
        })
        // 获取服务器时间
        wxApi.request({
          url: api.livingService.getNowDate,
        }).then(timeRes => {
          if (timeRes.data) {
            let nowTime = new Date(timeRes.data)
            let disable = !utils.time_range(serviceDetail.serverStartTime, serviceDetail.serverEndTime, nowTime)
            this.setState({
              disable: disable
            })
          } else {
            this.setState({
              disable: true
            })
          }
        }).catch(err => {
          this.setState({
            disable: true
          })
        })
        this.setState({
          goodsImages,
          serviceDetail,
          formExt,
          isStock
        })
        Taro.hideLoading()
      })
      .catch((error) => {
        Taro.hideLoading()
        if (error.data.data) {
          const goodsImages: any[] = [];
          goodsImages.push(...error.data.data.img.split(','));
          const serviceDetail = error.data.data || [];
          this.setState({
            goodsImages,
            serviceDetail,
            disable: true,
          });
        } else {
          this.apiError();
        }
      });
  }

  apiError() {
    this.setState({
      error: true,
    });
  }

  onChangeStore(storeId) {
    return wxApi
      .request({
        url: api.store.choose_new + '?storeId=' + storeId,
        loading: true,
      })
      .then((res) => {
        const store = res.data;
        if (store) {
          if (this.props.appInfo.canCustomDecorate) {
            app
              .getMultiStorePageConfig(store.id)
              .then((res) => {
                store.dispatch(updateGlobalDataAction({homeConfig: JSON.parse(res.homeConfig)}));
                this.props.homePageConfig = JSON.parse(res.homeConfig);
                const xConfig = JSON.parse(res.config);
                console.log(xConfig.mineConfig, 'xConfig.mineConfig--------xConfig.mineConfig')
                store.dispatch(updateGlobalDataAction({mineConfig: xConfig.mineConfig && xConfig.mineConfig.value}));
                this.props.mineConfig = xConfig.mineConfig && xConfig.mineConfig.value;
                updateCurrentStore(store);
              })
            return;
          }
          updateCurrentStore(store);
        }
      });
  }

  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel() {
    this.getShareDialog().show();
  }

  chooseGender = ({
                    currentTarget: {
                      dataset: {value, index},
                    },
                  }) => {
    const formExt: any = this.state.formExt;
    formExt[index].value = value;
    this.setState({
      formExt: formExt,
    });
  }

  inputValue = (e) => {
    const index = e.currentTarget.dataset.index;
    const value = e.detail.value;
    const formExt: any = this.state.formExt;
    formExt[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({formExt});
  }

  onRegionPickerChange = (value, {index}) => {
    const formExt = this.state.formExt;
    formExt[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({formExt});
  }

  onReduce = () => {
    if (this.state.num === 1) {
      return null;
    }
    const num = this.state.num - 1;
    this.setState({
      num: num,
    });
  }

  onAdd = () => {
    if (!this.state.isStock || (this.state.serviceDetail.useStock == 1 && this.state.serviceDetail.itemStock <= this.state.num)) {
      return
    }
    this.setState({
      num: this.state.num + 1,
    });
  }

  handleBuyNow = () => {
    const formExt = this.submiteCart();
    const error = this.checkForm(formExt);
    if (error) {
      Taro.showToast({icon: 'none', title: error});
      return;
    }
    const detail: any = Object.assign({}, this.state.serviceDetail);
    const itemNo = detail.itemNo;
    const id = this.state.id;
    detail.formExt = formExt;
    detail.count = this.state.num;

    wxApi.setStorageSync("serviceDetail", detail)
    if (detail.scan == 0) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay/index',
        data: {
          sourceType: this.state.sourceType,
          itemNo: itemNo,
          id: id,
        },
      });

      return;
    }

    if (storeData.globalData.roomCode) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay/index',
        data: {
          sourceType: this.state.sourceType,
          itemNo: itemNo,
          id: id,
          roomCode: storeData.globalData.roomCode,
          roomCodeTypeId: storeData.globalData.roomCodeTypeId,
          trueCode: storeData.globalData.roomCodeTypeName ? storeData.globalData.roomCodeTypeName + '-' + storeData.globalData.roomCode : storeData.globalData.roomCode,
        },
      });
    } else {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
        data: {
          sourceType: this.state.sourceType,
          id: id,
          itemNo: itemNo,
          singleService: true,
        },
      });
    }
  }

  previewImg = (e) => {
    const currentUrl = e.currentTarget.dataset.currenturl;
    const previewUrls = this.state.goodsImages;
    wxApi.previewImage({
      current: currentUrl,
      urls: previewUrls,
    });
  }

  checkForm(dataList) {
    let error = '';
    dataList.forEach((item) => {
      if (error) return;
      if (item.required) {
        if (item.type == "uploadImage") {
          if (!(item.value && item.value.length > 0)) {
            error = item.label + '不能为空';
          }
        } else {
          if (!item.value) {
            error = item.label + '不能为空';
          }
        }
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });
    return error;
  }

  /**
   * 获取上传的图片
   * @param {*} e
   */
  handleImagesChange = (images: string[], uploadIndex: number) => {
    // 配置多个
    const formExt = this.state.formExt;
    formExt[uploadIndex].value = images;
    this.setState({
      formExt, // 图片上传列表
    });
  }

  submiteCart() {
    const {formExt}: any = this.state;
    // 地址栏数据处理
    const list: any[] = [];
    formExt.forEach((item: any, index) => {
      let value = item.value || '';
      if (item.type === 'address') {
        const isNotEmptyArray = !!value && !!value.push;
        if (isNotEmptyArray) {
          if (value[0] === value[1]) {
            value = value.concat().splice(1).join('');
          } else {
            value = value.join('');
          }
        }
        value += formExt[index + 1] ? formExt[index + 1].value : '';
        list.push({
          type: item.type,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: value || '',
        });
      } else if (item.type == 'checkbox') {
        // 初始化options数据格式
        let keyName = [];
        keyName = item.options
          .filter((el) => {
            return el.checked;
          })
          .map((ele) => {
            return ele.value;
          });
        list.push({
          type: item.type,
          options: item.options,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: keyName.join(',') || '',
        });
      } else {
        list.push({
          type: item.type,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: item.value ? item.value : '',
        });
      }
    });
    return list;
  }

  // 单选修改
  changeRadioValue = (e) => {
    const items = e.target.dataset.item.options
    const value = e.detail.value
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false
      if (items[i].value === value) {
        items[i].checked = true
      }
    }
    const formExt: any = this.state.formExt
    const index = e.target.dataset.index
    formExt[index].options = items
    formExt[index].value = value
    this.setState({
      formExt,
    })
  }

  handleGetType = (value) => {
    console.log(value, '-------value');
    this.setState({
      postType: value
    });
  }
  closeMe = () => {
    this.setState({
      postType: null,
    });
  }

  // 多选修改
  bindValue = (e) => {
    const items = e.target.dataset.item.options;
    const values = e.detail.value;
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false;
      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].value === values[j]) {
          items[i].checked = true;
          break;
        }
      }
    }
    const formExt: any = this.state.formExt;
    const index = e.target.dataset.index;
    formExt[index].options = items;
    this.setState({
      formExt,
    });
  }

  render() {
    const {
      error,
      serviceDetail,
      navOpacity,
      navBgColor,
      goodsImages,
      tmpStyle,
      formExt,
      num,
      disable,
      dataLoad,
      realPath,
      dataTotal,
      isStock,
      postType,
      changeType
    }: any = this.state;
    console.log('dataTotal>>>>>>', dataTotal);

    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mplg-GoodsRentalDetails"
        className="wk-mplg-GoodsRentalDetails"
      >
        {!!error && !serviceDetail.length && <Empty message="暂无服务记录"></Empty>}

        {!!error && <Error></Error>}
        <CustomerHeader navOpacity={navOpacity} title="服务订单详情" bgColor={navBgColor}></CustomerHeader>
        {!error && (
          <View className="cards-detail-container">
            {!!goodsImages.length && (
              <Swiper indicatorDots={goodsImages.length > 1}>
                {goodsImages?.map((item: any, index) => {
                  return (
                    <Block key={'cards-image' + index}>
                      <SwiperItem>
                        {item.length > 0 && (
                          <View
                            className="cards-image"
                            style={_safe_style_(
                              'background: transparent url(' + item + ') no-repeat 50% 50%;background-size: cover;'
                            )}
                            onClick={_fixme_with_dataset_(this.previewImg, {currenturl: item})}
                          ></View>
                        )}
                      </SwiperItem>
                    </Block>
                  );
                })}
              </Swiper>
            )}

            <View className="sale-info">
              <View className="sale-order-info">
                <Text className="carouseltext-title">{serviceDetail.serverName}</Text>
                <Text
                  className="serve-time">服务时段：{serviceDetail.serverStartTime || ''}-{serviceDetail.serverEndTime || ''}</Text>
              </View>
              <View className="sale-order-info">
                <Text className="carouseltext-text">{serviceDetail.serverDesc}</Text>
              </View>

              <View className="sale-price-info">
                <View className="price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  {serviceDetail.needPayFee == 1 ? <Text className="price-symbol">￥</Text> : ''}
                  {serviceDetail.needPayFee == 1 ? filters.moneyFilter(serviceDetail.salePrice, true) : getFreeLabel(serviceDetail.showType)}
                </View>
              </View>

              {serviceDetail.originalPrice ? (
                <View className="label-price">
                  <Text>￥</Text>
                  {filters.moneyFilter(serviceDetail.originalPrice, true)}
                </View>
              ) : ''}
            </View>
            {serviceDetail.serverInfo && (
              // <DetailParser headerLabel="服务描述" itemDescribe={serviceDetail.serverInfo}></DetailParser>
              <View className="service-info serverInfo">
                <View className="carouseltext-title">服务描述</View>
                <DetailParser itemDescribe={serviceDetail.serverInfo} showHeader={false}></DetailParser>
              </View>
            )}
            <Form>
              <View className="add-service">
                {formExt && formExt.length > 0 && (
                  <View className="white-box">
                    {/* <View className="add-service-title">添加服务</View> */}
                    {
                      formExt?.map((item, index) => {
                        return (
                          <View
                            className={'form-item ' + item.type + '-item ' + (item.required ? 'required' : '')}
                            key={'form-item' + index}
                          >
                            {item.type !== 'comment' && (
                              <View>
                                <View className="form-item-ipt">
                                  {item.type === 'sex' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'sex' && (
                                    <View className="phone-input form-item-content form-sex">
                                      <View
                                        onClick={_fixme_with_dataset_(this.chooseGender, {value: '男', index: index})}
                                        className={item.value === '男' ? 'selected' : ''}
                                      >
                                        <Image
                                          mode="aspectFit"
                                          className="gender-img"
                                          src={getStaticImgUrl.subPackages.male_png}
                                        ></Image>
                                        <View>男</View>
                                      </View>
                                      <View
                                        onClick={_fixme_with_dataset_(this.chooseGender, {value: '女', index: index})}
                                        className={item.value === '女' ? 'selected' : ''}
                                      >
                                        <Image
                                          mode="aspectFit"
                                          className="gender-img"
                                          src={getStaticImgUrl.subPackages.female_png}
                                        ></Image>
                                        <View>女</View>
                                      </View>
                                    </View>
                                  )}

                                  {/*                 &lt;!&ndash; 单选框 &ndash;&gt; */}
                                  {item.type === 'radio' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'radio' && (
                                    <RadioGroup
                                      name={'radio-group-' + item.uuid + index}
                                      className="radio-box"
                                      onChange={_fixme_with_dataset_(this.changeRadioValue, {index: index, item: item})}
                                    >
                                      {item.options?.map((optionItem, index) => {
                                        return (
                                          <Label
                                            className="radio-box-item"
                                            style={_safe_style_(
                                              `background: ${optionItem.checked === true ? tmpStyle.btnColor : '#fff'};
                                                color:${optionItem.checked === true ? '#fff' : '#343434'};
                                                border-color:${optionItem.checked === true ? tmpStyle.btnColor : '#979797'}`
                                            )}
                                            key={'radio-item-' + item.uuid + index}
                                            for={'radio-item-' + item.uuid + index}>
                                            <Radio
                                              value={optionItem.value}
                                              checked={optionItem.checked}
                                              color={tmpStyle.btnColor}>
                                              {optionItem.value}
                                            </Radio>
                                          </Label>
                                        );
                                      })}
                                    </RadioGroup>
                                  )}

                                  {/*                 &lt;!&ndash; 多选框 &ndash;&gt; */}
                                  {item.type === 'checkbox' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'checkbox' && (
                                    <CheckboxGroup
                                      className="checkout-box"
                                      name="checkbox-group"
                                      onChange={_fixme_with_dataset_(this.bindValue, {index: index, item: item})}
                                    >
                                      {item.options &&
                                        item.options.map((child, index) => {
                                          return (
                                            <Label
                                              className="checkout-box-item"
                                              style={_safe_style_(
                                                `background: ${child.checked === true ? tmpStyle.btnColor : '#fff'};
                                                color:${child.checked === true ? '#fff' : '#343434'};
                                                border-color:${child.checked === true ? tmpStyle.btnColor : '#979797'}`
                                              )}>
                                              <Checkbox value={child.value} checked={child.checked}></Checkbox>
                                              {child.value}
                                            </Label>
                                          );
                                        })}
                                    </CheckboxGroup>
                                  )}

                                  {/*                 &lt;!&ndash; 时间选择器 &ndash;&gt; */}
                                  {item.type === 'date' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'date' && (
                                    <Picker
                                      className="phone-input"
                                      mode="date"
                                      start={item.dateRange ? item.dateRange[0] : ''}
                                      end={item.dateRange ? item.dateRange[1] : ''}
                                      onChange={_fixme_with_dataset_(this.inputValue, {index: index})}
                                    >
                                      {item.value ? (
                                        <Text className="picker">{item.value}</Text>
                                      ) : (
                                        <Text className="picker" style={_safe_style_('color: #888;')}>
                                          请选择
                                        </Text>
                                      )}

                                      <Image
                                        className="picker-icon"
                                        src={getStaticImgUrl.images.rightIcon_png}
                                      ></Image>
                                    </Picker>
                                  )}

                                  {/*                 &lt;!&ndash; 文本类型单行输入框 &ndash;&gt; */}
                                  {(item.type === 'text' || item.type === 'email') && (
                                    <View className="contact-number">{item.label}</View>
                                  )}

                                  {(item.type === 'text' || item.type === 'email') && (
                                    <Input
                                      className="phone-input"
                                      placeholder="请输入"
                                      value={item.value}
                                      onBlur={_fixme_with_dataset_(this.inputValue, {index: index})}
                                    ></Input>
                                  )}

                                  {/*                 &lt;!&ndash; 数字类型单行输入框 &ndash;&gt; */}
                                  {item.type === 'phone' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'phone' && (
                                    <Input
                                      className="phone-input"
                                      placeholder="请输入"
                                      type="phone"
                                      value={item.value}
                                      onBlur={_fixme_with_dataset_(this.inputValue, {index: index})}
                                    ></Input>
                                  )}

                                  {/*                 &lt;!&ndash; 多行输入框 &ndash;&gt; */}
                                  {item.type === 'textarea' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'textarea' && (
                                    <View className="textarea">
                                      <Textarea
                                        // autoHeight
                                        maxlength="-1"
                                        placeholder="请输入留言内容"
                                        onInput={_fixme_with_dataset_(this.inputValue, {index: index})}
                                      ></Textarea>
                                      {/*  <View class="num-tip">5/50</View>  */}
                                    </View>
                                  )}

                                  {/*                 &lt;!&ndash; 地址 &ndash;&gt; */}
                                  {item.type === 'address' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'address' && (
                                    <RegionPicker
                                      // className="phone-input"
                                      // mode="region"
                                      // range={[]}
                                      // value={item.value}
                                      // onChange={_fixme_with_dataset_(this.inputValue, { index: index })}

                                      className="phone-input"
                                      // mode="region"
                                      options={{index: index}}
                                      onChange={this.onRegionPickerChange}
                                    >
                                      {item.value ? (
                                        <Text className="picker">{item.value}</Text>
                                      ) : (
                                        <Text className="picker" style={_safe_style_('color: #888;')}>
                                          请选择
                                        </Text>
                                      )}

                                      <Image
                                        className="picker-icon"
                                        src={getStaticImgUrl.images.rightIcon_png}
                                      ></Image>
                                    </RegionPicker>
                                  )}

                                  {/*                 &lt;!&ndash;详细地址 &ndash;&gt; */}
                                  {item.type === 'detail-address' && (
                                    <View className="contact-number">{item.label}</View>
                                  )}

                                  {item.type === 'detail-address' && (
                                    <Textarea
                                      className="textarea"
                                      disableDefaultPadding
                                      autoHeight
                                      placeholder="请输入详细地址"
                                      onInput={_fixme_with_dataset_(this.inputValue, {index: index})}
                                      value={item.value}
                                      style={_safe_style_('min-height: 88rpx')}
                                    ></Textarea>
                                  )}

                                  {item.type === 'comment' && (
                                    <View className="comment">
                                      <Text>{item.value}</Text>
                                    </View>
                                  )}

                                  {(item.type === 'uploadImage') && (
                                    <View className="uploadImgBox">
                                      <View className="contact-number">{item.label} <Text
                                        className="serve-time"> (可上传{item.max}张)</Text></View>
                                      <View className='uploadImage-box'>
                                        <View className='img-box'>
                                          <UploadImg onChange={this.handleImagesChange} value={item.value}
                                                     max={item.max} containerIndex={index}></UploadImg>
                                        </View>
                                      </View>
                                    </View>
                                  )}
                                </View>
                              </View>
                            )}
                          </View>
                        );
                      })}
                  </View>
                )}
                {serviceDetail.showNumber === 1 &&
                  <View className="service-num">
                    <Text className="num-title">数量</Text>
                    <View className="operation-box">
                      <Image
                        className="image"
                        src={getStaticImgUrl.plugin.minus_png}
                        onClick={this.onReduce}
                      >
                        -
                      </Image>
                      <Text className="num">{num}</Text>
                      <Image
                        className="image"
                        src={getStaticImgUrl.images.service_icon_svg}
                        onClick={this.onAdd}
                        style={_safe_style_(!isStock || (serviceDetail.useStock == 1 && serviceDetail.itemStock <= num) ? 'opacity: 0.3;' : null)}
                      >
                        +
                      </Image>
                    </View>
                  </View>
                }
              </View>
            </Form>
          </View>
        )}
        {changeType && changeType != '1' && <ContactHousekeeper typeChange={dataTotal} onGetType={this.handleGetType}
                                                                className="customer-x"></ContactHousekeeper>}
        {postType && postType == 2 && (
          <View>
            <View className="posters-dialog-m" onClick={this.closeMe}></View>
            <View className='internal-posters-dialog'>
              <View className="internal-title">{dataTotal.title}</View>
              <View className='internal-posters-box'>
                <View className="internal-text">{dataTotal.customerDescribe}</View>
                <Image src={dataTotal.imgUrl} show-menu-by-longpress="true" className="qr-code"></Image>

                <View className="desc">长按识别二维码</View>
              </View>
              {/* <View className='close-T-C' onClick={this.closeMe}>X</View> */}
              <Image src="https://htrip-static.ctlife.tv/wk/marketing-base/close.png" className='close-T-C' onClick={this.closeMe}></Image>
            </View>
          </View>
        )}

        {dataLoad && (
          <Block>
            <BuyNow
              disable={disable || !isStock}
              style="z-index: 55;"
              showShare={false}
              operationText={!isStock ? '库存不足' : '不在服务时间'}
              defaultText="申请服务"
              onBuyNow={this.handleBuyNow}
              immediateShare={true}
              onShare={this.handleSelectChanel}
              showCart={false}
            ></BuyNow>
          </Block>
        )}

        <HomeActivityDialog showPage={realPath}></HomeActivityDialog>

      </View>
    );
  }
}

export default GoodsRentalDetails;
