import { _fixme_with_dataset_, _safe_style_, $getRouter } from 'wk-taro-platform';
import { View, Radio, Text, Image, Input, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
// import filters from '@/wxat-common/utils/money.wxs.js';
import state from '@/state/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import utils from '@/wxat-common/utils/util.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import store from '@/store';
import './index.scss';
const storeData = store.getState();
class GoodsReserveList extends React.Component {

  /**
   * 页面的初始数据
   */
  state = {
    template: {},
    goodsList: [],
    totalPrice: 0,
    allChecked: false,
    goodsEdit: false,
    editAllChecked: false,
  }

  $router = $getRouter()

  /**
   * 生命周期函数--监听页面加载
   */
   componentDidMount () {
    this.getTemplateStyle();
    let that = this;
    Taro.getStorage({
      key: 'storageList',
      success(res) {
        // 处理表单数据
        let arr = res.data.map((item) => {
          return {
            ...item,
            // ...JSON.parse(item),
            checked: true,
            editChecked: false,
          };
        });
        that.setState(
          {
            goodsList: arr,
          },

          () => {
            that.judgeAllCheck();
            that.calcPrice();
          }
        );
      },
    });
  }

  calCount() {
    return;
    let serviceTreeData = this.state.goodsList;
    let count = 0;
    let totalPrice = 0;
    serviceTreeData.forEach((item) => {
      count += item.checked ? item.count : 0;
      totalPrice += parseInt(item.salePrice * item.count) + parseInt(item.deposit * item.count);
    });
    this.setState({
      count,
      totalPrice: totalPrice / 100 || 0,
    });
  }
  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 选择商品
  handleCheck = (e) => {
    let index = e.currentTarget.dataset.index;
    let list = this.state.goodsList;
    if (this.state.goodsEdit) {
      list[index].editChecked = !list[index].editChecked;
    } else {
      list[index].checked = !list[index].checked;
    }
    this.setState({
      goodsList: list,
    },() => {
      this.judgeAllCheck();
      this.calcPrice();
    });
    this.calCount();
  }

  // 判断是否全选
  judgeAllCheck() {
    let list = this.state.goodsList;
    for (let i = 0; i < list.length; i++) {
      if (this.state.goodsEdit) {
        if (!list[i].editChecked) {
          this.setState({
            editAllChecked: false,
          });
          break;
        }
        this.setState({
          editAllChecked: true,
        });
      } else {
        if (!list[i].checked) {
          this.setState({
            allChecked: false,
          });
          break;
        }
        this.setState({
          allChecked: true,
        });
      }
    }
  }

  // 计算选择商品的价格
  calcPrice() {
    let list = this.state.goodsList;
    let count = 0;
    list.forEach(item => {
      if (item.checked) {
        count += (parseInt((item.salePrice?item.salePrice:item.wxItem.salePrice)*item.count));
      }
    });
    this.setState({
      totalPrice: count / 100 || 0,
    });
  }

  // 全选
  handleAllCheck = () => {
    let list = this.state.goodsList;
    let allCheck = this.state.allChecked;
    let editAllChecked = this.state.editAllChecked;
    let goodsEdit = this.state.goodsEdit;
    if (goodsEdit) {
      if (editAllChecked) {
        list = list.map((item) => {
          return {
            ...item,
            editChecked: false,
          };
        });
      } else {
        list = list.map((item) => {
          return {
            ...item,
            editChecked: true,
          };
        });
      }
    } else {
      if (allCheck) {
        list = list.map((item) => {
          return {
            ...item,
            checked: false,
          };
        });
      } else {
        list = list.map((item) => {
          return {
            ...item,
            checked: true,
          };
        });
      }
    }
    this.setState({
      goodsList: list,
      allChecked: goodsEdit ? allCheck : !allCheck,
      editAllChecked: goodsEdit ? !editAllChecked : editAllChecked,
    },() => {
      this.calcPrice();
      this.calCount();
    });
  }

  // 编辑
  handleEdit = () => {
    let list = this.state.goodsList;
    if (!this.state.goodsEdit) {
      list = list.map((item) => {
        return {
          ...item,
          editChecked: false,
        };
      });
    }
    this.setState({
      goodsEdit: !this.state.goodsEdit,
      goodsList: list,
    });
  }

  // 立即下单或删除
  async handleFoot()  {
    if (!authHelper.checkAuth()) return;
    const { goodsEdit, goodsList } = this.state
    let list = goodsList;
    if (goodsEdit) {
      let ishasDeleteList = list.find((item) => {
        return item.editChecked == true;
      });
      if (!ishasDeleteList) {
        wxApi.showToast({
          icon: 'none',
          title: '请选择要删除的商品',
        });
        return;
      }
      let filterList = list.filter((item) => {
        return !item.editChecked;
      });
      this.setState({
        goodsEdit: !this.state.goodsEdit,
        goodsList: filterList,
      });
      this.calcPrice();
      // 缓存购物车数据
      let stringifyList = filterList.map((item) => {
        return item;
      });
      wxApi.setStorageSync('storageList', stringifyList);
      wxApi.setStorageSync('storageOrderList', stringifyList);
      this.calCount();
    } else {
      // 下单数据
      let orderList = this.state.goodsList.filter((item) => {
        return item.checked;
      });
      if (orderList.length == 0) {
        Taro.showToast({
          icon: 'none',
          title: '请选择要下单的服务',
        });

        return;
      }

      // 循环生成格式缓存，然后扫码
      const serverGoodsList = orderList.map(item => {
        if (item.itemNo) return item
        const { wxItem } = item
        const _item = {
          itemNo: wxItem.itemNo,
          scan: item.scan,
          barcode: wxItem.barcode,
          pic: wxItem.thumbnail,
          name: wxItem.name,
          noNeedPay: wxItem.noNeedPay,
          freight: wxItem.freight,
          wxItem: wxItem,
          itemCount: item.count,
          count: item.count,
          salePrice: wxItem.salePrice,
        }
        return _item
      })
      // 获取从商品详情添加的商品
      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', serverGoodsList);
      Taro.setStorageSync('GOODS_LIST_CUT', JSON.stringify(serverGoodsList))
      window.sessionStorage.setItem('GOODS_LIST_CUT', JSON.stringify(serverGoodsList))
      wxApi.setStorageSync('storageOrderList', serverGoodsList);

      //判断是否包含需要扫码的商品
      const flag = await utils.checkNeedScan(serverGoodsList.map((e) => typeof(e) == 'string' ? JSON.parse(e) : e));
      if (!flag) {
        wxApi.$navigateTo({
          url: '/sub-packages/order-package/pages/pay-order/index',
          data: {
            categoryType: 5,
            title: '确认订单',
          },
        });
        return;
      }

      if (storeData.globalData.roomCode) {
        if (orderList.length == 0) {
          wxApi.showToast({
            icon: 'none',
            title: '请选择要下单的服务',
          });
          return;
        }
        wxApi.$navigateTo({
          url: '/sub-packages/order-package/pages/pay-order/index',
          data: {
            roomCode: state.roomCode,
            roomCodeTypeId: state.roomCodeTypeId,
            roomCodeTypeName: state.roomCodeTypeName,
            trueCode: state.roomCodeTypeName ? state.roomCodeTypeName + '-' + state.roomCode : state.roomCode,
            categoryType: 5,
            title: '确认订单',
          },
        });
      } else {
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
          data: {
            isDistributionServices: true
          },
        })
      }
    }
  }

  //购物车添加
  clickPlus = (e) => {
    const index = e.currentTarget.dataset.index;
    let goodsList = this.state.goodsList;
    let count = goodsList[index].count;
    count = count + 1;
    goodsList[index].count = count;
    this.setState({
      goodsList: goodsList,
    },()=>{
      this.calcPrice();
      this.calCount();
    });

    goodsList = goodsList.map((item) => {
      return item;
    });
    wxApi.setStorageSync('storageList', goodsList);
    wxApi.setStorageSync('storageOrderList', goodsList); //下单缓存
  }

  //购物车减少
  clickMinus = (e) => {
    const index = e.currentTarget.dataset.index;
    let goodsList = this.state.goodsList;
    let count = goodsList[index].count;

    if (count <= 1) {
      goodsList.splice(index, 1);
      this.setState({
        goodsList: goodsList,
      });

      Taro.setStorageSync('storageList', goodsList);
      Taro.setStorageSync('storageOrderList', goodsList); //下单缓存
      this.calcPrice();
      return false;
    }

    count = count - 1;
    goodsList[index].count = count;
    this.setState({
      goodsList: goodsList,
    });

    this.calcPrice();
    goodsList[index].count = count;

    wxApi.setStorageSync('storageList', goodsList);
    wxApi.setStorageSync('storageOrderList', goodsList);
    this.calCount();
  }

  render() {
    const { goodsEdit, goodsList, tmpStyle, editAllChecked, allChecked, totalPrice } = this.state;
    return (
      <View data-scoped="wk-mpls-ServiceReserveList" className="wk-mpls-ServiceReserveList">
        {goodsList.length !== 0 && (
          <View className="header">
            <View>
              <Radio
                value={goodsEdit ? editAllChecked : allChecked}
                color={tmpStyle.btnColor}
                checked={goodsEdit ? editAllChecked : allChecked}
                onChange={this.handleAllCheck}
              ></Radio>
              <Text>全选</Text>
            </View>
            <Text onClick={this.handleEdit}>{goodsEdit ? '完成' : '编辑'}</Text>
          </View>
        )}

        <View className="container">
          {goodsList.length !== 0 ? (
            <View>
              {goodsList &&
                goodsList.map((item, index) => {
                  return (
                    <View key={'goods-item-' + index} className="goods-item">
                      <Radio
                        value={goodsEdit ? item.editChecked : item.checked}
                        color="rgba(243,95,40,1)"
                        checked={goodsEdit ? item.editChecked : item.checked}
                        onChange={_fixme_with_dataset_(this.handleCheck, { index: index })}
                      ></Radio>
                      <View className="goods-item-info">
                        <Image src={item.thumbnailMid} className="goods-item-info-img"></Image>
                        <View>
                          <View className="goods-item-info-name">{item.wxItem.name}</View>
                          {item.skuInfo && <View className="attrs">{item.skuInfo}</View>}

                          <View className="goods-item-info-form">
                            <View style={_safe_style_('color: ' + tmpStyle.btnColor + ';')}>
                              {'￥' + (item.salePrice ? item.salePrice / 100 : item.wxItem.salePrice / 100)}
                            </View>
                          </View>
                        </View>
                        {/*  <view class="goods-item-info-right" wx:if="{{!goodsEdit}}">
                              <view>x{{item.count}}</view>
                              <view>原价：￥{{ item.deposit / 100 }}</view>
                              <view>售价：￥{{ item.wxItem.salePrice / 100 }}</view>
                            </view>  */}
                        <View className="goods-item-info-right">
                          {item.count > 0 && (
                            <View className="count-box">
                              <View
                                className="minus left-radius"
                                onClick={_fixme_with_dataset_(this.clickMinus, {
                                  item: item,
                                  index: index,
                                })}
                              >
                                -
                              </View>
                              <View className="num">
                                <Input className="input-comp" type="number" value={item.count} disabled></Input>
                              </View>
                              <View
                                className="plus right-radius"
                                onClick={_fixme_with_dataset_(this.clickPlus, {
                                  item: item,
                                  index: index,
                                })}
                              >
                                +
                              </View>
                            </View>
                          )}
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>
          ) : (
            <View className="empty">暂无预订清单</View>
          )}
        </View>
        {goodsList.length !== 0 && (
          <View className="goods-all-info">
            <View>
              <Radio
                value={goodsEdit ? editAllChecked : allChecked}
                color="rgba(243,95,40,1)"
                checked={goodsEdit ? editAllChecked : allChecked}
                onClick={this.handleAllCheck}
              ></Radio>
              <Text>全选</Text>
            </View>
            {!goodsEdit && (
              <View className="goods-all-info-price">
                <View>合计：</View>
                <Text className="font-color">{'￥' + totalPrice}</Text>
              </View>
            )}

            <Button
              className="mini-btn sBtn"
              style={_safe_style_('background:' + tmpStyle.btnColor + ' ;')}
              onClick={this.handleFoot.bind(this)}
            >
              {goodsEdit ? '删除' : '结算'}
            </Button>
          </View>
        )}
      </View>
    );
  }
}

export default GoodsReserveList;
