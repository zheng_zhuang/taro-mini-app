import { _fixme_with_dataset_, _safe_style_, $getRouter } from 'wk-taro-platform';
import {
  Block,
  View,
  Image,
  Text,
  Input,
  Textarea,
  Form,
  RadioGroup,
  Label,
  Radio,
  CheckboxGroup,
  Checkbox,
  Picker,
  Button,
  ScrollView,
} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import industryEnum from '@/wxat-common/constants/industryEnum.js';
import wxApi from '@/wxat-common/utils/wxApi';
import constants from '@/wxat-common/constants/index.js';
import api from "../../../../../wxat-common/api";
import store from '@/store';
import utils from '../../../../../wxat-common/utils/util.js';
import template from '../../../../../wxat-common/utils/template.js';
import priceHandler from '../../../utils/priceHandler.js';
import couponEnum from '../../../../../wxat-common/constants/couponEnum.js';
import BottomDialog from "../../../../../wxat-common/components/bottom-dialog";
import deliveryWay from '@/wxat-common/constants/delivery-way.js';
import './index.scss';
import payUtils from '@/wxat-common/utils/pay';
import RegionPicker from '@/sub-packages/scene-package/components/region-picker/index'
// import UploadImg from "@/wxat-common/components/upload-img";
import {getFreeLabel} from '@/wxat-common/utils/service'
import { connect } from 'react-redux';
const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};

const storeData = store.getState();
const mapStateToProps = (state) => {
  return {
    currentStore: state.base.currentStore,
    userInfo:state.base.userInfo,
    roomCode: state.globalData.roomCode,
    roomCodeTypeId: state.globalData.roomCodeTypeId,
    roomCodeTypeName: state.globalData.roomCodeTypeName,
    trueCode: state.globalData.roomCodeTypeName + '-' + state.globalData.roomCode,
  };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class GoodsRentalPendingpayMerge extends React.Component {
  $router = $getRouter();

  state = {
    payLoading: false, // 支付提交中
    tmpStyle: {}, // 主题模板配置
    industryEnum, // 行业类型常量
    industry: storeData.globalData.industry, // 行业类型
    itemNo: null, // 商品单号
    id: null, // 商品id
    goodsImages: [], // 商品图片
    goodsImages_one: null, // 第一张商品图片
    serviceDetail: null, // 服务商品详情
    totalPrice: null, // 合计价钱
    dataLoad: false,
    sessionId: null,
    isExpand: false,
    couponEnum,
    extraData: null,
    _optionsData: null,
    _storeId: null,
    formBody: [], // 表单内容
    roomCode: '', // 房间号 上UAT换为空
    sourceType: '', // 商品类型
    goodsList: [],
    deposit: 0,
    salePrice: 0,
    trueCode: '',
    qrCodeTypeId: null,
    orderMessage: '',
    price: {},
    coupons: {},
    curCoupon: {},
    needScan: false, // 公众号h5标识，需要扫码
    isShowTips:false,
    tipsMsgs:[]
  }

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    const options = this.$router.params
    console.log(options)
    wxApi.hideShareMenu({
      menus: ['shareAppMessage', 'shareTimeline'],
    });

    this.getTemplateStyle(); // 获取主题模板配置

    if (options && options.isDetail == 1) {
      // const serviceDetail = JSON.parse(options.serviceDetail);
      const serviceDetail = JSON.parse(wxApi.getStorageSync('serviceDetail'));
      serviceDetail.salePriceText = (serviceDetail.salePrice / 100).toFixed(2);
      console.log(serviceDetail)
      this.setState(
        {
          goodsList: [serviceDetail],
          deposit: serviceDetail.deposit || 0,
          salePrice: serviceDetail.salePrice || 0,
          totalPrice: serviceDetail.deposit + serviceDetail.salePrice || 0,
        },

        () => {
          this.handleGoodsInfoChange();
        }
      );
    } else {
      this.getServiceDetail();
      // 计算优惠劵
    }
    this.setState({
      roomCode: this.props.roomCode || options.roomCode,
      trueCode: this.props.roomCode ? this.props.roomCodeTypeName + '-' + this.props.roomCode : options.trueCode,
      qrCodeTypeId: this.props.roomCodeTypeId || options.roomCodeTypeId,
      needScan: !!options.needScan
    });
  }

  // 错误提示方法
  errorTip(msg) {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }


  //
  handleGoToCouponList = () => {
    const couponSet = this.state.coupons.couponSet;
    if (!couponSet || couponSet.length < 1) {
      return this.errorTip('无可用优惠券');
    }

    this.setState({
      isCoupon: 1,
    });

    this.getChoseDialog().showModal();
  }

  // 获取优惠对话框
  getChoseDialog() {
    return this.chooseCardDialogCOMP;
  }

  // 计算优惠劵的价格
  handleGoodsInfoChange() {
    let data = {
      expressType: 0,
      storeId: this.props.currentStore.id,
      orderServerPlatformDTO: {
        platformDay: 1,
      },
    };

    data.itemDTOList = this.state.goodsList.map((item) => {
      const p = {
        itemNo: item.itemNo,
        skuId: item.skuId,
        itemCount: item.count || 1,
        itemType: 44,
      };

      // 促销相关的请求参数
      if (item.mpActivityType) {
        p.mpActivityType = item.mpActivityType;
      }
      if (item.mpActivityId) {
        p.mpActivityId = item.mpActivityId;
      }
      if (item.mpActivityName) {
        p.mpActivityName = item.mpActivityName;
      }

      // 赠品相关的请求参数
      if (item.activityType) {
        p.activityType = item.activityType;
      }
      if (item.activityId) {
        p.activityId = item.activityId;
      }
      return p;
    });

    data = utils.formatParams(data);
    this.setState({
      data,
    });

    priceHandler
      .fetchPreferentialList(data)
      .then((res) => {
        if (res) {
          const { curRedPacke, curCoupon, coupons, couponSet } = res;
          if (curCoupon.code) {
            this.setState({
              curRedPacke,
              curCoupon,
              coupons,
            });

            data['orderRequestPromDTO.couponNo'] = curCoupon.code;
          } else {
            this.setState({
              curRedPacke,
              coupons,
              curCoupon,
            });
          }
        }
        // 获取订单价格

        return priceHandler.getPrice(data);
      })
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
          payAble: true,
        });
      })
      .catch((error) => {
        if (!error.quite) {
          // 未选择收货地址时报错
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  onSelectCard = (e) => {
    const coupon = e.currentTarget.dataset.coupon;
    // 优惠券或红包状态是可用的或者优惠类型为礼品卡或次数卡（次数后端返回都是可用的）
    if (
      (coupon.status && coupon.status === 1) ||
      coupon.cardType === constants.card.type.gift ||
      coupon.cardType === constants.card.type.coupon
    ) {
      switch (this.state.isCoupon) {
        case 1:
          const data = this.state.data;
          data['orderRequestPromDTO.couponNo'] = coupon.code;
          this.setState({
            curCoupon: coupon,
            data,
          });

          break;
        case 2:
          this.setState({
            curRedPacke: coupon,
          });

          break;
        case 3:
          this.setState({
            serveCard: coupon,
          });

          break;
        case 4:
          this.setState({
            curGiftCard: coupon,
          });

          break;
      }

      this.getChoseDialog().hideModal();
      this.handlePreferentialChange();
    } else {
      wxApi.showToast({
        title: coupon.rejReason || '该优惠不能使用。',
        icon: 'none',
      });
    }
  }

  /**
   * 优惠信息变化时的handler，
   * 如：优惠券选择，礼品卡选择、红包选择等
   */
  handlePreferentialChange() {
    this.setState({
      payAble: false,
    });

    wxApi.showLoading({
      title: '请稍等',
    });

    priceHandler
      .getPrice(this.state.data)
      .then((res) => {
        const { price } = res;
        this.setState({
          price,
          payAble: true,
        });
      })
      .catch((error) => {
        if (!error.quite) {
          // 未选择收货地址时报错
          wxApi.showToast({
            title: error.msg,
            icon: 'none',
          });
        }
      })
      .finally(() => {
        wxApi.hideLoading();
      });
  }

  // 不使用优惠
  onNoUseCard = () => {
    switch (this.state.isCoupon) {
      case 1:
        const data = this.state.data;
        delete data['orderRequestPromDTO.couponNo'];
        this.setState({
          curCoupon: {},
          data: data,
        });

        break;
      case 2:
        this.setState({
          curRedPacke: {},
        });

        break;
      case 3:
        this.setState({
          serveCard: {},
        });

        break;
      case 4:
        this.setState({
          curGiftCard: {},
        });

        break;
    }

    this.getChoseDialog().hideModal();
    this.handlePreferentialChange();
  }

  // 获取主题
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  getServiceDetail() {
    const that = this;
    wxApi.getStorage({
      key: 'serviceOrderList',
      success(res) {
        let deposit = 0;
        let salePrice = 0;
        const list = res.data.map((item) => {
          return JSON.parse(item);
        });
        list.forEach((item) => {
          deposit += item.deposit * item.count;
          salePrice += item.salePrice * item.count;

          item.salePriceText = (item.salePrice / 100).toFixed(2);
        });
        that.setState(
          {
            goodsList: list,
            deposit: deposit,
            salePrice: salePrice,
            totalPrice: deposit + salePrice,
          },

          () => {
            that.handleGoodsInfoChange();
          }
        );
      },
    });
  }

  // 单选修改
  changeRadioValue = (e) => {
    const items = e.target.dataset.item.options
    const value = e.detail.value
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false
      if (items[i].value === value) {
        items[i].checked = true
      }
    }
    const formExt: any = this.state.formExt
    const index = e.target.dataset.index
    formExt[index].options = items
    formExt[index].value = value
    this.setState({
      formExt,
    })
  }

  // 多选修改
  bindValue = (e) => {
    const items = e.target.dataset.item.options;
    const values = e.detail.value;
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false;
      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].value === values[j]) {
          items[i].checked = true;
          break;
        }
      }
    }
    const formExt: any = this.state.formExt;
    const index = e.target.dataset.index;
    formExt[index].options = items;
    console.log(formExt)
    this.setState({
      formExt,
    });
  }

  inputValue = (e) => {
    const index = e.currentTarget.dataset.index;
    const value = e.detail.value;
    const formBody = this.state.formBody;
    formBody[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({ formBody });
  }

  onRegionPickerChange=(value,{index})=>{
    const formBody = this.state.formBody;
    formBody[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({ formBody });
  }

  chooseGender = ({
    currentTarget: {
      dataset: { value, index },
    },
  }) => {
    const formBody = this.state.formBody;
    formBody[index].value = value;
    this.setState({
      formBody: formBody,
    });
  }

  checkForm(dataList) {
    let error = '';
    dataList.forEach((item) => {
      if (error) return;
      if (item.required && !item.value) {
        error = item.label + '不能为空';
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });
    return error;
  }

  /**
   * 支付
   * @param {*} e
   */
  handlePay = () => {
    if(this.state.payLoading){
      return
    }
    const { formBody } = this.state;
    const error = this.checkForm(formBody);
    if (error) {
      wxApi.showToast({ icon: 'none', title: error });
      return;
    }
    if (this.state.needScan && !this.state.roomCode) {
      wxApi.showToast({
        icon: 'none',
        title: '请输入位置信息'
      })
      return
    }
    const timeSlot = `${this.state.inTime},${this.state.outTime}`;

    const list = [];
    let isFree = true // 免费商品判断
    const data = this.state.goodsList.map((item) => {
      return {
        ...item,
        formExt: JSON.stringify(item.widgetValue) || '',
        itemCount: item.count,
      };
    });
    for (let i = 0; i < this.state.goodsList.length; i++) {
      if (this.state.goodsList[i].needPayFee !== 0) {
        isFree = false
        break
      }
    }
    // this.state.payLoading = true
    this.setState({
      payLoading:true
    })
      wxApi
      .request({
        url: api.livingService.unifiedPlatformOrderV1,
        method: 'POST',
        quite:true,
        data: {
          formExt: JSON.stringify(list),
          platformItemList: data,
          itemNo: this.state.itemNo,
          itemCount: 1,
          deposit: this.state.deposit,
          // deposit: 0.01,
          platformDay: 1,
          timeSlot: timeSlot,
          roomCode: this.state.roomCode,
          // roomCode: 600,
          qrCodeTypeId: this.state.qrCodeTypeId,
          // qrCodeTypeId: 17,
          orderMessage: this.state.orderMessage,
          orderRequestPromDTO: {
            couponNo: this.state.curCoupon.code,
          },
          payTradeType: payUtils.returnPayTradeType()
        },
      })
      .then((res) => {
        const payInfo = res.data;
        // FIXME: 下单金额是否免费
        if(!payInfo.price){
          isFree = true
        }else{
          isFree = false
        }

        // 下单清除serviceOrderList下单缓存，更新serviceTreeData购物车缓存
        const treeData = (wxApi.getStorageSync('serviceTreeData') || []).map((item) => {
          return JSON.parse(item);
        });

        const orderList = (wxApi.getStorageSync('serviceOrderList') || []).map((item) => {
          return JSON.parse(item);
        });

        for (let v = 0; v < treeData.length; v++) {
          for (let i = 0; i < orderList.length; i++) {
            if (treeData[v].id == orderList[i].id) {
              treeData.splice(v, 1);
            }
          }
        }
        const stringifyTreeData = treeData.map((item) => {
          return JSON.stringify(item);
        });
        wxApi.setStorageSync('serviceTreeData', stringifyTreeData);
        wxApi.setStorageSync('serviceOrderList', []);
        if (isFree) {
          wxApi.$navigateTo({
            url: '/wxat-common/pages/tabbar-order/index',
            data: {
              tabType: 'service_order',
            },
          });
          return
        }
        if(process.env.WX_OA === 'true'){
          wx.chooseWXPay({
            timestamp: payInfo.timeStamp,
            nonceStr: payInfo.nonceStr,
            package: 'prepay_id=' + payInfo.prepayId,
            signType: 'RSA',
            paySign: payInfo.paySign,
            success(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
            cancel(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
          });
        } else {
          wxApi.requestPayment({
            timeStamp: payInfo.timeStamp + '',
            nonceStr: payInfo.nonceStr,
            package: 'prepay_id=' + payInfo.prepayId,
            signType: 'RSA',
            paySign: payInfo.paySign,
            success(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
            fail(e) {
              wxApi.$navigateTo({
                url: '/wxat-common/pages/tabbar-order/index',
                data: {
                  tabType: 'service_order',
                },
              });
            },
          });
        }
      })
      .catch((error) => {
        const {data:{errorMessage}} = error
        if(errorMessage.indexOf(',') !== -1){
          this.setState({
            tipsMsgs:errorMessage.split(','),
            isShowTips:true
          },()=>{
            setTimeout(()=>{
              this.setState({
                tipsMsgs:[],
                isShowTips:false
              })
            },1000)
          })
        }else{
          wxApi.showToast({
            title: errorMessage,
            icon: 'none',
            duration: 1000
          })
        }
      }).finally((res) => {
        this.setState({
          payLoading:false,
        })
      });

  }

  // 扫码获取房间号
  scanCode = () => {
    Taro.scanCode({
      success: (res) => {
        if (!res.path) {
          return wxApi.showToast({
            title: '无法识别房间信息',
            icon: 'none',
          });
        }

        let path:any = decodeURIComponent(res.path);

        // 取url后面的参数 无需关注
        // let vars = res.path.split('=');
        // vars.splice(0, 1);
        // vars = vars.join('=');
        // const query = vars.split('&');
        // const params = {};
        // for (let i = 0; i < query.length; i++) {
        //   const q = query[i].split('=');
        //   if (q.length === 2) {
        //     params[q[0]] = q[1];
        //   }
        // }
        path = path.substring(path.lastIndexOf('?') + 1)
        path = path.split('&')
        const params = {}
        for(let i=0;i<path.length;i++){
          const val = path[i].split('=')
          params[val[0]] = val[1]
        }
        if (params.id) {
          wxApi
            .request({
              url: api.queryQrCodeScenes,
              isLoginRequest: true,
              method: 'GET',
              data: {
                id: params.id,
              },
            })
            .then(({ data }) => {
              const dataObj = {};
              data.split('&').forEach((item) => {
                const key = item.split('=')[0];
                const value = item.split('=')[1];
                dataObj[key] = value;
              });
              return wxApi.request({
                url: api.selectQrCodeNumber,
                isLoginRequest: true,
                method: 'POST',
                data: {
                  id: params.id,
                  hotelId: dataObj.pId,
                  qrCodeNumber: params.qrcodeNo,
                },
              });
            })
            .then(({ data: { qrCodeNumber, roomCode, roomNumber, qrCodeTypeId, qrCodeTypeName,storeId } }) => {
              if (this.props.userInfo.storeId !== storeId) {
                wxApi.showToast({
                  title: '小程序门店与二维码门店不匹配',
                  icon: 'none'
                });
                return
              }
              if (!roomCode && (params.tp == 1 || !params.tp)) {
                wxApi.showToast({
                  title: '获取不到位置信息',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                wxApi.showToast({
                  title: '获取不到位置信息',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? qrCodeTypeName + '-' + roomNumber
                      : qrCodeTypeName + '-' + (params.id ? roomCode : qrCodeNumber)
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
              },()=>{
                console.log(this.state)
              });
            })
            .catch((err) => {});
        } else if (params.qrcodeNo) {
          wxApi
            .request({
              url: api.selectQrCodeNumber,
              isLoginRequest: true,
              method: 'POST',
              data: {
                hotelId: params.pId || params.hId,
                qrCodeNumber: params.qrcodeNo,
              },
            })
            .then(({ data: { qrCodeNumber, roomCode, roomNumber, qrCodeTypeId, qrCodeTypeName } }) => {
              if ((!roomCode || !qrCodeNumber) && (params.tp == 1 || !params.tp)) {
                wxApi.showToast({
                  title: '获取不到位置信息',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                wxApi.showToast({
                  title: '获取不到位置信息',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? qrCodeTypeName + '-' + roomNumber
                      : qrCodeTypeName + '-' + (params.id ? roomCode : qrCodeNumber)
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
              });
            })
            .catch((err) => {});
        }
      },
    });
  }

  remarkInputChange = (e) => {
    this.setState({
      orderMessage: e.detail.value,
    });
  }

  // 去预订清单页
  toReserveList = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/living-service/goods-reserve-list/index',
    });
  }

  // 输入位置信息
  inputRoomCode = (e) => {
    const value = e.detail.value
    this.setState({
      roomCode: value
    })
  }

  // 查看上传的图片
  previewUploadImage(previewUrls,e){
    e.stopPropagation()
    wxApi.previewImage({
      urls: previewUrls
    });
  }

  refChooseCardDialogCOMP = (node) => (this.chooseCardDialogCOMP = node);

  render() {
    const {
      goodsList,
      tmpStyle,
      payLoading,
      deposit,
      salePrice,
      roomCode,
      trueCode,
      orderMessage,
      formBody,
      curCoupon,
      couponEnum,
      price,
      coupons,
      payDetails,
      curGiftCard,
      seckillNo,
      useLogisticsCoupon,
      choosedDelivery,
      serveCard,
      curRedPacke,
      scoreName,
      goodsIntegral,
      goodsInternal,
      isCardpackType,
      goodsInfoList,
      goodsTotalPrice,
      isEstateType,
      allFreightCollect,
      isGiftActivity,
      isFrontMoneyItem,
      isFullScreen,
      totalPrice,
      isError,
      isCoupon,
      needScan,
      isShowTips,
      tipsMsgs
    } = this.state;
    return (
      <View
        data-scoped="wk-mplg-GoodsRentalPendingpayMerge"
        className="wk-mplg-GoodsRentalPendingpayMerge goods-rental-details"
      >
        <View className="goods-rental-details-item">
          {goodsList &&
            goodsList.map((item, index) => {
              return (
                <View key={item.serverId} className="goods-item">
                  <View className="goods-rental-information">
                    <View className="information-img">
                      <Image
                        className="information-img"
                        src={filters.imgListFormat(item.img)}
                        mode="aspectFill"
                      ></Image>
                    </View>
                    <View className="information-text">
                      <View className="informationtext-title">{item.serverName}</View>
                      <View className="informationtext-form">
                        {item.widgetValue &&
                          item.widgetValue.map((el, index) => {
                            return (
                              <View key={el.label}>
                                <View className="informationtext-text">

                                  {el.type === 'uploadImage'&& el.value.length ? (
                                    <View>{el.label}: 共{el.value.length}张图片 <Text style={_safe_style_(`color:${tmpStyle.btnColor};`)} onClick={this.previewUploadImage.bind(this,el.value)}>查看</Text>
                                    </View>
                                  ) : (<View>{el.label + '：' + el.value}</View>)}

                                  </View>
                              </View>
                            );
                          })}
                      </View>
                      <View
                        className="informationtext-bottom"
                        style={_safe_style_('margin-top: ' + (item.widgetValue ? (item.widgetValue.length ? '28px' : '75px') : '75px'))}
                      >
                        <View className="informationtext-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                          {item.needPayFee == 1 ? <Text>￥</Text> : ''}
                          {item.needPayFee == 1 ? item.salePriceText : getFreeLabel(item.showType)}
                        </View>
                        <View className="informationtext-count">{'x' + item.count}</View>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
        </View>
        <View className="goods-rental-info">
          <View className="information-itemlist">
            {deposit ? (
              <View className="information-items-item">
                <View className="itemsItem-left">应押押金</View>
                <View className="itemsItem-right">{'￥' + filters.moneyFilter(deposit, true)}</View>
              </View>
            ) : ''}

            {salePrice ? (
              <View className="information-items-item">
                <View className="itemsItem-left">应收费用</View>
                <View className="itemsItem-right">{'￥' + filters.moneyFilter(salePrice, true)}</View>
              </View>
            ) : ''}

            {trueCode ? (
              <View className="information-items-item">
                <View className="itemsItem-left">
                  配送位置
                  <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>
                </View>
                <View className="itemsItem-right" onClick={this.scanCode} style={_safe_style_('width:500rpx;')}>
                <Text style={_safe_style_('text-align:right;flex:1;color:#8893a6')}>{trueCode}</Text>
                  {!trueCode ? (
                    <Image
                      style={_safe_style_(
                        'background-color:' +
                          (tmpStyle.btnColor || 'rgba(243, 95, 40, 0.8)') +
                          ";background-image: url('https://front-end-1302979015.file.myqcloud.com/images/scan.svg')"
                      )}
                      className="mask-img"
                    ></Image>
                  ) : ''}
                </View>
              </View>
            ) : (needScan ? (
              <View className="information-items-item">
                <View className="itemsItem-left">
                  配送位置
                  <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>
                </View>
                <View className="itemsItem-right" style={_safe_style_('width:500rpx;')}>
                  <Input
                    placeholder="请输入位置信息"
                    value={roomCode == 'null' ? '' : trueCode}
                    style={_safe_style_('width:100%；color: #444')}
                    placeholderStyle="text-align: left;color:#8893A6"
                    onBlur={_fixme_with_dataset_(this.inputRoomCode)}
                  ></Input>
                </View>
              </View>
            ) : '')}

            <View className="information-items-item">
              <View className="itemsItem-left" style={_safe_style_('height: 3em;width:240rpx;')}>
                备注
              </View>
              <View className="itemsItem-right">
                <Textarea
                  value={orderMessage}
                  className='textarea-server'
                  maxlength="260"
                  placeholder="请输入备注"
                  placeholderStyle="color:#8893A6"
                  style={_safe_style_('width:250px;border: 1px solid #ccccccc;height: 42px;')}
                  onInput={this.remarkInputChange}
                ></Textarea>
              </View>
            </View>
          </View>
        </View>
        <Form>
          <View className="form-body">
            <View className="white-box">
              {formBody &&
                formBody.map((item, index) => {
                  return (
                    <View className={'form-item ' + item.type + '-item'} key={item.uuid}>
                      {item.type !== 'comment' && (
                        <View className="form-item-name">
                          <Text>{item.label}</Text>
                          {item.required && <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>}
                        </View>
                      )}

                      {/*  性别  */}
                      {item.type === 'sex' && (
                        <View className="form-item-content form-sex">
                          <View
                            onClick={_fixme_with_dataset_(this.chooseGender, { value: '男', index: index })}
                            className={item.value === '男' ? 'selected' : ''}
                          >
                            <Image
                              mode="aspectFit"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/male.png"
                            ></Image>
                            <View>男</View>
                          </View>
                          <View
                            onClick={_fixme_with_dataset_(this.chooseGender, { value: '女', index: index })}
                            className={item.value === '女' ? 'selected' : ''}
                          >
                            <Image
                              mode="aspectFit"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/female.png"
                            ></Image>
                            <View>女</View>
                          </View>
                        </View>
                      )}

                      {/*  单选框  */}
                      {item.type === 'radio' && (
                        <RadioGroup
                          className="form-item-content form-radio"
                          name="radio-group"
                          onChange={_fixme_with_dataset_(this.changeRadioValue, { index: index, item: item })}
                        >
                          {item.options &&
                            item.options.map((optionItem, index) => {
                              return (
                                <Label key={optionItem.value}>
                                  <Radio value={optionItem} color={tmpStyle.btnColor}></Radio>
                                  {optionItem}
                                </Label>
                              );
                            })}
                        </RadioGroup>
                      )}

                      {/*  多选框  */}
                      {item.type === 'checkbox' && (
                        <CheckboxGroup
                          className="form-item-content form-radio"
                          name="radio-group"
                          onChange={_fixme_with_dataset_(this.bindValue, { index: index })}
                        >
                          {item.options &&
                            item.options.map((optionItem, index) => {
                              return (
                                <Label key={optionItem.value}>
                                  <Checkbox value={optionItem} color={tmpStyle.btnColor}></Checkbox>
                                  {optionItem}
                                </Label>
                              );
                            })}
                        </CheckboxGroup>
                      )}

                      {/*  时间选择器  */}
                      {item.type === 'date' && (
                        <Picker
                          className="form-item-content form-date"
                          mode="date"
                          start={item.dateRange[0]}
                          end={item.dateRange[1]}
                          onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                        >
                          {item.value ? (
                            <Text className="picker">{item.value}</Text>
                          ) : (
                            <Text style={_safe_style_('color: #888;')}>请选择</Text>
                          )}

                          <Image
                            className="picker-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                          ></Image>
                        </Picker>
                      )}

                      {/*  文本类型单行输入框  */}
                      {(item.type === 'text' || item.type === 'email') && (
                        <Input
                          className="form-item-content form-input"
                          placeholder="请输入"
                          value={item.value}
                          onBlur={_fixme_with_dataset_(this.inputValue, { index: index })}
                        ></Input>
                      )}

                      {/*  数字类型单行输入框  */}
                      {item.type === 'phone' && (
                        <Input
                          className="form-item-content form-input"
                          placeholder="请输入"
                          type="number"
                          onBlur={_fixme_with_dataset_(this.inputValue, { index: index })}
                        ></Input>
                      )}

                      {/*  多行输入框  */}
                      {item.type === 'textarea' && (
                        <View className="form-textarea">
                          <Textarea
                            className="form-item-content"
                            maxlength="-1"
                            placeholder="请输入留言内容"
                            onInput={_fixme_with_dataset_(this.inputValue, { index: index })}
                          ></Textarea>
                          {/*  <View class="num-tip">5/50</View>  */}
                        </View>
                      )}

                      {/*  地址  */}
                      {item.type === 'address' && (
                        <RegionPicker
                          // className="form-item-content form-address"
                          // mode="region"
                          // onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                          className="form-item-content form-address"
                                      // mode="region"
                                      options={{index:index}}
                                      onChange={this.onRegionPickerChange}
                        >
                          {item.value ? (
                            <Text className="picker">{item.value}</Text>
                          ) : (
                            <Text style={_safe_style_('color: #888;')}>请选择</Text>
                          )}

                          <Image
                            className="picker-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                          ></Image>
                        </RegionPicker>
                      )}

                      {/* 详细地址  */}
                      {item.type === 'detail-address' && (
                        <View className="textarea">
                        <Textarea
                          className="form-item-content form-detail-address"
                          disableDefaultPadding
                          autoHeight
                          placeholder="请输入详细地址"
                          onInput={_fixme_with_dataset_(this.inputValue, { index: index })}
                        ></Textarea>
                        </View>
                      )}

                      {item.type === 'comment' && (
                        <View className="comment">
                          <Text>{item.value}</Text>
                        </View>
                      )}
                    </View>
                  );
                })}
            </View>
          </View>
        </Form>
        <View className="row-box" onClick={this.handleGoToCouponList}>
          <View className="row-label">优惠劵</View>
          {curCoupon && curCoupon.name ? (
            <View className="right-text">
              {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
              )}

              {'减￥' + filters.moneyFilter(price.couponPrice, true)}
            </View>
          ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
            <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
          ) : (
            <View className="right-text">无可用</View>
          )}

          <Image
            className="right-icon"
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
          ></Image>
        </View>
        {/*  优惠信息  */}
        <View className="shipping-method">
          {price.fullDecrementPromFee && (
            <View className="row-box">
              <View className="row-label">满减</View>
              <View className="right-text">{'减￥' + filters.moneyFilter(price.fullDecrementPromFee, true)}</View>
            </View>
          )}

          {payDetails &&
            payDetails.map((item, index) => {
              return (
                <Block key={item.index}>
                  {item.isCustom ? (
                    <View className="row-box">
                      <View className="row-label">{item.label}</View>
                      <View className="right-text">{item.value}</View>
                    </View>
                  ) : (
                    <Block>
                      {item.name === 'gift-card' && (
                        <View className="row-box" onClick={this.handleGoToGiftCardList}>
                          <View className="row-label">礼品卡</View>
                          {curGiftCard && curGiftCard.cardItemName ? (
                            <View className="right-text">
                              {'抵扣￥' + filters.moneyFilter(price.giftCardPrice, true)}
                            </View>
                          ) : coupons && coupons.userGiftCardDTOSet && coupons.userGiftCardDTOSet.length ? (
                            <View className="right-text">{'可用' + coupons.userGiftCardDTOSet.length + '张'}</View>
                          ) : (
                            <View className="right-text">无可用</View>
                          )}

                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'coupon' && !seckillNo && (
                        <View className="row-box" onClick={this.handleGoToCouponList}>
                          <View className="row-label">优惠劵</View>
                          {curCoupon && curCoupon.name ? (
                            <View className="right-text">
                              {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                              )}

                              {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                            </View>
                          ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                            <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                          ) : (
                            <View className="right-text">无可用</View>
                          )}

                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'coupon' &&
                        useLogisticsCoupon == 1 &&
                        seckillNo &&
                        choosedDelivery !== deliveryWay.SELF_DELIVERY.value && (
                          <View className="row-box" onClick={this.handleGoToCouponList}>
                            <View className="row-label">运费券</View>
                            {curCoupon && curCoupon.name ? (
                              <View className="right-text">
                                {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                  <Text>{filters.discountFilter(curCoupon.discountFee, true) + '折'}</Text>
                                )}

                                {'减￥' + filters.moneyFilter(price.couponPrice, true)}
                              </View>
                            ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                              <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                            ) : (
                              <View className="right-text">无可用</View>
                            )}

                            <Image
                              className="right-icon"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                            ></Image>
                          </View>
                        )}

                      {item.name === 'card' && (
                        <View className="row-box" onClick={this.handleGoToCard}>
                          <View className="row-label">次数卡</View>
                          <View className="right-text">{serveCard.cardItemName}</View>
                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'redpacke' && (
                        <View className="row-box" onClick={this.handleRedPackeList}>
                          <View className="row-label">红包</View>
                          {curRedPacke && curRedPacke.code ? (
                            <View className="right-text">
                              {'减￥' + filters.moneyFilter(price.redPacketPrice, true)}
                            </View>
                          ) : coupons && coupons.redPacketSet && coupons.redPacketSet.length ? (
                            <View className="right-text">{'可用' + coupons.redPacketSet.length + '个'}</View>
                          ) : (
                            <View className="right-text">无可用</View>
                          )}

                          <Image
                            className="right-icon"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                          ></Image>
                        </View>
                      )}

                      {item.name === 'discount' && (
                        <View className="row-box">
                          <View className="row-label">会员折扣</View>
                          <View className="right-text">
                            {(coupons.memberDiscount && coupons.memberDiscount < 100
                              ? coupons.memberDiscount / 10 + '折'
                              : '无') +
                              '\r\n            减￥' +
                              filters.moneyFilter(price.memberPrice, true)}
                          </View>
                        </View>
                      )}

                      {item.name === 'integral' && (
                        <View className="row-box">
                          <View className="row-label">{scoreName + '抵扣'}</View>
                          <View className="right-text">
                            {goodsIntegral + scoreName + '减￥' + filters.moneyFilter(price.integralPromFee, true)}
                          </View>
                        </View>
                      )}

                      {item.name === 'innerBuy' && (
                        <View className="row-box">
                          <View className="row-label">内购券</View>
                          <View className="right-text">
                            {goodsInternal + '张减￥' + filters.moneyFilter(price.innerBuyPromFee, true)}
                          </View>
                        </View>
                      )}

                      {item.name === 'seckill' && (
                        <View className="row-box">
                          <View className="row-label">秒杀优惠</View>
                          <View className="right-text">{'减￥' + filters.moneyFilter(price.secKillPromFee, true)}</View>
                        </View>
                      )}
                    </Block>
                  )}
                </Block>
              );
            })}
          {price.groupPromFee && (
            <View className="row-box">
              <View className="row-label">拼团优惠</View>
              <View className="right-text">{'-¥ ' + filters.moneyFilter(price.groupPromFee, true)}</View>
            </View>
          )}

          {!!isCardpackType && (
            <Block>
              <View className="row-box">
                <View className="row-label">卡包明细</View>
              </View>
              {goodsInfoList[0].wxItem.couponInfos &&
                goodsInfoList[0].wxItem.couponInfos.map((item, index) => {
                  return (
                    <Block key={item.index}>
                      <View className="row-box card-pack-box">
                        <View className="label">{item.name}</View>
                        <View className="right-text">{(item.couponNum || 0) + '张'}</View>
                      </View>
                    </Block>
                  );
                })}
            </Block>
          )}

          <Block>
            {isEstateType ? (
              <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                <View className="row-label">认筹金</View>
                <View className="right-text">
                  {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                </View>
              </View>
            ) : (
              <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                <View className="row-label">商品原价</View>
                {/*  <View wx:if="{{goodsInfoList[0].seckillPrice > (price.totalPrice||goodsTotalPrice)}}" class="right-text">¥ {{filters.moneyFilter(goodsInfoList[0].seckillPrice,true)}}</View>  */}
                <View className="right-text">
                  {'¥ ' + filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)}
                </View>
              </View>
            )}
          </Block>
          <Block>
            {payDetails &&
              payDetails.map((item, index) => {
                return (
                  <View className="row-box" key={item.index}>
                    <View className="row-label">运费</View>
                    {price.freight && price.freight > 0 ? (
                      <View className="right-text">{'￥' + filters.moneyFilter(price.freight, true)}</View>
                    ) : (
                      <View className="right-text">{allFreightCollect ? '到付' : '包邮'}</View>
                    )}
                  </View>
                );
              })}
          </Block>

          <View className="row-box">
            <View className="row-label">配送费</View>
            {price.freight && price.freight > 0 ? (
              <View className="right-text">{'￥' + filters.moneyFilter(price.freight, true)}</View>
            ) : (
              <View className="right-text">免配送费</View>
            )}
          </View>

          <View className="row-box">
            <View className="row-label">共优惠</View>
            <View className="right-text">
              {'- ¥ ' +
                (isGiftActivity
                  ? filters.moneyFilter(price.totalPrice || goodsTotalPrice, true)
                  : filters.moneyFilter(price.totalFreePrice || 0, true) < 0
                  ? 0
                  : filters.moneyFilter(price.totalFreePrice || 0, true))}
            </View>
          </View>
          {price && isFrontMoneyItem && (
            <View className="row-box">
              <View className="row-label">尾款</View>
              <View className="right-text">{'¥ ' + filters.moneyFilter(price.restMoney, true)}</View>
            </View>
          )}
        </View>
        {/*  支付按钮  */}
        <View className={'pay-box ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {totalPrice ? (
            <View className="total-price">
              <Text className="price-title">合计：</Text>
              <Text className="price-logo">¥</Text>
              <Text className="price-num">{filters.moneyFilter(price.needPayPrice, true)}</Text>
              {!!deposit && <Text className="deposit">（含押金）</Text>}
            </View>
          ) : ''}

          <Button
            className="to-pay-btn"
            style={_safe_style_('background:' + (isError ? '#cccccc' : tmpStyle.btnColor))}
            onClick={this.handlePay}
            disabled={payLoading}
          >
            {totalPrice ? '确定支付' : '确定提交'}
          </Button>
        </View>
        {/*  选择优惠对话框  */}
        <BottomDialog id="choose-card" customClass="bottom-dialog-custom-class" ref={this.refChooseCardDialogCOMP}>
          {isCoupon === 4 && (
            <ScrollView
              scrollY
              className="coupon-select">
              {coupons.userGiftCardDTOSet &&
                coupons.userGiftCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name limit-line">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用礼品卡
              </View>
            </ScrollView>
          )}

          {/*  优惠券优惠类型  */}
          {isCoupon === 1 && (
            <ScrollView
              scrollY
              className="coupon-select">
              {coupons.couponSet &&
                coupons.couponSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab couponSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-img-box">
                        {!item.status ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gray.png"
                          ></Image>
                        ) : item.couponCategory === 1 ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-blue.png"
                          ></Image>
                        ) : (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gren.png"
                          ></Image>
                        )}
                      </View>
                      <View className="coupon-tab-left">
                        {item.discountFee === 0 ? (
                          <View className="coupon-freeFreight">免运费</View>
                        ) : (
                          <View className="coupon-price">
                            {(item.couponCategory === couponEnum.TYPE.freight.value ||
                              item.couponCategory === couponEnum.TYPE.fullReduced.value) && (
                              <Block>
                                <Text className="price-logo">￥</Text>
                                <Text className="price-num">{filters.moneyFilter(item.discountFee, true)}</Text>
                              </Block>
                            )}

                            {item.couponCategory === couponEnum.TYPE.discount.value && (
                              <Block>
                                <Text className="price-num">{filters.discountFilter(item.discountFee, true)}</Text>
                                <Text className="price-logo">折</Text>
                              </Block>
                            )}
                          </View>
                        )}

                        {item.minimumFee === 0 ? (
                          <View className="coupon-minimumFee">无门槛</View>
                        ) : (
                          <View className="coupon-minimumFee">
                            {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                          </View>
                        )}

                        {item.couponCategory === 1 && <View className="coupon-type">运费券</View>}

                        {item.couponCategory === 2 && <View className="coupon-type">折扣券</View>}

                        {item.couponCategory === 0 && <View className="coupon-type">满减券</View>}
                      </View>
                      <View className="coupon-tab-right">
                        <View className="coupon-name limit-line">{item.name}</View>
                        {item.endTime && (
                          <View className="coupon-validityDate">{'有效期：' + filters.dateFormat(item.endTime)}</View>
                        )}
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </ScrollView>
          )}

          {/*  红包优惠类型  */}
          {isCoupon === 2 && (
            <ScrollView
              scrollY
              className="coupon-select">
              {coupons.redPacketSet &&
                coupons.redPacketSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab redPacketSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        {item.thresholdFee === 0 ? (
                          <View className="coupon-name limit-line">无门槛</View>
                        ) : (
                          <View className="coupon-name limit-line">
                            {'满' + filters.moneyFilter(item.thresholdFee, true) + '可用'}
                          </View>
                        )}

                        {item.endTime && (
                          <View className="coupon-validityDate">
                            {'有效期：' + filters.dateFormat(item.endTime, 'yyyy-MM-dd hh:mm:ss')}
                          </View>
                        )}
                      </View>
                      <View className="coupon-tab-bottom">
                        金额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.luckMoneyFee, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用红包
              </View>
            </ScrollView>
          )}

          {/*  卡项优惠类型  */}
          {isCoupon === 3 && (
            <ScrollView
              scrollY
              className="coupon-select">
              {coupons.userCardDTOSet &&
                coupons.userCardDTOSet.map((item, index) => {
                  return (
                    <View
                      className="coupon-tab card-tab"
                      key={item.id}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-tab-top">
                        <Text className="coupon-name">{item.cardItemName}</Text>
                        <Text className="coupon-validityDate">
                          {'有效期：' + (item.validityDate ? item.validityDate : '永久')}
                        </Text>
                      </View>
                      <View className="coupon-tab-bottom">
                        余额<Text className="price-logo">￥</Text>
                        <Text className="price-num">{filters.moneyFilter(item.balance, true)}</Text>
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </ScrollView>
          )}
        </BottomDialog>
        {
          isShowTips ? <View className="tips">
            {
              tipsMsgs.map((item,index)=>{
                return (
                  <Text className="tips-text" key={index}>
                    {item}
                  </Text>
                )
              })
            }

          </View> : ''
        }

      </View>
    );
  }
}

export default GoodsRentalPendingpayMerge;
