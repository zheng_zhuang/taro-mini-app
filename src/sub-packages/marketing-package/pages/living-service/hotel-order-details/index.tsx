import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Swiper, SwiperItem, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import industryEnum from '../../../../../wxat-common/constants/industryEnum.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import state from '../../../../../state/index.js';
import template from '../../../../../wxat-common/utils/template.js';
import checkOptions from '../../../../../wxat-common/utils/check-options.js';
import CustomerHeader from '../../../../../wxat-common/components/customer-header/index';
import BuyNow from '../../../../../wxat-common/components/buy-now/index';
import DetailParser from '../../../../../wxat-common/components/detail-parser/index';
import Empty from '@/wxat-common/components/empty/empty'
import { connect } from 'react-redux';
import './index.scss';
const app = Taro.getApp();

// @withWeapp({
//   /**
//    * 监听器
//    */
//   on: {
//     async stateChange(data) {
//       // 是否已经登录，存在sessionId
//       if (data && data.key === 'sessionId') {
//         let formattedOptions = await checkOptions.checkOnLoadOptions(this.data._optionsData);

//         if (!!formattedOptions) {
//           this.init(formattedOptions);
//         }
//       }
//     },
//   },
// })

const mapStateToProps = ({ globalData }) => {
  return {
    globalData
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class HotelOrderDetails extends React.Component {

  state = {
    tmpStyle: {}, // 主题模板配置
    industryEnum, // 行业类型常量
    industry: this.props.globalData.industry, // 行业类型
    id: null, // 商品单号
    sourceType: null, //服务类型
    goodsImages: [], // 商品图片
    serviceDetail: [], // 服务商品详情
    dataLoad: false,
    sessionId: null,
    error: false,
    isExpand: false,
    extraData: null,
    _optionsData: null,
    _storeId: null,
    formExt: [], //表单提交
    navOpacity: 0,
    navBgColor: null,
    storeId: null,
  }

  /**
  * 生命周期函数--监听页面加载
  */
  onLoad(options) {
    this.setState(
      {
        formExt: options.formExt,
        _optionsData: options
      }
    )

    checkOptions.checkOnLoadOptions(options).then(res => {
      this.init(res)
    })

    this.setState({
      navBgColor: this.props.globalData.tabbars ? this.props.globalData.tabbars.list[0].navBackgroundColor : {},
      storeId: options.storeId,
    });
  }

  init(options) {
    let id = options.id;
    let sourceType = options.sourceType;
    let inTime = options.inTime;
    let outTime = options.outTime;
    let DaysBetween = options.DaysBetween;
    this.setState(
      {
        inTime,
        outTime,
        id,
        sourceType,
        DaysBetween
      },
      () => {
        this.initRenderData(); // 初始化渲染的数据
      }
    )

    
  }

  initRenderData() {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({
      dataLoad: true,
    });

    this.getTemplateStyle(); // 获取主题模板配置
    this.getServiceDetail(); // 获取商品详情
  }

  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }
  
  onChangeStore(storeId) {
    return wxApi
      .request({
        url: api.store.choose_new + '?storeId=' + storeId,
        loading: true,
      })
      .then((res) => {
        const store = res.data;
        if (!!store) {
          if (state.base.appInfo.canCustomDecorate) {
            app
              .getMultiStorePageConfig(store.id)
              .then((res) => {
                this.props.globalData.homeConfig = JSON.parse(res.homeConfig);
                state.base.homePageConfig = JSON.parse(res.homeConfig);
                const xConfig = JSON.parse(res.config);
                console.log(xConfig, 'xConfig-----------------------1')
                this.props.globalData.mineConfig = xConfig.mineConfig && xConfig.mineConfig.value;
                state.base.mineConfig = xConfig.mineConfig && xConfig.mineConfig.value;
                state.store.updateCurrentStore(store, true);
              })
              .catch((error) => {});
            return;
          }
          state.store.updateCurrentStore(store, true);
        }
      });
  }

  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel = () => {
    this.getShareDialog().show();
  }

  getServiceDetail = () => {
    wxApi
      .request({
        url: api.livingService.getPlatformDetails,
        data: {
          id: this.state.id,
          sourceType: this.state.sourceType,
        },
      })
      .then((res) => {
        let goodsImages = [];
        if (!!res.data.img) {
          goodsImages.push(...res.data.img.split(','));
        }
        const serviceDetail = res.data || [];
        var formExt = [];
        if (serviceDetail.formExt) {
          formExt = JSON.parse(res.data.formExt).widgetValue;
        }
        this.setState({
          goodsImages,
          serviceDetail,
          formExt,
        });
      })
      .catch(() => {
        this.apiError();
      });
  }

  handleBuyNow = () => {
    let itemNo = this.state.serviceDetail.itemNo;
    let id = this.state.id;
    const serviceName = this.state.serviceDetail.serverName;
    const salePrice = this.state.serviceDetail.salePrice;
    const deposit = this.state.serviceDetail.deposit;
    const inTime = this.state.inTime;
    const outTime = this.state.outTime;
    const DaysBetween = this.state.DaysBetween;
    const storeId = this.state.storeId;
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/living-service/hotel-list-pendingPay/index',
      data: {
        sourceType: this.state.sourceType,
        itemNo,
        id,
        salePrice,
        deposit,
        serviceName,
        inTime,
        outTime,
        DaysBetween,
        storeId,
        formExt: this.state.formExt
      },
    });
  }

  apiError = () => {
    this.setState({
      error: true,
    })
  }

  previewImg = (e) => {
    const currentUrl = e.currentTarget.dataset.currenturl;
    const previewUrls = this.state.goodsImages;
    Taro.previewImage({
      current: currentUrl,
      urls: previewUrls,
    })
  }

  /**
  * 页面滚动
  * @param {*} ev
  */
  onPageScroll(e) {
    this.setState({
      navOpacity: e.scrollTop / 50,
    })
  }

  render() {
    const { error, serviceDetail, navOpacity, navBgColor, goodsImages, tmpStyle, dataLoad } = this.state
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mplh-HotelOrderDetails"
        className="wk-mplh-HotelOrderDetails"
      >
        {!!error && serviceDetail.length === 0 && <Empty message="暂无服务记录"></Empty>}

        {!!error && <Error></Error>}
        <CustomerHeader navOpacity={navOpacity} title="服务订单详情" bgColor={navBgColor}></CustomerHeader>
        {!error && (
          <View className="cards-detail-container">
            {goodsImages.length > 0 && (
              <Swiper>
                {goodsImages &&
                  goodsImages.map((item, index) => {
                    return (
                      <Block key={'cards-item-' + index}>
                        <SwiperItem>
                          {item.length > 0 && (
                            <View
                              className="cards-image"
                              style={_safe_style_(
                                'background: transparent url(' + item + ') no-repeat 50% 50%;background-size: cover;'
                              )}
                              onClick={_fixme_with_dataset_(this.previewImg, { currenturl: item })}
                            ></View>
                          )}
                        </SwiperItem>
                      </Block>
                    );
                  })}
              </Swiper>
            )}

            <View className="sale-info">
              <View className="sale-order-info">
                <Text className="carouseltext-title">{serviceDetail.serverName}</Text>
              </View>
              <View className="sale-order-info">
                <Text className="carouseltext-text">{serviceDetail.serverDesc}</Text>
              </View>
              {serviceDetail.salePrice && (
                <View className="sale-price-info">
                  <View className="price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    <Text className="price-symbol">￥</Text>
                    {filters.moneyFilter(serviceDetail.salePrice, true)}
                  </View>
                </View>
              )}

              {serviceDetail.originalPrice && (
                <View className="label-price">
                  <Text>￥</Text>
                  {filters.moneyFilter(serviceDetail.originalPrice, true)}
                </View>
              )}
            </View>
            <DetailParser headerLabel="房型描述" itemDescribe={serviceDetail.serverInfo}></DetailParser>
          </View>
        )}

        {dataLoad && (
          <Block>
            <BuyNow
              style="z-index: 55;"
              showShare={true}
              operationText="立即提交"
              defaultText="立即提交"
              onBuyNow={this.handleBuyNow}
              immediateShare={true}
              onShare={this.handleSelectChanel}
              showCart={false}
            ></BuyNow>
          </Block>
        )}
      </View>
    );
  }
}

export default HotelOrderDetails;
