import {_safe_style_, $getRouter} from 'wk-taro-platform';
import {View} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import template from '@/wxat-common/utils/template';
import store from '@/store';
import './index.scss';

const storeData = store.getState();

class ScanCode extends React.Component {
  state = {
    singleService: false,
    tmpStyle: {},
    isDetail: 1,
    categoryType: 1,
    isDistributionServices: '',
    sourceType: '',
    id: 1,
    itemNo: 1,
    serviceDetail: {},
    roomCode: '',
    trueCode: ''
  }

  componentDidMount() {
    const options = this.$router.params
    console.log(options);
    this.setState({
      isDetail: options.isDetail,
      categoryType: options.categoryType,
      isDistributionServices: options.isDistributionServices,
      sourceType: options.sourceType, // 单个住中服务传的参数
      id: options.id, // 单个住中服务传的参数
      singleService: options.singleService ? options.singleService : false, // 从单个住中服务下单
      itemNo: options.itemNo, // 单个住中服务
    });
    this.getTemplateStyle();
  }

  $router = $getRouter();

  // 获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  onScanCode = () => {
    // if (process.env.WX_OA === 'true') {
    //   // 微信公众号
    //   wx.scanQRCode({
    //     needResult: 1,
    //     success: (res) => {
    //       console.log(res)
    //     },
    //     fail: (err => {
    //       console.log(err)
    //     }),
    //     complete: (res => {
    //       console.log(res)
    //     })
    //   })
    // } else {
    Taro.scanCode({
      // needResult:1,
      success: (res) => {
        console.log(123123, res);
        if (!res.path) {
          return wxApi.showToast({
            title: '无法识别房间信息',
            icon: 'none',
          });
        }
        let path: any = decodeURIComponent(res.path);
        // 取url后面的参数 无需关注
        // let vars = res.path.split('=');
        // vars.splice(0, 1);
        // vars = vars.join('=');
        // const query = vars.split('&');
        // const params: any = {};
        // for (let i = 0; i < query.length; i++) {
        //   const q = query[i].split('=');
        //   if (q.length === 2) {
        //     params[q[0]] = q[1];
        //   }
        // }

        path = path.substring(path.lastIndexOf('?') + 1)
        path = path.split('&')
        const params = {}
        for (let i = 0; i < path.length; i++) {
          const val = path[i].split('=')
          if (val.length > 2) {
            params[val[1]] = val[2]
          } else {
            params[val[0]] = val[1]
          }
        }
        if (params.id) {
          wxApi
            .request({
              url: api.queryQrCodeScenes,
              isLoginRequest: true,
              method: 'GET',
              data: {
                id: params.id,
              },
            })
            .then(({data}) => {
              const dataObj: any = {};
              data.split('&').forEach((item) => {
                const key = item.split('=')[0];
                const value = item.split('=')[1];
                dataObj[key] = value;
              });
              return wxApi.request({
                url: api.selectQrCodeNumber,
                isLoginRequest: true,
                method: 'POST',
                data: {
                  id: params.id,
                  hotelId: dataObj.pId,
                  qrCodeNumber: params.qrcodeNo,
                },
              });
            })
            .then(({data: {qrCodeNumber, roomCode, roomNumber, qrCodeTypeName, qrCodeTypeId, storeId}}) => {
              console.log('qrCodeTypeId', qrCodeTypeId);
              console.log('params', params);
              if (storeData.base.userInfo.storeId !== storeId) {
                wxApi.showToast({
                  title: '小程序门店与二维码门店不匹配',
                  icon: 'none'
                });
                return
              }
              if (!roomCode && (params.tp == 1 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? `${qrCodeTypeName}-${roomNumber}`
                      : `${qrCodeTypeName}-${params.id ? roomCode : qrCodeNumber}`
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
              }, () => {
                if (this.state.isDetail == 1) {
                  wxApi.$navigateTo({
                    url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
                    data: {
                      isDetail: this.state.isDetail,
                      serviceDetail: this.state.serviceDetail,
                      roomCode: this.state.roomCode,
                      roomCodeTypeId: qrCodeTypeId,
                      trueCode: this.state.trueCode,
                    },
                  });
                } else if (this.state.categoryType == 5 || this.state.isDistributionServices === 'true') {
                  if (this.state.categoryType == 5) {
                    wxApi.$navigateTo({
                      url: '/sub-packages/order-package/pages/pay-order/index',
                      data: {
                        roomCode: this.state.roomCode,
                        qrCodeTypeId: qrCodeTypeId,
                        roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                        trueCode: this.state.trueCode,
                        categoryType: 5,
                        title: '确认订单',
                      },
                    });
                  } else {
                    wxApi.$navigateTo({
                      url: '/sub-packages/order-package/pages/pay-order/index',
                      data: {
                        roomCode: this.state.roomCode,
                        qrCodeTypeId: qrCodeTypeId,
                        roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                        trueCode: this.state.trueCode,
                        isDistributionServices: true,
                        categoryType: 5,
                        title: '确认订单',
                      },
                    });
                  }
                } else if (this.state.singleService) {
                  wxApi.$navigateTo({
                    url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay/index',
                    data: {
                      roomCode: this.state.roomCode,
                      roomCodeTypeId: qrCodeTypeId,
                      roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                      trueCode: this.state.trueCode,
                      sourceType: this.state.sourceType,
                      id: this.state.id,
                      serviceDetail: this.state.serviceDetail,
                      itemNo: this.state.itemNo,
                    },
                  });
                } else {
                  wxApi.$navigateTo({
                    url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
                    data: {
                      roomCode: this.state.roomCode,
                      roomCodeTypeId: qrCodeTypeId,
                      roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                      trueCode: this.state.trueCode,
                    },
                  });
                }
              });

            })
            .catch((err) => {
              console.log(err);
            });
        } else if (params.qrcodeNo) {
          wxApi
            .request({
              url: api.selectQrCodeNumber,
              isLoginRequest: true,
              method: 'POST',
              data: {
                id: params.id,
                hotelId: params.pId || params.hId || params.storeId,
                qrCodeNumber: params.qrcodeNo,
              },
            })
            .then(({data: {qrCodeNumber, roomCode, roomNumber, qrCodeTypeName, qrCodeTypeId}}) => {
              console.log('qrCodeTypeId', qrCodeTypeId);
              if ((!roomCode || !qrCodeNumber) && (params.tp == 1 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              if (!roomNumber && (params.tp == 2 || !params.tp)) {
                wxApi.showToast({
                  title: '该二维码没有绑定房间号',
                  icon: 'none',
                });

                return;
              }
              this.setState({
                trueCode:
                  roomCode || roomNumber
                    ? params.tp == 2
                      ? `${qrCodeTypeName}-${roomNumber}`
                      : `${qrCodeTypeName}-${params.id ? roomCode : qrCodeNumber}`
                    : '',
                roomCode: params.tp == 2 ? roomNumber : params.id ? roomCode : qrCodeNumber,
              });

              if (this.state.isDetail == 1) {
                wxApi.$navigateTo({
                  url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
                  data: {
                    isDetail: this.state.isDetail,
                    roomCode: this.state.roomCode,
                    roomCodeTypeId: qrCodeTypeId,
                    trueCode: this.state.trueCode,
                  },
                });
              } else if (this.state.categoryType == 5 || this.state.isDistributionServices === 'true') {
                if (this.state.categoryType == 5) {
                  wxApi.$navigateTo({
                    url: '/sub-packages/order-package/pages/pay-order/index',
                    data: {
                      roomCode: this.state.roomCode,
                      qrCodeTypeId: qrCodeTypeId,
                      roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                      trueCode: this.state.trueCode,
                      categoryType: 5,
                      title: '确认订单',
                    },
                  });
                } else {
                  wxApi.$navigateTo({
                    url: '/sub-packages/order-package/pages/pay-order/index',
                    data: {
                      roomCode: this.state.roomCode,
                      qrCodeTypeId: qrCodeTypeId,
                      roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                      trueCode: this.state.trueCode,
                      isDistributionServices: true,
                      categoryType: 5,
                      title: '确认订单',
                    },
                  });
                }
              } else if (this.state.singleService) {
                wxApi.$navigateTo({
                  url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay/index',
                  data: {
                    roomCode: this.state.roomCode,
                    roomCodeTypeId: qrCodeTypeId,
                    roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                    trueCode: this.state.trueCode,
                    sourceType: this.state.sourceType,
                    id: this.state.id,
                    itemNo: this.state.itemNo,
                  },
                });
              } else {
                wxApi.$navigateTo({
                  url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
                  data: {
                    roomCode: this.state.roomCode,
                    roomCodeTypeId: qrCodeTypeId,
                    roomCodeTypeName: storeData.globalData.roomCodeTypeName,
                    trueCode: this.state.trueCode,
                  },
                });
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }
      },
      fail: (err => {
        console.log(err)
      }),
      complete: (res => {
        console.log(res)
      })
    });
    // }
  }

  render() {
    const {tmpStyle}: any = this.state;
    return (
      <View data-scoped="wk-mpls-ScanCode" className="wk-mpls-ScanCode scan-code">
        <View
          onClick={this.onScanCode}
          className="image"
          style={_safe_style_(
            `background: transparent url('https://front-end-1302979015.file.myqcloud.com/images/fill.svg') no-repeat 50% 50%;
            background-size: cover;
            background-color: ${tmpStyle.btnColor};`
          )}
        ></View>
        <View className="tips">
          <View>请点击上方图标扫码</View>
          <View>确认您的配送位置</View>
        </View>
      </View>
    );
  }
}

export default ScanCode;
