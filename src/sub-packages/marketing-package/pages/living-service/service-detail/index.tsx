import {_fixme_with_dataset_, _safe_style_, $getRouter} from 'wk-taro-platform';
import {
  Block,
  View,
  Swiper,
  SwiperItem,
  Text,
  Form,
  Image,
  RadioGroup,
  Label,
  Radio,
  CheckboxGroup,
  Checkbox,
  Picker,
  Input,
  Textarea,
  RichText,
} from '@tarojs/components';
// import checkOptions from '@/wxat-common/utils/check-options.js';
// import Img from '../../img/close.png'
import React from 'react';
import Taro from '@tarojs/taro';
import store from '@/store';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
// import state from '@/state/index.js';
import {connect} from 'react-redux';
import template from '@/wxat-common/utils/template.js';
import CustomerHeader from '@/wxat-common/components/customer-header/index';
import DetailParser from '@/wxat-common/components/detail-parser';
import RegionPicker from '@/sub-packages/scene-package/components/region-picker/index'
import UploadImg from "@/wxat-common/components/upload-img";
import utils from '@/wxat-common/utils/util.js';
import {getFreeLabel} from '@/wxat-common/utils/service'
import ContactHousekeeper from '@/wxat-common/components/contact-housekeeper/index';
import './index.scss';

const app = Taro.getApp();
const storeData = store.getState();
const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};
const mapStateToProps = (state) => {
  return {
    currentStore: state.base.currentStore,
    roomCode: state.globalData.roomCode,
    roomCodeTypeId: state.globalData.roomCodeTypeId,
    roomCodeTypeName: state.globalData.roomCodeTypeName,
    trueCode: state.globalData.roomCodeTypeName + '-' + state.globalData.roomCode,
  };
};

@connect(mapStateToProps, undefined, undefined, {forwardRef: true})
class ServiceDetail extends React.Component {
  $router = $getRouter();

  state = {
    id: null,
    sourceType: null,
    phone: null,
    num: 1,
    navOpacity: 0,
    navBgColor: null,
    goodsImages: [],
    serviceDetail: {},
    tmpStyle: {},
    disable: false, //默认不禁止点击提交按钮
    isStock: true,  //是否有库存
    changeType: null, //门店设置类型,1:不配置 2:客服图片 3:客服对话,
    imgeUrl: null, //二维码还是链接
    dataTotal: null, //总数据
    postType: null, //等于2就弹框
  }


  async componentDidShow() {
    const options = this.$router.params
    wxApi.removeStorageSync('serviceDetail'); // 移除单个服务详情缓存
    console.log(storeData.base.sessionId, '-------------------------storeData.base.sessionId')
    if (!storeData.base.sessionId) {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/home/index',
      });

    }
    if (storeData.base.sessionId) {
      Taro.setStorageSync('sessionId', storeData.base.sessionId)
      // localStorage.setItem('sessionId',storeData.base.sessionId);
    } else {
      // const sessionIdData = localStorage.getItem('sessionId');
      const sessionIdData = Taro.getStorageSync('sessionId');
      console.log(sessionIdData, '------------------sessionIdData')
      if (sessionIdData) {
        this.setState({
          sessionId: sessionIdData,
        });
      }
    }
    this.setState({
      id: options.id,
      sourceType: options.sourceType,
      navBgColor: storeData.globalData.tabbars && storeData.globalData.tabbars.list && storeData.globalData.tabbars.list[0].navBackgroundColor ? storeData.globalData.tabbars.list[0].navBackgroundColor : '',
      num: 1
    }, () => {
      this.getServiceDetail();
    });
    this.getTemplateStyle();
    this.getType();
    wxApi.removeStorageSync('serviceDetail'); // 移除单个服务详情缓存
  }

  componentDidMount() {

  }

  //组件更新时被调用
  componentWillReceiveProps(nextProps) {
    console.log(nextProps, '------------nextProps');
    this.getTemplateStyle(); // 获取主题模板配置
  }

  // 获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
    console.log(this.state.tmpStyle, '-ooooooooooo---------------tmpStyle')
  }

  checkForm(dataList) {
    let error = '';
    dataList.forEach((item) => {
      if (error) return;
      if (item.required) {
        if (item.type == "uploadImage") {
          if (!(item.value && item.value.length > 0)) {
            error = item.label + '不能为空';
          }
        } else {
          if (!item.value) {
            error = item.label + '不能为空';
          }
        }
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });
    return error;
  }

  getType() {
    wxApi
      .request({
        url: api.livingService.getType,
      })
      .then((res) => {
        console.log(res, '---------res');
        this.setState({
          changeType: res.data.customerType || null,
          imgeUrl: res.data.imgUrl || null,
          dataTotal: res.data || null,
        });
      });
  }

  getServiceDetail() {
    wxApi
      .request({
        url: api.livingService.getPlatformDetails,
        data: {
          id: this.state.id,
          sourceType: 2,
          storeId: storeData.base.currentStore.id,
        },
      })
      .then((res) => {
        const goodsImages = [];
        if (res.data.img) {
          goodsImages.push(...res.data.img.split(','));
        }

        const serviceDetail = res.data || [];
        let isStock = true
        if (serviceDetail.useStock == 1 && !serviceDetail.itemStock) {
          isStock = false
        }

        let formExt = [];
        if (serviceDetail.formExt) {
          formExt = JSON.parse(res.data.formExt).widgetValue;
        }
        formExt.forEach((item, index) => {
          if (item.type === 'address') {
            formExt.splice(index + 1, 0, {
              label: '详细地址',
              type: 'detail-address',
              required: false,
              value: '',
            });
          }
          // 单选
          if (item.type === 'radio') {
            item.options = item.options.map((el) => {
              return {
                value: el,
                text: el,
                checked: false
              }
            })
          }
          // 多选
          if (item.type === 'checkbox') {
            item.options = item.options.map((el) => {
              return {
                value: el,
                checked: false,
              };
            });
          }
          // 图片上传
          if (item.type === 'uploadImage') {
            item.value = []
          }
        });
        // 获取服务器时间
        wxApi.request({
          url: api.livingService.getNowDate,
        }).then(timeRes => {
          if (timeRes.data) {
            let nowTime = new Date(timeRes.data)
            let disable = !utils.time_range(serviceDetail.serverStartTime, serviceDetail.serverEndTime, nowTime)
            this.setState({
              disable: disable
            })
          } else {
            this.setState({
              disable: true
            })
          }
        }).catch(err => {
          this.setState({
            disable: true
          })
        })
        this.setState({
          goodsImages,
          serviceDetail,
          formExt,
          isStock
        });
      })
      .catch((error) => {
        if (error.data.data) {
          const goodsImages = [];
          goodsImages.push(...error.data.data.img.split(','));
          const serviceDetail = error.data.data || [];
          this.setState({
            goodsImages,
            serviceDetail,
            disable: true,
          });
        }
      });
  }

  onSetPhone(e) {
    this.setState({
      phone: e.detail.value,
    });
  }

  onReduce = () => {
    if (this.state.num === 1) {
      return null;
    }
    const num = this.state.num - 1;
    this.setState({
      num: num,
    });
  }

  onAdd = () => {
    if (!this.state.isStock || (this.state.serviceDetail.useStock == 1 && this.state.serviceDetail.itemStock <= this.state.num)) {
      return
    }
    this.setState({
      num: this.state.num + 1,
    });
  }

  arrMap(arr) {
    const map = new Map();
    if (arr) {
      arr.forEach((item) => {
        const obj = JSON.parse(item);
        if (map.has(obj.id)) {
          const mapObj = map.get(obj.id)
          mapObj.count = mapObj.count + obj.count
          map.set(obj.id, mapObj)
        } else {
          map.set(obj.id, obj)
        }
      });
    }
    return map;
  }

  mapArr(map) {
    const arr = [];
    map.forEach((item) => {
      arr.push(JSON.stringify(item));
    });
    return arr;
  }

  judgeStock = (list) => {
    const arr = this.arrMap(list);
    if (arr.has(this.state.serviceDetail.id)) {
      const detail = arr.get(this.state.serviceDetail.id)
      detail.count = detail.count + this.state.num;

      if (detail.count > this.state.serviceDetail.itemStock) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  onAddJoinBooking = () => {
    if (!this.state.isStock) {
      wxApi.showToast({
        icon: "none",
        title: '库存不足'
      })
      return
    }
    const formExt = this.submiteCart();
    const error = this.checkForm(formExt);
    if (error) {
      wxApi.showToast({icon: 'none', title: error});
      return;
    }
    const serviceTreeData = wxApi.getStorageSync('serviceTreeData') || [];
    const widgetValue = JSON.parse(this.state.serviceDetail.formExt);
    widgetValue.widgetValue = formExt;
    this.state.serviceDetail.formExt = JSON.stringify(widgetValue);
    this.state.serviceDetail.widgetValue = formExt;

    //判断库存清单数据
    if (this.judgeStock(serviceTreeData) && this.state.serviceDetail.useStock == 1) {
      wxApi.showToast({
        icon: "none",
        title: '库存不足'
      })
      return
    }
    if (this.state.formExt && this.state.formExt.length > 0) {
      const detail = this.state.serviceDetail;
      detail.count = this.state.num;
      serviceTreeData.push(JSON.stringify(detail));
      wxApi.setStorageSync('serviceTreeData', serviceTreeData);
    } else {
      const arr = this.arrMap(serviceTreeData);
      let detail = {};
      if (arr.has(this.state.serviceDetail.id)) {
        detail = arr.get(this.state.serviceDetail.id);
        detail.count = detail.count + this.state.num;
      } else {
        detail = this.state.serviceDetail;
        detail.count = this.state.num;
        arr.set(detail.id, detail);
      }
      wxApi.setStorageSync('serviceTreeData', this.mapArr(arr));
    }

    const serviceOrderList = wxApi.getStorageSync('serviceOrderList') || [];
    if (this.state.formExt && this.state.formExt.length > 0) {
      const detail = this.state.serviceDetail;
      detail.count = this.state.num;
      serviceOrderList.push(JSON.stringify(detail));
      wxApi.setStorageSync('serviceOrderList', serviceOrderList);
    } else {
      const arr = this.arrMap(serviceOrderList);
      let detail = {};
      if (arr.has(this.state.serviceDetail.id)) {
        detail = arr.get(this.state.serviceDetail.id);
        detail.count = detail.count + this.state.num;
      } else {
        detail = this.state.serviceDetail;
        detail.count = this.state.num;
      }
      arr.set(detail.id, detail);
      wxApi.setStorageSync('serviceOrderList', this.mapArr(arr));
    }
    wxApi.showToast({
      title: '预购成功',
      icon: 'success',
      duration: 2000,
    });
  }

  onImmediately = () => {
    if (!this.state.isStock) {
      wxApi.showToast({
        icon: "none",
        title: '库存不足'
      })
      return
    }
    if (this.state.disable) {
      return
    }
    const formExt = this.submiteCart();
    const error = this.checkForm(formExt);
    if (error) {
      wxApi.showToast({icon: 'none', title: error});
      return;
    }
    const detail = Object.assign({}, this.state.serviceDetail);
    detail.formExt = formExt;
    detail.widgetValue = formExt;
    detail.count = this.state.num;
    const serviceDetail = JSON.stringify(detail);
    wxApi.setStorageSync('serviceDetail', serviceDetail);

    // 判断是否包含需要扫码的商品

    if (detail.scan == 0) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
        data: {
          isDetail: 1,
        },
      });

      return;
    }

    if (this.props.roomCode) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
        data: {
          isDetail: 1,
          //  serviceDetail:JSON.stringify(detail),
          roomCode: this.props.roomCode,
          roomCodeTypeId: this.props.roomCodeTypeId,
          trueCode: this.props.roomCodeTypeName ? this.props.roomCodeTypeName + '-' + this.props.roomCode : this.props.roomCode,
        },
      });
    } else {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
        data: {
          isDetail: 1
        },
      });
    }
  }

  chooseGender = ({
                    currentTarget: {
                      dataset: {value, index},
                    },
                  }) => {
    const formExt = this.state.formExt;
    formExt[index].value = value;
    this.setState({
      formExt: formExt,
    });
  }

  inputValue = (e) => {
    const index = e.currentTarget.dataset.index;
    const value = e.detail.value;
    const formExt = this.state.formExt;
    formExt[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({formExt});
  }

  onRegionPickerChange = (value, {index}) => {
    const formExt = this.state.formExt;
    formExt[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({formExt});
  }

  /**
   * 获取上传的图片
   * @param {*} e
   */
  handleImagesChange = (images: string[], uploadIndex: number) => {
    // 配置多个
    const formExt = this.state.formExt;
    formExt[uploadIndex].value = images;
    this.setState({
      formExt, // 图片上传列表
    });
  }

  submiteCart() {
    const {formExt} = this.state;
    // 地址栏数据处理
    const list = [];
    formExt.forEach((item, index) => {
      let value = item.value || '';
      if (item.type === 'address') {
        const isNotEmptyArray = !!value && !!value.push;
        if (isNotEmptyArray) {
          if (value[0] === value[1]) {
            value = value.concat().splice(1).join('');
          } else {
            value = value.join('');
          }
        }
        value += formExt[index + 1] ? formExt[index + 1].value : '';
        list.push({
          type: item.type,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: value || '',
        });
      } else if (item.type == 'checkbox') {
        // 初始化options数据格式
        const arr = [];
        let keyName = [];
        keyName = item.options
          .filter((el) => {
            return el.checked;
          })
          .map((ele) => {
            return ele.value;
          });
        list.push({
          type: item.type,
          options: item.options,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: keyName.join(',') || '',
        });
      } else {
        list.push({
          type: item.type,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: item.value ? item.value : '',
        });
      }
    });
    return list;
  }

  // 单选修改
  changeRadioValue = (e) => {
    const items = e.target.dataset.item.options
    const value = e.detail.value
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false
      if (items[i].value === value) {
        items[i].checked = true
      }
    }
    const formExt: any = this.state.formExt
    const index = e.target.dataset.index
    formExt[index].options = items
    formExt[index].value = value
    this.setState({
      formExt,
    })
  }

  closeMe = () => {
    this.setState({
      postType: null,
    });
  }

  handleGetType = (value) => {
    console.log(value, '-------value');
    this.setState({
      postType: value
    });
  }

  // 多选修改
  bindValue = (e) => {
    const items = e.target.dataset.item.options;
    const values = e.detail.value;
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false;
      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].value === values[j]) {
          items[i].checked = true;
          break;
        }
      }
    }
    const formExt: any = this.state.formExt;
    const index = e.target.dataset.index;
    formExt[index].options = items;
    this.setState({
      formExt,
    });
  }

  /**
   * 页面滚动
   * @param {*} ev
   */
  onPageScroll(e) {
    this.setState({
      navOpacity: e.scrollTop / 50,
    });
  }

  render() {
    const {
      navOpacity,
      navBgColor,
      goodsImages,
      serviceDetail,
      tmpStyle,
      error,
      formExt,
      num,
      disable,
      isStock,
      dataTotal,
      changeType,
      postType
    } = this.state;
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mpls-ServiceDetail"
        className="wk-mpls-ServiceDetail"
      >
        <CustomerHeader navOpacity={navOpacity} title="服务详情" bgColor={navBgColor}></CustomerHeader>
        <View className="service-detail">
          <View className="container">
            {!error && (
              <View className="cards-detail-container">
                {(!!goodsImages && goodsImages.length > 0) && (
                  <Swiper indicatorDots={goodsImages.length > 1}>
                    {(!!goodsImages && goodsImages.length > 0) &&
                      goodsImages.map((item, index) => {
                        return (
                          <Block key={item.unique}>
                            <SwiperItem>
                              {item.length > 0 && (
                                <View
                                  className="cards-image"
                                  style={_safe_style_(
                                    'background: transparent url(' +
                                    item +
                                    ') no-repeat 50% 50%;background-size: cover;'
                                  )}
                                  onClick={_fixme_with_dataset_(this.previewImg, {currenturl: item})}
                                ></View>
                              )}
                            </SwiperItem>
                          </Block>
                        );
                      })}
                  </Swiper>
                )}

                <View className="sale-info">
                  <View className="sale-order-info">
                    <Text className="carouseltext-title">{serviceDetail.serverName}</Text>
                    <Text
                      className="serve-time">服务时段：{serviceDetail.serverStartTime || ''}-{serviceDetail.serverEndTime || ''}</Text>
                  </View>
                  <View className="sale-order-info">
                    <Text className="carouseltext-text">{serviceDetail.serverDesc}</Text>
                  </View>
                  <View className="sale-price-info">
                    <View className="price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      {serviceDetail.needPayFee == 1 ? <Text className="price-symbol">￥</Text> : ''}

                      {serviceDetail.needPayFee == 1 ? serviceDetail.salePrice / 100 : getFreeLabel(serviceDetail.showType)}
                    </View>
                  </View>
                  {serviceDetail.originalPrice ? (
                    <View className="label-price">
                      <Text>￥</Text>
                      {serviceDetail.originalPrice / 100}
                    </View>
                  ) : ''}
                </View>
                <DetailParser headerLabel="服务描述" itemDescribe={serviceDetail.serverDesc}></DetailParser>
              </View>
            )}

            {/*     <View class="add-service"> */}
            {/*       <View class="add-service-title">添加服务</View> */}
            {/*       <View class="contact-number">联系人电话</View> */}
            {/*       <input bindinput="onSetPhone" class="phone-input" placeholder="请输入"></input> */}
            {/*       <View class="service-num"> */}
            {/*         <text class="num-title">数量</text> */}
            {/*         <View class="operation-box"> */}
            {/*           <text bindtap="onReduce">-</text> */}
            {/*           <text class="num">{{num}}</text> */}
            {/*           <text bindtap="onAdd">+</text> */}
            {/*         </View> */}
            {/*       </View> */}
            {/*     </View> */}
            <Form>
              <View className="add-service">
                {!!formExt && formExt.length > 0 && (
                  <View className="white-box">
                    {/* <View className="add-service-title">添加服务</View> */}
                    {!!formExt &&
                      formExt.map((item, index) => {
                        return (
                          <View
                            className={'form-item ' + item.type + '-item ' + (item.required ? 'required' : '')}
                            key={item.uuid}
                          >
                            {item.type !== 'comment' && (
                              <View>
                                <View className="form-item-ipt">
                                  {item.type === 'sex' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'sex' && (
                                    <View className="phone-input form-item-content form-sex">
                                      <View
                                        onClick={_fixme_with_dataset_(this.chooseGender, {value: '男', index: index})}
                                        className={item.value === '男' ? 'selected' : ''}
                                      >
                                        <Image
                                          mode="aspectFit"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/male.png"
                                        ></Image>
                                        <View>男</View>
                                      </View>
                                      <View
                                        onClick={_fixme_with_dataset_(this.chooseGender, {value: '女', index: index})}
                                        className={item.value === '女' ? 'selected' : ''}
                                      >
                                        <Image
                                          mode="aspectFit"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/female.png"
                                        ></Image>
                                        <View>女</View>
                                      </View>
                                    </View>
                                  )}

                                  {/*                 &lt;!&ndash; 单选框 &ndash;&gt; */}
                                  {item.type === 'radio' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'radio' && (
                                    <RadioGroup
                                      className="radio-box"
                                      name={'radio-group' + item.uuid}
                                      onChange={_fixme_with_dataset_(this.changeRadioValue, {index: index, item: item})}
                                    >
                                      {!!item.options &&
                                        item.options.map((optionItem, index) => {
                                          return (
                                            <Label
                                              className="radio-box-item"
                                              style={_safe_style_(
                                                `background: ${optionItem.checked === true ? tmpStyle.btnColor : '#fff'};
                                              color:${optionItem.checked === true ? '#fff' : '#343434'};
                                              border-color:${optionItem.checked === true ? tmpStyle.btnColor : '#979797'}`
                                              )}
                                              key={'radio-item' + item.uuid + index}
                                              for={'radio-item' + item.uuid + index}>
                                              <Radio
                                                value={optionItem.value}
                                                checked={optionItem.checked}
                                                color={tmpStyle.btnColor}>
                                                {optionItem.value}
                                              </Radio>
                                            </Label>
                                          );
                                        })}
                                    </RadioGroup>
                                  )}

                                  {/*                 &lt;!&ndash; 多选框 &ndash;&gt; */}
                                  {item.type === 'checkbox' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'checkbox' && (
                                    <CheckboxGroup
                                      className="checkout-box"
                                      name="radio-group"
                                      onChange={_fixme_with_dataset_(this.bindValue, {index: index, item: item})}
                                    >
                                      {!!item.options &&
                                        item.options.map((child, index) => {
                                          return (
                                            <Label
                                              className="checkout-box-item"
                                              style={_safe_style_(
                                                `background: ${child.checked === true ? tmpStyle.btnColor : '#fff'};
                                                color:${child.checked === true ? '#fff' : '#343434'};
                                                border-color:${child.checked === true ? tmpStyle.btnColor : '#979797'}`
                                              )}
                                              key={child.index}>
                                              <Checkbox value={child.value} checked={child.checked}></Checkbox>
                                              {child.value}
                                            </Label>
                                          );
                                        })}
                                    </CheckboxGroup>
                                  )}

                                  {/*                 &lt;!&ndash; 时间选择器 &ndash;&gt; */}
                                  {item.type === 'date' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'date' && (
                                    <Picker
                                      className="phone-input"
                                      mode="date"
                                      start={item.dateRange ? item.dateRange[0] : ''}
                                      end={item.dateRange ? item.dateRange[1] : ''}
                                      onChange={_fixme_with_dataset_(this.inputValue, {index: index})}
                                    >
                                      {item.value ? (
                                        <Text className="picker">{item.value}</Text>
                                      ) : (
                                        <Text className="picker" style={_safe_style_('color: #888;')}>
                                          请选择
                                        </Text>
                                      )}

                                      <Image
                                        className="picker-icon"
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                      ></Image>
                                    </Picker>
                                  )}

                                  {/*                 &lt;!&ndash; 文本类型单行输入框 &ndash;&gt; */}
                                  {(item.type === 'text' || item.type === 'email') && (
                                    <View className="contact-number">{item.label}</View>
                                  )}

                                  {(item.type === 'text' || item.type === 'email') && (
                                    <Input
                                      className="phone-input"
                                      placeholder="请输入"
                                      value={item.value}
                                      onBlur={_fixme_with_dataset_(this.inputValue, {index: index})}
                                    ></Input>
                                  )}

                                  {/*                 &lt;!&ndash; 数字类型单行输入框 &ndash;&gt; */}
                                  {item.type === 'phone' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'phone' && (
                                    <Input
                                      className="phone-input"
                                      placeholder="请输入"
                                      type="phone"
                                      value={item.value}
                                      onBlur={_fixme_with_dataset_(this.inputValue, {index: index})}
                                    ></Input>
                                  )}

                                  {/*                 &lt;!&ndash; 多行输入框 &ndash;&gt; */}
                                  {item.type === 'textarea' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'textarea' && (
                                    <View className="textarea">
                                      <Textarea
                                        maxlength="-1"
                                        placeholder="请输入留言内容"
                                        onInput={_fixme_with_dataset_(this.inputValue, {index: index})}
                                      ></Textarea>
                                      {/*  <View class="num-tip">5/50</View>  */}
                                    </View>
                                  )}

                                  {/*                 &lt;!&ndash; 地址 &ndash;&gt; */}
                                  {item.type === 'address' && <View className="contact-number">{item.label}</View>}

                                  {item.type === 'address' && (
                                    <RegionPicker
                                      className="phone-input"
                                      // mode="region"
                                      options={{index: index}}
                                      onChange={this.onRegionPickerChange}
                                      // onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                                    >
                                      {item.value ? (
                                        <Text className="picker">{item.value}</Text>
                                      ) : (
                                        <Text className="picker" style={_safe_style_('color: #888;')}>
                                          请选择
                                        </Text>
                                      )}

                                      <Image
                                        className="picker-icon"
                                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                      ></Image>
                                    </RegionPicker>
                                  )}

                                  {/*                 &lt;!&ndash;详细地址 &ndash;&gt; */}
                                  {item.type === 'detail-address' && (
                                    <View className="contact-number">{item.label}</View>
                                  )}

                                  {item.type === 'detail-address' && (
                                    <View className="textarea">
                                      <Textarea
                                        // className="phone-input"
                                        disableDefaultPadding
                                        autoHeight
                                        placeholder="请输入详细地址"
                                        onInput={_fixme_with_dataset_(this.inputValue, {index: index})}
                                        value={item.value}
                                      ></Textarea>
                                    </View>
                                  )}

                                  {item.type === 'comment' && (
                                    <View className="comment">
                                      <Text>{item.value}</Text>
                                    </View>
                                  )}

                                  {(item.type === 'uploadImage') && (
                                    <View className="uploadImgBox">
                                      <View className="contact-number">{item.label} <Text
                                        className="serve-time"> (可上传{item.max}张)</Text></View>
                                      <View className='uploadImage-box'>
                                        <View className='img-box'>
                                          <UploadImg onChange={this.handleImagesChange} value={item.value}
                                                     max={item.max} containerIndex={index}></UploadImg>
                                        </View>
                                      </View>
                                    </View>
                                  )}
                                </View>
                              </View>
                            )}
                          </View>
                        );
                      })}
                  </View>
                )}
                {serviceDetail.showNumber === 1 &&
                  <View className="service-num">
                    <Text className="num-title">数量</Text>
                    <View className="operation-box">
                      <Image
                        className="image"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/plugin/minus.png"
                        onClick={this.onReduce}
                      >
                        -
                      </Image>
                      <Text className="num">{num}</Text>
                      <Image
                        className="image"
                        src="https://front-end-1302979015.file.myqcloud.com/images/service-icon.svg"
                        onClick={this.onAdd}
                        style={_safe_style_(!isStock || (serviceDetail.useStock == 1 && serviceDetail.itemStock <= num) ? 'opacity: 0.3;' : null)}
                      >
                        +
                      </Image>
                    </View>
                  </View>
                }
              </View>
            </Form>
            {changeType && changeType != '1' &&
              <ContactHousekeeper typeChange={dataTotal} onGetType={this.handleGetType}
                                  className="customer-x"></ContactHousekeeper>}
            {postType && postType == 2 && (
              <View>
                <View className="posters-dialog-m" onClick={this.closeMe}></View>
                <View className='internal-posters-dialog'>
                  <View className="internal-title">{dataTotal.title}</View>
                  <View className='internal-posters-box'>
                    <View className="internal-text">{dataTotal.customerDescribe}</View>
                    <Image src={dataTotal.imgUrl} show-menu-by-longpress="true" className="qr-code"></Image>
                    <View className="desc">长按识别二维码</View>
                  </View>
                  <Image src="https://htrip-static.ctlife.tv/wk/marketing-base/close.png" className='close-T-C' onClick={this.closeMe}></Image>
                </View>
              </View>
            )}
            {serviceDetail.serverInfo && (
              <View className="service-info serverInfo">
                <View className="carouseltext-title">服务描述</View>
                <RichText className="rich-text" nodes={serviceDetail.serverInfo}></RichText>
              </View>
            )}
          </View>
          <View className="footer">
            <View onClick={this.onAddJoinBooking} className="join-booking-btn">
              加入预订
            </View>
            <View
              style={_safe_style_('background:' + (disable || !isStock ? "#cccccc" : tmpStyle.btnColor) + ';')}
              onClick={this.onImmediately}
              className="order-now-btn"
            >
              {!isStock ? '库存不足' : disable ? '不在服务时间' : '立即下单'}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default ServiceDetail;
