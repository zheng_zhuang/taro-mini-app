import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { View, Swiper, SwiperItem, Image, Text, ScrollView } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
// import industryEnum from '../../../../../wxat-common/constants/industryEnum.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import state from '../../../../../state/index.js';
import api from '../../../../../wxat-common/api/index.js';
import report from '@/sdks/buried/report/index.js';
// import utils from '../../../../../wxat-common/utils/util.js';
import template from '../../../../../wxat-common/utils/template.js';
// import checkOptions from '../../../../../wxat-common/utils/check-options.js';
// import AuthPuop from '../../../../../wxat-common/components/authorize-puop/index';
import CustomerHeader from '../../../../../wxat-common/components/customer-header/index';
import Icalendar from '../../../../../wxat-common/components/calendar/index';
import { connect } from 'react-redux';
import { getDecorateHomeData } from '@/wxat-common/x-login/decorate-configuration-store';
import './index.scss';

// @withWeapp({
//   on: {
//     stateChange(data) {
//       //切换店铺
//       if (data && data.key === 'currentStore') {
//       }
//       if (data && data.key === "appInfo") {
//         this.setData({
//           showScanCode: state.base.appInfo && !!state.base.appInfo.scanCode
//         })
//       }
//     }
//   },
// })

const mapStateToProps = ({ globalData }) => {
  return {
    globalData
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class HotelReservationDetail extends React.Component {

  state = {
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
    indicatorActiveColor: '#FFFFFF',
    indicatorColor: '#FFFFFF',
    tmpStyle: {}, // 主题模板配置
    showcalendar: false,
    inTime: '',
    outTime: '',

    showinTime: '',
    showoutTime: '',
    DaysBetween: '',
    address: '',
    name: '',
    longitude: '',
    latitude: '',
    storeId: '',
    outValue: '',
    hotelDetails: [], //酒店详情
    serverStoreList: [], //酒店服务列表
    bedTypeList: [], //酒店房间列表
    storeImgList: [], //头部轮播
    storeImg: '',
    formExt: [],
    defalutTime: {
      inTime: '',
      outTime: '',
    },

    storeInfo: null,
    navOpacity: 1,
    navBgColor: null
  }

  /**
   * 生命周期函数--监听页面加载
   */
    onLoad(options) {
    if (process.env.TARO_ENV === 'weapp') {
      Taro.hideShareMenu({
        menus: ['shareAppMessage', 'shareTimeline'],
      });
    }
    let storeId = options.id;
    let inTime = options.inTime;
    let outTime = options.outTime;
    let DaysBetween = options.DaysBetween;
    let showinTime = options.showinTime;
    let showoutTime = options.showoutTime;
    let outValue = this.getWeekDate(outTime);
    this.setState({
      storeId,
      inTime,
      outTime,
      DaysBetween,
      showinTime,
      showoutTime,
      outValue,
      storeInfo: state.base.currentStore,
      navBgColor: this.props.globalData.tabbars.list[0].navBackgroundColor,
    });

    this.getTemplateStyle(); // 获取主题模板配置
    this.gethotelDetail(); // 获取酒店列表
  }

  //获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //获取酒店详情
  gethotelDetail() {
    Taro.showLoading(
      {
        title: '正在加载',
        mask: true
      }
    )
    wxApi
      .request({
        url: api.livingService.getHotelDetails,
        data: {
          storeId: this.state.storeId,
        },
      })
      .then((res) => {
        let hotelDetails = res.data || [];
        let serverStoreList = res.data.serverStoreList || [];
        let bedTypeList = res.data.bedTypeList || [];
        bedTypeList.forEach((item) => {
          item.imgList = [];
          item.img = item.img || '';
          item.img.split(',').forEach((url) => {
            item.imgList.push(url);
          });
        });
        let longitude = res.data.longitude;
        let latitude = res.data.latitude;
        let storeImg = res.data.storeImg || '';
        let address = res.data.hotelAddress;
        let name = res.data.hotelName;
        let storeImgList = storeImg.split(',');
        this.setState({
          hotelDetails,
          serverStoreList,
          bedTypeList,
          longitude,
          latitude,
          address,
          name,
          storeImgList,
        })
        Taro.hideLoading()
      }).catch(() => {
        Taro.hideLoading()
      })
  }

  checkDate = () => {
    this.setState({
      ['defalutTime.inTime']: this.state.inTime,
      ['defalutTime.outTime']: this.state.outTime,
      showcalendar: true
    })
  }

  closeDialog = (e) => {
    if (!!e.ishasValue) {
      const inTime = e.inTime;
      const outTime = e.outTime;
      let showinTime = this.MonthDayformat(inTime);
      let showoutTime = this.MonthDayformat(outTime);
      let outValue = this.getWeekDate(outTime);
      this.setState({
        showcalendar: false,
        inTime,
        outTime,
        showinTime,
        showoutTime,
        outValue,
        DaysBetween: e.DaysBetween,
      });
    } else {
      this.setState({
        showcalendar: false,
      });
    }
  }

  //计算两个时间之前的天数
  getDaysBetween = (dateString1, dateString2) => {
    var startDate = Date.parse(dateString1);
    var endDate = Date.parse(dateString2);
    if (startDate > endDate) {
      return 0;
    }
    if (startDate == endDate) {
      return 1;
    }
    var days = (endDate - startDate) / (1 * 24 * 60 * 60 * 1000);
    return days;
  }

  //跳转去酒店服务列表页
  hotelServiceList = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/living-service/hotel-service-list/index',
    });
  }

  //格式化日期，只显示月份和日
  MonthDayformat = (date) => {
    let Dtime = new Date(date);
    var Dtimemonth = Dtime.getMonth() + 1;

    // Dtimemonth < 10 ? '0' + Dtimemonth : Dtimemonth
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}月${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }日`;
  }

  //获取当前是周几
  getWeekDate = (dateDay) => {
    let now = new Date(dateDay);
    let day = now.getDay();
    let weeks = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');

    let week = weeks[day];
    return week;
  }

  navigationMap = () => {
    report.clickStoreLocation();
    Taro.openLocation({
      latitude: parseFloat(this.state.latitude),
      longitude: parseFloat(this.state.longitude),
      name: this.state.name,
      address: this.state.address
    })
  }

  //预订
  bookingBtn = (item) => {
    const serviceName = item.serverName
    const salePrice = item.salePrice
    const deposit = item.deposit;
    const id = item.id;
    const inTime = this.state.inTime;
    const outTime = this.state.outTime;
    const itemNo = item.itemNo;
    const DaysBetween = this.state.DaysBetween;
    const storeId = item.storeId;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/living-service/hotel-order-details/index',
      data: {
        itemNo,
        serviceName,
        salePrice,
        deposit,
        inTime,
        outTime,
        DaysBetween,
        id,
        sourceType: 2,
        storeId,
        formExt: item.formExt
      },
    });
  }

  //查看图片
  // previewImg(e) {
  //   const previewUrls = e.currentTarget.dataset.imgList;

  //   const currentUrl = e.currentTarget.dataset.index;
  //   wx.previewImage({
  //     current: currentUrl,
  //     urls: previewUrls
  //   });
  // },
  
  /**
   * 页面滚动
   * @param {*} ev
   */
   onPageScroll(e) {
    // if (e.scrollTop < 30 && this.data.navOpacity != 0.0) {
    //   this.setData({
    //     navOpacity: 0,
    //   });
    //   return
    // }
    // const homeConfig = getDecorateHomeData();
    // let height = homeConfig[1].config.height / 2 || 60;
    // this.setState({
    //   navOpacity: e.scrollTop / height,
    // })
  }

  render() {
    const {
      navOpacity,
      navBgColor,
      indicatorDots,
      autoplay,
      interval,
      indicatorActiveColor,
      duration,
      storeImgList,
      hotelDetails,
      tmpStyle,
      outValue,
      showinTime,
      DaysBetween,
      showoutTime,
      bedTypeList,
      defalutTime,
      showcalendar,
    } = this.state
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-mplh-HotelReservationDetail"
        className="wk-mplh-HotelReservationDetail"
      >
        <CustomerHeader navOpacity={navOpacity} title="酒店详情" bgColor={navBgColor}></CustomerHeader>
        <View className="hotel-reservation-detail">
          <View className="detail-box">
            <View className="detail-item-first">
              {hotelDetails.storeImg && (
                <View className="hotel-detail-carousel">
                  <Swiper
                    indicatorDots={indicatorDots}
                    autoplay={autoplay}
                    interval={interval}
                    circular
                    indicatorActiveColor={indicatorActiveColor}
                    duration={duration}
                    className="swiper-img"
                  >
                    {storeImgList &&
                      storeImgList.map((item, index) => {
                        return (
                          <SwiperItem className="swiper-img" key={index}>
                            <View className="swiper-item">
                              <Image className="swiper-img" src={item} mode="aspectFill"></Image>
                            </View>
                          </SwiperItem>
                        );
                      })}
                  </Swiper>
                </View>
              )}

              <View className="hotel-infomation">
                <View className="hotel-infomation-text">
                  <View className="hotel-name">{hotelDetails.hotelName}</View>
                  {/*  <view class="hotel-service">
                            <view class="hotel-service-item" wx:for="{{serverStoreList}}" wx:key="{{index}}">
                                <view>
                                    <image class="hotel-service-img" src="{{filters.imgListFormat(item.img)}}"
                                        mode="aspectFit"></image>
                                </view>
                                <view class="hotel-service-text">{{item.serverName}}</view>
                            </view>
                        </view>  */}
                </View>
                {/*  <view class="hotel-infomation-img" bindtap="hotelServiceList">
                          <view class="hotel-service-list">
                              <Text class="service-list-text"  style="color:{{tmpStyle.btnColor}}">酒店服务</Text><Text class="service-list-img">
                                  <image mode="aspectFit" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png" class="right-icon"></image>
                              </Text>
                          </view>
                      </view>  */}
              </View>
              <View className="hotel-address">
                <View className="hotel-infomation-address">
                  <View className="hotel-address-text">{hotelDetails.hotelAddress}</View>
                </View>
                <View className="hotel-infomation-img">
                  <View className="hotel-service-list" onClick={this.navigationMap}>
                    <Text className="service-list-text" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      地图/导航
                    </Text>
                      <Image
                        mode="aspectFit"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="right-icon"
                      ></Image>
                  </View>
                </View>
              </View>
            </View>
            <View className="xian"></View>
            <View className="detail-item">
              <View className="hotel-infomation">
                <View className="hotel-date">
                  <View className="date-text">
                    <Text>今天入住</Text>
                    <Text></Text>
                    <Text>{outValue + '离店'}</Text>
                  </View>
                  <View className="date-date">
                    <Text className="date-left">{showinTime}</Text>
                    <Text className="date-middle">{'共' + DaysBetween + '晚'}</Text>
                    <Text className="date-right">{showoutTime}</Text>
                  </View>
                </View>
                <View className="hotel-infomation-img" onClick={this.checkDate}>
                  <View className="hotel-service-list">
                    <Text className="service-list-text" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      修改日期
                    </Text>
                      <Image
                        mode="aspectFit"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        className="right-icon"
                      ></Image>
                  </View>
                </View>
              </View>
              <ScrollView className="hotel-room-list">
                {bedTypeList &&
                  bedTypeList.map((item, index) => {
                    return (
                      <View className="hotel-room-item" key={index}>
                        <View className="hotel-item-left">
                          <View className="hotelImgBox">
                            <Swiper autoplay={autoplay} interval={interval} circular className="hotelImg">
                              {item.imgList &&
                                item.imgList.map((img, index) => {
                                  return (
                                    <SwiperItem
                                      key={index}
                                      data-imgList={item.imgList}
                                      data-index={img}
                                      className="hotelImg"
                                    >
                                      <View className="hotelImg">
                                        <Image className="hotelImg" src={img} mode="aspectFill"></Image>
                                      </View>
                                    </SwiperItem>
                                  );
                                })}
                            </Swiper>
                          </View>
                          <View className="hotelText">
                            <View className="hotelText-room">{item.serverName}</View>
                            <View className="hotelText-food">{item.serverDesc}</View>
                            <View className="hotelText-price">
                              {'￥' + filters.moneyFilter(item.salePrice, true)}
                              <Text className="label-price">
                                {'￥' + filters.moneyFilter(item.originalPrice, true)}
                              </Text>
                            </View>
                          </View>
                        </View>
                        <View
                          className="hotel-item-right"
                          style={_safe_style_('background:' + tmpStyle.btnColor)}
                        >
                          <View className="bookingBtn">
                            {/* <View className="bookingText" onClick={_fixme_with_dataset_(this.bookingBtn, {
                            id: item.id,
                            name: item.serverName,
                            price: item.salePrice,
                            deposit: item.deposit,
                            itemNo: item.itemNo,
                            storeId: item.storeId,
                          })}>预订</View> */}
                            <View className="bookingText" onClick={() => {this.bookingBtn(item)}}>预订</View>
                          </View>
                        </View>
                      </View>
                    );
                  })}
              </ScrollView>
            </View>
          </View>
        </View>
        <Icalendar
          className="calendar"
          defalutTime={defalutTime}
          showcalendar={showcalendar}
          onMyevent={this.closeDialog}
        ></Icalendar>
      </View>
    );
  }
}

export default HotelReservationDetail;
