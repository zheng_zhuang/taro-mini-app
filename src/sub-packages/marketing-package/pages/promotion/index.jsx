import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, ScrollView, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import api from '../../../../wxat-common/api/index.js';
import wxApi from '../../../../wxat-common/utils/wxApi';
import imageUtils from '../../../../wxat-common/utils/image.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import protectedMailBox from '../../../../wxat-common/utils/protectedMailBox.js';
import constants from '../../../../wxat-common/constants/index.js';
import timer from '../../../../wxat-common/utils/timer.js';
import promotionEnum from '../../../../wxat-common/constants/promotionEnum.js';
import LoadMore from '../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../wxat-common/components/error/error';
import Empty from '../../../../wxat-common/components/empty/empty';
import HoverCart from '../../../../wxat-common/components/cart/hover-cart/index';
import GoodsItem from '../../../../wxat-common/components/industry-decorate-style/goods-item/index';
import store from '@/store/index';
import template from '../../../../wxat-common/utils/template.js';
import displayEnum from '../../../../wxat-common/constants/displayEnum';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import './index.scss';

const app = store.getState();
const loadMoreStatusEnum = constants.order.loadMoreStatus;

@hoc
class GoodsInfo extends React.Component {
  $router = $getRouter();
  state = {
    promotionEnum: promotionEnum.STATUS,
    activityId: null,
    activityDetail: {},
    promotionList: [], //活动列表
    goodsList: [], //商品数据
    posterData: [], //海报数据
    isTop: false,
    reportSource: reportConstants.SOURCE_TYPE.activity.key,
    display: null,
    itemClass: null,
    containerClass: null,
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    tmpStyle: {},
  };

  _thredId = null;

  onPageScroll(res) {
    this.setState({
      isTop: res.scrollTop > 300,
    });
  }

  componentDidMount() {
    const params = this.$router.params;
    const dataSource = protectedMailBox.read('dataSource') || {};

    let activityId = null;
    if (params.activityId) {
      activityId = params.activityId;
    } else if (dataSource.data && dataSource.data.length) {
      //初始化去第一个活动
      activityId = dataSource.data[0].id;
    }

    //如果左右滑动改成一行两个
    let display = dataSource.display;
    if ((dataSource.display = 'horizon')) {
      display = 'oneRowTwo';
    }

    const itemClass = displayEnum.getClassByShowType(displayEnum.classTypeEnum.item, display);
    const containerClass = displayEnum.getClassByShowType(displayEnum.classTypeEnum.container, display);

    this.setState(
      {
        itemClass: itemClass,
        containerClass: containerClass,
        posterData: dataSource.posterData || [],
        display: display || 'oneRowTwo',
        promotionList: dataSource.data || [],
        activityId,
      },

      () => {
        if (activityId) {
          this.getpartitions();
          this.getList();
        }
      }
    );

    this.getTemplateStyle();
  }

  /**
   * 生命周期函数--监听页面卸载
   */
  componentWillUnmount() {
    if (this._thredId) {
      timer.deleteQueue(this._thredId);
      this._thredId = null;
    }
  }

  // onPullDownRefresh() {
  //   this.setState(
  //     {
  //       pageNo: 1,
  //       hasMore: true,
  //       goodsList:[]
  //     },
  //     () => {
  //       this.getList().then(res=>{
  //         wxApi.stopPullDownRefresh();
  //       });
  //     }
  //   )
  // }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatusEnum.LOADING,
        },
        () => {
          this.getList()
        }
      )
    }
  }

  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatusEnum.LOADING,
      },
      () => {
        this.getList()
      }
    )


  };

  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  /**
   * 获取模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
   * 判断活动状态
   */
  judgeActivityStatus(startTime, endTime) {
    const now = new Date();
    if (now < startTime) {
      return this.state.promotionEnum.NOT_STARTED.value; // 未开始
    } else if (startTime < now < endTime) {
      return this.state.promotionEnum.ON_GOING.value; // 进行中
    } else {
      return this.state.promotionEnum.ENDED.value; // 已结束
    }
  }

  /**
   * 格式化倒计时
   * @param {*} obj
   */
  updateTime(obj) {
    const now = new Date().getTime();
    const startTime = obj.startTime.replace(/-/g, '/');
    const endTime = obj.endTime.replace(/-/g, '/');
    if (
      obj.status === this.state.promotionEnum.NOT_STARTED.value ||
      obj.status === this.state.promotionEnum.ON_GOING.value
    ) {
      obj.status = this.judgeActivityStatus(new Date(startTime), new Date(endTime));
    }

    //根据拼团的活动状态判断时间
    if (obj.status === this.state.promotionEnum.NOT_STARTED.value) {
      obj.remainTime = parseInt((new Date(startTime).getTime() - now) / 1000);
    } else if (obj.status === this.state.promotionEnum.ON_GOING.value) {
      obj.remainTime = parseInt((new Date(endTime).getTime() - now) / 1000);
    } else {
      obj.remainTime = -1;
    }

    if (obj.status !== this.state.promotionEnum.NOT_STARTED.value && (!obj.remainTime || obj.remainTime <= 0)) {
      obj.status = this.state.promotionEnum.ENDED.value; // 将状态设为已结束
      //倒计时清空
      obj.displayTime = null;
      this.setState({
        activityDetail: obj,
      });

      // 活动结束删除定时器
      if (this._thredId) {
        timer.deleteQueue(this._thredId);
        this._thredId = null;
      }
      return;
    }

    obj.remainTime -= 1;

    const day = parseInt(obj.remainTime / (24 * 3600));

    const hourS = parseInt(obj.remainTime % (24 * 3600));
    const hourInt = parseInt(hourS / 3600);
    const hour = hourInt < 10 ? '0' + hourInt : hourInt;

    const minuteS = hourS % 3600;
    const minuteInt = parseInt(minuteS / 60);
    const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

    const secondS = parseInt(minuteS % 60);
    const second = secondS < 10 ? '0' + secondS : secondS;

    obj.displayTime = {
      day: day < 10 ? '0' + day : day,
      hour: hour + '',
      minute: minute + '',
      second: second + '',
    };

    this.setState({
      activityDetail: obj,
    });
  }

  handleTab = (e) => {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
        activityId: e.currentTarget.dataset.index,
        goodsList:[]
      },

      () => {
        this.getpartitions();
        this.getList();
      }
    );
  };

  //查询活动详情
  getpartitions() {
    wxApi
      .request({
        url: api.much_buy.query,
        data: {
          id: this.state.activityId,
        },
      })
      .then((res) => {
        this.setState(
          {
            activityDetail: res.data,
          },

          () => {
            this.updateTime(this.state.activityDetail);
            // 定时器格式化时间
            this._thredId = timer.addQueue(() => {
              this.updateTime(this.state.activityDetail); // 格式化参团时间
            });
          }
        );
      })
      .catch((error) => {
        this.apiError(error);
      });
  }

  // 列表
  async getList() {
    console.log("页数",this.state.pageNo)
    await wxApi
      .request({
        url: api.much_buy.item,
        loading: true,
        data: {
          moreDiscountActivityId: this.state.activityId,
          pageNo: this.state.pageNo,
          pageSize: 10,
        },
      })
      .then((res) => {
        const data = res.data || []
        this.setState(
          {
            pageNo: ++this.state.pageNo,
            goodsList: this.state.goodsList.concat(data)
          },
          () => {
            if (this.state.goodsList.length === res.totalCount || !res.totalCount) {
              this.setState(
                {
                  hasMore: false
                }
              )
            }
            if (this.state.goodsList.length === res.totalCount && res.totalCount) {
              this.setState(
                {
                  loadMoreStatus: loadMoreStatusEnum.BASELINE
                }
              )
            }
          }
        )
      })
      .catch((error) => {
        this.apiError(error);
      });
  }

  fakeData(goodsList) {
    goodsList.forEach((item) => {
      item.activityTags = [
        {
          name: '折扣',
          tagCss: 'background:white;color:red;border:1rpx solid red',
        },

        {
          name: '实惠',
          tagCss: '',
        },

        {
          name: '满减',
          tagCss: '',
        },
      ];

      item.goodsTags = [
        {
          name: '5元/片',
          tagCss: 'background:white;color:rgba(163,163,163,1);border:1rpx solid rgba(163,163,163,1)',
        },

        {
          name: '5g/包',
          tagCss: 'background:white;color:red;border:1rpx solid red',
        },

        {
          name: '超级优惠',
          tagCss: 'background:white;color:rgba(163,163,163,1);border:1rpx solid rgba(163,163,163,1)',
        },
      ];
    });
  }

  render() {
    const {
      isTop,
      posterData,
      activityId,
      promotionList,
      activityDetail,
      promotionEnum,
      goodsList,
      error,
      containerClass,
      itemClass,
      display,
      reportSource,
      loadMoreStatus,
      tmpStyle,
    } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-mpp-Promotion' className='wk-mpp-Promotion'>
        <View className='activitiy'>
          <View className={'fixed ' + (isTop || !posterData || !posterData.length ? 'top' : '')}>
            {
              /* style={{height: posterData && posterData.length !== 0 ? '150px' : '0px'}}*/
              !!(posterData && posterData.length !== 0) && (
                <View className='activitiy-img'>
                  {posterData.map((item, index) => {
                    return (
                      <Image className='activitiy-img__image' mode='aspectFill' key={index} src={item.src}></Image>
                    );
                  })}
                </View>
              )
            }

            {!!(promotionList && promotionList.length > 1) && (
              <ScrollView className='classfiy-box' scrollX>
                {promotionList.map((item, index) => {
                  return (
                    <View
                      onClick={_fixme_with_dataset_(this.handleTab, { index: item.id })}
                      className={'tab ' + (item.id == activityId ? 'active' : '')}
                      key={index}
                      style={_safe_style_(item.id == activityId ? 'font-weight:600;color:' + tmpStyle.btnColor : '')}
                    >
                      <View>{item.name}</View>
                      <View
                        className='bot'
                        style={_safe_style_(item.id == activityId ? 'background:' + tmpStyle.btnColor : '')}
                      ></View>
                    </View>
                  );
                })}
              </ScrollView>
            )}
          </View>
          <View
            className='goods-list'
            style={_safe_style_(
              'margin-top: ' +
                Taro.pxTransform(
                  (posterData && posterData.length ? 418 : 118) - (promotionList && promotionList.length > 1 ? 0 : 88)
                )
            )}
          >
            <View className='remain-time-box'>
              {activityDetail.displayTime ? (
                <View className='remain-time-title'>
                  {'距离活动' + (activityDetail.status === promotionEnum.NOT_STARTED.value ? '开始' : '结束') + '还剩'}
                  <Text className='time-block'>{activityDetail.displayTime.day}</Text>
                  <Text className='symbol'>天</Text>
                  <Text className='time-block'>{activityDetail.displayTime.hour}</Text>
                  <Text className='symbol'>小时</Text>
                  <Text className='time-block'>{activityDetail.displayTime.minute}</Text>
                  <Text className='symbol'>分</Text>
                  <Text className='time-block'>{activityDetail.displayTime.second}</Text>
                  <Text className='symbol'>秒</Text>
                </View>
              ) : (
                activityDetail.status === promotionEnum.ENDED.value && (
                  <View className='remain-time-title'>活动已结束</View>
                )
              )}

              <View className='hr'></View>
              <View className='time'>{'活动时间：' + activityDetail.startTime + ' - ' + activityDetail.endTime}</View>
              <View className='rule'>
                {'活动规则：' + activityDetail.ruleName}
                {!!activityDetail.buyLimit && <Text>{',每人限购' + activityDetail.buyLimit + '件'}</Text>}
                <Text>{',' + (activityDetail.sameItemSwitch ? '不可' : '可') + '跨商品'}</Text>
              </View>
              <View
                className='content'
                style={_safe_style_('background:' + tmpStyle.bgColor + ';color:' + tmpStyle.btnColor + ';')}
              >
                {activityDetail.ruleName}
              </View>
            </View>
            {!!(!!goodsList && goodsList.length == 0) && <Empty message='敬请期待...'></Empty>}
            {!!error ? (
              <Error></Error>
            ) : (
              <View className={containerClass}>
                {goodsList.map((item, index) => {
                  return (
                    <View className={itemClass} key={item.id}>
                      <GoodsItem                       
                        display={display}
                        reportSource={reportSource}
                        goodsItem={item}
                        showDivider={index !== goodsList.length - 1}
                        attributeName={activityDetail.ruleName}
                      />
                    </View>
                  );
                })}
              </View>
            )}

            <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore} />
          </View>
        </View>
        <HoverCart />
        <HomeActivityDialog showPage="sub-packages/marketing-package/pages/promotion/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default GoodsInfo;
