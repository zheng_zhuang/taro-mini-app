import {$getRouter} from 'wk-taro-platform';
import React from 'react'; // @scoped
import '@/wxat-common/utils/platform';
import {View, WebView} from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

class CustomWebView extends React.Component {
  $router = $getRouter();
  state = {};

  render() {
    const { linkUrl } = this.$router.params;
    return (
      <View data-scoped='wk-sgps-custom-webview' className='wk-sgps-contact-webview'>
        {!!linkUrl && <WebView src={linkUrl} />}
      </View>
    );
  }
}

export default CustomWebView;
