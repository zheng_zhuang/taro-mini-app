import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './ReceiveBtn.scss';

type IProps = {
  text: string;
  styles: string;
  onReceiveClick: (object) => any;
};

let ReceiveBtn: FC<IProps> = (props) => {
  const { text, styles, onReceiveClick } = props;
  return (
    <View
      data-scoped='wk-prr-ReceiveBtn'
      className='wk-prr-ReceiveBtn receive-btn-container'
      style={_safe_style_(styles)}
      onClick={onReceiveClick}
    >
      <View className='receive-btn'>{text}</View>
    </View>
  );
};
ReceiveBtn.defaultProps = {
  text: '',
  styles: '',
};

export default ReceiveBtn;
