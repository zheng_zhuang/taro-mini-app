import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from '../../../../../wxat-common/api/index.js';
import report from '../../../../../sdks/buried/report/index.js';
import subscribeEnum from '../../../../../wxat-common/constants/subscribeEnum.js';

/**
 * 领取红包组件
 */
import AuthUser from '../../../../../wxat-common/components/auth-user';
import ReceiveBtn from './ReceiveBtn';
import './index.scss';

class ReceiveRedPacket extends React.Component {
  static defaultProps = {
    // 新的红包信息
    packet: null,
  };

  /**
   * 组件的初始数据
   */
  state = {
    actionText: '领取',
    styles:
      'width: 376rpx;' +
      '  height: 96rpx;' +
      '  border-radius: 0;' +
      '  margin-top: 74rpx;' +
      '  background: none !important;' +
      '  font-size: 28prx;' +
      '  font-family: PingFangSC-Regular;' +
      '  font-weight: 400;' +
      '  color: rgba(255, 255, 255, 1);' +
      '  line-height: 96rpx;' +
      '  padding-left: 0;' +
      '  padding-right: 0;' +
      '  text-align: center;',
  };

  // 领取红包
  handleCollectRedPacket = () => {
    //拆红包成功的订阅消息
    const Ids = [subscribeEnum.REDPACKETS_SUCCESS.value];

    //subscribeMsg.sendMessage(Ids).then(() => {
    wxApi
      .request({
        url: api.redPacket.collect,
        loading: true,
      })
      .then((result) => {
        if (result.data) {
          this.props.onReceived(result.data);
        } else {
          wxApi.showToast({
            title: '领取红包失败，请重新领取',
          });

          this.setState({
            actionText: '重新领取',
          });
        }
        report.getRedPacket(!!result.data);
      });
    //});
  };

  // 点击领取
  handleClick = () => {
    this.handleCollectRedPacket();
  };

  render() {
    const { packet } = this.props;
    const { styles, actionText } = this.state;
    return (
      <View data-scoped='wk-prr-ReceiveRedPacket' className='wk-prr-ReceiveRedPacket receive-red-packet-container'>
        <View className='packet-title'>恭喜</View>
        <View className='packet-money'>
          <Text>{packet.totalFee + '元'}</Text>红包已包好
        </View>
        <View className='packet-tip'>快拆开和小伙伴分享红包</View>

        <AuthUser
          scene='custom_btn'
          custom_btn_desc='授权后领取红包'
          custom_style={styles}
          hideBorder
          //showSlot
        >
          <ReceiveBtn text={actionText} styles='margin-top: 117rpx;' onReceiveClick={this.handleClick} />
        </AuthUser>
      </View>
    );
  }
}

export default ReceiveRedPacket;
