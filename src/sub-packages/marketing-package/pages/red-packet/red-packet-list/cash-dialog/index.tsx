// @externalClassesConvered(BottomDialog)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, ScrollView, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '../../../../../../wxat-common/utils/template';
import api from '../../../../../../wxat-common/api/index';
import wxApi from '../../../../../../wxat-common/utils/wxApi';
import money from '../../../../../../wxat-common/utils/money';

import BottomDialog from '../../../../../../wxat-common/components/bottom-dialog';
import './index.scss';

import { connect } from 'react-redux';
import { ITouchEvent } from '@tarojs/components/types/common';
import React, { ComponentClass } from 'react';

type StateProps = {
  templ: any;
};

type CashDialogProps = {
  onCashSelect?: Function;
};

const mapStateToProps = () => {
  return {
    templ: template.getTemplateStyle(),
  };
};

interface CashDialog {
  props: StateProps & CashDialogProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class CashDialog extends React.Component {
  state = {
    allChecked: false,
    pageNo: 1,
    hasMore: true,
    selectPacketList: [],
    totalCount: 0,
  };

  cashDialogCMPT: any;

  getCashDialog = () => {
    return this.cashDialogCMPT;
  };

  hide = () => {
    this.getCashDialog().hideModal();
  };

  show = () => {
    this.resetData();
    this.setState({ allChecked: false }, () => {
      this.getCashDialog().showModal();
    });
  };

  resetData = () => {
    this.setState({ pageNo: 1, hasMore: true }, this.listRedPacketList);
  };

  listRedPacketList = async () => {
    const params = {
      cashWithdrawalType: 1,
      autoCashingSwitch: 0,
      pageNo: this.state.pageNo,
      pageSize: 100,
    };

    const res = await wxApi.request({ url: api.redPacket.listV2, data: params });

    let selectPacketList = res.data || [];

    const totalCount = this.state.totalCount + selectPacketList.length;

    selectPacketList = selectPacketList.filter((item) => {
      item.luckyMoneyFee = money.fen2Yuan(item.luckyMoneyFee);

      return item.cashWithdrawalStatus === 0;
    });

    if (this.isLoadMoreRequest()) {
      selectPacketList = this.state.selectPacketList.concat(selectPacketList || []);
    } else {
      selectPacketList = selectPacketList || [];
    }

    this.setState({
      pageNo: this.state.pageNo + 1,
      hasMore: totalCount !== res.totalCount,
      selectPacketList,
      totalCount,
    });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  handlerSelect = () => {
    const codeList: any[] = [];

    this.state.selectPacketList.forEach((v: any) => {
      if (v.checked) {
        codeList.push(v.code);
      }
    });

    if (!codeList.length) {
      wxApi.showToast({
        icon: 'none',
        title: '请选择提现红包',
        duration: 2000,
      });

      return false;
    }
    if (typeof this.props.onCashSelect === 'function') {
      this.props.onCashSelect({ codeList });
    }
  };

  lower = () => {
    if (this.state.hasMore) {
      this.listRedPacketList();
    }
  };

  // 单条sku，点击check按钮
  handlerCheck = (ev: ITouchEvent) => {
    const index = ev.currentTarget.dataset.index;
    const item: any = this.state.selectPacketList[index];

    item.checked = !item.checked;
    this.setState({ selectPacketList: this.state.selectPacketList });

    if (item.checked) {
      let checkedLen = 0;

      this.state.selectPacketList.forEach((v: any) => {
        if (v.checked) {
          checkedLen++;
        } else {
          return false;
        }
      });
      if (checkedLen == this.state.selectPacketList.length) {
        this.setState({ allChecked: true });
      }
    } else {
      this.setState({ allChecked: false });
    }
  };

  // 点击全选
  handlerAllCheck = () => {
    this.state.selectPacketList.forEach((v: any) => {
      if (!this.state.allChecked) {
        v.checked = true;
      } else {
        v.checked = false;
      }
    });

    this.setState({
      allChecked: !this.state.allChecked,
      selectPacketList: this.state.selectPacketList,
    });
  };

  render() {
    const { allChecked, selectPacketList } = this.state;
    const { templ } = this.props;

    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-rrc-CashDialog'
        className='wk-rrc-CashDialog'
      >
        <BottomDialog ref={(node) => (this.cashDialogCMPT = node)} anim-class='cash-dialog'>
          <View className='cash-dialog'>
            <View className='cash-dialog-head'>
              <View className='cd-all-checkbox' onClick={this.handlerAllCheck}>
                <View
                  className={'custom-checkbox ' + (allChecked ? 'checked' : '')}
                  style={_safe_style_(
                    allChecked ? 'background:' + templ.btnColor + ';border-color:' + templ.btnColor : ''
                  )}
                ></View>
                全选
              </View>
            </View>
            <ScrollView className='cash-dialog-list' scrollY onScrollToLower={this.lower}>
              {selectPacketList.map((item: any, index) => {
                return (
                  <View className='cash-dialog-item' key={item.id}>
                    <View
                      className={'custom-checkbox ' + (item.checked ? 'checked' : '')}
                      style={_safe_style_(
                        item.checked ? 'background:' + templ.btnColor + ';border-color:' + templ.btnColor : ''
                      )}
                      onClick={_fixme_with_dataset_(this.handlerCheck, { index: index })}
                    ></View>
                    <View className='cd-content'>
                      <Image
                        src="https://htrip-static.ctlife.tv/wk/redpacket-icon.png"
                        className='packet-icon'
                      ></Image>
                      <View className='packet-info'>
                        <View className='info-money'>
                          <Text className='desc'>拆红包获得</Text>
                          <Text className='desc money-fee' style={_safe_style_('color: ' + templ.btnColor)}>
                            {item.luckyMoneyFee}
                          </Text>
                          <Text className='desc'>元</Text>
                        </View>
                        <View className='info-tip limit-line'>{item.planName}</View>
                      </View>
                    </View>
                  </View>
                );
              })}
            </ScrollView>
            <Button className='close' onClick={this.handlerSelect} style={_safe_style_('background:' + templ.btnColor)}>
              确认提现
            </Button>
          </View>
        </BottomDialog>
      </View>
    );
  }
}

export default CashDialog as ComponentClass<CashDialogProps>;
