import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, ScrollView, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../../../../wxat-common/api/index';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import report from '@/sdks/buried/report/index';
import template from '../../../../../wxat-common/utils/template';
import shareUtil from '../../../../../wxat-common/utils/share';
import money from '../../../../../wxat-common/utils/money';

import CashDialog from './cash-dialog/index';
import RtItem from './rt-item/index';
import ShareDialog from '../../../../../wxat-common/components/share-dialog/index';
import LoadMore from '../../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../../wxat-common/components/error/error';
import Empty from '../../../../../wxat-common/components/empty/empty';
import './index.scss';
import { connect } from 'react-redux';
import hoc from '@/hoc';

const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};

const Type = {
  toInCash: 'toInCash',
  toUse: 'toUse',
  toUnZip: 'toUnZip',
};

type StateProps = {
  templ: any;
  maAppName: string;
};

interface RedPacketList {
  props: StateProps;
}

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    maAppName: state.globalData.maAppName,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class RedPacketList extends React.Component {
  state = {
    error: false,
    redPacketList: [],
    pageNo: 1,
    hasMore: true,
    nothing: false,
    loadMoreStatus: loadMoreStatus.HIDE,
    pendingSharedRedPacket: {} as Record<string, any>,
    codeList: [],
    qrCodeParams: {
      verificationNo: null,
      verificationType: 5,
      maPath: 'sub-packages/marketing-package/pages/red-packet/index',
    },

    tabs: [
      { value: Type.toInCash, name: '红包提现' },
      { value: Type.toUse, name: '待使用红包' },
      { value: Type.toUnZip, name: '待拆红包' },
    ],

    activeTab: Type.toInCash,
    balance: '',
    historyCashBalance: '',
    freezeCashBalance: '',
    hasQualify: false,
    hasUnZip: true,
    showCashBtn: false,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    const { templ } = this.props;

    if (templ.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templ.titleColor, // 必写项
      });
    }
  }

  componentDidMount() {
    wxApi.hideShareMenu();
    this.listRedPacketList();
    this.getBalance();
    this.getRedPacketStatus();
  }

  /**
   * 点击tab切换，获取订单列表
   * @param ev
   */
  onSwitchTab = (ev: any) => {
    const tab = typeof ev === 'object' ? ev.currentTarget.dataset.item.value : ev;

    this.setState(
      {
        activeTab: tab,
        pageNo: 1,
        redPacketList: [],
        hasMore: true,
        nothing: false,
      },

      () => {
        this.listRedPacketList(true);
        if (this.state.activeTab === Type.toInCash) {
          this.getBalance();
        }
      }
    );
  };

  listRedPacketList = (isFromPullDown = false) => {
    const tab = this.state.activeTab;
    this.setState({ error: false });

    const isUnZip = tab === Type.toUnZip;
    const url = isUnZip ? api.redPacket.UnZipList : api.redPacket.listV2;
    const params: Record<string, any> = {
      pageNo: this.state.pageNo,
      pageSize: 100,
    };

    if (isUnZip) {
      params.statusList = [0, 2];
    } else {
      params.cashWithdrawalType = tab == Type.toInCash ? 1 : 0;
    }
    wxApi
      .request({
        url: url,
        loading: true,
        method: isUnZip ? 'POST' : 'GET',
        data: params,
      })
      .then((res) => {
        let redPacketList = [];
        if (this.isLoadMoreRequest()) {
          redPacketList = this.state.redPacketList.concat(res.data || []);
        } else {
          redPacketList = res.data || [];
        }

        this.setState({
          pageNo: this.state.pageNo + 1,
          hasMore: redPacketList.length !== res.totalCount,
          nothing: redPacketList.length ? false : true,
          redPacketList,
          loadMoreStatus: loadMoreStatus.HIDE,
        });

        if (tab == Type.toUnZip && !redPacketList.length) {
          this.setState({ hasUnZip: false });
        }
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({ error: true });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  getBalance = async () => {
    const res1 = await wxApi.request({
      url: api.redPacket.balance,
      data: {},
    });

    const res2 = await wxApi.request({
      url: api.redPacket.count,
      data: { autoCashingSwitch: 0, cashWithdrawalStatus: 0, cashWithdrawalType: 1 },
    });

    this.setState({
      balance: res1.data ? money.fen2Yuan(res1.data.cashBalance || 0) : '0.00',
      historyCashBalance: res1.data ? money.fen2Yuan(res1.data.historyCashBalance || 0) : '0.00',
      freezeCashBalance: res1.data ? money.fen2Yuan(res1.data.freezeCashBalance || 0) : '0.00',
      showCashBtn: res2.data ? true : false,
    });
  };

  toFlow = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/red-packet/red-packet-flow/index',
    });
  };

  getRedPacketStatus = async () => {
    const res1 = await wxApi.request({
      url: api.redPacket.status,
      data: {},
    });

    const res2 = await wxApi.request({
      url: api.redPacket.UnZipList,
      method: 'POST',
      data: { statusList: [0], pageSize: 1 },
    });

    const res3 = await wxApi.request({ url: api.redPacket.plan, data: {} });

    if (res2.data && res2.data.length) {
      this.setState({
        hasUnZip: true,
        hasQualify: true,
      });
    } else {
      this.setState({
        hasUnZip: false,
        hasQualify: !res3.data ? false : res1.data,
      });
    }
  };

  toCash = (flag = false) => {
    const codeList = this.state.codeList;

    wxApi.showModal({
      title: '提示',
      content: '是否确认提现？',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.redPacket.cashing,
              method: 'POST',
              data: {
                codeList,
              },
            })
            .then(() => {
              setTimeout(() => {
                wxApi.showToast({
                  icon: 'none',
                  title: '提现申请成功，如72小时未到账，请联系商家',
                  duration: 2000,
                });
              }, 100);
              if (flag) {
                this.getCashDialog().hide();
              }
              this.onPullDownRefresh();
              this.getBalance();
            });
        }
      },
    });
  };

  onPullDownRefresh = () => {
    this.setState({ pageNo: 1, hasMore: true }, () => {
      this.listRedPacketList(true);
    });
  };

  toRedPacket = () => {
    wxApi.showModal({
      showCancel: false,
      title: '',
      content: '红包提现门槛为0.3元，满足门槛后红包会自动发放至微信零钱账户，快去拆更多红包吧~',
      confirmText: '去拆包',
      success: () => {
        // 如果有待拆红包，挑去待拆红包tab
        if (this.state.hasUnZip) {
          this.onSwitchTab(Type.toUnZip);
          // 如果没有待拆红包，但有资格，跳到活动页面(领取红包)，没有资格则隐藏入口
        } else if (this.state.hasQualify) {
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/red-packet/index',
          });
        }
      },
    });
  };

  // 查看详情
  toDetail = ({ luckyMoneyNo }) => {
    if (this.state.activeTab !== Type.toUnZip) return false;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/red-packet/index',
      data: { redPacketNo: luckyMoneyNo },
    });
  };

  toUse = ({ isAvailable }) => {
    if (isAvailable) {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/home/index',
        data: {},
      });
    }
  };

  cashDialogCMPT: any;
  getCashDialog = () => {
    return this.cashDialogCMPT;
  };

  shareDialog = React.createRef<any>();
  getShareDialog = () => {
    return this.shareDialog.current;
  };

  lower = () => {
    if (this.state.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.listRedPacketList();
    }
  };

  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.listRedPacketList();
  };

  showCashDialog = () => {
    this.getCashDialog().show();
  };

  toInCash = (ev) => {
    if (ev.cashWithdrawalStatus === 0) {
      this.setState({
        codeList: [ev.code],
      });

      this.toCash();
    }
  };

  toShare = (detail) => {
    this.setState(
      {
        qrCodeParams: { ...this.state.qrCodeParams, verificationNo: detail.luckyMoneyNo },
      },

      async () => {
        //查询海报的文案和图片
        const res = await wxApi.request({
          url: api.redPacket.planInfo,
          data: {
            planId: detail.luckyMoneyPlanId,
          },
        });

        this.setState({ pendingSharedRedPacket: res.data }, () => {
          this.getShareDialog().show();
        });
      }
    );
  };

  onSavePosterImage = () => {
    this.getShareDialog().savePosterImage(this);
  };

  onShareAppMessage(options) {
    report.share(true);
    if (options.from === 'button') {
      this.getShareDialog().hide();
      const shareRedPacket = this.state.pendingSharedRedPacket;
      const appName = this.props.maAppName ? this.props.maAppName : '';
      const path = shareUtil.buildShareUrlPublicArguments({
        url:
          '/sub-packages/marketing-package/pages/red-packet/index?redPacketNo=' +
          this.state.qrCodeParams.verificationNo,
        bz: shareUtil.ShareBZs.FISSION_RED_PACKET_MINE,
        bzName: `裂变红包-${shareRedPacket ? shareRedPacket.totalFee / 100 + '元红包' : ''}`,
        bzId: this.state.qrCodeParams.verificationNo,
        sceneName: shareRedPacket ? `${shareRedPacket.totalFee / 100} 元红包` : '',
      });

      console.log('sharePath => ', path);
      return {
        title: `${appName}送你 ${shareRedPacket.totalFee / 100} 元红包，点击领取>`,
        path,
        imageUrl: 'https://htrip-static.ctlife.tv/wk/ic-share-img.png',
        success: function () {
          report.shareRedPacket(true);
        },
        fail: function () {
          report.share(false);
          report.shareRedPacket(false);
        },
      };
    }
    return {};
  }

  cashSelect = (ev) => {
    this.setState({
      codeList: ev.codeList,
    });

    this.toCash(true);
  };

  render() {
    const {
      activeTab,
      tabs,
      showCashBtn,
      balance,
      hasQualify,
      historyCashBalance,
      freezeCashBalance,
      redPacketList,
      nothing,
      error,
      loadMoreStatus,
      pendingSharedRedPacket,
      qrCodeParams,
    } = this.state;

    const { templ } = this.props;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-prr-RedPacketList'
        className='wk-prr-RedPacketList'
      >
        {/*  头部订单tab状态切换区域  */}
        <View className='status-box'>
          {tabs.map((item, index) => {
            return (
              <View
                onClick={_fixme_with_dataset_(this.onSwitchTab, { item: item })}
                key={index}
                className={'status-label ' + (item.value === activeTab ? 'active' : '')}
              >
                <Text className='name' style={_safe_style_(item.value === activeTab ? 'color:' + templ.btnColor : '')}>
                  {item.name}
                </Text>
                {item.value === activeTab && (
                  <View
                    className='active-line'
                    style={_safe_style_(item.value === activeTab ? 'background:' + templ.btnColor : '')}
                  ></View>
                )}
              </View>
            );
          })}
        </View>
        <ScrollView className='red-packet-list-container' scrollY onScrollToLower={this.lower}>
          {activeTab === 'toInCash' && (
            <Block>
              <View className='cash-list'>
                <View className={'header ' + (showCashBtn ? 'header-v2' : '')}>
                  {balance !== '' && <View className='title'>待提现红包</View>}
                  <View className='cash-total'>{balance}</View>
                  {!!showCashBtn && (
                    <View className='cash-btn' onClick={this.showCashDialog}>
                      提现
                    </View>
                  )}

                  <View className='cash-left-btn record' onClick={this.toFlow}>
                    红包流水
                  </View>
                  {!!hasQualify && (
                    <View className='cash-left-btn unpack' onClick={this.toRedPacket}>
                      去拆红包
                    </View>
                  )}
                </View>
                <View className='side'>
                  {historyCashBalance !== '' && (
                    <View className='side-box'>
                      <View>
                        已提现
                        <Text className='side-money' style={_safe_style_('color: ' + templ.btnColor)}>
                          {historyCashBalance}
                        </Text>
                        元
                      </View>
                      <View>
                        提现中
                        <Text className='side-money' style={_safe_style_('color: ' + templ.btnColor)}>
                          {freezeCashBalance}
                        </Text>
                        元
                      </View>
                    </View>
                  )}
                </View>
              </View>
              {redPacketList.map((item: any) => {
                return (
                  <RtItem
                    key={item.id}
                    tabType={activeTab}
                    item={item}
                    onUse={this.toInCash}
                    onDetail={this.toDetail}
                  ></RtItem>
                );
              })}
            </Block>
          )}

          {!!nothing && <Empty message='暂无红包'></Empty>}
          {!!error && <Error></Error>}
          {/*  已拆红包  */}
          {activeTab === 'toUse' && (
            <Block>
              {redPacketList.map((item: any) => {
                return (
                  <RtItem
                    key={item.id}
                    tabType={activeTab}
                    item={item}
                    onUse={this.toUse}
                    onDetail={this.toDetail}
                  ></RtItem>
                );
              })}
            </Block>
          )}

          {/*  未拆红包  */}
          {activeTab === 'toUnZip' && (
            <Block>
              {redPacketList.map((item: any) => {
                return (
                  <RtItem
                    key={item.id}
                    tabType={activeTab}
                    item={item}
                    onShare={this.toShare}
                    onDetail={this.toDetail}
                  ></RtItem>
                );
              })}
            </Block>
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
          <View className='shim'></View>
        </ScrollView>
        <CashDialog ref={(node) => (this.cashDialogCMPT = node!)} onCashSelect={this.cashSelect}></CashDialog>
        {/*  分享对话弹框  */}
        <ShareDialog
          childRef={this.shareDialog}
          posterData={pendingSharedRedPacket}
          posterType='red-packet'
          posterHeaderType={pendingSharedRedPacket.posterType || 1}
          posterTips={pendingSharedRedPacket.posterContent || '一起来拆红包呀'}
          posterLogo={pendingSharedRedPacket.posterLogo}
          onSave={this.onSavePosterImage}
          qrCodeParams={qrCodeParams}
        ></ShareDialog>
        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='red-packet-share-canvas'></Canvas>
      </View>
    );
  }
}

export default RedPacketList;
