import React, { ComponentClass } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../../wxat-common/utils/money.wxs';
import utils from '../../../../../../wxat-common/utils/index';
import constants from '../../../../../../wxat-common/constants/index';
import template from '../../../../../../wxat-common/utils/template';

import ShareButton from '../../share-button/index';
import './index.scss';

import { connect } from 'react-redux';

type StateProps = {
  templ: any;
};

type RTItemProps = {
  item: any;
  tabType: string;
  onUse?: Function;
  onDetail?: Function;
  onShare?: Function;
};

interface RTItem {
  props: RTItemProps & StateProps;
}

const mapStateToProps = () => {
  return {
    templ: template.getTemplateStyle(),
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class RTItem extends React.Component {
  state = {
    luckyMoneyNo: 0,
    endTime: 0,
    remain: '',
    luckyMoneyPlanId: 0,
    luckyMoneyFee: '',
    totalFee: '',
    thresholdFeeLabel: '',
    isAvailable: false,
    planName: '',
    code: '',
    cashWithdrawalStatus: 0,
    sharePacket: {},
    status: 0,
    statusLabel: '',
    autoCashingSwitch: 0,
  };

  componentDidMount(): void {
    this.init();
  }

  init = () => {
    const packet = this.props.item;
    const isAvailable = packet.useStatus === constants.useStatus.AVAILABLE.value;
    const luckyMoneyPlanId = packet.luckyMoneyPlanId || '';
    const luckyMoneyNo = packet.luckyMoneyNo || '';
    const autoCashingSwitch = packet.autoCashingSwitch || 0;

    this.setState({
      sharePacket: {
        luckyMoneyPlanId,
        isAvailable,
        luckyMoneyNo,
      },

      luckyMoneyNo,
      endTime: packet.endTime,
      remain: packet.remain || '',
      luckyMoneyPlanId,
      code: packet.code,
      luckyMoneyFee: utils.money.fen2Yuan(packet.luckyMoneyFee),
      totalFee: utils.money.fen2Yuan(packet.totalFee),
      thresholdFeeLabel: packet.thresholdFee > 0 ? `满${utils.money.fen2Yuan(packet.thresholdFee)}元可用` : '无门槛',
      isAvailable,
      planName: packet.planName || '',
      cashWithdrawalStatus: packet.cashWithdrawalStatus || 0,
      status: packet.status || 0,
      autoCashingSwitch,
    });
  };

  toDetail = ({ luckyMoneyNo }) => {
    if (typeof this.props.onDetail === 'function') {
      this.props.onDetail({ luckyMoneyNo });
    }
  };

  toShare = (detail) => {
    const luckyMoneyNo = detail.luckyMoneyNo;
    const luckyMoneyPlanId = detail.luckyMoneyPlanId;
    if (typeof this.props.onShare === 'function') {
      this.props.onShare({ luckyMoneyNo, luckyMoneyPlanId });
    }
  };

  toUse = ({ luckyMoneyNo, isAvailable, code, cashWithdrawalStatus, luckyMoneyPlanId }) => {
    if (typeof this.props.onUse === 'function') {
      this.props.onUse({ luckyMoneyNo, luckyMoneyPlanId, isAvailable, code, cashWithdrawalStatus });
    }
  };

  render() {
    const {
      luckyMoneyNo,
      statusLabel,
      isAvailable,
      cashWithdrawalStatus,
      autoCashingSwitch,
      luckyMoneyFee,
      planName,
      thresholdFeeLabel,
      endTime,
      totalFee,
      remain,
      luckyMoneyPlanId,
      code,
      sharePacket,
      status,
    } = this.state;

    const { tabType, templ } = this.props;

    return (
      <View
        data-scoped='wk-rrr-RtItem'
        className='wk-rrr-RtItem rp-item'
        onClick={this.toDetail.bind(this, { luckyMoneyNo: luckyMoneyNo })}
      >
        {!!(tabType === 'toUse' && !isAvailable) && <Text className='use-label'>{statusLabel}</Text>}
        <Image src="https://htrip-static.ctlife.tv/wk/redpacket-icon.png" className='packet-icon'></Image>
        {/*  提现红包 or 已拆红包  */}
        {tabType !== 'toUnZip' && (
          <View className='packet-info'>
            <View className='info-money'>
              <Text className='desc'>拆红包获得</Text>
              {tabType === 'toInCash' ? (
                <Text
                  className='desc money-fee'
                  style={_safe_style_(
                    'color:' +
                      (cashWithdrawalStatus === 2 || autoCashingSwitch === 1 ? '#A8A8A8' : templ.btnColor) +
                      ';'
                  )}
                >
                  {luckyMoneyFee}
                </Text>
              ) : (
                <Text
                  className='desc money-fee'
                  style={_safe_style_('color:' + (!isAvailable ? '#A8A8A8' : templ.btnColor) + ';')}
                >
                  {luckyMoneyFee}
                </Text>
              )}

              <Text className='desc'>元</Text>
            </View>
            {tabType === 'toInCash' ? (
              <View className='info-tip limit-line info-name'>{planName}</View>
            ) : (
              <View className='info-tip'>{thresholdFeeLabel}</View>
            )}

            {!!(!!endTime && tabType !== 'toInCash') && (
              <View className='info-tip'>{'有效期：' + filters.dateFormat(endTime, 'yyyy-MM-dd')}</View>
            )}
          </View>
        )}

        {/*  未拆红包  */}
        {tabType === 'toUnZip' && (
          <View className='packet-info'>
            <View className='info-money'>
              <Text className='desc'>一起瓜分</Text>
              <Text className='desc money-fee' style={_safe_style_('color: ' + templ.btnColor)}>
                {totalFee}
              </Text>
              <Text className='desc'>元</Text>
            </View>
            <View className='info-tip' style={_safe_style_('margin-top: 18rpx;')}>
              {'还差' + remain + '个人即可拆红包'}
            </View>
          </View>
        )}

        {/*  提现红包  */}
        {!!(tabType === 'toInCash' && autoCashingSwitch === 0) && (
          <View
            className='packet-btn'
            style={_safe_style_('color:' + (cashWithdrawalStatus === 2 ? '#A8A8A8' : templ.btnColor) + ';')}
            onClick={this.toUse.bind(this, {
              isAvailable: isAvailable,
              luckyMoneyNo: luckyMoneyNo,
              luckyMoneyPlanId: luckyMoneyPlanId,
              cashWithdrawalStatus: cashWithdrawalStatus,
              code: code,
            })}
          >
            {cashWithdrawalStatus === 0 ? '去提现' : cashWithdrawalStatus === 1 ? '提现中' : '已提现'}
          </View>
        )}

        {/*  已拆红包  */}
        {tabType === 'toUse' && (
          <View
            className='packet-btn'
            style={_safe_style_('color:' + (isAvailable ? templ.btnColor : '#A8A8A8') + ';')}
            onClick={this.toUse.bind(this, { isAvailable: isAvailable, luckyMoneyNo: luckyMoneyNo })}
          >
            {!isAvailable ? '已使用' : '去使用'}
          </View>
        )}

        {/*  未拆红包  */}
        {!!(tabType === 'toUnZip' && status === 0) && (
          <View className='share-button'>
            <ShareButton
              width='210rpx'
              text='邀请好友一起拆'
              height='74rpx'
              fontSize='26rpx'
              paddingLeft='0'
              paddingRight='0'
              color={templ.btnColor}
              onShare={this.toShare}
              redPacket={sharePacket}
            ></ShareButton>
          </View>
        )}

        {!!(tabType === 'toUnZip' && status !== 0) && (
          <View className='packet-btn' style={_safe_style_('color:#A8A8A8')}>
            已结束
          </View>
        )}
      </View>
    );
  }
}

export default RTItem as ComponentClass<RTItemProps>;
