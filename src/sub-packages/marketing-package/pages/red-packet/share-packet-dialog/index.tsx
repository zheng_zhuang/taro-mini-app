import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';

/**
 * 邀请好友拆红包的弹窗
 */
import SharePacket from '../share-packet/index';
import Dialog from '../../../../../wxat-common/components/dialog/index';
import './index.scss';

class SharePacketDialog extends React.Component {
  static defaultProps = {
    redPacket: null,
  };

  /**
   * 组件的初始数据
   */
  state = {
    showSharePacket: false,
  };

  isShow = () => {
    return this.state.showSharePacket;
  };
  show = () => {
    this.setState({
      showSharePacket: true,
    });
  };
  hide = () => {
    this.setState({
      showSharePacket: false,
    });
  };
  onCancelClick = () => {
    this.setState({
      showSharePacket: false,
    });
  };
  onShare = () => {
    this.props.onShare();
  };
  render() {
    const { showSharePacket } = this.state;
    const { redPacket } = this.props;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-prs-SharePacketDialog'
        className='wk-prs-SharePacketDialog'
      >
        <Dialog
          visible={showSharePacket}
          top='45%'
          width={Taro.pxTransform(500)}
          borderRadius={Taro.pxTransform(20)}
          showTitle={false}
          showFooter={false}
          contenMargin='0 0'
          contentPadding='0 0'
          backOpacity='0.615'
        >
          <View className='share-unpack-container'>
            <SharePacket redPacket={redPacket} onShare={this.onShare} />
            <Image
              src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png"
              className='cancel-icon'
              onClick={this.onCancelClick}
            />
          </View>
        </Dialog>
      </View>
    );
  }
}

export default SharePacketDialog;
