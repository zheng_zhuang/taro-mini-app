import React, { FC, useMemo, useRef } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import constants from '@/wxat-common/constants/index.js';
import report from '@/sdks/buried/report/index.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';

import subscribeEnum from '@/wxat-common/constants/subscribeEnum.js';

import ShareButton from "../share-button";
import './index.scss';

export interface RedPacketContentProps {
  remainTime?: number;
  time?: number;
  onJoined: () => void;
  onShare: () => void;
  onUnpackNew: () => void;
  redPacket: {
    endTime: number;
    hasThresholdFee: boolean;
    luckyMoneyBackgroundType: number;
    luckyMoneyBackgroundUrl?: string;
    luckyMoneyFee?: number;
    luckyMoneyNo: string;
    luckyMoneyPlanId: number;
    planStatus: number;
    quantity: number;
    remain: number;
    rules: string;
    status: number;
    successTime?: number;
    suitCurrentStore: boolean;
    thresholdFee: number;
    totalFee: string;
    useStatus: unknown;
    userId: number;
    users: Array<{
      avatarImgUrl: string;
      createTime: number;
      luckyMoneyFee?: string;
      luckyMoneyFeeLabel?: string;
      luckyMoneyNo: number;
      nickname: string;
      useStatus: unknown;
      userId: number;
    }>;
  };
}

const RedPacketContent: FC<RedPacketContentProps> = (props) => {
  const { redPacket, remainTime, time } = props;
  const { base } = useSelector((state) => ({
    base: state.base,
  }));

  const joining = useRef(false);

  const currentUser = useMemo(() => {
    return (
      redPacket &&
      redPacket.users.find((item) => {
        return item.userId === base.userInfo.id;
      })
    );
  }, [redPacket]);

  const topFourUsers = useMemo(() => {
    return redPacket && redPacket.users.slice(0, 4);
  }, [redPacket]);

  // 活动进行中
  const ongoing = useMemo(() => {
    return redPacket && redPacket.status === constants.redPacket.ONGOING.value;
  }, [redPacket]);

  // 红包拆分成功
  const success = useMemo(() => {
    return redPacket && redPacket.status === constants.redPacket.SUCCESS.value;
  }, [redPacket]);

  // 红包活动时间
  const activictySuccess = useMemo(() => {
    return redPacket && redPacket.planStatus === constants.redPacketActivictyStatus.ONGOING.value;
  }, [redPacket]);

  // 活动失败
  const fail = useMemo(() => {
    return redPacket && redPacket.status === constants.redPacket.FAIL.value;
  }, [redPacket]);

  // 是否是发起者
  const isInitiator = useMemo(() => {
    return redPacket && redPacket.userId === base.loginInfo.userId;
  }, [redPacket]);

  // 消费门槛
  const thresholdTip = useMemo(() => {
    return redPacket && redPacket.hasThresholdFee ? `满${redPacket.thresholdFee}元可用` : '无';
  }, [redPacket]);

  const canIUse = useMemo(() => {
    return redPacket && redPacket.useStatus === constants.useStatus.AVAILABLE.value;
  }, [redPacket]);

  const useStatus = useMemo(() => {
    if (!redPacket) {
      return '';
    } else {
      const curStatusKey = Object.keys(constants.useStatus).find((key) => {
        return constants.useStatus[key].value === redPacket.useStatus;
      });
      const curStatus = constants.useStatus[curStatusKey];
      return curStatus ? curStatus.label : '';
    }
  }, [redPacket]);

  const suitCurrentStore = useMemo(() => {
    return redPacket && redPacket.suitCurrentStore;
  }, [redPacket]);

  const handleJoinUnpackRedPacket = () => {
    const Ids = [subscribeEnum.REDPACKETS_SUCCESS.value];

    // subscribeMsg.sendMessage(Ids).then(() => {
    if (joining.current) {
      return false;
    }
    joining.current = true;
    wxApi
      .request({
        url: api.redPacket.join,
        data: {
          luckyMoneyNo: redPacket.luckyMoneyNo,
        },

        loading: true,
      })
      .then((result) => {
        if (result.data) {
          props.onJoined();
        }
        report.openRedPacket(!!result.data);
      })
      .finally(() => {
        joining.current = false;
      });
    // });
  };

  // 使用红包
  const onUsePacketClick = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  // 一起拆红包
  const onUnpackClick = () => {
    if (authHelper.checkAuth()) {
      handleJoinUnpackRedPacket();
    }
  };

  // 拆一个新红包
  const onUnpackNewClick = () => {
    if (authHelper.checkAuth()) {
      props.onUnpackNew();
      report.openNewRecdPacket();
    }
  };

  const onShare = () => {
    props.onShare();
  };

  return (
    <View data-scoped='wk-prr-RedPacketContent' className='wk-prr-RedPacketContent unpack-red-packet-container'>
      {!!ongoing && (
        <View className={'wait-unpack-container ' + (isInitiator ? 'initiator' : 'joiner')}>
          <View className='join-vip-icon-container'>
            {!!topFourUsers &&
              topFourUsers.map((user, index) => {
                return (
                  <Image
                    className='vip-avator'
                    src={user.avatarImgUrl || getStaticImgUrl.wxatCommon.defaultImg_png}
                    key={user.userId}
                  ></Image>
                );
              })}
            {redPacket.users.length > 4 && <Text>...</Text>}
          </View>
          <View className='unpack-info'>
            {'差' + redPacket.remain + '人瓜分' + redPacket.totalFee + '元红包, 邀好友拆红包'}
          </View>
          {!!remainTime && (
            <View className='activity-deadline'>
              {remainTime}
              {time !== 0 && <Text>后结束</Text>}
            </View>
          )}
        </View>
      )}

      {/*  红包拆分成功后的显示内容  */}
      {!!success && (
        <View className='upack-success-container'>
          {currentUser ? (
            <Block>
              <View className='success-tip'>恭喜您已成功瓜分红包</View>
              <View>
                <View className='packet-info'>
                  {currentUser.luckyMoneyFeeLabel}
                  <Text className='unit'>元</Text>
                </View>
                {false && <View>{'消费门槛：' + thresholdTip}</View>}
              </View>
            </Block>
          ) : (
            <Block>
              <View className='packet-info'>该红包已分完</View>
            </Block>
          )}
        </View>
      )}

      {/*  各个操作按钮  */}
      <View>
        {!!(ongoing && isInitiator) && <ShareButton onShare={onShare} width='460rpx'></ShareButton>}

        {/*  活动成功显示立即使用  */}
        {!!(success && !!currentUser && canIUse) && (
          <View className='btn bg-red' onClick={onUsePacketClick}>
            立即使用
          </View>
        )}

        {/*  红包使用状态  */}
        {!!(success && !!currentUser && !canIUse) && <View className='btn bg-disabled'>{useStatus}</View>}

        {/*  不是发起者时，并且活动正在进行中  */}
        {!!(!isInitiator && !success && !currentUser) && (
          <View className='btn bg-red' onClick={onUnpackClick}>
            帮他助力
          </View>
        )}

        {/*  活动成功或者不是发起者  */}
        {!!((success || !isInitiator) && activictySuccess && suitCurrentStore) && (
          <View className='btn bg-orange' onClick={onUnpackNewClick}>
            拆一个新红包
          </View>
        )}
      </View>
      <View className='btn bg-orange' onClick={onUsePacketClick}>
        看更多商品
      </View>
    </View>
  );
};

RedPacketContent.defaultProps = {
  redPacket: undefined,
  remainTime: 0,
  time: 1,
};

export default RedPacketContent;
