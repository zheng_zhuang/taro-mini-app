import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import wxApi from '../../../../wxat-common/utils/wxApi';
import api from '../../../../wxat-common/api/index.js';
import money from '../../../../wxat-common/utils/money.js';
import date from '../../../../wxat-common/utils/date.js';
import timer from '../../../../wxat-common/utils/timer.js';
import constants from '../../../../wxat-common/constants/index.js';
import report from '../../../../sdks/buried/report/index.js';
import login from '../../../../wxat-common/x-login/index.js';
import utils from '../../../../wxat-common/utils/util.js';
import ShareDialog from '../../../../wxat-common/components/share-dialog/index';
import ProductModule from '../../../../wxat-common/components/base/productModule';
import SharePacketDialog from './share-packet-dialog/index';
import JoinVipList from './join-vip-list/index';
import RedPacketContent from './red-packet-content/index';
import ReviceRedPacket from './receive-red-packet/index';
import './index.scss';
import shareUtil from '@/wxat-common/utils/share';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';

const mapStateToProps = (state) => ({
  base: state.base,
  globalData: state.globalData,
});

const ONE_DAY = 24 * 3600;

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class RedPacket extends React.Component {
  $router = $getRouter();
  RefShareRedDialogCMTP = React.createRef();
  shareUnPackDialogRef = React.createRef<SharePacketDialog>();

  state = {
    // 红包的唯一标识
    redPacketNo: null,
    // 新红包信息
    planRedPacket: null,
    // 领取过的红包记录
    recordRedPacket: null,
    // 推荐商品列表
    promoteProducts: null,

    showEmptyPlanPacket: false,

    // 活动状态
    planStatus: null,
    luckyMoneyBackgroundUrl: null,
    time: 1,
    displayTime: 0,
    pendingSharedRedPacket: {} as Record<string, any>,
    qrCodeParams: {
      verificationNo: null,
      verificationType: 5,
      maPath: 'sub-packages/marketing-package/pages/red-packet/index',
    },

    emptyText: '抱歉，红包活动已结束',
  };

  componentDidMount() {
    // 隐藏转发按钮
    wxApi.hideShareMenu();
    let redPacketNo = null;
    if (!!this.$router.params.scene) {
      const scene = '?' + decodeURIComponent(this.$router.params.scene);
      redPacketNo = utils.getQueryString(scene, 'No');
    } else {
      redPacketNo = this.$router.params.redPacketNo || null;
    }
    this.setState(
      {
        redPacketNo,
      },

      () => {
        // 如果有redPacketNo，说明该红包已经在拆开的过程中，直接获取红包记录；否则，获取红包计划，进行领取
        if (this.state.redPacketNo) {
          this.setState({
            'qrCodeParams.verificationNo': this.state.redPacketNo,
          });

          this.handleCheckRedPacketRecord();
          this.handleListPromoteProducts();
        } else {
          this.handleCheckRedPacketPlan();
        }
      }
    );
  }
  componentDidShow(): void {
    this._thredId = timer.addQueue(() => {
      this.updateTime();
    });
  }
  componentDidHide(): void {
    if (this._thredId) {
      timer.deleteQueue(this._thredId);
      this._thredId = null;
    }
  }

  onShareAppMessage = () => {
    report.share(true);
    this.getShareDialog().hide();
    const appName = this.props.globalData.maAppName;
    const path = shareUtil.buildShareUrlPublicArguments({
      url: '/sub-packages/marketing-package/pages/red-packet/index?redPacketNo=' + this.state.redPacketNo,
      bz: shareUtil.ShareBZs.FISSION_RED_PACKET_DETAIL,
      bzName: `裂变红包${this.state.recordRedPacket ? '-' + this.state.recordRedPacket.totalFee + '元红包' : ''}`,
      bzId: this.state.redPacketNo,
      sceneName: this.state.recordRedPacket ? this.state.recordRedPacket.totalFee + '元红包' : '',
    });

    console.log('sharePath => ', path);
    return {
      title: `${appName ? appName : ''}送你 ${this.state.recordRedPacket.totalFee} 元红包，点击领取>`,
      path,
      imageUrl: 'https://htrip-static.ctlife.tv/wk/ic-share-img.png',
      success: (res) => {
        report.shareRedPacket(true);
        //分享成功后隐藏弹窗
        console.log(this.RefShareRedDialogCMTP);
        if (this.shareUnPackDialogRef.current && this.shareUnPackDialogRef.current.isShow()) {
          this.shareUnPackDialogRef.current.hide();
        }
      },
      fail: (res) => {
        report.share(false);
        report.shareRedPacket(false);
      },
    };
  };
  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel = () => {
    if (this.state.pendingSharedRedPacket.name) {
      this.hideShareTipDialog();
      this.getShareDialog().show();
    } else {
      wxApi
        .request({
          url: api.redPacket.planInfo,
          data: {
            planId: this.state.recordRedPacket.luckyMoneyPlanId,
          },
        })
        .then((res) => {
          console.log(res);
          this.setState({
            pendingSharedRedPacket: res.data,
          });

          this.hideShareTipDialog();
          this.getShareDialog().show();
        });
    }
  };

  /**
   * 获取邀请好友对话弹框
   */
  getShareDialog() {
    if (!!this.RefShareRedDialogCMTP.current) {
      this._shareDialog = this.RefShareRedDialogCMTP.current;
    }
    return this._shareDialog;
  }

  // 保存图片
  onSavePosterImage = (e) => {
    this.getShareDialog().savePosterImage(this);
  };

  //推荐的商品列表
  handleListPromoteProducts = () => {
    login.login().then(() => {
      wxApi
        .request({
          url: api.classify.itemSkuList,
          data: {
            pageNo: 1,
            pageSize: 10,
            status: 1,
            isShelf: 1,
          },

          loading: false,
          quite: true,
        })
        .then((result) => {
          this.setState({
            promoteProducts: result.data || [],
          });
        });
    });
  };
  // 获取红包计划
  handleCheckRedPacketPlan = () => {
    if (this.state.planStatus === constants.redPacketActivictyStatus.FINISH.value) {
      this.setState({ showEmptyPlanPacket: true });
    }
    this.setState({ showEmptyPlanPacket: false });
    wxApi
      .request({
        url: api.redPacket.plan,
        loading: true,
      })
      .then((result) => {
        if (result.data) {
          const planRedPacket = result.data;
          this.parseRedPacket(planRedPacket);
          this.setState({
            planRedPacket: planRedPacket,
            luckyMoneyBackgroundUrl: planRedPacket.luckyMoneyBackgroundUrl,
          });
        } else {
          this.setState({ showEmptyPlanPacket: true });
        }
      });
  };

  // 获取红包领取记录
  handleCheckRedPacketRecord = () => {
    login.login().then((res) => {
      wxApi
        .request({
          url: api.redPacket.record,
          data: {
            luckyMoneyNo: this.state.redPacketNo,
          },

          loading: true,
        })
        .then((result) => {
          if (result.data) {
            /*红包拆失败时，活动结束时，不展示红包信息，直接提示活动已结束*/
            if (
              result.data.status === constants.redPacket.FAIL.value ||
              result.data.status === constants.redPacket.ACTIVITY_FINISH.value
            ) {
              const emptyText =
                result.data.status === constants.redPacket.FAIL.value
                  ? '红包已过期，快去重新拆包吧～'
                  : this.state.emptyText;

              this.setState({
                showEmptyPlanPacket: true,
                emptyText,
              });
            } else {
              const recordRedPacket = result.data;
              this.parseRedPacket(recordRedPacket);
              this.setState({
                recordRedPacket: recordRedPacket,
                planStatus: recordRedPacket.planStatus,
                luckyMoneyBackgroundUrl: recordRedPacket.luckyMoneyBackgroundUrl,
                qrCodeParams: {
                  ...this.state.qrCodeParams,
                  verificationNo: recordRedPacket.luckyMoneyNo,
                },
              });

              this.updateTime();

              if (this.isInitiator() && this.isUnpackOngoing()) {
                this.showShareTipDialog();
              }
            }
          } else {
            wxApi.showToast({
              title: '获取红包失败',
              icon: 'none',
            });
          }
        })
        .catch((error) => {
          console.log('error--->', error);
          wxApi.showToast({
            title: '获取红包失败',
            icon: 'none',
          });
        });
    });
  };

  updateTime = () => {
    if (!(this.state.recordRedPacket && this.state.recordRedPacket.endTime)) {
      return;
    }

    // 结束时间
    const endTime = this.state.recordRedPacket.endTime;

    const nowDate = new Date();
    const endDate = new Date(endTime);

    let remainTime = (endDate.getTime() - nowDate.getTime()) / 1000;

    if (remainTime <= 0) {
      this.setState({
        displayTime: '抱歉，红包活动已结束',
        time: 0,
      });

      return;
    }
    const day = parseInt(remainTime / ONE_DAY);
    let mod = remainTime % ONE_DAY;
    const hour = date.appendZero(parseInt(mod / 3600));
    mod = mod % 3600;
    const minute = date.appendZero(parseInt(mod / 60));
    const second = date.appendZero(parseInt(mod % 60));
    const displayTime = day ? `${day}天${hour}:${minute}:${second}` : `${hour}:${minute}:${second}`;

    remainTime -= 1;

    this.setState({
      displayTime: displayTime,
      time: remainTime,
    });
  };

  //解析红包字段，并重新包装
  parseRedPacket = (redPacket) => {
    // 总价
    if (redPacket.totalFee) {
      redPacket.totalFee = money.fen2Yuan(redPacket.totalFee);
    }

    // 增加消费门槛标识
    redPacket.hasThresholdFee = !!redPacket.thresholdFee;
    // 消费门槛
    if (redPacket.thresholdFee) {
      redPacket.thresholdFee = money.fen2Yuan(redPacket.thresholdFee);
    }

    // 用户信息
    if (redPacket.users && redPacket.users.length) {
      redPacket.users.forEach((user) => {
        user.luckyMoneyFeeLabel = money.fen2Yuan(user.luckyMoneyFee);
      });
    }
  };

  // 是否是发起者
  isInitiator = () => {
    return this.state.recordRedPacket && this.state.recordRedPacket.userId === this.props.base.loginInfo.userId;
  };

  isUnpackSuccess = () => {
    return this.state.recordRedPacket && this.state.recordRedPacket.status === constants.redPacket.SUCCESS.value;
  };

  isUnpackOngoing = () => {
    return this.state.recordRedPacket && this.state.recordRedPacket.status === constants.redPacket.ONGOING.value;
  };

  showShareTipDialog = () => {
    if (!!this.shareUnPackDialogRef.current) {
      this.shareUnPackDialogRef.current.show();
    }
  };

  hideShareTipDialog = () => {
    if (!!this.shareUnPackDialogRef.current) {
      this.shareUnPackDialogRef.current.hide();
    }
  };

  /**
   * 红包领取成功后的回调
   * @param redPacketNo 红包的唯一标识
   */
  onRedPacketReceived = (e) => {
    this.setState(
      {
        redPacketNo: e,
        qrCodeParams: {
          ...this.state.qrCodeParams,
          verificationNo: e,
        },
      },

      () => {
        this.handleCheckRedPacketRecord();
      }
    );
  };

  //查看红包活动规则
  onActivityRuleClick = () => {
    const redPacket = this.state.planRedPacket || this.state.recordRedPacket;
    wxApi.showModal({
      showCancel: false,
      title: '活动规则',
      content: redPacket.rules,
    });
  };

  //回到首页
  handleToHome = () => {
    let url = '/wxat-common/pages/home/index';
    if (this.props.globalData.tabbars && this.props.globalData.tabbars.list) {
      const tabbars = this.props.globalData.tabbars.list;
      url = tabbars[0].pagePath;
    }
    wxApi.$navigateTo({
      url: url,
    });
  };

  onJoinedUnpackRedPacket = () => {
    this.handleCheckRedPacketRecord();
  };
  handleShowNewRedPacket = () => {
    this.setState({
      redPacketNo: null,
      planRedPacket: null,
      recordRedPacket: null,
    });

    this.handleCheckRedPacketPlan();
  };

  render() {
    const {
      luckyMoneyBackgroundUrl,
      planRedPacket,
      recordRedPacket,
      redPacketNo,
      emptyText,
      showEmptyPlanPacket,
      time,
      displayTime,
      promoteProducts,
      pendingSharedRedPacket,
      qrCodeParams,
    } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-mpr-RedPacket' className='wk-mpr-RedPacket'>
        <View className='red-packet-container'>
          {luckyMoneyBackgroundUrl ? (
            <Image src={luckyMoneyBackgroundUrl} className='red-packet-bg'></Image>
          ) : (
            <Image src="https://htrip-static.ctlife.tv/wk/redpacket-bg.png" className='red-packet-bg'></Image>
          )}

          {/*  活动规则  */}
          {!!(planRedPacket || recordRedPacket) && (
            <View className='activity-rule-btn' onClick={this.onActivityRuleClick}>
              活动规则
            </View>
          )}

          {/*  回到首页  */}
          <View className='to-home-btn' onClick={this.handleToHome}>
            返回首页
          </View>

          {/*  领取红包  */}
          {!!(!redPacketNo && planRedPacket) && (
            <ReviceRedPacket packet={planRedPacket} onReceived={this.onRedPacketReceived} />
          )}

          {!!showEmptyPlanPacket && (
            <View className='empty-red-packet-plan'>
              <Text>{emptyText}</Text>
            </View>
          )}

          {/*  包括拆红包、推荐商品等 */}
          {!!(redPacketNo && recordRedPacket) && (
            <View className='unpack-main-container'>
              <View className='box'>
                <RedPacketContent
                  redPacket={recordRedPacket}
                  time={time}
                  remainTime={displayTime}
                  onShare={this.handleSelectChanel}
                  onUnpackNew={this.handleShowNewRedPacket}
                  onJoined={this.onJoinedUnpackRedPacket}
                />
              </View>
              <View className='box vip-list'>
                {this.isUnpackSuccess() && <JoinVipList unpackUsers={recordRedPacket.users}></JoinVipList>}
              </View>
              {/*  推荐商品label  */}
              {!!(!!promoteProducts && !!promoteProducts.length) && (
                <View className='promote-product-label'>
                  <Image src className='line'></Image>
                  <Text className='label'>更多商品推荐</Text>
                  <Image src className='line'></Image>
                </View>
              )}

              {/*  推荐商品列表  */}
              {!!(!!promoteProducts && !!promoteProducts.length) && (
                <ProductModule list={promoteProducts}></ProductModule>
              )}
            </View>
          )}
        </View>
        <SharePacketDialog
          redPacket={recordRedPacket}
          onShare={this.handleSelectChanel}
          ref={this.shareUnPackDialogRef}
        />

        {/*  分享对话弹框  */}
        <ShareDialog
          id='share-dialog'
          posterData={pendingSharedRedPacket}
          posterType='red-packet'
          posterHeaderType={pendingSharedRedPacket.posterType || 1}
          posterTips={pendingSharedRedPacket.posterContent || '一起来拆红包呀'}
          posterLogo={pendingSharedRedPacket.posterLogo}
          onSave={this.onSavePosterImage}
          qrCodeParams={qrCodeParams}
          childRef={this.RefShareRedDialogCMTP}
        />

        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='red-packet-share-canvas'></Canvas>
        <AuthPuop></AuthPuop>
      </View>
    );
  }
}

export default RedPacket;
