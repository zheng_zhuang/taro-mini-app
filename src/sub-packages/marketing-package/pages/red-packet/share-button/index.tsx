import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, Button, View } from '@tarojs/components';
import Taro from '@tarojs/taro';

import './index.scss';

class ShareButton extends React.Component {
  static defaultProps = {
    redPacket: null,
    width: '501rpx',
    height: '94rpx',
    fontSize: '36rpx',
    paddingLeft: '14rpx',
    paddingRight: '14rpx',
    text: '邀请好友帮我助力',
    color: '',
  };

  onShare = (e) => {
    e.stopPropagation();
    this.props.onShare(this.props.redPacket);
  };
  render() {
    const { width, height, fontSize, paddingLeft, paddingRight, color, text } = this.props;
    return (
      <Button
        data-scoped='wk-prs-ShareButton'
        className='wk-prs-ShareButton btn'
        style={_safe_style_(
          'width:' +
            width +
            ';height: ' +
            height +
            ';font-size: ' +
            fontSize +
            ';line-height: ' +
            height +
            ';padding-left:' +
            paddingLeft +
            ';padding-right:' +
            paddingRight +
            ';background:' +
            color +
            ';'
        )}
        onClick={this.onShare}
      >
        {text}
      </Button>
    );
  }
}

export default ShareButton;
