import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import utils from "../../../../../wxat-common/utils";

/**
 * 加入拆红包的vip列表
 */
import './index.scss';

class JoinVipList extends React.Component {
  static defaultProps = {
    unpackUsers: null,
  };

  state = {
    luckestUser: null,
  }; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    const luckestUser = this.getLuckestUser();
    this.setState({
      luckestUser: luckestUser,
    });
  }
  getLuckestUser() {
    return utils.arrayUtil.findMax(
      this.props.unpackUsers,
      (u1, u2) => {
        return u1.luckyMoneyFee < u2.luckyMoneyFee;
      },
      true
    );
  }

  render() {
    const { unpackUsers } = this.props;
    const { luckestUser } = this.state;
    return (
      <View data-scoped='wk-prj-JoinVipList' className='wk-prj-JoinVipList join-vip-list-container'>
        <View className='title-container'>看大家手气</View>
        {!!unpackUsers &&
          unpackUsers.map((user, index) => {
            return (
              <View className='vip-item-container' key={index}>
                <Image
                  className='avator'
                  src={user.avatarImgUrl || getStaticImgUrl.wxatCommon.defaultImg_png}
                ></Image>
                <View className='nickName'>{user.nickname || '未知'}</View>
                <View className='red-packet'>
                  <Text className='money'>{user.luckyMoneyFeeLabel}</Text>
                  <Text className='unit'>元</Text>
                </View>
                {user.userId === luckestUser.userId && <View className='best-luck'>手气最佳</View>}
              </View>
            );
          })}
      </View>
    );
  }
}

export default JoinVipList;
