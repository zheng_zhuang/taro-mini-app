import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../../../../wxat-common/api/index.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import utils from '../../../../../wxat-common/utils/index.js';
import template from '../../../../../wxat-common/utils/template.js';

import LoadMore from '../../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../../wxat-common/components/error/error';
import Empty from '../../../../../wxat-common/components/empty/empty';
import './index.scss';

const loadMoreStatus = {
  HIDE: 0,
  LOADING: 1,
  ERROR: 2,
};

const Type = {
  toInCash: 'toInCash',
  toUse: 'toUse',
  toUnZip: 'toUnZip',
};

class RedPacketFlow extends React.Component {
  state = {
    error: false,
    flowList: [],
    pageNo: 1,
    hasMore: true,
    nothing: false,
    loadMoreStatus: loadMoreStatus.HIDE,
    tmpStyle: {},
  };

  componentDidMount(): void {
    wxApi.hideShareMenu();
    this.getFlowList();
    this.getTemplateStyle();
  }
  onPullDownRefresh() {
    this.state.pageNo = 1;
    this.state.hasMore = true;
    this.getFlowList(true);
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.getFlowList();
    }
  }

  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.getFlowList();
  };

  getFlowList(isFromPullDown) {
    this.setState({ error: false });

    wxApi
      .request({
        url: api.redPacket.flow,
        loading: true,
        data: {
          pageNo: this.state.pageNo,
          pageSize: 100,
        },
      })
      .then((res) => {
        let flowList = [],
          result = res.data || [];

        result.forEach((v, i) => {
          let tag = result[i].amount[0],
            amount = Number(result[i].amount.substr(1));

          result[i].type = tag === '+' ? 0 : 1;
          result[i].amount = tag + '￥' + utils.money.fen2Yuan(amount);
          result[i].createTime = utils.date.format(new Date(result[i].createTime), 'yyyy-MM-dd hh:mm:ss');
        });
        if (this.isLoadMoreRequest()) {
          flowList = this.state.flowList.concat(result);
        } else {
          flowList = result;
        }

        this.setState({
          pageNo: this.state.pageNo + 1,
          hasMore: flowList.length !== res.totalCount,
          nothing: flowList.length ? false : true,
          flowList,
        });
      })
      .catch((error) => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  }

  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }
  //获取模板配置
  getTemplateStyle() {
    const tmpStyle = template.getTemplateStyle();
    this.setState({ tmpStyle });
    if (tmpStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: tmpStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { nothing, error, flowList, tmpStyle, loadMoreStatus } = this.state;
    return (
      <View data-scoped='wk-prr-RedPacketFlow' className='wk-prr-RedPacketFlow red-packet-flow'>
        {!!nothing && <Empty message='暂无红包流水'></Empty>}
        {!!error && <Error></Error>}
        {flowList.map((item, index) => {
          return (
            <View className='rp-flow-item' key={item.id}>
              <View>
                <View className='rp-flow-title limit-line'>{item.desc}</View>
                <View className='rp-flow-date'>{item.createTime}</View>
              </View>
              <View className='rp-flow-money' style={_safe_style_(item.type === 0 ? 'color:' + tmpStyle.btnColor : '')}>
                {item.amount}
              </View>
            </View>
          );
        })}
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default RedPacketFlow;
