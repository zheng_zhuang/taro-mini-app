import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
// pages/red-packet/share-packet/index.copy.js

/**
 * 分享给好友邀请拆红包
 */
import ShareButton from '../share-button/index';
import './index.scss';

class SharePacket extends React.Component {
  static defaultProps = {
    redPacket: {
      type: Object,
      value: null,
    },
  };

  onShare = () => {
    this.props.onShare();
  };
  render() {
    const { redPacket } = this.props;
    return (
      <View data-scoped='wk-prs-SharePacket' className='wk-prs-SharePacket share-unpack-container'>
        <Image src="https://htrip-static.ctlife.tv/wk/share-bg.png" className='packet-bg'></Image>
        <View className='packet-info-container'>
          <View className='info'>
            还差<Text className='info-value'>{redPacket.remain}</Text>
            个人即可拆红包
          </View>
          <View className='info'>
            一起瓜分<Text className='info-value'>{redPacket.totalFee}</Text>
            元红包
          </View>
          <ShareButton
            className='share-button'
            width='330rpx'
            redPacket={redPacket}
            onShare={this.onShare}
          ></ShareButton>
        </View>
      </View>
    );
  }
}

export default SharePacket;
