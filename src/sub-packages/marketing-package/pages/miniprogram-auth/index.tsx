import React from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import '@/wxat-common/utils/platform';
import { View, Image, Button, Block } from '@tarojs/components';
import Taro from '@tarojs/taro';
import login from '@/wxat-common/x-login';

import './index.scss';
import { connect } from 'react-redux';

type StateProps = {
  userId: number;
  appName: string;
};

interface MiniprogramAuth {
  props: StateProps;
}

const mapStateToProps = (state) => {
  return {
    userId: state.base.loginInfo && (state.base.loginInfo.userId as number),
    appName: state.base.appInfo && state.base.appInfo.appName,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class MiniprogramAuth extends React.Component {
  handleBack = () => {
    if (wxApi.canIUse('navigateBackMiniProgram')) {
      const { userId } = this.props;
      wxApi.navigateBackMiniProgram({ extraData: { userId } });
    }
  };

  handleQueryLogin = async () => {
    await login.login({ ignoreLoginInfo: true });
  };

  render() {
    const { userId, appName } = this.props;

    const showAppName = appName || '微商城';

    const renderTips = userId ? (
      <View className='desc'>身份激活后，就可以在微信端推广「{showAppName}」了哦</View>
    ) : (
      <Block>
        <View className='desc'>您未注册过该「{showAppName}」账号</View>
        <View className='desc'>请先前往微信-小程序搜索「{showAppName}」注册后，再激活导购账号</View>
      </Block>
    );

    return (
      <View data-scoped='wk-mpm-MiniprogramAuth' className='wk-mpm-MiniprogramAuth content'>
        <Image
          className='image'
          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/shopping-guide/auth.png'
        ></Image>
        <View className='title'>激活身份</View>
        {renderTips}
        <Button className='btn' onClick={this.handleBack}>
          {userId ? '立即激活' : '返回'}
        </Button>
        {userId ? null : (
          <View onClick={this.handleQueryLogin} className='refresh'>
            若已注册，请点击
            <View className='keyword'>刷新</View>
          </View>
        )}
      </View>
    );
  }
}

export default MiniprogramAuth;
