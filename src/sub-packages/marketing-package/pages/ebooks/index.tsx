import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, ScrollView, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
// pages/ebooks/index
import report from '../../../../sdks/buried/report/index';
import wxApi from '../../../../wxat-common/utils/wxApi';
import api from '../../../../wxat-common/api/index';
import imageUtils from '../../../../wxat-common/utils/image';
import pdfDownloader from './pdf-downloader';
import shareUtil from '../../../../wxat-common/utils/share';

import Empty from '../../../../wxat-common/components/empty/empty';
import './index.scss';
import hoc from '@/hoc';
import { connect } from 'react-redux';

type StateProps = {
  sessionId: '';
};

interface Ebooks {
  props: StateProps;
}

const mapStateToProps = (state) => ({
  sessionId: state.base.sessionId,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class Ebooks extends React.Component {
  state = {
    previewMsg: '',
    categoryList: [],
    categoryId: '',
    ebooksList: [],
    pageNo: 1,
    hasMore: true, // 是否可以加载更多
  };

  componentDidMount() {
    if (!!this.props.sessionId) {
      this.initRenderData();
    }
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.initRenderData();
    }
  }

  onShareAppMessage() {
    report.share(true);
    const path = shareUtil.buildShareUrlPublicArguments({
      url: '/sub-packages/marketing-package/pages/ebooks/index',
      bz: shareUtil.ShareBZs.E_BOOKS,
      bzName: '全部手册',
    });

    console.log('sharePath => ', path);
    return {
      title: '全部手册',
      path,
    };
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState({ pageNo: this.state.pageNo + 1 }, () => {
        this.getEbooksList(this.state.categoryId);
      });
    }
  }

  initRenderData = async () => {
    await this.getCategoryList();
    //获取默认分类的id
    const item: any = this.state.categoryList.find((item: any) => item.defaultCategory == 1);
    const e = { currentTarget: { dataset: { id: item.id } } };
    await this.handleChangeCategory(e);
  };

  //切换分类
  handleChangeCategory = async ({ currentTarget }) => {
    const currentIndex = parseInt(currentTarget.dataset.id);
    const data = this.state.categoryList;
    data.forEach((item: any) => {
      item.isChecked = item.id == currentIndex ? true : false;
    });
    this.setState({
      pageNo: 1,
      categoryList: data,
      categoryId: currentIndex,
    });

    await this.getEbooksList(currentIndex);
  };

  getCategoryList = async () => {
    const { data } = await wxApi.request({
      url: api.ebooks.categoryList,
      loading: true,
      data: { pageNo: 1, pageSize: 1000 },
    });

    return new Promise((reslove) => this.setState({ categoryList: data }, reslove));
  };

  //根据分类获取电子画册
  getEbooksList = async (index) => {
    const res = await wxApi.request({
      url: api.ebooks.ebooksList,
      loading: true,
      data: {
        pageNo: this.state.pageNo,
        pageSize: 10,
        categoryId: index,
        albumStatus: 1, //始终展示已上架
      },
    });

    const data = res.data || [];

    data.forEach((item: any) => {
      item.pdfUrl = imageUtils.cdn(item.pdfUrl);
      item.coverUrl = imageUtils.thumbnailOneRowTwo(item.coverUrl);
    });

    let renderList;
    if (this.isLoadMoreRequest()) {
      renderList = this.state.ebooksList.concat(data);
    } else {
      renderList = data;
    }
    const hasMore = renderList.length < res.totalCount;
    this.setState({ ebooksList: renderList, hasMore });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  copyPdfUrl = ({ currentTarget }) => {
    const url = currentTarget.dataset.id;
    wxApi.setClipboardData({
      data: url,
      success: function () {
        wxApi.hideToast();
        wxApi.getClipboardData({
          success: function () {
            wxApi.showModal({
              title: '已复制PDF下载链接，请前往浏览器粘贴地址下载文档。',
              confirmText: '确定',
              showCancel: false,
              success: function (res) {
                if (res.confirm) {
                  //点击确定按钮
                }
              },
            });
          },
        });
      },
    });
  };

  // 下载并打开打开画册
  openFile = ({ currentTarget }) => {
    const { pdfUrl } = currentTarget.dataset.item;
    pdfDownloader.start(
      pdfUrl,
      // success
      (previewMsg: string) => {
        this.setState({ previewMsg });
      },
      // fail
      (filePath: string) => {
        this.setState({ previewMsg: null });
        pdfDownloader.previewPdf(filePath);
      }
    );
  };

  onUnload = () => {
    pdfDownloader.abort();
  };

  render() {
    const { categoryList, previewMsg, ebooksList } = this.state;

    return (
      <View data-scoped='wk-mpe-Ebooks' className='wk-mpe-Ebooks page-container'>
        <View className='label-box'>
          <ScrollView className='scroll-nav' scrollX style={_safe_style_('width: 100%')}>
            {categoryList.map((item: any, index: number) => {
              return (
                <View
                  key={index}
                  onClick={_fixme_with_dataset_(this.handleChangeCategory, { id: item.id })}
                  className='item-nav}}'
                >
                  <Text className={'label-nav ' + (item.isChecked ? 'nav-hover' : '')}>{item.categoryName}</Text>
                  {!!item.isChecked && <View className='underline-nav'></View>}
                </View>
              );
            })}
          </ScrollView>
        </View>
        {!!previewMsg && (
          <View className='preview-progress'>
            <Text>{previewMsg}</Text>
          </View>
        )}

        {ebooksList && ebooksList.length ? (
          <View className='ebooks-container'>
            {ebooksList.map((item: any, index: number) => {
              return (
                <View className='ebooks-item' key={index}>
                  <Image
                    className='ebooks-img'
                    onClick={_fixme_with_dataset_(this.openFile, { item: item })}
                    src={item.coverUrl}
                    mode='aspectFill'
                  ></Image>
                  <View className='ebooks-tit'>{item.title}</View>
                  <View className='unload-btn' onClick={_fixme_with_dataset_(this.copyPdfUrl, { id: item.pdfUrl })}>
                    下载
                  </View>
                </View>
              );
            })}
          </View>
        ) : (
          <View className='empty-wrapper'>
            <Empty message='敬请期待...'></Empty>
          </View>
        )}
      </View>
    );
  }
}

export default Ebooks;
