import wxApi from '@/wxat-common/utils/wxApi';
import Taro from '@tarojs/taro';

/**
 * Created by a123 on 2020/6/27.
 * @author trumpli<李志伟>
 */
const config = {
  taskUrl: null,
  task: null,
  successHandler: null,
  msgHandler: null,
  msg: '',
  timer: null,
};

const fileManager = {
  checkFile(filePath) {
    return new Promise((success, fail) => {
      wxApi.getFileSystemManager().getFileInfo({ filePath, success, fail });
    });
  },

  getStoreTempFiles() {
    let cache = [];
    try {
      cache = wxApi.getStorageSync('pdf-mapping');
    } catch (xe) {
      console.log('fileManager.getStoreTempFile.catch', xe);
    }
    return cache || [];
  },
  getMappingTempFile(key) {
    const cache = this.getStoreTempFiles();
    const index = cache.findIndex((item) => {
      return !!item[key];
    });
    if (index === -1) return null;
    return cache[index];
  },
  setStoreTempFile(key, path) {
    const item = { [key]: path };
    const storeItem = this.getMappingTempFile(key);
    if (!!storeItem) return;
    let cache = this.getStoreTempFiles();
    if (cache.length > 9) cache = [item].concat(cache.slice(0, 8));
    else cache = [item].concat(cache);
    try {
      wxApi.setStorageSync('pdf-mapping', cache);
    } catch (xe) {
      console.log('fileManager.setStorageSync.catch', xe);
    }
  },
};

function abort() {
  !!config.task && config.task.abort();
  config.taskUrl = config.successHandler = config.msgHandler = config.task = config.timer = null;
  config.msg = '';
}

function onSuccess(res) {
  if (res.statusCode === 200) {
    const filePath = res.filePath || res.tempFilePath;
    fileManager.setStoreTempFile(config.taskUrl, filePath);
    config.successHandler(filePath);
    abort();
  }
}

function ifWatching() {
  return !!config.task;
}

function consumer() {
  if (ifWatching() && !config.timer) {
    config.timer = setTimeout(() => {
      if (ifWatching()) {
        if (!!config.msg) {
          config.msgHandler(config.msg);
          config.msg = null;
        }
        config.timer = null;
        consumer();
      }
    }, 300);
  }
}

function watch() {
  const DownloadTask = config.task;

  //监听下载进度变化事件
  DownloadTask.progress((res) => {
    if (!ifWatching()) return;
    const progress = '已加载:' + res.progress + '%';
    if (progress !== config.msg) config.msg = progress;
    consumer();
  });

  //监听 HTTP Response Header 事件。会比请求完成事件更早
  DownloadTask.headersReceived(() => {
    if (ifWatching()) config.msgHandler((config.msg = '开始下载'));
  });
}

export default {
  abort,
  async start(url, msgHandler, successHandler) {
    abort();
    try {
      const storeItem = fileManager.getMappingTempFile(url);
      if (!!storeItem) {
        const res = await fileManager.checkFile(storeItem[url]);
        if (!!res.size) {
          return successHandler(storeItem[url]);
        }
      }
    } catch (xe) {
      console.log('create.checkFile', xe);
    }
    config.taskUrl = url;
    config.msgHandler = msgHandler;
    config.msg = '连接中...';
    config.successHandler = successHandler;
    config.task = wxApi.downloadFile({
      url,
      header: { 'content-type': 'application/pdf' },
      success: onSuccess,
      fail: abort,
    });

    watch();
  },
  previewPdf(filePath) {
    wxApi.openDocument({ showMenu: true, filePath });
  },
};
