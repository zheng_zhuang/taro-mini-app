import { $getRouter, _safe_style_ } from 'wk-taro-platform';
import { View, Text, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi/index';
import template from '@/wxat-common/utils/template.js';
import navegateToOtherWx from '../../utils/navegateToOtherWx.js';
import WxOpenWeapp from '@/wxat-common/components/wx-open-launch-weapp';
import './index.scss';
import store from '@/store';

class PaySuccess extends React.Component {
  $router = $getRouter();

  state = {
    constPrice: '',
    tmpStyle: '',
    from: '',
    orderNo: ''
  };

  goHome() {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  goOther() {
    if (this.state.from == "ticket") {
      let h5Link = 'https://kfqtest.ctlife.tv/pages/bookProduct/index'
      if (process.env.ENVIRONMENT === 'release') { // 线上域名
        h5Link = 'https://kfq.ctlife.tv/pages/bookProduct/index'
      }
      const { ext } = store.getState();
      // window.location.href = `${h5Link}?order_no=${this.state.orderNo}&app_id=${ext.appId}`
      wxApi.$locationTo(`${h5Link}?order_no=${this.state.orderNo}&app_id=${ext.appId}`)
    } else {
      navegateToOtherWx({
        // 正式环境 携旅院线
        appId: 'wx2560348cdcdd57fe',
        // path: 'pages/menber/menber1',
        // 测试环境 携旅住无忧二
        // appId: 'wxed6c99dc0cd41de7',
        path: 'pages/menber/menber1',
      });
    }
  }

  componentDidMount(): void {
    this.setState({
      constPrice: this.$router.params.costPrice,
      from: this.$router.params.from,
      orderNo: this.$router.params.orderNo
    });

    this.getTemplateStyle();
  }

  render() {
    const { constPrice, tmpStyle, from } = this.state;
    const transPx = document.documentElement.clientWidth / (Taro.config.designWidth || 750);
    const btnStyles = `.navBtn {
      width: ${280 * transPx}px;
      height: ${86 * transPx}px;
      opacity: 1;
      border-radius: ${45 * transPx}px;
      background: #14b99a;
      color: #fff;
      line-height: ${86 * transPx}px;
      margin-top: 0;
      border-width: 0;
      outline: 0;
      font-size: ${34 * transPx}px;
    }`;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-smpp-PaySuccess'
        className='wk-smpp-PaySuccess'
      >
        <View className='logo top'>
          <Text className='success'>✔</Text>
          <Text className='pay-success'>支付成功</Text>
          <Text className='pay-money'>{'微信支付：' + filters.moneyFilter(constPrice, true)}</Text>
        </View>
        <View className='btn-box'>
          <Button
            onClick={this.goHome}
            style={_safe_style_('color: ' + tmpStyle.btnColor + ';border-color: ' + tmpStyle.btnColor)}
          >
            返回首页
          </Button>

          {Taro.getEnv() === Taro.ENV_TYPE.WEB ? (
            from === "ticket" ? <Button style={_safe_style_('background: ' + tmpStyle.btnColor)} onClick={() => {this.goOther()}}>
              去使用
            </Button> :
            // username="gh_24e933b6a7b2"
            <WxOpenWeapp
              username='gh_73c309d26824'
              path='pages/menber/menber1'
              styles={btnStyles}
              slotTel={
                <Button className='navBtn' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                  去使用
                </Button>
              }
            ></WxOpenWeapp>
          ) : (
            <Button style={_safe_style_('background: ' + tmpStyle.btnColor)} onClick={() => {this.goOther()}}>
              去使用
            </Button>
          )}
        </View>
      </View>
    );
  }
}

export default PaySuccess;
