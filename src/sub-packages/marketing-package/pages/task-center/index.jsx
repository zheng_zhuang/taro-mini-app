import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Image, Button, Canvas, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '@/wxat-common/api/index';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';
import shareUtil from '@/wxat-common/utils/share';
import drawPoster from '@/wxat-common/utils/draw-poster';
import cdnConfig from '@/wxat-common/constants/cdnResConfig';
import authHelper from '@/wxat-common/utils/auth-helper';
import hoc from '@/hoc';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import { connect } from 'react-redux';

const commonImg = cdnConfig.common;

const mapStateToProps = ({ base: { userInfo, loginInfo } }) => {
  return { userInfo, loginInfo };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class TaskCenter extends React.Component {
  state = {
    title: [
      { id: 0, name: '我的任务' },
      { id: 1, name: '我的奖励' },
    ],

    pitchId: 0,
    pageNo: 1,
    awardPageNo: 1,
    pageSize: 10,
    taskTotal: 0,
    awardTotal: 0,
    taskList: [],
    awardList: [],
    showModal: false,
    showShare: false,
    inviteMemberList: [],
    shareId: '',
    myId: '',
    taskType: '',
    newUser: '',
    taskReferId: '',
    current: {},
    tmpStyle: {},
    flag: false,
    isFirst: false,
    isShowShare: false,
    currentUnfinishedList: [],
    shareQrCode: '',
    coverImageUrl: '',
    posterPath: '',
    wxShareImage: cdnConfig.taskCenter.wxShare,
    wxSharePosterImage: cdnConfig.taskCenter.wxPoster,
    renderLoading: false,
  };

  componentDidMount() {
    authHelper.checkAuth();
  }

  componentDidShow() {
    if (!authHelper.checkAuth(false)) return;

    this.getTaskList();
    this.getTemplateStyle();
    this.getAwardList();
  }

  onReachBottom() {
    const { pitchId, awardPageNo, pageNo } = this.state;
    if (pitchId === 0) {
      this.setState({ pageNo: pageNo + 1 }, this.getTaskList);
    } else {
      this.setState({ awardPageNo: awardPageNo + 1 }, this.getAwardList);
    }
  }

  onShareAppMessage(options) {
    const title = this.state.text || '发现了一个好东西，看看吧';
    const pagePath = this.state.pagePath || 'wxat-common/pages/home/index';

    // 点击右上角分享
    if (options.from === 'menu') {
      return {};
    }

    // 分享品牌代言人
    if (this.state.newUser) {
      const url = `${pagePath}${/\?/gi.test(pagePath) ? '&' : '?'}promotion=1&targetType=1&myId=${
        this.state.myId
      }&taskType=${this.state.taskType}&shareId=${this.state.shareId}&newUser=${this.state.newUser}`;
      const path = shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.TASK_CENTER,
        bzName: `任务${this.state.text ? '-' + this.state.text : ''}`,
      });

      console.log('sharePath => ', path);
      return {
        title,
        imageUrl: this.state.coverImageUrl || cdnConfig.taskCenter.defaultPoster,
        path,
      };
    } else {
      const url = `${pagePath}${/\?/gi.test(pagePath) ? '&' : '?'}promotion=1&targetType=1&myId=${
        this.state.myId
      }&taskType=${this.state.taskType}&shareId=${this.state.shareId}&taskReferId=${this.state.taskReferId}`;
      const path = shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.TASK_CENTER,
        bzName: `任务${this.state.text ? '-' + this.state.text : ''}`,
      });

      console.log('sharePath => ', path);
      return {
        title,
        imageUrl: this.state.coverImageUrl || cdnConfig.taskCenter.defaultPoster,
        path,
      };
    }
  }

  //获取任务中心数据
  getTaskList = async () => {
    const { taskTotal, taskList, pageNo, pageSize } = this.state;

    if (taskTotal && taskList && taskList.length >= taskTotal) {
      return;
    }

    const params = { pageNo, pageSize };

    const { data, totalCount } = await wxApi.request({
      url: api.taskCenter.getTaskList,
      loading: true,
      data: params,
      method: 'GET',
    });

    data.forEach((item) => (item.detailStatus = false));

    this.setState({
      taskTotal: totalCount,
      taskList: data.pageNo == 1 ? data : [...taskList, ...data],
    });
  };

  getShareId = async (id) => {
    const { data } = await wxApi.request({
      url: api.taskCenter.getShareId + `?taskId=${id}`,
      loading: true,
      method: 'POST',
    });

    return data;
  };

  //获取邀请的用户列表
  getInviteMemberList = async (shareId) => {
    const params = { shareId };
    const { data } = await wxApi.request({
      url: api.taskCenter.getInviteMemberList,
      loading: true,
      data: params,
      method: 'GET',
    });

    this.setState({ inviteMemberList: data });
  };

  showDialog = async (ev) => {
    const { item } = ev.currentTarget.dataset;
    const myId = item.id;
    const shareId = item.shareId;
    const { taskType, taskReferId, inviteCount, targetTotal } = item;
    const currentUnfinishedList = Array.from({ length: Math.max(targetTotal - inviteCount || 0, 0) }).map((_, i) => i);
    this.setState({ isShowShare: false, current: item, currentUnfinishedList });
    if (shareId) {
      this.getInviteMemberList(shareId);
    } else {
      const { shareId } = await this.getShareId(myId);
      this.getInviteMemberList(shareId);
    }
    const newUser = taskType == 1 ? 1 : 0;
    this.setState({
      showModal: true,
      current: item,
      myId,
      taskType,
      taskReferId,
      newUser,
    });
  };

  handSwitch = (ev) => {
    const { id } = ev.currentTarget.dataset;
    this.setState({ pitchId: id });
  };

  checkDetail = (ev) => {
    const { index } = ev.currentTarget.dataset;
    const taskList = this.state.taskList.slice();
    taskList[index].detailStatus = !taskList[index].detailStatus;

    this.setState({ taskList });
  };

  toShareDialog = (ev) => {
    const { item } = ev.currentTarget.dataset;
    this.setState({ current: item });
  };

  getAwardList = async () => {
    const { awardPageNo, pageSize, awardTotal = 0, awardList = [] } = this.state;

    if (awardTotal && awardList && awardList.length >= awardTotal) return;

    const params = {
      pageNo: awardPageNo,
      pageSize: pageSize,
    };

    const { data, totalCount } = await wxApi.request({
      url: api.taskCenter.getAwardList,
      loading: true,
      data: params,
      method: 'GET',
    });

    this.setState({
      awardTotal: totalCount,
      awardList: params.pageNo == 1 ? data : [...this.state.awardList, ...data],
    });
  };

  toCheck = async (ev) => {
    const { type } = ev.target.dataset;
    if (type == 1) {
      wxApi.$navigateTo({ url: 'wxat-common/pages/coupon-list/index' });
    } else {
      wxApi.$navigateTo({ url: 'sub-packages/mine-package/pages/integral/index' });
    }
  };

  awardRuleDialog = () => {
    this.setState({ flag: !this.state.flag });
  };

  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff',
        backgroundColor: templateStyle.titleColor,
      });
    }

    this.setState({ tmpStyle: templateStyle });
  };

  shareFail = () => {
    wxApi.showToast({ title: '分享失败，请稍后重试', icon: 'none', duration: 2000 });
  };

  toShare = async (ev) => {
    //任务中心的订阅消息
    const Ids = [subscribeEnum.TASK_CENTER.value]; //subscribeEnum.GIFT_CARD.value,
    subscribeMsg.sendMessage(Ids);

    const { item } = ev.currentTarget.dataset;
    const myId = item.id;
    const { taskType, taskReferId, inviteCount, targetTotal } = item;
    const currentUnfinishedList = new Array(Math.max(targetTotal - inviteCount || 0, 0)).join(',');
    this.setState({
      current: item,
      isShowShare: true,
      currentUnfinishedList,
    });

    const data = await this.getShareId(myId);

    const shareId = data.shareId;
    const shareQrCode = data.shareQrCode;
    const pagePath = data.pagePath;
    const coverImageUrl = data.coverImageUrl;
    const text = data.text;

    this.setState({
      showShare: true,
      shareId,
      myId,
      taskType,
      taskReferId,
      isShowShare: true,
      shareQrCode,
      pagePath,
      coverImageUrl,
      text,
      newUser: taskType == 1 ? 1 : 0,
    });
  };

  drawPoster = async () => {
    const ctx = wxApi.createCanvasContext('myCanvas', this);
    wxApi.showLoading({ title: '正在生成海报' });
    this.setState({ renderLoading: true });
    try {
      await drawPoster(
        ctx,
        this.state.shareQrCode,
        this.state.coverImageUrl || cdnConfig.taskCenter.defaultPoster,
        this.state.text || '我发现了一个好东西，快来看看吧！'
      );

      ctx.draw(true);
      setTimeout(() => {
        wxApi.canvasToTempFilePath({
          canvasId: 'myCanvas',
          x: 0,
          y: 0,
          width: 550,
          height: 806,
          success: (res) => {
            wxApi.hideLoading();
            this.setState(
              {
                posterPath: res.tempFilePath,
                renderLoading: false,
              },

              () => {
                wxApi.previewImage({ urls: [this.state.posterPath] });
              }
            );
          },
          fail: (e) => {
            wxApi.hideLoading();
            this.setState(
              {
                renderLoading: false,
              },

              () => {
                wxApi.showToast({ title: '海报生成失败', icon: 'none' });
              }
            );

            console.error(e);
          },
        });
      }, 2000);
    } catch (e) {
      wxApi.hideLoading();
      this.setState(
        {
          renderLoading: false,
        },

        () => {
          wxApi.showToast({ title: '海报生成失败', icon: 'none' });
        }
      );

      console.error(e);
    }
  };

  hiddenShare = () => {
    if (this.state.renderLoading) return;
    this.setState({ showShare: false });
  };

  ok = () => {
    this.setState({ showModal: false, flag: false });
  };

  render() {
    const {
      pitchId,
      tmpStyle,
      title,
      taskList,
      awardList,
      wxShareImage,
      isShowShare,
      shareId,
      wxSharePosterImage,
      shareQrCode,
      showShare,
      showModal,
      inviteMemberList,
      currentUnfinishedList,
      flag,
      current,
    } = this.state;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-mpt-TaskCenter' className='wk-mpt-TaskCenter'>
        <View className='task-top'>
          {title.map((item, index) => {
            return (
              <View
                className={pitchId == item.id ? 'title' : 'default'}
                style={_safe_style_('color:' + tmpStyle.btnColor)}
                key={index}
                onClick={_fixme_with_dataset_(this.handSwitch, { id: item.id })}
              >
                {item.name}
              </View>
            );
          })}
        </View>
        <View className='task-top'>
          {title.map((item, index) => {
            return (
              <Text
                className={pitchId == item.id ? 'solid' : 'transparency'}
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                key={index}
                onClick={_fixme_with_dataset_(this.handSwitch, { id: item.id })}
              ></Text>
            );
          })}
        </View>
        {pitchId == 0 && (
          <Block>
            {taskList && taskList.length ? (
              <View className='wrap'>
                <View className='task-content'>
                  {!!(!!taskList && !!taskList.length) && (
                    <Image src="https://htrip-static.ctlife.tv/wk/task-center.png" className='big-image'>
                      <View className='center-hint'>
                        <Text className='award-rule-chinese'>完成邀约任务得奖励</Text>
                        <Text className='award-rule-english'>Reward for completing the invitation task</Text>
                        <Button className='award-rule-btn' onClick={this.awardRuleDialog}>
                          奖励规则
                        </Button>
                      </View>
                    </Image>
                  )}

                  {taskList.map((item, index) => {
                    return (
                      <View key={index} className='task-last-wrap'>
                        <View
                          className='outer-wrap'
                          style={_safe_style_(item.detailStatus ? '' : 'margin-bottom:20rpx')}
                        >
                          {item.awardType == 1 && (
                            <Block>
                              <Image
                                src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/preferential-card.svg"
                                className='icon'
                              ></Image>
                            </Block>
                          )}

                          {item.awardType == 2 && (
                            <Block>
                              <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/integral.svg" className='icon'></Image>
                            </Block>
                          )}

                          {item.awardType == 3 && (
                            <Block>
                              <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/cash.svg" className='icon'></Image>
                            </Block>
                          )}

                          <View className='inner-wrap'>
                            <View
                              className='left-content'
                              onClick={_fixme_with_dataset_(this.toShareDialog, { item: item })}
                            >
                              <Text className='task-name'>{item.taskName}</Text>
                              <Text className='task-active' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                                {item.awardInfo}
                              </Text>
                              <Block>
                                <View
                                  className='task-number'
                                  onClick={_fixme_with_dataset_(this.showDialog, { item: item })}
                                >
                                  {'邀请' + item.targetTotal + '名用户 （'}
                                  <Text className='number' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                                    {item.inviteCount || 0}
                                  </Text>
                                  {'/' + item.targetTotal + '）'}
                                  <Image
                                    className='task-number__image'
                                    src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/arrowRight.svg"
                                  ></Image>
                                </View>
                              </Block>
                            </View>
                            <View className='right-content'>
                              <Block>
                                {item.isComplete ? (
                                  <Button className='to-over'>已完成</Button>
                                ) : item.status == 2 ? (
                                  <Button
                                    className='to-share'
                                    style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                                    onClick={_fixme_with_dataset_(this.toShare, { item: item })}
                                  >
                                    去分享
                                  </Button>
                                ) : (
                                  <Button className='to-over'>已结束</Button>
                                )}
                              </Block>
                              <Text
                                className='check-detail'
                                onClick={_fixme_with_dataset_(this.checkDetail, { index: index })}
                              >
                                {item.detailStatus ? '收起详情' : '查看详情'}
                              </Text>
                            </View>
                          </View>
                        </View>
                        {!!item.detailStatus && (
                          <Block>
                            <View className='activity-detail'>
                              {item.taskType === 1 && (
                                <Block>
                                  <Text>任务描述：客户通过分享者的分享成功授权手机号码 \n</Text>
                                </Block>
                              )}

                              {item.taskType === 2 && (
                                <Block>
                                  <Text>任务描述：新客户通过分享者的分享观看直播即算邀约成功 \n</Text>
                                </Block>
                              )}

                              {item.taskType === 3 && (
                                <Block>
                                  <Text>任务描述：新客户通过分享者的分享发起或参与拼团即算邀约成功 \n</Text>
                                </Block>
                              )}

                              {item.taskType === 4 && (
                                <Block>
                                  <Text>任务描述：新客户通过分享者的分享参与秒杀即算邀约成功 \n</Text>
                                </Block>
                              )}

                              {item.taskType === 5 && (
                                <Block>
                                  <Text>任务描述：新客户通过分享者的分享参与砍价即算邀约成功 \n</Text>
                                </Block>
                              )}

                              <Text>{'任务时间：' + item.startTime + '-' + item.endTime}</Text>
                            </View>
                          </Block>
                        )}
                      </View>
                    );
                  })}
                </View>
              </View>
            ) : (
              <Empty message='敬请期待...'></Empty>
            )}
          </Block>
        )}

        {pitchId == 1 && (
          <Block>
            {awardList && awardList.length ? (
              <View className='wrap'>
                <View style={_safe_style_('height: 5rpx')}></View>
                {awardList.map((item, index) => {
                  return (
                    <View className='award-wrap' key={index}>
                      <View className='award-outer-wrap'>
                        <Text className='time'>{item.createTime}</Text>
                        <Block>
                          {item.awardType === 3 ? (
                            <Text className='hint'>请到微信零钱查看</Text>
                          ) : (
                            <Text
                              className='check'
                              onClick={_fixme_with_dataset_(this.toCheck, { type: item.awardType })}
                              style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                            >
                              去查看
                            </Text>
                          )}
                        </Block>
                      </View>
                      <View className='award-inner-wrap'>
                        {item.awardType == 1 && (
                          <Block>
                            <Image
                              src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/preferential-card.svg"
                              className='icon'
                            ></Image>
                          </Block>
                        )}

                        {item.awardType == 2 && (
                          <Block>
                            <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/integral.svg" className='icon'></Image>
                          </Block>
                        )}

                        {item.awardType == 3 && (
                          <Block>
                            <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/task-center/task-center/cash.svg" className='icon'></Image>
                          </Block>
                        )}

                        <View className='award-content'>
                          <Text className='award-task-name'>{item.taskName}</Text>
                          <View>
                            <Text className='award-task-award' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                              {item.awardInfo}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                })}
              </View>
            ) : (
              <Empty></Empty>
            )}
          </Block>
        )}

        <View className='poster-modal'></View>
        {!!showShare && (
          <View className='modal'>
            <View className='modal-mask' onTap={this.hiddenShare} onTouchMove={this.preventTouchMove}></View>
            <View className='modal-content'>
              {!!(isShowShare && !shareId) && (
                <Button className='btn-image btn-share' onClick={this.shareFail}>
                  <Image className='icon' src={wxShareImage}></Image>
                  <View className='text'>活动页分享</View>
                </Button>
              )}

              {!!(shareId && isShowShare) && (
                <ButtonWithOpenType className='btn-image btn-share' openType='share'>
                  <Image className='icon' src={wxShareImage}></Image>
                  <View className='text'>活动页分享</View>
                </ButtonWithOpenType>
              )}

              {!!shareQrCode && (
                <Button onTap={this.drawPoster} className='poster-share btn-image'>
                  <Image className='icon' src={wxSharePosterImage}></Image>
                  <View className='text'>海报分享</View>
                </Button>
              )}
            </View>
          </View>
        )}

        <Canvas canvasId='myCanvas' id='myCanvas'></Canvas>
        {!!showModal && <View className='mask' onTouchMove={this.preventTouchMove} onClick={this.ok}></View>}
        {!!showModal && (
          <View className='modalDlg'>
            <Image src="https://htrip-static.ctlife.tv/wk/task-center.png" className='big-picture'>
              <View className='ball'></View>
              <View className='ball-two'></View>
              <View className='ball-three'></View>
              <View className='task-hint'>
                {current.taskType === 1 && (
                  <Block>
                    <Text className='task-name'>邀请新用户推广任务</Text>
                  </Block>
                )}

                {current.taskType === 2 && (
                  <Block>
                    <Text className='task-name'>观看直播推广任务</Text>
                  </Block>
                )}

                {current.taskType === 3 && (
                  <Block>
                    <Text className='task-name'>参与拼团推广任务</Text>
                  </Block>
                )}

                {current.taskType === 4 && (
                  <Block>
                    <Text className='task-name'>参与秒杀推广任务</Text>
                  </Block>
                )}

                {current.taskType === 5 && (
                  <Block>
                    <Text className='task-name'>参与砍价推广任务</Text>
                  </Block>
                )}

                {current.taskType === 1 && (
                  <Block>
                    <View className='task-content-midden'>
                      {'邀请' + current.targetTotal + '名新用户（'}
                      <Text className='number' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {current.inviteCount || 0}
                      </Text>
                      {'/' + current.targetTotal + '）'}
                    </View>
                  </Block>
                )}

                {current.taskType === 2 && (
                  <Block>
                    <View className='task-content-midden'>
                      {'邀请' + current.targetTotal + '名新用户观看直播 （'}
                      <Text className='number' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {current.inviteCount || 0}
                      </Text>
                      {'/' + current.targetTotal + '）'}
                    </View>
                  </Block>
                )}

                {current.taskType === 3 && (
                  <Block>
                    <View className='task-content-midden'>
                      {'邀请' + current.targetTotal + '名新用户参与拼团 （'}
                      <Text className='number' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {current.inviteCount || 0}
                      </Text>
                      {'/' + current.targetTotal + '）'}
                    </View>
                  </Block>
                )}

                {current.taskType === 4 && (
                  <Block>
                    <View className='task-content-midden'>
                      {'邀请' + current.targetTotal + '名新用户参与秒杀 （'}
                      <Text className='number' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {current.inviteCount || 0}
                      </Text>
                      {'/' + current.targetTotal + '）'}
                    </View>
                  </Block>
                )}

                {current.taskType === 5 && (
                  <Block>
                    <View className='task-content-midden'>
                      {'邀请' + current.targetTotal + '名新用户参与砍价 （'}
                      <Text className='number' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {current.inviteCount || 0}
                      </Text>
                      {'/' + current.targetTotal + '）'}
                    </View>
                  </Block>
                )}
              </View>
            </Image>
            <Block>
              <ScrollView scrollY='true' style={_safe_style_('height: 400rpx;')}>
                {inviteMemberList.map((item) => {
                  return (
                    <View className='dialog-task' key={item.id}>
                      <Image
                        src={item.inviteUserAvatar ? item.inviteUserAvatar : commonImg.headDefault}
                        className='dialog-task-icon'
                      ></Image>
                      <View className='dialog-task-wrap'>
                        <Text className='dialog-task-name'>{item.inviteUserName}</Text>
                        <Text
                          className='dialog-task-time'
                          style={_safe_style_(
                            'font-size:24rpx;font-family:PingFangSC-Regular,PingFang SC;font-weight:400;color:rgba(153,153,153,1);'
                          )}
                        >
                          {item.createTime}
                        </Text>
                      </View>
                    </View>
                  );
                })}
                {currentUnfinishedList.map((i) => {
                  return (
                    <View className='dialog-task' key={i}>
                      <Image
                        src="https://htrip-static.ctlife.tv/wk/default-img.png"
                        className='dialog-task-unIcon'
                        style={_safe_style_('width: 105rpx;height: 105rpx')}
                      ></Image>
                      <View className='dialog-task-wrap'>
                        <Text className='dialog-task-unName'></Text>
                        <Text className='dialog-task-unTime'></Text>
                      </View>
                    </View>
                  );
                })}
              </ScrollView>
            </Block>
            <Block>
              {!current.isComplete ? (
                <View>
                  <View className='task-content-hint' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    再接再厉，还差
                    <Text>{current.targetTotal - (current.inviteCount || 0)}</Text>
                    {'名即可获得' + current.awardInfo}
                  </View>
                </View>
              ) : (
                <View className='task-content-hint' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  恭喜完成了任务
                </View>
              )}
            </Block>
            <Block></Block>
            <Block>
              {!!(shareId && isShowShare) && (
                <ButtonWithOpenType
                  className='ok'
                  openType='share'
                  style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                >
                  去分享
                </ButtonWithOpenType>
              )}
            </Block>
            <Block>
              {!!(isShowShare && !shareId) && (
                <Button
                  className='ok'
                  onClick={this.shareFail}
                  style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                >
                  去分享
                </Button>
              )}
            </Block>
          </View>
        )}

        {!!flag && <View className='mask'></View>}
        {!!flag && (
          <View className='modalDlg-award'>
            <View className='award-rule-head'>奖励规则</View>
            <View className='award-rule-content'>
              <Text>1.分享任务页面，并成功邀请客户完成任务目标。\n</Text>
              <Text>2.邀请人数达到任务目标后，立即发放奖励，其中：\n</Text>
              <Text>现金奖励将直接发放到微信零钱; \n</Text>
              <Text>优惠券奖励可前往优惠券页面查看; \n</Text>
              <Text>积分奖励可前往积分中心查看。</Text>
            </View>
            <Image src={commonImg.close} className='close' onClick={this.ok}></Image>
          </View>
        )}
            <HomeActivityDialog showPage="sub-packages/marketing-package/pages/task-center/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default TaskCenter;
