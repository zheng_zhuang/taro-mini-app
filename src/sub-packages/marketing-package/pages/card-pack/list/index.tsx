import React, {FC, useEffect} from 'react'; // @externalClassesConvered(Empty)
import {_safe_style_} from '@/wxat-common/utils/platform';
import {Block, Button, Image, ScrollView, Text, View} from '@tarojs/components';
import Taro, {useRouter} from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
import format from '@/wxat-common/utils/filter-card-pack.wxs';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Empty from '@/wxat-common/components/empty/empty';
import screen from '@/wxat-common/utils/screen';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import useList from '@/hooks/useList';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import './index.scss';

const CARDSTATUS = [0, 1, 2]; // 0：未开始 1：进行中 2：已售罄 3：已结束 4：已删除

type CardPackListProps = {
  isFullScreen: boolean;
  templ: any;
};

let CardPackList: FC<CardPackListProps> = (props) => {
  const router = useRouter();
  const templ = useTemplateStyle({ autoSetNavigationBar: true });

  const isFullScreen = screen.isFullScreenPhone;

  const jump = ({ itemNo }) => {
    wxApi.$navigateTo({
      url: `/sub-packages/marketing-package/pages/card-pack/detail/index?itemNo=${itemNo}`,
    });
  };
  const { list, hasMore, status, retry } = useList({
    query: async (page, pageSize) => {
      const params = { pageNo: page, pageSize, channel: 1 };
      CARDSTATUS.forEach((item, i) => {
        const key = `statusList[${i}]`;
        params[key] = item;
      });
      const { data } = await wxApi.request({
        url: api.couponpocket.couponpocketList,
        data: params,
        loading: true,
      });

      return data || [];
    },
  });

  useEffect(() => {
    const { title = '代金卡包' } = router.params;
    wxApi.setNavigationBarTitle({ title });
  }, []);

  return (
    <ScrollView
      scrollY
      data-fixme='02 block to view. need more test' data-scoped='wk-pcpl-List' className='wk-pcpl-List'>
      {list.length > 0 && (
        <View className={'graph-list ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {list.map((item: any) => {
            return (
              <Block key={item.id}>
                <View className='classic-list' onClick={jump.bind(null, { itemNo: item.itemNo })}>
                  <View className='cards-img-box'>
                    <Image className='cards-img-box__image' mode='aspectFill' src={item.itemImageUrls[0]}></Image>
                    {item.pocketStatus !== 1 && (
                      <View className='sold-out-cover'>{format.formatStatus(item.pocketStatus)}</View>
                    )}
                  </View>
                  <View className='card-detail'>
                    <View className='more-ellipsis title-wraper'>
                      {item.provNum !== 0 && (
                        <View className='limit-order' style={_safe_style_('background:' + templ.bgGradualChange)}>
                          {'限购' + item.buyLimit + '组'}
                        </View>
                      )}

                      <Text className='title'>{item.name}</Text>
                    </View>
                    <View>
                      <View>
                        <View className='price' style={_safe_style_('color:' + templ.btnColor)}>
                          <Text className='price-symbol'>￥</Text>
                          {filters.moneyFilter(item.salePrice, true)}
                        </View>
                        {/*  <View class='original-price'>￥{{item.originalPrice}}</View>  */}
                      </View>
                      <View className='sold'>{(item.cardSalesVolume || 0) + '人已购买'}</View>
                    </View>
                    <Button
                      className='btn-red'
                      style={_safe_style_(
                        'background:' + (item.pocketStatus !== 1 ? 'rgba(204, 204, 204, 1)' : templ.btnColor)
                      )}
                    >
                      {format.formatStatus(item.pocketStatus)}
                    </Button>
                  </View>
                </View>
              </Block>
            );
          })}
        </View>
      )}

      {!!(list.length === 0 && !hasMore) && <Empty message='敬请期待...'></Empty>}
      <LoadMore status={status} onRetry={retry}></LoadMore>
      <HomeActivityDialog showPage="sub-packages/marketing-package/pages/card-pack/list/index"></HomeActivityDialog>
    </ScrollView>
  );
};

export default CardPackList;
