import {$getRouter} from 'wk-taro-platform';
import React from 'react';
import {_fixme_with_dataset_, _safe_style_} from '@/wxat-common/utils/platform';
import {Block, Canvas, Image, ScrollView, Swiper, SwiperItem, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import '@/wxat-common/api';
import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import utilDate from '@/wxat-common/utils/date.js';

import ShareDialog from '@/wxat-common/components/share-dialog/index';
import BuyNow from '@/wxat-common/components/buy-now';
import DetailParser from '@/wxat-common/components/detail-parser/index';
import screen from '@/wxat-common/utils/screen';
import ChooseAttributeDialog from '@/wxat-common/components/choose-attribute-dialog';
import Error from '@/wxat-common/components/error/error';
import PromoterError from '@/wxat-common/components/promoter-error/index';
import './index.scss';
import {connect} from 'react-redux';
import report from '@/sdks/buried/report/index.js';
import {ITouchEvent} from '@tarojs/components/types/common';
import utils from '@/wxat-common/utils/util';
import shareUtil from '@/wxat-common/utils/share';
import checkOptions from '@/wxat-common/utils/check-options';
import PaDetailTmpl from '../../../components/tpl/pa-detail';
import PaDetailWxworkTmpl from '../../../components/tpl/pa-detail-wxword';
import PostersDialog from '@/sub-packages/marketing-package/components/posters-dialog';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import {bindPromoterEnum} from '@/wxat-common/constants/promoterEnum.js';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import getStaticImgUrl from '../../../../../wxat-common/constants/frontEndImgUrl'

interface StateProps {
  templ: any;
  industry: string;
  sessionId: string;
  currentStore: any;
  environment: any;
  appInfo: any;
}

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
  };
};

interface CardPackDetail {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class CardPackDetail extends React.Component {
  $router = $getRouter();
  state = {
    goodsImages: [], // 商品图片
    goodsDetail: null, // 活动商品详情
    dataLoad: false,
    error: false,
    isExpand: false,
    posterBgImg: cdnResConfig.goods.posterBgOne,
    isRefuserPromoter: false,
  };

  itemNo: string;
  refUseId: number;
  storeId: number;
  activityId: number;
  promoterCommission: string;
  extraData: Record<string, any>;
  isPaDetail = false;
  refShareDialogCMPT = React.createRef<any>();
  refChooseAttributeDialogCMPT: any;
  cardPackEnum = { region: 1, promotion: 2 };
  bindPromoterEnum = bindPromoterEnum;
  pageUrl = 'sub-packages/marketing-package/pages/card-pack/detail/index';

  async componentDidMount() {
    const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);

    if (formattedOptions) {
      this.init(formattedOptions);
    }
  }

  async componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);
      if (formattedOptions) {
        this.init(formattedOptions);
      }
    }
  }

  /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */
  onShareAppMessage() {
    const { environment } = this.props;
    const itemNo = this.itemNo;
    const activityId = this.activityId;
    const extraData = this.extraData;
    const pageUrl = this.pageUrl;

    if (environment === 'wxwork' && this.isPaDetail) {
      // 企微环境中推广给大使
      const url = `/${pageUrl}?isPaDetail=1&itemNo=${itemNo}&activityId=${activityId}&promoterCommission=${extraData.promoterCommission}&activityStatus=${extraData.activityStatus}`;
      const path = shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.CARD_PACK_DETAIL,
        bzName: `卡包${extraData.name ? '-' + extraData.name : ''}`,
      });

      console.log('sharePath => ', path);
      return {
        title: extraData.name,
        path,
      };
    } else {
      report.share(true);
      this.refShareDialogCMPT.current.hide();
      const { goodsDetail } = this.state as any;
      const title = goodsDetail.name || goodsDetail.wxItem.name || '';
      if (goodsDetail) {
        let url = `/${pageUrl}?itemNo=${itemNo}`;

        if (activityId) {
          url += `&activityId=${activityId}`;
        }

        const path = shareUtil.buildShareUrlPublicArguments({
          url,
          bz: shareUtil.ShareBZs.CARD_PACK_DETAIL,
          bzName: `卡包${title ? '-' + title : ''}`,
        });

        console.log('sharePath => ', path);
        return {
          title,
          path,
        };
      }
    }
    return {};
  }

  // 保存图片
  onSavePosterImage = () => {
    this.refShareDialogCMPT.current.savePosterImage(this);
  };

  async init(options) {
    console.log(options, 'options');
    let params = options;
    const { environment, currentStore } = this.props;

    // 推广大使短链分享
    if (options.scene) {
      params = decodeURIComponent(options.scene)
        .split('&')
        .map((kv) => {
          const [key, val] = kv.split('=');
          return { [key]: val };
        })
        .reduce((pre, cur) => ({ ...pre, ...cur }), {});
    }

    // 普通跳转、转发、推广大使
    if (!!params.itemNo || !!params.activityId) {
      this.refUseId = params.refUseId || params._ref_useId || null;
      this.storeId = params.refStoreId || params._ref_storeId || null;
      this.activityId = params.promoterActivityId || params.activityId;
      this.itemNo = params.itemNo || params.id || null;
    } else if (options.scene) {
      // 卡包(卡包专区)分享
      const originScene = decodeURIComponent(options.scene);
      const scene = '?' + decodeURIComponent(options.scene);
      let itemParam = '';
      if (originScene.includes('No')) {
        // 老接口生成的链接包含No字段
        itemParam = utils.getQueryString(scene, 'No');
      } else {
        // 新接口生成的字段不包含No字段，并且originScene直接为参数
        itemParam = originScene;
      }
      this.itemNo = itemParam;
    }

    // 推广大使活动
    if (params.isPaDetail) {
      this.isPaDetail = true;
      this.promoterCommission = params.promoterCommission;
      if (this.refUseId) {
        const data = await this.getRefuserPromoter(this.refUseId);
        const isRefuserPromoter = data !== this.bindPromoterEnum.pass;
        this.setState({
          isRefuserPromoter,
        });

        if (isRefuserPromoter) {
          return;
        }
      }
    }

    // 推广大使企微底部状态
    if (environment === 'wxwork') {
      this.extraData = params;
    }

    // 推广大使卡包强制切换门店
    const storeId = this.storeId;
    if (params.activityId && storeId && currentStore.id != storeId) {
      await checkOptions.changeStore(storeId);
    }

    // 推广大使微信端禁止分享
    if (environment !== 'wxwork' && this.isPaDetail) {
      wxApi.hideShareMenu();
    }

    this.initRenderData(); // 初始化渲染的数据
  }

  /**
   * 获取邀请好友对话弹框
   */
  handleSelectChanel = () => {
    this.refShareDialogCMPT.current.show();
  };

  operationText = () => {
    const goodsDetail = this.state.goodsDetail as any;
    if (goodsDetail && goodsDetail.wxItem) {
      let text = '';
      console.log(goodsDetail.wxItem.pocketStatus, 'goodsDetail.wxItem.pocketStatus');
      // 0 '未开始', 1 '进行中', 2 '已售罄', 3 '已结束', 4 '已删除'
      switch (goodsDetail.wxItem.pocketStatus) {
        case 0:
          text = '活动未开始';
          break;
        case 1:
          text = '购买';
          break;
        case 2:
          text = '已售罄';
          break;
        case 3:
          text = '活动已结束';
          break;
        case 4:
          text = '活动已结束'; // 已删除文案
          break;
        default:
          text = '已售罄';
      }

      return text;
    }
    return '已售罄';
  };

  initRenderData = () => {
    const { dataLoad } = this.state;
    if (dataLoad) return;

    this.setState({ dataLoad: true });
    this.getGoodsDetail();
  };

  // 选择商品sku属性
  handlerChooseSku = (info) => {
    const goodsInfo = info;
    const goodsDetail = this.state.goodsDetail as any;
    // 埋点参数对象
    const reportParams = {
      sku_num: goodsInfo.skuId || null,
      itemNo: goodsInfo.itemNo || null,
      barcode: goodsInfo.barcode || null,
      product_count: goodsInfo.itemCount || null,
    };

    report.clickBuyNowProductpage(reportParams);
    goodsInfo.reportExt = JSON.stringify({
      sessionId: report.getBrowseItemSessionId(this.itemNo),
    });

    goodsInfo.expressType = '0';
    goodsInfo.itemType = goodsTypeEnum.CARDPACK.value;
    goodsInfo.itemStock = goodsInfo.wxItem.itemStock;
    const url = pageLinkEnum.orderPkg.payOrder;
    protectedMailBox.send(url, 'goodsInfoList', [goodsInfo]);

    const limitItemCount = goodsDetail.wxItem.buyLimit;
    // 判断已购买数量是否大于限购数量
    if (
      limitItemCount &&
      (goodsInfo.wxItem.purchasedNum >= limitItemCount ||
        goodsInfo.itemCount + goodsInfo.wxItem.purchasedNum > limitItemCount)
    ) {
      wxApi.showToast({
        title: `每人限制购买${limitItemCount}组`,
        icon: 'none',
      });

      return;
    }
    const payDetails = [];
    wxApi.$navigateTo({
      url: url,
      data: {
        payDetails: JSON.stringify(payDetails),
      },
    });

    this.hideAttrDialog();
  };

  // 判断是否是导购的推广大使
  getRefuserPromoter = async (employeeId) => {
    const params = {
      employeeId,
    };

    const { data } = await wxApi.request({
      url: api.user.queryBindAmbassador,
      data: params,
    });

    return data;
  };

  getGoodsDetail = async () => {
    const itemNo = this.itemNo;
    const params = { itemNo };

    try {
      const { data: cardpackDetail } = await wxApi.request({ url: api.couponpocket.detail, data: params });
      if (cardpackDetail) {
        const couponInfos = this.formatCouponList(cardpackDetail.couponInfos);
        const wxItem = {
          itemNo: cardpackDetail.itemNo,
          buyLimit: cardpackDetail.buyLimit,
          cardExplain: cardpackDetail.cardExplain,
          salePrice: cardpackDetail.salePrice,
          name: cardpackDetail.name,
          cardSalesVolume: cardpackDetail.cardSalesVolume,
          isShelf: cardpackDetail.isShelf,
          provNum: cardpackDetail.provNum,
          itemStock:
            cardpackDetail.provNum === 0 ? 9999999999 : cardpackDetail.provNum - cardpackDetail.cardSalesVolume, // 真实库存：provNum(总量) - cardSalesVolume(已售出) provNum为0时不限量
          purchasedNum: cardpackDetail.purchasedNum,
          thumbnail: cardpackDetail.itemImageUrls[0] || null,
          pocketStatus: cardpackDetail.pocketStatus, // 0 未开始 1 进行中 2 已售罄 3 已结束 4 已删除
          couponInfos,
        };

        const goodsDetail = {
          couponInfos, // 优惠券信息
          wxItem,
          itemNo: cardpackDetail.itemNo,
          itemStock:
            cardpackDetail.provNum === 0 ? 9999999999 : cardpackDetail.provNum - cardpackDetail.cardSalesVolume, // 真实库存：provNum(总量) - cardSalesVolume(已售出) provNum为0时不限量
          itemType: cardpackDetail.type,
          isCardPack: 1,
          activityId: this.activityId,
          refUseId: this.refUseId,
          channel: cardpackDetail.channel,
        };

        this.setState({
          goodsImages: cardpackDetail.itemImageUrls || [],
          goodsDetail,
        });

        if (!this.isPaDetail && cardpackDetail.channel === this.cardPackEnum.promotion) {
          wxApi.hideShareMenu();
        }
      } else {
        this.apiError();
      }
    } catch (e) {
      this.apiError();
    }
  };

  expandCoupon = () => {
    const { isExpand } = this.state;
    this.setState({ isExpand: !isExpand });
  };

  /**
   * 底部购买按钮
   */
  handleBuyNow = () => {
    const itemNo = this.itemNo;
    this.showAttrDialog();
    report.clickBuy(itemNo);
  };

  showAttrDialog = () => {
    if (this.refChooseAttributeDialogCMPT) {
      this.refChooseAttributeDialogCMPT.showAttributDialog();
    }
  };

  hideAttrDialog = () => {
    if (this.refChooseAttributeDialogCMPT) {
      this.refChooseAttributeDialogCMPT.hideAttributeDialog();
    }
  };

  /**
   * 轮播预览大图
   */
  previewImg = (ev: ITouchEvent) => {
    const currentUrl = ev.currentTarget.dataset.currenturl;
    const previewUrls = this.state.goodsImages;
    wxApi.previewImage({
      current: currentUrl,
      urls: previewUrls,
    });
  };

  formatCouponList = (list) => {
    if (!list || !list.length) return [];
    list.forEach((item) => {
      const endTime = new Date(item.endTime);
      const beginTime = new Date(item.beginTime);
      item.endTime = utilDate.format(endTime, 'yyyy-MM-dd');
      item.beginTime = utilDate.format(beginTime, 'yyyy-MM-dd');
      if (item.itemBriefs) {
        const { briefs } = this.getCount(item.itemBriefs);
        item.briefsStr = briefs;
      } else if (item.categoryBriefs) {
        const { briefs } = this.getCount(item.categoryBriefs);
        item.briefsStr = briefs;
      }
      if (item.storeBriefs) {
        const { briefs } = this.getCount(item.storeBriefs);
        item.storeBriefsStr = briefs;
      }
    });
    return list;
  };

  getCount = (data) => {
    let count = 0;
    let briefs = '';
    data.forEach((brief, index) => {
      if (brief.name) {
        count += brief.name.length;
        briefs += brief.name;
        if (index < data.length - 1) {
          briefs += '、';
        }
      }
    });
    return { count, briefs };
  };

  /**
   * 展示错误页面
   * @param {*} error
   */
  apiError = () => {
    this.setState({ error: true });
  };

  handleShare() {
    this.refShareDialogCMPT.current.show(this);
  }

  render() {
    const { error, goodsImages, goodsDetail = {}, isExpand, dataLoad, posterBgImg, isRefuserPromoter } = this
      .state as Record<string, any>;
    const { templ } = this.props;

    const qrCodeParams = {
      verificationNo: this.itemNo,
      verificationType: 5,
      maPath: this.pageUrl,
    };

    const isPaDetail = this.isPaDetail;
    const promoterCommission = this.promoterCommission;
    const extraData = this.extraData || {};
    const cardPackEnum = this.cardPackEnum;
    const { currentStore, environment } = this.props;
    const isFullScreen = screen.isFullScreenPhone;

    // 推广大使海报
    const uniqueShareInfoForQrCode = {
      page: this.pageUrl,
      refStoreId: currentStore.id,
      itemNo: this.itemNo,
      activityId: this.activityId,
      promoterActivityId: this.activityId,
      refBz: shareUtil.ShareBZs.CARD_PACK_DETAIL,
      refBzName: `卡包${extraData.name ? '-' + extraData.name : ''}`,
    };

    const operationText = this.operationText();

    if (!goodsDetail || !goodsDetail.wxItem) {
      return null;
    }

    return (
      <Block>
      <ScrollView
        scrollY
        data-fixme='02 block to view. need more test' data-scoped='wk-pcpd-Detail' className='wk-pcpd-Detail'>
        {isRefuserPromoter ? (
          <PromoterError></PromoterError>
        ) : error ? (
          <Error></Error>
        ) : (
          <View className='cards-detail-container'>
            <Swiper circular indicatorDots={goodsImages.length > 1} className='swiper'>
              {goodsImages.map((item) => {
                return (
                  <Block key={item}>
                    <SwiperItem>
                      <View
                        className='cards-image'
                        style={_safe_style_(
                          'background: transparent url(' + item + ') no-repeat 50% 50%;background-size: cover;'
                        )}
                        onClick={_fixme_with_dataset_(this.previewImg, { currenturl: item })}
                      ></View>
                    </SwiperItem>
                  </Block>
                );
              })}
            </Swiper>
            {/*  卡包名称价格信息  */}
            <View className='sale-info'>
              <View className='sale-price-info'>
                <View className='price' style={_safe_style_('color:' + templ.btnColor)}>
                  <Text className='price-symbol'>￥</Text>
                  {filters.moneyFilter(goodsDetail.wxItem.salePrice, true)}
                </View>
                <View className='order'>{'已售' + (goodsDetail.wxItem.cardSalesVolume || 0)}</View>
              </View>
              <View className='sale-order-info'>
                {goodsDetail.wxItem.provNum !== 0 && (
                  <View className='order-limit' style={_safe_style_('background:' + templ.bgGradualChange)}>
                    {'限购' + goodsDetail.wxItem.buyLimit + '组'}
                  </View>
                )}

                <Text className='title'>{goodsDetail.wxItem.name}</Text>
              </View>
            </View>
            {/*  卡包内容  */}
            <View className='card-content-wraper'>
              <View className='header'>
                <Text className='header-label' style={_safe_style_('background: ' + templ.btnColor)}></Text>
                <Text>卡包内容</Text>
              </View>
              {!!(goodsDetail.couponInfos && goodsDetail.couponInfos.length) && (
                <View className='content-wraper'>
                  {goodsDetail.couponInfos.map((item, index) => {
                    return (
                      <Block key={index}>
                        {index < (isExpand ? goodsDetail.couponInfos.length : 1) && (
                          <View className='coupon'>
                            <View className='name'>{item.name + 'x' + item.couponNum}</View>
                            <View className='coupon-detail'>
                              <View className='coupon-detail-item'>
                                <View className='label'>优惠金额</View>
                                {item.couponCategory === 2 ? (
                                  <View className='desc'>
                                    {item.discountFee / 10 + '折'}
                                    {item.minimumFee === 0 ? (
                                      <Block>(无门槛)</Block>
                                    ) : (
                                      <Block>{'(满' + filters.moneyFilter(item.minimumFee, true) + '元可用)'}</Block>
                                    )}
                                  </View>
                                ) : (
                                  <View className='desc'>
                                    {item.minimumFee === 0 ? (
                                      <Block>{filters.moneyFilter(item.discountFee, true) + '元(无门槛)'}</Block>
                                    ) : (
                                      <Block>
                                        {'满' +
                                          filters.moneyFilter(item.minimumFee, true) +
                                          '-' +
                                          filters.moneyFilter(item.discountFee, true)}
                                      </Block>
                                    )}
                                  </View>
                                )}
                              </View>
                              <View className='coupon-detail-item'>
                                <View className='label'>使用场景</View>
                                {item.useScopeType === 1 ? (
                                  <View className='desc'>线上商城</View>
                                ) : item.useScopeType === 2 ? (
                                  <View className='desc'>线下门店</View>
                                ) : (
                                  <View className='desc'>通用券</View>
                                )}
                              </View>
                              <View className='coupon-detail-item'>
                                <View className='label'>有效期</View>
                                <Block>
                                  {item.couponType === 0 ? (
                                    <View className='desc'>{'购买后' + item.fixedTerm + '天'}</View>
                                  ) : (
                                    <View className='desc'>{item.beginTime + ' 至 ' + item.endTime}</View>
                                  )}
                                </Block>
                              </View>
                              <View className='coupon-detail-item'>
                                <View className='label'>使用人群</View>
                                <View className='desc'>
                                  {item.memberLevelDTOS ? (
                                    <Block>
                                      {item.memberLevelDTOS.map((subItem, subIndex) => {
                                        return (
                                          <Text key={subIndex}>
                                            {subItem.levelName}
                                            {subIndex + 1 < item.memberLevelDTOS.length && <Text>、</Text>}
                                          </Text>
                                        );
                                      })}
                                    </Block>
                                  ) : (
                                    <Text>全部</Text>
                                  )}
                                </View>
                              </View>
                              <View className='coupon-detail-item'>
                                <View className='label'>使用范围</View>
                                <Block>
                                  {item.suitItemType === 0 ? (
                                    <View className='desc'>所有商品</View>
                                  ) : (
                                    <View className='desc'>{item.briefsStr}</View>
                                  )}
                                </Block>
                              </View>
                            </View>
                          </View>
                        )}
                      </Block>
                    );
                  })}
                  {goodsDetail.couponInfos.length > 1 && (
                    <View className='more-info' onClick={this.expandCoupon}>
                      {!isExpand && (
                        <Block>
                          <View>查看更多信息</View>
                          <Image className='right-angle' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
                        </Block>
                      )}

                      {!!isExpand && (
                        <Block>
                          <Text className='put-away' style={_safe_style_('color: ' + templ.btnColor)}>
                            收起
                          </Text>
                        </Block>
                      )}
                    </View>
                  )}
                </View>
              )}
            </View>
            {/*  商品详情  */}
            <DetailParser headerLabel='卡包描述' itemDescribe={goodsDetail.wxItem.cardExplain}></DetailParser>
          </View>
        )}

        {/*  选择商品属性对话弹框  */}
        {!!goodsDetail && (
          <ChooseAttributeDialog
            ref={(node) => (this.refChooseAttributeDialogCMPT = node)}
            onChoose={this.handlerChooseSku}
            goodsDetail={goodsDetail}
            showStock={false}
          ></ChooseAttributeDialog>
        )}

        {!!dataLoad && !isRefuserPromoter && (
          <Block>
            {isPaDetail ? (
              <Block>
                {environment !== 'wxwork' ? (
                  <Block>
                    <PaDetailTmpl
                      isFullScreen={isFullScreen}
                      goodsDetail={goodsDetail}
                      tmpStyle={templ}
                      activeStatus={goodsDetail.wxItem.pocketStatus === 3}
                      promoterCommission={promoterCommission}
                      onShare={this.handleShare.bind(this)}
                    ></PaDetailTmpl>
                    {/*  推广活动分享海报  */}
                    <PostersDialog
                      childRef={this.refShareDialogCMPT}
                      posterName={goodsDetail.wxItem.name}
                      posterImage={goodsDetail.wxItem.thumbnail}
                      uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
                    ></PostersDialog>
                    <Canvas canvasId='shareCanvas' className='promotion-canvas'></Canvas>
                  </Block>
                ) : (
                  <Block>
                    <PaDetailWxworkTmpl
                      isFullScreen={isFullScreen}
                      goodsDetail={goodsDetail}
                      tmpStyle={templ}
                      extraData={extraData}
                    ></PaDetailWxworkTmpl>
                  </Block>
                )}
              </Block>
            ) : (
              <Block>
                {/*  底部购买按钮栏  */}
                {!!dataLoad && (
                  <BuyNow
                    style='z-index: 55;'
                    disable={goodsDetail.wxItem.pocketStatus !== 1}
                    showShare={goodsDetail.channel === cardPackEnum.region}
                    operationText={operationText}
                    defaultText='立即购买'
                    onBuyNow={this.handleBuyNow}
                    immediateShare={false}
                    onShare={this.handleSelectChanel}
                    showCart={false}
                  ></BuyNow>
                )}

                {/*  分享对话弹框  */}
                <ShareDialog
                  childRef={this.refShareDialogCMPT}
                  posterTips='代金卡包，赶紧来抢购吧！'
                  posterName={goodsDetail.wxItem.name}
                  posterImage={goodsDetail.wxItem.thumbnail}
                  backgroundImage={posterBgImg}
                  salePrice={filters.moneyFilter(goodsDetail.wxItem.salePrice, true) + ''}
                  onSave={this.onSavePosterImage}
                  qrCodeParams={qrCodeParams}
                  showLabelPrice={false}
                ></ShareDialog>
                {/* shareCanvas必须放在page里，否则无法保存图片 */}
                <Canvas canvasId='shareCanvas' className='share-canvas'></Canvas>
              </Block>
            )}

            {/*  卡包活动详情*****  */}
          </Block>
        )}
      </ScrollView>
      <HomeActivityDialog showPage={'sub-packages/marketing-package/pages/card-pack/detail/index?id=' + this.itemNo}></HomeActivityDialog>
      </Block>
    );
  }
}
CardPackDetail.enableShareAppMessage = true
export default CardPackDetail;
