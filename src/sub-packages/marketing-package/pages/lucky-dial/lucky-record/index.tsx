import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro, { Config } from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import constants from '@/wxat-common/constants/index';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import hoc from '@/hoc';

const loadMoreStatus = constants.order.loadMoreStatus;

@hoc
class LuckyRecord extends React.Component {
  $router = $getRouter();
  state = {
    record: [] as any[],
    pageNo: 0,
    pageSize: 10,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
  };

  componentDidMount() {
    this.getLuckyRecordList();
  }

  getLuckyRecordList = async () => {
    const apiUrl = api.luckyDial.getUserRecords;

    const { pageNo, pageSize } = this.state;
    try {
      const res = await wxApi.request({
        url: apiUrl,
        loading: true,
        data: {
          luckyTurningId: this.$router.params.id,
          pageNo: pageNo + 1,
          pageSize,
        },
      });

      const xData = res.data || [];

      this.setState({
        record: this.state.record.concat(xData),
        pageNo: res.pageNo,
        loadMoreStatus: loadMoreStatus.HIDE,
        hasMore: xData.length === 10,
      });
    } catch (err) {
      this.setState({
        loadMoreStatus: loadMoreStatus.ERROR,
      });
    }
  };

  onRetryLoadMore = () => {
    this.onReachBottom();
  };

  onReachBottom = () => {
    if (this.state.loadMoreStatus !== loadMoreStatus.LOADING && this.state.hasMore) {
      this.getLuckyRecordList();
    }
  };

  render() {
    const { record, hasMore, loadMoreStatus } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-pll-LuckyRecord'
        className='wk-pll-LuckyRecord'
      >
        {record.length > 0 && (
          <View className='lucky-record'>
            <View className='list'>
              {record.map((item) => {
                return (
                  <View className='list-item' key={item.id}>
                    <Image className='list-item__img' src={item.luckyGiftPhoto}></Image>
                    <View className='list-item__content'>
                      <Text className='list-item__content-name'>{item.luckyGiftName}</Text>
                      <Text className='list-item__content-time'>{'中奖时间：' + item.luckyTime}</Text>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        )}

        {!!(record.length === 0 && !hasMore) && <Empty message='暂无中奖记录'></Empty>}
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default LuckyRecord;
