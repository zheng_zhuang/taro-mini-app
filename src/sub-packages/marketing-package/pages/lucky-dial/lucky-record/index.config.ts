export default {
  navigationBarTextStyle: 'black',
  enablePullDownRefresh: false,
  navigationBarTitleText: '中奖记录',
  backgroundTextStyle: 'light',
};
