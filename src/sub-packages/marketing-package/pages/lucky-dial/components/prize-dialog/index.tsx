import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';

const commonImg = cdnResConfig.common;

// giftType 为0是积分，跳转积分中心，giftType为1是优惠券跳转优惠券中心。
const giftTypeForUrl = ['/sub-packages/mine-package/pages/integral/index', '/wxat-common/pages/coupon-list/index'];

type PrizeDialogProps = {
  luckyData: any;
  onHide: Function;
};

interface PrizeDialog {
  props: PrizeDialogProps;
}

class PrizeDialog extends React.Component {
  hide = (ev: ITouchEvent) => {
    const { onHide, luckyData } = this.props;
    const tap = ev.target.dataset.tap;
    if (tap == 1) {
      onHide();
    } else if (tap == 2) {
      onHide();
      const url = giftTypeForUrl[luckyData.giftType] || '';
      if (url) {
        wxApi.$navigateTo({ url });
      }
    }
  };

  render() {
    const { luckyData } = this.props;

    return (
      <View
        data-scoped='wk-lcp-PrizeDialog'
        className='wk-lcp-PrizeDialog prize-dialog'
        onClick={_fixme_with_dataset_(this.hide, { tap: '1' })}
      >
        <View className='prize-dialog-container'>
          <Image
            onClick={_fixme_with_dataset_(this.hide, { tap: '1' })}
            src={commonImg.close}
            className='close-icon'
          ></Image>
          <View className='header'>
            恭喜您！
            <Image src={cdnResConfig.lucky_dial.dialog_header} className='header-bg'></Image>
          </View>
          <View className='prize-message'>
            <View className='prize-name'>
              获得<View style={_safe_style_('color: #F52636;')}>{luckyData.giftName}</View>
            </View>
            <Image src={luckyData.giftPhotoUrl} className='prize-img'></Image>
            <View className='tip'>运气真的这么好，再来一次？</View>
            <View className='btn-wrapper'>
              <View className='use-btn'>去查看</View>
              <View className='close-btn'>继续抽奖</View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default PrizeDialog;
