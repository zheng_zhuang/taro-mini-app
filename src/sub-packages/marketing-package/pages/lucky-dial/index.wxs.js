function getDialItemClass(index) {
  let str = '';
  switch (index) {
    case 0:
      str += 'border-top-left-radius';
      break;
    case 2:
      str += 'border-top-right-radius';
      break;
    case 4:
      str += 'start';
      break;
    case 6:
      str += 'border-bottom-left-radius';
      break;
    case 8:
      str += 'border-bottom-right-radius';
      break;
  }
  return str;
}

export default {
  getDialItemClass,
};
