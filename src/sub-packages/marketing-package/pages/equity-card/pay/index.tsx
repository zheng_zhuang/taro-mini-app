import React, { FC, useState, useEffect } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import screen from '@/wxat-common/utils/screen';
import filters from '@/wxat-common/utils/money.wxs';
import PaySuccess from '../components/pay-success';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import wxApi from '@/wxat-common/utils/wxApi';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';
import constants from '@/wxat-common/constants/index';
import pay from '@/wxat-common/utils/pay';
import { useSelector } from 'react-redux';

import './index.scss';

const PAY_CHANNEL = constants.order.payChannel;

interface EquityCardPayProps {
  isFullScreen: boolean;
  templ: any;
}

const EquityCardPay: FC<EquityCardPayProps> = () => {
  const [item, setItem] = useState({} as Record<string, any>);
  const [orderNo, setOrderNo] = useState('');
  const [paySuccess, setPaySuccess] = useState(false);
  const templ = useTemplateStyle({ autoSetNavigationBar: true });
  const isFullScreen = screen.isFullScreenPhone;
  const { currentStore } = useSelector((state: Record<string, any>) => state.base);

  useEffect(() => {
    const ECorderInfo = protectedMailBox.read('ECorderInfo');
    setItem(ECorderInfo);
  }, []);

  // 支付
  const handlePay = () => {
    // 订阅消息模板
    const ids: number[] = [subscribeEnum.PAY_SUCCESS.value];

    // 先授权订阅消息
    subscribeMsg.sendMessage(ids).then(() => {
      const options = {
        // 微信支付成功
        wxPaySuccess: (e, { orderNo }) => {
          setOrderNo(orderNo);
          setPaySuccess(true);
        },
        // 支付失败跳转到订单列表
        wxPayFail: () => {
          wxApi.$redirectTo({
            url: '/wxat-common/pages/tabbar-order/index?index=1',
          });
        },
        // 埋点上报需要商品像信息
        originParams: {
          itemDTOList: [item],
        },
      };

      pay.handleBuyNow(
        {
          expressType: 0,
          itemDTOList: [
            {
              itemNo: item.itemNo,
              itemCount: item.buyCount,
            },
          ],
        },

        PAY_CHANNEL.WX_PAY,
        options
      );
    });
  };

  return (
    <View data-scoped='wk-mpqc-pay' className='wk-mpqc-pay'>
      {!paySuccess ? (
        <Block>
          <View className='card-item'>
            <Image src={item.styleUrl[0]} mode='aspectFill' className='thumbnail' />
            <View className='content'>
              <View className='title limit-line line-2'>{item.name}</View>

              <View className='price-count-box gray'>
                <View>x{item.buyCount || 1}</View>
                <View className='money' style={{ color: templ.btnColor }}>
                  ￥{filters.moneyFilter(item.salePrice, true)}
                </View>
              </View>
            </View>
          </View>

          <View className='extra-info'>
            <View className='item'>
              <Text>核销门店</Text>
              <Text className='gray'>{currentStore.name}</Text>
            </View>
            <View className='item'>
              <Text>有效时间</Text>
              <Text className='gray'>
                {!item.validityType
                  ? '永久有效'
                  : `${filters.dateFormat(item.validityBeginTime, 'yyyy/MM/dd')}-${filters.dateFormat(
                      item.validityEndTime,
                      'yyyy/MM/dd'
                    )}`}
              </Text>
            </View>
          </View>

          <View className={'bottom-operate' + (isFullScreen ? ' fix-full-screen' : '')}>
            <View className='money-total'>
              合计：
              <Text className='money' style={{ color: templ.btnColor }}>
                ￥{filters.moneyFilter(item.totalMoney || item.salePrice, true)}
              </Text>
            </View>
            <Button onClick={handlePay} className='btn' style={{ background: templ.btnColor }}>
              确认支付
            </Button>
          </View>
        </Block>
      ) : (
        <PaySuccess templ={templ} orderNo={orderNo} />
      )}
    </View>
  );
};

export default EquityCardPay;
