import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Image, Text, Button, Block, Canvas, Swiper, SwiperItem } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';
import filters from '@/wxat-common/utils/money.wxs';
import DetailParser from '@/wxat-common/components/detail-parser/index';
import ShareDialog from '@/wxat-common/components/share-dialog-new/index';
import template from '@/wxat-common/utils/template';
import { connect } from 'react-redux';
import screen from '@/wxat-common/utils/screen';
import { WaitComponent } from '@/decorators/Wait';
import checkOptions from '@/wxat-common/utils/check-options';
import authHelper from '@/wxat-common/utils/auth-helper';
import report from '@/sdks/buried/report/index';
import shareUtil from '@/wxat-common/utils/share';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import BZSceneUtil from '@/wxat-common/utils/bz-scene-util';
import { qs2obj } from '@/wxat-common/utils/query-string';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import './index.scss';

interface StateProps {
  templ: any;
  sessionId: string;
  currentStore: Record<string, any>;
}

interface EquityCardDetail {
  props: StateProps;
}

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class EquityCardDetail extends WaitComponent {
  $router = $getRouter();
  state = {
    buyCount: 1,
    totalMoney: 0,
    detail: {},
    error: false,
    showNotGoods: false,
    notGoods: cdnResConfig.goods.notGoods,
  } as Record<string, any>;

  shareDialogRefComp = React.createRef<any>();
  itemNo: string;
  pageUrl = 'sub-packages/marketing-package/pages/equity-card/detail/index';

  async onWait() {
    const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);
    if (formattedOptions) {
      this.init(formattedOptions);
    }
  }

  onShareAppMessage() {
    const itemNo = this.itemNo;
    const pageUrl = this.pageUrl;
    const { detail } = this.state;

    const url = `/${pageUrl}?itemNo=${itemNo}`;

    report.share(true);
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.EQUITY_CARD_DETAIL,
      bzscene: shareUtil.ShareBzscene.EQUITY_CARD,
      bzId: detail.itemNo,
      bzName: `权益卡${detail && detail.name ? '-' + detail.name : ''}`,
      sceneName: detail && detail.name,
    });

    console.log('sharePath => ', path);
    return {
      title: detail.name,
      path,
    };
  }

  init(options) {
    let params = options;
    if (options.scene) {
      params = qs2obj(options.scene);
    }

    if (!params.itemNo) return;
    this.itemNo = params.itemNo;

    this.fetchData();
  }

  async fetchData() {
    try {
      const { data } = await wxApi.request({
        url: api.quityCard.detail,
        data: {
          itemNo: this.itemNo,
        },

        loading: true,
      });

      data.styleUrl = data.styleUrl.split(',');

      this.setState({
        totalMoney: data.salePrice,
        detail: data || {},
      });

      // 业务场景埋点
      BZSceneUtil.add({
        sceneType: shareUtil.ShareBZs.EQUITY_CARD_DETAIL,
        sceneId: data.itemNo,
        itemType: data.type,
        itemNo: data.itemNo,
        activityName: data.name,
      });

      // 浏览上报
      const goodsImages = data.styleUrl || [];
      const reportParams = {
        source: 'equity_card',
        itemNo: data.itemNo,
        itemType: data.type,
        name: data.name,
        salePrice: data.salePrice,
        thumbnail: goodsImages[0],
        sessionId: report.getBrowseItemSessionId(data.itemNo),
      };

      report.browseItem(reportParams);
    } catch (e) {
      // this.setState({ error: true });
      this.setState({
        showNotGoods: true,
      });
    }
  }

  handleJumpToHome = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  handleShowShareDialog = () => {
    this.shareDialogRefComp.current.show();
  };

  handleCanvas = () => {
    this.shareDialogRefComp.current.savePosterImage(this);
  };

  handleSetBuyCount = (action) => {
    const {
      detail: { salePrice },
    } = this.state;
    let { buyCount, totalMoney } = this.state;

    const subscriber = {
      decrease() {
        buyCount > 1 && buyCount--;
      },
      increase() {
        buyCount++;
      },
    };

    subscriber[action]();
    totalMoney = salePrice * buyCount;

    this.setState({
      buyCount,
      totalMoney,
    });
  };

  // 立即购买
  handleJumpToBuy = () => {
    if (!authHelper.checkAuth()) {
      return;
    }

    const { detail, buyCount, totalMoney } = this.state;

    const url = 'sub-packages/marketing-package/pages/equity-card/pay/index';
    const data = {
      ...detail,
      buyCount,
      totalMoney,
    };

    protectedMailBox.send(url, 'ECorderInfo', data);

    wxApi.$navigateTo({
      url,
    });
  };

  /**
   * 轮播预览大图
   */
  previewImg = (item) => {
    const currentUrl = item;
    const previewUrls = this.state.detail.styleUrl;
    wxApi.previewImage({
      current: currentUrl,
      urls: previewUrls,
    });
  };

  render() {
    const isFullScreen = screen.isFullScreenPhone;
    const { templ, currentStore } = this.props;
    const { detail, buyCount, totalMoney, error, showNotGoods, notGoods } = this.state;

    // 海报分享
    const uniqueShareInfoForQrCode = {
      page: this.pageUrl,
      itemNo: this.itemNo,
      refBz: shareUtil.ShareBZs.EQUITY_CARD_DETAIL,
      bzscene: shareUtil.ShareBzscene.EQUITY_CARD,
      refBzId: this.itemNo,
      refBzName: `权益卡${detail && detail.name ? '-' + detail.name : ''}`,
      refSceneName: detail && detail.name,
    };

    const styleUrl = detail.styleUrl || [];
    return (
      <View data-scoped='wk-mpqc-detail' className='wk-mpqc-detail'>
        {!!detail.id && (
          <Block>
            <Swiper circular indicatorDots={styleUrl.length > 1} className='swiper'>
              {styleUrl.map((item) => {
                return (
                  <Block key={item}>
                    <SwiperItem>
                      <Image
                        src={item}
                        onClick={this.previewImg.bind(this, item)}
                        mode='aspectFill'
                        className='image'
                      />
                    </SwiperItem>
                  </Block>
                );
              })}
            </Swiper>

            <View className='goods-info'>
              <View className='goods-name limit-line line-2'>
                <Text>{detail.name}</Text>
              </View>
              <View className='price-count-box'>
                <Text className='price'>￥{filters.moneyFilter(detail.salePrice, true)}</Text>
                <Text className='gray'>已售{(detail.cardSalesVolume || 0) + (detail.virtualSalesAmount || 0)}件</Text>
              </View>

              <View className='extra-info'>
                <View className='item'>
                  <Text>核销门店</Text>
                  <Text className='gray'>{currentStore.name}</Text>
                </View>
                <View className='item'>
                  <Text>有效时间</Text>
                  <Text className='gray'>
                    {!detail.validityType
                      ? '永久有效'
                      : `${filters.dateFormat(detail.validityBeginTime, 'yyyy/MM/dd')}-${filters.dateFormat(
                          detail.validityEndTime,
                          'yyyy/MM/dd'
                        )}`}
                  </Text>
                </View>
              </View>
            </View>

            {/* <View className='buy-count-box'>
            <View className='item'>
            <Text>数量</Text>
            <View className='num-operation-box'>
              <View onClick={this.handleSetBuyCount.bind(this, 'decrease')} className='num-decrease'>
                -
              </View>
              <View className='num-input'>{buyCount}</View>
              <View onClick={this.handleSetBuyCount.bind(this, 'increase')} className='num-increase'>
                +
              </View>
            </View>
            </View>
            <View className='item'>
            <Text>小计</Text>
            <Text className='money'>￥{filters.moneyFilter(totalMoney, true)}</Text>
            </View>
            </View> */}

            <View className='rich-text-box'>
              <DetailParser itemDescribe={detail.cardDesc} showHeadline={false} showHeader headerLabel='商品详情' />
            </View>

            <View className={'bottom-operate ' + (isFullScreen ? 'fix-full-screen' : '')}>
              <View className='image-btn-box'>
                <Button onClick={this.handleJumpToHome} className='image-btn'>
                  <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-home.png"></Image>
                  <Text>返回首页</Text>
                </Button>
                <Button onClick={this.handleShowShareDialog} className='image-btn'>
                  <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-share.png"></Image>
                  <Text>分享</Text>
                </Button>
              </View>
              {!!detail.isShelf && detail.stock > 0 ? (
                <Button onClick={this.handleJumpToBuy} className='buy-btn' style={{ background: templ.btnColor }}>
                  立即购买
                </Button>
              ) : (
                <Button className='buy-btn disabled'>{detail.isShelf ? '已售罄' : '已下架'}</Button>
              )}
            </View>

            {/* 分享对话弹框 */}
            <ShareDialog
              childRef={this.shareDialogRefComp}
              posterTips='专属权益卡，赶紧来抢购吧！'
              posterSalePriceLabel='优惠价'
              posterName={detail.name}
              posterImage={detail.styleUrl[0]}
              salePrice={filters.moneyFilter(detail.salePrice, true)}
              salesType={0}
              uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
              onSave={this.handleCanvas}
            />

            {/* shareCanvas必须放在page里，否则无法保存图片 */}
            <Canvas canvasId='shareCanvas' className='share-canvas'></Canvas>
          </Block>
        )}

        {showNotGoods && (
          <View className='not-goods-container'>
            <Image src={notGoods} className='not-img' />
            <Text className='not-goods-txt'>商品跑丢了</Text>
          </View>
        )}
      </View>
    );
  }
}

export default EquityCardDetail;
