import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import wxApi from '@/wxat-common/utils/wxApi';

import './index.scss';

type EquityCardPaySuccessProps = {
  templ: any;
  orderNo: string;
};

let EquityCardPaySuccess: FC<EquityCardPaySuccessProps> = (props) => {
  const { templ, orderNo } = props;
  const successImage = cdnResConfig.pay_success.pay_success;

  const handleJumpToOrder = () => {
    wxApi.$navigateTo({
      url: `/sub-packages/order-package/pages/order-details/index?orderNo=${orderNo}`,
    });
  };

  const handleJumpToHome = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  return (
    <View data-scoped='wk-mpqc-pay-success' className='wk-mpqc-pay-success'>
      <View className='wrap'>
        <Image className='tips-img' src={successImage}></Image>
        <View className='tips'>购买成功</View>

        <View className='btn-box'>
          <Button
            onClick={handleJumpToOrder}
            className='btn'
            style={{ borderColor: templ.btnColor, color: templ.btnColor }}
          >
            查看订单详情
          </Button>
          <Button onClick={handleJumpToHome} className='btn' style={{ background: templ.btnColor, color: '#fff' }}>
            返回首页
          </Button>
        </View>
      </View>
    </View>
  );
};

EquityCardPaySuccess.defaultProps = {
  templ: {},
  orderNo: '',
};

export default EquityCardPaySuccess;
