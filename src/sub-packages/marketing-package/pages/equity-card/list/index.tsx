import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Empty from '@/wxat-common/components/empty/empty';
import screen from '@/wxat-common/utils/screen';
import useList from '@/hooks/useList';
import EquityCardItem, { EquityCard } from '@/wxat-common/components/equity-card-item';

import './index.scss';

let EquityCardList: FC = () => {
  const isFullScreen = screen.isFullScreenPhone;

  const { list, hasMore, status, retry } = useList({
    query: async (page, pageSize) => {
      const params = { pageNo: page, pageSize, type: 37 };

      const { data } = await wxApi.request({
        url: api.quityCard.list,
        data: params,
        loading: true,
      });

      return data || [];
    },
  });

  return (
    <View data-scoped='wk-mpqc-list' className='wk-mpqc-list'>
      {list.length > 0 && (
        <View className={'wrap ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {list.map((item: EquityCard) => (
            <EquityCardItem item={item} key={item.itemNo} />
          ))}
        </View>
      )}

      {!!(list.length === 0 && !hasMore) && <Empty message='敬请期待...'></Empty>}
      <LoadMore status={status} onRetry={retry}></LoadMore>
    </View>
  );
};

export default EquityCardList;
