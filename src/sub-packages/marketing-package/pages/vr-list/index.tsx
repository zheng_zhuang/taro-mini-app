import React, { useEffect, useState, FC } from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import wxApi from '@/wxat-common/utils/wxApi';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import Empty from '@/wxat-common/components/empty/empty';
import VRModule from '@/wxat-common/components/decorate/VRModule';
import './index.scss';
import useTemplateStyle from '@/hooks/useTemplateStyle';

let VRList: FC = () => {
  const [dataSource, setDataSource] = useState({ data: [] });
  useTemplateStyle({ autoSetNavigationBar: true });

  useEffect(() => {
    const VRData = protectedMailBox.read('VRData');

    wxApi.setNavigationBarTitle({
      title: VRData.textNavSource.title,
    });

    setDataSource(VRData);
  }, []);

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-vr-list' className='wk-vr-list'>
      {!!dataSource && !!dataSource.data.length ? (
        <VRModule dataSource={dataSource} limit={999} showNav={false}></VRModule>
      ) : (
        <Empty message='敬请期待...'></Empty>
      )}
    </View>
  );
};

export default VRList;
