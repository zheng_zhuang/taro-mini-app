import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import store from '@/store/index';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import constants from '@/wxat-common/constants/index.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import template from '@/wxat-common/utils/template.js';
import util from '@/wxat-common/utils/util.js';
import HoverCart from '@/wxat-common/components/cart/hover-cart/index';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import ProductModule from '@/wxat-common/components/base/productModule';
import ServeModule from '@/wxat-common/components/base/serveModule';
// import CardModule from '@/wxat-common/components/cardModule/index'

import './index.scss';
import displayEnum from '@/wxat-common/constants/displayEnum';
import shareUtil from '@/wxat-common/utils/share.js';
import GoodsListFilter from '@/wxat-common/components/goods-list-filter/index';

const app = store.getState();
const loadMoreStatusEnum = constants.order.loadMoreStatus;

@hoc
class More extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    // 0、更多服务 1、更多产品 2、更多卡项
    sourceType: null,
    dataSource: [],
    error: false,
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    showType: null,
    title: '',
    sortType: null,
    sortField: null,
  };

  /**
   * 生命周期函数--监听页面加载
   */
  componentDidMount() {
    const options = this.$router.params;
    const showType = options.display || options.showType || displayEnum.VERTICAL; // display是商品类的展示字段，showType是营销组件类的展示字段
    this.getTemplateStyle();
    const x = parseInt(options.type);
    let title = '';
    if (!!options.name) {
      wxApi.setNavigationBarTitle({
        title: options.name,
      });

      title = options.name;
    } else {
      if (x == 0) {
        wxApi.setNavigationBarTitle({
          title: '服务列表',
        });

        title = '服务列表';
      } else if (x == 1) {
        wxApi.setNavigationBarTitle({
          title: '产品列表',
        });

        title = '产品列表';
      } else if (x == 32) {
        wxApi.setNavigationBarTitle({
          title: '楼盘列表',
        });

        title = '楼盘列表';
      } else {
        wxApi.setNavigationBarTitle({
          title: '卡项列表',
        });

        title = '卡项列表';
      }
    }
    let links;
    if (x === 0) {
      links = api.goods.list;
    } else if (x === 1 || x === 32) {
      links = api.classify.itemSkuList;
    } else {
      links = api.card.list;
    }
    (this.industry = app.globalData.industry),
      (this.pageNo = 1),
      (this.url = links),
      this.setState(
        {
          sourceType: x,
          showType: showType,
          title,
        },

        () => {
          this.getList();
        }
      );
  }

  onShareAppMessage() {
    const { sourceType, showType, title } = this.state;
    const path = shareUtil.buildShareUrlPublicArguments({
      url: `/sub-packages/marketing-package/pages/more/index?display=${showType}&type=${sourceType}&name=${title}`,
      bz: shareUtil.ShareBZs.MORE,
      bzName: title,
    });

    console.log('sharePath => ', path);
    return {
      title,
      path,
    };
  }

  onFilterChange = (detail) => {
    const { sortField, sortType } = detail;
    this.setState(
      {
        sortType,
        sortField,
      },

      () => {
        this.pageNo = 1;
        this.hasMore = true;
        this.getList(true);
      }
    );
  };

  industry = app.globalData.industry;
  pageNo = 1;
  hasMore = true;
  url = null;

  getList(isFromPullDown) {
    this.setState({
      error: false,
    });

    const params = {
      status: 1,
      type: this.state.sourceType === 2 ? goodsTypeEnum.TIME_CARD.value : this.state.sourceType,
      isShelf: 1,
      pageNo: this.pageNo,
      pageSize: 10,
    };

    // 判断请求的商品类型为产品时，则删除请求参数中的type，改用typeList包含产品类型及组合商品类型
    if (params.type === goodsTypeEnum.PRODUCT.value) {
      params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];
    }

    if (params.type === goodsTypeEnum.PRODUCT.value) {
      params.sortType = this.state.sortType;
      params.sortField = this.state.sortField;
    }

    wxApi
      .request({
        url: this.url,
        loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true) {
          let dataSource = this.state.dataSource || [];
          if (this.isLoadMoreRequest()) {
            dataSource = dataSource.concat(res.data || []);
          } else {
            dataSource = res.data || [];
          }
          if (dataSource && dataSource.length === res.totalCount) {
            this.hasMore = false;
          }
          this.setState({
            dataSource,
          });
        }
      })
      .catch(() => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatusEnum.ERROR,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      })
      .finally(() => {
        if (isFromPullDown) {
          wxApi.stopPullDownRefresh();
        }
      });
  }

  isLoadMoreRequest() {
    return this.pageNo > 1;
  }

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.pageNo = 1;
    this.hasMore = true;
    this.getList(true);
  }

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if (this.hasMore) {
      this.pageNo++;
      this.getList();
    }
  }

  onRetryLoadMore() {
    this.setState({
      loadMoreStatus: loadMoreStatusEnum.LOADING,
    });

    this.getList(this.url, this.state.sourceType);
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { dataSource, error, showType, loadMoreStatus, sourceType } = this.state;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-mpm-More' className='wk-mpm-More'>
        <View>
          {sourceType === 0 && (
            <View className='serve-list'>
              {!!(!!dataSource && dataSource.length == 0) && <Empty message='敬请期待...'></Empty>}
              {!!error && <Error />}
              {!!(!!dataSource && !!dataSource.length) && (
                <ServeModule list={dataSource} display={showType}></ServeModule>
              )}

              <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>
            </View>
          )}

          {sourceType === 1 && (
            <View className='goods-list'>
              {!!(!!dataSource && dataSource.length == 0) && <Empty message='敬请期待...'></Empty>}
              {!!error ? (
                <Error></Error>
              ) : (
                <View>
                  {!!(!!dataSource && !!dataSource.length) && (
                    <Block>
                      <GoodsListFilter onFilterChange={this.onFilterChange}></GoodsListFilter>
                      <ProductModule list={dataSource} display={showType}></ProductModule>
                    </Block>
                  )}

                  <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>
                </View>
              )}
            </View>
          )}

          {/*{(sourceType === 2 || sourceType === 3 || sourceType === 4) && (
                     <View className='cards-list'>
                       {!!dataSource && dataSource.length == 0 && (
                         <Empty message='暂无卡项'></Empty>
                       )}
                       {error && <Error></Error>}
                       {dataSource && dataSource.length && (
                         <Cardmodule dataSource={dataSource}></Cardmodule>
                       )}
                       <LoadMore
                         status={loadMoreStatus}
                         onRetry={this.onRetryLoadMore}
                       ></LoadMore>
                     </View>
                    )} */}
        </View>
        {sourceType === 1 && <HoverCart />}
      </View>
    );
  }
}

export default More;
