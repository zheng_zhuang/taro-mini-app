import React from 'react'
import template from "@/wxat-common/utils/template";
import wxApi from '@/wxat-common/utils/wxApi'
import { $getRouter } from '@/wxat-common/utils/platform';
import { connect } from 'react-redux';
import { Block, View,RichText} from '@tarojs/components';
import './index.scss'
interface IState{
  nodes:any,
  H5PayUrl:any,
  jumpType:any,
  tmpStyle:any
}
interface IProps{
  tabbars:any,
  iosSetting:any
}
const mapStateToProps = (state) => {
  return {
    tabbars: state.globalData.tabbars,
    iosSetting:state.globalData.iosSetting
  };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class PayH5SubmitSuccess extends React.Component<IProps,IState> {
  $router = $getRouter()
  state = {
    nodes:'',
    H5PayUrl:'',
    jumpType:'1',
    tmpStyle:''
  }
  static defaultProps={
    tabbars:[]
  }
  componentDidMount () {
    const options = this.$router.params
    this.setState({
      nodes: this.props.iosSetting.text,
      H5PayUrl: decodeURIComponent(options.H5PayUrl),
      jumpType: this.props.iosSetting.jumpType
    })
    this.getTemplateStyle()
  }
  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle
    })
  }
  // 点击复制支付链接
  copyPayUrl = (e) => {
    wxApi.setClipboardData({
      data: this.state.H5PayUrl,
      success: function(res) {
        wxApi.getClipboardData({
          success: function(res) {
            wxApi.showToast({
              icon: 'succees',
              title: '复制成功'
            })
          }
        })
      }
    })
  }
  //回到首页
  goHome=()=>{
    let url= "/wxat-common/pages/home/index"
    const {tabbars} = this.props
    if (tabbars && tabbars.list){
      url= tabbars.list[0].pagePath;
    }
    wxApi.$navigateTo({
      url: url
    });
  }
  render(){
    const {nodes,H5PayUrl,jumpType,tmpStyle} = this.state
    return(
      <View className="payH5">
        <View className="payH5-wrap">
          <RichText nodes={nodes}></RichText>
        </View>
        {
          (H5PayUrl && jumpType == 1) ? <View className="btn" onClick={this.copyPayUrl} style={{'background':tmpStyle.btnColor}}>复制链接</View> : <View className="btn" onClick={this.goHome} style={{'background':tmpStyle.btnColor}}>返回首页</View>
        }
      </View>
    )
  }
}
export default PayH5SubmitSuccess
