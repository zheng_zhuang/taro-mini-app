import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(AnimatDialog)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Swiper, SwiperItem, Image, Text, ScrollView, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import withWeapp from '@tarojs/with-weapp'
import filters from '../../../../../wxat-common/utils/money.wxs.js';
// import wxClass from '../../../../../wxat-common/utils/wxClass.js'
import template from '../../../../../wxat-common/utils/template.js';
import industryEnum from '../../../../../wxat-common/constants/industryEnum.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import api from "../../../../../wxat-common/api";
// import state from '../../../../../state/index.js'
import { connect } from 'react-redux';
import hoc from '@/hoc/index';
import report from "../../../../../sdks/buried/report";
import timer from '../../../../../wxat-common/utils/timer.js';
import seckillEnum from '../../../../../wxat-common/constants/seckillEnum.js';
import constants from "../../../../../wxat-common/constants";
import protectedMailBox from '../../../../../wxat-common/utils/protectedMailBox.js';
import goodsTypeEnum from '../../../../../wxat-common/constants/goodsTypeEnum.js';
import pageLinkEnum from '../../../../../wxat-common/constants/pageLinkEnum.js';
import shareUtil from '../../../../../wxat-common/utils/share.js';

import FrontMoneyItem from "../../../../../wxat-common/components/front-money-item";
import ShareDialog from "../../../../../wxat-common/components/share-dialog";
// import ShareDialog from "../../../../../wxat-common/components/share-dialog-new";
import CombinationItemDetail from "../../../../../wxat-common/components/combination-item-detail";
import ChooseAttributeDialog from '../../../../../wxat-common/components/choose-attribute-dialog';
import AnimatDialog from "../../../../../wxat-common/components/animat-dialog";
import BuyNow from '../../../../../wxat-common/components/buy-now';
import DetailParser from "../../../../../wxat-common/components/detail-parser";
import CommentModule from "../../../../../wxat-common/components/comment-module";
import TaskError from "../../../../../wxat-common/components/task-error";
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import { getDecorateHomeData } from '@/wxat-common/x-login/decorate-configuration-store.ts';
import DialogModule from '@/wxat-common/components/base/dialogModule/index';

import getStaticImgUrl from '../../../../../wxat-common/constants/frontEndImgUrl'
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import './index.scss';
import checkOptions from '@/wxat-common/utils/check-options.js';
import { qs2obj } from '@/wxat-common/utils/query-string';
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'
const commonImg = cdnResConfig.common;

const mapStateToProps = (state) => ({
  allState: state,
  globalData: state.globalData,
  homeChangeTime: state.globalData.homeChangeTime,
  iosSetting:state.globalData.iosSetting
});

interface SeckillDetailProps { allState: any; globalData: any;iosSetting:any }
type IProps = SeckillDetailProps;

interface SeckillDetail {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class SeckillDetail extends React.Component {
  $router = $getRouter();
  private chooseAttributeCMPT: any;
  private childsCMTP: any;
  iosPayLimitDialog = React.createRef<any>()
  state: Record<string, any> = {
    defaultText: null,
    isGradualChange: null,
    tmpStyle: {}, // 主题模板配置

    industryEnum, // 行业类型常量
    industry: this.props.globalData.industry, // 行业类型

    // 秒杀规则
    rule: {
      title: '秒杀规则', // 秒杀规则标题
      setions: [
        // 秒杀规则说明
        '1、必须在活动期间内支付，否则不享受秒杀价。',
        '2、购买时需符合限购数量，超出限购数量不可下单。',
      ],

      posterType: '',
      posterContent: '',
      posterLogo: '',
      useLogisticsCoupon: 0,
    },

    itemNo: null, // 商品单号
    activityId: null, // 秒杀活动id

    goodsImages: [], // 商品图片
    goodsDetail: null, // 活动商品详情
    goodType: constants.goods.type.seckill, // 商品类型
    seckillItemDTO: {}, // 秒杀活动详情
    seckillEnumStatus: seckillEnum.STATUS, // 秒杀活动状态常量
    currentStatus: null, // 秒杀活动当前状态
    dataLoad: false,

    limitAmountTitle: null, // 限购类型标题，用户选择商品属性的时候，展示限购信息

    navigateToData: {}, // 跳转页面所需支付的参数，以便获取支付金额
    uniqueShareInfoForQrCode: null,
    skuHighPrice: 0,
    thredId: null,
    contactConfig: null,
  };

  async componentDidMount() {
    this.getTemplateStyle(); // 获取主题模板配置

    const options = await checkOptions.checkOnLoadOptions(this.$router.params);
    this.init(options);

    // 首页配置
    const homeConfig = getDecorateHomeData();
    if (homeConfig && homeConfig.length) {
      const contactConfig = homeConfig.find((item) => item.id === 'dialogModule') || null;
      this.setState({ contactConfig });
    }
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.homeChangeTime !== this.props.homeChangeTime) {
      // 首页配置
      const homeConfig = getDecorateHomeData();
      if (homeConfig && homeConfig.length) {
        const contactConfig = homeConfig.find((item) => item.id === 'dialogModule') || null;
        this.setState({ contactConfig });
      }
    }
  }

  async componentDidUpdate(nextProps,nextState) {
    if (!this.state.activityId) {
      const options = await checkOptions.checkOnLoadOptions(this.$router.params);
      this.init(options);
    }
  }

  /**
   * 生命周期函数--监听页面卸载
   */
  componentWillUnmount(): void {
    if (this.state.thredId) {
      timer.deleteQueue(this.state.thredId);
    }
  }

  // /**
  //  * 监听器
  //  */

  // stateChange = (data) => {
  //   // 是否已经登录您，存在sessionId
  //   if (data && data.key === 'sessionId') {
  //     this.initRenderData(); // 初始化渲染的数据
  //   }
  // };
  /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */

  onShareAppMessage() {
    report.share(true);
    // this.getShareDialog().hide()
    // this.hideShareDialog();
    const { itemNo, activityId, goodsDetail } = this.state;
    if (goodsDetail) {
      const url = `/sub-packages/marketing-package/pages/seckill/detail/index?itemNo=${itemNo}&activityId=${activityId}`;
      const activityName = goodsDetail.seckillItemDTO.name ? goodsDetail.seckillItemDTO.name : '';
      const path = shareUtil.buildShareUrlPublicArguments({
        url,
        bz: shareUtil.ShareBZs.SECKILL_DETAIL,
        bzName: `秒杀活动${activityName ? '-' + activityName : ''}`,
      });

      console.log('sharePath => ', path);
      return {
        title: activityName,
        path,
      };
    } else {
      return {};
    }
  }

  async init(options) {
    if (!options) return;

    if (typeof options.scene === 'string') {
      options = qs2obj(options.scene);
    }

    const { globalData } = this.props;

    await new Promise((resolve) => {
      this.setState(
        {
          ...options,
          sessionId: report.getBrowseItemSessionId(options.itemNo),
          industry: globalData.industry,
        },

        resolve
      );
    });

    await new Promise((resolve) => {
      this.setState(
        {
          defaultText: this.defaultTextMethod(),
          isGradualChange: this.isGradualChangeMethod(),
        },

        resolve
      );
    });

    if (this.props.allState.base.sessionId) {
      this.initRenderData(); // 初始化渲染的数据
    }
  }

  /**
   * 获取邀请好友对话弹框
   */
  // getShareDialog() {
  //   if (!this._shareDialog) {
  //     this._shareDialog = this.selectComponent('#share-dialog')
  //   }
  //   return this._shareDialog
  // }

  // 保存图片
  handleSavePosterImage = () => {
    if (this.refShareDialogCMTP.current) {
      this.refShareDialogCMTP.current.savePosterImage(this);
    }
  };

  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel() {
    console.log('进入了2');
    if (this.refShareDialogCMTP.current) {
      this.refShareDialogCMTP.current.show();
    }
    // this.showShareDialog()
  }

  // 原computed
  // 是否禁止编辑
  isDisableMethod() {
    // fixme 保留库存不足的逻辑
    if (this.state.goodsDetail) {
      // 参与秒杀商品剩余数量集合
      const activityRemain =
        this.state.goodsDetail.seckillItemDTO && this.state.goodsDetail.seckillItemDTO.activityRemain
          ? this.state.goodsDetail.seckillItemDTO.activityRemain
          : [];

      if (this.state.goodsDetail.wxItem.isShelf === 0) {
        return true;
      } else if (
        this.state.goodsDetail.wxItem.type !== goodsTypeEnum.SERVER.value &&
        this.state.goodsDetail.wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value &&
        this.state.goodsDetail.wxItem.type !==
        goodsTypeEnum.SLEEVE_SYSTEM.value &&
        this.state.goodsDetail.wxItem.type !== goodsTypeEnum.TICKETS.value &&
        this.state.goodsDetail.wxItem.itemStock <= 0
      ) {
        return true;
      } else if (activityRemain.length > 0) {
        // 判断秒杀商品数量是否还有剩余
        const isHaveRemain = activityRemain.find((item) => {
          return !!item.totalRemainCount;
        });
        if (this.state.currentStatus === this.state.seckillEnumStatus.NOT_STARTED.value) {
          // 活动未开始时，判断是否限制秒杀下单
          if (this.state.seckillItemDTO.orderRestrict) {
            return true;
          }
        }
        return !isHaveRemain;
      }
    }
    return false;
  }

  // 底部操作按钮文字
  operationTextMethod() {
    const iosSetting = this.props.iosSetting
    // fixme 保留库存不足的逻辑
    if (this.state.goodsDetail) {
      // 参与秒杀商品剩余数量集合
      const activityRemain =
        this.state.goodsDetail.seckillItemDTO && this.state.goodsDetail.seckillItemDTO.activityRemain
          ? this.state.goodsDetail.seckillItemDTO.activityRemain
          : [];

      if (this.state.goodsDetail.wxItem.isShelf === 0) {
        return '商品已下架';
      } else if (
        this.state.goodsDetail.wxItem.type !== goodsTypeEnum.SERVER.value &&
        this.state.goodsDetail.wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value &&
        this.state.goodsDetail.wxItem.type !==
        goodsTypeEnum.SLEEVE_SYSTEM.value &&
        this.state.goodsDetail.wxItem.type !== goodsTypeEnum.TICKETS.value &&
        this.state.goodsDetail.wxItem.itemStock <= 0
      ) {
        return '库存不足';
      } else if (activityRemain.length > 0) {
        // 判断秒杀商品数量是否还有剩余
        const isHaveRemain = activityRemain.find((item) => {
          return !!item.totalRemainCount;
        });
        // 活动未开始时，判断是否限制秒杀下单
        if (this.state.currentStatus === this.state.seckillEnumStatus.NOT_STARTED.value) {
          if (this.state.seckillItemDTO.orderRestrict) {
            return '即将开抢';
          }
        }
        let text ='立即购买'
        if(iosSetting && iosSetting?.payType && this.state.goodsDetail?.wxItem.type == 41){
          text = iosSetting.buttonCopywriting;
        }
        return isHaveRemain ? '立即购买' : '已抢光';
      }
      let text = "立即购买";
      if (iosSetting && iosSetting.payType && this.state.goodsDetail?.wxItem.type == 41) {
        text = iosSetting.buttonCopywriting;
      }
      return text;
    }
  }

  // 底部按钮默认文字
  defaultTextMethod() {
    let iosSetting = this.props.iosSetting;
      let text = "立即购买";
      if (iosSetting && iosSetting.payType && this.state.goodsDetail?.wxItem.type == 41) {
        text = iosSetting.buttonCopywriting;
      }
    if (this.state.goodsDetail) {
      if (this.state.currentStatus === this.state.seckillEnumStatus.ON_GOING.value) {
        return text;
      }
      let text2 = '原价购买'
      if (iosSetting && iosSetting.payType && this.state.goodsDetail?.wxItem.type == 41) {
        text2 = ' '+iosSetting.buttonCopywriting;
      }
      const salePrice = this.state.goodsDetail.wxItem.salePrice / 100;
      return '￥' + salePrice + text2;
    }
    return text;
  }

  // 是否渐变
  isGradualChangeMethod() {
    if (this.state.currentStatus === this.state.seckillEnumStatus.ON_GOING.value) {
      return true;
    }
    return false;
  }

  /**
   * 初始化渲染的数据
   */
  initRenderData() {
    if (this.state.dataLoad) {
      return;
    }
    this.setState(
      {
        dataLoad: true,
      },

      () => {
        this.getGoodsDetail(); // 获取商品详情
        this.querySetting(); // 查询活动规则
      }
    );
  }

  /**
   * 获取模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
   * 获取商品详情
   */
  getGoodsDetail = () => {
    const { activityId } = this.state;
    const params = {
      type: goodsTypeEnum.SEC_KILL.value,
      activityId,
    };

    console.log('getGoodsDetail==', goodsTypeEnum.SEC_KILL.value, activityId);
    wxApi
      .request({
        url: api.goods.detail,
        quite: true,
        data: params,
      })
      .then((res) => {
        console.log('res', res.data);
        const goodsDetail = res.data;
        if (goodsDetail) {
          const seckillItemDTO = goodsDetail.seckillItemDTO || {};
          if (goodsDetail.wxItem && Object.keys(goodsDetail.wxItem).length > 0) {
            wxApi.setNavigationBarTitle({
              title: goodsDetail.wxItem.name,
            });
          }

          let skuHighPrice = 0;
          if (goodsDetail.wxItem.salePriceRange) {
            skuHighPrice = goodsDetail.wxItem.salePriceRange;

            if (goodsDetail.wxItem.salePriceRange.indexOf('-') !== -1) {
              skuHighPrice = goodsDetail.wxItem.salePriceRange.split('-')[1]; // 售价取最高价格
            }
          }

          this.setState(
            {
              goodsImages: goodsDetail.materialUrls || [],
              goodsDetail,
              seckillItemDTO,
              skuHighPrice,
              error: false,
              uniqueShareInfoForQrCode: {
                activityId: activityId,
                page: 'sub-packages/marketing-package/pages/seckill/detail/index',
                refBz: shareUtil.ShareBZs.SECKILL_DETAIL,
                refBzName: `秒杀活动${seckillItemDTO.name ? '-' + seckillItemDTO.name : ''}`,
              },
            },

            () => {
              this.queryActivityRemain(); // 查询参与秒杀商品剩余数量
              if (Object.keys(this.state.seckillItemDTO).length > 0) {
                console.log('ssssssss');
                // 初始化时执行一次格式化时间及赋值
                this.checkSeckillActivityStatus(); // 检查秒杀活动状态
                // 执行定时器进行时间倒数
                const thredId = timer.addQueue(() => {
                  this.checkSeckillActivityStatus(); // 检查秒杀活动状态
                });
                this.setState({ thredId });
              }
            }
          );
        } else {
          this.apiError();
        }
      })
      .catch(() => {
        this.apiError();
      });
  };

  /**
   * 展示错误页面
   * @param {*} error
   */
  apiError() {
    this.setState({
      error: true,
    });
  }

  /**
   * 检查秒杀活动状态
   */
  checkSeckillActivityStatus() {
    const seckillItemDTO = this.state.seckillItemDTO;
    if (Object.keys(seckillItemDTO).length < 1) {
      return;
    }

    const startCountDown = +seckillItemDTO.startCountDown;
    const endCountDown = +seckillItemDTO.endCountDown;
    let currentStatus = this.state.seckillEnumStatus.NOT_STARTED.value; // 未开始

    // 剩余时间每秒减1，因为是时间戳，所以每秒减1000
    seckillItemDTO.startCountDown = startCountDown && startCountDown > 0 ? startCountDown - 1000 : 0;
    seckillItemDTO.endCountDown = endCountDown && endCountDown > 0 ? endCountDown - 1000 : 0;

    if (startCountDown && endCountDown && startCountDown < endCountDown) {
      seckillItemDTO.remainTitle = '距开始';
      seckillItemDTO.remainTime = Math.floor(startCountDown / 1000);
    } else if (endCountDown && endCountDown > 0) {
      seckillItemDTO.remainTitle = '距结束';
      seckillItemDTO.remainTime = Math.floor(endCountDown / 1000);
      currentStatus = this.state.seckillEnumStatus.ON_GOING.value; // 进行中
    } else {
      seckillItemDTO.remainTitle = '距结束';
      seckillItemDTO.remainTime = 0;
      currentStatus = this.state.seckillEnumStatus.ENDED.value; // 已结束

      // 活动结束删除定时器
      if (this.state.thredId) {
        timer.deleteQueue(this.state.thredId);
        this.setState({ thredId: null });
      }
    }

    this.updateTime(seckillItemDTO); // 格式化倒计时
    this.setState({
      seckillItemDTO,
      currentStatus, // 秒杀活动当前状态
    });
  }

  /**
   * 格式化倒计时
   * @param {*} obj
   */
  updateTime(obj) {
    if (!obj.remainTime || obj.remainTime <= 0) {
      obj.displayTime = {
        hour: '00',
        minute: '00',
        second: '00',
      };

      return;
    }

    const day = Math.floor(+obj.remainTime / (24 * 3600));

    const hourS = Math.floor(+obj.remainTime % (24 * 3600));
    const hourInt = Math.floor(+hourS / 3600);
    const hour = hourInt < 10 ? '0' + hourInt : hourInt;

    const minuteS = Math.floor(+hourS % 3600);
    const minuteInt = Math.floor(+minuteS / 60);
    const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

    const secondS = Math.floor(+minuteS % 60);
    const second = secondS < 10 ? '0' + secondS : secondS;
    if (day) {
      obj.displayTime = {
        day: day < 10 ? '0' + day : day,
        hour: hour + '',
        minute: minute + '',
        second: second + '',
      };
    } else {
      obj.displayTime = {
        hour: hour + '',
        minute: minute + '',
        second: second + '',
      };
    }
  }

  /**
   * 查询参与秒杀商品剩余数量
   */
  queryActivityRemain = () => {
    const params = {
      activityId: this.state.activityId,
    };

    wxApi
      .request({
        url: api.seckill.queryActivityRemain,
        data: params,
      })
      .then((res) => {
        if (res.data) {
          // 在商品详情的秒杀活动详情中增加参数：参与秒杀商品剩余数量集合
          const seckillItemDTO = this.state.goodsDetail.seckillItemDTO;
          if (seckillItemDTO) {
            seckillItemDTO.activityRemain = res.data;
          }
          this.setState({
            goodsDetail: this.state.goodsDetail,
          });

          console.log('seckillItemDTO====seckillItemDTO', this.state.seckillItemDTO);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  /**
   * 查询活动规则
   */
  querySetting() {
    wxApi
      .request({
        url: api.seckill.querySetting,
      })
      .then((res) => {
        const content = res.data.content || '';
        const array = content.split('↵');
        let setions = [];
        array.forEach((item) => {
          if (item === '') {
            return;
          }
          setions = setions.concat(item.split('\n').filter((section) => section !== ''));
        });

        this.setState({
          rule: {
            title: res.data.title,
            setions: setions,
            posterType: res.data.posterType,
            posterContent: res.data.posterContent,
            posterLogo: res.data.posterLogo,
            useLogisticsCoupon: res.data.useLogisticsCoupon,
          },
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  /**
   * 打开和关闭活动规则
   */
  refChildsCMTP = (node) => (this.childsCMTP = node);
  showInstruction = () => {
    const { rule } = this.state;
    rule && rule.title && this.childsCMTP.show({ scale: true });
  };

  handleHideInstruction = () => {
    this.childsCMTP.hide();
  };
  /**
   * 分享弹窗
   */
  // refShareDialogCMTP = (node) => this.shareDialogCMTP = node;

  /**
   * 购买弹窗
   */
  refChooseAttributeCMPT = (node) => (this.chooseAttributeCMPT = node);

  /**
   * 底部购买按钮
   */
  handleBuyNow() {
    const _iosSetting = this.props.iosSetting
    if(_iosSetting?.payType && this.state.goodsDetail.wxItem.type == goodsTypeEnum.VIRTUAL_GOODS.value && _iosSetting.payType  === 3){
      this.iosPayLimitDialog.current.showIospayLimitDialog();
      return
    }
    if (this.state.goodsDetail && this.state.seckillItemDTO && this.state.goodsDetail.wxItem) {
      const orderRequestPromDTO: any = {}; // 订单请求参数
      const navigateToData: any = {}; // 跳转页面所需的参数
      // 根据当前状态，判断是否以秒杀的形式购买商品，是则传活动id进行请求
      if (this.state.currentStatus === this.state.seckillEnumStatus.ON_GOING.value) {
        orderRequestPromDTO.secKillNo = this.state.activityId;
        // 只展示的支付信息，若不传，付款页面则默认展示全部优惠信息
        const payDetails = [{ name: 'freight' }, { name: 'seckill' }];
        if (this.state.rule.useLogisticsCoupon) {
          payDetails.push({ name: 'coupon' });
        }

        navigateToData.payDetails = JSON.stringify(payDetails);

        navigateToData.params = JSON.stringify({
          orderRequestPromDTO,
        });

        navigateToData.useLogisticsCoupon = this.state.rule.useLogisticsCoupon;
      }
      this.setState({
        navigateToData,
      });

      const directUrl = ''; // 直接跳转的url

      // 判断是否有直接跳转的url，有则表示不需要选择商品属性，直接跳转对应页面
      if (directUrl) {
        wxApi.$navigateTo({
          url: directUrl,
          data: navigateToData,
        });
      } else {
        // 判断是进行中的活动才进行限购数量判断
        const peopleLimitAmount = this.state.goodsDetail.seckillItemDTO.peopleLimitAmount;
        if (this.state.currentStatus === this.state.seckillEnumStatus.ON_GOING.value && peopleLimitAmount) {
          this.setState({
            limitAmountTitle: '秒杀',
          });
        }

        // 显示选择商品属性对话弹框
        this.chooseAttributeCMPT && this.chooseAttributeCMPT.showAttributDialog();
        // const dialog = this.selectComponent('#choose-attribute-dialog')
        // if (dialog) {
        //   dialog.showAttributDialog()
        // }
      }
    }
  }

  /**
   * 选定秒杀商品
   * @param {*} info
   */
  handleChooseSeckill = (info) => {
    console.log('info==========handlerChooseSeckill', info);
    const goodsDetail = this.state;
    const detail = info; // 选择的商品详情
    const dialog = this.chooseAttributeCMPT;
    if (goodsDetail.seckillItemDTO && goodsDetail.seckillItemDTO.peopleLimitAmount) {
      const peopleLimitAmount = goodsDetail.seckillItemDTO.peopleLimitAmount;
      // 判断是进行中的活动才进行限购数量判断
      if (
        this.state.currentStatus === this.state.seckillEnumStatus.ON_GOING.value &&
        detail.itemCount > peopleLimitAmount
      ) {
        wxApi.showToast({
          title: `购买数量最多只能${peopleLimitAmount}`,
          icon: 'none',
        });

        return;
      }
    }
    !!dialog && dialog.hideAttributeDialog();

    const goodsInfoList = info; // 购买的商品信息列表

    if (goodsDetail.wxItem && goodsDetail.wxItem.type === goodsTypeEnum.SERVER.value) {
      // 判断是否为服务秒杀 服务类型为0
      wxApi.$navigateTo({
        url: '/sub-packages/server-package/pages/appointment/index',
        data: {
          goodsInfo: JSON.stringify(goodsInfoList),
        },
      });
    } else {
      // 购买的商品信息列表
      const goodsInfoList = [
        {
          pic: detail.pic,
          salePrice: detail.salePrice,
          itemCount: detail.itemCount,
          noNeedPay: detail.noNeedPay,
          name: detail.name,
          itemNo: detail.itemNo,
          barcode: detail.barcode,
          skuId: detail.skuId,
          skuTreeIds: detail.skuTreeIds,
          skuTreeNames: detail.skuTreeNames,
          wxItem: detail.wxItem,
          reportExt: JSON.stringify({
            sessionId: this.state.sessionId,
          }),

          seckillPrice: detail.wxItem.seckillPrice || detail.sku.seckillPrice,
          drugType: detail.wxItem.drugType,
          mpActivityName: this.state.seckillItemDTO ? this.state.seckillItemDTO.name : '',
        },
      ];

      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', goodsInfoList);
      if(detail.wxItem.type === goodsTypeEnum.VIRTUAL_GOODS.value){
        this.state.navigateToData.itemNo = detail.itemNo
        this.state.navigateToData.mpActivityName = this.state.seckillItemDTO ? this.state.seckillItemDTO.name : ''
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index',
          data:this.state.navigateToData,
        });
      }else{
        const {navigateToData} = this.state
        navigateToData.params = encodeURI(navigateToData.params)
        navigateToData.payDetails = encodeURI(navigateToData.payDetails)
        wxApi.$navigateTo({
          url: pageLinkEnum.orderPkg.payOrder,
          data: navigateToData,
        });
      }
    }
  };

  refShareDialogCMTP = React.createRef<any>();
  render() {
    const {
      error,
      goodsImages,
      tmpStyle,
      seckillItemDTO = {},
      goodsDetail,
      skuHighPrice,
      itemNo,
      industry,
      industryEnum,
      dataLoad,
      isGradualChange,
      goodType,
      limitAmountTitle,
      rule,
      uniqueShareInfoForQrCode,
      contactConfig,
    } = this.state;
    const {iosSetting} = this.props
    return (
      <View data-scoped='wk-psd-Detail' className='wk-psd-Detail'>
        {!!error && <TaskError onCallSomeFun={this.getGoodsDetail}></TaskError>}
        {!error && (
          <View className='goods-detail-container'>
            <Swiper className='swiper' indicatorDots={goodsImages.length > 1}>
              {goodsImages.map((item, index) => {
                return (
                  <Block key={index}>
                    <SwiperItem>
                      <Image className='goods-image' src={item}></Image>
                    </SwiperItem>
                  </Block>
                );
              })}
            </Swiper>
            {/*  活动商品基础信息  */}
            <View className='activity-info'>
              <View className='tab'>
                <View className='sale-info-box' style={_safe_style_('background:' + tmpStyle.bgGradualChange)}>
                  <Image className='icon-lightning' src={getStaticImgUrl.images.icon_Lightning_png}></Image>
                  <View className='sale-info'>
                    {!!seckillItemDTO.priceRange && <View className='desc-box'>限时秒杀</View>}
                  </View>
                  {/*  活动剩余时间  */}
                  {!!seckillItemDTO.remainTitle && (
                    <View className='remain-time-box'>
                      <View className='remain-time-title'>{seckillItemDTO.remainTitle + '还剩：'}</View>
                      <View className='remain-time'>
                        {!!seckillItemDTO.displayTime.day && (
                          <Text className='time-block'>{seckillItemDTO.displayTime.day}</Text>
                        )}

                        {!!seckillItemDTO.displayTime.day && <Text className='symbol'>天</Text>}
                        <Text className='time-block'>{seckillItemDTO.displayTime.hour}</Text>
                        <Text className='symbol'>:</Text>
                        <Text className='time-block'>{seckillItemDTO.displayTime.minute}</Text>
                        <Text className='symbol'>:</Text>
                        <Text className='time-block'>{seckillItemDTO.displayTime.second}</Text>
                      </View>
                    </View>
                  )}
                </View>
                {!!goodsDetail && (
                  <View className='goods-price'>
                    <View className='sale-price' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      <Text className='unit' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        ￥
                      </Text>
                      <Text>
                        {seckillItemDTO.priceRange ||
                          (goodsDetail.wxItem && filters.moneyFilter(goodsDetail.wxItem.salePrice, true))}
                      </Text>
                    </View>
                    {/*  原售价  */}
                    {seckillItemDTO.priceRange ? (
                      <View className='label-price'>
                        {'￥' +
                          (skuHighPrice ||
                            (goodsDetail.wxItem && filters.moneyFilter(goodsDetail.wxItem.salePrice, true)))}
                      </View>
                    ) : (
                      <View className='label-price'>
                        {'￥' + (goodsDetail.wxItem && filters.moneyFilter(goodsDetail.wxItem.labelPrice, true))}
                      </View>
                    )}
                  </View>
                )}

                {/*  商品详情  */}
                {!!(goodsDetail && goodsDetail.seckillItemDTO && goodsDetail.seckillItemDTO.name) && (
                  <View className='goods-info'>
                    <View className='goods-name limit-line line-5'>
                      {!!(!!goodsDetail && !!goodsDetail.wxItem && !!goodsDetail.wxItem.drugType) && (
                        <Text className='drug-tag' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                          处方药
                        </Text>
                      )}

                      {goodsDetail.seckillItemDTO.name}
                    </View>

                    <View className='count'>{'已售' + (goodsDetail.seckillItemDTO.salesVolume || 0) + '件'}</View>
                  </View>
                )}

                {/*  规则详情入口  */}
                <View className='tag-box rule-info' onClick={this.showInstruction}>
                  <View className='tag-item'>规则详情</View>
                  <View className='more-rule'>
                    <Image className='img-box' src={getStaticImgUrl.images.rightIcon_png}></Image>
                  </View>
                </View>
                {/* 定金模式 */}
                {!!(goodsDetail && goodsDetail.wxItem && goodsDetail.wxItem.frontMoneyItem) && (
                  <FrontMoneyItem
                    leftRightMargin={0}
                    leftRightPadding={30}
                    frontMoney={goodsDetail.wxItem.frontMoney}
                  ></FrontMoneyItem>
                )}
              </View>
              {/*  组合商品明细  */}
              {!!(
                goodsDetail &&
                goodsDetail.wxItem &&
                goodsDetail.wxItem.combinationDTOS &&
                goodsDetail.wxItem.combinationDTOS.length > 0
              ) && (
                <CombinationItemDetail
                  combinationDtos={goodsDetail.wxItem.combinationDTOS}
                  style={{ display: 'block', borderBottom: '20rpx solid rgba(241, 244, 249, 1)' }}
                ></CombinationItemDetail>
              )}

              {/*  商品评论  */}
              {!!(itemNo && industry !== industryEnum.type.beauty.value) && (
                <CommentModule
                  showHeader
                  isBrief
                  commentTitle={goodsDetail && goodsDetail.wxItem ? goodsDetail.wxItem.name : ''}
                  itemNo={itemNo}
                ></CommentModule>
              )}

              {/*  商品详情  */}
              {!!goodsDetail && !!goodsDetail.wxItem && !!goodsDetail.wxItem.describe && (
                <DetailParser itemDescribe={goodsDetail.wxItem.describe}></DetailParser>
              )}
            </View>
            {/*  底部购买按钮栏  */}
            {!!dataLoad && (
              <BuyNow
                style='z-index: 55;'
                disable={this.isDisableMethod()}
                operationText={this.operationTextMethod()}
                defaultText={this.defaultTextMethod()}
                isGradualChange={isGradualChange}
                goodsDetail={goodsDetail}
                onBuyNow={this.handleBuyNow.bind(this)}
                immediateShare={false}
                onShare={this.handleSelectChanel.bind(this)}
                showCart={false}
              ></BuyNow>
            )}
          </View>
        )}

        {/*  选择商品属性弹框  */}
        {!!goodsDetail && (
          <ChooseAttributeDialog
            ref={this.refChooseAttributeCMPT}
            goodsType={goodType}
            goodsDetail={goodsDetail}
            limitAmountTitle={limitAmountTitle}
            onChoose={this.handleChooseSeckill}
          ></ChooseAttributeDialog>
        )}

        {/*  规则详情  */}
        <AnimatDialog animClass='rule-dialog' ref={this.refChildsCMTP}>
          <ScrollView className='scroll-view_H' scrollY className='scroll'>
            <View className='rule'>
              <View className='title'>{rule.title}</View>
              {rule.setions.map((item, index) => {
                return (
                  <View className='block' key={index}>
                    {item}
                  </View>
                );
              })}
            </View>
          </ScrollView>
          <Image className='icon-close' src={commonImg.close} onClick={this.handleHideInstruction}></Image>
        </AnimatDialog>
        {/*  分享对话弹框  */}
        <ShareDialog
          childRef={this.refShareDialogCMTP}
          posterHeaderType={rule.posterType || 1}
          posterLabelPriceLabel={seckillItemDTO.priceRange ? '活动结束价' : '零售价'}
          posterTips={
            seckillItemDTO.priceRange
              ? rule.posterContent || '物美价廉的好货，赶紧来秒杀吧！'
              : '物美价廉的好货，赶紧来秒杀吧！'
          }
          posterLogo={rule.posterLogo}
          posterSalePriceLabel={seckillItemDTO.priceRange ? '秒杀价' : '秒杀价'}
          posterLabelPriceLabel={seckillItemDTO.priceRange ? '活动结束价' : '零售价'}
          posterName={seckillItemDTO.name || (goodsDetail && goodsDetail.wxItem && goodsDetail.wxItem.name)}
          posterImage={goodsDetail && goodsDetail.wxItem && goodsDetail.wxItem.thumbnail}
          salePrice={
            seckillItemDTO.priceRange ||
            (goodsDetail && goodsDetail.wxItem && filters.moneyFilter(goodsDetail.wxItem.salePrice, true))
          }
          labelPrice={
            seckillItemDTO.priceRange
              ? goodsDetail && goodsDetail.wxItem && filters.moneyFilter(goodsDetail.wxItem.salePrice, true)
              : goodsDetail && goodsDetail.wxItem && filters.moneyFilter(goodsDetail.wxItem.labelPrice, true)
          }
          onSave={this.handleSavePosterImage}
          uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
        ></ShareDialog>
        {/* 客服/导购悬浮窗 */}
        {!!contactConfig && !!contactConfig.config && !!contactConfig.config.showInGoods && (
          <DialogModule dataSource={contactConfig.config} />
        )}

        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='share-canvas'></Canvas>
        <AuthPuop></AuthPuop>
        <HomeActivityDialog showPage={'sub-packages/marketing-package/pages/seckill/detail/index?itemNo=' + itemNo}></HomeActivityDialog>
        <IosPayLimitDialog ref={this.iosPayLimitDialog} tipsCopywriting={iosSetting?.tipsCopywriting}></IosPayLimitDialog>
      </View>
    );
  }
}

SeckillDetail.enableShareAppMessage = true

export default SeckillDetail;
