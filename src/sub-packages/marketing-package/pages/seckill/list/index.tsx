import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi';
import login from '@/wxat-common/x-login/index.js';
import api from '@/wxat-common/api/index.js';
import constants from '@/wxat-common/constants/index.js';
import report from '@/sdks/buried/report/index';
import shareUtil from '@/wxat-common/utils/share.js';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import SeckillModule from '@/wxat-common/components/base/seckillModule/index';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import './index.scss';

const loadMoreStatus = constants.order.loadMoreStatus;

class SecKillList extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    tmpStyle: {}, // 主题模板配置
    activityList: [], // 活动列表
    error: false, // 是否展示错误页面
    pageNo: 1,
    hasMore: true, // 是否可以加载更多
    loadMoreStatus: loadMoreStatus.HIDE,
    showType: 0,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  isLoading = false
  /**
   * 生命周期函数--监听页面加载
   */
  UNSAFE_componentWillMount() {
    const params = this.$router.params;
    // 判断是否有秒杀子组件展示类型参数
    if (params.showType || params.showType === 0) {
      this.setState({
        showType: params.showType,
      });
    }
    this.getTemplateStyle(); // 获取模板配置
    this.setState(
      {
        pageNo: 1,
      },

      () => {
        this.getSeckillList(); // 获取秒杀活动列表
      }
    );
  }
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

    if (this.state.hasMore && this.isLoading) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatus.LOADING,
        },

        () => {
          this.getSeckillList(); // 获取秒杀活动列表
        }
      );
    }
  }


  /**
   * 加载更多失败，重新加载
   */
  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatus.LOADING,
      },

      () => {
        this.getSeckillList(); // 获取秒杀活动列表
      }
    );
  };

  onShareAppMessage() {
    report.share(true);
    const url = '/sub-packages/marketing-package/pages/seckill/list/index';
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.SECKILL_LIST,
      bzName: '秒杀列表',
    });

    console.log('sharePath => ', path);
    return {
      title: '',
      path,
      imageUrl: '',
    };
  }

  /**
   * 获取模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }

    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
   * 获取秒杀活动列表
   */
  getSeckillList() {
    this.setState(
      {
        error: false,
      },

      () => {
        login.login().then(() => {
          wxApi
            .request({
              url: api.seckill.getSeckillList,
              loading: true,
              data: {
                pageNo: this.state.pageNo,
                pageSize: 10,
              },
            })
            .then((res) => {
              let activityList = this.state.activityList; // 活动列表
              // 判断是否加载更多的请求，是则叠加数据，否则重新赋值新数据
              if (this.isLoadMoreRequest()) {
                activityList = activityList.concat(res.data || []);
              } else {
                activityList = res.data || [];
              }

              // 判断是否还有更多，是则可以下来加载更多，否则不允许加载更多
              if (activityList.length === res.totalCount) {
                this.setState({
                  hasMore: false,
                });
              } else {
                this.setState({
                  hasMore: true,
                });
              }

              this.setState({
                activityList: activityList,
                pageNo: this.state.pageNo + 1,
                loadMoreStatus: loadMoreStatus.HIDE,
              },()=>{
                this.isLoading = true
              });
            })
            .catch((error) => {
              if (this.isLoadMoreRequest()) {
                this.setState({
                  loadMoreStatus: loadMoreStatus.ERROR,
                });
              } else {
                this.setState({
                  error: true,
                });
              }
            });
        });
      }
    );
  }

  /**
   * 是否加载更多请求
   */
  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  /**
   * 跳转秒杀详情页面
   * FIXME: 这个回调可能没有被触发
   * @param {*} e
   */
  handleClick = (e) => {
    // FIXME: detail 可能不存在
    const item = e.detail.item;
    console.log('最外面item', item);
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/seckill/detail/index',
      data: {
        itemNo: item.itemNo || null,
        activityId: item.id || null,
        seckillStatus: item.status || 0,
      },
    });
  };

  render() {
    const { activityList, error, showType, loadMoreStatus } = this.state;

    console.log('showType', showType, activityList);
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-psl-List' className='wk-psl-List'>
        <View className='seckill-list'>
          {!!(!!activityList && activityList.length == 0) && <Empty message='敬请期待...'></Empty>}
          {!!error && <Error></Error>}
          {!!(!!activityList && !!activityList.length) && (
            <SeckillModule
              showType={Number(showType)}
              dataList={activityList}
              onClick={this.handleClick}
            ></SeckillModule>
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        </View>
        <AuthPuop></AuthPuop>
        <HomeActivityDialog showPage="sub-packages/marketing-package/pages/seckill/list/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default SecKillList;
