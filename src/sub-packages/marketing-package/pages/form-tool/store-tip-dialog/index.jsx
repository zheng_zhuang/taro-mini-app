import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, CoverView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc';
import template from '../../../../../wxat-common/utils/template.js';
import './index.scss';
import { connect } from 'react-redux';

const mapStateToProps = () => {
  // template.getTemplateStyle 依赖 state
  return {
    templ: template.getTemplateStyle(),
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class StoreTipDialog extends React.Component {
  static defaultProps = {
    templ: { btnColor: 'rgb(243, 95, 40)' },
  };

  state = {
    visible: false
  }

  showDialog = () => {
    this.setState({ visible: true });
  };

  hideDialog = () => {
    this.setState({ visible: false });
  };

  render() {
    const { visible } = this.state;
    const { templ } = this.props;

    return (
      !!visible && (
        <View data-scoped='wk-pfs-StoreTipDialog' className='wk-pfs-StoreTipDialog store-tip-dialog'>
          <CoverView className='cover-poper'></CoverView>
          <CoverView className='store-tip'>
            <CoverView className='tip'>如果需要更改服务门店，请前往首页重新选择门店。</CoverView>
            <CoverView className='border-line'></CoverView>
            <CoverView className='btn' onClick={this.hideDialog} style={_safe_style_('color: ' + templ.btnColor)}>
              知道了
            </CoverView>
          </CoverView>
        </View>
      )
    );
  }
}

export default StoreTipDialog;
