import {$getRouter} from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import {_safe_style_} from '@/wxat-common/utils/platform';
import {Button, Form, Image, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import {connect} from 'react-redux';
import hoc from '@/hoc';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';
import template from '@/wxat-common/utils/template';
import authHelper from '@/wxat-common/utils/auth-helper';
// import defaultAvatar from '../../images/form-tool/form-header.png';
// import shareImage from '../../images/form-tool/share.png';
import './index.scss';
import StoreTipDialog from './store-tip-dialog';
import utils from '../../../../wxat-common/utils/util';
import checkOptions from '../../../../wxat-common/utils/check-options';
import shareUtil from '../../../../wxat-common/utils/share';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import Empty from '@/wxat-common/components/empty/empty';
import FormItem from './form-item';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';

type FormToolProps = {
  currentStore: any;
  templ: any;
  environment: string;
  sessionId: string | number;
};

interface FormTool {
  props: FormToolProps;
}

const mapStateToProps = ({ globalData, base }) => {
  const { currentStore } = base;
  return {
    currentStore,
    environment: globalData.environment,
    templ: template.getTemplateStyle(),
    sessionId: base.sessionId,
  };
};

const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};

type FormBaseItemType = {
  type: 'comment' | 'text' | 'email' | 'address' | 'detail-address' | 'phone' | 'sex' | 'textarea';
  label: string;
  value: any;
  required: boolean;
  uuid: string;
};

type FormRadioItemType = {
  type: 'radio';
  options: any[];
} & Omit<FormBaseItemType, 'type'>;

type FormDateItemType = {
  type: 'date';
  dateRange: string[];
} & Omit<FormBaseItemType, 'type'>;

export type FormItemType = FormBaseItemType | FormRadioItemType | FormDateItemType;

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class FormTool extends React.Component {
  $router = $getRouter();
  static defaultProps = {
    templ: { btnColor: 'rgb(243, 95, 40)' },
  };

  state = {
    avatar: '',
    avatarHeight: 300,
    name: '预约表单',
    buttonName: '提交表单',
    hideAvatar: false,
    formBody: [] as FormItemType[],
    loaded: false,
    formId: null,
    activity: {
      activityType: 1,
      activityId: '',
    },
  };

  tipDialogCMPT: StoreTipDialog | null = null;

  async componentDidMount() {
    const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);

    if (!!formattedOptions) {
      this.init(formattedOptions);
    }
  }

  async componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      const formattedOptions = await checkOptions.checkOnLoadOptions(this.$router.params);
      if (!!formattedOptions) {
        this.init(formattedOptions);
      }
    }
  }

  componentWillUnmount() {
    this.refreshPrepageData();
  }

  onShareAppMessage = () => {
    const url = `sub-packages/marketing-package/pages/form-tool/index?formId=${this.state.formId}`;
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.FORM_TOOL,
      bzName: this.state.name || '预约表单',
      bzId: this.state.formId,
      sceneName: this.state.name,
    });

    return {
      title: this.state.name || '预约表单',
      path,
      imageUrl: "https://htrip-static.ctlife.tv/wk/share.png",
    };
  };

  init = (options) => {
    const formId = options.formId || +utils.getQueryString('?' + decodeURIComponent(options.scene), 'formId') || '';
    const id = options.id ? options.id : '';
    this.setState(
      {
        formId: formId,
        activity: {
          ...this.state.activity,
          activityId: id,
        },
      },

      () => {
        this.getConfig();
      }
    );
  };

  getConfig = async () => {
    try {
      const res = await wxApi.request({
        url: api.formTool.getConfig,
        loading: true,
        data: { formId: this.state.formId },
      });

      const formBody = res.data.formBody ? JSON.parse(res.data.formBody) : [];
      const newFormBody: any[] = [];

      formBody.forEach((item: any) => {
        if (!item.value) item.value = '';

        newFormBody.push(item);
        if (item.type === 'address') {
          newFormBody.push({ label: '详细地址', type: 'detail-address', require: false, value: '' });
        }
      });

      this.setState({
        name: res.data.name,
        buttonName: res.data.buttonName || '提交表单',
        hideAvatar: res.data.hideAvatar || false,
        avatar: res.data.avatar,
        avatarHeight: res.data.avatarHeight,
        formBody: newFormBody,
      });

      wxApi.setNavigationBarTitle({
        title: res.data.name || '预约表单',
      });
    } finally {
      this.setState({ loaded: true });
    }
  };

  checkForm = (dataList) => {
    const { currentStore = {} } = this.props;
    let error = '';
    if (!currentStore.id) {
      error = '服务门店不能为空';
    }
    dataList.forEach((item) => {
      if (error) return;
      if (item.required && !item.value) {
        error = item.label + '不能为空';
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });

    return error;
  };

  refreshPrepageData = () => {
    const pages = Taro.getCurrentPages();
    const beforePage = pages[pages.length - 2];
    if (wxApi.$getCurrentPageRoute(beforePage).indexOf('micro-decorate') > -1) {
      beforePage.getOrderDetail(); // 更新微装页面浏览、预约量
    }
  };

  handleSubmit = () => {
    if (!authHelper.checkAuth()) return;

    const { formBody, formId } = this.state;
    const { currentStore } = this.props;
    const error = this.checkForm(formBody);
    if (error) {
      wxApi.showToast({ icon: 'none', title: error });
      return;
    }
    //地址栏数据处理
    const list: any[] = [];
    formBody.forEach((item: any, index) => {
      let value = item.value || '';
      if (item.type === 'address') {
        const isNotEmptyArray = !!value && !!value.push;
        if (isNotEmptyArray) {
          if (value[0] === value[1]) {
            value = value.concat().splice(1).join('');
          } else {
            value = value.join('');
          }
        }
        value += formBody[index + 1].value || '';
      } else if (item.type === 'detail-address') {
        return;
      }
      list.push({
        uuid: item.uuid,
        value,
      });
    });
    wxApi
      .request({
        url: api.formTool.addSubmit,
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded',
        },

        loading: true,
        data: {
          formId: +formId!,
          storeId: +currentStore.id,
          submitFormBody: JSON.stringify(list),
        },
      })
      .then(() => {
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/form-tool/commit-success/index',
        });
      })
  };

  handleShowTip = () => {
    if (this.tipDialogCMPT) {
      this.tipDialogCMPT.showDialog();
    }
  };

  handleChange = ({ value, index }) => {
    const { formBody } = this.state;
    formBody[index].value = typeof value === 'string' ? value.trim() : value;
    this.setState({ formBody });
  };

  render() {
    const { avatar, avatarHeight, buttonName, hideAvatar, formBody, loaded } = this.state;

    const { environment } = this.props;

    const { currentStore, templ } = this.props;

    const $form = formBody.map((item, index) => {
      return (
        <View key={'form-item' + item.uuid} className='form-item'>
          <FormItem index={index} item={item} value={formBody[index].value} onChange={this.handleChange} />
        </View>
      );
    });

    let $content: JSX.Element | null = null;

    let $submitBtn: JSX.Element;
    if (environment !== 'wxwork') {
      $submitBtn = (
        <Button formType='submit' onClick={this.handleSubmit} style={_safe_style_('background: ' + templ.btnColor)}>
          {buttonName}
        </Button>
      );
    } else {
      $submitBtn = (
        <ButtonWithOpenType openType='share' style={_safe_style_('background: ' + templ.btnColor)}>
          分享
        </ButtonWithOpenType>
      );
    }

    if (formBody.length && loaded) {
      $content = (
        <View className='form-tool'>
          {!hideAvatar && (
            <Image
              mode='aspectFill'
              style={{ height: `${Taro.pxTransform(+avatarHeight || 300)}` }}
              src={avatar || "https://htrip-static.ctlife.tv/wk/form-header.png"}
              className='form-header'
            ></Image>
          )}

          <Form>
            <View className='form-body'>
              <View className='white-box'>{$form}</View>
            </View>
            {!!formBody.length && (
              <View className='form-footer'>
                <View className='current-store' onClick={this.handleShowTip}>
                  <Image className='store-logo' src='https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/store.png'></Image>
                  <View className='store-title'>服务门店：</View>
                  <View className='store-name'>{currentStore.abbreviation || currentStore.name}</View>
                </View>
                <Image src='https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/bg.png' className='form-footer-bg'></Image>
                <View className='form-submit'>{$submitBtn}</View>
              </View>
            )}
          </Form>
        </View>
      );
    } else if (loaded) {
      $content = <Empty message='表单不存在，或者已被删除' />;
    }

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-mpf-FormTool' className='wk-mpf-FormTool'>
        {$content}
        <StoreTipDialog ref={(node) => (this.tipDialogCMPT = node)} />
        <AuthPuop></AuthPuop>
        <HomeActivityDialog showPage={'sub-packages/marketing-package/pages/form-tool/index?formId=' + this.state.formId}></HomeActivityDialog>
      </View>
    );
  }
}

export default FormTool;
