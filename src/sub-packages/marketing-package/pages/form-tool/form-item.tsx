import React from 'react'; // @scoped
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Text, Image, Input, RadioGroup, Radio, Label, Picker, Textarea } from '@tarojs/components';
import Taro from '@tarojs/taro';
import classnames from 'classnames';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import getStaticImgUrl from '../../../../wxat-common/constants/frontEndImgUrl'
import RegionPicker from '@/sub-packages/scene-package/components/region-picker/index'
import './form-item.scss';

interface FormBaseItemType {
  type: 'comment' | 'text' | 'email' | 'address' | 'detail-address' | 'phone' | 'sex' | 'textarea';
  label: string;
  value: any;
  required: boolean;
  uuid: string;
}

type FormRadioItemType = {
  type: 'radio';
  options: any[];
} & Omit<FormBaseItemType, 'type'>;

type FormDateItemType = {
  type: 'date';
  dateRange: string[];
} & Omit<FormBaseItemType, 'type'>;

export type FormItemType = FormBaseItemType | FormRadioItemType | FormDateItemType;

export interface FormItemProps {
  item: FormItemType;
  index: number;
  value: any;
  onChange: Function;
}

const FormItem = ({ item, index, value, onChange: handleChange }: FormItemProps) => {
  const templ = useTemplateStyle();

  if (!item) return null;

  const $label =
    item.type !== 'comment' ? (
      <View className='form-item-name'>
        <Text>{item.label}</Text>
        {!!item.required && <Text style={_safe_style_('color: #f56c6c; padding-left: 5rpx')}>*</Text>}
      </View>
    ) : null;

  let $content: JSX.Element | null = null;
  if (item.type === 'sex') {
    $content = (
      <View className='form-item-content form-sex'>
        <View
          onClick={() => handleChange({ value: '男', index: index })}
          className={classnames('form-sex__item boy', { selected: item.value === '男' })}
        >
          <Image className='form-sex__image' mode='aspectFit' src={getStaticImgUrl.subPackages.male_png}></Image>
          <View>男</View>
        </View>
        <View
          onClick={() => handleChange({ value: '女', index: index })}
          className={classnames('form-sex__item girl', { selected: item.value === '女' })}
        >
          <Image
            className='form-sex__image'
            mode='aspectFit'
            src={getStaticImgUrl.subPackages.female_png}
          ></Image>
          <View>女</View>
        </View>
      </View>
    );
  } else if (item.type === 'text' || item.type === 'email') {
    $content = (
      <Input
        className='form-item-content form-input'
        placeholder='请输入'
        value={item.value}
        onInput={({ target }) => handleChange({ value: (target as any).value, index })}
      ></Input>
    );
  } else if (item.type === 'radio') {
    $content = (
      <RadioGroup
        className='form-item-content form-radio'
        name={'radio-group' + item.uuid}
        onChange={({ detail }) => handleChange({ value: (detail as any).value, index })}
        key={'radio-group' + item.uuid}
      >
        {item.options.map((optionItem, index) => {
          return (
            <Label key={'radio-item-' + item.uuid + index} for={'radio-item-' + item.uuid + index}>
              <Radio value={optionItem.label} color={templ.bgColor}></Radio>
              {optionItem.label}
            </Label>
          );
        })}
      </RadioGroup>
    );
  } else if (item.type === 'address') {
    $content = (
      <RegionPicker
        className="phone-input"
        options={{index:index}}
        onChange={(value) => handleChange({ value, index })}>
        {item.value ? (
          <Text className="picker">{item.value}</Text>
        ) : (
          <Text className="picker" style={_safe_style_('color: #888;')}>
            请选择
          </Text>
        )}

        <Image
          className="picker-icon"
          src={getStaticImgUrl.images.rightIcon_png}
        ></Image>
      </RegionPicker>
    );
  } else if (item.type === 'detail-address') {
    $content = (
      <Textarea
        className='form-item-content form-detail-address'
        disableDefaultPadding
        autoHeight
        placeholder='请输入详细地址'
        maxlength={100}
        onInput={({ target }) => handleChange({ value: (target as any).value, index })}
      ></Textarea>
    );
  } else if (item.type === 'comment') {
    $content = <Text className='comment'>{item.value}</Text>;
  } else if (item.type === 'date') {
    $content = (
      <Picker
        value={value}
        className='form-item-content form-date'
        mode='date'
        start={item.dateRange ? item.dateRange[0] : ''}
        end={item.dateRange ? item.dateRange[1] : ''}
        onChange={({ detail }) => handleChange({ value: (detail as any).value, index })}
      >
        {item.value ? (
          <Text className='picker'>{item.value}</Text>
        ) : (
          <Text style={_safe_style_('color: #888;')}>请选择</Text>
        )}

        <Image className='picker-icon' src={getStaticImgUrl.images.rightIcon_png}></Image>
      </Picker>
    );
  } else if (item.type === 'textarea') {
    $content = (
      <View className='form-textarea'>
        <Textarea
          className='form-item-content'
          maxlength={-1}
          placeholder='请输入留言内容'
          onInput={({ target }) => handleChange({ value: (target as any).value, index })}
        ></Textarea>
      </View>
    );
  } else if (item.type === 'phone') {
    $content = (
      <Input
        className='form-item-content form-input'
        placeholder='请输入'
        type='number'
        onInput={({ target }) => handleChange({ value: (target as any).value, index })}
      ></Input>
    );
  }

  return (
    <View
      data-scoped='wk-smpf-FormItem'
      data-fixme='01 className existed'
      className={'wk-smpf-FormItem ' + classnames([`${item.type}-item`])}
      key={'wk-smpf-FormItem' + item.uuid}
    >
      {$label}
      {$content}
    </View>
  );
};

export default FormItem;
