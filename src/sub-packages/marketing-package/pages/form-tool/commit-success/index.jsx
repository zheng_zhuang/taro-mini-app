import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc';
import template from '@/wxat-common/utils/template';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';
import { connect } from 'react-redux';

const mapStateToProps = ({ globalData }) => {
  return {
    tabbars: (globalData || {}).tabbars,
    templ: template.getTemplateStyle(),
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class CommitSuccess extends React.Component {
  goHome = () => {
    const { tabbars } = this.props;
    let url = '/wxat-common/pages/home/index';
    if (tabbars && tabbars.list) {
      url = tabbars.list[0].pagePath;
    }
    wxApi.$navigateTo({
      url: url,
    });
  };

  render() {
    const { templ } = this.props;
    const successImage = cdnResConfig.pay_success.pay_success;
    return (
      <View data-scoped='wk-pfc-CommitSuccess' className='wk-pfc-CommitSuccess wrapper'>
        <View className='commit-success'>
          <View className='commit-area'>
            <Image className='commit-img' mode='aspectFit' src={successImage}></Image>
            <Text className='commit-title'>信息提交成功</Text>
          </View>
          <View className='btn-group'>
            <Button className='back' onClick={this.goHome} style={_safe_style_('background: ' + templ.btnColor)}>
              返回首页
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

export default CommitSuccess;
