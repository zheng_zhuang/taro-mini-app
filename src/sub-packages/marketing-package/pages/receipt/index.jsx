import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, WebView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import wxApi from '@/wxat-common/utils/wxApi';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import report from '@/sdks/buried/report/index';

@hoc
class Receipt extends React.Component {
  $router = $getRouter();
  state = {
    receiptURL: '',
  };

  reportList = []; // 埋点事件集合
  pageEnterTime = null; // 进入页面时间
  pageLeaveTime = null; // 离开页面时间

  componentDidMount() {
    const options = this.$router.params;
    const receiptURL = protectedMailBox.read('receiptURL');
    // 获取埋点事件集合
    const reportList = options && options.reportList ? JSON.parse(options.reportList) : [];
    this.reportList = reportList;
    this.setState({
      receiptURL: receiptURL,
    });

    const title = options.title;
    if (title) {
      wxApi.setNavigationBarTitle({
        title: title || '申请开票',
      });
    }
  }

  /**
   * 生命周期函数--监听页面显示
   */
  componentDidShow() {
    const reportList = this.reportList || [];
    reportList.forEach((item) => {
      // 判断是否存在埋点事件名称
      if (item.event && item.event == 'pageviewOut') {
        // 设置进入页面时间
        this.pageEnterTime = new Date().getTime();
      }
    });
  }

  /**
   * 生命周期函数--监听页面隐藏
   */
  componentDidHide() {
    const reportList = this.state.reportList || [];
    reportList.forEach((item) => {
      // 判断是否存在埋点事件名称
      if (item.event && item.event == 'pageviewOut') {
        this.pageviewOut(item); // 用户退出页面埋点事件
      }
    });
  }

  /**
   * 生命周期函数--监听页面卸载
   */
  componentWillUnmount() {
    const reportList = this.reportList || [];
    reportList.forEach((item) => {
      // 判断是否存在埋点事件名称
      if (item.event && item.event == 'pageviewOut') {
        this.pageviewOut(item); // 用户退出页面埋点事件
      }
    });
  }

  /**
   * 用户退出页面埋点事件
   * @param item 埋点事件对象
   */
  pageviewOut(item) {
    let params = {};
    // 判断是否存在埋点事件参数
    if (item.params) {
      params = {
        ...item.params,
      };
    }

    // 设置离开页面时间
    this.pageLeaveTime = new Date().getTime();

    // 计算页面停留时长(毫秒)
    params.stay_dur = null;
    if (this.pageEnterTime && this.pageLeaveTime) {
      params.stay_dur = this.pageLeaveTime - this.pageEnterTime;
    }

    // 清除空数据
    Object.keys(params).forEach((key) => {
      if (!params[key] || params[key] === '' || params[key].length < 1) {
        delete params[key];
      }
    });
    report.pageviewOut(params);
  }

  render() {
    const { receiptURL } = this.state;
    return (
      <View>
        <WebView src={receiptURL}></WebView>
      </View>
    );
  }
}

export default Receipt;
