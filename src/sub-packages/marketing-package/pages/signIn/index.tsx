import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';

import hoc from '@/hoc';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import utilDate from '@/wxat-common/utils/date.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import PosterRecommendModule from '@/wxat-common/components/decorate/poster-recommendation/index';
// import AuthUser from '@/wxat-common/components/auth-user';
import Rewards from './components/rewards/index';
import Remedy from './components/remedy/index';
import RuleDialog from './components/rule/index';
import Calendar from '@/wxat-common/components/wxCalendar';
import './index.scss';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
const btnStyle =
  'padding: 0;height: 164rpx;width: 164rpx;font-size:57rpx;background:linear-gradient(180deg,rgba(255,148,34,1) 0%,rgba(255,199,64,1) 100%);box-shadow:0px 3px 6px rgba(246,181,6,1);text-align: center;line-height: 164rpx;border-radius: 100%;';
const date = new Date();
let month = date.getMonth() + 1 + '';
let day = date.getDate() + '';
if (month.length === 1) {
  month = '0' + month;
}
if (day.length === 1) {
  day = '0' + day;
}
const today = date.getFullYear() + month + day;
const DefaultFirstPeriodDay = new Date();
DefaultFirstPeriodDay.setDate(date.getDate() - date.getDay() + 1);
DefaultFirstPeriodDay.setHours(0, 0, 0, 0);
const formatTime = (date) => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  return [year, month, day].map(formatNumber).join('/');
};

const formatNumber = (n) => {
  n = n.toString();
  return n[1] ? n : '0' + n;
};

@hoc
class SignInPage extends React.Component {
  $router = $getRouter();
  state = {
    currentYear: date.getFullYear(),
    currentMonth: date.getMonth() + 1,
    interactionDates: {
      '2019-8-12': true,
      '2019-8-11': true,
      '2019-8-10': true,
      '2019-8-9': true,
    },

    cdnResConfig: cdnResConfig.signIn,
    isShowRule: false,
    isShowRemedy: false,
    isShowRewards: false,
    btnStyle,
    // 当前选择的日期，以及日期的str格式
    selectDate: date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate(),
    selectDateStr: date,
    recommendConfig: {
      showPoster: false,
      limitNum: 4,
      background: '#FFA800',
      display: 'oneRowTwo',
    },

    calendarState: 'init', // 日历状态
    today: today,
    currentDate: today,
    signBtnText: '签到',
    // 当前日期时间戳
    currentTimestamp: new Date(),
    isSigned: false,
    // 签到活动id
    activityId: null,
    /**
     * 补签消耗积分
     */
    complementExpend: 0,
    /**
     * 用于展示的规则
     */
    signedRule: '',
    /**
     * 记录用户当前积分信息
     */
    userScore: 0,
    /**
     * 签到周期(0:week|1:month)
     */
    signedCycle: -1,
    /**
     * 日签奖励积分数
     */
    dailyAwardPoints: 0,
    signedDateList: [],
    // 连签天数
    cycleSignedCount: 0,
    /**
     * 周期补签上限
     */
    complementUpperLimit: 0,
    // 待展示优惠券列表
    couponList: [],
    // 周期内能补签的第一天
    firstPeriodDay: formatTime(DefaultFirstPeriodDay),
    // 优惠券
    couponData: {},
    // 通知是否叠加
    overlayDisplayNotify: 0,
    // 规则修改说明
    modifyRuleDesc: '',
    // 后台设置的规则
    ruleDesc: '',
  };

  refCalendarCMPT: Taro.RefObject<any> = React.createRef();

  componentDidMount() {
    Taro.eventCenter.on('signIn-changeSelectedDate', (cur_date, timestamp) => {
      this.changeSelectedDate(cur_date, timestamp);
    });

    const { id } = this.$router.params;
    if (id) {
      this.setState(
        {
          activityId: id,
        },

        () => {
          this.getSignInDetail();
          this.getSignInList();
        }
      );
    }

    // if (this.refCalendarCMPT.current) {
    //   this.calendar = this.refCalendarCMPT.current
    // }

    let years: number[] = [];
    for (let i = 2000; i <= date.getFullYear(); i++) {
      years.push(i);
    }
    this.setState({
      years,
      currentYearIndex: years.length - 1,
    });

    // this.getCalendarStateByYear();
  }

  componentWillUnmount() {
    Taro.eventCenter.off('signIn-changeSelectedDate');
  }

  handlePrevMonth(e) {
    // console.log(e,'sssss')
    this.refCalendarCMPT.current && this.refCalendarCMPT.current.handleCalendar(e);
  }
  handleNextMonth(e) {
    this.refCalendarCMPT.current && this.refCalendarCMPT.current.handleCalendar(e);
  }

  handleToToday() {
    this.refCalendarCMPT.current && this.refCalendarCMPT.current.setNowDate();
    // this.calendar = this.selectComponent("#calendar");
  }

  showRule = () => {
    this.setState({
      isShowRule: true,
      signedRule: this.state.ruleDesc,
    });
  };

  checkIn = () => {
    this.setState({
      isShowRemedy: true,
    });
  };
  hideRule = () => {
    this.setState({
      isShowRule: false,
    });
  };

  hideRemedy = () => {
    this.setState({
      isShowRemedy: false,
    });
  };
  hideRewards = () => {
    this.setState({
      isShowRewards: false,
    });

    if (this.state.couponList.length) {
      setTimeout(() => {
        let couponList = [...this.state.couponList];
        couponList.shift();
        this.setState({
          isShowRewards: true,
          couponData: this.state.couponList[0],
          couponList,
        });
      }, 200);
    }
  };

  getSignInDetail = () => {
    wxApi
      .request({
        url: api.signIn.getSignInDetail,
        loading: true,
        data: {
          id: this.state.activityId,
        },
      })
      .then((res) => {
        const data = res.data;

        if (data) {
          let firstPeriodDay = new Date();
          data.signedCycle === 0
            ? firstPeriodDay.setDate(date.getDate() - date.getDay() + 1)
            : firstPeriodDay.setDate(1);
          firstPeriodDay.setHours(0, 0, 0, 0);
          this.setState(
            {
              complementExpend: data.complementExpend,
              signedRule: data.signedRule,
              userScore: data.userScore || 0,
              firstPeriodDay: formatTime(firstPeriodDay),
              signedCycle: data.signedCycle,
              dailyAwardPoints: data.dailyAwardPoints,
              cycleSignedCount: data.cycleSignedCount || 0,
              complementUpperLimit: data.complementUpperLimit || 0,
              overlayDisplayNotify: data.overlayDisplayNotify || 0,
              ruleDesc: data.signedRule,
              modifyRuleDesc: data.modifyRuleDesc || '',
            },

            () => {
              if (!data.notifyUser) return false;
              const signedRule = this.state.overlayDisplayNotify
                ? this.state.modifyRuleDesc + '\n' + this.state.ruleDesc
                : this.state.modifyRuleDesc;
              this.setState({
                isShowRule: true,
                signedRule,
              });
            }
          );
        }
      })
      .catch((err) => {
        console.log('getSignInDetail error', err);
      });
  };
  clickSignBtn = () => {
    if (this.state.currentDate == this.state.today) {
      this.actionSignIn();
    } else {
      this.setState({
        isShowRemedy: true,
      });
    }
  };
  getSignInList = () => {
    if (!authHelper.checkAuth(false)) return false;
    wxApi
      .request({
        url: api.signIn.getSignInList,
        data: {
          activityId: this.state.activityId,
          pageSize: 100,
        },
      })
      .then((res) => {
        const data = res.data || [];
        const signedDateList = data.map((item) => {
          return item.signedDate;
        });
        const isSigned = !!~signedDateList.findIndex((item) => this.state.currentDate == item);
        this.setState({
          signedDateList,
          isSigned,
        });
      })
      .catch((err) => {
        console.log('getSignInList error', err);
      });
  };
  changeSelectedDate = (cur_date: string, timestamp: Date) => {
    const isSigned = !!~this.state.signedDateList.findIndex((item) => cur_date == item);
    console.log(timestamp);
    console.log(this.state.firstPeriodDay);
    this.setState({
      currentDate: cur_date,
      currentTimestamp: timestamp,
      isSigned,
    });
  };
  remedySignIn = () => {
    this.setState({
      isShowRemedy: false,
    });

    this.actionSignIn();
  };
  actionSignIn = () => {
    if (!authHelper.checkAuth()) return false;
    const params = {
      activityId: this.state.activityId,
      signedDate: this.state.currentDate,
    };

    let url = api.signIn.actionSignIn + '?';
    Object.keys(params).forEach((item) => {
      url += `${item}=${params[item]}&`;
    });
    url = url.slice(0, -1);
    wxApi
      .request({
        url,
        method: 'post',
        loading: true,
        data: params,
      })
      .then((res) => {
        let data = res.data;
        this.getSignInDetail();
        this.getSignInList();
        data.forEach((item) => {
          if (item.todayGiftType === 0) {
            wxApi.showToast({
              title: '获得积分' + item.todayPoints,
              icon: 'none',
            });
          } else {
            this.getGiftList(item.todayGiftId);
          }
        });
      });
  };

  goRecord = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/signIn/signIn-record/index',
      data: {
        id: this.state.activityId,
      },
    });
  };
  getGiftList = (couponInfoId) => {
    wxApi
      .request({
        url: api.signIn.getCoupon,
        data: {
          couponInfoId,
        },
      })
      .then((res) => {
        if (res.data === null) return false
        let data = res.data
        let endTime = new Date();
        if (data.endTime) {
          endTime = new Date(data.endTime);
        } else {
          endTime.setDate(endTime.getDate() + data.fixedTerm);
        }
        let couponData = {
          // 名字
          name: data.name,
          // 满多少
          minimumFee: data.minimumFee,
          // 减多少
          discountFee: data.discountFee,
          // 描述
          description: data.description || '',
          endTime: utilDate.format(endTime, 'yyyy-MM-dd'),
        };

        this.setState(
          {
            couponList: [...this.state.couponList, couponData],
          },

          () => {
            if (!this.state.isShowRewards) {
              let couponList = [...this.state.couponList];
              couponList.shift();
              this.setState({
                isShowRewards: true,
                couponData: this.state.couponList[0],
                couponList,
              });
            }
          }
        );
      });
  };

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {
  //   return {
  //     title: '学生托管教师端',
  //     path: `/pages/Index/Index`
  //   }
  // },

  render() {
    const {
      cdnResConfig,
      userScore,
      currentDate,
      today,
      isSigned,
      currentTimestamp,
      firstPeriodDay,
      complementUpperLimit,
      complementExpend,
      cycleSignedCount,
      signedDateList,
      interactionDates,
      signedRule,
      isShowRule,
      isShowRemedy,
      couponData,
      isShowRewards,
      recommendConfig,
    } = this.state;

    const showSigned =
      currentDate == today
        ? !isSigned
        : !isSigned &&
          new Date(currentTimestamp).getTime() >= new Date(firstPeriodDay).getTime() &&
          complementUpperLimit > 0 &&
          userScore >= complementExpend;

    const signBtnStyle = {
      backgroundImage: `url( ${cdnResConfig.sign_btn})`,
    };

    return (
      <View data-scoped='wk-mps-SignIn' className='wk-mps-SignIn container'>
        <Image src={cdnResConfig.main_bg} className='main-bg'></Image>
        <View className='body'>
          <View className='header'>
            <View className='my-integral'>{'我的积分：' + userScore}</View>
            <View className='rule-btn' onClick={this.showRule}>
              签到规则
            </View>
          </View>
          <View className='sign-wrapper'>
            {!!showSigned ? (
              <View className='sign-btn' style={_safe_style_(signBtnStyle)} onClick={this.clickSignBtn}>
                <Image src={cdnResConfig.sign_btn_icon} className='sign-btn-icon'></Image>
                <Text className='sign-btn__text'>{currentDate == today ? '签到' : '补签'}</Text>
              </View>
            ) : isSigned ? (
              <View className='sign-tip'>今日已签到</View>
            ) : (
              <View className='sign-tip'>该日未签到</View>
            )}
          </View>
          <View className='sign-day'>
            <Text className='sign-day__text'>{'你已经连续签到' + cycleSignedCount + '天'}</Text>
          </View>
          <View className='calendar-wrapper'>
            <Image className='calendar-bg' src={cdnResConfig.calendar_bg}></Image>
            {/*  <View class="encourage">再连续签到4天，额外赠送：xx积分</View>  */}
            <View className='calendar-content'>
              <Calendar
                firstPeriodDay={firstPeriodDay}
                onChangeSelectedDate={this.changeSelectedDate}
                signedDateList={signedDateList}
                ref={this.refCalendarCMPT}
                dataTags={interactionDates}
              ></Calendar>
            </View>
          </View>
          <View className='record-btn' onClick={this.goRecord}>
            签到记录 &gt;
          </View>
          {!!isShowRule && <RuleDialog onClose={this.hideRule} signedRule={signedRule}></RuleDialog>}
          {!!isShowRemedy && (
            <Remedy
              complementUpperLimit={complementUpperLimit}
              onConfirm={this.remedySignIn}
              complementExpend={complementExpend}
              onClose={this.hideRemedy}
            ></Remedy>
          )}

          {!!isShowRewards && <Rewards couponData={couponData} onClose={this.hideRewards}></Rewards>}
          <View className='recommend-title'>今日推荐</View>
          <View className='recommend-wrapper'>
            <PosterRecommendModule viewShow={true} dataSource={recommendConfig}></PosterRecommendModule>
          </View>
        </View>
        <HomeActivityDialog showPage="sub-packages/marketing-package/pages/signIn/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default SignInPage;
