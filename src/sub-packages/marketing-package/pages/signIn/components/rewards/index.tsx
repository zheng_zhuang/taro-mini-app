import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
// sub-packages/marketing-package/pages/signIn/components/rewards/index.js

import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import './index.scss';

const _cdnResConfig = cdnResConfig.signIn;
const commonImg = cdnResConfig.common;

type IProps = {
  type?: string;
  couponData: Record<string, any>;
  onClose: () => void;
};

let Rewards: FC<IProps> = ({ couponData, onClose, type }) => {
  return (
    <View data-scoped='wk-scr-Rewards' className='wk-scr-Rewards mask'>
      <View className='container'>
        <View className='title'>签到奖励</View>
        <View className='tip'>奖励已发放，可前往个人中心查看</View>
        {type === 'integral' ? (
          <Image className='icon-integral' src={_cdnResConfig.rewards_integral}></Image>
        ) : (
          <View className='coupon'>
            <Image className='coupon-bg' src={_cdnResConfig.coupon_bg}></Image>
            <View className='coupon-desc'>
              <Text className='coupon-desc__name'>{couponData.name}</Text>
              <Text className='coupon-desc__scope'>{couponData.description}</Text>
              <Text className='coupon-desc__time'>{'有效期：' + couponData.endTime}</Text>
            </View>
            <View className='coupon-line'></View>
            <View className='coupon-money'>
              <Text className='coupon-money__sale'>{'￥' + filters.moneyFilter(couponData.discountFee, true)}</Text>
              {couponData.minimumFee ? (
                <Text className='coupon-money__condition'>
                  {'满' + filters.moneyFilter(couponData.minimumFee, true) + '可用'}
                </Text>
              ) : (
                <Text className='coupon-money__condition'>无门槛</Text>
              )}
            </View>
          </View>
        )}

        <View className='ok-btn' onClick={onClose}>
          知道了
        </View>
        <Image className='close-btn' src={commonImg.close} onClick={onClose}></Image>
      </View>
    </View>
  );
};

Rewards.defaultProps = {
  type: '',
  couponData: {},
};

export default Rewards;
