import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const commonImg = cdnResConfig.common;

type IProps = {
  signedRule: string;
  onClose: () => void;
};

let Rule: FC<IProps> = ({ signedRule, onClose }) => {
  return (
    <View data-scoped='wk-scr-Rule' className='wk-scr-Rule mask'>
      <View className='wk-scr-Rule__container'>
        <View className='wk-scr-Rule__title'>签到规则</View>
        <View className='wk-scr-Rule__content'>{signedRule}</View>
        <Image className='wk-scr-Rule__close-btn' src={commonImg.close} onClick={onClose}></Image>
      </View>
    </View>
  );
};

Rule.defaultProps = {
  signedRule: '',
};

export default Rule;
