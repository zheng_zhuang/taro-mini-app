import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import './index.scss';

const _cdnResConfig = cdnResConfig.signIn;
const commonImg = cdnResConfig.common;

type IProps = {
  complementExpend: string | number;
  complementUpperLimit: number;
  onClose: () => void;
  onConfirm: () => void;
};

let Remedy: FC<IProps> = ({ complementExpend, complementUpperLimit, onClose, onConfirm }) => {
  return (
    <View data-scoped='wk-scr-Remedy' className='wk-scr-Remedy mask'>
      <View className='container'>
        <View className='title'>{'补签' + (complementExpend > 0 ? '支付' : '')}</View>
        <View className='pay-tip' style={_safe_style_('visibility: ' + (complementExpend > 0 ? 'visible' : 'hidden'))}>
          {'补签需支付' + complementExpend + '积分'}
        </View>
        <Image className='icon-calendar' src={_cdnResConfig.calendar}></Image>
        <View className='time-tip'>{'本月还有' + complementUpperLimit + '次补签机会'}</View>
        <View className='btn-wrapper'>
          <View className='cancel-btn btn' onClick={onClose}>
            取消
          </View>
          <View className='onConfirm-btn btn' onClick={onConfirm}>
            确定
          </View>
        </View>
        <Image className='close-btn' src={commonImg.close} onClick={onClose}></Image>
      </View>
    </View>
  );
};

export default Remedy;
