import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { Block, View, Text } from '@tarojs/components';
import Taro, { Config } from '@tarojs/taro';

import hoc from '@/hoc';
import date from './formatDate.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';

@hoc
class SignInRecordPage extends React.Component {
  $router = $getRouter();
  state = {
    record: [],
    activityId: null,
    giftType: ['日签积分', '优惠券', '连签积分'],
  };

  componentDidMount() {
    const { id } = this.$router.params;
    if (id) {
      this.setState({
        activityId: id,
      });

      this.getSignInList(id);
    }
  }

  formatDate(date) {
    date += '';
    let year = date.slice(0, 4);
    let mouth = date.slice(4, 6);
    let day = date.slice(6, 8);
    return year + '-' + mouth + '-' + day;
  }

  getSignInList(activityId: string) {
    wxApi
      .request({
        url: api.signIn.getSignInList,
        loading: true,
        data: {
          activityId,
          pageSize: 100,
        },
      })
      .then((res) => {
        const data = res.data || [];
        this.setState({
          record: data,
        });
      })
      .catch((err) => {
        console.log('getSignInList error', err);
      });
  }

  render() {
    const { record, giftType } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-pss-SignInRecord'
        className='wk-pss-SignInRecord'
      >
        {!!record && record.length == 0 ? (
          <Empty message='暂无签到记录'></Empty>
        ) : (
          <View className='lucky-record'>
            <View className='list'>
              {record.map((item, index) => {
                return (
                  <View className='list-item' key={item.id}>
                    <View className='gift'>{(item.recordPoint > 0 ? '+' : '') + item.recordPoint}</View>
                    <View className='list-item__content'>
                      <Text className='list-item__content-name'>{giftType[item.todayGiftType]}</Text>
                      <Text className='list-item__content-time'>{date.formatDate(item.signedDate)}</Text>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default SignInRecordPage;
