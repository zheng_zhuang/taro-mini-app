export default {
  navigationBarTextStyle: 'black',
  enablePullDownRefresh: false,
  navigationBarTitleText: '签到记录',
  backgroundTextStyle: 'light',
};
