function formatDate(date) {
  date += '';
  var year = date.slice(0, 4);
  var mouth = date.slice(4, 6);
  var day = date.slice(6, 8);
  return year + '-' + mouth + '-' + day;
}

export default {
  formatDate,
};
