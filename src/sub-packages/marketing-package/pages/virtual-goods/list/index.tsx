import { $getRouter, _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, Button, Image, Text, View, ScrollView } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi/index';
import constants from '@/wxat-common/constants/index.js';
import api from '@/wxat-common/api/index.js';
import template from '@/wxat-common/utils/template.js';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import { updateBaseAction } from '@/redux/base';
import { updateGlobalDataAction } from '@/redux/global-data';
import { connect } from 'react-redux';

const loadMoreStatus = constants.order.loadMoreStatus;
const app = Taro.getApp();
const CARDSTATUS = [0, 1, 2]; // 0：未开始 1：进行中 2：已售罄 3：已结束 4：已删除

interface PageStateProps {
  phoneInfo: any;
  iosSetting:any;
  isIOS:boolean
}

type IProps = PageStateProps
interface List {
  props: IProps;
}

const mapStateToProps = (state) => {
  return {
    phoneInfo: state.globalData.tabbars,
    iosSetting:state.globalData.iosSetting,
    isIOS:state.globalData.isIOS
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })

class List extends React.Component {
  $router = $getRouter();
  state = {
    tmpStyle: {}, // 主题模板配置
    cardStyle: 'title',
    categroyId: '',
    cardList: [],
    pageNo: 1,
    pageSize: 6,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    dialogShow: false,
    isFullScreen: !!this.props.phoneInfo && this.props.phoneInfo.isFullScreenDevice,
  };

  getList() {
    this.setState({ loadMoreStatus: loadMoreStatus.LOADING });
    let params = {
      type: 41,
      status: 1,
      pageNo: this.state.pageNo,
      pageSize: this.state.pageSize,
    };

    CARDSTATUS.forEach((item, i) => {
      let key = `statusList[${i}]`;
      params[key] = item;
    });
    return wxApi
      .request({
        url: api.virtualGoods.list,
        data: params,
        loading: true,
      })
      .then((res) => {
        let { data } = res || {};
        const xData = res.data || [];
        this.setState({
          loadMoreStatus: loadMoreStatus.HIDE,
          cardList: this.state.cardList.concat(xData),
          hasMore: data.length === 6,
        });
      })
      .catch(() => {
        this.setState({
          loadMoreStatus: loadMoreStatus.ERROR,
        });
      });
  }
  /**
   * 获取模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();

    Taro.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: templateStyle.titleColor,
    });

    this.setState({
      tmpStyle: templateStyle,
    });
  }
  jump(e) {
    const itemNo = e.currentTarget.dataset.itemno;
    wxApi.$navigateTo({
      url: `/sub-packages/marketing-package/pages/virtual-goods/detail/index?itemNo=${itemNo}`,
    });
  }

  componentDidShow() {
    const { pageNo, pageSize, cardList } = this.state;
    this.setState({
      pageNo: 1,
      pageSize: (cardList || []).length || pageSize,
      cardList: [],
    });

    this.getList().then(() => {
      this.setState({ pageNo, pageSize });
    });
  }

  componentDidMount() {
    const options = this.$router.params;
    let title = (options || {}).title || '代金卡包';
    let categroy = (options || {}).categroy || '';
    const cardStyle = (options || {}).cardStyle || 'title';
    this.getTemplateStyle();
    Taro.setNavigationBarTitle({
      title: title,
    });

    this.setState({
      categoryId: categroy,
      cardStyle,
    });
  }

  onReachBottom() {
    if (this.state.loadMoreStatus !== loadMoreStatus.LOADING && this.state.hasMore) {
      this.setState(
        {
          pageNo: ++this.state.pageNo,
        },

        () => {
          this.getList();
        }
      );
    }
  }
  onRetryLoadMore() {
    this.onReachBottom();
  }
  render() {
    const { isFullScreen, tmpStyle, cardList, hasMore, loadMoreStatus } = this.state;
    const {isIOS,iosSetting} = this.props
    return (
      <ScrollView
        scrollY
        data-fixme="02 block to view. need more test" data-scoped="wk-mpvl-List" className="wk-mpvl-List">
        {cardList.length > 0 && (
          <View className={'graph-list ' + (isFullScreen ? 'fix-full-screen' : '')}>
            {cardList &&
              cardList.map((item, index) => {
                return (
                  <Block key={index} data-item={item}>
                    <View
                      className="classic-list"
                      onClick={_fixme_with_dataset_(this.jump, { id: item.id, itemno: item.itemNo })}
                    >
                      <View className="cards-img-box">
                        <Image mode="aspectFill" src={item.thumbnail}></Image>
                      </View>
                      <View className="card-detail">
                        <View className="more-ellipsis title-wraper">
                          <Text className="title">{item.name}</Text>
                        </View>
                        {item.privilgeCardName && (
                          <View className="more-ellipsis title-wraper">
                            <Text className="title">{item.privilgeCardName}</Text>
                          </View>
                        )}

                        <View>
                          <View>
                            <View className="price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                              <Text className="price-symbol">￥</Text>
                              {filters.moneyFilter(item.salePrice, true)}
                            </View>
                          </View>
                        </View>
                        <Button className="btn-red" style={_safe_style_(tmpStyle.btnColor)}>
                          {isIOS && iosSetting && iosSetting.buttonCopywriting ? iosSetting.buttonCopywriting:'立即购买'}
                        </Button>
                      </View>
                    </View>
                  </Block>
                );
              })}
          </View>
        )}

        {cardList.length === 0 && !hasMore && <Empty message="敬请期待..."></Empty>}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}/>
      </ScrollView>
    );
  }
}

export default List;
