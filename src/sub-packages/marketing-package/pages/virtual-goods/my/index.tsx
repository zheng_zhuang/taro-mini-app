import { _safe_style_ } from 'wk-taro-platform';
import {  Navigator, View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi/index';
import api from '@/wxat-common/api/index.js';
import utilDate from '@/wxat-common/utils/date.js';

import './index.scss';


class My extends React.Component {

  state = {
    list: [],
    pageNo: 1,
    loading: false,
    end: false
  }


  componentDidMount(): void {
    this.addItems();
  }

   addItems = async () => {
    if (this.state.loading || this.state.end) return;
    this.setState({
      loading: true
    });
    let { data, totalCount } = await wxApi.request({
      url: api.virtualGoods.getMyCard,
      data: {
        pageSize: 10,
        pageNo: this.state.pageNo,
      },
    });
    this.setState({
      loading: false
    })
    data = data || [];
    data.forEach((item) => {
      item.createTime = utilDate.format(new Date(item.createTime), 'yyyy-MM-dd hh:mm:ss');
    });
    this.setState({
      list: this.state.list.concat(data),
    });
    this.setState({
      pageNo :this.state.pageNo++
    })
    if (this.state.list.length >= totalCount) {
      this.setState({
        end: true,
      });
    }
  }

  onReachBottom() {
    this.addItems();
  }

  render() {
    const { list, end } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpvm-My" className="wk-mpvm-My">
        {list &&
          list.map((item, index) => {
            return (
              <Navigator
                className="item"
                key={index}
                url={'/sub-packages/order-package/pages/order-details/index?orderNo=' + item.orderNo}
              >
                <View className="image" style={_safe_style_("background-image:url('" + item.thumbnail + "')")}></View>
                <View style={_safe_style_('font-size: 4vw;font-weight: 500;')}>{item.itemName}</View>
                {item.itemAttribute && (
                  <View style={_safe_style_('color:#999;font-size: 3vw;')}>{item.itemAttribute}</View>
                )}

                <View style={_safe_style_('color: #999;font-size: 3vw;color: red;')}>{'¥' + item.payFee / 100}</View>
                <View style={_safe_style_('color: #999;font-size: 3vw;')}>{item.createTime}</View>
              </Navigator>
            );
          })}
        {!end && <View style={_safe_style_('text-align: center;color: #999;font-size: 3vw;')}>加载中</View>}
      </View>
    );
  }
}

export default My;
