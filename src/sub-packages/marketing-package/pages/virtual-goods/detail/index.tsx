import { $getRouter, _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Swiper, SwiperItem, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import industryEnum from '@/wxat-common/constants/industryEnum.js';
import wxApi from '@/wxat-common/utils/wxApi/index';
import api from '@/wxat-common/api/index.js';
// import state from '../../../../../state';
import report from '@/sdks/buried/report/index.js';
import template from '@/wxat-common/utils/template.js';
import checkOptions from '@/wxat-common/utils/check-options.js';
import BuyNow from '@/wxat-common/components/buy-now/index';
import DetailParser from '@/wxat-common/components/detail-parser/index';
import Error from '@/wxat-common/components/error/error';
import './index.scss';
import { connect } from 'react-redux';
import updateCurrentStore from '@/wxat-common/x-login/store';
import { notifyStoreDecorateSwitch } from '@/wxat-common/x-login/decorate-configuration-store';
import ShareDialog from "@/wxat-common/components/share-dialog";
import shareUtil from '@/wxat-common/utils/share';
import scanH5 from '@/wxat-common/utils/scanH5'
import iosPayTypeEnum from "@/wxat-common/constants/iosPayTypeEnum";
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'
const app = Taro.getApp();

interface StateProps {
  templ: any;
  industry: string;
  sessionId: string;
  currentStore: any;
  environment: any;
  appInfo: any;
  iosSetting:any;
  isIOS:boolean
}

type IProps = StateProps;

const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
    iosSetting:state.globalData.iosSetting,
    isIOS:state.globalData.isIOS
  };
};

interface Detail {
  props: IProps;
}

console.log('***************');
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Detail extends React.Component {
  $router = $getRouter();
  state =  {
    tmpStyle: {}, // 主题模板配置
    industryEnum, // 行业类型常量
    industry: this.props.industry, // 行业类型
    itemNo: null, // 商品单号
    goodsImages: [], // 商品图片
    goodsDetail: null, // 活动商品详情
    dataLoad: false,
    sessionId: null,
    error: false,
    isExpand: false,
    extraData: null,
    _optionsData: null,
    _storeId: null,
    refShareDialogCMTP: React.createRef<any>(),
    isTicket: false, // 是否是预售券
    ticketTime: '' // 适用时间
  };
  iosPayLimitDialog = React.createRef<any>()
  onShareAppMessage = (e) => {
    const { itemNo, goodsDetail, goodsImages } = this.state;
    const imageUrl = goodsImages.length ? goodsImages[0] : '';
    const url = 'sub-packages/marketing-package/pages/virtual-goods/detail/index?itemNo=' + itemNo;
    const title = goodsDetail.wxItem.name;
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.GOODS_DETAIL,
      bzName: `产品${title ? '-' + title : ''}`,
    });
    console.log('sharePath => ', path);
    return { title, path, imageUrl };
  };
  componentDidCatch(error, info){
    console.log('***************',error, info);
  }

  async componentDidMount() {
    const options = this.$router.params;
    if (!!options && !!options.sk) options.scene = `scene=sk%3D${options.sk}`;
    const formattedOptions = await checkOptions.checkOnLoadOptions(options);
    if (process.env.WX_OA === 'true' && this.$router.params.scanId) {
      // h5扫码进入
      await scanH5.scanFn(this.$router.params.scanId, this.$router.params.tp).then((res) => {
        if (res) {
          this.setState({
            itemNo: res.data.verificationNo
          }, () => {
            this.init({
              itemNo: res.data.verificationNo,
            })
          })
          if (res.data.storeId && res.data.storeId != 0 && res.data.isOpen) {
            scanH5.changeStore(res.data.storeId)
          }
        }
      })
    } else if (formattedOptions) {
      this.init(formattedOptions);
    }
  }

  async componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      const options = this.$router.params;
      if (!!options && !!options.sk) options.scene = `scene=sk%3D${options.sk}`;
      const formattedOptions = await checkOptions.checkOnLoadOptions(options);
      if (process.env.WX_OA === 'true' && this.$router.params.scanId) {
        // h5扫码进入
        await scanH5.scanFn(this.$router.params.scanId, this.$router.params.tp).then((res) => {
          if (res) {
            this.setState({
              itemNo: res.data.verificationNo
            }, () => {
              this.init({
                itemNo: res.data.verificationNo,
              })
            })
            if (res.data.storeId && res.data.storeId != 0 && res.data.isOpen) {
              scanH5.changeStore(res.data.storeId)
            }
          }
        })
      } else if (formattedOptions) {
        this.init(formattedOptions);
      }
    }
  }


  operationText = () => {
    if (this.state.goodsDetail && this.state.goodsDetail.wxItem) {
      let text = '';
      // 0 '未开始', 1 '进行中', 2 '已售罄', 3 '已结束', 4 '已删除'
      switch (this.state.goodsDetail.wxItem.pocketStatus) {
        case 0:
          text = '活动未开始';
          break;
        case 1:
          text = '购买';
          break;
        case 2:
          text = '已售罄';
          break;
        case 3:
          text = '活动已结束';
          break;
        case 4:
          text = '活动已结束';
          break;
        default:
          text = '已售罄';
      }

      return text;
    }
    return '已售罄';
  }

  apiError = () => {
    this.setState({
      error: true,
    });
  };

  previewImg = (e) =>{
    const currentUrl = e.currentTarget.dataset.currenturl;
    const previewUrls = this.state.goodsImages;
    Taro.previewImage({
      current: currentUrl,
      urls: previewUrls,
    });
  };

   init = async (options) => {
    let params = options;
    // 推广大使短链分享
    if (options.scene) {
      params = decodeURIComponent(options.scene)
        .split('&')
        .map((kv) => {
          const [key, val] = kv.split('=');
          return { [key]: val };
        })
        .reduce((pre, cur) => ({ ...pre, ...cur }), {});
    }

    // 普通跳转、转发、推广大使
    // 虚拟商品扫码进入的参数No为itemNo
    if (params.itemNo || params.id || params.No) {
      this.setState({
        _storeId: params.refStoreId || params._ref_storeId || null,
        itemNo: params.itemNo || params.id || params.No || null,
        distributorId: options.distributorId,
      });
    } else if (options.scene) {
      const [itemNo, distributorId] = options.scene.split('_');
      this.setState({
        distributorId,
        itemNo,
      });
    }

    // 推广大使卡包强制切换门店
    const storeId = this.state._storeId;
    if (params.activityId && storeId && this.props.currentStore.id != storeId) {
      await this.onChangeStore(storeId);
    }

    this.setState({
      sessionId: report.getBrowseItemSessionId(this.state.itemNo),
      industry: this.props.industry,
    });

    if (this.props.sessionId) {
      this.initRenderData(); // 初始化渲染的数据
    }

  };

  initRenderData() {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({
      dataLoad: true,
    });

    this.getTemplateStyle(); // 获取主题模板配置
    this.getGoodsDetail(); // 获取商品详情
  };

  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  onChangeStore(storeId) {
    return wxApi
      .request({
        url: api.store.choose_new + '?storeId=' + storeId,
        loading: true,
      })
      .then((res) => {
        const store = res.data;
        if (store) {
          notifyStoreDecorateSwitch(store);
        }
      });
  }

  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel() {
    if (this.state.refShareDialogCMTP.current) {
      this.state.refShareDialogCMTP.current.show();
    }
  }

  getGoodsDetail() {
    wxApi
      .request({
        url: api.virtualGoods.detail,
        data: {
          itemNo: this.state.itemNo,
        },
      })
      .then(async (res) => {
        const cardpackDetail = res.data.wxItem;
        if (res.data.roomTicketDTO) {
          const { data = [] } = await wxApi.request({
            url: api.virtualGoods.ticketHotel,
            data: {
              itemNo: this.state.itemNo
            },
          })
          this.setState({
            isTicket: true,
            storeList: data,
            ticketTime: [res.data.roomTicketDTO.effectDateBegin, res.data.roomTicketDTO.effectDateEnd]
          })
        }
        if (cardpackDetail) {
          // 拼接正常商品详情数据
          const wxItem = {
            type: 0,
            itemNo: cardpackDetail.itemNo,
            buyLimit: cardpackDetail.buyLimit,
            cardExplain: cardpackDetail.cardExplain,
            salePrice: cardpackDetail.salePrice,
            name: cardpackDetail.name,
            subName: cardpackDetail.subName,
            cardSalesVolume: cardpackDetail.cardSalesVolume,
            isShelf: cardpackDetail.isShelf,
            provNum: cardpackDetail.provNum,
            purchasedNum: cardpackDetail.purchasedNum,
            thumbnail: cardpackDetail.thumbnail,
            pocketStatus: cardpackDetail.pocketStatus, // 0 未开始 1 进行中 2 已售罄 3 已结束 4 已删除
            labelPrice: cardpackDetail.labelPrice
          };

          const goodsDetail = {
            wxItem,
            outPrivilgeInput: cardpackDetail.outPrivilgeInput,
            itemNo: cardpackDetail.itemNo,
            itemType: cardpackDetail.type,
            channel: cardpackDetail.channel,
          };
          this.setState({
            // itemDescribe: cardpackDetail.itemDescribe,
            itemDescribe: cardpackDetail.describe,
            // goodsImages: [cardpackDetail.thumbnail],
            goodsImages: res.data.materialUrls,
            goodsDetail,
          });
        } else {
          this.apiError();
        }
      })
      .catch(() => {
        this.apiError();
      });
  }

  handleBuyNow =  async () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index',
      data: {
        itemNo: this.state.goodsDetail.itemNo,
        distributorId: this.state.distributorId,
      },
    });
  }

  handleIosBuyNow=()=>{
    if (!this.IosPayControl()) {
      return;
    }
    wxApi.$navigateTo({
      url:"/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index",
      data:{
        itemNo:this.state.goodsDetail.itemNo,
        distributorId:this.state.distributorId,
        iosPayNodes: this.props.iosSetting ? encodeURIComponent(this.props.iosSetting.text) : '',
        jumpType: this.props.iosSetting ? this.props.iosSetting.jumpType: '1'
      }
    })
  }
  // 根据后台设置配置支付
  IosPayControl=()=>{
    if (this.props.iosSetting.payType == iosPayTypeEnum.payType.JUST_LIKE_ANDROID.value) {
      this.handleBuyNow()
      return false
    } else if (this.props.iosSetting.payType == iosPayTypeEnum.payType.H5_PAY.value) {
      return true
    } else if (this.props.iosSetting.payType == iosPayTypeEnum.payType.IOS_PAY_NONSUPPORT.value) {
      // 做弹窗操作
      this.showIospayLimitDialog();
      return false
    }
  }

  showIospayLimitDialog = () =>{
    this.iosPayLimitDialog.current.showIospayLimitDialog();
  }

  // 跳转适用门店
  toTicketStoreList = () => {
    wxApi.$navigateTo({
      url:"/sub-packages/marketing-package/pages/store-list/index",
      data:{
        itemNo:this.state.goodsDetail.itemNo
      }
    })
  }

  render() {
    const { error,
      goodsImages,
      tmpStyle,
      goodsDetail,
      itemDescribe,
      dataLoad,
      isTicket,
      storeList,
      ticketTime
    } = this.state;
    const {iosSetting,isIOS} = this.props
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpvd-Detail" className="wk-mpvd-Detail">
        {!!error && <Error></Error>}
        {!error && (
          <View className="cards-detail-container">
            <Swiper indicatorDots={goodsImages.length > 1} className='swiper'>
              {goodsImages &&
                goodsImages.map((item, index) => {
                  return (
                    <Block key={index}>
                      <SwiperItem>
                        {item.length > 0 && (
                          <View
                            className="cards-image"
                            style={_safe_style_(
                              'background: transparent url(' + item + ') no-repeat 50% 50%;background-size: cover;'
                            )}
                            onClick={_fixme_with_dataset_(this.previewImg, { currenturl: item })}
                          ></View>
                        )}
                      </SwiperItem>
                    </Block>
                  );
                })}
            </Swiper>
            <View className="sale-info">
              <View className="sale-price-info">
                <View className="price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  <Text className="price-symbol">￥</Text>
                  { goodsDetail && filters.moneyFilter(goodsDetail.wxItem.salePrice, true) }
                </View>
                {
                  goodsDetail && goodsDetail.wxItem.labelPrice && (
                    <View className="label-price">
                      <Text>￥</Text>
                      {goodsDetail && filters.moneyFilter(goodsDetail.wxItem.labelPrice, true)}
                    </View>
                  )
                }
              </View>
              <View className="sale-order-info">
                <Text className="title">{goodsDetail && goodsDetail.wxItem.name}</Text>
              </View>
              <View className="sale-order-info">
                <Text className="title">{goodsDetail && goodsDetail.wxItem.privilgeCardName}</Text>
              </View>
            </View>
            {/* {适用门店} */}
            {
              isTicket && storeList.length > 0 &&
                <View className="suit-hotel">
                  <View className="detail-header">
                    <Text className="header-label"></Text>
                    <Text>适用门店</Text>
                    <Text className="suit-count" onClick={this.toTicketStoreList}>{storeList.length}家门店适用 ></Text>
                  </View>
                  <View className="store-show">
                    {storeList[0].name}
                    <View>{storeList[0].address}</View>
                  </View>
                </View>
            }
            {
              isTicket && storeList.length > 0 && (
                <View className="suit-hotel">
                  <View className="detail-header">
                    <Text className="header-label"></Text>
                    <Text>适用时间</Text>
                  </View>
                  {
                    ticketTime && ticketTime.length > 0 && <View className="store-show">
                      {filters.dateFormat(ticketTime[0], 'yyyy-MM-dd') + " ~ " + filters.dateFormat(ticketTime[1], 'yyyy-MM-dd')}
                    </View>
                  }
                </View>
              )
            }
            { itemDescribe && <DetailParser headerLabel="商品描述" itemDescribe={itemDescribe}></DetailParser> }
          </View>
        )}

        {dataLoad && (
          <Block>
            {
              !isIOS ? <BuyNow
              style="z-index: 55;"
              showShare
              operationText="立即购买"
              defaultText="立即购买"
              onBuyNow={this.handleBuyNow}
              immediateShare={false}
              onShare={this.handleSelectChanel.bind(this)}
              showCart={false}
            ></BuyNow> :
            <BuyNow style="z-index: 55;" showShare operationText={iosSetting && iosSetting.buttonCopywriting ? iosSetting.buttonCopywriting:'立即购买'} defaultText={iosSetting && iosSetting.buttonCopywriting?iosSetting.buttonCopywriting:'立即购买'} onBuyNow={this.handleIosBuyNow} immediateShare={false}  onShare={this.handleSelectChanel.bind(this)}
            showCart={false} />
            }

          </Block>
        )}
        <IosPayLimitDialog tipsCopywriting={iosSetting?.tipsCopywriting} ref={this.iosPayLimitDialog}></IosPayLimitDialog>
        {/*  分享对话弹框  */}
        <ShareDialog
          childRef={this.state.refShareDialogCMTP}
          qrCodeParams={null}
          showLabelPrice
          posterTips="向您推荐了这个商品"
        ></ShareDialog>
      </View>
    );
  }
}

Detail.enableShareAppMessage = true

export default Detail;
