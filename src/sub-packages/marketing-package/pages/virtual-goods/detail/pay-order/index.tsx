import { $getRouter, _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Image, Input, Text, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi/index';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import util from '@/wxat-common/utils/util.js';
import couponEnum from '@/wxat-common/constants/couponEnum.js';
import constants from '@/wxat-common/constants/index.js';
import Empty from '@/wxat-common/components/empty/empty';
import Error from '@/wxat-common/components/error/error';
import BottomDialog from '@/wxat-common/components/bottom-dialog/index';
import pay from '@/wxat-common/utils/pay';
import store from '@/store/index';
import './index.scss';
import { connect } from 'react-redux';
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'

const PAY_CHANNEL = constants.order.payChannel;
const rules = {
  mobile: [
    {
      required: true,
      message: '请输入手机号码',
    },

    {
      reg: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
      message: '手机号码无效',
    },
  ],
};
interface StateProps {
  templ: any;
  industry: string;
  sessionId: string;
  currentStore: Record<string, any>;
  environment: any;
  appInfo: any;
  promotion: any;
  shareId: any;
  targetType: any;
  vip: any;
  isFirst: any;
  taskType: any;
  phoneInfo:any;
  iosSetting:any;
  isIOS:boolean;
  seckillNo: string|number;
}

type IProps = StateProps;


const mapStateToProps = (state) => {
  return {
    templ: template.getTemplateStyle(),
    industry: state.globalData.industry,
    sessionId: state.base.sessionId,
    currentStore: state.base.currentStore,
    environment: state.globalData.environment,
    appInfo: state.base.appInfo,
    promotion: state.base.promotion,
    shareId: state.base.shareId,
    targetType: state.base.targetType,
    vip: state.base.vip,
    isFirst: state.base.isFirst,
    taskType: state.base.taskType,
    phoneInfo:state.globalData.phoneInfo,
    iosSetting:state.globalData.iosSetting,
    isIOS:state.globalData.isIOS
  };
};

interface PayOrder {
  props: IProps;
}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class PayOrder extends React.Component {
  $router = $getRouter();
  iosPayLimitDialog = React.createRef<any>()
  state = {
    couponEnum,
    distributorId: null,
    tmpStyle: {},
    form: {
      contact: '',
      mobile: '',
    },

    formValidated: {
      isValied: false,
      msg: ''
    },

    goodsDetail: null,
    coupons: null,
    curCoupon: {
      code: ''
    },
    isCoupon: 1,
    isGiftActivity: false,
    price: null,
    itemNo: null,
    error: false,
    orderMessage: null,
    itemCount: 1,
    currentStore: {
      id: '',
      name: ''
    },
    payChannel: PAY_CHANNEL.WX_PAY,
    PAY_CHANNEL,
    denyRepeatPay: false, // 拒绝重复支付
    payAble: true, // 是否计算价格
    isNull: false,
    payDetails: [
      {
        name: 'gift-card',
      },

      {
        name: 'coupon',
      },

      {
        name: 'redpacke',
      },

      {
        name: 'discount',
      },

      {
        name: 'freight',
      },
      ],
    // 需显示的 优惠或活动  暂时只显示优惠卷 可参考普通商品
    iosPayNodes: '',
    isLock:false,
    jumpType: '',
    num: 1,
    seckillNo: '',
    bargainNo: '',
    skuId: '',
    mpActivityName: '',
    minPeople: '',
    activityId: '',
    groupNo: ''
  };

  // 支付按钮是否置灰
  isError(formValidated, error, payAble, goodsDetail) {
    return (
      !formValidated.isValied || error || !payAble || goodsDetail?.wxItem.isShelf === 0
    );
  }

  componentDidShow() {
    this.getGoodsDetail();
    this.validate();
  }

  componentDidMount() {
      const option = this.$router.params;
      let isGiftActivity = false;
      let activityId = null; // 砍价
      let groupNo = null;  //
      let seckillNo = null;  // 秒杀
      let minPeople = null;
      let mpActivityName = '';
      let bargainNo = null;
      if (option.mpActivityName) {
        mpActivityName = option.mpActivityName;
      }

      if (option.payDetails) {
        const payDetails = JSON.parse(option.payDetails);
        isGiftActivity = !!payDetails.find((item) => {
          return item.name === 'giftActivity';
        });
        this.setState({
          payDetails: payDetails,
        });
      }
      if (option.params) {
        const params = JSON.parse(option.params);

        // 获取上一个页面传入的订单参数
        if (params.orderRequestPromDTO) {
          activityId = params.orderRequestPromDTO.activityId || null;
          groupNo = params.orderRequestPromDTO.groupNo || null;
          seckillNo = params.orderRequestPromDTO.secKillNo || null;
          minPeople = params.orderRequestPromDTO.minPeople || null;
          bargainNo = params.orderRequestPromDTO.bargainNo || null;
        }
      }
      if (this.props.isIOS) {
          // this.setState({
          //   iosPayNodes: this.props.iosSetting.text
          // })
        wxApi.setNavigationBarTitle({
          title: '订单确认'
        });
      }
      this.setState({
        jumpType: option.jumpType
      })
      const form = {
        mobile:option.mobile ? option.mobile : null
      }
      this.setState(
        {
          itemNo: option.itemNo,
          form: form,
          distributorId: option.distributorId ? option.distributorId : null,
          currentStore: this.props.currentStore,
          isGiftActivity: isGiftActivity,
          activityId: activityId,
          groupNo: groupNo,
          seckillNo: seckillNo,
          minPeople: minPeople,
          mpActivityName,
          bargainNo,
        },
        () => {
          this.getTemplateStyle()
          this.getGoodsDetail()
          this.validate()
        }
      )
  }

  handleIosPay=async()=>{
    // 0元支付调取原有逻辑
    if(this.state.price.needPayPrice==0 || (this.props.iosSetting && this.props.iosSetting.payType == 1)){
      this.handlePay();
      return
    }
    const that = this
    if(that.state.isLock){
      return
    }
    if (this.props.iosSetting && this.props.iosSetting.payType==3) {
      this.showIospayLimitDialog();
      return
    }
    await that.validate();
    if (!that.state.formValidated.isValied) {
      return that.errorTip(that.state.formValidated.msg);
    }
    if(that.state.isError){
      return
    }

    const parameter = {
      storeId: that.props.currentStore.id,
      storeName: that.props.currentStore.name,
      'orderRequestPromDTO.secKillNo': that.state.seckillNo,
      'orderRequestPromDTO.bargainNo': that.state.bargainNo,
      expressType: 0,
      itemCount: this.state.itemCount,
      itemDTOList: [
        {
          itemNo: that.state.itemNo,
          itemCount:that.state.itemCount,
          skuId: that.state.skuId || '',
          mpActivityName: that.state.mpActivityName || ''
        },
      ],
    };
    that.setState({
      isLock:true
    })
    const { data } = await wxApi.request({
      url: api.virtualGoods.unifiedVirtualOrder,
      method: 'POST',
      data: {
        ...util.formatParams(parameter),
        orderRequestPromDTO:{
          couponNo: that.state.curCoupon.code,
          minPeople: that.state.minPeople,
          activityId: that.state.activityId,
          secKillNo: that.state.seckillNo,
          bargainNo: that.state.bargainNo,
          groupNo: that.state.groupNo,
        },
        virtualItemOrderVO: {
          phone: that.state.form.mobile,
          username: that.state.form.contact
        },
        itemNo: that.state.itemNo,
        payTradeType: 4,
        distributorId: that.state.distributorId, // 分享人id
        orderMessage: that.state.orderMessage
      },
      loading: true,
    })
    if(!data.prepayId){
      if(this.state.price.needPayPrice==0){
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/pay-success/index',
          data:{
            costPrice:this.state.price.needPayPrice,
            from: this.state.goodsDetail.roomTicketDTO ? 'ticket' : '',
            orderNo: data.orderNo
          }
        },setTimeout(() => {
          that.setState({isLock:false})
        }, 1500))
        return
      }
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/payH5-submit-success/index',
        data:{
          nodes: encodeURIComponent(this.props.iosSetting.text),
          H5PayUrl: encodeURIComponent(data.payUrl),
          jumpType: this.props.iosSetting ? this.props.iosSetting.jumpType: '1'
        }
      },setTimeout(() => {
        that.setState({isLock:false})
      }, 1500))
      return
    }
    data.timeStamp = data.timeStamp + ''
    data.package = 'prepay_id=' + data.prepayId
    data.success = (res) => {
      console.log('~~~~~',res);
    }
    data.fail = ({ errMsg }) => {
      if (errMsg !== 'requestPayment:fail cancel') {
        wxApi.showToast({ title: errMsg, })
      }else {
        // wxApi.$redirectTo({
        //   url: 'sub-packages/moveFile-package/pages/order-list/index'
        // },setTimeout(() => {
          that.setState({isLock:false})
        // }, 1500))
      }
    }
    // wx.requestPayment(data)
  }

  getGoodsDetail() {
    if(!this.state.itemNo) return
    wxApi
      .request({
        url: api.virtualGoods.detail,
        data: {
          itemNo: this.state.itemNo,
        },

        loading: true,
      })
      .then((res) => {
        if (res.data.wxItem) {
          this.setState({
            goodsDetail: res.data,
          });
          // 房券最小预定数默认值
          if (res.data.roomTicketDTO && res.data.roomTicketDTO.minNum) {
            this.setState({
              itemCount: res.data.roomTicketDTO.minNum
            })
          }
          this.fetchPreferentialList();
        } else {
          this.setState({
            isNull: true,
          });

          // this.apiError()
        }
      })
      .catch((error) => {
        this.apiError();
      });
  }

// 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });

    if (templateStyle.titleColor) {
      Taro.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  validate() {
    for (const key in rules) {
      const value = this.state.form[key];
      const rule = rules[key];

      let isValied = true;
      let msg = '';
      for (const r of rule) {
        if (r.required && !value) {
          isValied = false;
          msg = r.message;
          break;
        } else if (r.reg && !r.reg.test(value)) {
          isValied = false;
          msg = r.message;
          break;
        }
      }
      if (!isValied) {
        const formValidated = this.state.formValidated;
        formValidated.isValied = false;
        formValidated.msg = msg;
        this.setState({ formValidated });
        return false;
      }
    }
    const formValidated = this.state.formValidated;
    formValidated.isValied = true;
    formValidated.msg = '验证通过';
    this.setState({ formValidated });
    return true;
  }

  messageInput = (e) => {
    this.setState({
      orderMessage: e.detail.value,
    });
  }

  mobileInput =(e) => {
    const form = this.state.form;
    form.mobile =e.detail.value.trim();
    this.setState({ form });
    this.validate();
  }

  contactInput =(e) => {
    const form = this.state.form;
    form.contact =e.detail.value.trim();
    this.setState({ form });
    this.validate();
  }

  // 确认支付
  handlePay = async () =>  {
    this.validate();
    if (!this.state.formValidated.isValied) {
      return this.errorTip(this.state.formValidated.msg);
    }
    if (this.state.isError) {
      return;
    }

    const parameter = {
      storeId: this.state.currentStore.id,
      payTradeType: pay.returnPayTradeType(),
      storeName: this.state.currentStore.name,
      'orderRequestPromDTO.secKillNo': this.state.seckillNo,
      'orderRequestPromDTO.bargainNo': this.state.bargainNo,
      expressType: 0,
      itemDTOList: [
        {
          itemNo: this.state.itemNo,
          itemCount: this.state.itemCount,
          skuId: this.state.skuId || '',
          mpActivityName: this.state.mpActivityName || '',
        },
      ],
    };

    const { data } = await wxApi.request({
      url: api.virtualGoods.unifiedVirtualOrder,
      method: 'POST',
      data: {
        ...util.formatParams(parameter),
        orderRequestPromDTO: {
          couponNo: this.state.curCoupon.code,
          minPeople: this.state.minPeople,
          activityId: this.state.activityId,
          secKillNo: this.state.seckillNo,
          bargainNo: this.state.bargainNo,
          groupNo: this.state.groupNo,
        },

        virtualItemOrderVO: {
          phone: this.state.form.mobile,
          username: this.state.form.contact
        },

        itemNo: this.state.itemNo,
        distributorId: this.state.distributorId, // 分享人id
        orderMessage: this.state.orderMessage,
      },

      loading: true,
    });
    if (!data.prepayId) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/pay-success/index',
        data: {
          costPrice: this.state.price.needPayPrice,
          from: this.state.goodsDetail.roomTicketDTO ? 'ticket' : '',
          orderNo: data.data.orderNo
        },
      });
      this.handlerCallback(data, {data:data});
      return
    }
    pay.payByOrder(
      data.orderNo,
      null,
      {
        wxPaySuccess: (res) => {
          console.log('wxPaySuccess', res)
          this.handlerCallback(data, res)
        },

        /**
         * @payChannel {string} 1微信支付 3余额支付
        */
        success: (res, payChannel) => {
          console.log('success 订单生成成功', res, payChannel)
          // 余额支付不调 wxPaySuccess
          // 微信支付执行会重复提醒
          if(payChannel === '3'){
            this.handlerCallback(data, res)
          }
        },

        wxPayFail: () => {
          wxApi.$navigateTo({
            url: '/wxat-common/pages/tabbar-order/index',
            data: {
              tabType: 'mall_order'
            }
          })
        },

        cancel: () => {
          wxApi.$navigateTo({
            url: '/wxat-common/pages/tabbar-order/index',
            data: {
              tabType: 'mall_order',
            }
          })
        }
      }
    )
  }

  handlerCallback(data, res) {
    const { promotion, shareId, targetType, taskType, vip, isFirst } = this.props;
    if (promotion && shareId && targetType && !vip && isFirst) {
      // 4为秒杀 3为拼团
      if (taskType == 4) {
        this.addInviteRecord(shareId, res.data.orderNo);
      } else {
        this.addInviteRecord(shareId, res.data.groupNo);
      }
      store.getState().base.isFirst = false;
    }
    // 拼团支付
    if (data && data.groupNo) {
      wxApi.$redirectTo({
        url: `/sub-packages/marketing-package/pages/group/join/index?groupNo=${data.groupNo}`,
      });
    } else {
      // 正常支付
      this.nextGoPage(data.orderNo);
    }
  }

  nextGoPage(orderNo) {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/pay-success/index',
      data:{
        costPrice: this.state.price.needPayPrice,
        from: this.state.goodsDetail.roomTicketDTO ? 'ticket' : '',
        orderNo
      }
    })
    // let url = `/wxat-common/pages/order-list/index`;
    // let index = 2;
    // url += '?orderNo=' + orderNo + '&showHome=1';
    // wxApi.$navigateTo({
    //   url,
    //   data: {
    //     index,
    //     tabType: ORDER_STATUS.labelList[0].value,
    //   },
    // });
  }

  // 错误提示方法
  errorTip(msg) {
    Taro.showToast({
      icon: 'none',
      title: msg,
    });
  }
  /**
   * 任务中心邀请上报
   */

  addInviteRecord(shareId, referId) {
    const data = {
      shareId,
      referId,
    };

    wxApi.request({
      url: api.taskCenter.addInviteRecord,
      data,
      method: 'POST',
    });
  }

  // 获取优惠卷
  fetchPreferentialList() {
    const data = {
      storeId: this.state.currentStore.id,
      'orderRequestPromDTO.secKillNo': this.state.seckillNo,
      'orderRequestPromDTO.bargainNo': this.state.bargainNo,
      expressType: 0,
      itemDTOList: [
        {
          itemNo: this.state.itemNo,
          itemCount: this.state.itemCount,
          skuId: this.state.skuId || '',
          mpActivityName: this.state.mpActivityName || '',
        },
      ],
    };

    wxApi
      .request({
        url: api.order.coupons,
        quite: true,
        loading: false,
        data: util.formatParams(data),
      })
      .then((res) => {
        const coupons = res.data;
        let curCoupon = {
          maxDiscountFee: 0,
        };

        if (coupons.couponSet) {
          coupons.couponSet.forEach((item) => {
            item.couponMinimumFee =
              item.minimumFee === 0 ? '无门槛' : '满' + parseFloat((item.minimumFee / 100).toFixed(2));
            switch (item.couponCategory) {
              // 满减券
              case 0:
                item.couponDiscountFee = '减￥' + parseFloat((item.discountFee / 100).toFixed(2));
                if (item.status && curCoupon.maxDiscountFee < item.discountFee) {
                  curCoupon = item;
                  curCoupon.maxDiscountFee = item.discountFee;
                }
                break;
              //  折扣券
              case 2:
                item.couponDiscountFee = '折扣券: ' + parseFloat((item.discountFee / 10).toFixed(1)) + '折';
                const totalDiscount = this.state.goodsDetail ?
                  this.state.goodsDetail.wxItem.salePrice -
                  (this.state.goodsDetail.wxItem.salePrice * item.discountFee) / 100 : 0
                if (item.status && curCoupon.maxDiscountFee < totalDiscount) {
                  curCoupon = item;
                  item.maxDiscountFee = totalDiscount;
                }
                break;
            }
          });
        }
        this.setState({
          coupons: coupons,
          curCoupon: curCoupon,
        },()=>{
          this.handlePreferentialChange();
        });
      });
  }

  // 打开优惠卷窗口
  handleGoToCouponList =() => {
    const couponSet = this.state.coupons.couponSet;
    if (!couponSet || couponSet.length < 1) {
      return this.errorTip('无可用优惠券');
    }
    this.setState({
      isCoupon: 1,
    });

    this.getChoseDialog().showModal();
  }

  // 获取优惠对话框
  getChoseDialog() {
    return this.chooseCardDialogCOMP;
  }

  // 选择优惠卷
  onSelectCard =(e) => {
    const coupon = e.currentTarget.dataset.coupon;

    if (
      (coupon.status && coupon.status === 1) ||
      coupon.cardType === constants.card.type.gift ||
      coupon.cardType === constants.card.type.coupon
    ) {
      const { isCoupon } = this.state
      if(isCoupon == 1) {
        this.setState({
          curCoupon: coupon,
        },() => {
          this.getChoseDialog().hideModal();
          this.handlePreferentialChange();

        });
        return;
      }
      this.getChoseDialog().hideModal();
      this.handlePreferentialChange();
    } else {
      this.errorTip(coupon.rejReason || '该优惠不能使用。')
    }
  }

  // 计算价格
  handlePreferentialChange() {
    const data = {
      storeId: this.state.currentStore.id,
      expressType: 0,
      'orderRequestPromDTO.couponNo': this.state.curCoupon?.code,
      'orderRequestPromDTO.minPeople': this.state.minPeople,
      'orderRequestPromDTO.activityId': this.state.activityId,
      'orderRequestPromDTO.groupNo': this.state.groupNo,
      'orderRequestPromDTO.secKillNo': this.state.seckillNo,
      'orderRequestPromDTO.bargainNo': this.state.bargainNo,
      itemDTOList: [
        {
          itemNo: this.state.itemNo,
          skuId: this.state.skuId || '',
          itemCount: this.state.itemCount,
          mpActivityName: this.state.mpActivityName || '',
        },
      ],
    };

    this.setState({
      payAble: false,
    });

    Taro.showLoading({
      title: '请稍等',
    });

    wxApi
      .request({
        url: api.order.cal_price,
        loading: false,
        data: util.formatParams(data),
      })
      .then((res) => {
        this.setState({
          price: res.data,
          payAble: true,
        });
      })
      .finally(() => {
        Taro.hideLoading();
      });
  }

  // 不使用优惠卷
  onNoUseCard = () => {
    const { isCoupon } = this.state
    if(isCoupon == 1) {
      this.setState({
        curCoupon: {},
      },() => {
        this.getChoseDialog().hideModal();
        this.handlePreferentialChange();

      });
      return;
    }
    this.getChoseDialog().hideModal();
    this.handlePreferentialChange();
  }

  apiError() {
    this.setState({
      error: true,
    });
  }

  showIospayLimitDialog = () =>{
    this.iosPayLimitDialog.current.showIospayLimitDialog();
  }

  refChooseCardDialogCOMP = (node) => (this.chooseCardDialogCOMP = node);
  // 商品数量减1
  handleDecreaseBuyNum = () => {
    let num = this.state.itemCount;
    const min = this.state.goodsDetail.roomTicketDTO.minNum;
    if (num == 1) {
      return;
    }
    if (num <= min) {
      wxApi.showToast({
        title: "至少购买" + min + "张",
        icon: 'none',
      });
      return;
    }
    num = num - 1;
    this.setState({
      itemCount: num
    }, () => {
      this.validate();
      this.handlePreferentialChange();
    })
  };

  // 商品数量加1
  handleIncreaseBuyNum = () => {
    const stock = this.state.goodsDetail.roomTicketDTO.stock;
    const max = this.state.goodsDetail.roomTicketDTO.maxNum;
    let num = this.state.itemCount;
    if (num == stock) {
      wxApi.showToast({
        title: "库存不足",
        icon: "error"
      })
      return;
    }
    if (num >= max && max != 0) {
      wxApi.showToast({
        title: "限购" + max + "张",
        icon: "error"
      })
      return;
    }
    num = num + 1;
    this.setState({
      itemCount: num
    }, () => {
      this.validate();
      this.handlePreferentialChange();
    })
  }

  render() {
    const {
      isNull,
      error,
      activityId,
      groupNo,
      tmpStyle,
      goodsDetail,
      itemCount,
      form,
      payDetails,
      curCoupon,
      couponEnum,
      price,
      coupons,
      seckillNo,
      bargainNo,
      orderMessage,
      goodsTotalPrice,
      isGiftActivity,
      isFullScreen,
      isCoupon,
      formValidated,
      payAble
    } = this.state;
    const isError = this.isError(formValidated, error, payAble, goodsDetail);
    const {isIOS,iosSetting} = this.props
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-pvdp-PayOrder" className="wk-pvdp-PayOrder">
        {isNull ? (
          <Empty message="商品已下架，请购买其它商品"></Empty>
        ) : (
          <Block>
            {!!error && <Error></Error>}
            {!error && (
              <View className="pay-order">
                {/* 团购进度条   */}
                {(activityId || groupNo) && (
                  <View className="group-steps">
                    <View className="tab">
                      <View className="step" style={_safe_style_('background-color:' + tmpStyle && tmpStyle.btnColor)}>
                        1
                      </View>
                      <View className="dotted-line" style={_safe_style_('border-color:' + tmpStyle && tmpStyle.bgColor)}></View>
                      <View className="step-text">选择商品开团/参团</View>
                    </View>
                    <View className="tab">
                      <View className="step" style={_safe_style_('background-color:' + tmpStyle && tmpStyle.btnColor)}>
                        2
                      </View>
                      <View className="dotted-line" style={_safe_style_('border-color:' + tmpStyle && tmpStyle.bgColor)}></View>
                      <View className="step-text">邀请好友参团</View>
                    </View>
                    <View className="tab">
                      <View className="step" style={_safe_style_('background-color:' + tmpStyle && tmpStyle.btnColor)}>
                        3
                      </View>
                      <View className="dotted-line" style={_safe_style_('border-color:' + tmpStyle && tmpStyle.bgColor)}></View>
                      <View className="step-text">人满成团</View>
                    </View>
                  </View>
                )}

                <View className="goods-box">
                  <View className="img-box">
                    <Image src={goodsDetail && goodsDetail.wxItem.thumbnail} mode="aspectFill" className="img"></Image>
                    { goodsDetail && goodsDetail.wxItem.isShelf === 0 && (
                      <View className="low-stock">
                        <View className="low-label" style={_safe_style_('width: 80rpx')}>
                          已下架
                        </View>
                      </View>
                    )}
                  </View>
                  <View className="goods-info-box">
                    <View className="goods-name limit-line">{goodsDetail && goodsDetail.wxItem.name}</View>
                    <View className="goods-num">{'x ' + itemCount}</View>
                  </View>
                </View>
                <View style={_safe_style_('height: 10px;')}></View>
                <View className="shipping-method">
                  <View className="row-box">
                    <View className="row-label">*联系人</View>
                    <View className="right-text">
                      <Input
                        style={_safe_style_('text-align:right')}
                        name="remark"
                        type="text"
                        placeholderClass="placeholder"
                        className="note"
                        placeholder="请输入联系人"
                        onInput={this.contactInput}
                        value={form.contact}
                      />
                    </View>
                  </View>
                  <View className="row-box">
                    <View className="row-label">*手机号</View>
                    <View className="right-text">
                      <Input
                        style={_safe_style_('text-align:right')}
                        name="remark"
                        type="text"
                        placeholderClass="placeholder"
                        className="note"
                        placeholder="请输入手机号"
                        onInput={this.mobileInput}
                        value={form.mobile}
                      />
                    </View>
                  </View>
                  {payDetails &&
                    payDetails.map((item, index) => {
                      return (
                        <Block key={index}>
                          {item.isCustom ? (
                            <View className="row-box">
                              <View className="row-label">{item.label}</View>
                              <View className="right-text">{item.value}</View>
                            </View>
                          ) : (
                            <Block>
                              {item.name === 'coupon' && !seckillNo && !bargainNo && (
                                <View onClick={this.handleGoToCouponList} className="row-box">
                                  <View className="row-label">优惠劵</View>
                                  {curCoupon && curCoupon.name ? (
                                    <View className="right-text">
                                      {curCoupon.couponCategory === couponEnum.TYPE.discount.value && (
                                        <Text>{filters.discountFilter(curCoupon && curCoupon.discountFee, true) + '折'}</Text>
                                      )}

                                      {'减￥' + filters.moneyFilter(price && price.couponPrice, true)}
                                    </View>
                                  ) : coupons && coupons.couponSet && coupons.couponSet.length ? (
                                    <View className="right-text">{'可用' + coupons.couponSet.length + '张'}</View>
                                  ) : (
                                    <View className="right-text">无可用</View>
                                  )}

                                  <Image
                                    className="right-icon"
                                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png"
                                  ></Image>
                                </View>
                              )}

                              {price && price.groupPromFee && (
                                <View className="row-box">
                                  <View className="row-label">拼团优惠</View>
                                  <View className="right-text">
                                    {'-¥ ' + filters.moneyFilter(price && price.groupPromFee, true)}
                                  </View>
                                </View>
                              )}

                              {item.name === 'seckill' && (
                                <View className="row-box">
                                  <View className="row-label">秒杀优惠</View>
                                  <View className="right-text">
                                    {'减￥' + filters.moneyFilter(price && price.secKillPromFee, true)}
                                  </View>
                                </View>
                              )}
                            </Block>
                          )}
                        </Block>
                      );
                    })}
                  <View className="row-box">
                    <View className="row-label">备注</View>
                    <View className="right-text">
                      <Input
                        name="remark"
                        type="text"
                        placeholderClass="placeholder"
                        className="note"
                        placeholder="建议留言前先与商家沟通确认"
                        onBlur={this.messageInput}
                        value={orderMessage}
                      ></Input>
                    </View>
                  </View>
                  <View className="row-box" style={_safe_style_('margin-top: 20rpx;')}>
                    <View className="row-label">商品原价</View>
                    <View className="right-text">
                      {'¥ ' + filters.moneyFilter(price && price.totalPrice || goodsTotalPrice, true)}
                    </View>
                  </View>
                  <View className="row-box">
                    <View className="row-label">共优惠</View>
                    <View className="right-text">
                      {'- ¥ ' +
                        (isGiftActivity
                          ? filters.moneyFilter(price && price.totalPrice || goodsTotalPrice, true)
                          : filters.moneyFilter(price && price.totalFreePrice || 0, true) < 0
                          ? 0
                          : filters.moneyFilter(price && price.totalFreePrice || 0, true))}
                    </View>
                  </View>
                  {
                    goodsDetail && goodsDetail.roomTicketDTO && (
                      <View className="row-box">
                        <View className="row-label">数量</View>
                        <View className="right-text">
                          <View className="count-box">
                            <View
                              className="minus left-radius"
                              onClick={this.handleDecreaseBuyNum}
                            >
                              -
                            </View>
                            <View className="num">
                              <Input className="input-comp" type="number" value={itemCount} disabled></Input>
                            </View>
                            <View
                              className="plus right-radius"
                              onClick={this.handleIncreaseBuyNum}
                            >
                              +
                            </View>
                          </View>
                        </View>
                      </View>
                    )
                  }
                </View>
                {/*  支付按钮  */}
                <View className={'pay-box ' + (isFullScreen ? 'fix-full-screen' : '')}>
                  <View className="total-price">
                    <Text>合计：</Text>
                    <Text className="price-logo">¥</Text>
                    <Text className="price-num">{filters.moneyFilter(price && price.needPayPrice, true)}</Text>
                  </View>
                  {
                    !isIOS || price?.groupPromFee ? <Button
                    onClick={this.handlePay}
                    className="to-pay-btn"
                    style={_safe_style_('background:' + (isError ? '#cccccc' : tmpStyle && tmpStyle.btnColor))}
                  >
                    确认支付
                  </Button> : <Button onClick={this.handleIosPay} className="to-pay-btn" style={{'background':isError ? "#cccccc" : tmpStyle.btnColor}}>{iosSetting?.buttonCopywriting || '确认支付'}</Button>
                  }
                </View>
              </View>
            )}
          </Block>
        )}
        <IosPayLimitDialog tipsCopywriting={iosSetting?.tipsCopywriting} ref={this.iosPayLimitDialog}></IosPayLimitDialog>
        {/*  选择优惠对话框  */}
        <BottomDialog id="choose-card" customClass="bottom-dialog-custom-class" ref={this.refChooseCardDialogCOMP}>
          {isCoupon === 1 && (
            <View className="coupon-select">
              { coupons && coupons.couponSet &&
                coupons.couponSet.map((item, index) => {
                  return (
                    <View
                      className={'coupon-tab couponSet-tab ' + (!item.status ? 'disable-coupon' : '')}
                      key={item.code}
                      onClick={_fixme_with_dataset_(this.onSelectCard, { coupon: item })}
                    >
                      <View className="coupon-img-box">
                        {!item.status ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gray.png"
                          ></Image>
                        ) : item.couponCategory === 1 ? (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-blue.png"
                          ></Image>
                        ) : (
                          <Image
                            className="coupon-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/bg-gren.png"
                          ></Image>
                        )}
                      </View>
                      <View className="coupon-tab-left">
                        <View className="coupon-price">
                          {(item.couponCategory === couponEnum.TYPE.freight.value ||
                            item.couponCategory === couponEnum.TYPE.fullReduced.value) && (
                            <Block>
                              <Text className="price-logo">￥</Text>
                              <Text className="price-num">{filters.moneyFilter(item.discountFee, true)}</Text>
                            </Block>
                          )}

                          {item.couponCategory === couponEnum.TYPE.discount.value && (
                            <Block>
                              <Text className="price-num">{filters.discountFilter(item.discountFee, true)}</Text>
                              <Text className="price-logo">折</Text>
                            </Block>
                          )}
                        </View>
                        {item.minimumFee === 0 ? (
                          <View className="coupon-minimumFee">无门槛</View>
                        ) : (
                          <View className="coupon-minimumFee">
                            {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                          </View>
                        )}

                        {item.couponCategory === 0 && <View className="coupon-type">满减券</View>}

                        {item.couponCategory === 2 && <View className="coupon-type">折扣券</View>}
                      </View>
                      <View className="coupon-tab-right">
                        <View className="coupon-name limit-line">{item.name}</View>
                        {item.endTime && (
                          <View className="coupon-validityDate">{'有效期：' + filters.dateFormat(item.endTime)}</View>
                        )}
                      </View>
                    </View>
                  );
                })}
              <View className="no-use-coupon" onClick={this.onNoUseCard}>
                不使用优惠
              </View>
            </View>
          )}
        </BottomDialog>
      </View>
    );
  }
}

export default PayOrder;
