import { _safe_style_ } from 'wk-taro-platform';
import { Block, View, Input } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import wxClass from '../../../../../../wxat-common/utils/wxClass.js';
import wxApi from '../../../../../../wxat-common/utils/wxApi.js';
import api from '../../../../../../wxat-common/api/index.js';

import AuthPuop from '../../../../../../wxat-common/components/authorize-puop/index';
import './index.scss';

@withWeapp({
  onLoad() {
    let prevPage = Taro.getCurrentPages().slice(-2)[0];
    this.setData({
      salePrice: prevPage.data.goodsDetail.wxItem.salePrice,
    });

    this.itemNo = prevPage.data.goodsDetail.itemNo;
    this.distributorId = prevPage.data.distributorId;
  },
  bindinput({ detail: { value } }) {
    this.qq = value;
  },
  async bindtap() {
    if (!this.qq) return;
    let { data } = await wxApi.request({
      url: api.virtualGoods.unifiedVirtualOrder,
      method: 'POST',
      data: {
        virtualItemOrderVO: {
          qq: this.qq,
        },

        itemNo: this.itemNo,
        distributorId: this.distributorId,
      },

      loading: true,
    });

    data.timeStamp = data.timeStamp + '';
    data.package = 'prepay_id=' + data.prepayId;
    data.success = () => {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/virtual-goods/my/index',
      });
    };
    data.fail = ({ errMsg }) => {
      if (errMsg !== 'requestPayment:fail cancel') {
        Taro.showToast({ title: errMsg });
      }
    };
    Taro.requestPayment(data);
  },
})
class Qq extends React.Component {
  render() {
    const { salePrice } = this.data;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-pvdq-Qq" className="wk-pvdq-Qq">
        <View style={_safe_style_('display: flex;background: #fff;align-items: center;height: 13vw;')}>
          <View style={_safe_style_('width: 22vw;padding-left: 2vw;')}>qq</View>
          <Input placeholder="请输入qq号" type="number" onInput={this.bindinput}></Input>
        </View>
        <View
          style={_safe_style_(
            'position:fixed;bottom: 0;right: 0;left: 0;background: #fff;display: flex;align-items: center;'
          )}
        >
          <View style={_safe_style_('font-size:4vw;margin-left: 3vw;')}>合计：</View>
          <View style={_safe_style_('font-size:3vw;color:red')}>
            ¥<View style={_safe_style_('display: inline;font-weight: bold;font-size: 5vw;')}>{salePrice / 100}</View>
          </View>
          <View
            onClick={this.bindtap}
            style={_safe_style_(
              'height: 14vw;width: 33vw;color: #fff;background: #3a8fff;text-align: center;margin-left: auto;line-height: 14vw;font-size: 5vw;'
            )}
          >
            确认绑定
          </View>
        </View>
      </View>
    );
  }
}

export default Qq;
