import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type'; // @externalClassesConvered(Empty)
import {_fixme_with_dataset_, _safe_style_} from '@/wxat-common/utils/platform';
import {Block, Button, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs';
import api from '../../../../../wxat-common/api/index.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import login from '../../../../../wxat-common/x-login/index.js';
import {connect} from 'react-redux';
import constants from '../../../../../wxat-common/constants/index.js';
import report from '../../../../../sdks/buried/report/index';

import template from '../../../../../wxat-common/utils/template.js';
import shareUtil from '../../../../../wxat-common/utils/share.js';

import LoadMore from '../../../../../wxat-common/components/load-more/load-more';
import Empty from '../../../../../wxat-common/components/empty/empty';
import './index.scss';
import hoc from '@/hoc';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';

const loadMoreStatus = constants.order.loadMoreStatus;
const mapStateToProps = (state) => ({
  allState: state,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class mycard extends React.Component {
  state = {
    customKey: null,
    pageConfig: null,
    cardList: [],
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    cards: [],
    bg: null,
    tmpStyle: {},
    constants,
    userId: '',
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    wxApi.hideShareMenu();
    this.getTemplateStyle();
    this.setState({
      userId: this.userIdMethod(),
    });
  }

  componentDidShow() {
    this.setState(
      {
        pageNo: 1,
        cardList: [],
        hasMore: true,
      },

      () => {
        this.listCardList();
      }
    );
  }
  componentDidUpdate(props){
    if(props.allState.base?.loginInfo!==this.props.allState.base?.loginInfo){
      if (this.props.allState.base.loginInfo && this.props.allState.base.loginInfo.userId) {
        this.setState({
          userId:this.props.allState.base.loginInfo.userId
        })
      }
    }
  }
  onGoto = (e) => {
    const id = e.currentTarget.dataset.id;
    const card = this.state.cardList.find((cardItem) => cardItem.id === id);
    if (!card) {
      return;
    }
    const userId = this.state.userId;
    let url = '';
    if (card.cardStatus === 1 && (card.receiveUserId === userId || card.ownerStatus === 0)) {
      // 跳转消费记录
      url = pageLinkEnum.marketingPkg.giftCard.mine.consumption + '?cardNo=' + card.userCardNo;
    } else if (card.ownerStatus === 1) {
      if (!!card.receiveUserId) {
        // 跳转已赠送界面
        url = pageLinkEnum.marketingPkg.giftCard.mine.sent + '?cardNo=' + card.userCardNo;
      } else {
        // 跳转赠送界面
        url = pageLinkEnum.marketingPkg.giftCard.mine.sending + '?cardNo=' + card.userCardNo;
      }
    }

    if (url) {
      wxApi.$navigateTo({ url: url });
    }
  };

  listCardList() {
    this.setState({ error: false, hasMore: false, loadMoreStatus: loadMoreStatus.LOADING }, () => {
      login.login().then(() => {
        wxApi
          .request({
            url: api.giftCard.gift_card,
            data: {
              pageNo: this.state.pageNo,
              pageSize: 10,
            },
          })
          .then((res) => {
            const data = (res.data || []).map((item) => {
              item.count = 0;
              //是否过期
              item.overdue = new Date(item.validityDate || '') < new Date();
              return item;
            })
            const list = this.state.cardList.concat(data)
            if (list.length === res.totalCount && res.totalCount) {
              this.setState({
                hasMore: false,
                loadMoreStatus: loadMoreStatus.BASELINE
              })
            } else {
              this.setState({
                hasMore: true,
                loadMoreStatus: loadMoreStatus.HIDE
              })
            }
            this.setState({
              cardList: list
            })
          })
          .catch(() => {
            if (!this.state.cardList.length) {
              this.setState({
                error: true,
                loadMoreStatus: loadMoreStatus.HIDE
              });
            } else {
              this.setState({
                loadMoreStatus: loadMoreStatus.ERROR
              });
            }
          })
      });
    });
  }

  userIdMethod() {
    const loginInfo = this.props.allState.base.loginInfo;
    if (loginInfo && loginInfo.userId) {
      return loginInfo.userId;
    }
    return;
  }

  onSelf = (e) => {
    const id = e.currentTarget.dataset.id;
    const card = this.state.cardList.find((cardItem) => cardItem.id === id);
    if (!card) {
      return;
    }
    // //礼品卡的订阅消息
    // let Ids = [subscribeEnum.BIND_GIFT_CARD.value]
    // //授权订阅消息
    // subscribeMsg.sendMessage(Ids).then(() => {

    wxApi.showModal({
      title: '提示',
      content: '选择自用，礼品卡将会激活绑定自用，不能送给朋友。',
      success(res) {
        if (res.confirm) {
          wxApi
            .request({
              url: api.giftCard.active,
              loading: true,
              data: {
                userGiftCardNo: card.userCardNo,
              },
            })
            .then((res) => {
              wxApi.$navigateTo({
                url: pageLinkEnum.marketingPkg.giftCard.activate + '?cardNo=' + card.userCardNo + '&isSelf=true',
              });
            })
            .catch((error) => {
              console.log(error);
            });
        } else if (res.cancel) {
          console.log('用户点击取消');
        }
      },
    });

    // })
  };
  onShareAppMessage(e) {
    report.share(true);
    if (!e.target) {
      return;
    }
    const id = e.target.dataset.item && e.target.dataset.item.id;
    const card = this.state.cardList.find((cardItem) => cardItem.id === id);
    if (!card) {
      return;
    }

    const _this = this;
    const userNickName = card.userNickname ? card.userNickname : '';
    const path = shareUtil.buildShareUrlPublicArguments({
      url: pageLinkEnum.marketingPkg.giftCard.receive + '?cardNo=' + card.userCardNo,
      bz: shareUtil.ShareBZs.GIFT_CARD_MINE_SENDING,
      bzName: `我赠送的礼品卡${card.userNickname ? '-' + card.userNickname : ''}`,
      bzId: card.userCardNo,
      sceneName: card.userNickname,
    });

    console.log('sharePath => ', path);
    return {
      title: `${userNickName} 送你一张礼品卡，价值￥${card.usableAmount / 100}，请点击领取。`,
      path,
      imageUrl: card.cardThemeImage,
      fail: function (res) {
        report.share(false);
        // 转发失败
        console.log('转发失败:' + JSON.stringify(res));
      },
    };
  }
  onSendCard = (e) => {
    if (!e.target) {
      return;
    }
    const id = e.target.dataset.id;
    const card = this.state.cardList.find((cardItem) => cardItem.id === id);
    if (!card) {
      return;
    }
    wxApi.showModal({
      title: '提示',
      content: '赠送好友，记得把激活密码一起发给对方哟。',
      showCancel: false,
      success(res) {
        if (res.confirm) {
          wxApi.request({
            url: api.giftCard.send_out,
            data: {
              userGiftCardNo: card.userCardNo,
            },
          });

          const url = pageLinkEnum.marketingPkg.giftCard.mine.sending + '?cardNo=' + card.userCardNo;
          wxApi.$navigateTo({ url });
        } else if (res.cancel) {
          console.log('用户点击取消');
        }
      },
    });
  };

  onPullDownRefresh() {
    this.setState(
      {
        pageNo: 1,
        cardList: [],
        hasMore: true,
      },

      () => {
        this.listCardList();
      }
    );
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState(
        {
          pageNo: this.state.pageNo + 1
        },

        () => {
          this.listCardList();
        }
      );
    }
  }

  onRetryLoadMore = () => {
      this.listCardList()
  };
  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    console.log("111111111111111111111111111111",this.state.cardList)
    const { cardList, error, constants, tmpStyle, userId, loadMoreStatus } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pgm-Mine' className='wk-pgm-Mine'>
        <View className='serve-list'>
          {!error && !cardList.length && !loadMoreStatus && (
            <Empty iconClass='empty-icon' message='暂无礼品卡'></Empty>
          )}

          {error && <Error></Error>}
          {!!cardList.length && (
            <View className='card-list'>
              {cardList.map((item, index) => {
                return (
                  <View
                    key={item.id}
                    className='card-item'
                    onClick={_fixme_with_dataset_(this.onGoto, { id: item.id })}
                  >
                    <View className='left'>
                      <View className='card-name'>{item.cardItemName}</View>
                      <View className='card-price'>{'价值：￥' + filters.moneyFilter(item.usableAmount, true)}</View>
                    </View>
                    <View className='right'>
                      {item.overdue && item.validityType === 1 ? (
                        <View className='card-status'>已过期</View>
                      ) : item.ownerStatus === constants.giftCard.ownerStatus.SELF &&
                        item.cardStatus === constants.giftCard.cardStatus.UNUSED ? (
                        <View className='card-status'>
                          <Button onClick={_fixme_with_dataset_(this.onSelf, { id: item.id })} className='btn btn-self'>
                            自用
                          </Button>
                          <ButtonWithOpenType
                            openType='share'
                            className='btn btn-send'
                            style={_safe_style_('background:' + tmpStyle.btnColor)}
                            onClick={_fixme_with_dataset_(this.onSendCard, { id: item.id })}
                            item={item}
                          >
                            赠送
                          </ButtonWithOpenType>
                        </View>
                      ) : (
                        <View className='card-status'>

                          {
                          item.cardStatus == constants.giftCard.cardStatus.USED &&
                          (item.ownerStatus === constants.giftCard.ownerStatus.SELF ||
                            item.receiveUserId === userId) ? (
                            <Block>{item.balance === 0 ? '已用完' : '已激活'}</Block>
                          ) : (
                            <Block>{!!item.receiveUserId ? '已赠送：' + item.receiveUserNickname : '赠送中'}</Block>
                          )}
                        </View>
                      )}

                      <View className>{'有效期' + (!!item.validityDate ? '：' + item.validityDate : '永久')}</View>
                    </View>
                  </View>
                );
              })}
            </View>
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
          <AuthPuop></AuthPuop>
        </View>
      </View>
    );
  }
}

export default mycard;
