import {$getRouter} from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
// @scoped
import {_fixme_with_dataset_} from '@/wxat-common/utils/platform';
import {Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../../wxat-common/utils/money.wxs.js';
import api from '../../../../../../wxat-common/api/index.js';
import wxApi from '../../../../../../wxat-common/utils/wxApi';
import template from '../../../../../../wxat-common/utils/template.js';
import pageLinkEnum from '../../../../../../wxat-common/constants/pageLinkEnum.js';

import Error from '../../../../../../wxat-common/components/error/error';
import Empty from '../../../../../../wxat-common/components/empty/empty';
import './index.scss';
import giftApi from '../../gift-api';

class consum extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    pageNo: 1,
    pageSize: 10,
    hasMore: true,
    empty: false,
    error: false,
    billError: false,
    giftCardInfo: {},

    billRecordList: [],
  };

  userCardNo: string; /*请尽快迁移为 componentDidMount 或 constructor*/
  /**
   * 生命周期函数--监听页面加载
   */ UNSAFE_componentWillMount() {
    const options = this.$router.params || {};
    wxApi.hideShareMenu();
    this.getTemplateStyle();

    if (!!options.cardNo) {
      this.userCardNo = options.cardNo;
      this.apiCardDetail();
    } else {
      this.setState({ empty: true });
    }
  }

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // 页面相关事件处理函数--监听用户下拉动作
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.getGiftCardBill(this.userCardNo);
      }
    );
  }

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    // 页面上拉触底事件的处理函数
    if (this.state.hasMore) {
      this.getGiftCardBill(this.userCardNo);
    }
  }

  onGotoOrderRecord(e) {
    const orderNo = e.currentTarget.dataset.orderNo;
    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.orderDetail,
      data: {
        orderNo: orderNo,
      },
    });
  }

  async apiCardDetail() {
    this.setState({ error: false, empty: false });
    const giftCardInfo = await giftApi.apiCardDetail(this.userCardNo);
    if (!giftCardInfo) {
      this.setState({ error: true });
    } else {
      this.setState({ giftCardInfo });
      this.onPullDownRefresh();
    }
  }

  getGiftCardBill(userGiftCardNo) {
    return wxApi
      .request({
        url: api.giftCard.giftCardBill,
        loading: true,
        data: {
          pageNo: this.state.pageNo,
          pageSize: 10,
          userGiftCardNo,
        },
      })
      .then((res) => {
        let billRecordList = [];

        if (this.isLoadMoreRequest()) {
          billRecordList = this.state.billRecordList.concat(res.data || []);
        } else {
          billRecordList = res.data || [];
        }

        this.setState({
          billRecordList: billRecordList,
          billError: false,
        });

        if (this.state.billRecordList.length === res.totalCount) {
          this.setState({
            hasMore: false,
          });
        }

        if (this.state.hasMore == true)
          this.setState({
            pageNo: this.state.pageNo++,
          });
      })
      .catch((error) => {
        if (!this.isLoadMoreRequest()) {
          this.setState({
            billError: true,
          });
        }
        console.log('balance: error: ' + JSON.stringify(error));
      });
  }
  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }
  bindErrorImage(e) {
    this.setState({
      ['giftCardInfo.cardThemeImage']: 'https://htrip-static.ctlife.tv/wk/empty.png',
    });
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }
  render() {
    const { empty, error, giftCardInfo, billRecordList, billError } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-pgmc-Consumption'
        className='wk-pgmc-Consumption'
      >
        {!!(!!empty && !error) && <Empty iconClass='empty-icon' message='敬请期待'></Empty>}

        {!!error && <Error></Error>}
        {!!(!empty && !error) && (
          <View className='actived-gift-card'>
            <View className='warning-text'>
              <View className='icon-container'>
                <Image className='activatedImage' src="https://htrip-static.ctlife.tv/wk/actived.png"></Image>
              </View>
              {/*  <text>已激活绑定</text>  */}
            </View>
            <View className='theme-card'>
              <View className='theme-card-container'>
                <View className='theme-image-area'>
                  <Image
                    className='theme-image-area'
                    src={giftCardInfo.cardThemeImage}
                    onError={this.bindErrorImage.bind(this)}
                  ></Image>
                </View>
                {/*  <View class="theme-image-name">{{giftCardInfo.cardThemeName}}</View>  */}
              </View>
            </View>
            <View className='gift-info'>
              <View className='gift-basic-info'>
                <Text>{giftCardInfo.cardItemName}</Text>
                {/*  <View class="gift-info-name">{{giftCardInfo.cardItemName}}</View>  */}
                {/*  <View class="gift-info-price">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </View>  */}
                {/*  <View class="gift-info-count" wx:if="{{giftCardInfo.count>1}}"><text>x{{giftCardInfo.count}}</text></View>  */}
              </View>
              <View className='gift-basic-info'>
                <Text>{'价值：¥' + filters.moneyFilter(giftCardInfo.usableAmount, true)}</Text>
              </View>
              <View className='gift-balance-info'>
                <Text>{'已消费：¥' + filters.moneyFilter(giftCardInfo.usableAmount - giftCardInfo.balance, true)}</Text>
              </View>
              <View className='gift-balance-info'>
                <Text>{'余额：¥' + filters.moneyFilter(giftCardInfo.balance, true)}</Text>
              </View>
            </View>
            <View className='gift-pay-record'>
              <View className='title'>消费记录</View>
              {!!(!!billRecordList && billRecordList.length === 0 && !billError) && (
                <Empty
                  iconClass='empty-icon'
                  message='暂无消费记录'
                  isDialog='true'
                  icon="https://htrip-static.ctlife.tv/wk/empty.png"
                  emptyIconStyle='width:334.6rpx;height:280rpx'
                ></Empty>
              )}

              {!!billError && <Error isDialog='true'></Error>}
              {!!(!!billRecordList && !!billRecordList.length && !billError) && (
                <View className='pay-record-table'>
                  {billRecordList.map((item, index) => {
                    return (
                      <View
                        className='pay-record-item'
                        key={item.id}
                        onClick={_fixme_with_dataset_(this.onGotoOrderRecord.bind(this), {
                          item: item,
                          index: index,
                          id: item.id,
                          orderNo: item.orderNo,
                        })}
                      >
                        <View className='record-item-no'>{'订单编号：' + item.orderNo}</View>
                        <View className='record-item-detail'>
                          订单时间：<Text>{filters.dateFormat(item.createTime, 'yyyy-MM-dd hh:mm:ss')}</Text>
                        </View>
                        <View className='record-item-detail'>
                          <Text>
                            {(item.type == 2 ? '退款金额：+' : '抵扣金额：-') +
                              '¥' +
                              filters.moneyFilter(item.orderAmount, true)}
                          </Text>
                        </View>
                      </View>
                    );
                  })}
                </View>
              )}
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default consum;
