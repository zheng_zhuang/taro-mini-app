import {$getRouter} from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import {_safe_style_} from '@/wxat-common/utils/platform';
import {Block, Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../../../wxat-common/utils/wxApi';
import api from "../../../../../../wxat-common/api";
import report from "../../../../../../sdks/buried/report";
import template from '../../../../../../wxat-common/utils/template.js';
import shareUtil from '../../../../../../wxat-common/utils/share.js';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import giftApi from '../../gift-api';

import './index.scss';

class sending extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    tmpStyle: {},
    cardInfo: null,
    cardpw: '******',
    error: false,
  };

  userCardNo: string; /* 请尽快迁移为 componentDidMount 或 constructor */
  /**
   * @param {Object} options - 启动参数
   * @param {string} options.cardNo - 礼品卡卡号
   */ UNSAFE_componentWillMount() {
    const options = this.$router.params || {};
    this.userCardNo = options.cardNo;
    this.getTemplateStyle();
    this.apiCardDetail();
  }

  /**
   * 生命周期函数--监听页面显示
   */
  componentDidShow() {
    this.getCardInfo();
  }

  onRetry() {
    this.apiCardDetail();
  }

  onShareAppMessage(e) {
    report.share(true);
    const card = this.state.cardInfo;
    const path = shareUtil.buildShareUrlPublicArguments({
      url: pageLinkEnum.marketingPkg.giftCard.receive + '?cardNo=' + this.userCardNo,
      bz: shareUtil.ShareBZs.GIFT_CARD_SENDING_DETAIL,
      bzName: `礼品卡${card && card.cardItemName ? '-' + card.cardItemName : ''}`,
      bzId: this.userCardNo,
      sceneName: card && card.cardItemName,
    });

    console.log('sharePath => ', path);
    return {
      title: `${card.userNickname ? card.userNickname : ''} 送你一张礼品卡，价值￥${
        card.usableAmount / 100
      }，请点击领取。`,
      path,

      imageUrl: card.cardThemeImage,
      success: function (res) {},
      fail: function (res) {
        report.share(false);
        // 转发失败
        console.log('转发失败:' + JSON.stringify(res));
      },
    };
  }

  onGoBack() {
    wxApi.navigateBack();
  }

  // 复制功能
  copyBtn(e) {
    const cardpw = this.state.cardpw.toString();
    wxApi.setClipboardData({
      data: cardpw,
      success: function (res) {
        wxApi.getClipboardData({
          success: function (res) {
            wxApi.showToast({
              title: '复制成功',
              icon: 'none',
            });

            console.log(res.data); // data
          },
        });
      },
    });
  }

  apiError() {
    this.setState({
      error: true,
    });
  }

  // 获取激活密码
  getCardInfo() {
    this.setState(
      {
        error: false,
      },

      () => {
        const userCardNo = this.userCardNo;
        wxApi
          .request({
            url: api.giftCard.card_pw,
            loading: true,
            data: {
              userGiftCardNo: userCardNo,
            },
          })
          .then((res) => {
            this.setState({
              cardpw: res.data,
            });
          })
          .catch((error) => {
            this.apiError(error);
          });
      }
    );
  }

  async apiCardDetail() {
    this.setState({ error: false });
    const cardInfo = await giftApi.apiCardDetail(this.userCardNo);
    if (!cardInfo) {
      this.apiError();
    } else {
      this.setState({ cardInfo });
    }
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  handleCancelSend = () => {
    wxApi.showModal({
      title: '温馨提示:',
      content: '确定取消赠送礼品卡?',
      cancelText: '取消',
      confirmText: '确定',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.giftCard.cancelSend,
              loading: true,
              data: {
                userGiftCardNo: this.userCardNo,
              },
            })
            .then((res) => {
              wxApi.showToast({
                mask: true,
                title: '已取消赠送',
              });

              setTimeout(() => {
                wxApi.$navigateBack();
              }, 1500);
            })
            .catch((error) => {});
        }
      },
    });
  };

  render() {
    const { tmpStyle, cardInfo, cardpw, error } = this.state;
    return (
      <View data-scoped='wk-gms-Sending' className='wk-gms-Sending sending'>
        {!!cardInfo && (
          <Block>
            <View className='title' style={_safe_style_('color:' + tmpStyle.btnColor)}>
              赠送中
            </View>
            <Image className='img-card' src={cardInfo.cardThemeImage}></Image>
            <View className='card-info'>
              <View className='card-name limit-line line-2'>{cardInfo.cardItemName}</View>
              <View className='card-con' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                {'面值：￥' + filters.moneyFilter(cardInfo.usableAmount, true)}
              </View>
            </View>
            <View className='active-info'>
              <View className='active-code'>
                激活密码：
                <Text selectable='true' onLongtap={this.copy} className='code'>
                  {cardpw}
                </Text>
                <Image
                  className='copy'
                  src="https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-copy.png"
                  onClick={this.copyBtn.bind(this)}
                ></Image>
              </View>
              <View className='card-tips'>为确保您的资金安全，请单独把激活密码发送给朋友。</View>
            </View>
            <View className='btn-box'>
              <View className='btn-send-box'>
                <ButtonWithOpenType
                  className='btn-send'
                  openType='share'
                  style={_safe_style_('background:' + tmpStyle.btnColor)}
                >
                  分享赠送
                </ButtonWithOpenType>
                <View className='btn-cancel' onClick={this.handleCancelSend}>
                  取消赠送
                </View>
              </View>
            </View>
          </Block>
        )}

        {!!((!cardInfo || !cardInfo.cardItemName) && !error) && (
          <Empty iconClass='empty-icon' message='未找到对应数据'></Empty>
        )}

        {!!error && <Error></Error>}
      </View>
    );
  }
}

export default sending;
