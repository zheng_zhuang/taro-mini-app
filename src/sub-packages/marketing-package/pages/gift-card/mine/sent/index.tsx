import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { Block, View, Image, Button } from '@tarojs/components';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import filters from '../../../../../../wxat-common/utils/money.wxs.js';
import giftApi from '../../gift-api';

import './index.scss';

class send extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state={
    cardInfo: null
  };

  userCardNo: string; /*请尽快迁移为 componentDidMount 或 constructor*/
  /**
   * 生命周期函数--监听页面加载
   */ UNSAFE_componentWillMount() {
    const options = this.$router.params || {};
    this.userCardNo = options.cardNo;
    this.apiCardDetail();
  }

  onComplete() {
    wxApi.navigateBack();
  }

  async apiCardDetail() {
    const cardInfo = await giftApi.apiCardDetail(this.userCardNo);
    if (!!cardInfo) {
      this.setState({ cardInfo });
    }
  }

  render() {
    const { cardInfo } = this.state;
    return (
      <View className='receive'>
        {!!cardInfo && !!cardInfo.cardItemName && (
          <Block>
            <Image src={cardInfo.cardThemeImage} className='banner'></Image>
            <View className='content'>
              <View className='label'>包含内容：</View>
              <View style={_safe_style_('display:flex;margin-bottom: 27rpx;')}>
                <View className='card-name ellipsis'>{cardInfo.cardItemName + '礼品卡1张'}</View>
                <View display='inline-block'>{'价值：￥' + filters.moneyFilter(cardInfo.usableAmount, true)}</View>
              </View>
              <View className='receiver'>
                <View className='avator'>
                  <Image className='avatar__image' src={cardInfo.receiveUserAvatar}></Image>
                  <View className='user-name ellipsis'>{cardInfo.receiveUserNickname || '****'}</View>
                </View>
                <View className='status'>已领取</View>
              </View>
            </View>
            <Button onClick={this.onComplete} className='btn-active'>
              完成
            </Button>
          </Block>
        )}
      </View>
    );
  }
}

export default send;
