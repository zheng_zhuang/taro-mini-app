import { $getRouter } from 'wk-taro-platform';
import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Button, Block } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../../wxat-common/utils/money.wxs.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import template from '../../../../../wxat-common/utils/template.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import giftApi from '../gift-api';
import Error from '@/wxat-common/components/error/error';

import './index.scss';

class actived extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    tmpStyle: {},
    cardInfo: null,
    isSelf: false,
    error: false,
  };

  userCardNo: string; /*请尽快迁移为 componentDidMount 或 constructor*/
  /**
   * 生命周期函数--监听页面加载
   */ UNSAFE_componentWillMount() {
    const options = this.$router.params || {};
    this.userCardNo = options.cardNo;
    this.setState({
      isSelf: options.isSelf === 'true',
    });

    this.getTemplateStyle();
    if (!!options.cardNo) this.apiCardDetail();
  }

  onComplete() {
    wxApi.$navigateTo({
      url: pageLinkEnum.marketingPkg.giftCard.mine.index,
    });
  }
  onGotoHome = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };
  onGotoCard = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/gift-card/bg-list/index',
    });
  };

  async apiCardDetail() {
    this.setState({ error: false });
    const cardInfo = await giftApi.apiCardDetail(this.userCardNo);
    if (!cardInfo) {
      this.setState({ error: true });
    } else {
      this.setState({ cardInfo });
    }
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();

    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { cardInfo, tmpStyle, isSelf, error } = this.state as Record<string, any>;

    return (
      <View data-scoped='wk-pga-Actived' className='wk-pga-Actived actived'>
        {!!(!!cardInfo && cardInfo.cardItemName) && (
          <Block>
            <View className='top'>
              <Image src={cardInfo.cardThemeImage} className='banner' mode='aspectFill'></Image>
              <View className='card-info'>
                <View className='card-name limit-line line-2'>{cardInfo.cardItemName}</View>
                <View className='card-con' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  {'面值：' + filters.moneyFilter(cardInfo.usableAmount, true) + '元'}
                </View>
              </View>
            </View>
            <View className='middle'>
              <View className='tips-title' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                已激活绑定
              </View>
              <View className='tips-text'>可在商城购物抵扣现金</View>
            </View>
            <View className='btn-box'>
              <Button
                className='btn btn-shoping'
                style={_safe_style_('background:' + tmpStyle.btnColor)}
                onClick={this.onGotoHome}
              >
                去购物
              </Button>
              {!isSelf && (
                <Button className='btn btn-rebate' onClick={this.onGotoCard}>
                  回赠礼品卡
                </Button>
              )}
            </View>
          </Block>
        )}

        {!!error && <Error></Error>}
      </View>
    );
  }
}

export default actived;
