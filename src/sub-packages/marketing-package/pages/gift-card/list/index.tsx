import { $getRouter } from 'wk-taro-platform';
import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image, Button, Text,ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import login from '@/wxat-common/x-login/index.js';
import constants from '@/wxat-common/constants/index.js';
import pay from '@/wxat-common/utils/pay.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg.js';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum.js';

import Empty from '@/wxat-common/components/empty/empty';
import Error from '@/wxat-common/components/error/error';
import filters from '@/wxat-common/utils/money.wxs';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';
import { ResList } from '@/wxat-common/api/types';

import './index.scss';
import screen from '@/wxat-common/utils/screen';

const CARD_TYPE = constants.card.type;
const PAY_CHANNEL = constants.order.payChannel;

const loadMoreStatus = constants.order.loadMoreStatus;
const PAGE_SIZE = 10;

export interface Card {
  appId: number;
  buyCount: number;
  buyLimit: number;
  canModify: number;
  cardExplain: null;
  cardReturnAmount: number;
  cardSalesVolume: number;
  chargeAmount: null;
  createTime: number;
  giftAmount: null;
  id: number;
  isShelf: number;
  itemCardServerList: null;
  itemNo: string;
  itemOrder: number;
  labelPrice: number;
  name: string;
  salePrice: number;
  special: null;
  status: number;
  style: null;
  styleUrl: null;
  type: number;
  typeDesc: string;
  updateTime: number;
  usableAmount: number;
  validity: number;
  validityType: number;

  count: number;
}

class GiftCard extends React.Component {
  $router = $getRouter();
  state = {
    cardList: [] as Card[],
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
    bg: null,
    total: 0,
    totalPrice: 0,
    tmpStyle: {},
  };

  constructor(props) {
    super(props);
  } /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    const option = this.$router.params;
    const bg = option.bg ? JSON.parse(decodeURIComponent(option.bg)) : {};
    wxApi.setNavigationBarTitle({
      title: bg.name,
    });

    this.state.bg = bg;
    this.getTemplateStyle();
  }

  onPullDownRefresh() {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.listCardList(true);
      }
    );
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatus.LOADING,
        },

        () => {
          this.listCardList();
        }
      );
    }
  }

  onRetryLoadMore() {
    this.setState(
      {
        loadMoreStatus: loadMoreStatus.LOADING,
      },

      () => {
        this.listCardList();
      }
    );
  }

  listCardList = async (isRefresh?: boolean) => {
    this.setState({ error: false, loadMoreStatus: loadMoreStatus.LOADING });
    try {
      await login.login();
      const res = await wxApi.request<ResList<Card>>({
        url: api.card.list,
        loading: true,
        data: {
          pageNo: this.state.pageNo,
          pageSize: PAGE_SIZE,
          status: 1,
          isShelf: 1,
          type: 14,
        },
      });

      const list = (res.data || NOOP_ARRAY).map((item) => {
        item.count = 0;
        return item;
      });

      const hasMore = list.length === PAGE_SIZE;

      this.setState({
        cardList: isRefresh ? list : this.state.cardList.concat(list),
        hasMore,
        pageNo: this.state.pageNo + 1,
        loadMoreStatus: loadMoreStatus.HIDE,
      });
    } catch (err) {
      this.setState({
        error: true,
        loadMoreStatus: loadMoreStatus.ERROR,
      });
    } finally {
      if (isRefresh) {
        wxApi.stopPullDownRefresh();
      }
    }
  };

  getBuyCard() {
    const bgId = this.state.bg.id;
    return this.state.cardList
      .filter((item) => item.count > 0)
      .map((card) => {
        return {
          itemCount: card.count,
          itemNo: card.itemNo,
          type: CARD_TYPE.gift,
          giftCardThemeId: bgId,
        };
      });
  }

  handleBuyNow = (e) => {
    if (!authHelper.checkAuth(true, false)) {
      return;
    }

    const list = this.getBuyCard();
    if (list.length === 0) {
      wxApi.showToast({
        title: '请选择要购买的礼品卡。',
        icon: 'none',
      });

      return;
    }
    const orderInfo = {
      itemDTOList: list,
    };

    // 礼品卡的订阅消息
    const Ids = [subscribeEnum.PAY_SUCCESS.value]; // subscribeEnum.GIFT_CARD.value,
    // 授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      // report.clickBuy(goodsDetail.itemNo);ACCOUNT  WX_PAY
      pay.handleBuyNow(orderInfo, PAY_CHANNEL.WX_PAY, {
        msg: '正在支付中.....',
        payInfo: this._payInfo,
        success(res, payChannel) {},
        wxPaySuccess(res) {
          // 微信支付成功
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/gift-card/mine/index',
          });
        },
        fail(error) {
          console.log(error);
        },
        wxPayFail(fail) {
          wxApi.$navigateTo({
            url: '/wxat-common/pages/tabbar-order/index',
          });
        },
      });
    });
  };

  onReduce = (e) => {
    const id = e.currentTarget.dataset.id;
    const card = this.state.cardList.find((cardItem) => cardItem.id === id);
    if (!card || card.count === 0) {
      return;
    }
    card.count -= 1;
    this.setState({ cardList: this.state.cardList }, () => {
      this.calculate();
    });
  };

  onAdd = (e) => {
    const id = e.currentTarget.dataset.id;
    const card = this.state.cardList.find((cardItem) => cardItem.id === id);
    if (!card) {
      return;
    }
    // fixme card.buyLimit === 0时，表示不限制购买
    if (card.buyLimit && card.buyCount >= card.buyLimit) {
      return;
    }
    if (card.buyLimit && card.count >= card.buyLimit - card.buyCount) {
      wxApi.showToast({
        title: '已到达最大数量。',
        icon: 'none',
      });

      return;
    }

    card.count += 1;
    this.setState({ cardList: this.state.cardList }, () => {
      this.calculate();
    });
  };

  calculate() {
    let count = 0;
    let price = 0;
    this.state.cardList.forEach((card) => {
      if (card.count <= 0) {
        return;
      }
      count += card.count;
      price += card.count * card.salePrice;
    });
    this.setState({
      total: count,
      totalPrice: price,
    });
  }

  // //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  componentDidShow() {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.listCardList(true);
      }
    );
  }

  render() {
    const { bg, cardList, error, total, totalPrice, tmpStyle, loadMoreStatus } = this.state;
    const isFullScreen = screen.isFullScreenPhone;
    return (
      <ScrollView
        data-scoped='wk-pgl-List'
        className={'wk-pgl-List ' + ('serve-list ' + (isFullScreen ? 'fix-full-screen' : ''))}
      >
        <Image className='banner' src={bg.bgImg} mode='aspectFill'></Image>
        {!!(!!cardList && cardList.length === 0 && !error) && (
          <Empty
            icon="https://htrip-static.ctlife.tv/wk/empty.png"
            iconClass='empty-icon'
            message='敬请期待...'
          ></Empty>
        )}

        {!!error && <Error></Error>}
        {!!(cardList && cardList.length) && (
          <View className='card-list'>
            <View className='card-list-title'>选择礼品卡</View>
            <View className='card-list-container'>
              {cardList.map((item) => {
                return (
                  <View key={item.id} className={'card-item ' + (item.count > 0 ? 'card-active' : '')}>
                    {item.count > 0 && <View className='card-count'>{item.count}</View>}
                    <View className='card-info'>
                      <View className='card-name'>{'名称：' + item.name}</View>
                      <View className='card-price'>
                        {'价值：' + filters.moneyFilter(item.usableAmount, true) + '元'}
                      </View>
                      <View className='card-sale-price'>
                        <Text className='sale-price'>{filters.moneyFilter(item.salePrice, true)}</Text>元
                      </View>
                    </View>
                    <View className='operator'>
                      {item.count > 0 && (
                        <View className='btn-box' onClick={_fixme_with_dataset_(this.onReduce, { id: item.id })}>
                          <Image className='btn-reduce' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-less.png"></Image>
                        </View>
                      )}

                      <View className='btn-box' onClick={_fixme_with_dataset_(this.onAdd, { id: item.id })}>
                        {item.buyLimit && item.buyCount >= item.buyLimit ? (
                          <View className='sell-end'>售罄</View>
                        ) : (
                          <Image className='btn-add' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/icon-add.png"></Image>
                        )}
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        )}

        <View className={'pay-box ' + (isFullScreen ? 'fix-full-screen' : '')}>
          {total > 0 && (
            <View className='left-price'>
              <View className='total-num'>{'共' + total + '份'}</View>
              <View className='total'>{'合计：¥ ' + filters.moneyFilter(totalPrice, true)}</View>
            </View>
          )}

          <Button
            className='to-pay-btn'
            onClick={this.handleBuyNow}
            style={_safe_style_('background:' + tmpStyle.btnColor)}
          >
            立即购买
          </Button>
        </View>
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>
      </ScrollView>
    );
  }
}

export default GiftCard;
