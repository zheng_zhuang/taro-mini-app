import React from 'react'; // @externalClassesConvered(Empty)
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../../../../wxat-common/api/index.js';
import wxApi from '../../../../../wxat-common/utils/wxApi';
import login from '../../../../../wxat-common/x-login/index.js';
import constants from '../../../../../wxat-common/constants/index.js';
import pay from '../../../../../wxat-common/utils/pay.js';
import template from '../../../../../wxat-common/utils/template.js';
import LoadMore from '../../../../../wxat-common/components/load-more/load-more';
import Error from '../../../../../wxat-common/components/error/error';
import Empty from '../../../../../wxat-common/components/empty/empty';
import './index.scss';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
const CARD_TYPE = constants.card.type;
const PAY_CHANNEL = constants.order.payChannel;

// const app = Taro.getApp()
const loadMoreStatus = constants.order.loadMoreStatus;

class Bglist extends React.Component {
  state = {
    customKey: null,
    pageConfig: null,
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.LOADING,
    bgList: [],
    _payInfo: null,
  };

  componentDidMount() {
    this.getTemplateStyle();
    this.listBgList();
  }

  onPullDownRefresh() {
    this.setState(
      {
        bgList: [],
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.listBgList();
      }
    );
  }

  onReachBottom() {
    if (this.state.loadMoreStatus === 1) return false
    if (this.state.hasMore) {
      this.setState(
        {
          pageNo: this.state.pageNo + 1,
        },

        () => {
          this.listBgList();
        }
      );
    }
  }

  onRetryLoadMore = () => {
    this.listBgList()
  };

  listBgList() {
    this.setState(
      {
        error: false,
        loadMoreStatus: loadMoreStatus.LOADING
      },
      () => {
        login.login().then(() => {
          wxApi
            .request({
              url: api.giftCard.theme,
              data: {
                pageNo: this.state.pageNo,
                pageSize: 10,
              },
            })
            .then((res) => {
              const data = this.state.bgList.concat(res.data || [])

              if (data.length === res.totalCount && res.totalCount) {
                this.setState(
                  {
                    hasMore: false,
                    loadMoreStatus: loadMoreStatus.BASELINE
                  }
                )
              } else {
                this.setState(
                  {
                    hasMore: true,
                    loadMoreStatus: loadMoreStatus.HIDE
                  }
                )
              }
              this.setState({
                bgList: data
              });

              if (data.length) {
                wxApi.setNavigationBarTitle({
                  title: data[0].name
                });
              }
            })
            .catch(() => {
              if (!this.state.bgList.length) {
                this.setState(
                  {
                    error: true,
                    loadMoreStatus: loadMoreStatus.HIDE
                  }
                )
              } else {
                this.setState(
                  {
                    loadMoreStatus: loadMoreStatus.ERROR
                  }
                )
              }
            })
        })
      }
    )
  }

  getBuyCard() {
    return this.state.bgList.map((card) => {
      return {
        itemCount: 1,
        itemNo: card.itemNo,
        type: 14,
        giftCardThemeId: 24,
      };
    });
  }
  handleBuyNow(e) {
    console.log(this.getBuyCard());
    const orderInfo = {
      itemDTOList: this.getBuyCard(),
    };

    const _this = this;
    //report.clickBuy(goodsDetail.itemNo);ACCOUNT  WX_PAY
    pay.handleBuyNow(orderInfo, PAY_CHANNEL.ACCOUNT, {
      msg: '正在支付中.....',
      payInfo: this.state._payInfo,
      success(res, payChannel) {
        if (payChannel != 1) {
          //1代表微信支付
          //_this.state._payInfo = res
          this.setState({
            _payInfo: res,
          });

          wxApi.navigateBack({});
        }
      },
      wxPaySuccess(res) {
        // 微信支付成功
        // _this.state._payInfo = null
        this.setState({
          _payInfo: null,
        });

        wxApi.navigateBack({});
      },
      fail(error) {
        console.log(error);
      },
      wxPayFail(fail) {
        console.log(fail);
      },
    });
  }
  onGotoCard = (e) => {
    const id = e.currentTarget.dataset.id;
    const bg = this.state.bgList.find((item) => item.id === id);
    console.log(JSON.stringify(bg));
    if (bg) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/gift-card/list/index',
        data: {
          bg: JSON.stringify(bg),
        },
      });
    }
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
  }

  render() {
    const { error, bgList, loadMoreStatus } = this.state;
    return (
      <View data-scoped='wk-pgb-BgList' className='wk-pgb-BgList serve-list'>
        {
          !error && !bgList.length && !loadMoreStatus && (
            <Empty
              icon="https://htrip-static.ctlife.tv/wk/empty.png"
              iconClass='empty-icon'
              message='敬请期待...'
            ></Empty>
          )
        }
        {error && <Error></Error>}
        {!!bgList.length && (
          <View className='card-list'>
            {bgList.map((item, index) => {
              return (
                <View
                  key={item.id}
                  className={index === 0 ? 'default-item' : 'card-item'}
                  onClick={_fixme_with_dataset_(this.onGotoCard, { id: item.id })}
                >
                  {index === 0 ? (
                    <Block>
                      <Image className='default-theme' mode='aspectFill' src={item.bgImg}></Image>
                    </Block>
                  ) : (
                    <Block>
                      <Image className='theme-img' mode='aspectFill' src={item.bgImg}></Image>
                      <View className='theme-name'>{item.name}</View>
                    </Block>
                  )}
                </View>
              );
            })}
          </View>
        )}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        <HomeActivityDialog showPage="wxat-common/pages/gift-card/bg-list/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default Bglist;
