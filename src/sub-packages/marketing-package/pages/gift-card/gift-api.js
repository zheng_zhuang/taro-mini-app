import login from '@/wxat-common/x-login/index';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';

/**
 * Created by a123 on 2020/9/7.
 * @author trumpli<李志伟>
 */

export default {
  /**
   * 礼品卡详情
   * @param userGiftCardNo
   * @return {Promise<*>}
   */
  async apiCardDetail(userGiftCardNo) {
    try {
      await login.login();
      const { data } = await wxApi.request({ url: api.giftCard.card_detail, loading: true, data: { userGiftCardNo } });
      const cardInfo = data || { cardItemName: '' };
      if (!!cardInfo.cardItemName) wxApi.setNavigationBarTitle({ title: cardInfo.cardItemName });
      return cardInfo;
    } catch (xe) {
      console.log('apiCardDetail xe', xe);
      return null;
    }
  },
};
