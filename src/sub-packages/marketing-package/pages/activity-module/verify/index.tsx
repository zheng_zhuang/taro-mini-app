import React, {FC, useMemo, useState} from 'react';
import Taro from '@tarojs/taro';
import {Block, Image, Input, Text, View} from '@tarojs/components';
import '@/wxat-common/utils/platform';
import debounce from 'lodash/debounce';
import wxApi from '@/wxat-common/utils/wxApi';
import style from './index.module.scss';
import api from '@/wxat-common/api/index';
import DateRange from '@/wxat-common/components/date-range/index';
import date from '@/wxat-common/utils/date';
import useList from '@/hooks/useList';
import Empty from '@/wxat-common/components/empty/empty';
import DebounceButton from '@/wxat-common/components/DebounceButton';

const tabs = [
  { id: 0, name: '核销工具' },
  { id: 1, name: '核销记录' },
];

const defaultRange = () => {
  const s = new Date();
  s.setMonth(s.getMonth() - 1);
  const e = new Date();
  return [s, e];
};

interface Record {
  activityTitle: string;
  customerName: string;
  customerPhone: string;
  orderNo: string;
  signTime: string;
  verifyBy: string;
}

/**
 * 核销工具
 */
let Verify: FC = () => {
  const [activeTab, setActiveTab] = useState(0);
  const [code, setCode] = useState('');
  const [dateRange, setDateRange] = useState(defaultRange);
  const [loading, setLoading] = useState(false);
  const [total, setTotal] = useState(0);
  const { list, reload, empty } = useList<Record>({
    query: async (page, pageSize) => {
      const params = {
        startTime: date.format(dateRange[0], 'yyyy-MM-dd') + ' 00:00:00',
        endTime: date.format(dateRange[1], 'yyyy-MM-dd') + ' 23:59:59',
        pageNo: page,
        pageSize: pageSize,
      };

      const { data, totalCount } = await wxApi.request({
        url: api.activeModule.verifyRecords,
        method: 'POST',
        data: params,
        loading: false,
      });

      setTotal(totalCount);
      return data || [];
    },
  });

  const debounceReload = useMemo(() => debounce(reload, 1000), []);

  const checkTab = (item: any) => {
    setActiveTab(item.id);
  };

  const handleCodeInput = (e) => {
    setCode(e.detail.value);
  };

  const verify = async (code) => {
    if (loading) {
      return;
    }

    try {
      setLoading(true);
      const params = { verificationCode: code };
      await wxApi.request({
        url: api.activeModule.verify,
        method: 'POST',
        data: params,
        loading: true,
      });

      wxApi.showModal({
        title: '提示',
        content: '核销成功',
        showCancel: false,
      });

      debounceReload();
    } finally {
      setLoading(false);
    }
  };

  const scanCode = async () => {
    const { result } = await wxApi.scanCode({
      scanType: ['qrCode'],
    });

    verify(result);
  };

  const handleVerifyCode = async () => {
    if (code.trim() === '') {
      wxApi.showToast({
        title: '请输入核销码',
        duration: 200,
        icon: 'none',
      });

      return;
    }

    await verify(code);
    setCode('');
  };

  const handleDateChange = (value) => {
    setDateRange(value);

    debounceReload();
  };

  return (
    <View className={style.page}>
      <View className={style.tabBox}>
        {tabs.map((item, index) => {
          return (
            <View
              className={item.id === activeTab ? style.tabStyleActived : style.tabStyleNoActived}
              key={index}
              onClick={() => checkTab(item)}
            >
              <Text className={style.name}>{item.name}</Text>
              {item.id === activeTab && (
                <View className={style.activeLine} style={item.id === activeTab ? 'background:#FF8400' : ''}></View>
              )}
            </View>
          );
        })}
      </View>
      <View className={style.tabs}>
        {activeTab === 0 && (
          <View className={style.toolBox}>
            <View className={style.item}>
              <View className={style.mainTit}>
                <Image
                  className={style.borderIcon}
                  src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png"
                />

                <Text>核销方法</Text>
              </View>
              <View className={style.itemBody}>
                <View className={style.title}>方法一：扫码核销</View>
                <View className={style.imgBox} onClick={scanCode}>
                  <Image
                    className={style.takeCode}
                    src={require('@/sub-packages/marketing-package/images/takecode.png')}
                  />
                </View>
                <View className={style.desc} onClick={scanCode}>
                  点击扫描二维码
                </View>
                <View className={style.title}>方法二：输入劵码核销</View>
                <View className={style.row}>
                  <Input
                    type='text'
                    className={style.input}
                    placeholder='请输入核销码'
                    value={code}
                    onInput={handleCodeInput}
                  ></Input>
                  <DebounceButton className={style.checkBtn} onClick={handleVerifyCode}>
                    确定
                  </DebounceButton>
                </View>
              </View>
            </View>
            <View className={style.item}>
              <View className={style.mainTit}>
                <Image
                  className={style.borderIcon}
                  src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png"
                />

                <Text>如何核销劵码</Text>
              </View>

              <View className={style.itemBody}>
                <View className={style.descTxt}>
                  引导用户进入小程序，
                  <Text className='bold'>
                    选择我的{'>'}我的活动{'>'}点击查看电子票进入电子票详情
                  </Text>
                  ，即可查看电子二维码。
                </View>
                <View className={style.itemText}>1.点击扫描二维码，扫描用户电子二维码完成核销</View>
                <View className={style.itemText}>2.点击输入劵码，输入用户数字劵码，完成核销</View>
              </View>
            </View>
          </View>
        )}

        {activeTab === 1 && (
          <Block>
            <View>
              <DateRange value={dateRange} onChange={handleDateChange} />
            </View>
            <View className={style.recordBox}>
              <View className={style.mainTit}>
                <Image
                  className={style.borderIcon}
                  src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png"
                />

                <Text>核销记录</Text>
                <Text className={style.num}>共计 {total} 张</Text>
              </View>
              <View className={style.recordList}>
                {empty ? (
                  <Empty></Empty>
                ) : (
                  list.map((i) => {
                    return (
                      <View className={style.record} key={i.orderNo}>
                        <View className={style.date}>{i.signTime}</View>
                        <View className={style.recordItem}>
                          <View className={style.lable}>活动劵信息：</View>
                          <View className={style.text}>{i.activityTitle}</View>
                        </View>
                        <View className={style.recordItem}>
                          <View className={style.lable}>用户信息：</View>
                          <View className={style.text}>
                            {i.customerName} {i.customerPhone}
                          </View>
                        </View>
                        <View className={style.recordItem}>
                          <View className={style.lable}>操作账号：</View>
                          <View className={style.text}>{i.verifyBy}</View>
                        </View>
                      </View>
                    );
                  })
                )}
              </View>
            </View>
          </Block>
        )}
      </View>
    </View>
  );
};

export default Verify;
