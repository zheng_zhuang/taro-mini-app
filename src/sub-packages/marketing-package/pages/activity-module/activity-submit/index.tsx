import React, {FC, useEffect, useState} from 'react';
import Taro, {useRouter} from '@tarojs/taro';
import {Block, Image, Input, Text, View} from '@tarojs/components';
import '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import {useSelector} from 'react-redux';
import {Activity, ActivityItem} from '@/types/activetype';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum';

import style from './index.module.scss';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import DebounceButton from '@/wxat-common/components/DebounceButton/index';

type FollowUser = {
  accompanName: string;
  accompanPhone: string;
};

/**
 * 活动报名确认提交页面
 * @param props
 */
let ActivitySubmit: FC = () => {
  const router = useRouter();

  const userInfo = useSelector((state) => state.base.userInfo);

  // 报名人
  const [activityData, setActivityData] = useState<Activity>();
  // 已选择活动项目
  const [activityItem, setActivityItem] = useState<ActivityItem>();
  const [userList, setUserList] = useState<any[]>([]);
  // 跟随人
  const [followUserList, setFollowUserList] = useState<FollowUser[]>([]);
  const [, setUserLimit] = useState<number>(1);

  // 跟随人限制
  const [followUserLimit, setFollowUserLimit] = useState(1);

  // 路由数据
  const [routerInfo, setRouterInfo] = useState<any>({});

  useEffect(() => {
    // 缓存数据
    setRouterInfo(router.params);

    const { num: _num, itemId } = router.params;
    const num = parseInt(_num, 10);
    const data: Activity = protectedMailBox.read('detail');

    let tempList = [...JSON.parse(data.activityJsonForm)];

    const itemInfo = data.itemDTOList.find((i) => String(i.id) === itemId);

    const userList: any[] = [];

    tempList.forEach((ele) => {
      ele.value = '';
    });

    for (let i = 0; i < num; i++) {
      userList.push([...tempList]);
    }

    setActivityData(data);
    setActivityItem(itemInfo);

    setUserList(userList);

    setUserLimit(num);
    setFollowUserLimit(data.followPersonMax);
  }, []);

  const addFollower = () => {
    if (followUserList.length >= followUserLimit) {
      return;
    }
    setFollowUserList(
      followUserList.concat({
        accompanName: '',
        accompanPhone: '',
      })
    );
  };

  const removeFollower = (index) => {
    const list = [...followUserList];
    list.splice(index, 1);
    setFollowUserList(list);
  };

  // submit 时, 非空校验
  const isCheck = () => {
    let flag = true;

    for (let i = 0; i < userList.length; i++) {
      const child = userList[i];
      let parIndex = i;
      for (let j = 0; j < child.length; j++) {
        const element = child[j];
        if (element.isRequired && element.value === '') {
          flag = false;
          let name = element.name;
          wxApi.showToast({ title: `报名人${parIndex + 1}, ${name}不能为空`, icon: 'none' });
          break;
        }
      }
      if (!flag) break;
    }

    return flag;
  };

  // 组装数据
  const assemblyData = () => {
    let tempUserList: any[] = [];

    userList.forEach((item) => {
      let trr = item.map((childItem) => {
        let key = childItem.key;
        let value = childItem.value;

        return {
          [key]: value,
          name: childItem.name,
        };
      });
      let i = {
        customerJsonRecord: JSON.stringify(trr),
      };

      tempUserList.push(i);
    });

    return tempUserList;
  };

  const onSubmit = () => {
    // 非空校验
    if (!isCheck()) return;

    // 组装数据
    const tempUserList = assemblyData();

    const follow = followUserList.filter((item) => item.accompanName !== '' || item.accompanPhone !== '');

    // 先生成订单,
    wxApi
      .request({
        url: api.activeModule.generateOrder,
        method: 'POST',
        data: {
          sku: router.params.sku,
          totalPrice: router.params.price,
          buyNum: router.params.num,
          personDTOList: tempUserList,
          accompanDTOList: follow,
          storeId: userInfo.storeId,
        },

        loading: true,
        // TODO: 正式环境移除
        checkSession: false,
      })
      .then((res) => {
        const dataInfo = res.data;

        // 订阅消息模板
        const ids: number[] = [subscribeEnum.PAY_SUCCESS.value];

        // 先授权订阅消息
        subscribeMsg.sendMessage(ids).then(() => {
          const target = 'sub-packages/marketing-package/pages/activity-module/activity-submit-success/index';
          protectedMailBox.send(target, 'detail', activityData);

          // 免费票种不需要调用微信支付, 直接跳转报名成功
          // ticketType 1 免费; 2 付费票; 3 保证金;

          if (routerInfo.ticketType !== '1') {
            let options = {
              package: `prepay_id=${dataInfo.prepayId}`,
              timeStamp: dataInfo.timeStamp + '',
              nonceStr: dataInfo.nonceStr,
              paySign: dataInfo.paySign,
              signType: dataInfo.signType,
              success: (e) => {
                wxApi
                  .request({
                    url: api.activeModule.payment,
                    method: 'GET',
                    data: {
                      orderNo: dataInfo.orderNo,
                    },

                    loading: {
                      title: '正在生成订单...',
                    },

                    // TODO: 正式环境移除
                    checkSession: false,
                  })
                  .then((ksd) => {
                    wxApi.$redirectTo({
                      url: target,
                      data: {
                        id: activityData!.id,
                      },
                    });
                  });
              },
              fail: (e) => {
                console.log('支付失败', e);
                const url = 'sub-packages/marketing-package/pages/activity-module/my-activities/index';
                wxApi.$redirectTo({ url });
              },
            };

            // 微信支付
            wxApi.requestPayment(options);
          } else {
            const target = 'sub-packages/marketing-package/pages/activity-module/activity-submit-success/index';

            wxApi.$redirectTo({
              url: target,
              data: {
                id: activityData!.id,
              },
            });
          }
        });
      });
  };

  const setUserValue = (index, childIdx, item, key, value) => {
    let list = [...userList];
    list[index][childIdx] = { ...list[index][childIdx], [key]: value };

    setUserList(list);
  };

  const setFollowerValue = (index, key, value) => {
    const list = [...followUserList];
    list[index] = { ...list[index], [key]: value };
    setFollowUserList(list);
  };

  if (activityData == null || activityItem == null) {
    return null;
  }

  return (
    <View className={style.sumbitBox}>
      <View className={style.sumbitDetal}>
        <Image className={style.activitiyImg} mode='aspectFill' src={activityData.imgUrl} />
        <View className={style.title}>
          <View className={style.upTit}>{activityData.activityTitle}</View>
          <View className={style.downTit}>
            {activityItem.activityStartTime} 至 {activityItem.activityEndTime}
          </View>
        </View>
      </View>
      <View>
        <View className={style.listBoxs}>
          {userList.map((item, index) => {
            return (
              // eslint-disable-next-line react/jsx-key
              <View key={index} className={style.main}>
                <View className={style.mainTit}>
                  <Image
                    className={style.borderIcon}
                    src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png"
                  />
                  报名人{index + 1}
                </View>
                <View className={style.mainForm}>
                  <Block>
                    {item.map((child, childIdx) => {
                      return (
                        <View key={childIdx} className={style.tab}>
                          <View className={style.tabLabel}>
                            {child.isRequired && <Text className={style.must}>*</Text>}
                            {child.name}
                          </View>
                          <Input
                            className={style.tabInput}
                            placeholder={'请输入客户' + child.name}
                            value={child.value}
                            onInput={(e) => {
                              setUserValue(index, childIdx, child, 'value', e.detail.value);
                            }}
                          ></Input>
                        </View>
                      );
                    })}
                  </Block>
                </View>
              </View>
            );
          })}
        </View>
        {!!followUserLimit && <View className={style.peopleNum}>最多添加 {followUserLimit}人</View>}
        <View className={style.listBox}>
          {followUserList.map((item, index) => {
            return (
              <View className={style.main} key={index}>
                <View className={style.mainTit}>
                  <Image
                    className={style.borderIcon}
                    src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png"
                  />

                  <Text>随行人{index + 1}</Text>
                  <View className={style.delBtn} onClick={() => removeFollower(index)}>
                    —
                  </View>
                </View>
                <View className={style.mainForm}>
                  <View className={style.tab}>
                    <View className={style.tabLabel}>随行人员</View>
                    <Input
                      className={style.tabInput}
                      placeholder='请输入随行人员姓名'
                      value={item.accompanName}
                      onInput={(e) => setFollowerValue(index, 'accompanName', e.detail.value)}
                    ></Input>
                  </View>
                  <View className={style.tab}>
                    <View className={style.tabLabel}>手机号码</View>
                    <Input
                      type='number'
                      maxLength={11}
                      className={style.tabInput}
                      placeholder='请输入随行人员手机号'
                      value={item.accompanPhone}
                      onInput={(e) => setFollowerValue(index, 'accompanPhone', e.detail.value)}
                    ></Input>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
        {followUserList.length < followUserLimit && (
          <View className={`${style.addBtn} ${style.main}`} onClick={addFollower}>
            <Image className={style.addIcon} src="https://htrip-static.ctlife.tv/wk/marketing-base/add.png" />
            <Text className={style.addText}>添加随行人员</Text>
          </View>
        )}
      </View>
      <View className={style.footer1}>
        <View className={style.left}>
          <Text className={style.leftLabel}>合计：</Text>
          <Text className={style.slight}>¥</Text>
          <Text className={style.leftPrice}>{routerInfo.price / 100}</Text>
        </View>
        {routerInfo.ticketType === '1' ? (
          <View className={style.footer}>
            <DebounceButton className={style.submitBtn} onClick={onSubmit}>
              提交报名
            </DebounceButton>
          </View>
        ) : (
          <View className={style.right}>
            <Text onClick={onSubmit} className={style.rightBtn}>
              支付
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default ActivitySubmit;
