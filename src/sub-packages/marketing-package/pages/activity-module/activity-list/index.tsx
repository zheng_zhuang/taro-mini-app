import {$getRouter} from 'wk-taro-platform';
import React from 'react';
import {Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import {connect} from 'react-redux';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import {ActivityStatus, ActivityStatusBgColor, ActivityStatusName} from '@/types/activetype';

import style from './index.module.scss';
import {activityDateFormatSimple} from '@/wxat-common/utils/activityFormat';

interface DataItems {
  activityNo: string;
  activityTitle: string;
  id: number;
  imgUrl: string;
  status: ActivityStatus;
  activityAddress: string;
  itemDTOList: any[];
  saleNum: number;
  date: string;
  ticketPrice: number;
  activityAddressMark: number;
}

const mapStateToProps = (state) => {
  return {
    currentStore: state.base.currentStore,
  };
};

/**
 * 活动列表
 */

export default
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ActivityList extends React.Component {
  $router = $getRouter();
  hasMore = true;
  state: {
    loading: boolean;
    activityList: DataItems[];
    pageNo: number;
    pageSize: number;
  } = {
    loading: false,
    pageNo: 0,
    pageSize: 15,
    activityList: [],
  };

  componentDidMount() {
    wxApi.setNavigationBarTitle({
      title: this.config.navigationBarTitleText,
    });

    this.getActivityList();
  }

  onReachBottom() {
    this.getActivityList();
  }
  formatNumber = (n) => {
    n = n.toString();
    return n[1] ? n : '0' + n;
  };
  formatTime(date: string) {
    if (date) {
      const time = new Date(date.substring(0, 10));
      const month = time.getMonth() + 1;
      const day = time.getDate();
      return [month, day].map(this.formatNumber).join('/');
    }
  }
  async getActivityList() {
    if (!this.hasMore || this.state.loading) {
      return;
    }
    try {
      this.setState({ loading: true });
      const params: any = {
        pageNo: this.state.pageNo + 1,
        pageSize: this.state.pageSize,
      };

      if (this.$router.params.type) {
        params.activityType = parseInt(this.$router.params.type);
      }

      const { data } = await wxApi.request({
        url: api.activeModule.getActivityList,
        loading: true,
        data: params,
        method: 'POST',
        checkSession: false,
      });

      if (data && data.length < this.state.pageSize) {
        this.hasMore = false;
      }
      const payload = data.map((child) => {
        return {
          ...child,
          date: activityDateFormatSimple(child.itemDTOList),
        };
      });
      this.setState({
        pageNo: this.state.pageNo + 1,
        activityList: this.state.activityList.concat(payload),
      });
    } finally {
      this.setState({ loading: false });
    }
  }

  toDetais(item) {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/activity-module/activity-detail/index',
      data: { id: item.id },
    });
  }

  render() {
    const { activityList } = this.state;
    const { currentStore }: any = this.props;

    return (
      <View className={style.container}>
        {activityList.map((item, index) => (
          <View
            key={item.id}
            className={style.item}
            onClick={() => {
              this.toDetais(item);
            }}
          >
            <View className={style.containerBox}>
              <Image className={style.activitiyImg} mode='aspectFill' src={item.imgUrl} />
            </View>
            <View className={style.paddingStyle}>
              <View className={style.activitiyTitle}>
                <View style={{ background: ActivityStatusBgColor[item.status] }} className={style.activitiyStart}>
                  {ActivityStatusName[item.status]}
                </View>
                <View className={style.title}>{item.activityTitle}</View>
              </View>
              <View className={style.activitiyDetail}>
                <View className={style.activitiyAddress}>
                  <View className={style.actLabel}>活动地址：</View>
                  {/* activityAddressMark 1 取当前门店地址 */}
                  {item.activityAddressMark === 1 ? (
                    <View className={style.actValue}>{currentStore.address}</View>
                  ) : (
                    <View className={style.actValue}>{item.activityAddress}</View>
                  )}
                </View>
                <View className={style.activitiyAddress}>
                  <View className={style.actLabel}>活动时间：</View>
                  <View className={style.actValue}>{item.date}</View>
                </View>
              </View>
              <View className={style.activitiyPriceBox}>
                <View className={style.activitiyPrice}>
                  {item.ticketPrice ? '¥' : ''}
                  <Text className={style.priceText}>{item.ticketPrice ? item.ticketPrice / 100 : '免费'}</Text>
                </View>
                <View className={style.activitiyNum}>
                  <Text className={style.activitiyNumText}>{item.saleNum || 0} 人已报名</Text>
                </View>
              </View>
            </View>
          </View>
        ))}
      </View>
    );
  }
}
