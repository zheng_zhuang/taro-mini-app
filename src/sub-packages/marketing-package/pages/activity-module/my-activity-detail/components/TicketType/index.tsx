import React, {FC, useEffect, useState} from 'react';
import '@/wxat-common/utils/platform';
import {Image, ScrollView, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import style from './index.module.scss';
import screen from '@/wxat-common/utils/screen';
import classNames from 'classnames';
import {Detail} from '@/types/activetype';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';

// 报名详情
type ComponentProps = {
  isOpened: boolean;
  onClose: Function;
  title: string;
  payLoadList: any[];
  detail: Detail;
};

let TicketType: FC<ComponentProps> = ({ isOpened, onClose, title, payLoadList, detail }) => {
  const [actived, setActived] = useState<number>(0);
  const [itemInfo, setItemInfo] = useState<any>({});

  useEffect(() => {
    // 默认选中第一项
    setItemInfo(payLoadList[0]);
  }, [payLoadList]);

  const toRefund = () => {
    // 退款
    const target = '/sub-packages/marketing-package/pages/activity-module/my-refund/index';

    const obj = detail;
    let tempInfo = itemInfo;
    tempInfo.imgUrl = obj.imgUrl;

    // redux中存数据
    protectedMailBox.send(
      'sub-packages/marketing-package/pages/activity-module/my-refund/index',
      'ticketTypeInfo',
      tempInfo
    );

    wxApi.$navigateTo({
      url: target,
      data: {
        orderNo: obj.orderNo,
        orderItemId: itemInfo.id,
      },
    });
  };

  // 选中某一项
  const handleChange = (data, idx) => {
    // 参数 data
    setItemInfo(data);
    setActived(idx);
  };

  // payStatus: 20 已支付状态;
  const isShow = detail && detail.payStatus === 20 && payLoadList && payLoadList.length > 0;

  return (
    <View>
      <View className={isOpened ? style.flolayoutActive : style.flolayout}>
        <View
          className={style.mask}
          onClick={() => {
            onClose();
          }}
        ></View>
        <View className={style.flolayoutContainer} style={{ paddingBottom: Taro.pxTransform(screen.safeAreaBottom) }}>
          <View className={style.title}>请选择进行退款操作的票</View>

          <View className={style.itemsbox}>
            <ScrollView className='scrollview' scrollY scrollWithAnimation scrollTop={0} style={{ height: '150px' }}>
              {isShow ? (
                payLoadList.map((item, index) => {
                  return (
                    <View
                      className={classNames(style.refgcontent, actived === index ? style.refgcontentActive : '')}
                      onClick={() => {
                        handleChange(item, index);
                      }}
                    >
                      <View className={style.contentleft}>
                        <View className={style.leftTitle}>{item.ticketName}</View>
                        <View className={style.leftInfo}>
                          <Text className={style.infoText}>x 1</Text>
                          <Text>{item.customerJsonRecord[0].itemKey0}</Text>
                        </View>
                      </View>
                      <View className={style.contentright}>￥{item.price === 0 ? item.price : item.price / 100}</View>

                      {actived == index && (
                        <Image
                          className={style.radioCheckIcon}
                          src={require('@/sub-packages/marketing-package/images/checked.png')}
                        />
                      )}
                    </View>
                  );
                })
              ) : (
                <View className={style.noRefgcontent}>
                  <Text className={style.text}>无可退票种</Text>
                </View>
              )}
            </ScrollView>
          </View>
          {/* 底部按钮 */}
          <View className={style.footer}>
            <View
              onClick={() => {
                onClose();
              }}
              className={style.leftBox}
            >
              取消
            </View>
            <View
              onClick={() => {
                payLoadList && payLoadList.length > 0 ? toRefund() : onClose();
              }}
              className={style.rightBox}
            >
              确定
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

TicketType.defaultProps = {
  isOpened: false,
  onClose: () => {},
  title: '弹窗',
};

export default TicketType;
