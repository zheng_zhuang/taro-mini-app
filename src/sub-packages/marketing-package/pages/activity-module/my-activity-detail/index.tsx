import React, {FC, useEffect, useState} from 'react';
import Taro, {useDidShow, useRouter} from '@tarojs/taro';
import {Image, Text, View} from '@tarojs/components';
import '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import style from './index.module.scss';
import {Detail} from '@/types/activetype';
import {TicketStatus, TicketStatusobj} from '@/wxat-common/utils/activityModule';
// 选择票种
import TivketType from './components/TicketType/index';

/**
 * 报名详情
 * @param props
 */
let MyActivityDetail: FC = () => {
  const router = useRouter();
  const orderNo = router.params.orderNo;
  const [myActivityDetail, setMyActivityDetail] = useState<Detail>();

  // 弹窗相关数据
  const [isOpenLayout, setIsOpenLayout] = useState(false);

  useEffect(() => {
    getActivityDetail();
  }, []);

  useDidShow(() => {
    // 验证完成后, 重新刷新数据
    getActivityDetail();
  });

  const getActivityDetail = async () => {
    const { data } = await wxApi.request({
      url: api.activeModule.getMyActivityDetails,
      method: 'GET',
      data: {
        orderNo,
      },

      loading: true,
    });

    let temCustomerJsonRecord = data.activityOrderItemDTOList.map((item) => {
      let element = JSON.parse(item.customerJsonRecord);
      item.customerJsonRecord = element;

      return item;
    });
    data.payLoadList = temCustomerJsonRecord.filter((item) => [10, 60].includes(item.status));
    // 若 status: 10 待验证【已支付】 status: 60 审核已拒绝, 则可以退款
    data.activityOrderItemDTOList = temCustomerJsonRecord;
    setMyActivityDetail(data);
  };

  const toTicket = (data) => {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/activity-module/my-activity-tickets/index',
      data: { orderNo, info: data },
    });
  };

  const onShowHandle = () => {
    setIsOpenLayout(true);
  };

  if (myActivityDetail == null) {
    return null;
  }

  return (
    <View className={style.signUpBox}>
      <View className={style.header}>
        <View className={style.activityCard}>
          <Image className={style.img} mode='aspectFill' src={myActivityDetail.imgUrl}></Image>
          <View className={style.title}>{myActivityDetail.activityTitle}</View>
        </View>
        <View className={style.activityItemBox}>
          <View className={style.activityItem}>
            <View className={style.lable}>活动地址</View>
            <View className={style.text}>{myActivityDetail.activityAddress}</View>
          </View>
          <View className={style.activityItem}>
            <View className={style.lable}>活动时间</View>
            <View className={style.text}>
              {myActivityDetail.activityStartTime} 至 {myActivityDetail.activityEndTime}
            </View>
          </View>
        </View>
      </View>

      <View className={style.containerBox}>
        <View className={style.mainTit}>
          <Image className={style.borderIcon} src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png" />
          <Text className={style.label}>报名人信息</Text>
        </View>
        <View className={style.userList}>
          {myActivityDetail &&
            myActivityDetail.activityOrderItemDTOList &&
            myActivityDetail.activityOrderItemDTOList.map((item, index) => {
              return (
                <View
                  className={style.userItem}
                  onClick={() => {
                    toTicket(item);
                  }}
                  key={item.id}
                >
                  <Text className={style.label}>报名人 {index + 1}</Text>

                  {/* 和产品已确认, 这里只展示第一个字段 */}
                  <Text className={style.item}>{item.customerJsonRecord[0].itemKey0}</Text>

                  <Text className={TicketStatus[item.status] === 'AUTHENTICATED' ? style.statusCheck : style.status}>
                    {TicketStatusobj[TicketStatus[item.status]]}
                  </Text>

                  <Image
                    className={style.rightIcon}
                    src={require('@/sub-packages/marketing-package/images/icon_@2x.png')}
                  ></Image>
                </View>
              );
            })}
        </View>
      </View>
      {myActivityDetail && myActivityDetail.accompanDTOList && myActivityDetail.accompanDTOList.length > 0 && (
        <View className={style.containerBox}>
          <View className={style.mainTit}>
            <Image className={style.borderIcon} src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png" />
            <Text className={style.label}>随行人信息</Text>
          </View>
          <View className={style.userList}>
            {myActivityDetail.accompanDTOList.map((item, index) => {
              return (
                <View className={style.userItem} onClick={toTicket} key={item.id}>
                  <Text className={style.label}>随行人 {index + 1}</Text>
                  <Text className={style.item}>
                    {item.accompanName}
                    {item.accompanPhone ? `(${item.accompanPhone})` : ''}
                  </Text>
                </View>
              );
            })}
          </View>
        </View>
      )}

      <View className={style.containerBox}>
        <View className={style.mainTit}>
          <Image className={style.borderIcon} src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png" />
          <Text className={style.label}>订单信息</Text>
        </View>
        <View className={style.userList}>
          <View className={style.orderItem}>
            <Text className={style.label}>订单编号：</Text>
            <Text className={style.item}>{myActivityDetail.orderNo}</Text>
          </View>
          <View className={style.orderItem}>
            <Text className={style.label}>创建时间：</Text>
            <Text className={style.item}>{myActivityDetail.createTime}</Text>
          </View>
        </View>

        {/* myActivityDetail  ticketType  myActivityDetail */}
        {myActivityDetail && myActivityDetail.ticketType === 2 && (
          <View className={style.footerBox}>
            <View className={style.tickets} onClick={onShowHandle}>
              申请退票
            </View>
          </View>
        )}
      </View>

      {/* 退票弹窗 */}
      <View className={style.layout}>
        <TivketType
          detail={myActivityDetail}
          payLoadList={myActivityDetail.payLoadList}
          isOpened={isOpenLayout}
          onClose={() => {
            setIsOpenLayout(false);
          }}
          title=''
        ></TivketType>
      </View>
    </View>
  );
};

export default MyActivityDetail;
