import React, {FC, useImperativeHandle} from 'react';
import Taro from '@tarojs/taro';
import {Image, View} from '@tarojs/components';
import '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';

import {userActivityDateFormat} from '@/wxat-common/utils/activityFormat';
import {ActivityItem, VerifyStatus} from '@/types/activetype';
import style from './index.module.scss';
import useList from '@/hooks/useList';
import Empty from '@/wxat-common/components/empty/empty';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';

export interface ListProps {
  status: VerifyStatus;
  active: boolean;
  ref: any;
}

export interface Item {
  activityAddress: string;
  activityItemDTOList: ActivityItem[];
  activityNo: string;
  activityTitle: string;
  imgUrl: string;
  orderNo: string;
  orderStatus: VerifyStatus;
  payStatus: VerifyStatus;
  totalNum: number;
}

let List: FC<ListProps> = React.forwardRef((props, ref) => {
  const { status, active } = props;

  useImperativeHandle(ref, () => ({
    showLog: () => {
      load();
    },
  }));

  let { list, empty, load } = useList<Item>({
    query: async (page, pageSize) => {
      if (!active) {
        throw useList.Cancel;
      }

      const payload: {
        status?: number;
        pageNo: number;
        pageSize: number;
      } = {
        status,
        pageSize,
        pageNo: page,
      };

      if (status === VerifyStatus.All) {
        delete payload.status;
      }

      const { data } = await wxApi.request({
        url: api.activeModule.getMyActivityList,
        method: 'POST',
        data: payload,
        loading: true,
      });

      return data;
    },
  });

  const toTicket = (item) => {
    // 待支付直接拉起微信支付

    if (item.payStatus === VerifyStatus.Pending) {
      wxApi
        .request({
          url: api.activeModule.generateOrder,
          method: 'POST',
          data: {
            orderNo: item.orderNo,
            type: 1,
          },

          loading: true,
          // TODO: 正式环境移除
          checkSession: false,
        })
        .then((res) => {
          const dataInfo = res.data;

          let options = {
            package: `prepay_id=${dataInfo.prepayId}`,
            timeStamp: dataInfo.timeStamp + '',
            nonceStr: dataInfo.nonceStr,
            paySign: dataInfo.paySign,
            signType: dataInfo.signType,
            success: (e) => {
              wxApi
                .request({
                  url: api.activeModule.payment,
                  method: 'GET',
                  data: {
                    orderNo: dataInfo.orderNo,
                  },

                  loading: true,
                  // TODO: 正式环境移除
                  checkSession: false,
                })
                .then((ksd) => {
                  console.log('ksd', ksd);

                  const target = 'sub-packages/marketing-package/pages/activity-module/activity-submit-success/index';
                  protectedMailBox.send(target, 'detail', item);

                  wxApi.$redirectTo({
                    url: target,
                  });
                });
            },
            fail: (e) => {
              console.log('支付失败', e);
              wxApi.showToast({
                title: '支付失败',
                icon: 'none',
                mask: true,
              });
            },
          };

          // 微信支付
          wxApi.requestPayment(options);
        });
    }
  };

  const toDetail = (item) => {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/activity-module/my-activity-detail/index',
      data: { orderNo: item.orderNo, payStatus: item.payStatus },
    });
  };

  /**
   * 活动支付状态
   */
  // NOT_PAY("待支付", 10), //用户下单
  // PAID("已支付", 20), //用户支付完成 （mq收到支付消息修改）
  // CANCELLED("已取消", 30),

  return (
    <View className={style.activityLists} style={{ display: active ? 'block' : 'none' }}>
      {empty ? (
        <Empty />
      ) : (
        list.map((item) => {
          return (
            <View
              key={item.orderNo}
              className={style.activityItem}
              onClick={(e) => {
                e.stopPropagation();
                toDetail(item);
              }}
            >
              <View className={style.actvityTit}>
                <Image className={style.activitiyImg} mode='aspectFill' src={item.imgUrl} />
                <View className={style.title}>
                  <View className={`${style.upTit} two-line`}>{item.activityTitle}</View>
                  <View className={style.downTit}>
                    <View className={style.num}>共 {item.totalNum} 张</View>
                    {/* { 已取消 Cancelled */}
                    <View className={style.statusTxt}>
                      {item.payStatus === VerifyStatus.Cancelled
                        ? '已取消'
                        : item.payStatus === VerifyStatus.Pending
                        ? '待支付'
                        : '已支付'}
                    </View>
                  </View>
                  {item.payStatus === VerifyStatus.Pending && (
                    <View className={style.ticketBtn}>
                      <View
                        className={style.btn}
                        onClick={(e) => {
                          e.stopPropagation();
                          toTicket(item);
                        }}
                      >
                        付款
                      </View>
                    </View>
                  )}
                </View>
              </View>
              <View className={style.activityDatail}>
                <View className={style.detailItem}>
                  <View className={style.label}>活动地址 </View>
                  <View className={style.text}>{item.activityAddress}</View>
                </View>
                <View className={style.detailItem}>
                  <View className={style.label}>活动时间 </View>
                  <View className={style.text}>{userActivityDateFormat(item.activityItemDTOList)}</View>
                </View>
              </View>
            </View>
          );
        })
      )}
    </View>
  );
});

export default List;
