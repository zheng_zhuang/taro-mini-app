import React, {FC, useRef, useState} from 'react';
import Taro, {useReachBottom} from '@tarojs/taro';
import {Text, View} from '@tarojs/components';
import '@/wxat-common/utils/platform';
import style from './index.module.scss';
import {VerifyStatus} from '@/types/activetype';
import List from './List';

/**
 * 活动支付状态
 */
// NOT_PAY("待支付", 10), //用户下单
// PAID("已支付", 20), //用户支付完成 （mq收到支付消息修改）
// CANCELLED("已取消", 30),

const tabs = [
  { id: VerifyStatus.All, name: '全部' },
  { id: VerifyStatus.Pending, name: '待支付' },
  { id: VerifyStatus.Verified, name: '已支付' },
];

/**
 * 我的活动
 * @param props
 */
let MyActivities: FC = () => {
  const [activeTab, setActiveTab] = useState<number>(VerifyStatus.All);
  const actived = useRef({});

  const childRef = useRef<{ showLog: () => void }>(null);

  const checkTab = (item: any) => {
    actived.current[item.id] = true;
    setActiveTab(item.id);
  };

  // 上拉加载, 分页
  useReachBottom(() => {
    if (childRef.current) {
      childRef.current.showLog();
    }
  });

  return (
    <View className={style.activityListsBox}>
      <View className={style.tabBox}>
        {tabs.map((item, index) => {
          return (
            <View
              className={item.id === activeTab ? style.tabStyleActived : style.tabStyleNoActived}
              key={index}
              onClick={() => checkTab(item)}
            >
              <Text className={style.name}>{item.name}</Text>
              {item.id === activeTab && (
                <View className={style.activeLine} style={item.id === activeTab ? 'background:#FF8400' : ''}></View>
              )}
            </View>
          );
        })}
      </View>
      {tabs.map((i) => {
        const active = activeTab === i.id;
        const visible = active || actived.current[i.id];
        return !!visible && <List status={i.id} key={i.id} ref={childRef} active={active} />;
      })}
    </View>
  );
};

export default MyActivities;
