import React, {FC} from 'react';
import Taro, {useShareAppMessage} from '@tarojs/taro';
import {Button, Image, View} from '@tarojs/components';
import '@/wxat-common/utils/platform';
import style from './index.module.scss';
import wxApi from '@/wxat-common/utils/wxApi';
import {Activity} from '@/types/activetype';
import useProtectedMailBox from '@/hooks/useProtectedMailBox';
import shareUtil from '@/wxat-common/utils/share.js';

export interface ActivitySubmitSuccessProps {}

let ActivitySubmitSuccess: FC<ActivitySubmitSuccessProps> = (props) => {
  const detail = useProtectedMailBox<Activity>('detail');
  const toMyActivity = () => {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/activity-module/my-activities/index',
    });
  };
  // 支付报名成功, 分享活动详情
  useShareAppMessage((evt) => {
    const path = shareUtil.buildShareUrlPublicArguments({
      url: `sub-packages/marketing-package/pages/activity-module/activity-detail/index?id=${detail.id}`,
      bz: shareUtil.ShareBZs.SEARCH,
      bzName: '活动详情',
    });

    return {
      title: detail.activityTitle,
      path,
      imageUrl: detail.imgUrl,
    };
  });

  return (
    <View className={style.successBox}>
      <View className={style.successImgBox}>
        <Image className={style.successImg} src={require('@/sub-packages/marketing-package/images/success.png')} />
        <View className={style.successTit}>报名成功</View>
        <View className={style.successText}>恭喜您，已经报名成功~</View>
      </View>
      <View className={style.btnGroup}>
        <View className={style.checkBtn} onClick={toMyActivity}>
          查看我的报名
        </View>
        <Button className={style.shareBtn} openType='share'>
          分享活动
        </Button>
      </View>
    </View>
  );
};

export default ActivitySubmitSuccess;
