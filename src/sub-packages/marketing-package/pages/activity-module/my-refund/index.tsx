import React, { useState, FC } from 'react';
import Taro, { useRouter } from '@tarojs/taro';
import { View, Text, Textarea, Image, Picker } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import style from './index.module.scss';
import api from '@/wxat-common/api/index.js';
import DebounceButton from '@/wxat-common/components/DebounceButton/index';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';

/**
 * 申请退款
 * @param props
 */
let MyRefund: FC = () => {
  const selector = ['计划有变, 参加不了', '重复购票', '不想参加了', '其他'];

  const [reasonVal, setReasonVal] = useState('');
  const [explain, setExplain] = useState('');

  let itemInfo = protectedMailBox.read('ticketTypeInfo');

  if (itemInfo && itemInfo.price > 0) itemInfo.price = itemInfo.price / 100;

  const routerInfo = useRouter();
  const params = routerInfo.params;

  const onSubmit = () => {
    let data: any = {};
    data.orderItemId = params.orderItemId;
    data.orderNo = params.orderNo;

    if (reasonVal === '') {
      wxApi.showToast({ title: '请选择退款原因', icon: 'none' });
      return;
    }

    // 退款原因, 这里接口要求传文字
    data.refundReason = reasonVal;

    // 退款说明, 非必填
    data.refundGist = explain;

    wxApi
      .request({
        url: api.activeModule.refundInterface,
        loading: true,
        method: 'POST',
        header: {
          'content-type': 'application/json', // 默认值
        },
        data: {
          ...data,
        },
      })
      .then((res) => {
        if (res.success) {
          const target =
            'sub-packages/marketing-package/pages/activity-module/my-refund/components/SuccessComponent/index';

          wxApi.$navigateTo({
            url: target,
            data: {},
          });
        }
      })
      .catch((error) => {});
  };

  // 底部滚动选择器
  const onChange = (e) => {
    // e.detail.value 是 selector 的索引
    const value = e.detail.value;
    setReasonVal(selector[value]);
  };

  return (
    <View className={style.activityListsBox}>
      <View className={style.tabBox}>
        <View className={style.infoContent}>
          <View className={style.title}>退款商品</View>

          <View className={style.box}>
            <View className={style.left}>
              <Image className={style.img} src={itemInfo.imgUrl}></Image>
            </View>
            <View className={style.right}>
              <View className={style.rightTitle}>{itemInfo.ticketName}</View>
              <View className={style.count}>x 1</View>
              <View className={style.price}>￥{itemInfo.price}</View>
            </View>
          </View>
        </View>

        {/* content */}
        <View className={style.content}>
          <View className={style.contentTitle}>退款信息</View>

          <View className={style.navInfo}>
            <Text className={style.fontSize}>退款金额</Text>
            <Text className={style.price}>￥{itemInfo.price}</Text>
          </View>
          <View className={style.reason}>
            <Text className={style.fontSize}>退款原因</Text>
            <Picker mode='selector' range={selector} onChange={onChange}>
              {reasonVal == '' ? (
                <Text className={style.choice}>请选择 ></Text>
              ) : (
                <Text className={style.choice}>{reasonVal}</Text>
              )}
            </Picker>
          </View>
        </View>

        {/* 退款说明 */}
        <View className={style.explain}>
          <Text className={style.title}>退款说明 (选填)</Text>
          <View className={style.textareabox}>
            <Textarea
              onInput={(e) => {
                setExplain(e.detail.value);
              }}
              maxlength={50}
              placeholder='补充描述, 有助于商家更好的处理售后问题'
              placeholderClass={style.place}
              className={style.input}
              autoFocus
            />
          </View>
        </View>

        {/* 底部按钮 */}
        <View className={style.footer}>
          <DebounceButton className={style.submitBtn} onClick={onSubmit}>
            提交
          </DebounceButton>
        </View>
      </View>
    </View>
  );
};

export default MyRefund;
