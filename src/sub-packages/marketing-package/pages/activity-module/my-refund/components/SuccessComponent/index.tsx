import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import { Activity, ActivityItem } from '@/types/activetype';
import './index.scss';

type ComponentProps = {
  isOpened: boolean;
  onClose: Function;
  title: string;
  showType: number;
  payLoadList?: ActivityItem[];
  buyNumMin?: number;
  detail?: Activity;
};

let SuccessComponent: FC<ComponentProps> = (props) => {
  const toHome = () => {
    // wxat-common/pages/home/index
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  return (
    <View className='success'>
      <View className='content'>
        <View className='imgBox'>
          <Image
            className='img'
            src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/mine/ic-cut-price.png'
          ></Image>
        </View>
        <View className='title'>申请成功</View>
        <View className='info'>退款申请通过后, 退款款项将原路返回您的账户</View>
        <View className='footer'>
          <View onClick={toHome} className='submitBtn'>
            返回首页
          </View>
        </View>
      </View>
    </View>
  );
};

export default SuccessComponent;
