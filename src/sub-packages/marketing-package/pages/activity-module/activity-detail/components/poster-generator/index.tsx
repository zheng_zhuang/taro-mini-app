import React, {FC, useImperativeHandle, useState} from 'react';
// import { useScope } from '@/wxat-common/utils/platform';
import {Button, Canvas, Image, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import {useSelector} from 'react-redux';

import authHelper from '@/wxat-common/utils/auth-helper.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import canvasHelper from '@/wxat-common/utils/canvas-helper.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';

import './index.scss';
import {useCanvasRef} from '@/wxat-common/utils/platform/createContext';
import shareUtil from '@/wxat-common/utils/share';

const commonImg = cdnResConfig.common;

const POSTER_BG = [
  cdnResConfig.goods.posterBgOne,
  cdnResConfig.goods.posterBgTwo,
  cdnResConfig.goods.posterBgThree,
  cdnResConfig.goods.posterBgFour,
];

// props的类型定义
type PosterGeneratorProps = {
  // 页面路径
  path: string;
  scene: string;
  /**
   * 海报上面的提示语
   */
  posterTips: string;
  /**
   * 海报主图
   */
  posterImage: string;
  /**
   * 商品名称
   */
  posterName?: string;
  desc1?: string;
  desc2?: string;
  /**
   * 海报优惠价标签
   */
  posterSalePriceLabel?: string;

  /**
   * 头像旁推荐语
   */
  recommendTips?: string;
  onSave: () => any;
  onClose?: () => void;
  ref?: React.Ref<PosterGeneratorMethod>;
};

export interface PosterGeneratorMethod {
  show: () => void;
  hide: () => void;
}

const preventTouchMove = () => {};

const WIDTH = 308;
const HEIGHT = 411;
const PADDING_TOP = 16;
const PADDING_LEFT = 19;

/**
 * 海报生成器
 * @param param0
 */
let PosterGenerator: FC<PosterGeneratorProps> = React.forwardRef(
  (
    {
      desc1,
      desc2,
      path,
      scene,
      uniqueShareInfoForQrCode,
      activityId,
      posterTips,
      posterImage,
      posterName,
      posterSalePriceLabel,
      recommendTips,
      onSave,
    },
    ref
  ) => {
    const [qrCodeUrl, setQrCodeUrl] = useState('');
    const user = useSelector((state) => state.base.wxUserInfo);
    const canvasRef = useCanvasRef('canvas');
    const scope = useScope();
    // 显示模态框
    const [showModalStatus, setShowModalStatus] = useState(false);

    // 需要暴露给外面使用的函数
    useImperativeHandle(ref, () => ({
      hide,
      show,
    }));

    const hide = () => {
      setShowModalStatus(false);
      setQrCodeUrl('');
    };

    const show = () => {
      if (!authHelper.checkAuth()) {
        return;
      }

      // 检查用户登录状态
      setShowModalStatus(true);
      handleShowPosterDialog();
    };

    const apiQRCodeByScene = () => {
      const publicArguments = shareUtil.buildSharePublicArguments('', true);
      const activityInfo = { id: activityId, page: path };
      const scene = Object.assign({}, publicArguments, uniqueShareInfoForQrCode, activityInfo);
      wxApi
        .request({
          url: api.generatorMapperQrCode,
          loading: true,
          data: {
            shareParam: JSON.stringify(scene),
            urlPath: path,
            //type: null
          },
        })
        .then((res) => {
          setQrCodeUrl(res.data);
        })
        .catch((err) => {
          hide();
        });
    };

    // 显示海报
    const handleShowPosterDialog = () => {
      setQrCodeUrl('');
      apiQRCodeByScene();
    };

    const handleSave = async () => {
      wxApi.showLoading({
        title: '正在保存图片...',
      });

      const canvasContext = canvasRef.current;

      if (!canvasContext) {
        console.error('cavans Context 为空');
        return;
      }

      const imageRequests = [
        wxApi.getImageInfo({
          src: user.avatarUrl,
        }),

        wxApi.getImageInfo({
          src: (posterImage || '').replace(/^https|http/, 'https'),
        }),
      ];

      if (qrCodeUrl) {
        imageRequests.push(
          wxApi.getImageInfo({
            src: qrCodeUrl,
          })
        );
      }

      try {
        const res = await Promise.all(imageRequests);

        canvasHelper.drawFillRect(canvasContext, 0, 0, WIDTH, HEIGHT, '#FFFFFF');

        // 头像
        canvasHelper.drawCircleImage(canvasContext, PADDING_LEFT + 20, PADDING_TOP + 20, 20, res[0].path);
        // 用户名
        canvasHelper.drawText(canvasContext, user.nickName, PADDING_LEFT + 40 + 10, PADDING_TOP * 2, {
          textAlign: 'start',
          fillStyle: '#373A44',
          fontSize: 14,
        });

        // 推荐语
        // 三角
        canvasHelper.drawFillLine(
          canvasContext,
          [
            {
              x: PADDING_LEFT + 40 + 10 - 5,
              y: PADDING_TOP + 20 + 10,
            },

            {
              x: PADDING_LEFT + 40 + 10 + 5,
              y: PADDING_TOP + 20 + 5,
            },

            {
              x: PADDING_LEFT + 40 + 10 + 5,
              y: PADDING_TOP + 20 + 10 + 10,
            },
          ],

          '#F0556C'
        );

        // 方框
        canvasHelper.drawFillRect(canvasContext, PADDING_LEFT + 40 + 10, PADDING_TOP + 20, 136, 20, '#fdab53');

        //拼团、砍价 商品详情文案
        canvasHelper.drawText(canvasContext, posterTips, PADDING_LEFT + 40 + 10, PADDING_TOP * 2 + 20, {
          textAlign: 'start',
          fillStyle: '#111',
          fontSize: 13,
        });

        // TODO: 重构
        canvasContext.draw(false, () => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'canvas',
              },

              scope
            )
            .then((res) => {
              return wxApi.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
              });
            })
            .then(() => {
              wxApi.showToast({
                title: '已保存到相册',
              });

              hide();
            })
            .catch((error) => {
              console.log('saveImage error: ', error);
              wxApi.showToast({
                title: '保存失败',
                icon: 'none',
              });
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      } catch (err) {
        console.error(err);
      } finally {
        wxApi.hideLoading();
      }
    };

    return (
      <View className='ps-gen'>
        <Canvas
          canvasId='canvas'
          className='share-canvas'
          ref={canvasRef}
          style={{ width: `${WIDTH}px`, height: `${HEIGHT}px` }}
        />

        {/* 海报预览框  */}
        {showModalStatus && (
          <View className='share-dialog'>
            {showModalStatus && <View className='posters-mask' onTouchMove={preventTouchMove} onClick={hide}></View>}
            <View className='posters-dialog'>
              <View className='posters-box'>
                <View className='posters-nick-header'>
                  <Image className='posters-head' mode='aspectFill' src={user.avatarUrl}></Image>
                  <View className='posters-right'>
                    <View className='name'>{user.nickName + recommendTips}</View>
                    <View className='posters-notes'>{posterTips}</View>
                  </View>
                </View>

                {/* 商品图片 */}
                <Image className='posters-img' mode='aspectFill' src={posterImage}></Image>
                {/* 活动类型、活动价格、原价 */}
                <View className='posters-detail'>
                  <View className='posters-info'>
                    {/* 商品名称 */}
                    <View className='goods-name limit-line line-2'>{posterName}</View>
                    <View className='goods-desc'>{desc1}</View>
                    <View className='goods-desc'>{desc2}</View>
                  </View>
                  <Image className='qr-code' src={qrCodeUrl} mode='aspectFit'></Image>
                </View>
              </View>
              <Button className='btn-save' onClick={handleSave}>
                保存海报
              </Button>

              <Image onClick={hide} className='posters-close' src={commonImg.close}></Image>
            </View>
          </View>
        )}
      </View>
    );
  }
);

PosterGenerator.defaultProps = {
  posterName: '',
  posterTips: '向您推荐了这个活动',
  salePrice: '',
  labelPrice: '',
  posterSalePriceLabel: '价格',
  recommendTips: '向你推荐',
};

export default PosterGenerator;
