import React, {FC, useEffect, useRef, useState} from 'react';
import '@/wxat-common/utils/platform';
import {Button, Image, Input, Label, Radio, RadioGroup, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import style from './index.module.scss';
import {Activity, ActivityItem, ActivityTicketItem} from '@/types/activetype';
import {activityDateFormatSimple} from '@/wxat-common/utils/activityFormat';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import screen from '@/wxat-common/utils/screen';
import PosterGenerator from '../poster-generator/index';

type ComponentProps = {
  isOpened: boolean;
  onClose: Function;
  title: string;
  showType: number;
  payLoadList?: ActivityItem[];
  buyNumMin?: number;
  detail?: Activity;
};

let FloatLayoutModule: FC<ComponentProps> = ({
  isOpened,
  onClose,
  title,
  showType,
  payLoadList,
  buyNumMin = 1,
  detail,
}) => {
  // const [dataList, setDataList] = useState<any[]>([]);
  const [itemInfo, setItem] = useState<ActivityItem>();
  const [ticket, setTicket] = useState<ActivityTicketItem>();
  const [ticketList, setTicketList] = useState<ActivityTicketItem[]>([]);
  const [dataType, setDataType] = useState<string>('');
  const [ticketsku, setTicketSku] = useState<string>('');
  const [buyNumber, setBuyNumber] = useState<number>(1);
  const [price, setPrice] = useState<number>(0);
  const posterGeneratorRef = useRef<{ show: () => void }>(null);

  const [tshow, setTshow] = useState(false);

  useEffect(() => {
    if (payLoadList && payLoadList.length > 0) {
      setBuyNumber(1);
      setDataType(payLoadList[0].id);
      setItem(payLoadList[0]);
      setTicketList(payLoadList[0].ticketDTOList);
      setTicketSku(payLoadList[0].ticketDTOList[0].sku);
      setPrice(payLoadList[0].ticketDTOList[0].price * 1);
      setTicket(payLoadList[0].ticketDTOList[0]);
      // setDataList(
      //   payLoadList.map((item) => {
      //     const weekDay = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
      //     const myDate = remoteDateParse(item.createTime);
      //     const yearDateList = item.createTime.split(' ');
      //     return {
      //       date: `${yearDateList[0]} ${weekDay[myDate.getDay()]} ${yearDateList[1]} `,
      //       id: item.id,
      //     };
      //   })
      // );
    }
  }, [payLoadList]);

  const handleDecreaseBuyNum = () => {
    if (buyNumber > 1) {
      setBuyNumber(buyNumber - 1);
      setPrice(ticket.price * (buyNumber - 1));
    }
  };

  const handleIncreaseBuyNum = () => {
    if (buyNumber < ticket.inventoryNum || !ticket.inventoryNum) {
      setBuyNumber(buyNumber + 1);
      setPrice(ticket.price * (buyNumber + 1));
    } else {
      wxApi.showToast({ title: '报名人数已达上限', icon: 'none' });
    }
  };

  const itemChange = (e) => {
    if (payLoadList && payLoadList.length > 0) {
      for (let i = 0; i < payLoadList.length; i++) {
        const item = payLoadList[i];
        if (item.id == e.detail.value) {
          const ticket = item.ticketDTOList[0];
          setItem(item);
          setDataType(e.detail.value);
          setTicketList(item.ticketDTOList);
          setTicket(ticket);

          // 重置购买数量
          if (ticket.buyLimit && buyNumber > ticket.buyLimit) {
            setBuyNumber(ticket.buyLimit);
            setPrice(ticket.price * ticket.buyLimit);
          } else {
            setPrice(ticket.price * buyNumber);
          }

          setTicketSku(ticket.sku);
          break;
        }
      }
    }
    // setTicketList()
  };

  const ticketChange = (e) => {
    console.log('选择票种e', e);

    for (const item of ticketList) {
      if (item.sku == e.detail.value) {
        console.log(item);
        setTicket(item);
        setTicketSku(item.sku);
        // 重置购买数量
        setBuyNumber(1);
        setPrice(item.price);
        // 重置保证金展示
        setTshow(false);
        // 限购次数 item.buyLimit
        // // 选择的数量 buyNumber
        // if (item.buyLimit && buyNumber > item.buyLimit) {
        //   setBuyNumber(item.buyLimit);
        //   setPrice(item.price * item.buyLimit);
        // } else {
        //   setPrice(item.price * buyNumber);
        // }
        break;
      }
    }
  };

  const handleShowPoster = () => {
    if (posterGeneratorRef.current) {
      posterGeneratorRef.current.show();
    }
    onClose();
  };

  const toNext = (iteminfo) => {
    let count = buyNumber;
    let tempPrice = price;

    if (ticket && ticket.inventoryNum == 0) {
      wxApi.showToast({ title: '该票剩余数量为0，请选择其他票种', icon: 'none' });
    } else {
      // 限购次数 item.buyLimit
      // 选择的数量 buyNumber
      if (iteminfo.buyLimit && buyNumber > iteminfo.buyLimit) {
        // 超过限制数量
        count = iteminfo.buyLimit;
        tempPrice = iteminfo.price * iteminfo.buyLimit;
        wxApi.showToast({ title: `该票每人限购${iteminfo.buyLimit}张，请重新选择购买数量`, icon: 'none' });
        return;
      } else {
        tempPrice = iteminfo.price * buyNumber;
      }

      const target = 'sub-packages/marketing-package/pages/activity-module/activity-submit/index';
      protectedMailBox.send(target, 'detail', detail);
      // onClose();
      // buyNumber 购买数量; ticketType 票种类型
      wxApi.$navigateTo({
        url: target,
        data: {
          itemId: itemInfo!.id,
          ticketId: ticket!.id,
          sku: ticketsku,
          num: count,
          price: tempPrice,
          ticketType: iteminfo.ticketType,
        },
      });
    }
  };

  const onShow = () => {
    setTshow(!tshow);
  };

  if (detail == null) {
    return null;
  }

  return (
    <View>
      {detail && (
        <PosterGenerator
          ref={posterGeneratorRef}
          path='sub-packages/marketing-package/pages/activity-module/activity-detail/index'
          scene={String(detail.id)}
          posterName={detail.activityTitle}
          posterImage={detail.imgUrl}
          activityId={detail.id}
          desc1={`活动地址: ${detail.activityAddress}`}
          desc2={`活动时间: ${activityDateFormatSimple(detail.itemDTOList)}`}
        />
      )}

      {showType === 2 && (
        <View>
          <View className={isOpened ? style.flolayoutActive : style.flolayout}>
            <View className={style.flolayoutContainer}>
              <View className={style.shareBox}>
                <Button className={style.haibaoBtn} openType='share' onClick={onClose}>
                  <View className={style.iconBox}>
                    <Image
                      className={style.haibaoIcon}
                      src={require('@/sub-packages/marketing-package/images/wx.png')}
                    />

                    <Text className={style.iconText}>分享给朋友</Text>
                  </View>
                </Button>
                <Button className={style.haibaoBtn} onClick={handleShowPoster}>
                  <View className={style.iconBox}>
                    <Image
                      className={style.haibaoIcon}
                      src={require('@/sub-packages/marketing-package/images/haibao.png')}
                    />

                    <Text className={style.iconText}>生成活动海报</Text>
                  </View>
                </Button>
              </View>
              <View
                className={style.layoutPhoneBtn}
                style={{ paddingBottom: Taro.pxTransform(screen.safeAreaBottom) }}
                onClick={() => {
                  onClose();
                }}
              >
                取消
              </View>
            </View>
          </View>
        </View>
      )}

      {showType === 3 && (
        <View>
          <View className={isOpened ? style.flolayoutActive : style.flolayout}>
            <View
              className={style.mask}
              onClick={() => {
                setTshow(false);
                onClose();
              }}
            ></View>
            <View
              className={style.flolayoutContainer}
              style={{ paddingBottom: Taro.pxTransform(screen.safeAreaBottom) }}
            >
              <View className={style.itemsbox}>
                {/* 1阶段暂时不用日期 */}
                {/* <View className={style.item}>
                <View className={style.itemTitle}>选择日期</View>
                <View>
                  <RadioGroup className={style.formRadio} onChange={itemChange}>
                    {dataList.map((item) => {
                      return (
                        <Label key={item.id} className={dataType == item.id ? style.labelCheck : style.label}>
                          <Radio value={item.id} className={style.radio} color='#FF8400'></Radio>
                          <Text className={style.radioTex}>{item.date}</Text>
                          {dataType == item.id && (
                            <Image className={style.radioCheckIcon} src={require('@/images/yangkangchecked.png')} />
                          )}
                        </Label>
                      );
                    })}
                  </RadioGroup>
                </View>
                </View> */}
                <View className={style.item}>
                  <View className={style.itemTitle}>选择票种</View>
                  <View>
                    <RadioGroup onChange={ticketChange} className={style.formRadio}>
                      {ticketList &&
                        ticketList.length > 0 &&
                        ticketList.map((item) => {
                          return (
                            <Label
                              key={item.sku}
                              className={ticketsku == item.sku ? style.ticketCheck : style.labelTick}
                            >
                              <Radio value={item.sku} className={style.radio} color='#FF8400'></Radio>
                              <Text className={style.ticketName}>{item.ticketName}</Text>

                              {/* 新增需求: 当门票性质为保证金时, 需进行标示; */}
                              {/* 1: 免费; 2: 付费; 3: 保证金 */}
                              {item.ticketType === 3 && (
                                <View className={style.ensure}>
                                  (保证金)
                                  <Text onClick={onShow} className={style.ensicon}>
                                    ?
                                  </Text>
                                </View>
                              )}

                              {item.price === 0 ? (
                                <Text className={style.ticketPrice}>
                                  {item.buyLimit > 0 && <Text className={style.limit}>每人限购{item.buyLimit}张</Text>}
                                  免费
                                </Text>
                              ) : (
                                <Text className={style.ticketPrice}>
                                  {item.buyLimit > 0 && <Text className={style.limit}>每人限购{item.buyLimit}张</Text>}¥
                                  {item.price / 100}
                                </Text>
                              )}

                              {ticketsku == item.sku && (
                                <Image
                                  className={style.radioCheckIcon}
                                  src={require('@/sub-packages/marketing-package/images/checked.png')}
                                />
                              )}
                            </Label>
                          );
                        })}
                    </RadioGroup>
                  </View>
                </View>

                {/* 新增需求: 当门票性质为保证金时, 需进行标示 */}
                {tshow ? (
                  <View className={style.ensuretitle}>
                    <View className={style.title}>保证金</View>
                    <View className={style.content}>
                      保证金为报名时暂时缴付的费用，线下签到核销后，会立即按原路退回用户的账户。若未到现场签到参与活动，则不进行退款
                    </View>
                  </View>
                ) : null}

                <View className={style.item}>
                  <View className={style.itemTitle}>选择数量</View>
                  <View className={style.numOperationBox}>
                    <View className={style.numDecrease} onClick={handleDecreaseBuyNum}>
                      {' '}
                      -{' '}
                    </View>
                    <View className={style.numInput}>
                      <Input className={style.numInputInput} type='number' value={buyNumber} disabled></Input>
                    </View>
                    <View className={style.numIncrease} onClick={handleIncreaseBuyNum}>
                      {' '}
                      +{' '}
                    </View>
                    <View className={style.inventoryNum}>剩余{ticket && ticket.inventoryNum}</View>
                  </View>
                </View>
                <View className={style.footer}>
                  <View className={style.left}>
                    <Text className={style.leftLabel}>合计：</Text>
                    <Text className={style.slight}>¥</Text>
                    <Text className={style.leftPrice}>{price / 100}</Text>
                  </View>
                  <View className={style.right}>
                    <Text
                      onClick={() => toNext(ticket)}
                      className={ticket && ticket.inventoryNum == 0 ? style.rightBtnNoAllow : style.rightBtn}
                    >
                      下一步
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

FloatLayoutModule.defaultProps = {
  isOpened: false,
  onClose: () => {},
  title: '弹窗',
};

export default FloatLayoutModule;
