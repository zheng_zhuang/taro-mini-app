import {$getRouter} from 'wk-taro-platform';
import React from 'react';
import {Block, Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import wxParse from '@/wxat-common/components/parse-wrapper/wxParse';
import ParseWrapper from '@/wxat-common/components/parse-wrapper/index';

import FloatLayoutModule from './components/floatLayout/index';
import {activityDateFormat, userActivityDateFormat} from '@/wxat-common/utils/activityFormat';
import style from './index.module.scss';
import {Activity, ActivityStatus} from '@/types/activetype';
import authHelper from '@/wxat-common/utils/auth-helper';
import screen from '@/wxat-common/utils/screen';

import subscribeEnum from '@/wxat-common/constants/subscribeEnum.js';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg';
import {connect} from 'react-redux';

//redux state的在这里定义
type PageStateProps = {
  currentStore: {
    id: number;
  };
};

//redux 的action方法再这里定义
type PageDispatchProps = {};

// 父子组件直接传递的props
type PageOwnProps = {
  pageConfig: Array<Record<string, any>>;
  viewShow: number;
  isPageShow: boolean;
  isTabbar: boolean;
  isHome: boolean;
};

// 组件的state声明
type PageState = {
  activityData: Activity | null;
  isOpenLayout: boolean;
  showType: number;
  detail?: any;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface ActivityDetail {
  props: IProps;
}

const mapStateToProps = ({ base: { currentStore } }) => ({
  currentStore,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class ActivityDetail extends React.Component {
  $router = $getRouter();
  get id() {
    return this.$router.params.id || this.$router.params.scene;
  }

  state = {
    isOpenLayout: false,
    showType: 1,
    activityData: null,
    detail: null,
  } as PageState;

  componentDidMount() {
    wxApi.setNavigationBarTitle({
      title: this.config.navigationBarTitleText || '',
    });

    this.getActivityDetail();
  }

  onShareAppMessage() {
    return {
      title: this.state.activityData!.activityTitle,
      path: `sub-packages/marketing-package/pages/activity-module/activity-detail/index?id=${this.id}`,
      imageUrl: this.state.activityData!.imgUrl,
    };
  }

  async getActivityDetail() {
    const currentStore = this.props.currentStore;
    const per_storeId = currentStore.id;

    const { data } = await wxApi.request({
      url: api.activeModule.getActivityDetails,
      method: 'GET',
      data: {
        id: this.id,
        per_storeId, // 新增字段, 首页门店 id
      },
      loading: true,
      checkSession: false,
    });

    this.setState({
      activityData: data,
      detail: wxParse('html', data.activityDetail),
    });
  }

  handleContact = async () => {
    const data = this.state.activityData;
    if (!data) {
      return;
    }

    if (data.contactsDTOList.length === 1) {
      wxApi.makePhoneCall({
        phoneNumber: data.contactsDTOList[0].contactsPhone,
      });
    } else {
      const res = await wxApi.showActionSheet({
        itemList: data.contactsDTOList.map((i) => `${i.contactsName} ${i.contactsPhone}`),
      });

      if (res.tapIndex >= 0) {
        wxApi.makePhoneCall({
          phoneNumber: data.contactsDTOList[res.tapIndex].contactsPhone,
        });
      }
    }
  };

  handleSignIn = () => {
    if (!authHelper.checkAuth()) {
      return;
    }

    /**
     * 活动状态
     */
    //  Removed, // 已删除
    //  Draft, // 操作
    //  Pending, // 进行中
    //  Doing, // 进行中
    //  Ended, // 已结束

    //判断订阅消息模板
    // 这里先暂时写成这样, 模板消息等待后端和微信端配置, 配置完记得更改
    let Ids: number[] = [];
    Ids = [subscribeEnum.ACTIVITY_START.value];

    //先授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      if (!this.state.activityData) return;

      if (this.state.activityData.status == ActivityStatus.Ended) {
        wxApi.showToast({ title: '活动已结束', icon: 'none' });
      } else {
        this.setState({
          isOpenLayout: true,
          showType: 3,
        });
      }
    });
  };

  handleShare = () => {
    this.setState({
      isOpenLayout: true,
      showType: 2,
    });
  };

  getSignDate() {
    const { activityData } = this.state;
    if (activityData == null) {
      return;
    }

    return userActivityDateFormat(activityData.itemDTOList);
  }

  // 格式化报名时间
  getSignUpDate() {
    const { activityData } = this.state;
    if (activityData == null) {
      return;
    }

    return activityDateFormat(activityData.itemDTOList);
  }

  handleOpenLocation() {
    const data = this.state.activityData;
    if (data == null) {
      return;
    }
    wxApi.openLocation({
      latitude: parseFloat(data.latitude),
      longitude: parseFloat(data.longitude),
    });
  }

  render() {
    const { activityData, isOpenLayout, showType } = this.state;

    if (activityData == null) {
      return null;
    }

    let flag = true;
    let btnText = '我要报名';
    const tempEndTime = activityData.itemDTOList[0].endTime;
    const tempStartTime = activityData.itemDTOList[0].startTime;

    // 活动已结束
    if (activityData.status == ActivityStatus.Ended) {
      flag = false;
      btnText = '活动已结束';
    }

    // 报名时间 startTime  endTime
    // 开始时间
    const startFormatTime = new Date(tempStartTime).valueOf();
    // 结束时间
    const endtFormatTime = new Date(tempEndTime).valueOf();
    // 当前时间
    const currentFormatTime = new Date().valueOf();

    // 当前时间小于 开始时间, 报名未开始
    if (currentFormatTime < startFormatTime) {
      flag = false;
      btnText = '报名未开始';
    } else {
      // 如果结束时间小于当前时间, 报名已结束
      if (endtFormatTime < currentFormatTime) {
        flag = false;
        btnText = '报名已结束';
      }
    }

    return (
      <View className={style.detailContainer}>
        {isOpenLayout && <View className={style.mark}></View>}
        <View className={style.activitiyImgBox}>
          <Image className={style.activitiyImg} mode='aspectFill' src={activityData.imgUrl} />
        </View>
        <View className={style.activitiyDataBox}>
          <View className={style.activitiy}>
            <View className={style.activitiyTitle}>
              <Text className={style.titleTxt}>{activityData.activityTitle}</Text>
            </View>
            <View className={style.deliveryStoreInfoItem}>
              {/* <View className='delivery-store-info-item store-name limit-line'> */}
              <Image
                className={`${style.cLocationDark} ${style.cLocationDark_LimitZoomOut}`}
                src={require('@/sub-packages/marketing-package/images/ic-location2x.png')}
              ></Image>
              <View className={style.cLoText}>活动地址：</View>
              <View className={style.cLoAddress} onClick={this.handleOpenLocation}>
                {(activityData && activityData.activityAddress) || '-'}
              </View>
              <View className={style.rIconBox}>
                <Image
                  className={style.rightIcon}
                  src={require('@/sub-packages/marketing-package/images/icon_@2x.png')}
                ></Image>
              </View>
            </View>
            <View className={style.deliveryStoreInfoItem}>
              <Image
                className={style.cLocationDark}
                src={require('@/sub-packages/marketing-package/images/time2x.png')}
              ></Image>
              <View className={style.cLoText}>活动时间：</View>
              <View className={style.cLoAddress}>{this.getSignDate()}</View>
            </View>
            <View className={style.deliveryStoreInfoItem}>
              <Image
                className={style.cLocationDark}
                src={require('@/sub-packages/marketing-package/images/time2x.png')}
              ></Image>
              <View className={style.cLoText}>报名时间：</View>
              <View className={style.cLoAddress}>{this.getSignUpDate()}</View>
            </View>
            <View className={style.deliveryStoreInfoItem}>
              <Image
                className={style.cLocationDark}
                src={require('@/sub-packages/marketing-package/images/person@2x.png')}
              ></Image>
              <Text className={style.cLoAcText}>{`已报名${activityData.saleNum || 0}人`}</Text>
              <View className={style.iconList}>
                {activityData.joinPeopleList &&
                  activityData.joinPeopleList.length &&
                  activityData.joinPeopleList.slice(0, 5).map((item, index) => {
                    if (index < 10) {
                      return <Image className={style.littleIcon} src={item}></Image>;
                    }
                  })}
              </View>
            </View>
            <View className={style.price}>
              {activityData.ticketPrice === 0 ? (
                <Text className={style.priceText}>免费</Text>
              ) : (
                <Block>
                  <Text>
                    ¥ <Text className={style.priceText}>{activityData.ticketPrice / 100}</Text>
                  </Text>
                </Block>
              )}
            </View>
          </View>
          <View className={style.detailBox}>
            <View className={style.activitiyDetTitle}>活动详情介绍</View>
            <View className={style.activitiyDetContent}>
              <ParseWrapper parseContent={this.state.detail} />
            </View>
          </View>
        </View>
        <View className={style.layout}>
          <FloatLayoutModule
            detail={activityData}
            payLoadList={showType === 3 && activityData.itemDTOList ? activityData.itemDTOList : []}
            showType={showType}
            isOpened={isOpenLayout}
            onClose={() => {
              this.setState({
                isOpenLayout: false,
              });
            }}
            title=''
          ></FloatLayoutModule>
        </View>
        <View className={style.imageBtnBox} style={{ paddingBottom: Taro.pxTransform(screen.safeAreaBottom) }}>
          <View className={style.imageBtn}>
            <View className={style.btnPhone} onClick={this.handleShare}>
              <Image
                className={style.btnIcon}
                src={require('@/sub-packages/marketing-package/images/shareIcon.png')}
              ></Image>
              <Text className={style.btnShareText}>分享</Text>
            </View>
          </View>
          <View className={style.btnBox}>
            <View className={style.btnShare} onClick={this.handleContact}>
              <Text>活动咨询</Text>
            </View>
            <View
              className={!flag ? style.btnStop : style.btnOther}
              onClick={() => {
                if (flag) {
                  this.handleSignIn();
                }
              }}
            >
              <Text>{btnText}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default ActivityDetail;
