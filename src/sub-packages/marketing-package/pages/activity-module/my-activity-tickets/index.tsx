import React, { useState, FC, useEffect } from 'react';
import Taro, { useRouter, useShareAppMessage } from '@tarojs/taro';
import '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { VerifyStatus, ActivityTicket } from '@/types/activetype';
import useInterval from '@/hooks/useInterval';
import Tickets from './Tickets';

/**
 * 电子票
 * @param props
 */
let MyActivityTickets: FC = () => {
  const router = useRouter();

  const [ticketDetail, setTicketDetail] = useState<ActivityTicket[]>([]);
  const [shareToken, setShareToken] = useState('');

  const getTicketDetail = () => {
    const orderNo = router.params.orderNo;
    wxApi
      .request({
        url: api.activeModule.getTicket,
        method: 'GET',
        data: {
          orderNo,
        },
      })
      .then((res) => {
        let infoData: { id: string } = { id: '' };

        if (router.params.info) infoData = JSON.parse(router.params.info);

        if (res.data) {
          let data = res.data;

          if (infoData.id !== '') data = data.filter((item) => item.id === infoData.id);

          data.forEach((item) => {
            if (item.customerJsonRecord) {
              item.customerJsonRecord = JSON.parse(item.customerJsonRecord);
            }
          });

          setTicketDetail(data);
        }
      });
  };

  const getShareToken = () => {
    wxApi
      .request({
        url: api.activeModule.getShareTickData,
        method: 'GET',
        data: {
          orderNo: router.params.orderNo,
        },
      })
      .then((res) => {
        setShareToken(res.data);
      });
  };

  useShareAppMessage(() => {
    if (shareToken) {
      const orderNo = router.params.orderNo;
      const title = ticketDetail[0].activityTitle;

      let infoData: { id: string } = { id: '' };

      if (router.params.info) infoData = JSON.parse(router.params.info);

      const path = `sub-packages/marketing-package/pages/activity-module/my-activity-tickets/share?orderNo=${orderNo}&id=${infoData.id}`;
      console.log(path);
      return {
        title,
        path,
      };
    }
  });

  useEffect(() => {
    getShareToken();
    getTicketDetail();
  }, []);

  useInterval({
    fn: getTicketDetail,
    // 如果还有未核销的，这继续轮询
    duration: ticketDetail.length === 0 || ticketDetail.some((i) => i.status != VerifyStatus.Verified) ? 5000 : 0,
    immediate: true,
  });

  return <Tickets list={ticketDetail} shareable={!!shareToken} />;
};

export default MyActivityTickets;
