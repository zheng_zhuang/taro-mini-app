import React, { FC, useState, useEffect } from 'react';
import Taro, { useRouter } from '@tarojs/taro';
import '@/wxat-common/utils/platform';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { ActivityTicket } from '@/types/activetype';
import Tickets from './Tickets';

export interface ShareTicketProps {}

let ShareTicket: FC<ShareTicketProps> = (props) => {
  const router = useRouter();
  const [ticketDetail, setTicketDetail] = useState<ActivityTicket[]>([]);

  const getTicketDetail = () => {
    const orderNo = router.params.orderNo;

    wxApi
      .request({
        url: api.activeModule.getTicket,
        method: 'GET',
        data: {
          orderNo,
        },
      })
      .then((res) => {
        const targetId = router.params.id;

        if (res.data) {
          let data = res.data;

          if (targetId !== '') data = data.filter((item) => item.id === Number(targetId));

          data.forEach((item) => {
            if (item.customerJsonRecord) {
              item.customerJsonRecord = JSON.parse(item.customerJsonRecord);
            }
          });

          setTicketDetail(data);
        }
      });
  };

  useEffect(() => {
    getTicketDetail();
  }, []);

  return <Tickets list={ticketDetail}></Tickets>;
};

export default ShareTicket;
