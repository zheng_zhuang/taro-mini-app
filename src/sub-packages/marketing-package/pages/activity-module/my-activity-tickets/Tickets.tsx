import React, { FC } from 'react';
import Taro from '@tarojs/taro';
import { View, Text, Image, Button } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import { ActivityTicket } from '@/types/activetype';
import { activityItemDateFormat } from '@/wxat-common/utils/activityFormat';
import style from './index.module.scss';

import { TicketStatus, TicketStatusobj } from '@/wxat-common/utils/activityModule';

export interface TicketsProps {
  list: ActivityTicket[];
  shareable?: boolean;
}

let Tickets: FC<TicketsProps> = (props) => {
  let { list = [], shareable } = props;

  console.log('list', list);

  return (
    <View className={style.ticketBox}>
      {list.map((item, index) => {
        return (
          <View key={item.id} className={style.item}>
            <View className={style.mainTit}>
              <Image className={style.borderIcon} src="https://htrip-static.ctlife.tv/wk/marketing-base/border.png" />
              <Text className={style.label}>报名人{index + 1}</Text>
              <Text className={style.user}>
                {item.customerJsonRecord[0].itemKey0}
                {item.customerJsonRecord[1].itemKey1 && `(${item.customerJsonRecord[1].itemKey1})`}
              </Text>
            </View>
            <View className={style.ticketDetail}>
              <View className={style.ticketName}>{item.activityTitle}</View>
              <View className={style.ticketDetailItem}>
                <View className={style.tab}>
                  <View className={style.tabLabel}>时间：</View>
                  <Text className={style.tabDesc}>{activityItemDateFormat(item)}</Text>
                </View>
                <View className={style.tab}>
                  <View className={style.tabLabel}>地点：</View>
                  <Text className={style.tabDesc}>{item.activityAddress}</Text>
                </View>
              </View>
              <View className={style.ticketShearLine}></View>
              {/* 10 待验证 就是 已支付 */}
              {item.status === 10 ? (
                <View className={style.ticketCodeBox}>
                  <View className={style.ticketCodeTit}>核销码</View>
                  <View className={style.ticketNum}>{item.verificationCode}</View>
                  <View className={style.ticketCode}>
                    <Image className={style.qrCode} src={item.verificationImage.replace(/[\r\n]/g, '')} />
                  </View>
                  <View className={style.tickHasCheck}>{TicketStatusobj[TicketStatus[item.status]]}</View>
                </View>
              ) : (
                <View className={style.ticketCodeBox}>
                  <View className={style.tickTitle}>{TicketStatusobj[TicketStatus[item.status]]}</View>
                </View>
              )}
            </View>
          </View>
        );
      })}
      {shareable && list[0].status === 10 && (
        <View className={style.footer}>
          <Button className={style.submitBtn} openType='share'>
            分享电子票
          </Button>
        </View>
      )}
    </View>
  );
};

export default Tickets;
