/* eslint-disable react/sort-comp */
// @externalClassesConvered(AnimatDialog)
import {_safe_style_} from '@/wxat-common/utils/platform';
import {Block, Button, Canvas, Icon, Image, Swiper, SwiperItem, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import {connect} from 'react-redux';
import hoc from '@/hoc/index';
import React, {ComponentClass, createRef} from 'react';
import filters from '@/wxat-common/utils/money.wxs.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import report from '@/wxat-common/../sdks/buried/report/index.js';
import template from '@/wxat-common/utils/template.js';
import industryEnum from '@/wxat-common/constants/industryEnum.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import utils from '@/wxat-common/utils/util.js';
import subscribeMsg from '@/wxat-common/utils/subscribe-msg.js';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum.js';
import shareUtil from '@/wxat-common/utils/share.js';
import cutEnum from '@/wxat-common/constants/cutEnum.js';
import {bindPromoterEnum} from '@/wxat-common/constants/promoterEnum.js';
import FrontMoneyItem from '@/wxat-common/components/front-money-item/index';
import ShareDialog from '@/wxat-common/components/share-dialog/index';
import CombinationItemDetail from '@/wxat-common/components/combination-item-detail/index';
import checkOptions from '@/wxat-common/utils/check-options';
// import AuthUser from '@/wxat-common/components/auth-user';
import DetailParser from '@/wxat-common/components/detail-parser/index';
import CommentModule from '@/wxat-common/components/comment-module/index';
import RuleDesc from '../rule-desc';
import ActivityInfo from '../activity-info';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import TaskError from '@/wxat-common/components/task-error/index';
import PromoterError from '@/wxat-common/components/promoter-error/index';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import BZSceneUtil from '@/wxat-common/utils/bz-scene-util';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import {qs2obj} from '@/wxat-common/utils/query-string';

import './index.scss';

import {updateBaseAction} from '@/wxat-common/../redux/base';
import screen from '@/wxat-common/utils/screen';
import {SkCodeComponent} from '@/decorators/SkCode';
import {getDecorateHomeData} from '@/wxat-common/x-login/decorate-configuration-store';
import DialogModule from '@/wxat-common/components/base/dialogModule/index';
import PaDetailTmpl from '@/sub-packages/marketing-package/components/tpl/pa-detail.js';
import PaDetailWxworkTmpl from '@/sub-packages/marketing-package/components/tpl/pa-detail-wxword.js';
import PostersDialog from '@/sub-packages/marketing-package/components/posters-dialog';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'

// redux state的在这里定义
interface PageStateProps {
  globalData: any;
  base: any;
  homeChangeTime: any;
  environment: any;
  currentStore: any;
  iosSetting:any;
  isIOS:boolean
}

// redux 的action方法再这里定义
interface PageDispatchProps {
  updateBase: (data) => void;
}

// 父子组件直接传递的props
interface PageOwnProps {}
// 组件的state声明
interface PageState {}
type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
interface CutPriceDetail {
  props: IProps;
}

const mapDispatchToProps = (dispatch) => ({
  updateBase: (data) => dispatch(updateBaseAction(data)),
});

const mapStateToProps = (state: any) => ({
  globalData: state.globalData,
  base: state.base,
  currentStore: state.base.currentStore,
  environment: state.globalData.environment,
  homeChangeTime: state.globalData.homeChangeTime,
  iosSetting:state.globalData.iosSetting,
  isIOS:state.globalData.isIOS
});

const storesAll = 1; // 所有门店
const storesEvery = 2; // 每个门店

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class CutPriceDetail extends SkCodeComponent<IProps, PageState> {
  private getRuleDiaogCMTP: any;
  state = {
    dataLoad: false,
    error: false,
    activityId: null, // 活动id
    activity: null, // 砍价活动信息
    industry: this.props.globalData.industry,
    tmpStyle: {},
    uniqueShareInfoForQrCode: null,
    // 砍价规则
    cutPriceRule: {},
    contactConfig: null,
    showContent: false,
    showContentBtn: false,
    isRefuserPromoter: false,
  } as Record<string, any>;
  iosPayLimitDialog = React.createRef<any>()
  refShareDialogCMTP = createRef<any>();
  refShareDialogCMPT = React.createRef<any>();
  promoterCommission: string;
  extraData: Record<string, any>;
  isPaDetail = false;
  refUseId: number;
  storeId: number;
  activityId: number;
  promoterActivityId = null;
  itemNo: string;
  bindPromoterEnum = bindPromoterEnum;
  isDisable: boolean = false;
  btnText: string;
  // activityId 活动id

  init = async (options: any = {}) => {
    const { environment, currentStore } = this.props;
    if (typeof options.scene === 'string') {
      options = qs2obj(options.scene);
    }
    const params = options;
    this.extraData = params;
    if (!!params.itemNo || !!params.activityId) {
      this.refUseId = params.refUseId || params._ref_useId || null;
      this.storeId = params.refStoreId || params._ref_storeId || null;
      this.activityId = params.activityId;
      this.promoterActivityId = params.promoterActivityId;
      this.itemNo = params.itemNo || params.id || null;
    }
    // 推广大使活动
    if (options.isPaDetail) {
      this.isPaDetail = true;
      this.promoterCommission = options.promoterCommission;
      this.promoterActivityId = options.promoterActivityId;
      if (this.refUseId) {
        const data = await this.getRefuserPromoter(this.refUseId);
        const isRefuserPromoter = data !== this.bindPromoterEnum.pass;
        this.setState({
          isRefuserPromoter,
        });

        if (isRefuserPromoter) {
          return;
        }
      }
    }
    // 推广大使强制切换门店
    const storeId = this.storeId;
    if (
      (params.activityId && params.promoterActivityId && storeId && currentStore.id != storeId) ||
      params.assignStore
    ) {
      await checkOptions.changeStore(storeId);
    }

    if (!!options && !!options.scene) {
      const scene = '?' + decodeURIComponent(options.scene);
      const activityId = utils.getQueryString(scene, 'activityId');
      this.setState(
        {
          activityId: activityId,
        },

        () => {
          if (this.props.base.sessionId) {
            this.initRenderData();
          }
        }
      );
    } else {
      const keys = Object.keys(options);
      const initData = {};
      keys.forEach((key) => (initData[key] = options[key]));
      this.setState(initData, () => {
          if (this.props.base.sessionId) {
            this.initRenderData();
          }
      });
    }
    // 推广大使微信端禁止分享
    if (environment !== 'wxwork' && this.isPaDetail) {
      wxApi.hideShareMenu();
    }
  };

  // FIXME: 没有被触发
  handleRuleLoad = (e) => {
    this.setState({
      cutPriceRule: e || {},
    });
  };

  initRenderData = () => {
    if (this.state.dataLoad) {
      return;
    }
    this.setState({ dataLoad: true });
    this.handleDetail();
    this.getTemplateStyle();
  };

  // 判断是否是导购的推广大使
  getRefuserPromoter = async (employeeId) => {
    const params = {
      employeeId,
    };

    const { data } = await wxApi.request({
      url: api.user.queryBindAmbassador,
      data: params,
    });

    return data;
  };

  handleDetail = () => {
    const { activityId } = this.state;
    if (!activityId) return;
    const params = {
      bargainId: activityId, // 活动id
      initiate: true, // 是否继续砍价，发起界面为true
    };
    wxApi
      .request({
        url: api.cut_price.detail,
        data: params,
      })
      .then((res) => {
        const data = res.data;
        if (!data) {
          this.apiError();
          return;
        } else {
          this.setState({ error: false });
        }
        data.wxItem = data.wxItem || {};
        wxApi.setNavigationBarTitle({
          title: (data.wxItem && data.wxItem.name) || '砍价详情',
        });

        // 业务场景埋点
        BZSceneUtil.add({
          sceneType: shareUtil.ShareBZs.CUT_PRICE_DETAIL,
          sceneId: data.id,
          activityId: data.id,
          activityType: 'bargain',
          activityName: data.activityName,
          itemType: data.itemType,
          itemNo: data.itemNo,
          itemName: data.itemName,
        });

        this.itemNo = data.itemNo;
        const activity = data;
        this.isDisable =
          activity &&
          (
            activity.itemStock === 0 ||
            this.judgeNumber(activity) ||
            activity.activityStatus === cutEnum.activityStatus.NO_START);
        if (activity.itemType === 41) {
          this.isDisable = false
        }
        this.btnText =
          activity &&
          (this.judgeNumber(activity)
            ? '商品已被抢光'
            : activity.itemStock === 0 && activity.itemType !== 41
            ? '门店库存不足'
            : activity.activityStatus === cutEnum.activityStatus.NO_START
            ? filters.dateFormat(activity.startTime, 'MM-dd hh:mm') + '开始'
            : '立即砍价');
        this.setState({
          activity: data,
          uniqueShareInfoForQrCode: {
            activityId: this.state.activityId,
            promoterActivityId: this.promoterActivityId,
            itemNo: data.itemNo,
            page: 'sub-packages/marketing-package/pages/cut-price/detail/index',
            refBz: shareUtil.ShareBZs.CUT_PRICE_DETAIL,
            bzscene: shareUtil.ShareBzscene.CUT_PRICE,
            refBzId: this.state.activityId,
            refBzName: `砍价活动${data.activityName ? '-' + data.activityName : ''}`,
            refSceneName: data.activityName,
          },
        });
      })
      .catch((err) => {
        this.apiError();
      });
  };

  apiError() {
    this.setState({ error: true });
  }

  onShareAppMessage() {
    report.share(true);
    this.refShareDialogCMTP.current ? this.refShareDialogCMTP.current.hide() : null;
    const { activity, activityId } = this.state;
    const pageUrl = `/sub-packages/marketing-package/pages/cut-price/detail/index`;
    let url = '';
    const { environment } = this.props;
    const extraData = this.extraData;
    const itemNo = this.itemNo;
    if (environment === 'wxwork' && this.isPaDetail) {
      // 企微环境中推广给大使
      url = `${pageUrl}?isPaDetail=1&itemNo=${itemNo}&activityId=${activityId}&promoterCommission=${extraData.promoterCommission}&activityStatus=${extraData.activityStatus}`;
    } else {
      url = `${pageUrl}?activityId=${activityId}`;
    }
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.CUT_PRICE_DETAIL,
      bzName: `砍价活动${activity.activityName ? '-' + activity.activityName : ''}`,
      bzscene: shareUtil.ShareBzscene.CUT_PRICE,
      bzId: activityId,
      sceneName: (activity && activity.activityName) || '',
    });

    return {
      title: activity.activityName ? activity.activityName : '',
      path,

      fail: function (res) {
        report.share(false);
        // 转发失败
      },
    };
  }

  /**
   * 打开邀请好友对话弹框
   */
  handleSelectChanel = () => {
    this.refShareDialogCMTP.current ? this.refShareDialogCMTP.current.show() : null;
  };

  // 保存图片
  handleSavePosterImage = (e) => {
    this.refShareDialogCMTP.current ? this.refShareDialogCMTP.current.savePosterImage() : null;
  };

  // 发起砍价
  handleInitate = () => {
    if (!authHelper.checkAuth()) {
      return;
    }

    const _iosSetting = this.props.iosSetting
    if(_iosSetting?.payType && this.state.activity.wxItem.type == goodsTypeEnum.VIRTUAL_GOODS.value && _iosSetting.payType  === 3){
      this.iosPayLimitDialog.current.showIospayLimitDialog();
      return
    }

    const { activity } = this.state;

    // isShelf  门店下架; 0: 已下架; 1: 上架

    // itemStock 可用库存数
    if (this.isDisable) {
      const text = activity.activityStatus === cutEnum.activityStatus.NO_START ? '砍价活动未开始' : this.btnText;
      return wxApi.showToast({
        icon: 'none',
        title: `${text}`,
      });
    }

    const { act_id, act_name, item_no, item_name, item_type } = BZSceneUtil.get.auth;

    // 上报-点击发起砍价
    report.reportClickStartBargain({
      act_id,
      act_name,
      item_no,
      item_name,
      item_type,
    });

    // 充值成功的订阅消息
    const Ids = [subscribeEnum.BARGAIN_END.value, subscribeEnum.BARGAIN_SUCCESS.value];
    // 授权订阅消息
    subscribeMsg.sendMessage(Ids).then(() => {
      const id = this.state.activityId;
      let params = {};
      params = {
        bargainId: id,
      };

      // 推广大使发起砍价带推广大使 id
      if (this.promoterActivityId) {
        params = {
          bargainId: id,
          promoterId: this.refUseId,
          activityId: this.promoterActivityId,
        };
      }
      report.startDiscount(id);
      wxApi
        .request({
          url: api.cut_price.initiate,
          loading: true,
          data: params,
        })
        .then((res) => {
          // 上报-成功发起砍价
          report.reportSuccessStartBargain({
            act_id,
            act_name,
            item_no,
            item_name,
            item_type,
          });

          const data = res.data;
          const url = `/sub-packages/marketing-package/pages/cut-price/join/index?bargainInitiateId=${data.id}&bargainId=${data.bargainId}&isCut=true`;
          wxApi.$navigateTo({ url });
        })
        .catch((error) => {
        });
    });
  };

  handleGoHome = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  refRuleDiaogCMTP = (node) => (this.getRuleDiaogCMTP = node);

  onViewRule = () => {
    this.getRuleDiaogCMTP.show({ scale: 1 });
  };

  handleCloseRule = () => {
    this.getRuleDiaogCMTP.hide();
  };

  onLoad(options) {
    this.init(options);

    // 首页配置
    const homeConfig = getDecorateHomeData();
    if (homeConfig && homeConfig.length) {
      const contactConfig = homeConfig.find((item) => item.id === 'dialogModule') || null;
      this.setState({ contactConfig });
    }
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.homeChangeTime !== this.props.homeChangeTime) {
      // 首页配置
      const homeConfig = getDecorateHomeData();
      if (homeConfig && homeConfig.length) {
        const contactConfig = homeConfig.find((item) => item.id === 'dialogModule') || null;
        this.setState({ contactConfig });
      }
    }
    if(this.props.base.sessionId !== nextProps.base.sessionId){
      this.initRenderData();
    }
  }

  switchConten = () => {
    this.setState({
      showContent: !this.state.showContent,
    });
  };

  componentDidMount() {
    setTimeout(() => {
      this.getContentHeight();
    }, 4000);
  }

  getContentHeight = () => {
    if (wxApi.canIUse('createSelectorQuery')) {
      wxApi
        .createSelectorQuery()
        .select('#refCutChild')
        .boundingClientRect((rect) => {
          if (rect && rect.height && rect.height > 60) {
            this.setState({
              showContentBtn: true,
            });
          }
        })
        .exec();
    }
  };

  judgeNumber = (activity = {}) => {
    return (
      (activity && activity.saleLimitType === storesAll && activity.saleLimitRemain <= 0) ||
      (activity && activity.saleLimitType === storesEvery && activity.storeSaleLimitRemain <= 0)
    );
  };

  handleShare() {
    this.refShareDialogCMPT.current.show(this);
  }

  // 保存图片
  onSavePosterImage = () => {
    this.refShareDialogCMPT.current.savePosterImage(this);
  };

  render() {
    const {
      error,
      activity,
      dataLoad,
      // goodsDetail,
      industry,
      tmpStyle,
      cutPriceRule,
      uniqueShareInfoForQrCode,
      contactConfig,
      showContent,
      showContentBtn,
      isRefuserPromoter,
    } = this.state;

    const { currentStore, environment,isIOS,iosSetting } = this.props;
    const extraData = this.extraData || {};
    const isPaDetail = this.isPaDetail;
    const isFullScreen = screen.isFullScreenPhone;
    const promoterCommission = this.promoterCommission;
    const isDisable = this.isDisable;
    const btnText = this.btnText;

    // isShelf  门店下架
    // 0: 已下架
    // 1: 上架

    // itemStock 可用库存数

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pccd-Detail' className='wk-pccd-Detail'>
        {/* <AuthUser /> */}
        {isRefuserPromoter ? (
          <PromoterError></PromoterError>
        ) : error ? (
          <TaskError onCallSomeFun={this.handleDetail}></TaskError>
        ) : (
          activity && (
            <View className='goods-detail-container'>
              <Swiper className='swiper' circular indicatorDots={activity.materials && activity.materials.length > 1}>
                {activity.materials.map((item, index) => {
                  return (
                    <Block key={'goods-item-' + index}>
                      <SwiperItem className='goods-item'>
                        <View
                          className='goods-image'
                          style={_safe_style_(
                            'background: transparent url(' + item + ') no-repeat 50% 50%;background-size: cover;'
                          )}
                        ></View>
                      </SwiperItem>
                    </Block>
                  );
                })}
              </Swiper>
              {/*  商品基础信息  */}
              <View>
                <View className='goods-info'>
                  <View className='goods-title'>
                    <View className='name-text limit-line line-5'>
                      {!!(!!activity && !!activity.wxItem && !!activity.wxItem.drugType) && (
                        <Text className='drug-tag' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                          处方药
                        </Text>
                      )}

                      {activity.activityName}
                    </View>
                    {activity.itemType === goodsTypeEnum.PRODUCT.value && (
                      <Text className='stock-text'>{'库存：' + (activity.itemStock || 0)}</Text>
                    )}
                  </View>
                  {!!(!!dataLoad && !!activity) && (
                    <ActivityInfo activity={activity} selfstyle='margin:30rpx 0;'></ActivityInfo>
                  )}
                </View>
                <View className="tag-box rule-info" onClick={this.onViewRule}>
                  <View className="tag-item">规则说明</View>
                  <View className="more-rule">
                    <Text>支付砍价，邀请好友参与，成功发货，失败退款</Text>
                    <Image
                      className="img-box"
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                    ></Image>
                  </View>
                </View>
                {/*  砍价说明  */}
                {/* {((cutPriceRule && cutPriceRule.content) || (activity && activity.rulesContent)) && (
                  <View className='cut-desc'>
                    <Text className='text'>{activity.rulesTitle || cutPriceRule.title}</Text>
                    <View>
                      <View className={showContent ? 'autoHeight content' : 'content'}>
                        <View id='refCutChild'>{activity.rulesContent || cutPriceRule.content}</View>
                      </View>
                      {showContentBtn && (
                        <View className='shrinkageBtn' onClick={this.switchConten}>
                          {showContent ? '收起' : '全部展开'}
                        </View>
                      )}
                    </View>
                  </View>
                )} */}

                {/* 定金模式 */}
                {!!activity.wxItem.frontMoneyItem && (
                  <FrontMoneyItem
                    leftRightMargin={0}
                    leftRightPadding={30}
                    frontMoney={activity.wxItem.frontMoney}
                  ></FrontMoneyItem>
                )}

                {/*  组合商品明细  */}
                {!!(activity.wxItem.combinationDTOS && activity.wxItem.combinationDTOS.length > 0) && (
                  <CombinationItemDetail
                    combinationDtos={activity.wxItem.combinationDTOS}
                    style={{ display: 'block', borderBottom: `${Taro.pxTransform(1)} solid #e5e5e5` }}
                  ></CombinationItemDetail>
                )}

                {/*  评论  */}
                {!!(dataLoad && activity.itemNo && industry !== industryEnum.type.beauty.value) && (
                  <CommentModule
                    showHeader
                    isBrief
                    commentTitle={activity.itemName}
                    itemNo={activity.itemNo}
                  ></CommentModule>
                )}

                {!!activity && <DetailParser itemDescribe={activity.describe}></DetailParser>}
              </View>
              {/* {activity.isShelf === 0 ? <View className='footer2 btn-disabled'>门店已下架</View> : ''} */}
              <View className={'footer ' + (screen.isFullScreenPhone ? 'fix-full-screen' : '')}>
                <View className='icon-btns'>
                  <Button className='btn' onClick={this.handleGoHome}>
                    <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-home.png"></Image>
                    <View className='text'>首页</View>
                  </Button>
                  <Button className='btn' onClick={this.handleSelectChanel}>
                    <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-share.png"></Image>
                    <View className='text'>分享</View>
                  </Button>
                </View>
                {/* activityStatus: {
              NO_START: 0, //未开始
              CUTING: 1, //进行中
              END: 2, //已结束
              },
              isShelf --> 门店下架; 0: 已下架; 1: 上架;  itemStock --> 可用库存数
              }; */}

                {isPaDetail ? (
                  <Block>
                    {environment !== 'wxwork' ? (
                      // {/* 推广大使-微信环境 */}
                      <Block>
                        <PaDetailTmpl
                          isFullScreen={isFullScreen}
                          goodsDetail={activity}
                          tmpStyle={tmpStyle}
                          promoterCommission={promoterCommission}
                          onShare={this.handleShare.bind(this)}
                        ></PaDetailTmpl>
                        {/*  推广活动分享海报  */}
                        <PostersDialog
                          childRef={this.refShareDialogCMPT}
                          posterName={activity.wxItem.name}
                          posterImage={activity.wxItem.thumbnail}
                          uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
                        ></PostersDialog>
                        <Canvas canvasId='shareCanvas' className='promotion-canvas'></Canvas>
                      </Block>
                    ) : (
                      // {/* 企业微信环境打开 */}
                      <PaDetailWxworkTmpl
                        isFullScreen={isFullScreen}
                        goodsDetail={activity}
                        tmpStyle={tmpStyle}
                        extraData={extraData}
                      ></PaDetailWxworkTmpl>
                    )}
                  </Block>
                ) : (
                  <Block>
                    {environment === 'wxwork' && !isDisable ? (
                      // 导购分享
                      <ButtonWithOpenType
                        openType='share'
                        className="btn-initate"
                        style={_safe_style_('background:' + tmpStyle.bgGradualChange + ';')}
                      >
                        分享
                      </ButtonWithOpenType>
                    ) : (

                        isIOS && iosSetting?.payType && activity.wxItem.type == 41  ? <Button onClick={this.handleInitate} className={activity.activityStatus === cutEnum.activityStatus.NO_START ? 'btn-disabled btn-initate' : 'btn-initate'} style={{'background':tmpStyle.bgGradualChange}}>{activity.activityStatus === cutEnum.activityStatus.NO_START ? filters.dateFormat(activity.startTime,'MM-dd hh:mm') + '开始' : iosSetting.buttonCopywriting}</Button> :
                        <Button
                        onClick={this.handleInitate}
                        className={'btn-initate ' + (isDisable ? 'btn-disabled' : '')}
                        style={_safe_style_('background:' + tmpStyle.bgGradualChange)}
                      >
                        {btnText}
                      </Button>

                    )}
                  </Block>
                )}
              </View>
            </View>
          )
        )}
        <AnimatDialog animClass='rule-dialog' ref={this.refRuleDiaogCMTP}>
          {!!dataLoad && <RuleDesc onRule={this.handleRuleLoad}></RuleDesc>}
          <Icon type='clear' className='close' onClick={this.handleCloseRule}></Icon>
        </AnimatDialog>
        {/*  分享对话弹框  */}
        {!!(!!cutPriceRule && !!activity) && (
          <ShareDialog
            posterHeaderType={cutPriceRule.posterType || 1}
            posterTips={cutPriceRule.posterContent || '物美价廉的好货，赶紧来砍价吧！'}
            posterLogo={cutPriceRule.posterLogo}
            posterSalePriceLabel='最低价'
            posterLabelPriceLabel='活动结束价'
            posterName={activity.activityName}
            posterImage={activity.thumbnail}
            salePrice={filters.moneyFilter(activity.lowestPrice, true)}
            labelPrice={filters.moneyFilter(activity.salePrice, true)}
            onSave={this.handleSavePosterImage}
            uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
            childRef={this.refShareDialogCMTP}
          />
        )}

        {/* 客服/导购悬浮窗 */}
        {!!contactConfig && !!contactConfig.config && !!contactConfig.config.showInGoods && (
          <DialogModule dataSource={contactConfig.config} />
        )}

        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='share-canvas'></Canvas>
        <AuthPuop></AuthPuop>
        <HomeActivityDialog showPage={'sub-packages/marketing-package/pages/cut-price/detail/index?activityId='+ this.state.activityId}></HomeActivityDialog>
        <IosPayLimitDialog ref={this.iosPayLimitDialog} tipsCopywriting={iosSetting?.tipsCopywriting}></IosPayLimitDialog>
      </View>
    );
  }
}

export default CutPriceDetail as ComponentClass;
