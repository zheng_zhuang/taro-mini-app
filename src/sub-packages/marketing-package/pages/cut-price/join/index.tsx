import {$getRouter} from 'wk-taro-platform'; /* eslint-disable react/sort-comp */
// @externalClassesConvered(AnimatDialog)
import {_fixme_with_dataset_, _safe_style_} from '@/wxat-common/utils/platform';
import {Block, Button, Canvas, Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import goodsTypeFilters from '@/wxat-common/utils/goodsType.wxs.js';
import {connect} from 'react-redux';
import hoc from '@/hoc/index';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import timer from '@/wxat-common/utils/timer.js';
import report from '@/wxat-common/../sdks/buried/report/index';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import industryEnum from '@/wxat-common/../wxat-common/constants/industryEnum.js';
import cutEnum from '@/wxat-common/constants/cutEnum.js';
import utils from '@/wxat-common/utils/util.js';
import dateUtils from '@/wxat-common/utils/date.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import shareUtil from '@/wxat-common/../wxat-common/utils/share.js';
import ShareDialog from '@/wxat-common/components/share-dialog/index';
import Success from '../success';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import './index.scss';

import React, {ComponentClass} from 'react';
import {updateBaseAction} from '@/redux/base';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import BZSceneUtil from '@/wxat-common/utils/bz-scene-util';
import {qs2obj} from '@/wxat-common/utils/query-string';
import checkOptions from '@/wxat-common/utils/check-options';
import {WaitComponent} from '@/decorators/Wait';

const ONE_DAY = 24 * 3600;

// redux state的在这里定义
interface PageStateProps {
  globalData: any;
  promotion: any;
  shareId: any;
  targetType: any;
  vip: any;
  isFirst: any;
  sessionId: any;
  userInfo: any;
  loginInfo: any;
  currentStore: any;
}

// redux 的action方法再这里定义
interface PageDispatchProps {
  updateBase: (data) => void;
}

// 父子组件直接传递的props
interface PageOwnProps {
  iosSetting:any
  isIOS:boolean
}
// 组件的state声明
interface PageState {}
type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface CutPriceJoin {
  props: IProps;
}

const mapDispatchToProps = (dispatch) => ({
  updateBase: (data) => dispatch(updateBaseAction(data)),
});

const mapStateToProps = (state) => ({
  globalData: state.globalData,
  promotion: state.base.promotion || null,
  shareId: state.base.shareId,
  targetType: state.base.targetType,
  vip: state.base.vip,
  isFirst: state.base.isFirst || null,
  sessionId: state.base.sessionId,
  userInfo: state.base.userInfo,
  loginInfo: state.base.loginInfo,
  currentStore: state.base.currentStore,
  iosSetting:state.globalData.iosSetting,
  isIOS:state.globalData.isIOS
});

const storesAll = 1; // 所有门店
const storesEvery = 2; // 每个门店

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class CutPriceJoin extends WaitComponent {
  $router = $getRouter();
  state = {
    option: null,
    industryEnum,
    extConfig: null,
    activityId: null,
    describeHtml: null,
    activity: null,
    remainTime: null,
    currentType: 0,
    showType: {
      detail: 0,
      comment: 1,
    },

    currentUserGroupIndex: 0,
    userSwiperHeight: 0,
    industry: this.props.globalData.industry,
    bargainId: null,
    bargainInitiateId: null,
    visible: false,
    qrCode: null,
    isHelpCut: false,
    tmpStyle: {},
    displayTime: '',
    uniqueShareInfoForQrCode: null,
    _thredId: null,
    userGroup: [],
    bargainPrice: null,
    cutPriceRule: null,
    dataLoad: false,
  };

  private successDialogRef = React.createRef<AnimatDialog>();
  private shareDialogRef = React.createRef();

  get rate() {
    const activity = this.state.activity;
    if (!activity) {
      return 0;
    }
    const diff = activity.salePrice - activity.lowestPrice;
    if (diff === 0) {
      return 1;
    }
    return activity.totalBargainPrice / diff;
  }

  get isFaild() {
    const { activity, remainTime } = this.state;

    if (!activity) {
      return false;
    }
    if (activity.initiateStatus === 0) {
      // 后端判断超时
      return true;
    }
    if (remainTime != null && remainTime <= 0 && activity.initiateStatus === 1) {
      // 砍价中，但时间已经超时，置为失败
      return true;
    }
    return false;
  }

  get isSuccess() {
    const { activity, remainTime } = this.state;
    if (!activity) {
      return false;
    }
    if (activity.initiateStatus === 3 || activity.initiateStatus === -2 || activity.initiateStatus === -3) {
      // 已支付 申请退款 退款成功，计入砍价成功
      return true;
    }
    if (activity.initiateStatus === 2 && remainTime > 0) {
      // 砍到底价并没超时，计入成功
      return true;
    }
    return false;
  }

  componentDidShow = () => {
    this.setState({
      _thredId: timer.addQueue(() => {
        this.updateTime();
      }),
    });
  };

  componentDidHide = () => {
    if (this.state._thredId) {
      timer.deleteQueue(this.state._thredId);
      this.setState({
        _thredId: null,
      });
    }
  };

  // bargainInitiateId 必选 参与砍价id
  // bargainId 必选 活动id
  onLoad = async (option) => {
    const { promotion, shareId, targetType, vip, isFirst, updateBase, currentStore } = this.props;
    let bargainId = null;
    let bargainInitiateId = null;
    let scene = option;
    if (typeof option.scene === 'string') {
      scene = qs2obj(option.scene);
    }
    // 砍价切换门店
    if (scene.refStoreId && scene.refStoreId !== currentStore.id) {
      await checkOptions.changeStore(scene.refStoreId);
    }
    if (option.scene) {
      const sceneStr = '?' + decodeURIComponent(option.scene);
      const mixNo = utils.getQueryString(sceneStr, 'No');
      const mixNoArr = mixNo.split('_');
      bargainId = mixNoArr[0];
      bargainInitiateId = mixNoArr[1];
      this.setState(
        {
          bargainId,
          bargainInitiateId,
          option,
        },

        () => {
          this.initRender();
        }
      );
    } else {
      const keys = Object.keys(option);
      const initData = {};
      keys.forEach((key) => (initData[key] = option[key]));
      bargainId = initData.bargainId;
      bargainInitiateId = initData.bargainInitiateId;
      this.setState(
        {
          initData,
          option,
        },

        () => {
          this.initRender();
        }
      );
    }

    if (option.isCut) {
      // this.successDialogRef.current
      //   ? this.successDialogRef.current.show({
      //       scale: 1,
      //     })
      //   : null;
      // 推广任务 && 携带分享Id && 邀请新用户 && 非会员 && 首次发起砍价
      if (promotion && shareId && targetType && !vip && isFirst) {
        this.addInviteRecord(shareId, bargainInitiateId);
        updateBase({
          isFirst: false,
        });
      }
    }
  };

  initRender() {
    const { sessionId } = this.props;
    const { dataLoad } = this.state;
    if (dataLoad) {
      return;
    }
    if (sessionId) {
      this.setState({ dataLoad: true });
      this.initDetail();
    }
  }

  initDetail = () => {
    this.getTemplateStyle();
    this.getDetail();
    this.getAssistList();
  };

  onHelpCut = () => {
    if (authHelper.checkAuth()) {
      const data = this.state.activity;
      // isShelf  门店下架; 0: 已下架; 1: 上架

      // itemStock 可用库存数
      // if (data.isShelf === 0) {
      //   wxApi.showToast({
      //     icon: 'none',
      //     title: '门店已下架',
      //   });

      //   return;
      // }
      if (this.judgeNumber(data)) {
        wxApi.showToast({
          icon: 'none',
          title: '商品已被抢光',
        });

        return;
      }
      if (data.itemStock === 0 && data.itemType !== 41) {
        wxApi.showToast({
          icon: 'none',
          title: '门店库存不足',
        });

        return;
      }
      if (data && !data.oneself && !data.haveBargain && data.initiateStatus === 1) {
        // 不是自己发起的砍价，并没有参与别人的砍价，并且是砍价中状态
        this.onAssist();
      }
    }
  };

  // 获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  // 获取砍价详情
  getDetail = () => {
    const { bargainId, bargainInitiateId } = this.state;
    const params = {
      bargainId, // 活动id
      initiate: false, // 是否参与砍价，join界面为false
      bargainInitiateId, // 用户发起砍价的id
    };
    wxApi
      .request({
        url: api.cut_price.detail,
        loading: true,
        data: params,
      })
      .then((res) => {
        const data = res.data;
        if (!data) {
          this.apiError();
          return;
        }
        wxApi.setNavigationBarTitle({
          title: data.activityName || '砍价详情',
        });

        const describe = data.describe;
        if (describe) {
          try {
            // WxParse('describeHtml', 'html', describe, this, 5);
            // WxParse('describeHtml', 'html', describe, this, 5);
          } catch (e) {
          }
        }

        // if (this.state.option && !!this.state.option.isCut) {
        //   this.successDialogRef.current
        //     ? this.successDialogRef.current.show({
        //         scale: 1,
        //       })
        //     : null;
        // }

        this.setState({
          activity: data,
          uniqueShareInfoForQrCode: {
            verificationNo: data.id + '_' + data.bargainInitiateId,
            activityId: data.id,
            page: 'sub-packages/marketing-package/pages/cut-price/join/index',
            refBz: shareUtil.ShareBZs.CUT_PRICE_JOIN,
            bzscene: shareUtil.ShareBzscene.CUT_PRICE,
            refBzId: data.id,
            refBzName: `砍价活动${data.activityName ? '-' + data.activityName : ''}`,
            refSceneName: data.activityName,
          },
        });

        // 业务场景埋点
        BZSceneUtil.add({
          sceneType: shareUtil.ShareBZs.CUT_PRICE_JOIN,
          sceneId: data.id,
          activityId: data.id,
          activityType: 'bargain',
          activityName: data.activityName,
          bargainInitiateId: data.bargainInitiateId,
          itemType: data.itemType,
          itemNo: data.itemNo,
          itemName: data.itemName,
        });

        this.updateTime();
      })
      .catch((error) => {
        this.apiError(error);
      });
  };

  // 获取多少人帮砍
  getAssistList = () => {
    const { userInfo } = this.props;
    wxApi
      .request({
        url: api.cut_price.assistList,
        data: {
          bargainInitiateId: this.state.bargainInitiateId,
          pageNo: 1,
          pageSize: 100,
        },
      })
      .then((res) => {
        const userGroup = [];
        let users = [];
        let tick = 0;
        userGroup.push(users);
        (res.data || []).forEach((user) => {
          if (tick >= 4) {
            tick = 0;
            users = [];
            userGroup.push(users);
          }
          users.push(user);
          tick++;
          if (userInfo.id === user.userId) {
            this.setState({
              isHelpCut: true,
            });
          }
        });
        this.setState({
          userSwiperHeight: userGroup[this.state.currentUserGroupIndex].length * 90,
          userGroup,
        });
      })
      .catch((error) => {
      });
  };

  apiError = () => {};

  // 任务中心邀请上报
  addInviteRecord = (shareId, referId) => {
    const data = {
      shareId,
      referId,
    };

    wxApi.request({
      url: api.taskCenter.addInviteRecord,
      data,
      method: 'POST',
    });
  };

  /*
   * 关闭砍价成功对话弹框
   */
  handleCloseSuccessDialog = () => {
    this.successDialogRef.current && this.successDialogRef.current.hide(true);
  };

  onCreatePoster = () => {
    this.successDialogRef.current && this.successDialogRef.current.hide(true);
    this.shareDialogRef.current ? this.shareDialogRef.current.show() : null;
  };

  selectShareChanel = () => {
    const activity = this.state.activity;

    // isShelf  门店下架; 0: 已下架; 1: 上架

    // itemStock 可用库存数
    // if (activity.isShelf === 0) {
    //   wxApi.showToast({
    //     icon: 'none',
    //     title: '门店已下架',
    //   });

    //   return;
    // }
    if (this.judgeNumber(activity)) {
      wxApi.showToast({
        icon: 'none',
        title: '商品已被抢光',
      });

      return;
    }
    if (activity.itemStock === 0 && activity.itemType !== 41) {
      wxApi.showToast({
        icon: 'none',
        title: '门店库存不足',
      });

      return;
    }

    this.shareDialogRef.current ? this.shareDialogRef.current.show() : null;
  };

  // 保存图片
  onSavePosterImage = (e) => {
    this.shareDialogRef.current ? this.shareDialogRef.current.savePosterImage(this) : null;
  };

  /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */
  onShareAppMessage = () => {
    const { loginInfo } = this.props;
    this.successDialogRef.current && this.successDialogRef.current.hide(true);
    this.shareDialogRef.current ? this.shareDialogRef.current.hide() : null;
    report.share(true);
    const { activity, bargainId, bargainInitiateId } = this.state;
    // 统计上报
    if (loginInfo && loginInfo.userId !== activity.initiateUserId) {
      // 帮别人分享砍价
      report.shareHelpDiscount(bargainInitiateId);
    } else {
      // 帮自己分享砍价
      report.shareDiscount(bargainInitiateId);
    }
    const userNickName = activity.initiateUserNickname ? activity.initiateUserNickname : '';
    const path = shareUtil.buildShareUrlPublicArguments({
      url: `/sub-packages/marketing-package/pages/cut-price/join/index?bargainId=${bargainId}&bargainInitiateId=${bargainInitiateId}`,
      bz: shareUtil.ShareBZs.CUT_PRICE_JOIN,
      bzId: bargainId,
      bzscene: shareUtil.ShareBzscene.CUT_PRICE,
      bzName: `砍价活动${activity.activityName ? '-' + activity.activityName : ''}`,
      sceneName: activity.activityName,
    });

    return {
      title: `${userNickName} 我发现一件好货，来一起砍价优惠购！`,
      path,
      fail: function (res) {
        report.share(false);
        // 转发失败
      },
      complete: () => {
        // this.getShareInfo().hide();
      },
    };
  };

  //
  onUserSwiperChange = (e) => {
    this.setState({
      currentUserGroupIndex: e.detail.current,
    });

    this.setState({
      userSwiperHeight: this.state.userGroup[this.state.currentUserGroupIndex].length * 90,
    });
  };

  updateTime = () => {
    if (!this.state.activity) {
      return;
    }

    let remainTime = this.state.remainTime;

    if (remainTime === null) {
      remainTime = this.state.activity.timeRemaining / 1000;
    }

    if (remainTime <= 0) {
      this.setState({
        displayTime: '',
        remainTime: remainTime,
      });

      return;
    }
    const day = parseInt(remainTime / ONE_DAY);
    let mod = remainTime % ONE_DAY;
    const hour = dateUtils.appendZero(parseInt(mod / 3600));
    mod = mod % 3600;
    const minute = dateUtils.appendZero(parseInt(mod / 60));
    const second = dateUtils.appendZero(parseInt(mod % 60));

    const displayTime = day ? `${day}天${hour}:${minute}:${second}` : `${hour}:${minute}:${second}`;
    remainTime -= 1;
    this.setState({
      displayTime: displayTime,
      remainTime,
    });
  };

  onAssist = () => {
    const { act_id, act_name, bargainInitiateId: bargain_id, item_no, item_name, item_type } = BZSceneUtil.get.auth;

    // 上报-点击帮忙砍价
    report.reportClickHelpBargain({
      act_id,
      act_name,
      bargain_id,
      item_no,
      item_name,
      item_type,
    });

    wxApi
      .request({
        url: api.cut_price.assist,
        loading: true,
        quite: true,
        data: {
          bargainId: this.state.bargainId,
          bargainInitiateId: this.state.bargainInitiateId,
        },
      })
      .then((res) => {
        const totalBargainPrice = this.state.activity.totalBargainPrice + res.data;
        this.setState(
          {
            bargainPrice: res.data,
            activity: { ...this.state.activity, totalBargainPrice: totalBargainPrice },
            isHelpCut: true,
          },

          () => {
            this.getAssistList();
            this.successDialogRef.current &&
              this.successDialogRef.current.show({
                scale: 1,
              });
          }
        );

        // 上报-成功帮忙砍价
        report.reportSuccessHelpGroup({
          act_id,
          act_name,
          bargain_id,
          item_no,
          item_name,
          item_type,
        });
      })
      .catch((error) => {
        this.getDetail();
        this.getAssistList();
        this.successDialogRef.current && this.successDialogRef.current.hide(true);
        if (!error.success) {
          wxApi.showToast({
            title: error.data.errorMessage,
            icon: 'none',
            duration: 1500,
          });
        }
      });
  };

  onGoPay = () => {
    if (authHelper.checkAuth()) {
      const { activity, bargainInitiateId } = this.state;
      const goodsDetail = activity;
      // 显示砍价后当前价格
      goodsDetail.currentPrice = goodsDetail.salePrice - goodsDetail.totalBargainPrice;
      const goodsInfoList = [
        {
          isCutPriceOrder: 1,
          id: activity.id,
          pic: activity.thumbnail,
          salePrice: activity.salePrice - activity.totalBargainPrice,
          itemCount: 1,
          noNeedPay: false,
          name: activity.activityName,
          itemNo: activity.itemNo,
          skuId: activity.skuId,
          barcode: activity.barcode,
          skuTreeNames: activity.itemAttrDesc,
          drugType: activity.drugType, // 处方药标识
        },
      ];

      const payDetails = encodeURI(JSON.stringify([
        {
          name: 'freight',
        },
      ]));

      const urlParams = encodeURI(JSON.stringify({
        orderRequestPromDTO: {
          bargainNo: bargainInitiateId,
        },
      }));

      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', goodsInfoList);


      let toUrl = pageLinkEnum.orderPkg.payOrder
      if (goodsDetail.wxItem.itemType === 41) {
        toUrl = '/sub-packages/marketing-package/pages/virtual-goods/detail/pay-order/index'
      }

      wxApi.$navigateTo({
        url: toUrl,
        data: {
          payDetails: payDetails,
          params: urlParams,
          itemNo:goodsDetail.itemNo
        },
      });
    }
  };

  // 我也要参与
  onJoin = () => {
    if (authHelper.checkAuth()) {
      const activity = this.state.activity;

      // isShelf  门店下架; 0: 已下架; 1: 上架

      // itemStock 可用库存数
      // if (activity.isShelf === 0) {
      //   wxApi.showToast({
      //     icon: 'none',
      //     title: '门店已下架',
      //   });

      //   return;
      // }
      if (this.judgeNumber(activity)) {
        wxApi.showToast({
          icon: 'none',
          title: '商品已被抢光',
        });

        return;
      }
      if (activity.itemStock === 0 && activity.itemType !== 41) {
        wxApi.showToast({
          icon: 'none',
          title: '门店库存不足',
        });

        return;
      }

      const id = activity.id;
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/cut-price/detail/index',
        data: {
          activityId: id,
        },
      });
    }
  };

  onGoAppointment = (e) => {
    if (authHelper.checkAuth()) {
      const goodsInfo = e.currentTarget.dataset.detail;
      goodsInfo.name = goodsInfo.itemName;
      const detail = JSON.parse(JSON.stringify(goodsInfo));
      delete detail.describe;
      wxApi.$navigateTo({
        url: '/sub-packages/server-package/pages/appointment/index',
        data: {
          goodsInfo: JSON.stringify(detail),
        },
      });
    }
  };

  onTouchMove = () => {
    return false;
  };

  onGoList = () => {
    if (authHelper.checkAuth()) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/cut-price/list/index',
      });
    }
  };

  // 评论相关
  handleComment = (e) => {
    const curtype = e.currentTarget.dataset.index;
    this.setState({
      currentType: curtype,
    });
  };

  onReachBottom = () => {
    if (this.$scope.selectComponent('#comment')) {
      this.$scope.selectComponent('#comment').onReachBottom();
    }
  };

  stopPopup = () => {
    return false;
  };

  async onWait() {
    const params = this.$router.params;
    if (params) {
      const { bargainId = null, bargainInitiateId = null } = params;
      await new Promise((resolve) => this.setState({ bargainId, bargainInitiateId }, resolve));
      this.onLoad(params);
    }
  }

  judgeNumber = (activity = {}) => {
    return (
      (activity && activity.saleLimitType === storesAll && activity.saleLimitRemain <= 0) ||
      (activity && activity.saleLimitType === storesEvery && activity.storeSaleLimitRemain <= 0)
    );
  };

  render() {
    const {
      activity,
      tmpStyle,
      displayTime,
      isHelpCut,
      userGroup,
      bargainId,
      bargainPrice,
      cutPriceRule,
      uniqueShareInfoForQrCode,
    } = this.state;
    const {isIOS,iosSetting} = this.props
    const { rate, isFaild, isSuccess } = this;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pcj-Join' className='wk-pcj-Join'>
        {/* <AuthUser/> */}
        {!!(!!bargainId && !!activity) && (
          <View className='goods-detail-container'>
            <View className='main-content'>
              <View className='cut-tab cut-activity'>
                <Image src={activity.thumbnail} className='goods-img' />
                <View className='activity-info'>
                  <View className='activity-name'>{activity.activityName}</View>
                  <View className='info-tab'>
                    {activity && activity.oneself ? (
                      <Text className='label-price' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {'砍到 ￥' + filters.moneyFilter(activity.lowestPrice, true) + ' 拿'}
                      </Text>
                    ) : (
                      <Text className='label-price' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {'最低 ' + filters.moneyFilter(activity.lowestPrice, true) + ' 元'}
                      </Text>
                    )}

                    <Text className='sale-count'>
                      {'已有 ' + (activity.bargainTimes + activity.virtualPeople) + ' 人参与'}
                    </Text>
                  </View>
                </View>
              </View>
              <View className='cut-tab cut-info'>
                {!!displayTime && (
                  <View className='remain-time-box'>
                    <View className='remain-time'>
                      {displayTime.split('').map((item, index) => {
                        return (
                          <Text className={item !== '天' && item !== ':' ? 'time-block' : 'symbol'} key={index}>
                            {item}
                          </Text>
                        );
                      })}
                    </View>
                    <View className='remain-time-text'>剩余时间</View>
                  </View>
                )}
              </View>
              <View className='chart'>
                <View className='current'>
                  <View>
                    已砍
                    <Text className='tip-text' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      {filters.moneyFilter(activity.totalBargainPrice, true)}
                    </Text>
                    元，还差
                    <Text className='tip-text' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      {filters.moneyFilter(
                        activity.salePrice - activity.totalBargainPrice - activity.lowestPrice,
                        true
                      )}
                    </Text>
                    元
                  </View>
                </View>
                <View className='bar-container'>
                  {rate > 0 && (
                    <View
                      className='bar'
                      style={_safe_style_(
                        'width:' + ((580 * rate) / 2) + 'px' + ';background:' + tmpStyle.bgGradualChange + ';'
                      )}
                    ></View>
                  )}

                  <View className='pointer' style={_safe_style_('left:' + ((580 * rate - 7) / 2) + 'px')}></View>
                </View>
                <View className='left'>{'￥' + filters.moneyFilter(activity.salePrice, true)}</View>
                <View className='right'>{'￥' + filters.moneyFilter(activity.lowestPrice, true)}</View>
              </View>
              <View className='cut-result'>
                {!!(activity && activity.initiateStatus === cutEnum.STATUS.SUCCESS.value) && (
                  <Text className='text-success' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                    砍价成功
                  </Text>
                )}

                {!!(activity && activity.initiateStatus === cutEnum.STATUS.TIME_OUT.value) && (
                  <Text className='text-failed'>砍价已过期</Text>
                )}
              </View>
              <View className='btn-wrapper'>
                {activity && activity.oneself ? (
                  <Block>
                    {/* PAID 已支付 */}
                    {(activity && activity.initiateStatus === cutEnum.STATUS.PAID.value) || isFaild ? (
                      <Button
                        className='btn-red'
                        onClick={this.onGoList}
                        style={_safe_style_('background:' + tmpStyle.btnColor)}
                      >
                        拿更多砍价商品
                      </Button>
                    ) : (
                      <Block>
                        {/* 没有支付的情况 */}

                        {/* SUCCESS 砍价成功 */}
                        {!!(
                          activity &&
                          activity.initiateStatus === cutEnum.STATUS.SUCCESS.value &&
                          goodsTypeFilters.goodsType(activity.itemNo) === 0
                        ) && (
                          <Button
                            className='btn-yellow'
                            onClick={_fixme_with_dataset_(this.onGoAppointment, { detail: activity })}
                            style={_safe_style_('background:' + tmpStyle.btnColor)}
                          >
                            立即预约
                          </Button>
                        )}

                        {/* 没达到最低，则显示找人帮砍按钮, CUTING 砍价中 */}
                        {!!(activity && activity.initiateStatus === cutEnum.STATUS.CUTING.value) && (
                          <Block>
                            <Button
                              className={
                                'btn-red ' +
                                (activity.itemStock === 0  && activity.itemType !== 41 || this.judgeNumber(activity)
                                  ? 'btn-disabled'
                                  : '')
                              }
                              onClick={this.selectShareChanel}
                              style={_safe_style_('background:' + tmpStyle.btnColor)}
                            >
                              {this.judgeNumber(activity)
                                ? '商品已被抢光'
                                : activity.itemStock === 0 && activity.itemType !== 41
                                ? '门店库存不足'
                                : '找好友砍一刀'}
                            </Button>
                          </Block>
                        )}

                        {/* SUCCESS 砍价成功 */}
                        {!!(
                          (activity && activity.initiateStatus === cutEnum.STATUS.SUCCESS.value) ||
                          (activity && activity.lowestPriceBuy === 0)
                        ) && (
                          <Button
                            className='btn-red'
                            onClick={this.onGoPay}
                            disabled={
                              !displayTime ||
                              (activity && activity.activityStatus === 2) ||
                              (activity && activity.activityStatus === -1)
                            }
                            style={_safe_style_('background:' + tmpStyle.btnColor)}
                          >
                            {!!displayTime &&
                            activity &&
                            activity.activityStatus !== 2 &&
                            activity &&
                            activity.activityStatus !== -1
                              ? isIOS && iosSetting && activity.wxItem.itemType == 41 && iosSetting.buttonCopywriting ? iosSetting.buttonCopywriting:'立即购买'
                              : '砍价已结束'}
                          </Button>
                        )}
                      </Block>
                    )}

                    {/* 还在砍价中。。。 */}
                  </Block>
                ) : (
                  <Block>
                    {!!activity && (activity.activityStatus === 2 || activity.activityStatus === -1) ? (
                      <Button className='btn-red' disabled style={_safe_style_('background:' + tmpStyle.btnColor)}>
                        砍价已结束
                      </Button>
                    ) : !!activity && !isHelpCut && !isSuccess ? (
                      <Button
                        className={
                          'btn-red ' +
                          (activity.itemStock === 0 && activity.itemType !== 41 || this.judgeNumber(activity)
                            ? 'btn-disabled'
                            : '')
                        }
                        onClick={this.onHelpCut}
                        style={_safe_style_('background:' + tmpStyle.btnColor)}
                      >
                        {this.judgeNumber(activity)
                          ? '商品已被抢光'
                          : activity.itemStock === 0 && activity.itemType !== 41
                          ? '门店库存不足'
                          : '帮好友砍一刀'}
                      </Button>
                    ) : (
                      <Button
                        className={
                          'btn-red ' +
                          (activity.itemStock === 0 && activity.itemType !== 41 || this.judgeNumber(activity)
                            ? 'btn-disabled'
                            : '')
                        }
                        onClick={this.onJoin}
                        style={_safe_style_('background:' + tmpStyle.btnColor)}
                      >
                        {this.judgeNumber(activity)
                          ? '商品已被抢光'
                          : activity.itemStock === 0 && activity.itemType !== 41
                          ? '门店库存不足'
                          : '我也要参与'}
                      </Button>
                    )}
                  </Block>
                )}

                {/* 别人参与，只需要显示参与按钮即可 */}
              </View>
            </View>
            <View className='main-content cut-list'>
              {userGroup.map((item, index) => {
                return (
                  <View key={'cut-list' + index}>
                    {item.map((items) => {
                      return (
                        <View className='user-item' key={items.userId}>
                          <Image src={items.userAvatar} className='user-avatar'></Image>
                          <View className='user-name limit-line'>{items.userNickname}</View>
                          <View className='user-help'>
                            {activity && !activity.oneself ? (
                              <Block>小刀一挥砍掉了</Block>
                            ) : (
                              <Block>
                                {activity.initiateUserId === items.userId
                                  ? '小刀一挥帮自己砍掉了'
                                  : '小刀一挥帮你砍掉了'}
                              </Block>
                            )}
                            <Text className='money'>{filters.moneyFilter(items.bargainPrice, true)}</Text>元
                          </View>
                        </View>
                      )
                    })}
                  </View>
                );
              })}
            </View>
          </View>
        )}

        {/* 砍价成功对话弹框 */}
        <AnimatDialog id='success-dialog' animClass='success-dialog' ref={this.successDialogRef}>
          {!!activity && (
            <View>
              <Success
                isSelf={activity ? activity.oneself : null}
                activity={activity}
                isSuccess={isSuccess}
                bargainPrice={bargainPrice}
                rate={rate}
                onCloseShare={this.onCloseShare}
                onCreatePoster={this.onCreatePoster}
              ></Success>
              <Image
                src={cdnResConfig.cutPrice.close}
                className='close-btn'
                onClick={this.handleCloseSuccessDialog}
              ></Image>
            </View>
          )}
        </AnimatDialog>
        {/* 分享对话弹框 */}
        {!!activity && (
          <ShareDialog
            posterHeaderType={cutPriceRule ? cutPriceRule.posterType : 1}
            posterTips={cutPriceRule ? cutPriceRule.posterContent : '物美价廉的好货，赶紧来砍价吧！'}
            posterLogo={cutPriceRule ? cutPriceRule.posterLogo : null}
            posterSalePriceLabel='最低价'
            posterLabelPriceLabel='活动结束价'
            posterName={activity.activityName}
            posterImage={activity.thumbnail}
            salePrice={filters.moneyFilter(activity.lowestPrice, true)}
            labelPrice={filters.moneyFilter(activity.salePrice, true)}
            onSave={this.onSavePosterImage}
            uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
            childRef={this.shareDialogRef}
          />
        )}

        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='share-canvas' />
        <AuthPuop></AuthPuop>
      </View>
    );
  }
}

export default CutPriceJoin as ComponentClass<PageOwnProps, PageState>;
