import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import timer from '@/wxat-common/utils/timer.js';
import cutEnum from '@/wxat-common/constants/cutEnum.js';
import template from '@/wxat-common/utils/template.js';
import './index.scss';

const ONE_DAY = 24 * 3600;

interface ActiveityInfo {
  activity: object;
  showTime: boolean;
  selfstyle: string;
}

class ActiveityInfo extends React.Component {
  static defaultProps = {
    activity: {},
    showTime: true,
    selfstyle: '',
  };

  state = {
    _thredId: null,
    remainTime: 0,
    tmpStyle: {},
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  /**
   * 组件的方法列表
   */ UNSAFE_componentWillMount() {
    if (!this.props.showTime) {
      return;
    }
    this.setState({
      _thredId: timer.addQueue(() => {
        this.updateTime(this.props.groups);
      }),
    });

    this.getTemplateStyle();
  }
  componentWillUnmount() {
    if (this.state._thredId) {
      timer.deleteQueue(this.state._thredId);
      this.setState({
        _thredId: null,
      });
    }
  }
  // 计算剩余时间
  updateTime = () => {
    if (!this.props.activity) {
      return;
    }

    const activity = this.props.activity;

    if (!this.state.remainTime) {
      const now = new Date().getTime();
      //根据砍价的活动状态判断砍价
      if (activity.activityStatus === cutEnum.activityStatus.NO_START) {
        this.setState({
          remainTime: parseInt((this.props.activity.startTime - now) / 1000),
        });
      } else {
        this.setState({
          remainTime: parseInt((this.props.activity.endTime - now) / 1000),
        });
      }
      return;
    }

    if (this.state.remainTime <= 0 || activity.activityStatus === cutEnum.activityStatus.END) {
      this.setState({
        displayTime: '活动已结束',
      });

      return;
    }

    const day = parseInt(this.state.remainTime / ONE_DAY);
    let mod = this.state.remainTime % ONE_DAY;
    const hour = parseInt(mod / 3600);
    mod = mod % 3600;
    const minute = parseInt(mod / 60);
    const second = parseInt(mod % 60);
    let text = '结束';
    if (activity.activityStatus === cutEnum.activityStatus.NO_START) {
      text = '开始';
    }
    const displayTime = `距离活动${text}${day}天${hour}:${minute < 10 ? '0' : ''}${minute}:${
      second < 10 ? '0' : ''
    }${second}`;
    this.setState({
      displayTime: displayTime,
      remainTime: this.state.remainTime - 1,
    });
  };

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { tmpStyle, displayTime } = this.state;
    const { activity, showTime, selfstyle } = this.props;
    return (
      <View data-scoped='wk-pca-ActivityInfo' className='wk-pca-ActivityInfo'>
        {!!activity && (
          <View
            className='activity-info'
            style={_safe_style_('background:' + tmpStyle.bgGradualChange + ';' + selfstyle)}
          >
            <View>
              <View className='lowestPrice money'>
                {'最低价：￥' + filters.moneyFilter(activity.lowestPrice, true)}
              </View>
              <View className='joined'>{'已有' + (activity.bargainTimes + activity.virtualPeople) + '人参与'}</View>
            </View>
            <View className='salePrice money'>{'原价：￥' + filters.moneyFilter(activity.salePrice, true)}</View>
            {!!showTime && <View className='display-time'>{displayTime}</View>}
          </View>
        )}
      </View>
    );
  }
}

export default ActiveityInfo;
