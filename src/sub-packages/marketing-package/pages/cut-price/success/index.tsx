import React, {Component} from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import '@/wxat-common/utils/platform';
import {Button, Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';

import './index.scss';

import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import hoc from '@/hoc/index';

type PageOwnProps = {
  isSelf: boolean;
  isSuccess: boolean;
  name: string;
  activity: Record<string, any>;
  rate: number;
  bargainPrice: number;
  url: string;
  onCloseShare?: () => void;
  onCreatePoster?: () => void;
};

type PageState = {};

@hoc
class Index extends Component<PageOwnProps, PageState> {
  constructor(props) {
    super(props);
  }
  state = {};
  componentDidMount() {}

  onJoin = () => {
    const id = this.props.activity.id;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/cut-price/detail/index',
      data: {
        activityId: id,
      },
    });
  };

  onCloseShare = () => {
    if (this.props.onCloseShare) {
      this.props.onCloseShare();
    }
  };

  onCreatePoster = () => {
    if (this.props.onCreatePoster) {
      this.props.onCreatePoster();
    }
  };

  render() {
    const { activity, isSelf, bargainPrice, isSuccess } = this.props;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pcs-Success' className='wk-pcs-Success'>
        <View className='banner'>
          <Image className='banner__image' src={cdnResConfig.cutPrice.cutMoney}></Image>
        </View>
        {!!isSelf ? (
          <View className='chart'>
            <View className='current'>
              <Text className='success-text'>砍价成功</Text>
              <Text className='text'>
                {'您一出手就帮自己砍了 ' + filters.moneyFilter(activity.totalBargainPrice, true) + ' 元'}
              </Text>
              <Text className='text'>分享给好友，帮你一起砍</Text>
            </View>
            <View className='btn-box'>
              <ButtonWithOpenType className='share-btn default-btn' openType='share' onClick={this.onCloseShare}>
                分享微信好友
              </ButtonWithOpenType>
              {process.env.TARO_ENV === 'weapp' && (
                <Button className='poster-btn default-btn' onClick={this.onCreatePoster}>
                  生成活动海报
                </Button>
              )}
            </View>
          </View>
        ) : (
          <View className='chart'>
            {!isSuccess ? (
              <View className='current'>
                <Text className='success-text'>砍价成功</Text>
                <Text className='text'>{'谢谢你帮我砍掉 ' + filters.moneyFilter(bargainPrice, true) + ' 元'}</Text>
                <Text className='text'>当前商品正在参加砍价活动， 快去发起自己的砍价吧</Text>
              </View>
            ) : (
              <View className='current'>
                <Text className='text'>好友已砍价成功，快去发起自己的砍价吧</Text>
              </View>
            )}

            <View className='btn-box'>
              <Button className='poster-btn default-btn' onClick={this.onJoin}>
                立即参与
              </Button>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default Index;
