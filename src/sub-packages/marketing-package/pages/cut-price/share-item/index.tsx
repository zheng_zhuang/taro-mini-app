import React, {Component} from 'react';
import {Button, Image, Text, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import './index.scss';

import getStaticImgUrl from '../../../../../wxat-common/constants/frontEndImgUrl'

const ONE_DAY = 24 * 3600;

class ShareItem extends Component {
  static defaultProps = {
    isSelf: {
      type: Boolean,
      value: false,
    },

    name: {
      type: String,
      value: '',
    },

    price: {
      type: Number,
      value: 0,
    },

    url: {
      type: String,
      value: '',
    },
  };

  // 计算剩余时间
  show() {
    this.getDialog().show({ scale: 1 });
  }
  hide() {
    this.getDialog().hide();
  }
  getDialog() {
    if (!this._dialog) {
      this._dialog = this.$scope.selectComponent('#dialog');
    }
    console.log(this.$scope.selectComponent('#dialog'));
    return this._dialog;
  }
  onCloseShare() {
    this.$scope.triggerEvent('onCloseShare');
  }
  onCreatePoster() {
    this.$scope.triggerEvent('onCreatePoster');
  }

  render() {
    return (
      <View className='share-wrapper'>
        <View className='share-item'>
          <Button openType='share' className='share-btn' onClick={this.onCloseShare}>
            <View className='image-block'>
              <Image className='share-img' src={getStaticImgUrl.group.share_png}></Image>
            </View>
            <Text>分享给朋友</Text>
          </Button>
        </View>
        {process.env.TARO_ENV === 'weapp' && (
          <View className='share-item'>
            <Button className='share-btn' onClick={this.onCreatePoster}>
              <View className='image-block'>
                <Image className='share-img' src={getStaticImgUrl.group.poster_png}></Image>
              </View>
              <View>生成商品海报</View>
            </Button>
          </View>
        )}
      </View>
    );
  }
}

export default ShareItem;
