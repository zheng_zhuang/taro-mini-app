import {$getRouter} from 'wk-taro-platform';
import React, {Component} from 'react';
import '@/wxat-common/utils/platform';
import {View} from '@tarojs/components';
import Taro from '@tarojs/taro';

import report from '@/sdks/buried/report/index.js';
import shareUtil from '@/wxat-common/utils/share.js';
import AuthUser from '@/wxat-common/components/auth-user';
import CutPriceList from '@/wxat-common/components/tabbarLink/cut-price-list/index';
import './index.scss';
import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index';

class List extends Component {
  $router = $getRouter();
  refCutPriceListCMTP: Taro.RefObject<any> = React.createRef();

  get showType() {
    const { showType } = this.$router.params;
    return showType != null ? +showType : 0;
  }

  onPullDownRefresh() {
    if (this.refCutPriceListCMTP.current) {
      this.refCutPriceListCMTP.current.onPullDownRefresh();
    }
  }

  onReachBottom() {
    if (this.refCutPriceListCMTP.current) {
      this.refCutPriceListCMTP.current.onReachBottom();
    }
  }

  onRetryLoadMore() {
    if (this.refCutPriceListCMTP.current) {
      this.refCutPriceListCMTP.current.onRetryLoadMore();
    }
  }
  onShareAppMessage() {
    report.share(true);
    const url = '/sub-packages/marketing-package/pages/cut-price/list/index';
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: shareUtil.ShareBZs.CUT_PRICE_LIST,
      bzName: '砍价活动列表',
    });

    console.log('sharePath => ', path);
    return {
      title: '',
      path,
      imageUrl: '',
    };
  }

  render() {
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-pcl-List' className='wk-pcl-List'>
        <AuthUser>
          <CutPriceList showType={this.showType} ref={this.refCutPriceListCMTP} />
        </AuthUser>
        <HomeActivityDialog showPage="sub-packages/marketing-package/pages/cut-price/list/index"></HomeActivityDialog>
      </View>
    );
  }
}

export default List;
