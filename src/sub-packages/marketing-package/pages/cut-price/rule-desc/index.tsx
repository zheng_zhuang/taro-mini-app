import React, {Component} from 'react';
import '@/wxat-common/utils/platform';
import {ScrollView, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';

interface RuleDesc {
  props: { onRule: Function };
}

class RuleDesc extends Component {
  static defaultProps = {
    activityList: [],
  };

  state = {
    title: '砍价规则说明',
    setions: [
      '1、点击砍价，每人自己有一次砍价机会。',
      '2、转发给朋友帮忙砍价，每人一次机会。',
      '3、砍到合适的价格，可以随时购买。',
      '4、也可以砍到底价后再购买。',
      '5、在设定的时间内，必须支付购买，否则砍价失败。',
    ],
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    wxApi
      .request({
        url: api.activity.cutSetting,
      })
      .then((res) => {
        if (this.props.onRule) {
          this.props.onRule(res.data);
        }
        const content = res.data.content || '';
        const array = content.split('↵');
        let setions = [];
        array.forEach((item) => {
          if (item === '') {
            return;
          }
          setions = setions.concat(item.split('\n').filter((section) => section !== ''));
        });
        this.setState({
          title: res.data.title,
          setions: setions,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { title, setions } = this.state;
    return (
      <ScrollView data-scoped='wk-pcr-RuleDesc' className='wk-pcr-RuleDesc scroll scroll-view_H' scrollY>
        <View className='rule'>
          <View className='title'>{title}</View>
          {setions.map((item, index) => {
            return (
              <View className='block' key={index}>
                {item}
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

export default RuleDesc;
