import { $getRouter,_safe_style_ } from 'wk-taro-platform';
import {View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import constants from '@/wxat-common/constants/index.js';
import pay from '@/wxat-common/utils/pay.js';
import AddressList from '@/wxat-common/components/address/list/index';
import GoodsItem from "../components/goods-item";
import './index.scss';
import hoc from '@/hoc';
import { connect } from 'react-redux';
const PAY_CHANNEL = constants.order.payChannel;
interface IState{
  tmpStyle:any;
  orderDetail:any;
  goodsInfo:any;
  curAddressData:any;
  freight:number | string;
  isManualAddress:boolean;
  price:number;
  addressOptions:{
    value:string;
    values:string[]
  };
  giftId:any
}
interface IProps{
  userInfo:any;
}
const mapStateToProps = (state) => {
  return { userInfo: state.base.userInfo };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class Confirm extends React.Component<IProps,IState>{
  state={
    tmpStyle: {}, // 主题样式
    orderDetail: { // 订单详情
      sendUserVO: {}
    }, 
    goodsInfo: {}, // 商品信息
    curAddressData: null, // 当前地址数据
    freight: 0, // 邮费
    isManualAddress: false, // 是否手动地址
    price: 0, //
    addressOptions: {
      value: '',
      values: ['使用微信地址', '手动添加地址'],
    },
    giftId:null
  }

  $router = $getRouter()
  // computed: {
  //   userAddress() {
  //     return (
  //       this.data.curAddressData &&
  //       this.data.curAddressData.province +
  //         this.data.curAddressData.city +
  //         this.data.curAddressData.region +
  //         this.data.curAddressData.address +
  //         this.data.curAddressData.roomNumber
  //     );
  //   },
  // },

  componentDidMount() {
    this.setState({
      giftId: this.$router.params.giftId,
    },()=>{
      this.getReceiveGiftDetail();
    });
    this.getTemplateStyle();
  }

  // 设置新增地址
  addAddress=()=>{
    this.setState({
      isManualAddress: true,
    });
  }

  /**
   * 改变地址
   * @param {*} e
   */
  onAddressChange=(data)=>{
    this.setState({
      isManualAddress: false,
      curAddressData: data,
    },()=>{
      this.getFreight();
    });
  }

  // 选择地址
  selectAddress=()=>{
    this.setState({
      isManualAddress: true,
    });
  }

  // 使用微信地址
  getWxAddress=()=>{
    wxApi.chooseAddress({
      success: (res) => {
        this.setState({
          curAddressData: {
            consignee: res.userName,
            mobile: res.telNumber,
            province: res.provinceName,
            city: res.cityName,
            region: res.countyName,
            address: res.detailInfo,
            roomNumber: '',
          },
        },()=>{
          this.getFreight();
        });
      },
    });
  }

  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 确认收礼
  confirmReceiveGift=()=>{
    if (!this.state.curAddressData) return this.selectAddress();
    wxApi
      .request({
        url: api.giveGift.receiveUnifiedOrder,
        method: 'POST',
        loading: true,
        data: {
          awardUserId: this.state.giftId,
          userAddressId: this.state.curAddressData?.id,
        },
      })
      .then((order) => {
        if (this.state.freight == 0) {
          wxApi.$redirectTo({
            url: '/sub-packages/moveFile-package/pages/order-list/index',
          });

          return;
        }
      
        const payParams = {
          timestamp: order.data.timeStamp + '',
          nonceStr: order.data.nonceStr,
          package: order.data.package || 'prepay_id=' + order.data.prepayId,
          signType: 'RSA',
          paySign: order.data.paySign,
        }
        const payFn = process.env.WX_OA === 'true' ? wx.chooseWXPay : wxApi.requestPayment;
        payFn({
          ...payParams,
          success (e) {
            report.clickPay({
              result: true,
              fail_reason: '',
              amount: 0,
              pay_type: PAY_CHANNEL,
            });
            wxApi.showToast({
                    icon: 'success',
                    title: '支付成功',
                    mask: true,
                    complete() {
                      wxApi.$redirectTo({
                        url: '/sub-packages/moveFile-package/pages/order-list/index',
                      });
                    },
                  });
            // if (order.data && order.data.wxPaySuccess) {
            //   if (order.data.extConfig && order.data.extConfig.immediateCallback) {
            //     order.data.wxPaySuccess(e, {
            //       orderNo,
            //     });
            //   } else {
            //     // 微信支付订单状态有延迟，延迟1秒加载
            //     setTimeout(() => {
            //       order.data.wxPaySuccess(e, {
            //         orderNo,
            //       });
            //     }, 1000);
            //   }
            // }
          },
          fail (e) {
            report.clickPay({
              result: false,
              fail_reason: e.errMsg,
              amount: 0,
              pay_type: PAY_CHANNEL,
            });
            wxApi.showToast({
                    icon: 'none',
                    title: '支付已取消',
                    mask: true,
                    complete: () => {
                      wxApi.$redirectTo({
                        url: '/sub-packages/marketing-package/pages/give-gift/detail/index',
                        data: {
                          giftId: this.state.giftId,
                          detailType: 2,
                        },
                      });
                    
            // if (order.data && order.data.wxPayFail) {
            //   order.data.wxPayFail(e);
            // }
          },
        })
      }
        // wxApi.requestPayment({
        //   timeStamp: order.data.timeStamp + '',
        //   nonceStr: order.data.nonceStr,
        //   package: 'prepay_id=' + order.data.prepayId,
        //   signType: order.data.signType,
        //   paySign: order.data.paySign,
        //   success: () => {
        //     wxApi.showToast({
        //       icon: 'success',
        //       title: '支付成功',
        //       mask: true,
        //       complete() {
        //         wxApi.$redirectTo({
        //           url: '/sub-packages/moveFile-package/pages/order-list/index',
        //         });
        //       },
        //     });
        //   },
        //   fail: () => {
        //     wxApi.showToast({
        //       icon: 'none',
        //       title: '支付已取消',
        //       mask: true,
        //       complete: () => {
        //         wxApi.$redirectTo({
        //           url: '/sub-packages/marketing-package/pages/give-gift/detail/index',
        //           data: {
        //             giftId: this.state.giftId,
        //             detailType: 2,
        //           },
        //         });
        //       },
        //     });
        //   },
        // });
      });
    });
  }

  // 获取运费
  getFreight=()=>{
    wxApi
      .request({
        url: api.giveGift.freight,
        method: 'POST',
        loading: true,
        data: {
          awardUserId: this.state.giftId,
          userAddressId: this.state.curAddressData?.id,
        },
      })
      .then((res) => {
        this.setState({
          freight: res.data ? (res.data / 100).toFixed(2) : 0,
        });
      });
  }

  // 获取收礼详情
  getReceiveGiftDetail=()=>{
    wxApi
      .request({
        url: api.giveGift.receiveDetail,
        loading: true,
        data: {
          awardUserId: this.state.giftId,
        },
      })
      .then((res) => {
        const goodsInfo = {
          name: res.data.itemName,
          pic: res.data.thumbnail,
          skuTreeNames: res.data.itemAttribute,
          itemCount: res.data.itemCount,
          salePrice: res.data.salePrice,
        };

        this.setState({
          orderDetail: res.data,
          goodsInfo,
        });
      });
  }

  render() {
    const {
      orderDetail,
      goodsInfo,
      addressOptions,
      curAddressData,
      freight,
      tmpStyle,
      isManualAddress,
    } = this.state;
    const  userAddress= curAddressData && curAddressData?.province + curAddressData?.city + curAddressData?.region + curAddressData?.address + curAddressData?.roomNumber
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpgc-Confirm" className="wk-mpgc-Confirm">
        {!isManualAddress ? (
          <View className="give-gift-confirm">
            <View className="container">
              <View className="gift-source">
                <View className="avatar">
                  <Image src={orderDetail.sendUserVO.userAvatar}></Image>
                </View>
                <View className="detail">
                  <View className="name">{orderDetail.sendUserVO.nickName}</View>
                  <View className="blessing">{orderDetail.greetingWord || ''}</View>
                </View>
              </View>
              {/*  商品项  */}
              <GoodsItem goodsInfo={goodsInfo} onHandleQuantity={this.onGoodsQuantity}></GoodsItem>
              {/*  选择地址  */}
              {!curAddressData ? (
                <View className="address-select" onClick={this.selectAddress}>
                  <View className="title">选择地址</View>
                  <View className="text-box">
                    {addressOptions.value || '请选择'}
                    <Image
                      className="arrow-icon"
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                    ></Image>
                  </View>
                </View>
              ) : (
                <View className="address-item" onClick={this.selectAddress}>
                  <Image
                    className="image"
                    src="https://img02.mockplus.cn/idoc/sketch/2020-11-19/39ed13a5-44c9-42c1-8072-52a65e6f57a8.86FBC2BF-8DC8-4B89-BEA9-EE822D5AE6CF.png"
                  ></Image>
                  <View className="info">
                    <View className="user-info">
                      <Text className="name">{curAddressData.consignee}</Text>
                      <Text className="phone">{curAddressData.mobile}</Text>
                    </View>
                    <View className="user-address">{userAddress}</View>
                  </View>
                </View>
              )}

              {!!freight && <View className="warning">配送到您所在的地区，还需支付运费</View>}

              {!curAddressData && <View className="hint">每人最多领取一份礼物</View>}
            </View>
            <View className="btn-box">
              <View className="cost">
                <View className="text">运费：</View>
                <View className="price">{'￥' + (freight || '0.00')}</View>
              </View>
              <View
                className="btn"
                onClick={this.confirmReceiveGift}
                style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              >
                确认收礼
              </View>
            </View>
          </View>
        ) : (
          <AddressList onChange={this.onAddressChange}></AddressList>
        )}

        {/*  地址选择列表  */}
      </View>
    );
  }
}


export default Confirm;
