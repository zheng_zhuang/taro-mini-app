import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import {  View, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import utilDate from '@/wxat-common/utils/date.js';

import './index.scss';

interface IState{
  tab:string | number;
  tmpStyle:any;
  giveList:any[];
  receiveList:any[];
  giveParams:{
    pageNo:number;
    pageSize:number;
    totalCount:number;
  };
  receiveParams:{
    pageNo:number;
    pageSize:number;
    totalCount:number;
  }
}
class MyGift extends React.Component<{},IState>{
  state={
    tab: '0',
    tmpStyle: {},
    giveList: [],
    receiveList: [],
    giveParams: {
      pageNo: 1,
      pageSize: 10,
      totalCount: 0,
    },

    receiveParams: {
      pageNo: 1,
      pageSize: 10,
      totalCount: 0,
    },
  }

  componentDidShow () {
    this.getGiveList();
    this.getReceiveList();
  }

  componentDidMount() {
    this.getTemplateStyle();
  }

  // 页面上拉触底
  onReachBottom=()=>{
    if (this.state.tab === '0' && this.state.giveParams.pageSize < this.state.giveParams.totalCount) {
      const giveParams = this.state.giveParams
      giveParams.pageSize = this.state.giveParams.pageSize + 10
      this.setState({
        giveParams
      },()=>{
        this.getGiveList();
      });

    } else if (this.state.tab === '1' && this.state.receiveParams.pageSize < this.state.receiveParams.totalCount) {
      const receiveParams = this.state.receiveParams
      receiveParams.pageSize = this.state.receiveParams.pageSize + 10
      this.setState({
        receiveParams
      },()=>{
        this.getReceiveList();
      });
    }
  }

  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 切换tab
  toggleTab=({
    currentTarget: {
      dataset: { tab },
    },
  })=>{
    this.setState({
      tab,
    });
  }

  // 跳转到详情
  toDetail=(data, e)=>{
    const { 
      detailType, 
      giftId, 
      orderNo
    } = data;

    // console.log(data, detailType, giftId, orderNo)

    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/give-gift/detail/index',
      data: {
        detailType: detailType,
        giftId: giftId,
        orderNo: orderNo,
      },
    });
  }

  // 获取送礼列表
  getGiveList=()=>{
    wxApi
      .request({
        url: api.giveGift.sendList,
        loading: true,
        data: {
          pageNo: this.state.giveParams.pageNo,
          pageSize: this.state.giveParams.pageSize,
        },
      })
      .then((res) => {
        const listData = res.data || [];

        listData.forEach((item) => {
          item.timeStr = utilDate.format(new Date(item.expireTime), 'yyyy-MM-dd hh:mm:ss');

          item.salePrice = (item.salePrice / 100).toFixed(2);
          item.payFee = (item.payFee / 100).toFixed(2);
          item.isEnd = new Date(item.endTime).getTime() <= new Date().getTime();
        });
        const giveParams = this.state.giveParams
        giveParams.totalCount = res.totalCount
        this.setState({
          giveList: listData,
          giveParams
        });
      });
  }

  // 获取收礼列表
  getReceiveList=()=>{
    wxApi
      .request({
        url: api.giveGift.receiveList,
        loading: true,
        data: {
          pageNo: this.state.receiveParams.pageNo,
          pageSize: this.state.receiveParams.pageSize,
        },
      })
      .then((res) => {
        const listData = res.data || [];

        listData.forEach((item) => {
          item.salePrice = (item.salePrice / 100).toFixed(2);
          item.payFee = (item.payFee / 100).toFixed(2);
        });
        const receiveParams=this.state.receiveParams
        receiveParams.totalCount= res.totalCount
        this.setState({
          receiveList: listData,
          receiveParams
        });
      });
  }
  
  render() {
    const { tab, giveList, tmpStyle, receiveList } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-mpgm-MyGift" className="wk-mpgm-MyGift">
        <View className="tabs">
          <View
            onClick={_fixme_with_dataset_(this.toggleTab, { tab: '0' })}
            className={'tab ' + (tab === '0' ? 'active' : '')}
          >
            送出
          </View>
          <View
            onClick={_fixme_with_dataset_(this.toggleTab, { tab: '1' })}
            className={'tab ' + (tab === '1' ? 'active' : '')}
          >
            收到
          </View>
        </View>
        {/*  送礼列表  */}
        {!!(tab === '0') && (
          <View>
            {giveList?.map((item, index) => {
                return (
                  <View className="order-item" key={item.id}>
                    <View className="header">{item.awardNumber ? '已领取' : '未领取'}</View>
                    <View
                      className="main"
                      onClick={this.toDetail.bind(this, {
                        detailType: item.playRuler === 1 || item.receiveStatus !== 1 ? 1 : 3,
                        giftId: item.id,
                        orderNo: item.orderNo,
                      })}
                    >
                      <View
                        className="image"
                        style={_safe_style_("background-image: url('" + item.thumbnail + "');")}
                      ></View>
                      <View className="text">
                        <View className="title">{item.itemName}</View>
                        <View className="sku">
                          <View>{item.itemAttribute || ''}</View>
                        </View>
                        <View className="quantity">{'x' + item.itemCount}</View>
                      </View>
                      <View className="amount">
                        <View className="price">{'￥' + item.payFee}</View>
                        <View className="total">{'￥' + item.salePrice}</View>
                      </View>
                    </View>
                    <View className="footer">
                      <View className="time">{item.receiveStatus !== -1 && <Text>{item.timeStr + ' 过期'}</Text>}</View>
                      {item.playRuler !== 1 && item.isEnd ? (
                        <View className="action" style={_safe_style_('background: #D6D6D6;')}>
                          已开奖
                        </View>
                      ) : item.receiveStatus === 1 ? (
                        <View
                          className="action"
                          onClick={this.toDetail.bind(this, {
                            detailType: item.playRuler === 1 ? 1 : 3,
                            giftId: item.id,
                            orderNo: item.orderNo,
                          })}
                          style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                        >
                          {item.receiveStatusDesc}
                        </View>
                      ) : (
                        <View className="action" style={_safe_style_('background: #D6D6D6;')}>
                          {item.receiveStatusDesc}
                        </View>
                      )}
                    </View>
                  </View>
                );
              })}
          </View>
        )}

        {/*  收礼列表  */}
        {!!(tab === '1') && (
          <View>
            {receiveList?.map((item, index) => {
                return (
                  <View className="order-item" key={item.id}>
                    <View className="header">
                      <View
                        className="avatar"
                        style={_safe_style_('background-image: url(' + item.sendUserVO.userAvatar + ')')}
                      ></View>
                      <View className="name">{item.sendUserVO.nickName}</View>
                    </View>
                    <View
                      className="main"
                      onClick={this.toDetail.bind(this, {
                        detailType: item.receiveStatus === 1 || item.receiveStatus === 2 ? 3 : 2,
                        giftId: item.receiveStatus === 1 || item.receiveStatus === 2 ? item.awardId : item.id,
                        orderNo: item.orderNo,
                      })}
                    >
                      <View
                        className="image"
                        style={_safe_style_("background-image: url('" + item.thumbnail + "');")}
                      ></View>
                      <View className="text">
                        <View className="title">{item.itemName}</View>
                        <View className="sku">
                          <View>{item.itemAttribute || ''}</View>
                        </View>
                        {/*  <View class="quantity">x{{item.itemCount}}</View>  */}
                      </View>
                      {/*  <View class="amount">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <View class="price">￥{{item.payFee}}</View>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <View class="total">￥{{item.salePrice}}</View>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </View>  */}
                    </View>
                    <View className="footer">
                      <View className="time"></View>
                      {item.receiveStatus === 1 && (
                        <View
                          className="action"
                          onClick={this.toDetail.bind(this, {
                            detailType: '3',
                            giftId: item.awardId,
                            orderNo: item.orderNo,
                          })}
                          style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                        >
                          等待开奖
                        </View>
                      )}

                      {item.receiveStatus === 6 && (
                        <View className="action" style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}>
                          待付款
                        </View>
                      )}

                      {(item.receiveStatus === 3 || item.receiveStatus === 4) && (
                        <View
                          className="action"
                          onClick={this.toDetail.bind(this, {
                            detailType: '2',
                            giftId: item.id,
                            orderNo: item.orderNo,
                          })}
                          style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                        >
                          待填写地址
                        </View>
                      )}

                      {/*  <View class="action" wx:if="{{item.receiveStatus === 6}}" data-detailType="2" data-giftId="{{item.id}}" data-orderNo="{{item.orderNo}}" data-giftId="{{item.id}}" data-orderNo="{{item.orderNo}}" bindtap="toDetail" style="background: {{tmpStyle.btnColor}};">待付款</View>  */}
                      {item.receiveStatus === 5 && (
                        <View className="action" style={_safe_style_('background: #D6D6D6;')}>
                          已转赠
                        </View>
                      )}

                      {item.receiveStatus === 7 && (
                        <View className="action" style={_safe_style_('background: #D6D6D6;')}>
                          已收礼
                        </View>
                      )}

                      {item.receiveStatus === 8 && (
                        <View className="action" style={_safe_style_('background: #D6D6D6;')}>
                          已失效
                        </View>
                      )}
                    </View>
                  </View>
                );
              })}
          </View>
        )}
      </View>
    );
  }
}


export default MyGift;
