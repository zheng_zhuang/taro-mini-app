import {View, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';

import './index.scss';

interface IProps{
  url:string;
  onHandleUpdateCover:Function
}
class CoverUpload extends React.Component<IProps>{
  static defaultProps ={
    url:'',
    onHandleUpdateCover:()=>{
      return null;
    }
  }
  // 更新模板封面
  updateCover=()=>{
    this.props.onHandleUpdateCover();
  }
  render() {
    const { url } = this.props;
    return (
      <View data-scoped="wk-pgcc-CoverUpload" className="wk-pgcc-CoverUpload cover-img">
        <View className="img" onClick={this.updateCover}>
          <Image src={url}></Image>
          <View className="hint">更换封面</View>
        </View>
      </View>
    );
  }
}

export default CoverUpload;
