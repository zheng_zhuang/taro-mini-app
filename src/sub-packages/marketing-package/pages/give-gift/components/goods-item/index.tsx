import { View, Image, Input } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';

interface IProps{
  goodsInfo:any;// 商品信息
  isEdit:boolean; // 是否编辑模式
  onHandleQuantity:(value:number) => void
}
interface IState{
  quantity:number;
  salePrice:number
}
class GoodsItem extends React.Component<IProps,IState>{
  static defaultProps={
    goodsInfo:{},
    isEdit:false,
    onHandleQuantity:()=>{
      return null
    }
  }

  state={
    quantity: 1,
    salePrice: 0,
  }

  componentDidUpdate(preProps) {
    if(preProps.goodsInfo !== this.props.goodsInfo){
      this.setState({
        quantity: this.props.goodsInfo?.itemCount,
        salePrice: this.props.goodsInfo?.salePrice,
      });
    }
  }

  subtractQuantity=()=>{
    if (this.state.quantity === 1) {
      return wxApi.showToast({
        icon: 'none',
        title: '商品数量不能小于1',
      });
    }
    this.setState({
      quantity: this.state.quantity - 1,
    },()=>{
      this.props.onHandleQuantity(this.state.quantity)
    });

  }

  addQuantity=()=>{
    this.setState({
      quantity: this.state.quantity + 1,
    },()=>{
      this.props.onHandleQuantity(this.state.quantity)
    });
  }

  onInputQuantity=(e)=>{
    const value = parseInt(e.detail.value);
    if (!value || value <= 0) {
      this.setState({
        quantity: this.state.quantity,
      });

      wxApi.showToast({
        icon: 'none',
        title: '请输入正确的数量',
      });
    } else {
      this.setState({
        quantity: value,
      },()=>{
        this.props.onHandleQuantity(this.state.quantity)
      });
    }
  }

  render() {
    const { salePrice, quantity } = this.state;
    const {isEdit,goodsInfo} = this.props
    const price = ((quantity * salePrice) / 100).toFixed(2)
    return (
      <View data-scoped="wk-pgcg-GoodsItem" className="wk-pgcg-GoodsItem goods-item">
        <View className="image">
          <Image src={goodsInfo?.pic} ></Image>
        </View>
        <View className="detail">
          <View className="detail-box">
            <View className="title">{goodsInfo?.name}</View>
            <View className="sku">{goodsInfo?.skuTreeNames || ''}</View>
            <View className="num">{'x' + quantity}</View>
          </View>
          {isEdit && <View className="price">{'￥' + price}</View>}
          {isEdit && (
            <View className="set-quantity">
              <View className="subtract btn" onClick={this.subtractQuantity}>
                -
              </View>
              <Input value={quantity} className="input" type="num" onBlur={this.onInputQuantity}></Input>
              <View className="add btn" onClick={this.addQuantity}>
                +
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}


export default GoodsItem;
