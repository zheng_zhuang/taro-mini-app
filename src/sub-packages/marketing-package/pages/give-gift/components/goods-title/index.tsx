import { _safe_style_ } from 'wk-taro-platform';
import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxClass from '@/wxat-common/utils/wxClass.js';
import template from '@/wxat-common/utils/template.js';
import wxApi from '@/wxat-common/utils/wxApi.js';

import './index.scss';
interface IProps{
  isWeight:boolean;
  title:string;
  text:string
}
class GoodsTitle extends React.Component<IProps>{
  static defaultProps={
    isWeight:false,
    title:'',
    text:''
  }
  render() {
    const { isWeight, title, text } = this.props;
    return (
      <View data-scoped="wk-pgcg-GoodsTitle" className="wk-pgcg-GoodsTitle goods-title">
        <View className="title" style={_safe_style_('font-weight: ' + (isWeight ? 'bold' : 400) + ' ;')}>
          {title}
        </View>
        <View className="text">{text}</View>
      </View>
    );
  }
}


export default GoodsTitle;
