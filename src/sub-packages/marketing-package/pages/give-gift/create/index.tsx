import { $getRouter,_safe_style_ } from 'wk-taro-platform';
import { View, Picker, Image, Text, Input, Textarea } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import utilDate from '@/wxat-common/utils/date.js';
import CoverUpload from '../components/cover-upload/index';
import GoodsItem from '../components/goods-item/index';
import GoodsTitle from '../components/goods-title/index';
import pay from '@/wxat-common/utils/pay.js';
import constants from '@/wxat-common/constants';
import './index.scss';
const PAY_CHANNEL = constants.order.payChannel;

interface IState{
  tmpStyle:any;
  playOptions:{
    index:number;
    textArr:string[];
    valueArr:number[];
    date:Date | string;
    time:null | string | Date;
    openTime:Date | string;
  };
  peopleOptions:{
    index:number;
    textArr:string[];
    valueArr:number[]
  };
  goodsInfo:any;
  submitParams: {
    itemCount:number;
    itemNo: string | null;
    skuId: string | null;
    playRuler: number | string | null;
    openTime: string;
    minPlayer: number;
    maxPlayer: number;
    greetingWords: string;
    greetingImgUrl: string;
  };
  curDate:string;
  defaultCoverUrl:string
}

class Create extends React.Component<{},IState>{
  state = {
    tmpStyle: {},
    playOptions: {
      index: 0,
      textArr: ['直接送礼','定时开奖'],
      valueArr: [1, 2],
      date: utilDate.format(new Date(), 'yyyy-MM-dd'), // 结束日期
      time: null, // 结束时间
      openTime: '',
    },
    peopleOptions: {
      index: 0,
      textArr: ['单人', '多人'],
      valueArr: [1, 2],
    },
    goodsInfo: null,
    submitParams: {
      itemCount: 0,
      itemNo: null,
      skuId: null,
      playRuler: 1,
      openTime: '',
      minPlayer: 0,
      maxPlayer: 0,
      greetingWords: '一点心意，希望你喜欢~',
      greetingImgUrl: '',
    },
    curDate: utilDate.format(new Date(), 'yyyy-MM-dd'),
    defaultCoverUrl: 'https://front-end-1302979015.file.myqcloud.com/images/gift-share.png',
  }
  $router = $getRouter()
  componentDidMount() {
    this.getTemplateStyle();
    const params = JSON.parse(this.$router.params.params as string);
    const submitParams = this.state.submitParams
    submitParams.itemNo = params.itemNo
    submitParams.skuId = params.skuId
    submitParams.itemCount = params.itemCount
    submitParams.playRuler = this.state.playOptions.valueArr[this.state.playOptions.index]
    this.setState({
      goodsInfo: params,
      submitParams:submitParams
    });
  }
  // 单人 / 多人
  onPeoplePickerChange=(e)=>{
    const index = e.detail.value;
    const peopleOptions = this.state.peopleOptions
    peopleOptions.index= index
    this.setState({
      peopleOptions
    });
  }
  // 监听玩法设置
  onPlayPickerChange=(e)=>{
    const index = e.detail.value;
    const {playOptions,submitParams} = this.state
    playOptions.index= index
    submitParams.playRuler =playOptions.valueArr[index]
    submitParams.maxPlayer = 0
    this.setState({
      playOptions,
      submitParams
    });
  }
  // 监听日期设置
  onDatePickerChange=(e)=>{
    const {playOptions,submitParams} = this.state
    playOptions.date = e.detail.value
    playOptions.openTime = new Date(`${e.detail.value} ${playOptions.time}`).toString()
    submitParams.openTime = `${e.detail.value} ${this.state.playOptions.time}:00`
    this.setState({
      playOptions,
      submitParams
    });
  }
  // 监听时间设置
  onTimePickerChange=(e)=>{
    const {playOptions,submitParams} = this.state
    playOptions.time = e.detail.value
    playOptions.openTime = new Date(`${this.state.playOptions.date} ${e.detail.value}`).toString()
    submitParams.openTime = `${this.state.playOptions.date} ${e.detail.value}:00`
    this.setState({
      playOptions,
      submitParams
    });
  }
  // 监听参与人数设置
  onBlurMaxPlayer=(e)=>{
    const value = parseInt(e.detail.value).toFixed(0);
    if (!value) {
      return wxApi.showToast({
        icon: 'none',
        title: '请输入参与人数',
      });
    }
    const submitParams = this.state.submitParams
    submitParams.maxPlayer = Number(value)
    this.setState({
      submitParams
    });
  }
  // 监听祝福语输入
  onInputBlessing=(e)=>{
    const submitParams = this.state.submitParams
    submitParams.greetingWords = e.detail.value
    this.setState({
      submitParams
    });
  }
  // 监听商品数量设置
  onGoodsQuantity=(value)=>{
    const submitParams = this.state.submitParams
    submitParams.itemCount = value
    this.setState({
      submitParams
    });
  }
  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }
  //小程序不支持一次上传多张图片，改为多次上传一张图片
  uploadImage=()=>{
    wxApi.chooseImage({
      success: (res) => {
        wxApi
          .$uploadFile({
            loading: true,
            url: api.upload,
            filePath: res.tempFilePaths[0],
            name: 'files',
            formData: {}, //额外参数
          })
          .then((res) => {
            const submitParams = this.state.submitParams
            submitParams.greetingImgUrl = res.data[0]
            this.setState({
              submitParams
            });
          })
          .finally((error) => {
          });
      },
    });
  }

  // 生成礼包
  createGift=()=>{
    // 判断是否填写开奖时间
    if (this.state.submitParams.playRuler !== 1 && (!this.state.playOptions.date || !this.state.playOptions.time)) {
      return wxApi.showToast({
        icon: 'none',
        title: '请设置开奖时间',
      });
    }
    // 判断是否填写开奖时间
    console.log(new Date(this.state.submitParams.openTime).getTime(), new Date().getTime() + 17280000);

    if (new Date(this.state.submitParams.openTime).getTime() > new Date().getTime() + 172800000) {
      return wxApi.showToast({
        icon: 'none',
        title: '开奖时间需在48小时内',
      });
    }
    // 判断是否设置参与人数
    if (
      this.state.submitParams.playRuler !== 1 &&
      (!this.state.submitParams.maxPlayer || this.state.submitParams.maxPlayer <= 0)
    ) {
      return wxApi.showToast({
        icon: 'none',
        title: '请设置参与人数',
      });
    }
    // 判断开奖时间是否大于当前时间
    if (this.state.submitParams.playRuler !== 1 && new Date(this.state.playOptions.openTime).getTime() < new Date().getTime()) {
      return wxApi.showToast({
        icon: 'none',
        title: '开奖时间不能小于当前时间',
      });
    }

    // 如果是单人则为1，多人则为商品数量
    if (this.state.submitParams.playRuler === 1) {
      const submitParams = this.state.submitParams
      submitParams.maxPlayer= this.state.peopleOptions.index == 1 ? this.state.submitParams.itemCount : 1,
      this.setState({
        submitParams
      },()=>{
        // 调用下单接口
        this.sendUnifiedOrder()
      })
    }else{
      this.sendUnifiedOrder()
    }
  }

  sendUnifiedOrder=()=>{
    const orderParams = Object.assign({}, this.state.submitParams, {
      payTradeType: pay.returnPayTradeType()
    })

    wxApi
    .request({
      url: api.giveGift.sendUnifiedOrder,
      method: 'POST',
      loading: true,
      data: orderParams,
    })
    .then((order = {}) => {
      // // 订阅消息模板,显示支付成功和快递发货通知
      // let Ids = [subscribeEnum.PAY_SUCCESS.value, subscribeEnum.DELIVERY.value];

      // //先授权订阅消息
      // subscribeMsg.sendMessage(Ids).then(() => {

      const {data = {}} = order
      const payParams = {
        timeStamp: data.timeStamp + '',
        nonceStr: data.nonceStr,
        package: 'prepay_id=' + data.prepayId,
        signType: data.signType,
        paySign: data.paySign,
      }

      pay.payByOrder(data.orderNo, PAY_CHANNEL.WX_PAY, {
        payInfo: {
          data: payParams
        },
        success: (res) => {
          wxApi.showToast({
            icon: 'success',
            title: '支付成功',
            mask: true,
            complete: () => {
              // 跳转礼物详情页面
              wxApi.$redirectTo({
                url: '/sub-packages/marketing-package/pages/give-gift/detail/index',
                data: {
                  giftId: data.awardId,
                  detailType: orderParams.playRuler === 1 ? 1 : 3,
                },
              });
            },
          });
        },
        fail: (err) => {
          wxApi.showToast({
            icon: 'none',
            title: '支付已取消',
            mask: true,
          });
        },
      })
    })
    .catch((error) => {
      console.error(error)
    })
    .finally(() => {});
  }


  render() {
    const { submitParams, defaultCoverUrl, goodsInfo, playOptions, peopleOptions, curDate, tmpStyle } = this.state;
    return (
      <View data-scoped="wk-mpgc-Create" className="wk-mpgc-Create give-gift-create">
        <View className="container">
          <CoverUpload
            url={submitParams.greetingImgUrl || defaultCoverUrl}
            onHandleUpdateCover={this.uploadImage}
          ></CoverUpload>
          {/*  商品标题  */}
          <GoodsTitle isWeight={true} title="伴手礼"></GoodsTitle>
          {/*  商品项  */}
          <GoodsItem goodsInfo={goodsInfo} isEdit={true} onHandleQuantity={this.onGoodsQuantity}></GoodsItem>
          {/*  玩法  */}
          <Picker range={playOptions.textArr} value={playOptions.index} onChange={this.onPlayPickerChange}>
            <View className="picker-item">
              <View className="title">玩法</View>
              <View className="text-box">
                {playOptions.textArr[playOptions.index]}
                <Image
                  className="arrow-icon"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                ></Image>
              </View>
            </View>
          </Picker>
          {/*  单人 / 多人  */}
          {submitParams.playRuler === 1 && (
            <Picker range={peopleOptions.textArr} value={peopleOptions.index} onChange={this.onPeoplePickerChange}>
              <View className="picker-item">
                <View className="title">赠送</View>
                <View className="text-box">
                  {peopleOptions.textArr[peopleOptions.index]}
                  <Image
                    className="arrow-icon"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                  ></Image>
                </View>
              </View>
            </Picker>
          )}

          {/*  开奖时间  */}
          {submitParams.playRuler !== 1 && (
            <View className="picker-item">
              <View className="title">开奖时间</View>
              <View className="text-box">
                <Picker mode="date" value={playOptions.date} start={curDate} onChange={this.onDatePickerChange}>
                  <Text>{playOptions.date || '选择日期'}</Text>
                  <Image
                    className="arrow-icon"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                  ></Image>
                </Picker>
                <Picker mode="time" value={playOptions.time} onChange={this.onTimePickerChange}>
                  <Text>{playOptions.time || '选择时间'}</Text>
                  <Image
                    className="arrow-icon"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                  ></Image>
                </Picker>
              </View>
            </View>
          )}

          {/*  参与人数  */}
          {submitParams.playRuler !== 1 && (
            <View className="input-item">
              <View className="title">参与人数</View>
              <View className="text-box">
                <Input
                  className="weui-input"
                  type="number"
                  value={submitParams.maxPlayer.toString()}
                  placeholder="请输入人数"
                  onBlur={this.onBlurMaxPlayer}
                ></Input>
                <Text>人</Text>
              </View>
            </View>
          )}

          {/*  商品标题  */}
          <GoodsTitle title="祝福语"></GoodsTitle>
          {/*  祝福语  */}
          <View className="blessing">
            <Textarea
              cols="30"
              rows="10"
              maxlength={11}
              value={submitParams.greetingWords}
              placeholder="请输入祝福语"
              placeholderClass="placeholder"
              onInput={this.onInputBlessing}
            ></Textarea>
            <View className="font-length">{submitParams.greetingWords.length + '/11'}</View>
          </View>
          <View className="hint_tips">
            {submitParams.playRuler === 1 ? '赠送单人时，所有礼品均送给一个人' : '每人最多领取一份礼物'}
            {!(submitParams.playRuler !== 1) && <View>赠送多人时，每人最多领取一份礼物</View>}
          </View>
        </View>
        <View className="btn-box">
          <View
            className="btn"
            style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
            onClick={this.createGift}
          >
            生成礼包
          </View>
        </View>
      </View>
    );
  }
}


export default Create;
