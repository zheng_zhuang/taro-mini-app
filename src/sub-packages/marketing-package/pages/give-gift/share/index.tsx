import { $getRouter} from 'wk-taro-platform'
import {  View, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import subscribeEnum from '@/wxat-common/constants/subscribeEnum.js';
import './index.scss';
import hoc from '@/hoc';
import { connect } from 'react-redux';


interface IState{
  tmpStyle:any;
  orderDetail:any;
  giftId:null |string | undefined;
  userIdGiven:null | string | undefined;
  isSelf:boolean;
  isJoin:boolean;
  url:string;
  detailType:null | number;
  userId:string | null
}
interface IProps{
  messageTemplateId:any
}
const mapStateToProps = (state) => {
  return {
    messageTemplateId: state.globalData.messageTemplateId,
  };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class Share extends React.Component<IProps,IState>{
  state={
    tmpStyle: {},
    orderDetail: {},
    giftId: null, // 礼物id
    userIdGiven: null, // 送礼人ID
    isSelf: false, // 是否自己送的礼物
    isJoin: false, // 是否已参加
    url: '', // 跳转页面url
    detailType: null, // 详情类型 1：送礼详情 2：收礼详情 3：拆礼详情
    userId: null,
  }

  $router = $getRouter()
  componentDidMount() {
    this.setState({
      giftId: this.$router.params.giftId,
      userIdGiven: this.$router.params?.userIdGiven,
      userId: wxApi.getStorageSync('userId'),
    },()=>{
      this.getOpenGiftDetail();
    });
  }

  componentDidShow() {
    this.setState({
      userId: wxApi.getStorageSync('userId'),
    },()=>{
      if(this.state.giftId){
        this.getOpenGiftDetail();
      }
    });

  }

  // 拆礼
  openGift=()=>{
    if(!authHelper.checkAuth()) return
    if (this.state.isSelf) {
      return wxApi.showToast({
        title: '送礼人不能拆礼',
        icon: 'none',
      });
    }
    if (this.state.isJoin) {
      return wxApi.showToast({
        title: '您已参与过',
        icon: 'none',
      });
    }
    if (
      (this.state.orderDetail.list.length === this.state.orderDetail.maxPlayer ||
        this.state.orderDetail.endTime <= new Date().getTime()) &&
      (!this.state.userIdGiven || this.state.userIdGiven === 'undefined')
    ) {
      return wxApi.showToast({
        title: '活动已结束',
        icon: 'none',
      });
    }

    if (this.state.orderDetail.playRuler === 2) {
      const subscribeList = this.props.messageTemplateId;
      // 小程序
      if(process.env.TARO_ENV === 'weapp'){
        wxApi.requestSubscribeMessage({
          // tmplIds: ['wQ5_QVx2pJQNXsbYpdau6GyPc2TmYEO8ImMmOCUKevs'],
          // tmplIds: ['Ku0KlypMgyM8k7w4muV06P43b1UnDmB0l9kMrN9m4po'],
          tmplIds: [subscribeList[subscribeEnum.LOTTERY.value] || 'Ku0KlypMgyM8k7w4muV06P43b1UnDmB0l9kMrN9m4po'],

          success: (res) => {
            console.log('已授权接收订阅消息', res);
          },
          fail: (res) => {
            console.log('错误订阅消息', res);
          },
          complete: () => {
            this.getOpenGiftDetail(() => {
              if (this.state.isSelf) {
                return wxApi.showToast({
                  title: '送礼人不能参与',
                  icon: 'none',
                });
              }
              if (this.state.isJoin) {
                return wxApi.showToast({
                  title: '您已参与过',
                  icon: 'none',
                });
              }
              if (
                (this.state.orderDetail.list.length === this.state.orderDetail.maxPlayer ||
                  this.state.orderDetail.endTime <= new Date().getTime()) &&
                (!this.state.userIdGiven || this.state.userIdGiven === 'undefined')
              ) {
                return wxApi.showToast({
                  title: '活动已结束',
                  icon: 'none',
                });
              }
              this.participateInLuckyDraw();
            });
          },
        });
      }else{
        this.getOpenGiftDetail(() => {
          if (this.state.isSelf) {
            return wxApi.showToast({
              title: '送礼人不能参与',
              icon: 'none',
            });
          }
          if (this.state.isJoin) {
            return wxApi.showToast({
              title: '您已参与过',
              icon: 'none',
            });
          }
          if (
            (this.state.orderDetail.list.length === this.state.orderDetail.maxPlayer ||
              this.state.orderDetail.endTime <= new Date().getTime()) &&
            (!this.state.userIdGiven || this.state.userIdGiven === 'undefined')
          ) {
            return wxApi.showToast({
              title: '活动已结束',
              icon: 'none',
            });
          }
          this.participateInLuckyDraw();
        });
      }

    } else {
      this.getOpenGiftDetail(() => {
        if (this.state.isSelf) {
          return wxApi.showToast({
            title: '送礼人不能参与',
            icon: 'none',
          });
        }
        if (this.state.isJoin) {
          return wxApi.showToast({
            title: '您已参与过',
            icon: 'none',
          });
        }
        if (
          (this.state.orderDetail.list.length === this.state.orderDetail.maxPlayer ||
            this.state.orderDetail.endTime <= new Date().getTime()) &&
          (!this.state.userIdGiven || this.state.userIdGiven === 'undefined')
        ) {
          return wxApi.showToast({
            title: '活动已结束',
            icon: 'none',
          });
        }
        this.participateInLuckyDraw();
      });
    }
  }

  // 参与抽奖
  participateInLuckyDraw=()=>{
    const params = {
      awardId: this.state.giftId,
      userId: this.state.userId,
    };

    if (this.state.userIdGiven && this.state.userIdGiven !== 'undefined') {
      params.userIdGiven = this.state.userIdGiven;
    }

    wxApi
      .request({
        url: api.giveGift.participateInLuckyDraw,
        method: 'POST',
        loading: true,
        data: params,
      })
      .then((order) => {
        wxApi.$redirectTo({
          url: this.state.url,
          data: {
            giftId:
              this.state.orderDetail.playRuler !== 1 && (!this.state.userIdGiven || this.state.userIdGiven === 'undefined')
                ? this.state.giftId
                : order.data.id,
            detailType: this.state.detailType,
          },
        });
      });
  }

  // 获取拆礼礼详情
  getOpenGiftDetail=(callback)=>{
    wxApi
      .request({
        url: api.giveGift.openGiftDetail,
        method: 'POST',
        loading: true,
        data: {
          awardId: this.state.giftId,
          userId: this.state.userId,
        },
      })
      .then((res) => {
        res.data.list = res.data.list || [];
        let isGetGift = false; // 是否收礼
        let receiveGiftId = ''; // 收礼id
        const isJoin = res.data.list.some((user) => {
          if (user.userId === this.state.userId) {
            isGetGift = user.awardStatus >= 3;
            receiveGiftId = user.id;
            return true;
          }
        });
        const isSelf = this.state.userId === res.data.userId;

        // 判断跳转确认收礼还是详情页面
        const url =
          res.data.playRuler !== 1 || isSelf || isJoin
            ? '/sub-packages/marketing-package/pages/give-gift/detail/index'
            : '/sub-packages/marketing-package/pages/give-gift/confirm/index';

        this.setState({
          url,
          isJoin,
          isSelf,
          orderDetail: res.data,
          detailType: isSelf && res.data.playRuler === 1 ? 1 : 3,
        },()=>{
          // 判断是否转赠
          this.setState({
            detailType:
              !isSelf && this.state.userIdGiven && this.state.userIdGiven !== 'undefined' ? 2 : this.state.detailType,
          },()=>{
            // 如果是自己送礼或者已经参与游戏的就跳转详情页
              if (isSelf || (isJoin && !isGetGift)) {
                wxApi.$redirectTo({
                  url: url,
                  data: {
                    giftId: this.state.giftId,
                    detailType: this.state.detailType,
                  },
                });
              } else if (isJoin && isGetGift) {
                wxApi.$redirectTo({
                  url: '/sub-packages/marketing-package/pages/give-gift/detail/index',
                  data: {
                    giftId: receiveGiftId,
                    detailType: 2,
                  },
                });
              }
              callback && callback();
          });
        });
      })
      .finally(() => {
       
      });
  }

  render() {
    const { orderDetail } = this.state;
    return (
      <View data-scoped="wk-mpgs-Share" className="wk-mpgs-Share give-gift-share">
        <View className="avatar">
          <Image src={orderDetail.avatarImgUrl} mode="widthFix"></Image>
        </View>
        <View className="btn" onClick={this.openGift}></View>
        <View className="name">{orderDetail.nickName}</View>
        <View className="blessing">{orderDetail.greetingWords}</View>
      </View>
    );
  }
}

export default Share;
