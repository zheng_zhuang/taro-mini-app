import { $getRouter,_safe_style_ } from 'wk-taro-platform';
import { View, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import utilDate from '@/wxat-common/utils/date.js';
import ShareDialog from '@/wxat-common/components/share-dialog/index';
import shareUtil from '@/wxat-common/utils/share.js';
import { connect } from 'react-redux';
import GoodsTitle from "../components/goods-title";
import './index.scss';

interface IState{
  giftId:string | undefined;
  detailType:any;
  tmpStyle:any;
  orderDetail:any;
  isFold:boolean;
  isJoin:boolean;
  isSelf:boolean;
  isPastDue:boolean;
  subhead:any;
  winUserGiftId:string;
  defaultBackgroundCoverUrl:string;
  backgroundImage:string;
  defaultCoverUrl:string
}
interface IProps{
  userInfo:any
}
const mapStateToProps = (state) => {
  return { userInfo: state.base.userInfo };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class Detail extends React.Component<IProps,IState>{
  state={
    giftId: '',
    detailType: null,
    tmpStyle: {},
    orderDetail: {
      awardReceiveUserVOList: [],
      sendUserVO: {},
    },
    isFold: false,
    isJoin: false, // 是否已参与
    isSelf: false, // 是否自己的送礼
    isPastDue: false, // 是否过期
    subhead: '',
    winUserGiftId: '', // 中奖用户的收礼id
    defaultBackgroundCoverUrl: 'https://front-end-1302979015.file.myqcloud.com/images/gift-default-cover.jpg',
    backgroundImage: 'https://front-end-1302979015.file.myqcloud.com/images/gift-cover.jpg',
    defaultCoverUrl: 'https://front-end-1302979015.file.myqcloud.com/images/gift-share.png',
  }

  private detailShareDialogRef = React.createRef<ShareDialog>();

  $router = $getRouter()
  componentDidMount() {
    this.getTemplateStyle();
    this.setState({
      giftId: this.$router.params.giftId,
      detailType: Number(this.$router.params.detailType)
    },()=>{
      this.init()
    });
  }

  init=()=>{
    if(this.state.giftId){
      switch (this.state.detailType) {
        // 送礼详情
        case 1:
          this.getGiveGiftDetail();
          break;
        // 收礼详情
        case 2:
          this.getReceiveGiftDetail();
          break;
        // 拆礼详情
        case 3:
          this.getOpenGiftDetail();
          break;
      }
    }
  }

  componentDidShow () {
    this.init()
  }

  // 分享图文设置
  onShareAppMessage=(res)=>{
    const {
      orderDetail,
      defaultCoverUrl,
      detailType,
      isSelf,
      isPastDue,
    } = this.state;
    const {userInfo} = this.props;
    let url;
    const {receiveStatus} = orderDetail

    if(detailType === 1 && receiveStatus === 1){
      //送礼
      url = `/sub-packages/marketing-package/pages/give-gift/share/index?giftId=${orderDetail.id}`;
    }else if(detailType === 2 && (receiveStatus === 3 || receiveStatus === 4)){
      //转赠
      url = `/sub-packages/marketing-package/pages/give-gift/share/index?giftId=${orderDetail.id}&userIdGiven=${userInfo.id}`;
    }else if(detailType === 3 && isSelf && !isPastDue){
      //拆礼
      url = `/sub-packages/marketing-package/pages/give-gift/share/index?giftId=${orderDetail.awardId}`
    }

    this.getShareDialog().hide();

    const path = shareUtil.buildShareUrlPublicArguments({
      url: url,
    });

    return {
      title: orderDetail.greetingWord,
      path,
      imageUrl: orderDetail.greetingImgUrl || defaultCoverUrl,
    };
    // if (res.from === 'button') {
    //   return {
    //     title: orderDetail.greetingWord,
    //     path: `/sub-packages/marketing-package/pages/give-gift/share/index?giftId=${giftId}&userIdGiven=${userInfo.id}`,
    //     imageUrl: orderDetail.greetingImgUrl || defaultCoverUrl,
    //   };
    // }
  }

  /**
   * 获取邀请好友对话弹框
   */
   getShareDialog = () => {
    if (this.detailShareDialogRef.current) {
      this._shareDialog = this.detailShareDialogRef.current;
    }
    return this._shareDialog;
  };

  /**
   * 打开分享弹框
   */
  handleSelectChanel = () => {
    this.getShareDialog().show();
  };

  // 切换订单折叠
  toggle = (e) => {
    e.stopPropagation();
    this.setState({
      isFold: !this.state.isFold,
    });
  }

  // 获取模板配置
  getTemplateStyle=()=>{
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 设置地址
  setAddress=()=>{
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/give-gift/confirm/index',
      data: {
        giftId: this.state.detailType === 3 ? this.state.winUserGiftId : this.state.giftId,
      },
    });
  }

  // 跳转商品页面
  toGoodsPage=()=>{
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: this.state.orderDetail.itemNo,
      },
    });
  }

  // 跳转订单页面
  toOrderPage=()=>{
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/order-list/index',
    });
  }

  // 获取送礼详情
  getGiveGiftDetail=()=>{
    wxApi
      .request({
        url: api.giveGift.sendDetail,
        loading: true,
        data: {
          awardId: this.state.giftId,
        },
      })
      .then((res) => {
        res.data.orderTimeText = utilDate.format(new Date(res.data.orderTime), 'yyyy-MM-dd hh:mm:ss');

        res.data.salePrice = (res.data.salePrice / 100).toFixed(2);
        res.data.totalPrice = (res.data.salePrice * res.data.itemCount).toFixed(2);

        res.data.awardReceiveUserVOList = res.data.awardReceiveUserVOList || [];
        res.data.refundFee = res.data.refundFee ? (res.data.refundFee / 100).toFixed(2) : null;
        res.data.awardReceiveUserVOList.forEach((item) => {
          item.createTimeText = utilDate.format(new Date(item.createTime), 'yyyy-MM-dd hh:mm:ss');
        });
        this.setState({
          orderDetail: res.data,
          isSelf: true,
        },()=>{
          this.setSubhead();
        });

      });
  }

  // 获取收礼详情
  getReceiveGiftDetail=()=>{
    wxApi
      .request({
        url: api.giveGift.receiveDetail,
        loading: true,
        data: {
          awardUserId: this.state.giftId,
        },
      })
      .then((res) => {
        res.data.orderTimeText = utilDate.format(new Date(res.data.orderTime), 'yyyy-MM-dd hh:mm:ss');

        res.data.salePrice = (res.data.salePrice / 100).toFixed(2);
        res.data.totalPrice = (res.data.salePrice * res.data.itemCount).toFixed(2);

        this.setState({
          orderDetail: res.data,
        },()=>{
          this.setSubhead();
        });


      });
  }

  // 获取拆礼礼详情
  getOpenGiftDetail=()=>{
    wxApi
      .request({
        url: api.giveGift.openGiftDetail,
        method: 'POST',
        loading: true,
        data: {
          awardId: this.state.giftId,
          userId: wxApi.getStorageSync('userId'),
        },
      })
      .then((res) => {
        res.data.list = res.data.list || [];
        const isJoin = res.data.list.some((user) => user.userId === wxApi.getStorageSync('userId'));

        const isSelf = wxApi.getStorageSync('userId') === res.data.userId;
        res.data.orderTimeText = utilDate.format(new Date(res.data.orderTime), 'yyyy-MM-dd hh:mm:ss');

        res.data.itemAttribute = res.data.itemTitle;
        res.data.thumbnail = res.data.itemPic;
        res.data.salePrice = (res.data.itemPrice / 100).toFixed(2);
        res.data.totalPrice = (res.data.salePrice * res.data.itemCount).toFixed(2);

        res.data.greetingWord = res.data.greetingWords;
        res.data.receiveStatus = res.data.awardStatus;

        // 获取中奖用户的收礼id
        if (res.data.awardStatus === 3) {
          res.data.list &&
            res.data.list.some((item) => {
              if (wxApi.getStorageSync('userId') === item.userId) {
                this.setState({
                  winUserGiftId: item.id,
                });

                return true;
              }
            });
        }

        this.setState({
          isJoin,
          isSelf,
          isPastDue: new Date().getTime() >= res.data.endTime || res.data.maxPlayer <= res.data.list.length,
          orderDetail: res.data,
        },()=>{
          this.setSubhead();
        });
      });
  }

  // 支付订单（待付款支付）
  payOrder=()=>{
    wxApi
      .request({
        url: api.order.payOrder,
        method: 'GET',
        loading: true,
        data: {
          orderNo: this.state.orderDetail.orderNo,
          payChannel: 1, // 微信支付
        },
      })
      .then((order) => {
        // 调用微信支付api
        wxApi.requestPayment({
          timeStamp: order.data.timeStamp + '',
          nonceStr: order.data.nonceStr,
          package: 'prepay_id=' + order.data.prepayId,
          signType: order.data.signType,
          paySign: order.data.paySign,
          success: () => {
            wxApi.showToast({
              icon: 'success',
              title: '支付成功',
              mask: true,
              complete: () => {
                this.toOrderPage();
                // this.getReceiveGiftDetail();
              },
            });
          },
          fail: () => {
            wxApi.showToast({
              icon: 'none',
              title: '支付已取消',
              mask: true,
            });
          },
        });
      });
  }

  // 副标题
  setSubhead() {
    let text = '';

    switch (this.state.detailType) {
      // 送礼详情
      case 1:
        text = '礼物会快递发给好友';
        break;
      // 收礼详情
      case 2:
        text = '';
        break;
      // 拆礼详情
      case 3:
        if (
          new Date().getTime() >= this.state.orderDetail.endTime ||
          this.state.orderDetail.maxPlayer === this.state.orderDetail.list.length
        ) {
          text = '已开奖';
        } else {
          text = `${utilDate.format(new Date(this.state.orderDetail.endTime), 'yyyy-MM-dd hh:mm:ss')} 开奖/满人开奖`;
        }
        break;
    }

    this.setState({
      subhead: text,
    });
  }

  render() {
    const {
      backgroundImage,
      orderDetail,
      defaultBackgroundCoverUrl,
      detailType,
      subhead,
      isSelf,
      isFold,
      tmpStyle,
      isPastDue,
      isJoin,
    } = this.state
    const userInfo = this.props.userInfo
    return (
      <View data-scoped="wk-mpgd-Detail" className="wk-mpgd-Detail give-gift-detail">
        <View className="container">
          {orderDetail.greetingImgUrl ? (
            <View className="cover" style={_safe_style_("background-image: url('" + backgroundImage + "');")}>
              <View
                className="cover-img"
                style={_safe_style_("background-image: url('" + orderDetail.greetingImgUrl + "')")}
              ></View>
              <View className="blessing">{orderDetail.greetingWord || ''}</View>
            </View>
          ) : (
            <View
              className="nocover"
              style={_safe_style_("background-image: url('" + defaultBackgroundCoverUrl + "');")}
            >
              {orderDetail.greetingWord || ''}
            </View>
          )}

          {detailType === 2 && (
            <View className="giver">
              <View
                className="avatar"
                style={_safe_style_("background-image: url('" + orderDetail.sendUserVO.userAvatar + "');")}
              ></View>
              <View className="name">{orderDetail.sendUserVO.nickName || ''}</View>
              <View className="words">{orderDetail.greetingWord || ''}</View>
            </View>
          )}

          <GoodsTitle isWeight title="礼物" text={subhead}></GoodsTitle>
          {/*  <View class="refundFee" wx:if="{{detailType === 1 && orderDetail.refundFee}}">退款金额: ￥{{orderDetail.refundFee}}</View>  */}
          <View className="gift" onClick={this.toGoodsPage}>
            <View className="main">
              <View
                className="image"
                style={_safe_style_("background-image: url('" + orderDetail.thumbnail + "');")}
              ></View>
              <View className="title">{orderDetail.itemName}</View>
              <View className="sku">{orderDetail.itemAttribute || ''}</View>
              {isSelf && <View className="quantity">{'x' + orderDetail.itemCount}</View>}

              {isSelf && (
                <View className="amount">
                  <View className="price">{'￥' + orderDetail.salePrice}</View>
                  <View
                    className="total"
                    onClick={this.toggle.bind(this)}
                    // onTouchMove={this.privateStopNoop}
                  >
                    {'￥' + orderDetail.totalPrice}
                  </View>
                </View>
              )}

              <View
                className="arrow"
                onClick={this.toggle.bind(this)}
                // onTouchMove={this.privateStopNoop}
                style={_safe_style_('transform: ' + (isFold ? 'rotate(0deg)' : 'rotate(180deg )') + ';')}
              ></View>
            </View>
            {isFold && (
              <View className="fold-box">
                <View className="fold-item">
                  <View className="title">订单编号</View>
                  <View className="text">{orderDetail.orderNo}</View>
                </View>
                <View className="fold-item">
                  <View className="title">下单时间</View>
                  <View className="text">{orderDetail.orderTimeText}</View>
                </View>
              </View>
            )}
          </View>
          {/*  送礼列表  */}
          {detailType === 1 && (
            <View className="recipients">
              <View className="header">
                <View className="tag">已领取</View>
                <View className="count">
                  {orderDetail.awardReceiveUserVOList.length + '/' + (orderDetail.maxPlayer || 1)}
                </View>
              </View>
              {orderDetail.awardReceiveUserVOList &&
                orderDetail.awardReceiveUserVOList.map((item, index) => {
                  return (
                    <View className="recipient" key={item.userId}>
                      <View
                        className="avatar"
                        style={_safe_style_("background-image: url('" + item.userAvatar + "');")}
                      ></View>
                      <View className="name">
                        <View className="left">{item.nickName}</View>
                        <View className="right">{item.statusDesc}</View>
                      </View>
                      <View className="words">
                        <View>{item.reply || ''}</View>
                        <View>{item.createTimeText}</View>
                      </View>
                    </View>
                  );
                })}
            </View>
          )}

          {/*  拆礼物列表  */}
          {detailType === 3 && (
            <View className="join">
              <View className="header">
                <View className="tag">参与人数</View>
                <View className="count">{(orderDetail.list?orderDetail.list.length:'0') + '/' + (orderDetail.maxPlayer || 1)}</View>
              </View>
              <View className="list">
                {orderDetail.list &&
                  orderDetail.list.map((item, index) => {
                    return (
                      <View
                        className="avatar"
                        key={item.userId}
                        style={_safe_style_('background-image: url(' + item.avatarImgUrl + ')')}
                      ></View>
                    );
                  })}
              </View>
            </View>
          )}
        </View>
        <View className="footer">
          {detailType === 1 && orderDetail.receiveStatus === 1 && (
            (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true'?
              (
                <Button
                  className="btn-item"
                  onClick={this.handleSelectChanel.bind(this)}
                  style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                >
                  立即送礼
                </Button>
              ): (
                <Button
                  className="btn-item"
                  data-giftId={orderDetail.id}
                  openType="share"
                  style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                >
                  立即送礼
                </Button>
              )
            )
          )}

          {detailType === 1 && orderDetail.receiveStatus !== 1 && (
            <View
              className="btn-item"
              style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              onClick={this.toGoodsPage}
            >
              再送一次
            </View>
          )}

          {/*  收礼详情  */}
          {detailType === 2 && (orderDetail.receiveStatus === 3 || orderDetail.receiveStatus === 4) && (
            (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true'?
              (
                <View className="btn-item btn-box">
                  <Button onClick={this.handleSelectChanel.bind(this)} className="btn lf">
                    转赠
                  </Button>
                  <View
                    className="btn rg"
                    onClick={this.setAddress}
                    style={_safe_style_('background: ' + tmpStyle.btnColor + ';border-color: ' + tmpStyle.btnColor)}
                  >
                    填写地址
                  </View>
                </View>
              ): (
                <View className="btn-item btn-box">
                  <Button openType="share" data-userIdGiven={userInfo.id} data-giftId={orderDetail.id} className="btn lf">
                    转赠
                  </Button>
                  <View
                    className="btn rg"
                    onClick={this.setAddress}
                    style={_safe_style_('background: ' + tmpStyle.btnColor + ';border-color: ' + tmpStyle.btnColor)}
                  >
                    填写地址
                  </View>
                </View>
              )
            )
          )}

          {detailType === 2 && orderDetail.receiveStatus === 6 && (
            <View
              className="btn-item"
              style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              onClick={this.payOrder}
            >
              待付款
            </View>
          )}

          {detailType === 2 && orderDetail.receiveStatus === 7 && (
            <View
              className="btn-item"
              style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              onClick={this.toOrderPage}
            >
              查看订单
            </View>
          )}

          {detailType === 2 && (orderDetail.receiveStatus === 8 || orderDetail.receiveStatus === 5) && (
            <View className="btn-item" style={_safe_style_('background: #D6D6D6;')}>
              {orderDetail.receiveStatusDesc}
            </View>
          )}

          {/*  拆礼详情  */}
          {detailType === 3 && isSelf && !isPastDue && (
            (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true'?
              (
                <Button
                  className="btn-item"
                  onClick={this.handleSelectChanel.bind(this)}
                  style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                >
                  立即送礼
                </Button>
              ): (
                <Button
                  className="btn-item"
                  data-giftId={orderDetail.awardId}
                  openType="share"
                  style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
                >
                  立即送礼
                </Button>
              )
            )
          )}

          {detailType === 3 && isSelf && isPastDue && (
            <View
              className="btn-item"
              style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              onClick={this.toGoodsPage}
            >
              再送一次
            </View>
          )}

          {detailType === 3 && orderDetail.receiveStatus === 2 && !isSelf && (
            <View
              className="btn-item"
              style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              onClick={this.toGoodsPage}
            >
              立即下单
            </View>
          )}

          {detailType === 3 && orderDetail.receiveStatus === 1 && !isSelf && isJoin && (
            <View className="btn-item" style={_safe_style_('background: #D6D6D6;')}>
              已参加，等待开奖
            </View>
          )}

          {detailType === 3 && orderDetail.receiveStatus === 3 && !isSelf && (
            <View
              className="btn-item"
              style={_safe_style_('background: ' + tmpStyle.btnColor + ';')}
              onClick={this.setAddress}
            >
              去收礼
            </View>
          )}

          {detailType === 3 && orderDetail.receiveStatus === 3 && !isSelf && orderDetail.playRuler === 2 && (
            <View className="hint">恭喜你，中奖啦</View>
          )}

          {detailType === 3 && orderDetail.receiveStatus === 2 && !isSelf && orderDetail.playRuler === 2 && (
            <View className="hint">很抱歉，没有中奖</View>
          )}
        </View>

        {/*  分享对话弹框  */}
        <ShareDialog childRef={this.detailShareDialogRef}></ShareDialog>
      </View>
    );
  }
}


export default Detail;
