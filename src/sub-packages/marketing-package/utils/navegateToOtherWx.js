import Taro from '@tarojs/taro';

export default (params) => {
  Taro.navigateToMiniProgram(params);
};
