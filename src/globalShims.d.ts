import getStaticImgUrl from '../src/wxat-common/constants/frontEndImgUrl'
export {} declare global {
  interface Window {
    $GetStaticImgUrl?: getStaticImgUrl
  }
}