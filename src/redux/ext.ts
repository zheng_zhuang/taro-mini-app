const INITIAL_STATE = {
  appId: null,
  epId: null,
  maAppId: null,
  sellerId: null,
  sellerTemplateId: null,
};

export const updateExtAction = (data) => {
  return {
    type: 'updateExt',
    data,
  };
};

export default function reducers(state = INITIAL_STATE, action) {
  const { type, data } = action;
  if (type === 'updateExt') {
    return { ...state, ...data };
  }
  return state;
}
