import { combineReducers } from 'redux';
import globalData from './global-data';
import base from './base';
import ext from './ext';

export default combineReducers({
  globalData,
  base,
  ext,
});
