import Taro from '@tarojs/taro';

const INITIAL_STATE = {
  appReady: true,
  userInfo: null,
  wkb_userInfo: null, // 唯客宝的账户信息
  extAppid: null,
  sellerId: null,
  appId: null,
  /*当前门店*/
  sellerTemplateId: null,
  currentStore: {},
  maAppId: null,
  epId: null,
  sellerInfoResovle: null,
  industry: 'retail',
  homeChangeTime: 0,
  tabbars: null,
  wxExtTabbars: null /* 写给微信的tabbars,所有写在tabbar里的页面 */,
  themeConfig: null,
  mineConfig: null,
  launchOptForQrCodeReport: null,
  // 分销员id，全局分销共享，内存级别的。该分销id，是通过他人分享后进入小程序获取的分销id，如果自己是分销员的话，这里并不是自己的分销id。
  distributorId: 0,
  subscribeList: {}, // 订阅消息模板
  // https://work.weixin.qq.com/api/doc/90001/90144/92392
  // 在企业微信中，会额外返回一个 environment 字段（微信中不返回），如此字段值为 wxwork，则表示当前小程序运行在企业微信环境中
  environment: (Taro.getSystemInfoSync() as any).environment || 'wechat',

  // 启动来源
  launchRef: {
    _ref_bz: '',
    _ref_useId: '',
    _ref_storeId: '',
    _ref_wxOpenId: '',
  },

  isShowGiveGift: false, // 是否开启送礼
  messageTemplateId: {}, // 消息模板
  hideMarketingDialogPageList: [], // 要隐藏的营销弹窗page路径list
  dealFile: '', // 充值协议

  iosSetting: null, //ios支付设置
  isIOS: null, //是否ios
};

export const updateGlobalDataAction = (data) => {
  return {
    type: 'updateGlobalData',
    data,
  };
};

export default function reducers(state = INITIAL_STATE, action) {
  const { type, data } = action;
  if (type === 'updateGlobalData') {
    return { ...state, ...data };
  }
  return state;
}
