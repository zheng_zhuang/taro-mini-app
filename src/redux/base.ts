const INITIAL_STATE = {
  /** 微信授权访问的用户信息 **/
  wxUserInfo: null,
  /** 后台登录的用户信息，包含等级信息等 **/
  userInfo: null,
  /** 惟客宝登录后的信息*/
  loginInfo: null,
  /*门店列表*/
  storeList: null,
  /*当前门店*/
  currentStore: {},
  /*附近门店可多选时，预选的门店列表*/
  pendingChooseStoreList: null,
  /** 惟客宝登录后的sessionId*/
  sessionId: null,
  /** 惟客宝小程序对应的店铺Id*/
  appId: null,
  /** 钟意**/
  focusOnServePerson: null,
  /** 是否已注册**/
  registered: false,
  /**当前店铺管理的店铺是否显示*/
  currentStoreViewVisible: false,
  /** 店铺配送方式，默认是快递*/
  appInfo: null,
  /** 登录过程是否发生错误 */
  loginEnvError: false,
  /** 登录过程是否发生错误 */
  loginEnvErrorMessage: '',
  /** tabbar是否准备好 */
  tabbarReady: false,
  /** 当前经纬度 **/
  gps: {
    longtitude: 0,
    latitude: 0,
  },
  /** 当前定位门店 */
  storeVO: null,
  /** 当前门店个人中心配置 **/
  mineConfig: {},
  /** 当前播放的视频id **/
  currentVideoId: null,
  // 是否有员工核销的权限
  chargeOff: false,
  // 是否携旅员工
  enterpriseStaff: null,
  // 携旅员工信息
  enterpriseStaffInfo: null,
  /**
   * 分享来源：
   *  wechat 微信分享
   *  wxwork 企微分享
   */
  source: 'wechat',
};

export const updateBaseAction = (data) => {
  return {
    type: 'updateBase',
    data,
  };
};

export default function reducers(state = INITIAL_STATE, action) {
  const { type, data } = action;
  if (type === 'updateBase') {
    return { ...state, ...data };
  }
  return state;
}
