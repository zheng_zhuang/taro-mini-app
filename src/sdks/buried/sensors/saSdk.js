const sensors = require('@/sdks/buried/sensors/sensorsdata.min');

sensors.setPara({
// 传入后台配置的全局变量
  name: 'sensors',
  server_url: 'https://trace.ctlife.tv:8898/miniapp/send',
  autoTrack: {
    appLaunch: true, // 默认为 true，false 则关闭 $MPLaunch 事件采集·
    appShow: true, // 默认为 true，false 则关闭 $MPShow 事件采集
    appHide: true, // 默认为 true，false 则关闭 $MPHide 事件采集
    pageShow: true, // 默认为 true，false 则关闭 $MPViewScreen 事件采集
    pageShare: true, // 默认为 true，false 则关闭 $MPShare 事件采集
    mpClick: true, // 默认为 false，true 则开启 $MPClick 事件采集
    mpFavorite: true, // 默认为 true，false 则关闭 $MPAddFavorites 事件采集
    pageLeave: true, // 默认为 false， true 则开启 $MPPageLeave事件采集
  },
  // 自定义渠道追踪参数，如 source_channel: ["custom_param"]
  source_channel: [],
  // 是否允许控制台打印查看埋点数据(建议开启查看)
  show_log: false,
  // 是否允许修改 onShareAppMessage 里 return 的 path，用来增加(登录 ID，分享层级，当前的 path)，在 app onShow 中自动获取这些参数来查看具体分享来源、层级等
  allow_amend_share_path: true,
  heatmap: {
    clickmap: 'default',
  },
});

sensors.init();

sensors.registerApp({
  platform: '携旅小屏跨多端开发平台',
});
export default sensors;
