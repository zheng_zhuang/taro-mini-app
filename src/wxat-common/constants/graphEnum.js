export default {
  STATUS: {
    OFF_SHELVE: {
      label: '已下架',
      value: 0,
    },
    ON_SHELVE: {
      label: '已上架',
      value: 1,
    },
    SUCCESS: {
      label: '已删除',
      value: 2,
    },
  },
};
