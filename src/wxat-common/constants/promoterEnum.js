const bindPromoterEnum = {
    pending: 0, // 待审核;
    pass: 1, // 1:审核通过;
    reject: 2 //2:审核未通过
  }

export { bindPromoterEnum }
