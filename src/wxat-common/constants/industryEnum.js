// import state from '../././state/index.js'
import store from '../../store';

export default {
  type: {
    beauty: {
      value: 'beautiful',
      id: 1,
    },
    restaurant: {
      value: 'restaurant',
      id: 2,
    },
    retail: {
      value: 'retail',
      id: 3,
    },
    estate: {
      value: 'estate',
      id: 88,
    },
  },
  appTag: {
    jianpai: 'jianpai',
  },
  findIndustryById(id) {
    const specKey = Object.keys(this.type).find((key) => {
      return this.type[key].id === id;
    });
    return this.type[specKey];
  },
  isSpecApp(appTag) {
    const { base } = store.getState();
    const appInfo = base.appInfo;
    return appInfo && appInfo.appFeatures && appInfo.appFeatures.indexOf(appTag) !== -1;
  },
};
