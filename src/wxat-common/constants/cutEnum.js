export default {
  STATUS: {
    TIME_OUT: {
      label: '砍价已过期',
      value: 0,
    },
    CUTING: {
      label: '砍价中',
      value: 1,
    },
    SUCCESS: {
      label: '砍价成功',
      value: 2,
    },
    PAID: {
      label: '已支付',
      value: 3,
    },
    NOT_PAY: {
      label: '待支付',
      value: 4,
    },
    REFUNDING: {
      label: '退款中',
      value: -2,
    },
    REFUND_COMPLETED: {
      label: '退款完成',
      value: -3,
    },
  },
  activityStatus: {
    NO_START: 0, //未开始
    CUTING: 1, //进行中
    END: 2, //已结束
  },
};
