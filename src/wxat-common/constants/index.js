export default {
  order: {
    payChannel: {
      // 微信支付
      WX_PAY: '1',
      // 支付宝支付
      ALIPAY: '2',
      // 账户余额
      ACCOUNT: '3',
      // 次数卡抵扣自
      CARD_COUPON: '4',
      // 无需支付
      NO_NEED_PAY: '5',
    },

    // 发货类型
    shippingType: {
      SELF_SUPPORT: 1, // 自有物流
      THIRD_PART: 2, // 三方物流
      NO_NEED: 3, // 无需物流
    },

    // 发货类型中文

    expressTypeCN: {
      0: {
        value: 'SELF_PICKUP',
        label: '到店自提',
      },
      1: {
        value: 'DELIVERY',
        label: '快递配送',
      },
      2: {
        value: 'NO_EXPRESS',
        label: '免物流',
      },
      3: {
        value: 'INTRA_CITY',
        label: '同城配送',
      },
    },

    // 订单状态
    orderStatus: {
      // 待支付
      waitPay: 10,
      // 已支付
      paid: 20,
      // 商家已确认
      confirm: 30,
      // 等待骑手接单
      taking: 31,
      // 已入住
      checkin: 35,
      // 定金订单已支付，待核销
      frontMoneyPaid: 38,
      // 已发货
      shipping: 40,
      // 退款中
      refund: -10,
      // 退款成功
      refundSuccess: -20,
      // 退款已拒绝
      refundReject: -30,
      // 订单已完成
      success: 60,
      // 订单已取消
      cancel: -1,
      // 骑手配送中
      distribution: 41,
      labelList: [
        {
          value: 'all',
          label: '全部',
        },
        {
          value: 'wait_pay',
          label: '待付款',
        },
        {
          value: 'wait_off',
          label: '待核销',
        },
        {
          value: 'wait_shipping',
          label: '待发货',
        },
        {
          value: 'wait_receive',
          label: '待收货',
        },
        {
          value: 'on_going',
          label: '进行中',
        },
        {
          value: 'done',
          label: '已完成',
        },
        {
          value: 'after_sale',
          label: '售后',
        },
      ],
      valueList: [
        {
          value: 'all',
          label: null,
        },
        {
          value: 'wait_pay',
          label: [10],
        },
        {
          value: 'wait_off',
          label: [38],
        },
        {
          value: 'wait_shipping',
          label: [20, 30, 31, 38],
        },
        {
          value: 'wait_receive',
          label: [40, 41],
        },
        {
          value: '',
          label: [60, -30],
        },
        {
          value: 'after_sale',
          label: [-10, -20, -30],
        },
        {
          value: 'done',
          label: [60],
        },
      ],
    },

    // 退款状态
    refundStatus: {
      request_refund: -1, // 申请退款
      agree_refund_request: -2, // 已同意申请
      refund_item_shipping: -3, // 退款商品已发货
      confirm_refund_item: -4, // 已确认退货
      confirm_refund: -5, // 已确认退款
      refund_reject: -6, // 已拒绝退款
      refund: -10, // 退款中
      refund_success: -20, // 退款完成
      refund_failed: -30, // 退款失败
    },

    // BASELINE 上拉加载, 到底部了,
    loadMoreStatus: {
      HIDE: 0,
      LOADING: 1,
      ERROR: 2,
      BASELINE: 3
    },

    appointmentStatus: {
      // 待支付
      waitPay: 10,
      waitService: 70,
      overtime: 80,
      // 已支付
      paid: 20,
      // 商家已确认
      confirm: 30,
      // 已发货
      shipping: 40,
      // 申请退款
      refund: -10,
      // 退款成功
      refundSuccess: -20,
      // 退款已拒绝
      refundReject: -30,
      success: 60,
      cancel: -1,
      labelList: ['全部', '待付款', '待服务', '已超时', '已完成', '已取消'],
      valueList: [null, [10], [70], [80], [60], [-1]],
    },

    // 服务订单状态
    serviceStatus: {
      // 待支付
      notPay: 10,
      // 待接单
      confirmed: 30,
      // 服务中
      shinpping: 40,
      // 待完成
      unsuccess: 49,
      // 服务完成
      success:60,
      labelList: [{
        value: "",
        label: "全部"
      },
      {
        value: 30,
        label: "待接单"
      },
      {
        value: 40,
        label: "服务中"
      },
      {
        value: 49,
        label: "待完成"
      },
      {
        value: 60,
        label: "已完成"
      },
      ],
      valueList: [
        {
          value: "all",
          label: null
        },
        {
          value: "wait_pay",
          label: [10]
        },
        {
          value: "wait_off",
          label: [38]
        },
        {
          value: "wait_shipping",
          label: [20, 30, 31, 38]
        },
        {
          value: "wait_receive",
          label: [40, 41]
        },
        {
          value: "",
          label: [60, -30]
        },
        {
          value: "after_sale",
          label: [-10, -20, -30]
        },
        {
          value: "done",
          label: [60]
        }
      ]
    },

    microStatus: {
      // 待审核
      confirm: 10,
      waitService: 70,
      overtime: 80,
      // 待下单
      paid: 20,
      // 待支付
      waitPay: 30,
      // 使用中
      used: 37,
      // 调货中
      shipping: 40,
      // 待收货
      receipt: 50,
      // 申请退款
      refund: -10,
      // 退款成功
      refundSuccess: -20,
      // 退款已拒绝
      refundReject: -30,
      success: 60,
      cancel: -1,
      itemType: 18,
      labelList: ['全部', '审核中', '待付款', '调货中', '待收货', '已完成'],
      valueList: [null, [10], [20, 30], [40], [50], [60]],
    },
  },

  goods: {
    type: {
      serve: 'serve',
      product: 'product',
      group: 'group',
      seckill: 'seckill',
      sleeveSystem: 'sleeveSystem',
    },
    itemType: {
      serve: 0,
      product: 1,
      // 订购
      micro: 20,
      sleeveSystem: 29,
    },
    /* 运费模板类型，buyerBear：买家承担，freeShipping：卖家包邮 freightCollect：运费到付 */
    freightType: {
      buyerBear: 0,
      freeShipping: 1,
      freightCollect: 2,
    },
  },

  ticket: {
    oneday: 0,
    package: 1,
  },

  card: {
    type: {
      // 次数卡
      coupon: 3,
      // 充值卡
      charge: 4,
      // 礼品卡
      gift: 14,
      // 代金卡包
      cardPack: 35,
      // 权益卡
      equityCard: 37,
    },
  },
  coupon: {
    status: {
      EFFECTIVE: 1, // 有效
      USED: 2, // 已使用
      EXPIRED: 3, // 已过期
    },
    alreadyHave: {
      NOT: null, // 未领取
      RECEIVED: 1, // 领取
    },
    labelList: ['未使用', '已使用', '已过期'],
  },
  tag: {
    NONE: 0, // 不作任何设置
    WKB_PACKAGE_ORDER: 1, // 惟客宝套餐订单
    SCHEDULE_ORDER: 1 << 1, // 服务商品订单
    NO_NEED_PAY: 1 << 2, // 无需支付订单
    NEED_CONFIRM: 1 << 3, // 是否需要商家确认
  },

  // 拼团状态
  group: {
    status: {
      ON_PROGRESS: -1, // 拼团中
      SUCCESS: 1, // 拼团成功
      FAILED: 0, // 拼团失败
      REFUNDING: -2, // 退款中
      REFUND_COMPLETED: -3, // 退款完成
    },
  },

  // 拼团活动状态
  groupActivity: {
    status: {
      DELETED: -2, // 已删除
      NOT_START: -1, // 未开始
      SHELF: 1, // 进行中
      NOT_SHELF: 0, // 已结束
    },
  },

  verification: {
    MAX_LOOP_TIMES: 200, // 最大轮寻次数
    LOOP_INTERVAL: 3000, // 轮寻间隔：5秒

    type: {
      SCHEDULED: 1, // 预约核销
      COUPON: 2, // 优惠券核销
      CARD: 3, // 服务卡核销
    },

    status: {
      SUCCESS: 0, // 核销成功
      NOT_PAY: 530004, // 该订单未支付
      NOT_PERMIT: 530005, // 没有核销权限
      NOT_APPLICABLE_STORE: 530006, // 该核销不适用门店
      INADEQUATE_CARD: 530007, // 核销次数卡不足
    },
  },
  comment: {
    // 评价等级
    grade: {
      // 全部
      All: null,
      // 好评
      PRAISE: 1,
      // 中评
      MIDDLE: 2,
      // 差评
      BAD: 3,
    },
    sellerReplyStatus: {
      // 已回复
      REPLIED: 1,
      // 未回复
      NOREPLY: 2,
    },
  },

  // 红包拆分状态
  redPacket: {
    ONGOING: {
      value: 0,
      label: '进行中',
    },
    SUCCESS: {
      value: 1,
      label: '已完成',
    },
    FAIL: {
      value: 2,
      label: '失败',
    },
    ACTIVITY_FINISH: {
      value: 3,
      label: '活动停止',
    },
  },

  redPacketActivictyStatus: {
    INVALID: {
      value: 0,
      label: '无效',
    },
    FINISH: {
      value: 2,
      label: '已结束',
    },
    ONGOING: {
      value: 1,
      label: '活动进行中',
    },
    NOT_START: {
      value: -1,
      label: '未开始',
    },
  },

  // 红包使用状态
  useStatus: {
    UNAVAILABLE: {
      value: 0,
      label: '不可用',
    },
    AVAILABLE: {
      value: 1,
      label: '可用',
    },
    USED: {
      value: 2,
      label: '已使用',
    },
    EXPIRED: {
      value: 3,
      label: '已过期',
    },
    DELETE: {
      value: 4,
      label: '已删除',
    },
    INUSE: {
      value: 5,
      label: '使用中',
    },
  },

  giftCard: {
    cardStatus: {
      // 未激活
      UNUSED: 0,
      // 激活
      USED: 1,
    },
    ownerStatus: {
      // 自用
      SELF: 0,
      // 赠送
      GIVE: 1,
      // 获赠
      RECEIVED: 2,
    },
  },

  PREFIX_BASE64: 'data:image/png;base64,',

  // 商品类型
  salesTypeEnum: {
    ONLINE: 0, // 线上商品
    OFFLINE: 1, // 线下商品
    NO_STOCK: 2, // 无实际库存商品
  },
};
