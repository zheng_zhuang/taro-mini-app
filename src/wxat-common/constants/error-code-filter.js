// 不显示showToast提示框的错误码集合
export default {
  groupFullCode: {
    value: 110086,
    label: '参团人数已满',
  },
  seckillRemainLackCode: {
    value: 110087,
    label: '秒杀活动商品剩余数量不足',
  },
};
