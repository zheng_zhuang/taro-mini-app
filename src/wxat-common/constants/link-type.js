import reportContants from '@/sdks/buried/report/report-constants.js';
import wxApi from '../utils/wxApi';
import api from "../api";
import authHelper from '../utils/auth-helper.js';

const navigate = {
  HOME: `/wxat-common/pages/home/index`,
  DEAL: `/sub-packages/mine-package/pages/deal/index`,
  MINE_COUPON: '/wxat-common/pages/coupon-list/index',
  RED_PACKET_OPEN: '/sub-packages/marketing-package/pages/red-packet/index',

  ACTIVITY: `/sub-packages/marketing-package/pages/activity/index`,
  CUSTOM: `/wxat-common/pages/custom/index`,
  CATEGORY_DETAIL: `/sub-packages/marketing-package/pages/category-detail/index`,
  // LUCKY_DIAL: '/sub-packages/marketing-package/pages/lucky-dial/index',
  LUCKY_DIAL: '/sub-packages/moveFile-package/pages/lucky-dial/index',
  VOUCHER_CENTER: '/sub-packages/mine-package/pages/recharge-center/index',
  SIGN_IN: '/sub-packages/marketing-package/pages/signIn/index',
  COUPON_CENTER: '/sub-packages/mine-package/pages/coupon/index',
  // COUPON_CENTER: '/sub-packages/moveFile-package/pages/coupon-list/index',
  INTEGRAL_MALL: '/sub-packages/moveFile-package/pages/mine/integral-mall/index',
  GIFT_CARD_LIST: '/sub-packages/marketing-package/pages/gift-card/bg-list/index',
  GROUP_LIST: '/sub-packages/marketing-package/pages/group/list/index',
  CUT_PRICE_LIST: '/sub-packages/marketing-package/pages/cut-price/list/index',
  SEC_KILL_LIST: '/sub-packages/marketing-package/pages/seckill/list/index',
  GRAPH_LIST: '/sub-packages/marketing-package/pages/graph/list/index',
  PRODUCTS: '/sub-packages/marketing-package/pages/more/index?type=1',
  SERVES: '/sub-packages/marketing-package/pages/more/index?type=0',
  STORE_LIST: '/wxat-common/pages/store-list/index',
  MEMBER_CODE: 'sub-packages/mine-package/pages/member-code/index',
  MINE_CUT_PRICE: '/sub-packages/mine-package/pages/cut-price/index',
  MINE_GROUP: '/sub-packages/mine-package/pages/group/index',
  MINE_RED_PACKET: '/sub-packages/marketing-package/pages/red-packet/red-packet-list/index',
  CART: '/wxat-common/pages/shopping-cart/index',
  MINE_APPOINTMENT: '/sub-packages/server-package/pages/appointment/list/index',
  BIND_MEMBER: 'sub-packages/mine-package/pages/member-card/index',
  ADDRESS_MANAGE: '/sub-packages/mine-package/pages/address/index',
  DISTRIBUTION: '/sub-packages/distribution-package/pages/center/index',
  MINE_CENTER: '/sub-packages/mine-package/pages/user-info/index',
  FREE_CENTER: '/sub-packages/mine-package/pages/free-center/index',
  INTERNAL_PURCHASE: '/sub-packages/distribution-package/pages/internal-purchase/center/index',
  TASK_CENTER: '/sub-packages/marketing-package/pages/task-center/index',
  FORM_TOOL: '/sub-packages/marketing-package/pages/form-tool/index',
  CARD_PACK: '/sub-packages/marketing-package/pages/card-pack/list/index',
  CARD_DETAIL: '/sub-packages/marketing-package/pages/card-pack/detail/index',
  MY_RESERVATION: '/sub-packages/marketing-package/pages/micro-decorate/order/index',
  E_BOOKS: '/sub-packages/marketing-package/pages/ebooks/index',
  PROMOTION_AMBASSADOR: 'sub-packages/marketing-package/pages/promotion-amb/index',
  MINE_DRUG:'/sub-packages/medicine-package/pages/prescription/my/index',
  MY_GIFT: '/sub-packages/marketing-package/pages/give-gift/my-gift/index', // 我的礼物链接
  VERIFICATION: '/sub-packages/order-package/pages/staff-verification/index',
  MYACTIVITY: '/sub-packages/marketing-package/pages/activity-module/my-activities/index',
  WHOLE_FAMILY: '/sub-packages/order-package/pages/whole-family/index',
  VIRTUAL_GOODS: '/sub-packages/marketing-package/pages/virtual-goods/my/index', // 虚拟商品列表
  LIVING_SERVICE: '/sub-packages/marketing-package/pages/living-service/order-list/index', // 订单列表
  PERSONAL_CENTER:'/sub-packages/separate-package/pages/personal-center/index' //分佣分账个人中心
};

export default {
  navigate,
  CUSTOM: {
    value: 'custom',
    desc: '自定义页',
    getLink (route) {
      route = route.split('=')[1];
      return navigate.CUSTOM + `?route=${route}`;
    },
  },

  ACTIVITY: {
    value: 'activity',
    desc: '活动详情页',
    getLink (topicId) {
      return navigate.ACTIVITY + `?topicId=${topicId}`;
    },
  },

  CATEGORY: {
    value: 'category',
    desc: '类目详情页',
    getLink (id, name) {
      return (
        navigate.CATEGORY_DETAIL + `?id=${id}&name=${name}&type=1&source=${reportContants.SOURCE_TYPE.nav_item.key}`
      );
    },
  },
  SERVER_CATEGORY: {
    value: 'server-category',
    desc: '服务类目详情页',
    getLink (id, name) {
      return (
        navigate.CATEGORY_DETAIL + `?id=${id}&name=${name}&type=0&source=${reportContants.SOURCE_TYPE.nav_item.key}`
      );
    },
  },
  LUCKY: {
    value: 'lucky',
    desc: '幸运大转盘',
    isCheckAuth: false,
    getLink (id, name) {
      if (id) {
        return `${navigate.LUCKY_DIAL}?id=${id}`;
      }
    },
    checkStatus (value) {
      if (this.isCheckAuth && !authHelper.checkAuth()) return Promise.reject('未登录');
      return wxApi
        .request({
          url: api.luckyDial.getActivityDetails,
          data: {
            luckyTurningId: value.id,
          },
          loading: true,
        })
        .then((res) => {
          const flag = res.data.activityStatus === 3;
          if (!flag) {
            wxApi.showToast({
              title: '活动已结束',
              icon: 'none',
              duration: 1500,
              mask: true,
            });
          }
          return flag;
        });
    },
  },
  COUPON_CENTER: {
    value: 'coupon-center',
    desc: '领券中心',
    getLink () {
      return navigate.COUPON_CENTER;
    },
  },
  VOUCHER_CENTER: {
    value: 'voucher-center',
    desc: '充值中心',
    getLink () {
      return navigate.VOUCHER_CENTER;
    },
  },
  INTEGRAL_MALL: {
    value: 'integral-mall',
    desc: '积分商城',
    getLink () {
      return navigate.INTEGRAL_MALL;
    },
  },
  GIFT_CARD_LIST: {
    value: 'gift-card-list',
    desc: '礼品卡商城',
    getLink () {
      return navigate.GIFT_CARD_LIST;
    },
  },
  GROUP_LIST: {
    value: 'group-list',
    desc: '拼团列表',
    getLink () {
      return navigate.GROUP_LIST;
    },
  },
  CUT_PRICE_LIST: {
    value: 'cut-price-list',
    desc: '砍价列表',
    getLink () {
      return navigate.CUT_PRICE_LIST;
    },
  },
  SEC_KILL_LIST: {
    value: 'sec-kill-list',
    desc: '秒杀列表',
    getLink () {
      return navigate.SEC_KILL_LIST;
    },
  },
  GRAPH_LIST: {
    value: 'graph-list',
    desc: '图文列表',
    getLink () {
      return navigate.GRAPH_LIST;
    },
  },
  FREE_CENTER: {
    value: 'free-center',
    desc: '赠品中心',
    getLink () {
      return navigate.FREE_CENTER;
    },
  },

  INTERNAL_PURCHASE: {
    value: 'internal-purchase',
    desc: '内购专区',
    getLink () {
      return navigate.INTERNAL_PURCHASE;
    },
  },

  PRODUCTS: {
    value: 'products',
    desc: '产品列表',
    getLink () {
      return navigate.PRODUCTS;
    },
  },
  SERVES: {
    value: 'serves',
    desc: '服务列表',
    getLink () {
      return navigate.SERVES;
    },
  },
  STORE_LIST: {
    value: 'near-store',
    desc: '附近门店',
    getLink (id, name) {
      return navigate.STORE_LIST;
    },
  },
  SIGN_IN: {
    value: 'checkin',
    desc: '签到有礼',
    getLink (id, name) {
      return `${navigate.SIGN_IN}?id=${+id}`;
    },
    isCheckAuth: true,
    checkStatus (id) {
      if (this.isCheckAuth && !authHelper.checkAuth()) return Promise.reject('未登录');
      return wxApi
        .request({
          url: api.signIn.hasSignIn,
          loading: true,
        })
        .then((res) => {
          if (!res.data.signedActivityId) {
            wxApi.showToast({
              title: '活动已结束',
              icon: 'none',
              duration: 1500,
              mask: true,
            });
          }
          return !!res.data.signedActivityId;
        });
    },
  },
  FORM_TOOL: {
    value: 'formTool',
    desc: '表单',
    getLink (id, name) {
      if (id) {
        return `${navigate.FORM_TOOL}?formId=${id}`;
      }
    },
  },
  TASK_CENTER: {
    value: 'task-center',
    desc: '任务中心',
    getLink () {
      return `${navigate.TASK_CENTER}`;
    },
  },
  CARD_PACK: {
    value: 'card-pack',
    desc: '代金卡包',
    getLink (id) {
      return id ? `${navigate.CARD_DETAIL}?id=${id}` : navigate.CARD_PACK;
    },
  },
  MY_RESERVATION: {
    value: 'my-reservation',
    desc: '我的预约',
    getLink (id) {
      return `${navigate.MY_RESERVATION}`;
    },
  },
  E_BOOKS: {
    value: 'ebooks',
    desc: '电子画册',
    getLink (id) {
      return navigate.E_BOOKS;
    },
  },
  PERSONAL_CENTER: {
    value: 'personal-center',
    desc: '个人中心',
    getLink (id) {
      return navigate.PERSONAL_CENTER;
    },
  },
};
