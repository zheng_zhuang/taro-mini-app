export default {
  relationship: {
    SELF: { value: 0, name: '本人' },
    FAMILY: { value: 1, name: '家庭成员' },
    RELATIVE: { value: 2, name: '亲戚' },
    FRIEND: { value: 3, name: '朋友' },
  },
  relationshipList: [
    { value: 0, name: '本人' },
    { value: 1, name: '家庭成员' },
    { value: 2, name: '亲戚' },
    { value: 3, name: '朋友' },
  ],
};
