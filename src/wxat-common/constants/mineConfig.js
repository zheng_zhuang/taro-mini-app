import store from '../../store/index';

export default {
  /**
   * 个人信息配置config
   */
  infoConfig() {
    const globalData = store.getState().globalData;
    let infoConfig = {};
    if (globalData.mineConfig) {
      const mineInfoModule = globalData.mineConfig.find((item) => {
        return item.id === 'mineInfoModule';
      });
      infoConfig = mineInfoModule && mineInfoModule.config ? mineInfoModule.config : null;
    }
    return infoConfig;
  },

  /**
   * 订单配置config
   */
  orderConfig() {
    const globalData = store.getState().globalData;
    let orderConfig = {};
    if (globalData.mineConfig) {
      const mineOrderModule = globalData.mineConfig.find((item) => {
        return item.id === 'mineOrderModule';
      });
      orderConfig = mineOrderModule && mineOrderModule.config ? mineOrderModule.config : null;
    }
    return orderConfig;
  },

  /**
   * 工具栏配置config
   */
  navigationConfig() {
    const globalData = store.getState().globalData;
    let navigationConfig = {};
    if (globalData.mineConfig) {
      const mineNavigationModule = globalData.mineConfig.find((item) => {
        return item.id === 'mineNavigationModule';
      });
      navigationConfig = mineNavigationModule && mineNavigationModule.config ? mineNavigationModule.config : null;
    }
    return navigationConfig;
  },
};
