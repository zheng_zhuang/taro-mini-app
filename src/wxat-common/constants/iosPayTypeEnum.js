export default {
  payType: {
    JUST_LIKE_ANDROID: {
      label: '与安卓支付流程相同',
      value: 1
    },
    H5_PAY: {
      label: '站外h5支付',
      value: 2
    },
    IOS_PAY_NONSUPPORT: {
      label: 'IOS限制不支持支付',
      value: 3
    }
  }
}
