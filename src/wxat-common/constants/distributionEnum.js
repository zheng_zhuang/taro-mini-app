export default {
  applyStatus: {
    //申请中
    APPLYING: 0,
    //已通过
    SUCCESS: 1,
    //解除分销员绑定，不再是分销员
    UNBIND: 2,
    //拒绝申请
    REJECT: -1,
  },
  //佣金类型
  commissionType: {
    //自定义佣金金额
    amount: 1,
    //自定义佣金比例
    proportion: 2,
  },
  commissionStatusEnum: {
    //冻结中
    frozen: 0,
    //已结算
    settlement: 1,
    //已失效
    invalid: 2,
  },
  commissionStatus: {
    '0': '冻结中',
    '1': '已结算',
    '2': '已失效',
  },
  commissionMode: {
    '1': '推广佣金',
    '2': '服务佣金',
  },
  cashOutStatusEnum: {
    //提现中
    cashing: 0,
    //已发放
    success: 1,
  },
  cashOutStatus: {
    '0': '提现中',
    '1': '已发放',
  },
  //活动类型
  activityType: {
    distribution: 0, //分销
    internal: 1, //内购
    all: -1, //全部
  },
  activityList: [
    {
      label: '全员营销',
      value: 0,
    },
    {
      label: '员工内购',
      value: 1,
    },
  ],
  GAINTYPE: {
    FIRST_ENTER: 1, //首次进入内购页面
    DAILY_LOGIN: 2, //每日登录
    FRIEND_HELP: 3, //好友助力
    SYSTEM_GIVE: 4, //系统派送
  },
};
