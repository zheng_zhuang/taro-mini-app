const prefix = 'https://front-end-1302979015.file.myqcloud.com/images/';

const get = (tail) => {
  return prefix + tail;
};

export default {
  goods: {
    cardMinCount_png: get('c/images/goods/card-min-count.png'),
    rechargeCard_png: get('c/images/goods/recharge-card.png'),
    authentication_png: get('c/images/goods/authentication.png')
  },
  subPackages: {
    male_png: get('c/sub-packages/marketing-package/images/form-tool/male.png'),
    female_png: get('c/sub-packages/marketing-package/images/form-tool/female.png'),
    ic_large_share_png: get('c/sub-packages/distribution-package/images/ic-large-share.png'),
    ic_unbind_tip_png: get('c/sub-packages/distribution-package/images/ic-unbind-tip.png'),
    ic_detail_gray_png: get('sub-packages/distribution-package/images/ic-detail-gray.png'),
    ic_detail_png: get('c/sub-packages/distribution-package/images/ic-detail.png'),
    ic_record_gray_png: get('c/sub-packages/distribution-package/images/ic-record-gray.png'),
    ic_record_png: get('c/sub-packages/distribution-package/images/ic-record.png'),
    ic_rank_2_png: get('c/sub-packages/distribution-package/images/ic-rank-2.png'),
    ic_rank_1_png: get('c/sub-packages/distribution-package/images/ic-rank-1.png'),
    ic_rank_3_png: get('c/sub-packages/distribution-package/images/ic-rank-3.png'),

  },
  wxatCommon: {
    defaultImg_png: get('c/wxat-common/images/default-img.png'),
    iconBackLink_svg: get('c/wxat-common/components/icon-backLink.svg'),
    iconHomeLink_svg: get('c/wxat-common/components/icon-homeLink.svg')
  },
  images: {
    rightAngleGray_png: get('c/images/right-angle-gray.png'),
    rightIcon_png: get('c/images/right-icon.png'),
    iconMessage_png: get('c/images/icon-message.png'),
    cart_png: get('c/images/cart.png'),
    search_icon_png: get('c/images/search-icon.png'),
    close_png: get('c/images/close.png'),
    service_icon_svg: get('service-icon.svg'), // 这个图标不用前缀 全路径 https://front-end-1302979015.file.myqcloud.com/images/service-icon.svg
    ic_location_dark_png: get('c/images/ic-location-dark.png'),
    ic_del_png: get('c/images/ic-del.png'),
    icon_Lightning_png: get('c/images/icon-lightning.png'),
    line_close_png: get('c/images/line-close.png'),
    coupons_header_png: get('c/images//coupons-header.png'),
    coupons_bg_png: get('c/images/coupons-bg.png'),
    gift_enter_btn_png: get('gift-enter-btn.png'),
  },
  plugin: {
    minus_png: get('c/images/plugin/minus.png'),
    plus_png: get('c/images/plugin/plus.png'),
    cart_png: get('c/images/plugin/cart.png'),
    emptyCart_png: get('c/images/plugin/empty-cart.png')
  },
  order: {
    default_png: get('c/images/order/default.png'),
    parcel_png: get('c/images/order/parcel.png')
  },
  mine: {
    rectanglePath_png: get('c/images/mine/rectangle-path.png'),
    group_png: get('c/images/mine/group.png'),
    levelLogo_png: get('c/images/mine/level-logo.png')
  },
  group: {
    poster_png: get('c/images/group/poster.png'),
    share_png: get('c/images/group/share.png'),
  },
  tabbar: {
    homeOff_png: get('c/images/tabbar/home-off.png'),
  },
  shoppingCart: {
    nameSign_png: get('c/images/shopping-cart/name-sign.png'),
    group_png: get('c/images/shopping-cart/group.png')
  },
  sort: {
    normal_png: get('c/images/sort/normal.png'),
    desc_png: get('c/images/sort/desc.png'),
    asc_png: get('c/images/sort/asc.png'),
    sort_normal_png: get('images/sort/sort_normal.png'),
    sort_on_png: get('c/images/sort/sort_on.png'),
    sort_off_png: get('c/images/sort/sort_off.png'),
    sort_desc_png: get('c/images/sort/sort_desc.png'),
    sort_asc_png: get('c/images/sort/sort_asc.png')
  },
  comment: {
    PRAISE_png: get('c/images/comment/PRAISE.png'),
    MIDDLE_png: get('c/images/comment/MIDDLE.png'),
    BAD_png: get('c/images/comment/BAD.png')
  },
  iosPay:{
    step_tip_png: get('c/images/ios_pay_step_tip.png')
  }
}
