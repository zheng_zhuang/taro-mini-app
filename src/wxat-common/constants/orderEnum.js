export default {
  // 整家订单
  wholeFamily: {
    label: '整家订单',
    name: 'wholeFamily',
    simpleMapping: {
      1: '订单处理中',
      2: '货品准备中',
      3: '预约安装中',
      4: '待评价',
      5: '安装完成',
      6: '已完成',
    },
    status: {
      ORDER_PROCESSING: {
        label: '订单处理中',
        value: 1,
      },
      GOODS_PREPARATION: {
        label: '货品准备中',
        value: 2,
      },
      BOOKING_INSTALLATION: {
        label: '预约安装中',
        value: 3,
      },
      PENDING_EVALUATION: {
        label: '待评价',
        value: 4,
      },
      INSTALLATION_COMPLETED: {
        label: '安装完成',
        value: 5,
      },
      COMPLETED: {
        label: '已完成',
        value: 6,
      },
    },
  },
};
