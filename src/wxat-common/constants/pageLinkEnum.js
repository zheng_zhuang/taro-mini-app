const orderPackagePrefix = 'sub-packages/order-package/pages/';
const minePkgPrefix = 'sub-packages/mine-package/pages/';
const commonPkgPrefix = 'wxat-common/pages/';
const marketingPackagePrefix = 'sub-packages/marketing-package/pages/';
export default {
  orderPkg: {
    afterSale: {
      applyType: orderPackagePrefix + 'after-sale/apply-type/index',
      applyRefund: orderPackagePrefix + 'after-sale/apply-refund/index',
      refundOrder: orderPackagePrefix + 'after-sale/refund-order/index',
      refundDetail: orderPackagePrefix + 'after-sale/refund-detail/index',
      shippingCompany: orderPackagePrefix + 'after-sale/shipping-company-list/index',
    },
    orderDetail: orderPackagePrefix + 'order-details/index',
    payOrder: orderPackagePrefix + 'pay-order/index',
    paySuccess: orderPackagePrefix + 'pay-success/index',
    verification: orderPackagePrefix + 'verification/index',
    toveritication: orderPackagePrefix + 'to-verification/index',
  },
  common: {
    home: commonPkgPrefix + 'home/index',
    mine: commonPkgPrefix + 'mine/index',
    storeList: commonPkgPrefix + 'store-list/index',
    couponDetail: minePkgPrefix + 'coupon/detail/index',
    classify: commonPkgPrefix + 'classify/index',
    cart: commonPkgPrefix + 'shopping-cart/index'
  },
  marketingPkg: {
    giftCard: {
      mine: {
        index: marketingPackagePrefix + 'gift-card/mine/index',
        consumption: marketingPackagePrefix + 'gift-card/mine/consumption/index', // done
        sent: marketingPackagePrefix + 'gift-card/mine/sent/index', // done
        sending: marketingPackagePrefix + 'gift-card/mine/sending/index', // done
      },
      activate: marketingPackagePrefix + 'gift-card/actived/index', // done
      receive: marketingPackagePrefix + 'gift-card/receive/index', // done
    },
  },
};
