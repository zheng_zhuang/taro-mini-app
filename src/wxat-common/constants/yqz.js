export const defaultLocation = {
  address: '深圳市',
  cityCode: '4403',
  cityName: '深圳市',
  provinceName: "广东省",
  lat: 22.54845664,
  lng: 114.06455184,
}