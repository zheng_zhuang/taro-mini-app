export default {
  DELIVERY: {
    value: 0,
    label: '订单发货',
  },

  PAY_SUCCESS: {
    value: 1,
    label: '订单支付成功',
  },

  REFUND: {
    value: 2,
    label: '退款通知',
  },

  RESERVE_SUCCESS: {
    value: 3,
    label: '预约成功通知',
  },

  RESERVE_CANCEL: {
    value: 4,
    label: '预约取消通知',
  },

  GROUP_SUCCESS: {
    value: 5,
    label: '拼团成功通知',
  },

  GROUP_FAIL: {
    value: 6,
    label: '拼团失败通知',
  },

  BARGAIN_SUCCESS: {
    value: 7,
    label: '砍价成功通知',
  },

  BARGAIN_END: {
    value: 8,
    label: '砍价即将结束通知',
  },

  REDPACKETS_SUCCESS: {
    value: 9,
    label: '拆红包成功',
  },

  REDPACKETS_END: {
    value: 10,
    label: '拆红包即将结束',
  },

  CHARGE_SUCCESS: {
    value: 11,
    label: '充值成功',
  },

  BALANCE: {
    value: 12,
    label: '余额消费',
  },

  GIFT_CARD: {
    value: 13,
    label: '礼品卡购买成功',
  },

  BIND_GIFT_CARD: {
    value: 14,
    label: '礼品卡绑定成功',
  },

  RESERVE_SHOP: {
    value: 16,
    label: '预约到店提醒',
  },

  GRAPH_COMMENT_REPLY: {
    value: 19,
    label: '图文评论回复通知',
  },

  AMB_APPLY_RESULT: {
    value: 21,
    label: '审核结果通知',
  },

  LOTTERY: {
    value: 24,
    label: '开奖通知',
  },

  ORDER_SEND: {
    value: 39,
    label: '订单发货提醒',
  },

  REFUND_SUCCESS: {
    value: 40,
    label: '退款成功通知',
  },

  COUPONS_ARRIVE: {
    value: 41,
    label: '优惠券到账提醒',
  },

  COUPONS_OVERDUE: {
    value: 42,
    label: '优惠券到期提醒',
  },

  REFUND_FAIL: {
    value: 43,
    label: '退款失败通知',
  },

  TASK_CENTER: {
    value: 50,
    label: '任务奖励到账提醒',
  },

  subscribeList: [1, 2, 3, 6, 7, 8, 9, 11, 12, 13, 18, 19, 20, 21], // 所有数组
};
