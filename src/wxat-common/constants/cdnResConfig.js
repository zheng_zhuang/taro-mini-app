const prefix = 'https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/';
const prefix2 = "https://front-end-1302979015.file.myqcloud.com/images/c/";
const get = (tail) => {
  return prefix + tail;
};

export default {
  share_gift: {
    bg: get('share-gift/bg.png'),
    float: get('share-gift/float.png'),
    point: get('share-gift/point.png'),
    ticket: get('share-gift/ticket.png'),
  },
  lucky_dial: {
    background: get('lucky-dial/background.png'),
    dial_bg: get('lucky-dial/dial-bg.png'),
    dial_logo: get('lucky-dial/dial-logo.png'),
    dialog_header: get('lucky-dial/dialog-header.png'),
    share: get('lucky-dial/share.png'),
    thanks: get('lucky-dial/thanks.png'),
    lucky_dial: get('lucky-dial/luck-dial.jpg'),
  },
  pay_success: {
    apply1: get('pay-success/apply1.png'),
    apply2: get('pay-success/apply2.png'),
    lucky: get('pay-success/lucky.png'),
    pay_success: get('pay-success/pay-success.png'),
    ticket: get('pay-success/ticket.png'),
  },
  signIn: {
    calendar_bg: get('signIn/calendar-bg.png'),
    calendar: get('signIn/calendar.png'),
    coupon_bg: get('signIn/coupon-bg.png'),
    main_bg: get('signIn/main-bg.png'),
    rewards_integral: get('signIn/rewards-integral.png'),
    sign_dialog_bg: get('signIn/sign-dialog-bg.png'),
    sign_btn: get('signIn/sign-btn.png'),
    sign_btn_icon: get('signIn/sign-btn-icon.png'),
  },
  group: {
    // 限时抢购渐变背景图片集合
    bgList: {
      bg_v1: get('group/bg-v1.png'),
      bg_v2: get('group/bg-v2.png'),
      bg_v3: get('group/bg-v3.png'),
      bg_v4: get('group/bg-v4.png'),
      bg_v5: get('group/bg-v5.png'),
      bg_v6: get('group/bg-v6.png'),
    },
    bgGroupFullImg: get('group/bg-group-full.png'), // 参团人员已满对话弹框背景图片
  },
  coupon: {
    bg: get('coupon/coupon-bg.png'),
    switch: get('coupon/switch.png'),
    headBg: get('coupon/head-bg.png'),
    detailBg: get('coupon/detail-bg.png'),
    disableBg: get('coupon/coupon-disabled.png'),
    bgBlue: get('coupon/bg-blue.png'),
    bgGray: get('coupon/bg-gray.png'),
    bgGren: get('coupon/bg-gren.png'),
    full: get('coupon/full.png'),
    icon: get('coupon/icon.png'),
    none: get('coupon/none.png'),
    toVerification: get('coupon/to-verification.png'),
  },
  mine: {
    mine_black_bg: get('mine/black-bg.png'),
    default_avatar: get('mine/default-avatar.png'),
  },
  distribution: {
    invitationCodeBg: get('distribution/bg-invitation-code.png'),
  },
  redPacket: {
    shareBg: get('red-packet/bg-redpacket-share.png'),
    redPacketBg: get('red-packet/bg-of-redpack-popup-v2.png'),
    redPacketProgress: get('red-packet/red-packet-progress.png'),
  },
  integral: {
    integralBg: get('integral/integral-bg.png'),
  },
  internalBuy: {
    couponBg: get('internal-buy/coupn-bg.png'),
    posterBg: get('internal-buy/poster-bg.png'),
    internalBg: get('internal-buy/internal-bg.png'),
    jianpaiInternalBg: get('internal-buy/jp-internal-bg.png'),
  },
  goods: {
    vrLogo: get('goods/ic-vr-v2.png'),
    notGoods: get('goods/not-goods.png'),
    posterBgOne: get('poster/poster-bg-0.png'),
    posterBgTwo: get('poster/poster-bg-1.png'),
    posterBgThree: get('poster/poster-bg-2.png'),
    posterBgFour: get('poster/poster-bg-3.png'),
    posterMask: get('poster/poster-mask.png'),
    locationIcon: get('goods/location-icon.png')
  },
  wechatShare: {
    form: get('wechat-share/wechat-share-form.png'),
    coupon: get('wechat-share/wechat-share-coupon.png'),
  },
  gift: {
    new: get('gift/new-user-gift-bg.png'),
    old: get('gift/old-user-gift-bg.png'),
  },
  dialog: {
    dialogMsg: get('dialog/default-msg.png'),
    dialogShoppingGuide: get('dialog/default-sg.png'),
    dialogCollapse: get('dialog/default-collapse.png'),
  },
  taskCenter: {
    wxPoster: get('task-center/wx-poster.png'),
    wxShare: get('task-center/wx-share.png'),
    defaultPoster: get('task-center/default-poster.png'),
  },
  shoppingGuide: {
    auth: get('shopping-guide/auth.png'),
  },
  cutPrice: {
    close: get('cut-price/close.png'),
    cutMoney: get('cut-price/cut-money.png'),
    cutBg: get('cut-price/cut-price.png'),
  },
  auth: {
    withoutAuth: get('auth/no-auth.png'),
    wxIcon: get('auth/wx-icon.png')
  },
  drug: {
    empty: get('drug/prescription-empty.png'),
  },
  promotion_amb: {
    save: get('promotion-amb/icon-save.png'),
    share: get('promotion-amb/icon-share.png'),
    shareBg: get('promotion-amb/share-bg.png'),
    shareBg02: get('promotion-amb/share-bg02.png'),
    success: get('promotion-amb/success.png'),
  },
  micro_decorate: {
    VRLabel: get('micro-decorate/view.png'),
  },
  search: {
    viewList: prefix + 'search/view-list.png',
    viewPic: prefix + 'search/view-pic.png',
  },
  graph: {
    activeLike: get('graph/active-like.png'),
    activeLikeNew: get('graph/active-like-new.png'),
    comment: get('graph/comment.png'),
    commentNew: get('graph/comment-new.png'),
    like: get('graph/like.png'),
    like1: get('graph/like1.png'),
    likeNew: get('graph/like-new.png'),
    share: get('graph/share.png'),
    shareNew: get('graph/share-new.png'),
  },
  internal: {
    classfiyIcon: get('internal/classfiy-icon.png'),
    couponIcon: get('internal/coupon-icon.png'),
    poster: get('internal/poster.png'),
    share: get('internal/share.png'),
    shareIcon: get('internal/share-icon.png'),
    titleBg: get('internal/title-bg.png'),
    wechat: get('internal/wechat.png'),
  },
  common: {
    close: get('common/close.png'),
    error: get('common/error.png'),
    headDefault: get('common/head-default.png'),
    iconMap: get('common/icon-map.png'),
    iconPath: get('common/icon-path.png'),
    iconPhone: get('common/icon-phone.png'),
    noAvatar: get('common/noAvatar.png'),
    retailEmpty: get('common/retail-empty.png'),
    sortNormal: get('common/gray-sort.png')
  },
  decorate: {
    couponDefault: get('decorate/coupon-default.png'),
    couponsBg: get('decorate/coupons-bg.png'),
    couponsHeader: get('decorate/coupons-header.png'),
    downAngle: get('decorate/down-angle.png'),
    haggleIcon: get('decorate/haggle-icon.png'),
    location: get('decorate/location.png'),
    phone: get('decorate/phone.png'),
    rechargeCard: get('decorate/recharge-card.png'),
    scanCode: get('decorate/scan-code.png'),
    store: get('decorate/store.png'),
    timesCard: get('decorate/times-card.png'),
    customDialogClose: get('decorate/custom-dialog-close.png')
  },
  scan_code: {
    input: get('scan-code/input.png'),
    inputTip: get('scan-code/input-tip.png'),
    paySuccess: get('scan-code/pay-success.png'),
    scan: get('scan-code/scan.png'),
  },
  live: {
    view: prefix + 'micro-decorate/look.png',
    like: prefix + 'micro-decorate/like-btn.png',
    likeChecked: prefix + 'micro-decorate/like-checked.png',
    liveBg: prefix + 'auth/live-bg.png',
    livePosterBg: prefix + 'auth/live-poster-img.png',
    likeLight: prefix + 'auth/like.svg',
    likeLightChecked: prefix + 'auth/like-checked.png',
    titleBg: prefix + 'auth/title-bg.png',
    commentIcon: prefix + 'micro-decorate/comment-icon.png',
    likeIcon: prefix + 'micro-decorate/like-icon.png',
    likeCheckedIcon: prefix + 'micro-decorate/like-checked-icon.png'
  },
  marketing:{
    gift: prefix2 + 'wxat-common/components/home-activity-dialog/gift.png',
    giftBtn: prefix2 + 'wxat-common/components/home-activity-dialog/gift-btn.png',
    couponBg: prefix2 + 'wxat-common/components/home-activity-dialog/coupon-bg.png',
    couponItemBg: prefix2 + 'wxat-common/components/home-activity-dialog/coupon-item-bg.png',
    close: prefix2 + 'wxat-common/components/home-activity-dialog/close.png',
  }
};
