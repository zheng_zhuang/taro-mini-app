/**
 * @author lzw
 * @description 埋点配置信息
 */

function format(option) {
  const report = {};
  for (const key in this.report) {
    if (option.hasOwnProperty(key)) report[key] = option[key] || this.report[key];
  }
  return report;
}

export default {
  ITEM_DETAIL: {
    key: 'item-detail',
    format,
    report: {
      itemNo: '',
      activityType: '',
      activityId: '',
      distributorId: '',
      source: '',
    },
  },
};
