export default {
  TYPE: {
    fullReduced: {
      value: 0,
      label: '满减券',
    },
    freight: {
      value: 1,
      label: '运费券',
    },
    discount: {
      value: 2,
      label: '折扣券',
    },
  },
  SCOPE: {
    Universal: {
      value: 0,
      label: '通用券',
    },
    Online: {
      value: 1,
      label: '线上商城',
    },
    offline: {
      value: 2,
      label: '线下门店',
    },
  },
};
