import api from '@/wxat-common/api/index';
import { DOMAIN } from '../../../config/config.js'
// 判断是否正式域名切换易企住域名
// let H5_URL = 'https://wetripdev.ctlife.tv:50000/h5/#/'; // 测试环境易企住域名
let H5_URL = ' https://wetripdev.ctlife.tv/h5/#/'; // 测试环境易企住域名
// console.log('window.location.origin', window.location.origin);
// if (window.location.origin === 'https://mall.htrip.tv') {
//   H5_URL = 'https://wetriphotel.ctlife.tv:8000/h5/#/'; // 正式环境易企住域名
// }

// - https://mall.htrip.tv/yqz
// https://eksmall.ctlife.tv/yqz
const yqzEnv = process.env.REQUEST_ENV || 'EKS'
let YQZ_H5_URL = `${DOMAIN[yqzEnv]}/yqz/#/`
if (process.env.NODE_ENV === 'development') {
  YQZ_H5_URL = 'http://localhost:8080/yqz/#/'
}

export default {
  H5_URL,
  YQZ_H5_URL,
  // 酒店列表
  HOTELLISTPAGE: {
    url: 'pages/hotelList/hotelList',
    type: 'hotelListPage',
  },
  // 订单列表
  ORDERPAGE: {
    url: 'pages/order/order',
    type: 'orderPage',
  },
  // 企业管理
  ENTERPRISEPAGE: {
    url: 'pages/enterprise/index',
    type: 'enterprisePage',
  },
  // 员工注册
  REGISTERSTAFFPAGE: {
    url: 'pages/register/registerStaff',
    type: 'registerStaffPage',
  },
  // 注册页
  REGISTERPAGE: {
    url: 'pages/register/register',
    type: 'registerPage',
  },
  // 支付成功
  ORDERSTATUSSUCCESSPAGE: {
    url: 'pages/order/orderStatus',
    type: 'orderStatusSuccessPage',
  },
  // 支付失败
  ORDERSTATUSFAILSPAGE: {
    url: 'pages/order/orderStatus',
    type: 'orderStatusFailsPage',
  },
}
