export default {
  // 秒杀活动状态
  STATUS: {
    NOT_STARTED: {
      label: '未开始',
      value: 0,
    },
    ON_GOING: {
      label: '进行中',
      value: 1,
    },
    ENDED: {
      label: '已结束',
      value: 2,
    },
    DELETED: {
      label: '已删除',
      value: -1,
    },
  },
};
