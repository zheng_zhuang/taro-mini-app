export default {
  HORIZON: 'horizon',
  VERTICAL: 'vertical',
  ONE_ROW_ONE: 'oneRowOne',
  ONE_ROW_TWO: 'oneRowTwo',
  ONE_ROW_THREE: 'oneRowThree',
  classTypeEnum: {
    item: 'item',
    container: 'container',
  },
  getClassByShowType(classType, display) {
    const showType = new Map([
      [this.VERTICAL, this.VERTICAL + '-' + classType],
      [this.HORIZON, this.HORIZON + '-' + classType],
      [this.ONE_ROW_ONE, this.ONE_ROW_ONE + '-' + classType],
      [this.ONE_ROW_TWO, this.ONE_ROW_TWO + '-' + classType],
      [this.ONE_ROW_THREE, this.ONE_ROW_THREE + '-' + classType],
    ]);
    return showType.get(display);
  },
};
