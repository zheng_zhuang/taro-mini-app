import wxApi from '../utils/wxApi';
import api from '../api/index.js';

export default {
  GOODS: {
    canLike: false, // 是否有点赞功能
    canReplay: true, // 是否有商家回复
    noGrade: false, // 是否有等级评星
    imgCount: 3, // 可选评论图片数量
    getCommentList(data) {
      return wxApi.request({
        url: api.comment.query,
        loading: false,
        checkSession: true,
        data: data,
      });
    },
    getCommentCount(data) {
      return wxApi.request({
        url: api.comment.count,
        loading: false,
        checkSession: true,
        data: data,
      });
    },
    addComment(data) {
      return wxApi.request({
        url: api.comment.add,
        loading: false,
        checkSession: true,
        data: data,
      });
    },
  },
  ARTICLE: {
    canLike: true,
    canReplay: false,
    noGrade: true,
    imgCount: 1,
    getCommentList(data) {
      return new Promise((resolve, reject) => {
        wxApi
          .request({
            url: api.article.commentList, //TODO：文章评论的接口
            loading: true,
            checkSession: true,
            data: {
              status: 0, //未删除的状态
              articleId: data.itemNo,
              pageNo: data.pageNo,
              pageSize: data.pageSize,
              orderBy: data.orderBy,
            },
          })
          .then((res) => {
            res.data.map((item, index) => {
              item['avatarImgUrl'] = item.authorAvatar;
              item['userName'] = item.authorName;
              item['images'] = item.contentImageUrl ? [item.contentImageUrl] : [];
              return item;
            });
            resolve(res);
          })
          .catch((err) => {
            console.log('err', err);
            reject(err);
          });
      });
    },
    deleteComment(data) {
      return wxApi
        .request({
          url: api.article.deleteComment,
          loading: true,
          method: 'POST',
          data,
          header: {
            'content-type': 'application/x-www-form-urlencoded',
          },
        })
        .then((res) => {
          wxApi.showToast({
            title: '删除成功',
            icon: 'success',
            duration: 1000,
          });
          return res;
        });
    },
    addComment(data) {
      return wxApi.request({
        url: api.article.addComment,
        method: 'POST',
        loading: true,
        checkSession: true,
        data: {
          articleId: +data.orderNo,
          content: data.content,
          contentImageUrl: (data['imagePaths'] || [])[0],
        },
      });
    },
    changeLike(data) {
      return wxApi.request({
        url: api.article.changeLike,
        method: 'POST',
        loading: true,
        checkSession: true,
        data: {
          ...data,
          likeType: 2, //1文章点赞，2评论点赞
        },
      });
    },
  },
};
