export default {
  SELF_DELIVERY: {
    value: '0',
    label: '到店自提',
  },

  EXPRESS: {
    value: '1',
    label: '快递配送',
  },

  ALL: {
    value: '2',
    label: '全部',
  },

  CITY_DELIVERY: {
    value: '3',
    label: '同城配送',
  },
};
