export default {
  SERVER: {
    value: 0,
    label: '服务',
  },
  PRODUCT: {
    value: 1,
    label: '产品',
  },
  COMBINATIONITEM: {
    value: 2,
    label: '组合商品',
  },
  TIME_CARD: {
    value: 3,
    label: '次数卡',
  },
  CHARGE_CARD: {
    value: 4,
    label: '充值卡',
  },
  MANUAL_CHARGE: {
    value: 5,
    label: '手动充值',
  },
  GIFT_CARD: {
    value: 14,
    label: '礼品卡',
  },
  MEDICINE: {
    value: 8192,
    label: '药品',
  },
  ROOMS: {
    value: 18,
    label: '客房',
  },
  TICKETS: {
    value: 19,
    label: '票务',
  },
  MICRO_ORDER: {
    value: 20,
    label: '订购',
  },
  TISANE_ORDER: {
    value: 21,
    label: '煎药',
  },
  INTEGRAL: {
    value: 22,
    label: '积分商品',
  },
  GIFT_GOODS: {
    value: 24,
    label: '赠品',
  },
  SEC_KILL: {
    value: 25,
    label: '秒杀',
  },
  FREE_GOODS_ACTIVITY: {
    value: 26,
    label: '赠品专区',
  },
  GROUP: {
    value: 27,
    label: '拼团',
  },
  INTERNAL_BUY: {
    value: 28,
    label: '内购',
  },
  SLEEVE_SYSTEM: {
    value: 29,
    label: '套系',
  },
  WORKS: {
    value: 30,
    label: '作品',
  },
  CUSTOM: {
    value: 30,
    label: '客片',
  },
  SAMPLE: {
    value: 31,
    label: '样片',
  },
  ESTATE: {
    value: 32,
    label: '楼盘',
  },
  ESTATE_LAYOUT: {
    value: 33,
    label: '户型',
  },
  CARDPACK: {
    value: 35,
    label: '代金卡包',
  },
  CUSTOM_GOODS: {
    value: 36,
    label: '定制商品',
  },
  EQUITY_CARD: {
    value: 37,
    label: '权益卡',
  },
  VIRTUAL_GOODS: {
    value: 41,
    label: '虚拟商品',
  },
};
