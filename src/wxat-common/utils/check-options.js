import wxApi from './wxApi';
import api from '../api/index';
import store from '../../store';
import shareUtil from './share.js';
import { notifyStoreDecorateSwitch } from '@/wxat-common/x-login/decorate-configuration-store.ts';

/**
 * 将带短码的页面参数转换成对应存储的内容，并返回完整的页面参数
 * @param options {Object}
 * @return {Promise<null|Object>}
 */
async function checkOnLoadOptions(options) {
  const state = store.getState();
  //未登录，不提供启动参数
  if (!state.base.sessionId) {
    return;
  }

  // 热启动状态下 根据基础配置判断是否需要切换至分享者信息携带的门店 ('分享海报'热启动下)
  const useShareUserLocation =
    state.base.appInfo &&
    (state.base.appInfo.useShareUserLocation || state.base.appInfo.forbidChangeStoreForGuideShare);
  if (options && options._ref_storeId && useShareUserLocation) {
    await changeStore(options._ref_storeId);
  }

  //使用短码打开
  if (!!options.scene && !!shareUtil.MapperQrCode.getMapperKey(options.scene)) {
    let scene;
    // 冷启动能拿到分享内容
    scene = shareUtil.MapperQrCode.getMapperContent();
    // 热启动
    if (!scene) {
      scene = await shareUtil.MapperQrCode.apiMapperContent(options.scene);
      // 热启动状态下 根据基础配置判断是否需要切换至分享者信息携带的门店 ('分享海报'热启动下)
      if (!!scene.refStoreId && useShareUserLocation) {
        await changeStore(scene.refStoreId);
      }
    }

    // 使用短码对应存储的内容，替换原来scene供业务页面使用
    if (!!scene) {
      let result = '';
      for (const key in scene) result += (result ? '&' : '') + key + '=' + scene[key];
      options.scene = encodeURIComponent(result);
    }
  }

  return options;
}

// 切换当前门店
function changeStore(storeId) {
  return wxApi
    .request({
      url: api.store.choose_new + '?storeId=' + storeId,
      loading: true,
    })
    .then((res) => {
      const data = res.data;
      if (!!data) {
        notifyStoreDecorateSwitch(data);
      }
    });
}

export default {
  checkOnLoadOptions,
  changeStore,
};
