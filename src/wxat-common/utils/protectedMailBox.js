// @ts-check
import wxApi from '@/wxat-common/utils/wxApi';

/**
 * 受保护访问的 邮箱
 *
 * 用途: 设置指定页面的用户读完一次后，将立即销毁
 *
 * 发送
 *   ProtectedMailBox.send(
 *    'pages/home/index','优惠券赠送提醒',[{name:'xx',...}]
 *   );
 * 读取
 *   ProtectedMailBox.read('优惠券赠送提醒')
 *
 * 读取者
 *  如果是在 pages/home/index 页面，将能获取到结果，并且只能获取一次
 * 否则 得到 undefined
 *
 * H5 端存放在 sessionStorage 中
 */

/**
 * @param {string} addr
 */
function normalizeAddress (addr) {
  const idx = addr.lastIndexOf('?');
  if (idx !== -1) {
    return addr.slice(0, idx);
  }
  return addr;
}

const PERSIST_KEY = '_mainbox_';

export class Box {
  /** @type {Map<string, any>}  */
  store = new Map();
  /**
   * @param {string} address
   */
  constructor(address) {
    this.address = address;
    if (process.env.TARO_ENV === 'h5') {
      // 从 session storage 恢复
      const value = window.sessionStorage.getItem(`${PERSIST_KEY}_${address}`);
      if (value) {
        const res = JSON.parse(value);
        Object.keys(res).forEach((key) => {
          this.store.set(key, res[key]);
        });
      }
    }
  }

  /**
   * @param {string} key
   */
  get (key) {
    return this.store.get(key);
  }

  /**
   * @param {string} key
   * @param {any} value 必须是可以序列化的数据
   */
  set (key, value) {
    this.store.set(key, value);
    if (process.env.TARO_ENV === 'h5') {
      // session 持久化保存
      // 后续优化
      const obj = {};
      for (const [key, value] of this.store.entries()) {
        obj[key] = value;
      }
      window.sessionStorage.setItem(`${PERSIST_KEY}_${this.address}`, JSON.stringify(obj));
    }
  }

  /**
   *
   * @param {string} key
   */
  has (key) {
    return this.store.has(key);
  }

  /**
   *
   * @param {string} key
   */
  delete (key) {
    this.store.delete(key);
  }
}

export class Pool {
  /** @type {Map<string, Box>}  */
  pool = new Map();
  /**
   * @param {string} address
   */
  get (address) {
    const taked = this.pool.get(address);
    if (taked) {
      return taked;
    }
    // 创建一个新的箱子
    const box = new Box(address);
    this.pool.set(address, box);
    return box;
  }
}

const pool = new Pool();
export default {
  /**
   * @param {string} page 指定访问者所在页面
   * @param {string} subject 内容的名称
   * @param {any} content subject 对应的内容
   */
  send (page = 'pages/home/index', subject, content) {
    if (!subject) {
      return;
    }

    page = normalizeAddress(page);

    let pageMailBox = pool.get(page);

    // //覆写保护
    // if (box[page].hasOwnProperty(key)){
    //   return
    // }
    pageMailBox.set(subject, content);
  },

  /**
   * 试图访问 某人邮寄的物品
   * @param name 邮寄物品的名称
   */
  read (name) {
    let page = wxApi.$getCurrentPageRoute();

    if (page) {
      page = normalizeAddress(page);
      const pageMailBox = pool.get(page);
      if (pageMailBox.has(name)) {
        const value = pageMailBox.get(name);
        pageMailBox.delete(name);
        return value;
      }
    }
  },
};
