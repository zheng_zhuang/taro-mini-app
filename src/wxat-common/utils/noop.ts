/**
 * 一些空对象
 */
/* eslint-disable */

export const NOOP_OBJECT = {};
export const NOOP_ARRAY = [];
// @ts-ignore
export const NOOP = (...args: any[]): any => {};
export const RETURN_FALSE = () => false;
export const RETURN_TRUE = () => true;
export const IDENTIFY = <T = any>(a: T) => a;
