// @ts-check
import store from '../../store/index';

/**
 * @typedef {{
 *   titleColor: string | null,
 *   btnColor: string,
 *   btnColor1: string,
 *   bgColor: string,
 *   bgGradualChange: string,
 * }} TemplateStyle
 */

/**
 * titleColor： 顶部导航的颜色 (注： titleColor不支持rgb格式， 因为顶部导航不支持)
 * btnColor: 主要按钮的颜色
 * btnColor1： 如果有两个按钮， 次级按钮的颜色， 例如加入购物车， 立即购买
 * bgColor: 文字颜色是btnColor， 浅色的背景颜色， 例如优惠卷的背景颜色
 * bgGradualChange：渐变的背景颜色
 */
const config = {
  v1: {
    // 灵动橙
    global: {
      titleColor: null,
      btnColor: 'rgba(243,95,40,1)',
      btnColor1: 'rgba(255, 158, 122, 1)',
      bgColor: 'rgba(255, 205, 187, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(255,177,111,1) 0%,rgba(255,41,41,1) 100%)',
    },
  },

  v2: {
    // 稳重蓝
    global: {
      titleColor: '#2284FF',
      btnColor: 'rgba(34, 132, 255, 1)',
      btnColor1: 'rgba(116, 178, 255, 1)',
      bgColor: 'rgba(179, 213, 255, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(34,173,255,1) 0%,rgba(34,132,255,1) 100%)',
    },
  },

  v3: {
    // 鲜活绿
    global: {
      titleColor: '#26C17F',
      btnColor: 'rgba(38, 193, 127, 1)',
      btnColor1: 'rgba(108, 211, 167, 1)',
      bgColor: 'rgba(176, 230, 207, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(94,229,160,1) 0%,rgba(38,193,127,1) 100%)',
    },
  },

  v4: {
    // 活力红
    global: {
      titleColor: '#E53232',
      btnColor: 'rgba(229, 50, 50, 1)',
      btnColor1: 'rgba(255, 137, 137, 1)',
      bgColor: 'rgba(251, 214, 214, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(255,102,40,1) 0%,rgba(229,50,50,1) 100%)',
    },
  },

  v5: {
    // 魅力粉
    global: {
      titleColor: '#FF4E72',
      btnColor: 'rgba(255, 78, 114, 1)',
      btnColor1: 'rgba(255, 151, 172, 1)',
      bgColor: 'rgba(255, 200, 211, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(255,138,162,1) 0%,rgba(255,78,114,1) 100%)',
    },
  },

  v6: {
    // 商务黑
    global: {
      titleColor: '#33373E',
      btnColor: 'rgba(51, 55, 62, 1)',
      btnColor1: 'rgba(136, 147, 166, 1)',
      bgColor: 'rgba(221, 227, 236, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(97,104,134,1) 0%,rgba(50,52,60,1) 100%)',
    },
  },

  v7: {
    // 缤纷橙
    global: {
      titleColor: '#F35F28',
      btnColor: 'rgba(243, 95, 40, 1)',
      btnColor1: 'rgba(255, 158, 122, 1)',
      bgColor: 'rgba(255, 205, 187, 1)',
      bgGradualChange: 'linear-gradient(90deg,rgba(255,177,111,1) 0%,rgba(255,41,41,1) 100%)',
    },
  },
};

/**
 * JS颜色十六进制转换为rgb或rgba,返回的格式为 rgba（255，255，255，0.5）字符串
 * sHex为传入的十六进制的色值
 * alpha为rgba的透明度
 */
function colorRgba(sHex, alpha) {
  if (!sHex) {
    sHex = '#ffffff';
  }
  // 十六进制颜色值的正则表达式
  const reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
  /* 16进制颜色转为RGB格式 */
  let sColor = sHex.toLowerCase();
  if (sColor && reg.test(sColor)) {
    if (sColor.length === 4) {
      let sColorNew = '#';
      for (let i = 1; i < 4; i += 1) {
        sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
      }
      sColor = sColorNew;
    }
    // 处理六位的颜色值
    const sColorChange = [];
    for (let i = 1; i < 7; i += 2) {
      sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)));
    }

    return 'rgba(' + sColorChange.join(',') + ',' + alpha + ')';
  } else {
    return sColor;
  }
}

/**
 * 获取当前模板样式
 * @param {any} globalData
 */
function getTemplateStyle(globalData = null) {
  globalData = globalData || store.getState().globalData;
  if (globalData && globalData.themeConfig) {
    if (globalData.themeConfig.value === 'custom') {
      const template = globalData.themeConfig.template;
      if (template) {
        return {
          titleColor: template.navColor,
          btnColor: template.navColor,
          btnColor1: template.assistColor,
          // btnColor1: colorRgba(template.navColor, 0.6),
          bgColor: colorRgba(template.navColor, 0.1),
          bgGradualChange:
            'linear-gradient(90deg,' +
            (template.bgGradualChange1) +
            ' 0%,' +
            (template.bgGradualChange2) +
            ' 100%)',
          assistColor: template.assistColor,
          assistContentColor: template.assistContentColor,
          themeColor: template.themeColor,
          contentColor: template.contentColor,
          topBgColor: template.topBgColor,
          topContentColor: template.topContentColor,
        };
      }
    }
    const themeConfig = config[globalData.themeConfig.value];
    return themeConfig ? themeConfig.global : config.v1.global;
  } else if (globalData && globalData.industry === 'beauty') {
    return config.v5.global;
  } else {
    return config.v1.global;
  }
}

export default {
  getTemplateStyle,
  config,
};
