var filters = {
  subStr: function(str, start, num) {
    var startNum = start || 0
    var subNum = num || str.length
    return (str || '').substring(startNum, subNum)
  }
}

module.exports = {
  subStr: filters.subStr
}
