export default {
  /**
   *格式化时间文本
   *@param {String} format 格式化的文本格式
   *@param {Date} date 时间对象
   *@returns {String} 格式化后的文本
   *@example
   * format(new Date(), '现在是yyyy年MM月dd日 hh点mm分ss秒，星期w');
   */
  format: function (date, format = 'yyyy-MM-dd') {
    const o = {
      'M+': date.getMonth() + 1, // month
      'd+': date.getDate(), // day
      'h+': date.getHours(), // hour
      'm+': date.getMinutes(), // minute
      's+': date.getSeconds(), // second
      'q+': Math.floor((date.getMonth() + 3) / 3), // quarter
      S: date.getMilliseconds(), // millisecond
      w: '日一二三四五六'.charAt(date.getDay()),
    };

    format = format.replace(/y{4}/, date.getFullYear()).replace(/y{2}/, date.getFullYear().toString().substring(2));

    let k, reg;
    for (k in o) {
      reg = new RegExp(k);
      format = format.replace(reg, match);
    }

    function match(m) {
      return m.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length);
    }

    return format;
  },
  addDays(date, days) {
    return new Date(date.getTime() + 86400000 * days);
  },
  appendZero(time) {
    if (time < 10) {
      return '0' + time;
    }
    return time;
  },
  /**
   * 获取以今天为基准的第几天的日期，如果是前几天就传负值，如前7天传-7
   * @param day
   * @return {string}
   */
  getDay(day) {
    const today = new Date();
    const targetday_milliseconds = today.getTime() + 86400000 * day;
    today.setTime(targetday_milliseconds); // 注意，这行是关键代码
    const tYear = today.getFullYear();
    let tMonth = today.getMonth();
    let tDate = today.getDate();
    tMonth = doHandleMonth(tMonth + 1);
    tDate = doHandleMonth(tDate);
    return tYear + '-' + tMonth + '-' + tDate;
  },

  // 获取当前日期
  getNowDay() {
    const today = new Date();
    const targetday_milliseconds = today.getTime();
    today.setTime(targetday_milliseconds); // 注意，这行是关键代码
    const tYear = today.getFullYear();
    let tMonth = today.getMonth();
    let tDate = today.getDate();
    tMonth = doHandleMonth(tMonth + 1);
    tDate = doHandleMonth(tDate);
    return tYear + '-' + tMonth + '-' + tDate;
  },
  /**
   * 获取当年，当月，当月共多少天
   * @return {{month: number, year: number, monthDay: number}}
   */
  getCurYearDate() {
    const date = new Date();
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    const monthDay = new Date(year, month, 0).getDate();
    if (month < 10) {
      month = '0' + month;
    }
    return {
      year,
      month,
      monthDay,
    };
  },

  /**
   * 获取日期范围，如当前月，当天
   * @param tag today/month
   * @return {Object}
   */
  getDateRank(tag) {
    if (tag == "total") {
      return {};
    }
    let beginTime = "";
    let endTime = "";
    if (tag == "today") {
      beginTime = this.format(new Date(new Date().setHours(0, 0, 0, 0)), 'yyyy-MM-dd hh:mm:ss');
      endTime = this.format(new Date(new Date().setHours(23, 59, 59, 999)), 'yyyy-MM-dd hh:mm:ss');

    }
    if (tag == "yesterday") {
      console.log(new Date().setHours(0, 0, 0, 0))
      beginTime = this.format(new Date(new Date().setHours(0, 0, 0, 0) - 86400000), 'yyyy-MM-dd hh:mm:ss');
      endTime = this.format(new Date(new Date().setHours(0, 0, 0, 0) - 1), 'yyyy-MM-dd hh:mm:ss');

    }
    if (tag == "month") {
      let date = new Date();
      let currentMonth = date.getMonth();
      let nextMonth = ++currentMonth;
      let nextMonthFirstDay = new Date(date.getFullYear(), nextMonth, 1);
      let oneDay = 1000 * 60 * 60 * 24;
      beginTime = this.format(new Date(new Date(new Date().setDate(1)).setHours(0, 0, 0, 0)), 'yyyy-MM-dd hh:mm:ss');
      endTime = this.format(new Date(new Date(nextMonthFirstDay - oneDay).setHours(23, 59, 59, 999)), 'yyyy-MM-dd hh:mm:ss');
    }
    return {
      beginTime: beginTime,
      endTime: endTime
    }
  },

  /**
   * 获取日期范围，如当天，当周，当前月
   * @param tag today/week/month
   * @param initDate 可以为空，默认当天
   * @param day 第几天 前几天就传负值,默认1
   * @param format 时间格式，默认 'YYY'
   * @return {Object}
   */
  getDateRange(tag, initDate, day=0, format) {
    const one_day = 86400000;
    let startTime = null;
    let endTime = null;
    const today = initDate ? new Date(initDate) : new Date();
    let targetday_milliseconds = today.getTime() + one_day * day;
    today.setTime(targetday_milliseconds); // 注意，这行是关键代码;
    if (tag == 'day') {
      startTime = today;
      endTime = today;
    }
    if (tag == 'week') {
      const inDay = today.getDay();// 返回0-6,0表示周日
      if (day == -1) {
        startTime = new Date(targetday_milliseconds - (6 * one_day));
        endTime = new Date(targetday_milliseconds);
      } else {
        startTime = new Date(targetday_milliseconds - ((inDay == 0?0:inDay) - 1) * (one_day));
        endTime = new Date(targetday_milliseconds + (7 - inDay) * (one_day));
      }
    }
    if (tag == 'month') {
      startTime = new Date(today.setDate(1))
      endTime = new Date(today.getFullYear(), today.getMonth() + 1, 0)
    }

    return [
      getFormat(startTime),
      getFormat(endTime)
    ];
  }

};
function doHandleMonth(month) {
  let m = month;
  if (month.toString().length === 1) {
    m = '0' + month;
  }
  return m;
}
function getFormat(date, format = 'yyyy-MM-dd') {
  const tYear = date.getFullYear();
  let tMonth = date.getMonth();
  let tDate = date.getDate();
  tMonth = doHandleMonth(tMonth + 1);
  tDate = doHandleMonth(tDate);
  return tYear + '-' + tMonth + '-' + tDate;
}
