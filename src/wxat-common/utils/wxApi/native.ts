/**
 * 原生 API
 */
import { NativeAPI } from '@/wk-taro-platform';

export default () => NativeAPI;
