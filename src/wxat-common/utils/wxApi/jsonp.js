import JsonP from 'jsonp';

/**
 * Format params into querying string.
 * @param {{}}
 * @return {string[]}
 */
function formatParams(queryName, value) {
  queryName = queryName.replace(/=/g, '');
  var result = [];

  switch (value.constructor) {
    case String:
    case Number:
    case Boolean:
      result.push(encodeURIComponent(queryName) + '=' + encodeURIComponent(value));
      break;

    case Array:
      value.forEach(function (item) {
        result = result.concat(formatParams(queryName + '[]=', item));
      });
      break;

    case Object:
      Object.keys(value).forEach(function (key) {
        var item = value[key];
        result = result.concat(formatParams(queryName + '[' + key + ']', item));
      });
      break;
  }

  return result;
}
/**
 * Flat querys.
 *
 * @param {any} array
 * @returns
 */
function flatten(array) {
  var querys = [];
  array.forEach(function (item) {
    if (typeof item === 'string') {
      querys.push(item);
    } else {
      querys = querys.concat(flatten(item));
    }
  });
  return querys;
}
/**
 * Convert params to querying str.
 * @param {{}}
 * @return {string[]}
 */
function contactParams(params) {
  var queryStrs = [];
  Object.keys(params).forEach(function (queryName) {
    queryStrs = queryStrs.concat(formatParams(queryName, params[queryName]));
  });
  return flatten(queryStrs).join('&');
}

export default function jsonp(url, params, timeout) {
  params = params || {};
  timeout = timeout || null;

  return new Promise(function (resolve, reject) {
    if (typeof url !== 'string') {
      throw new Error('[Vue.jsonp] Type of param "url" is not string.');
    }

    var queryStr = contactParams(params);
    let src = url + (/\?/.test(url) ? '&' : '?') + queryStr;
    JsonP(
      src,
      {
        param: 'callback',
      },
      function (err, res) {
        if (res.status == 0) {
          resolve(res);
        } else {
          reject(err);
        }
      }
    );
  });
}
