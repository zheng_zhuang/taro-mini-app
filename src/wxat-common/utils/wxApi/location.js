export default function wxGetLocation() {
  // console.log('获取定位')
  var promise = new Promise(function (resolve, reject) {
    var map = new BMap.Map('allmap');
    var geolocation = new BMap.Geolocation();
    geolocation.getCurrentPosition(function (r) {
      if (this.getStatus() == BMAP_STATUS_SUCCESS) {
        var mk = new BMap.Marker(r.point);
        map.addOverlay(mk);
        map.panTo(r.point);
        resolve(r);
      } else {
        console.log('failed' + this.getStatus());
        reject(r);
      }
    });
  });
  return promise;
}
