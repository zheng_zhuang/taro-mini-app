export default {
  entries(obj, callback) {
    for (const ikey in obj) {
      callback(ikey, obj[ikey]);
    }
  },
  obj2KeyValueArray(obj) {
    const ary = [];
    for (const key in obj) {
      ary.push([key, obj[key]]);
    }
    return ary;
  },
};
