import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';

function decamelize(str, separator) {
  separator = typeof separator === 'undefined' ? '_' : separator;
  return str
    .replace(/([a-z\d])([A-Z])/g, '$1' + separator + '$2')
    .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separator + '$2')
    .toLowerCase();
}
async function checkHotelOrder(params = {}, config = {}) {
  if (!params || Object.keys(params).length === 0) {
    return wxApi.showToast({
        title: "参数缺失",
        icon: "none",
      });
  }
  const apiUrl = api.hotelOrder.hotelCheck;
  try {
    const { data = {} } = await wxApi.request({ url: apiUrl, method: 'POST', loading: true, data: params, });
    const result = {}
    for (const key in data) {
      result[decamelize(key)] = data[key]
    }
    result.price_items && result.price_items.forEach(item => {
      for (const key in item) {
        item[decamelize(key)] = item[key]
        delete item[key]
      }
    })
    result.priceItems = result.price_items
    delete result.price_items
    return result
  } catch (error) {
    return error
  }
}

export default checkHotelOrder