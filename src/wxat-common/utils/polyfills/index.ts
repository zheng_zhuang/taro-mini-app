if (Promise.prototype.finally == null) {
  Promise.prototype.finally = function (callback: () => void) {
    const P = this.constructor;
    return this.then(
      (value) => P.resolve(callback()).then(() => value),
      (reason) =>
        P.resolve(callback()).then(() => {
          throw reason;
        })
    );
  };
}
