import Taro from '@tarojs/taro';
import constants from "../constants";
import api from "../api";
import login from "../x-login";
import wxApi from './wxApi';
import store from "../../store";
import util from './util';
import tplmsg from './tplmsg';
import money from './money';
import report from "../../sdks/buried/report";

const PAY_CHANNEL = constants.order.payChannel;
const WX_PAY_CHANNEL = '微信支付';
const ACCOUNT_PAY_CHANNEL = '余额支付';
let payFlag = false; // 支付状态是否支付中

export default {
  /**
   * 点击付款
   * @param orderInfo 订单信息 必传
   *  @param orderInfo.itemList 订单商品信息列表，必传，[{},{},...]
   *  每个商品的属性需要包括如下参数：
   *    @param item.itemNo 必传
   *    @param item.itemCount 不必传 默认是1
   *    @param item.itemSkuId 不必传
   *  @param orderInfo.userAddressId 订单地址id 不必传
   *  @param orderInfo.scheduledTime 预约时间，服务相关产品需要传该字段
   *  @param orderInfo.subscriberName 预约人姓名，服务相关产品需要传该字段
   *  @param orderInfo.subscriberMobile 预约人电话，服务相关产品需要传该字段
   *  @param orderInfo.serviceCardNo 次数卡抵扣
   * @param payChannel 支付渠道 不必传，如果不传，会弹出弹窗选择
   * @param options 回调函数，以及extConfig配置
   */
  handleBuyNow (orderInfo, payChannel = null, options, url = api.order.unifiedOrderNew) {
    const itemDTOList = orderInfo.itemDTOList;
    if (itemDTOList && itemDTOList.length && itemDTOList[0].type === constants.card.type.charge) {
      this._checkPayByPayChannel(orderInfo, PAY_CHANNEL.WX_PAY, options, url);
    } else if (payChannel) {
      this._checkPayByPayChannel(orderInfo, payChannel, options, url);
    } else {
      this.getAccountBalance().then((balance) => {
        let accountPayChannel = ACCOUNT_PAY_CHANNEL;
        if (balance) {
          accountPayChannel += '(￥' + balance + ')';
        }
        wxApi.showActionSheet({
          itemList: [WX_PAY_CHANNEL, accountPayChannel],
          success: (res) => {
            const payChannel = res.tapIndex === 0 ? PAY_CHANNEL.WX_PAY : PAY_CHANNEL.ACCOUNT;
            this._checkPayByPayChannel(orderInfo, payChannel, options, url);
          },
        });
      });
    }
  },

  _checkPayByPayChannel (orderInfo, payChannel, options, url) {
    // fixme 不做===判断，String和Number都支持
    if (payChannel === PAY_CHANNEL.WX_PAY) {
      this._toPay(orderInfo, payChannel, options, url);
    } else if (payChannel === PAY_CHANNEL.ACCOUNT) {
      if (options.extConfig && options.extConfig.hideAccountModal) {
        this._toPay(orderInfo, payChannel, options, url);
      } else {
        wxApi.showModal({
          title: '支付确认',
          content: '确定使用余额支付',
          success: (res) => {
            if (res.confirm) {
              this._toPay(orderInfo, payChannel, options, url);
            } else {
              options.cancel();
            }
          },
        });
      }
    } else if (payChannel === PAY_CHANNEL.CARD_COUPON) {
      wxApi.showModal({
        title: '支付确认',
        content: '确定使用次数卡抵扣?',
        success: (res) => {
          if (res.confirm) {
            this._toPay(orderInfo, payChannel, options, url);
          }
        },
      });
    } else if (payChannel === PAY_CHANNEL.NO_NEED_PAY) {
      this._toPay(orderInfo, payChannel, options, url);
    }
  },

  _toPay (orderInfo, payChannel, options, url) {
    const currentStore = store.getState().base.currentStore;
    const params = Object.assign(
      {
        wxAppId: store.getState().globalData.maAppId,
        appId: store.getState().globalData.appId,
        storeId: store.getState().base.currentStore.id,
        storeName: currentStore.abbreviation || currentStore.name,
        storeTel: store.getState().base.currentStore.tel,
        payChannel: payChannel,
      },

      orderInfo
    );

    const formatParams = util.formatParams(params);
    options.originParams = params;
    this.submitPay(url, formatParams, payChannel, options);
  },

  /**
   * 普通订单点击付款
   * @param orderNo 订单id ，必传
   * @param payChannel 支付渠道 不必传，如果不传，会弹出弹窗选择
   * @param options {Ojbect} success,fail生成订单回调，wxPaySuccess,wxPayFail微信支付回调
   */
  payByOrder (orderNo, payChannel = null, options) {
    if (payChannel) {
      this._checkPayByOrderChannel(orderNo, payChannel, options);
    } else {
      this.getAccountBalance().then((balance) => {
        let accountPayChannel = ACCOUNT_PAY_CHANNEL;
        if (balance) {
          accountPayChannel += '(￥' + balance + ')';
        }
        wxApi.showActionSheet({
          itemList: [WX_PAY_CHANNEL, accountPayChannel],
          success: (res) => {
            const payChannel = res.tapIndex === 0 ? PAY_CHANNEL.WX_PAY : PAY_CHANNEL.ACCOUNT;
            this._checkPayByOrderChannel(orderNo, payChannel, options);
          },
        });
      });
    }
  },

  // 获取余额
  getAccountBalance () {
    return wxApi
      .request({
        url: api.userInfo.balance,
        loading: true,
        data: {},
      })
      .then((res) => {
        return res.data.balance;
      })
      .then((balance) => {
        if (balance || balance === 0) {
          return money.fen2Yuan(balance);
        }
        return null;
      });
  },

  _checkPayByOrderChannel (orderNo, payChannel, options) {
    // fixme 不做===判断，String和Number都支持
    if (payChannel === PAY_CHANNEL.WX_PAY) {
      this._toPayOrder(orderNo, payChannel, options);
    } else if (payChannel === PAY_CHANNEL.ACCOUNT) {
      wxApi.showModal({
        title: '支付确认',
        content: '确定使用余额支付',
        success: (res) => {
          if (res.confirm) {
            this._toPayOrder(orderNo, payChannel, options);
          } else {
            options.cancel();
          }
        },
      });
    } else if (payChannel === PAY_CHANNEL.CARD_COUPON) {
      wxApi.showModal({
        title: '支付确认',
        content: '确定使用次数卡抵扣?',
        success: (res) => {
          if (res.confirm) {
            this._toPayOrder(orderNo, payChannel, options);
          }
        },
      });
    } else if (payChannel === PAY_CHANNEL.NO_NEED_PAY) {
      this._toPayOrder(orderNo, payChannel, options);
    }
  },

  _toPayOrder (orderNo, payChannel, options) {
    const params = {
      orderNo: orderNo,
      payChannel: payChannel,
    };

    this.submitPay(api.order.payOrder, params, payChannel, options);
  },

  /**
   * 付款
   * @param url 提交的url
   * @param params 提交参数
   * @param payChannel 支付渠道 不必传，如果不传，会弹出弹窗选择
   * @param options {Object} success,fail生成订单回调，wxPaySuccess,wxPayFail微信支付回调
   */
  submitPay (url, params, payChannel, options) {
    if(payFlag) return
    payFlag = true;
    let orderNo = '';
    const originParams = options.originParams;
    login.login()
      .then(async (res) => {
        if (payChannel === PAY_CHANNEL.WX_PAY && options.payInfo) {
          // 有订单信息并且是微信支付，无需再次生成订单，直接支付
          const payInfo = options.payInfo.data;
          const payParams = {
            timestamp: payInfo.timeStamp + '',
            nonceStr: payInfo.nonceStr,
            package: payInfo.package || 'prepay_id=' + payInfo.prepayId,
            signType: 'RSA',
            paySign: payInfo.paySign,
            appId: payInfo.appId
          }
          const payFn = process.env.WX_OA === 'true' ? wx.chooseWXPay : wxApi.requestPayment;
          payFn({
            ...payParams,

            success(e) {
              report.clickPay({
                result: true,
                fail_reason: '',
                amount: 0,
                pay_type: payChannel,
              });

              if (options && options.wxPaySuccess) {
                if (options.extConfig && options.extConfig.immediateCallback) {
                  options.wxPaySuccess(e, {
                    orderNo,
                  });
                } else {
                  // 微信支付订单状态有延迟，延迟1秒加载
                  setTimeout(() => {
                    options.wxPaySuccess(e, {
                      orderNo,
                    });
                  }, 1000);
                }
              }
            },
            fail(e) {
              report.clickPay({
                result: false,
                fail_reason: e.errMsg,
                amount: 0,
                pay_type: payChannel,
              });

              if (options && options.wxPayFail) {
                options.wxPayFail(e);
              }
            },
          });

          return;
        }
        const distributorId = store.getState().globalData.distributorId;
        if (distributorId) {
          params.distributorId = distributorId;
          if (originParams) originParams.distributorId = distributorId;
        }
        params.payTradeType = this.returnPayTradeType()
        if (originParams) originParams.payTradeType = this.returnPayTradeType()
        if (options && options.beforeSubmit) {
          options.beforeSubmit();
        }
        const newParams = Object.assign({}, originParams);
        newParams.orderRequestDTOS = [
          {
            expressType: newParams.expressType,
            userAddressId: newParams.userAddressId,
            itemDTOList: newParams.itemDTOList
          }
        ];
        delete newParams.itemDTOList;
        if (params.orderNo) {
          // url + '?orderNo=' + params.orderNo
          url = `${url}?orderNo=${params.orderNo}&payTradeType=${this.returnPayTradeType()}&payChannel=${payChannel}`
        } else {
          await wxApi.request({
            url: api.order.cal_price_new,
            quite: true,
            loading: false,
            method: 'POST',
            data: newParams,
          })
        }
        return wxApi.request({
            url: url,
            method: 'post',
            loading: {
              // TODO 安卓上会有这个两个弹窗，先注释掉这个
              title: (options && options.msg) || '正在拉取支付',
              // title: false,
            },
            data: params.orderNo ? '' : newParams,
          })
          .then(
            async (res) => {
              payFlag = false;
              if (options && options.success) {
                // console.log(options, res);
                console.log('----------正在拉取支付', res.data);
                this.handlePromotionTask(res);
                await options.success(res, payChannel);
              }
              // 到店支付，后续不会有回调了，在这里发送模板消息
              if (payChannel == PAY_CHANNEL.NO_NEED_PAY && params.formId !== undefined && !!params.formId) {
                tplmsg.send(params.formId);
              }
              if (!params.orderNo && originParams && originParams.itemDTOList) {
                orderNo = res.data.orderNo;
                const reportParams = {
                  itemInfos: originParams.itemDTOList,
                  orderNo: res.data.orderNo,
                  result: 1,
                };

                report.uniferOrder(reportParams);
              }
              return res;
            },
            (res) => {
              payFlag = false;
              if (options && options.fail) {
                options.fail(res);
              }
              if (!params.orderNo && originParams && originParams.itemDTOList) {
                const reportParams = {
                  itemInfos: originParams.itemDTOList,
                  orderNo: null,
                  result: 0,
                };

                report.uniferOrder(reportParams);
              }
              throw new Error(res);
            }
          );
      })
      .then((res) => {
        if (payChannel === PAY_CHANNEL.WX_PAY) {
          const payData = res.data
          const payFn = process.env.WX_OA === 'true' ? WeixinJSBridge.invoke : wxApi.requestPayment;

          const payParams = {
            timeStamp: payData.timeStamp.toString(),
            nonceStr: payData.nonceStr, // 支付签名随机串，不长于 32 位
            package: 'prepay_id=' + payData.prepayId,
            signType: payData.signType,
            paySign: payData.paySign, // 支付签名
            appId: payData.appId
          }
          if (process.env.WX_OA === 'true') {
            WeixinJSBridge.invoke(
              'getBrandWCPayRequest', payParams, function (res) {
              console.log("支付成功结果=>", res);
              if (res.err_msg == "get_brand_wcpay_request:ok") {
                // 使用以上方式判断前端返回,微信团队郑重提示：
                //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
                if (originParams && originParams.itemDTOList) {
                  const reportParams = {
                    itemInfos: originParams.itemDTOList,
                    orderNo: orderNo,
                    result: 1,
                  };

                }
                if (options && options.wxPaySuccess) {
                  // 微信支付，发送模板消息
                  if (params.formId !== undefined && !!params.formId) {
                    tplmsg.send(params.formId);
                  }
                  if (options.extConfig && options.extConfig.immediateCallback) {
                    options.wxPaySuccess(e, {
                      orderNo,
                    });
                  } else {
                    // 微信支付订单状态有延迟，延迟1秒加载
                    setTimeout(() => {
                      options.wxPaySuccess(e, {
                        orderNo,
                      });
                    }, 1000);
                  }
                }
              }
              // 支付失败
              if (res.err_msg == "get_brand_wcpay_request:fail") {
                if (originParams && originParams.itemDTOList) {
                  const reportParams = {
                    itemInfos: originParams.itemDTOList,
                    orderNo: orderNo,
                    result: 0,
                  };
                }

                if (options && options.wxPayFail) {
                  options.wxPayFail(res);
                }
              }
              // 支付过程中用户取消
              if (res.err_msg == "get_brand_wcpay_request:cancel") {
                if (originParams && originParams.itemDTOList) {
                  const reportParams = {
                    itemInfos: originParams.itemDTOList,
                    orderNo: orderNo,
                    result: 0,
                  };

                }

                if (options && options.wxPayFail) {
                  options.wxPayFail(res);
                }
              }
            });
          } else {
            wxApi.requestPayment({
              ...payParams,
              success (e) {
                if (originParams && originParams.itemDTOList) {
                  const reportParams = {
                    itemInfos: originParams.itemDTOList,
                    orderNo: orderNo,
                    result: 1,
                  };

                }
                if (options && options.wxPaySuccess) {
                  // 微信支付，发送模板消息
                  if (params.formId !== undefined && !!params.formId) {
                    tplmsg.send(params.formId);
                  }
                  if (options.extConfig && options.extConfig.immediateCallback) {
                    options.wxPaySuccess(e, {
                      orderNo,
                    });
                  } else {
                    // 微信支付订单状态有延迟，延迟1秒加载
                    setTimeout(() => {
                      options.wxPaySuccess(e, {
                        orderNo,
                      });
                    }, 1000);
                  }
                }
              },
              fail (e) {
                if (originParams && originParams.itemDTOList) {
                  const reportParams = {
                    itemInfos: originParams.itemDTOList,
                    orderNo: orderNo,
                    result: 0,
                  };

                }

                if (options && options.wxPayFail) {
                  options.wxPayFail(e);
                }
              },
              cancel(e){
                if (originParams && originParams.itemDTOList) {
                  const reportParams = {
                    itemInfos: originParams.itemDTOList,
                    orderNo: orderNo,
                    result: 0,
                  };

                }

                if (options && options.wxPayFail) {
                  options.wxPayFail(e);
                }
              },
              complete (e) {
                // 打印支付结果，微信认为的订单过期不会置为成功或失败，只能用complete打印
                if (e.errMsg.indexOf('requestPayment:fail:') > -1) {
                  if (originParams && originParams.itemDTOList) {
                    const reportParams = {
                      itemInfos: originParams.itemDTOList,
                      orderNo: orderNo,
                      result: 0,
                    };

                    report.payResult(reportParams);
                  }
                  wxApi.showModal({
                    showCancel: false,
                    content: e.errMsg.replace('requestPayment:fail:', ''),
                    title: '支付失败',
                  });
                }
              },
            });
          }
        } else if (payChannel === PAY_CHANNEL.ACCOUNT) {
          if (res.data.paySign === 'false') {
            wxApi.showToast({
              title: '支付失败，余额不足。',
              icon: 'none',
            });

            if (originParams && originParams.itemDTOList) {
              const reportParams = {
                itemInfos: originParams.itemDTOList,
                orderNo: orderNo,
                result: 0,
              };

              report.payResult(reportParams);
            }
            report.clickPay({
              result: false,
              fail_reason: '支付失败，余额不足。',
              amount: 0,
              pay_type: payChannel,
            });

            return;
          }
          wxApi.showToast({
            title: '支付成功',
            icon: 'success',
          });

          if (originParams && originParams.itemDTOList) {
            const reportParams = {
              itemInfos: originParams.itemDTOList,
              orderNo: orderNo,
              result: 1,
            };

            report.payResult(reportParams);
          }
          report.clickPay({
            result: true,
            fail_reason: '',
            amount: 0,
            pay_type: payChannel,
          });

          // 账户余额支付，发送模板消息
          if (params.formId !== undefined && !!params.formId) {
            tplmsg.send(params.formId);
          }
        }
      })
      .catch((error) => {
        console.log('pay error: ' + JSON.stringify(error));
      });
  },

  // 推广任务处理
  handlePromotionTask (res) {
    // 推广任务 && 携带分享Id && 邀请新用户 && 非会员 && 首次发起砍价
    if (res.success) {
      const shareId = store.getState().base.shareId;
      if (
        store.getState().base.promotion &&
        shareId &&
        store.getState().base.targetType &&
        !store.getState().base.vip &&
        store.getState().base.isFirst
      ) {
        const { orderNo, groupNo } = res.data;
        // 4为秒杀 3为拼团
        if (store.getState().base.taskType == 4) {
          this.addInviteRecord(shareId, orderNo, orderNo);
        } else {
          this.addInviteRecord(shareId, groupNo, orderNo);
        }
        store.getState().base.isFirst = false;
      }
    }
  },
  /**
   * 任务中心邀请上报
   */
  addInviteRecord (shareId, referId, orderNo) {
    const data = {
      shareId,
      referId,
      orderNo,
    };

    wxApi.request({
      url: api.taskCenter.addInviteRecord,
      data,
      method: 'POST',
    });
  },

  /**
   * 代客下单订单点击付款
   * @param orderNo 订单id ，必传
   * @param payChannel 支付渠道 必传
   * @param options {Ojbect} success,fail生成订单回调，wxPaySuccess,wxPayFail微信支付回调
   */
  payByValetOrder (orderNo, payChannel, url = api.order.payValetOrder, options) {
    if (payChannel === PAY_CHANNEL.WX_PAY) {
      this._toPayValetOrder(orderNo, payChannel, url, options);
    } else if (payChannel === PAY_CHANNEL.ACCOUNT) {
      wxApi.showModal({
        title: '支付确认',
        content: '确定使用余额支付',
        success: (res) => {
          if (res.confirm) {
            this._toPayValetOrder(orderNo, payChannel, url, options);
          } else {
            options.cancel();
          }
        },
      });
    }
  },

  _toPayValetOrder (orderNo, payChannel, url, options) {
    const params = {
      valetOrderNo: orderNo,
      payChannel: payChannel,
    };

    this.submitPay(url, params, payChannel, options);
  },

  returnPayTradeType () {
    // 1:微信小程序支付 3:微信公众号支付
    let payTradeType = 1
    if (process.env.WX_OA === 'true') {
      payTradeType = 3
    }
    return payTradeType
  },

};
