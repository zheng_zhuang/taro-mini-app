import { ActivityItem } from '@/types/activetype';
import fecha from 'fecha';
import date from '@/wxat-common/utils/date';

export const format = 'MM月dd日 hh:mm';
export const formatSimple = 'MM/dd';

export function remoteDateParse(date: string) {
  return fecha.parse(date, 'YYYY-MM-DD HH:mm:ss')!;
}

export function activityDateFormat(items: ActivityItem[]) {
  const start = new Date(Math.min(...items.map((i) => remoteDateParse(i.startTime).getTime())));
  const end = new Date(Math.max(...items.map((i) => remoteDateParse(i.endTime).getTime())));

  return `${date.format(start, format)} 至 ${date.format(end, format)}`;
}

// 用户报名流程, 活动详情, 活动时间;
export function userActivityDateFormat(items: ActivityItem[]) {
  const start = new Date(Math.min(...items.map((i) => remoteDateParse(i.activityStartTime).getTime())));
  const end = new Date(Math.max(...items.map((i) => remoteDateParse(i.activityEndTime).getTime())));

  return `${date.format(start, format)} 至 ${date.format(end, format)}`;
}

export function activityDateFormatSimple(items: ActivityItem[]) {
  const start = new Date(Math.min(...items.map((i) => remoteDateParse(i.activityStartTime).getTime())));
  const end = new Date(Math.max(...items.map((i) => remoteDateParse(i.activityEndTime).getTime())));

  const startStr = date.format(start, formatSimple);
  const endStr = date.format(end, formatSimple);
  if (startStr === endStr) {
    return startStr;
  }
  return `${startStr} - ${endStr}`;
}

export function activityItemDateFormat(item: { startTime: string; endTime: string }) {
  const start = remoteDateParse(item.startTime);
  const end = remoteDateParse(item.endTime);

  return `${date.format(start, format)} 至 ${date.format(end, format)}`;
}
