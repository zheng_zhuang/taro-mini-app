export default {
  drawCircleImage(canvasContext, x, y, radius, imageSrc) {
    canvasContext.save(); // 先保存状态 已便于画完圆再用
    //先画个圆
    canvasContext.arc(x, y, radius, 0, Math.PI * 2, false);
    canvasContext.clip(); //画了圆 再剪切  原始画布中剪切任意形状和尺寸。一旦剪切了某个区域，则所有之后的绘图都会被限制在被剪切的区域内
    canvasContext.drawImage(imageSrc, x - radius, y - radius, 2 * radius, 2 * radius); // 推进去图片
    canvasContext.restore(); //恢复之前保存的绘图上下文 恢复之前保存的绘图上下文即状态 可以继续绘制
  },
  drawImage(canvasContext, x, y, width, height, imageSrc) {
    canvasContext.drawImage(imageSrc, x, y, width, height);
  },
  drawText(canvasContext, fillText, x, y, config) {
    const { textAlign, fillStyle, fontSize, maxWidth, totalWidth, lineHeight = 15, oneLineTop, rowLength = 2 } = config || {};

    canvasContext.setTextAlign(textAlign || 'start'); // 文字居左
    canvasContext.setFillStyle(fillStyle || '#000000'); // 文字颜色：黑色
    canvasContext.setFontSize(fontSize || 12); // 文字字号：12px

    if (totalWidth) {
      const textWidth = canvasContext.measureText(fillText).width;
      x = (totalWidth - textWidth) / 2 + x;
    }
    if (maxWidth) {
      const chr = fillText.split(''); //这个方法是将一个字符串分割成字符串数组
      let temp = '';
      let row = [];

      for (let i = 0; i < chr.length; i++) {
        if (canvasContext.measureText(temp).width < maxWidth) {
          temp += chr[i];
        } else {
          i--; //这里添加了a-- 是为了防止字符丢失，效果图中有对比
          row.push(temp);
          temp = '';
        }
      }
      row.push(temp); //如果数组长度大于2 则截取前两个
      if (row.length > rowLength) {
        const rowCut = row.slice(0, rowLength);
        const rowPart = rowCut[rowLength - 1];
        let test = '';
        const empty = [];
        for (let i = 0; i < rowPart.length; i++) {
          if (canvasContext.measureText(test).width < maxWidth - 30) {
            test += rowPart[i];
          } else {
            break;
          }
        }
        empty.push(test);
        const group = empty[0] + '...'; //这里只显示两行，超出的用...表示
        rowCut.splice(rowLength - 1, 1, group);
        row = rowCut;
      }

      if (oneLineTop && row.length === 1) {
        y = oneLineTop; // 一行文本调整居中
      }

      for (let i = 0; i < row.length; i++) {
        canvasContext.fillText(row[i], x, y + i * lineHeight, maxWidth);
      }
    } else {
      if (config.bold) {
        canvasContext.fillText(fillText, x, y - 0.2, maxWidth);
        canvasContext.fillText(fillText, x - 0.2, y, maxWidth);
      }
      canvasContext.fillText(fillText, x, y, maxWidth);
      if (config.bold) {
        canvasContext.fillText(fillText, x, y + 0.2, maxWidth);
        canvasContext.fillText(fillText, x + 0.2, y, maxWidth);
      }
    }
  },
  drawFillLine(canvasContext, pointList, fillColor) {
    if (pointList && pointList.length) {
      canvasContext.beginPath();
      pointList.forEach((point, index) => {
        if (index === 0) {
          canvasContext.moveTo(point.x, point.y);
        } else {
          canvasContext.lineTo(point.x, point.y);
        }
      });
      canvasContext.closePath();
      canvasContext.setFillStyle(fillColor || '#000');
      canvasContext.fill();
    }
  },
  drawFillRect(canvasContext, x, y, width, height, fillColor) {
    canvasContext.setFillStyle(fillColor || '#000');
    canvasContext.fillRect(x, y, width, height);
  },
  roundRect(ctx, x, y, w, h, r, c = '#fff') {
    ctx.save();
    if (w < 2 * r) {
      r = w / 2;
    }
    if (h < 2 * r) {
      r = h / 2;
    }

    ctx.beginPath();
    ctx.fillStyle = c;

    ctx.arc(x + r, y + r, r, Math.PI, Math.PI * 1.5);
    ctx.moveTo(x + r, y);
    ctx.lineTo(x + w - r, y);
    ctx.lineTo(x + w, y + r);

    ctx.arc(x + w - r, y + r, r, Math.PI * 1.5, Math.PI * 2);
    ctx.lineTo(x + w, y + h - r);
    ctx.lineTo(x + w - r, y + h);

    ctx.arc(x + w - r, y + h - r, r, 0, Math.PI * 0.5);
    ctx.lineTo(x + r, y + h);
    ctx.lineTo(x, y + h - r);

    ctx.arc(x + r, y + h - r, r, Math.PI * 0.5, Math.PI);
    ctx.lineTo(x, y + r);
    ctx.lineTo(x + r, y);

    ctx.fill();
    ctx.closePath();
    ctx.restore();
  },
  drawStrokeLine(canvasContext, pointList, strokeColor, lineWidth) {
    if (pointList && pointList.length) {
      canvasContext.beginPath();
      pointList.forEach((point, index) => {
        if (index === 0) {
          canvasContext.moveTo(point.x, point.y);
        } else {
          canvasContext.lineTo(point.x, point.y);
        }
      });
      canvasContext.closePath();
      canvasContext.lineWidth = lineWidth || 1; //设置线条宽度
      canvasContext.strokeStyle = strokeColor || '#000'; //设置线条颜色
      canvasContext.stroke();
    }
  }
};
