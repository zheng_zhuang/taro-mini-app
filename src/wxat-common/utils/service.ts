const FreeEnum = {
  '免费':0,
  '不展示':1,
  '挂账':2,
  '到付':3
}

export const getFreeLabel=(key)=>{
  let label = '免费'
  Object.keys(FreeEnum).forEach((item)=>{
    if(key == FreeEnum[item]){
      if(key == 1){
        label = ''
      }else{
        label = item
      }
    }
  })
  return label
}
