import {updateGlobalDataAction} from '@/redux/global-data';
import store from "../../store";
import api from "../api";
import wxApi from '../utils/wxApi';
import {getParams, authGuide} from '../utils/platform/official-account.h5';
import {setLocalKey} from '../x-login/utils';
import {$getRouter} from '@/wk-taro-platform'
import Taro from "@tarojs/taro";

export default {
  getPageConfig(dataSource) {
    const {ext} = store.getState();

    const params = {
      appId: ext.appId,
      // sellerTemplateId: ext.sellerTemplateId,
      ...(typeof dataSource === 'string' ? {index: dataSource} : {pId: dataSource.pId}),
    };

    return new Promise((resolve, reject) => {
      if (!ext.appId || !ext.sellerTemplateId) {
        wxApi.getExtConfig().then((res) => {
          params.appId = res.extConfig.appId;
          params.sellerTemplateId = res.extConfig.sellerTemplateId;
          wxApi
            .request({url: api.getPageConfig, loading: true, data: params})
            .then((resp) => {
              resolve(JSON.parse(resp.data));
            })
            .catch((error) => {
              reject(JSON.stringify(error));
            });
        });
        return;
      }
      wxApi
        .request({url: api.getPageConfig, loading: true, data: params})
        .then((res) => {
          resolve(JSON.parse(res.data));
        })
        .catch((error) => {
          reject(JSON.stringify(error));
        });
    });
  },

  /**
   * 跳转分类页（开启快速注册后，无法获取）。 开启快速注册后，开发工具中无法跳转
   * @param succes
   * @param fail
   */
  jumpToClassify(succes, fail) {
    let url = '';
    let tabbarItem = this.findPagePathItem('wxat-common/pages/classify/index');
    if (!tabbarItem) {
      tabbarItem = this.findPagePathItem('pages/quick-buy/index');
    }
    if (tabbarItem) {
      url = tabbarItem.pagePath
    } else {
      url = '/wxat-common/pages/home/index';
    }
    if (url) {
      wxApi.$navigateTo({
        url: url,
        success: () => {
          succes && succes();
        },
        fail: () => {
          fail && fail();
        },
      });
    }
  },

  /**
   * 跳转分类页（开启快速注册后，无法获取）。 开启快速注册后，开发工具中无法跳转
   * @param succes
   * @param fail
   */
  jumpToCart(succes, fail) {
    let url = '';
    // 购物车或者快速购买在tabbar时，首先跳转到tabbar
    let tabbarItem = this.findPagePathItem('pages/tabbar-shopping-cart/index');
    if (!tabbarItem) {
      tabbarItem = this.findPagePathItem('pages/quick-buy/index');
    }
    if (tabbarItem) {
      url = tabbarItem.pagePath;
    } else {
      url = '/wxat-common/pages/shopping-cart/index';
    }
    if (url) {
      wxApi.$navigateTo({
        url: url,
        data: {
          title: '购物车'
        },
        success: () => {
          succes && succes();
        },
        fail: () => {
          fail && fail();
        },
      });
    }
  },
  getNavColor: function (pagePath) {
    return this.findPagePathItem(pagePath);
  },

  setNavColor(oldPath, newPath) {
    const params = this.getNavColor(newPath) || this.getNavColor(oldPath);
    if (params) {
      wxApi.setNavigationBarColor({
        frontColor: params.navFrontColor || '#000000',
        backgroundColor: params.navBackgroundColor || '#ffffff',
      });
    }
  },

  findPagePathItem(pagePath) {
    const {tabbars} = store.getState().globalData;
    let x = null;
    (!!tabbars && tabbars.list ? tabbars.list : []).forEach((item) => {
      if (item.realPath.indexOf(pagePath) !== -1 || pagePath.indexOf(item.realPath) !== -1) {
        x = item;
      }
    });
    return x;
  },

  // 判断tabbar可能被链接的页面是否在tabbars配置了，没有配置的需要隐藏页面的tabbar，并取消padding;
  judgePageInTabbars(pagePath) {
    return !!this.findPagePathItem(pagePath);
  },

  findPagePathItem(pagePath) {
    let x = null;
    // 兼容二维码跳转，调整三目结果为app.json定义tabbar
    (!!store.getState().globalData.tabbars && store.getState().globalData.tabbars.list
        ? store.getState().globalData.tabbars.list
        : [
          {
            pagePath: '/wxat-common/pages/home/index',
          },
          {
            pagePath: '/wxat-common/pages/classify/index',
          },
          {
            pagePath: '/wxat-common/pages/tabbar-custom/index',
          },
          {
            pagePath: '/wxat-common/pages/mine/index',
          },
        ]
    ).forEach((item) => {
      if (item.pagePath.indexOf(pagePath) !== -1 || pagePath.indexOf(item.pagePath) !== -1) {
        x = item;
      }
    });
    return x;
  },


  // 查询是否开启送礼
  queryIsGiveGift() {
    const {ext} = store.getState();
    wxApi.request({
      url: api.giveGift.switch,
      method: 'GET',
      loading: true,
      data: {
        per_chooseAppId: ext.appId
      }
    }).then(res => {
      store.dispatch(updateGlobalDataAction({isShowGiveGift: !!res.data}));
    });
  },

  // 获取订阅消息模板Id
  getTemplateIdList() {
    const pages = Taro.getCurrentPages();
    if (process.env.TARO_ENV == 'h5') {
      if (window.location.href.indexOf("separate-package") != -1) {
        return;
      }
    } else {
      if (pages[0].route.indexOf("separate-package") != -1) {
        return;
      }
    }
    const {ext} = store.getState();
    wxApi.request({
      url: api.messageTemplate.getTemplateIdList,
      method: 'GET',
      loading: true,
      data: {
        appId: ext.appId
      },
      quite: true
    }).then(res => {
      if (!res.data || !res.data.length) return
      const templateIds = {};
      res.data.forEach(item => {
        templateIds[item.id] = item.wxSubMsgId;
      });
      store.dispatch(updateGlobalDataAction({messageTemplateId: templateIds}));
    });
  },

  getHomeConfig(params) {
    const paramsLocal = getParams();
    const keys = setLocalKey(paramsLocal.appid);
    const LOGIN_INFO_KEY = keys.LOGIN_INFO_KEY;
    // const keys = setLocalKey(params.appid);
    const {data = {}} = wxApi.getStorageSync(LOGIN_INFO_KEY);

    let currentStoreId = data.storeVO.id;
    wxApi
      .request({
        url: api.store.choose + '?storeId=' + currentStoreId,
      })
      .then(() => {
      });
    const router = $getRouter().params

    //判断是否 住中或房间 二维码进来 去二维码中携带的门店id （已存在缓存中）
    const scanStoreId = wxApi.getStorageSync('scan_storeId')
    if (scanStoreId && router?.id && (router?.tp == 1 || router?.tp == 2)) {
      currentStoreId = scanStoreId
      wxApi.removeStorage({key: 'scan_storeId'})
    }
    return wxApi.request({
      url: api.getUserConfig,
      loading: true,
      data: {
        storeId: currentStoreId,
        // sellerId: params.sellerId,
        appId: params.appId,
        sellerTemplateId: params.sellerTemplateId,
      },
    });
  },
  getUserInfo() {
    return wxApi.request({
      url: api.wxoa.getUserInfo,
      loading: false,
      data: {},
    });
  },
};
