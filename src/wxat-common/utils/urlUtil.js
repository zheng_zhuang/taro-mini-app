export default {
  getAllQueryParamsStr(url) {
    if (url && url.indexOf('?') !== -1) {
      return url.split('?')[1];
    }
    return '';
  },
};
