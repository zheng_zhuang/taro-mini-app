// https://github.com/developit/mitt
import mitt from '../lib/mitt/index.js';
export default mitt();
