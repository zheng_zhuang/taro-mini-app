import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { notifyStoreDecorateRefresh } from '@/wxat-common/x-login/decorate-configuration-store';
import { updateGlobalDataAction } from '@/redux/global-data';
import {updateBaseAction} from '@/redux/base'
import storeData from '@/store'
const state = storeData.getState()

function scanFn(scanId, tp) {
  // 扫码进入h5
  let url = ''
  if (tp == 3) {
    // 普通商品详情、虚拟商品详情、组合商品详情h5二维码
    url = 'getWXOADetailParams'
  } else if (tp == 4) {
    // 自定义页h5二维码
    url = 'getWXOACustomParams'
  }
  return new Promise((resolve, reject) => {
    wxApi.request({
      url: api.wxoa[url],
      isLoginRequest: true,
      method: 'GET',
      data: {
        id: scanId,
      },
      quite:true
    }).then(res => {
      resolve(res)
    }).catch(err => {
      console.log(err)
    })
  })
}

function changeStore(storeId) {
  wxApi.request({
    url: api.store.choose_new + '?storeId=' + storeId,
    loading: true,
  })
  .then((res) => {
    const store = res.data;
    if (store) {
        storeData.dispatch(updateBaseAction({
          currentStore:store
        }))
      notifyStoreDecorateRefresh(store);
    }
  });
}
async function changeStoreAsync(storeId) {
  return new Promise((resovle,reject)=>{
    wxApi.request({
      url: api.store.choose_new + '?storeId=' + storeId,
      loading: true,
    })
    .then(async (res) => {
      const store = res.data;
      if (store) {
          storeData.dispatch(updateBaseAction({
            currentStore:store
          }))
        // await notifyStoreDecorateRefresh(store);
      }
      resovle()
    });
  })
}
function scanRoomCodeFunc(options){
  let value:any = null
  let customIndex:any = null
  return new Promise((resolve,reject)=>{
    let isScan = false
    let redirectToUrl = ''
    let title = ''
    if(options.id && (options.tp == 1 || options.tp == 2)){
        wxApi.request({
          url: api.queryQrCodeScenes,
          isLoginRequest: true,
          method: 'GET',
          data: {
            id: options.id
          },
        }).then(({ data }) => {
          const dataObj:any = {};
          data.split('&').forEach(item => {
            const key = item.split('=')[0];
            const value = item.split('=')[1];
            dataObj[key] = value;
          })
          value = dataObj
          return wxApi.request({
            url: api.selectQrCodeNumber,
            isLoginRequest: true,
            method: 'POST',
            data: {
              id: options.id,
              hotelId: dataObj.pId,
              qrCodeNumber: options.qrcodeNo,
            },
          })
        }).then(async({ data: { roomCode, roomNumber, qrCodeTypeId, qrCodeTypeName,storeId,roomToPage,xielvToPage } }) => {
          storeData.dispatch(updateGlobalDataAction({
            roomCode: options.tp == 2 ? roomNumber : roomCode,
            roomCodeTypeId: qrCodeTypeId, // 房间号类型id
            roomCodeTypeName: qrCodeTypeName // 房间号类型名称
          }));
          if(storeId){
            await changeStoreAsync(storeId)
            //把二维码所携带的门店Id存在在本地 ，初始加载用这个storeID ,使用 wxAppProxy.js getHomeConfig
            wxApi.setStorageSync('scan_storeId',storeId)
          }
          const page = options.tp == 1 ?  roomToPage : xielvToPage
          let componentId:any = null

          if (!roomCode && options.tp == 1) {
            isScan = true
          }
          try{
            const tabbarList =  storeData.getState().globalData?.tabbars?.list
            if(page.includes('moveFile-package/pages/shopping-cart/index')){
              componentId = 'tabbar-shopping-cart'
            }else if(page.includes('wxat-common/pages/custom/index') || page.includes('wxat-common/pages/tabbar-custom/index') || page.includes('wxat-common/pages/tabbar-custom1/index') || page.includes('wxat-common/pages/tabbar-custom2/index')){
              if(page.includes('wxat-common/pages/tabbar-custom/index')){
                if(tabbarList[1].realPath.includes('pages/custom/index?route')){
                  componentId = 'custom'
                }else{
                  redirectToUrl = '/wxat-common/pages/home/index'
                  title='首页'
                }
              }else if(page.includes('wxat-common/pages/tabbar-custom1/index')){
                if(tabbarList[2].realPath.includes('pages/custom/index?route')){
                  componentId = 'custom'
                }else{
                  redirectToUrl = '/wxat-common/pages/home/index'
                  title='首页'
                }
              }else if(page.includes('wxat-common/pages/tabbar-custom2/index')){
                if(tabbarList[3].realPath.includes('pages/custom/index?route')){
                  componentId = 'custom'
                }else{
                  redirectToUrl = '/wxat-common/pages/home/index'
                  title='首页'
                }
              }else{
                customIndex = 'wxat-common/pages/custom/index'
                componentId = 'custom'
              }
            }else if(page.includes('wxat-common/pages/tabbar-order-list/index')){
              // componentId = 'tabbar-order-list'
              tabbarList.forEach((item)=>{
                if(item.realPath === page){
                  redirectToUrl = item.pagePath
                  title = '订单'
                }
              })
              if(!redirectToUrl){
                redirectToUrl = '/wxat-common/pages/home/index'
                title = '首页'
              }
            }else if(page.includes('wxat-common/pages/classify/index')){
              tabbarList.forEach((item)=>{

                if(item.realPath === page){
                  redirectToUrl = item.pagePath
                  title = '分类'
                }
              })
              if(!redirectToUrl){
                redirectToUrl = '/wxat-common/pages/home/index'
                title='首页'
              }
            }
          }catch(e){
            console.log('扫码跳转错误',e)
            resolve({isScan:isScan})
          }
          resolve({isScan:isScan,customCodeData:value,componentId:componentId,redirectToUrl:redirectToUrl,title:title,customIndex:customIndex})
        }).catch(err => {
          reject(null)
        });

    }else{
      resolve(null)
    }
  })
}

export default{
  scanFn,
  changeStore,
  scanRoomCodeFunc
}
