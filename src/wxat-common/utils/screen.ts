import wxApi from '@/wxat-common/utils/wxApi';
import Taro from '@tarojs/taro';

let info: Taro.getSystemInfoSync.Result;

// 惰性获取
function getInfo() {
  if (info) {
    return info;
  }

  info = wxApi.getSystemInfoSync();

  // 开发环境在模拟器模拟 safearea
  if (process.env.NODE_ENV === 'development' && info.model === 'iPhone X') {
    info.safeArea = {
      bottom: info.screenHeight - 10,
      height: info.screenHeight - 44 - 10,
      left: 0,
      right: info.screenWidth,
      top: 44,
      width: info.screenWidth,
    };
  }

  return info;
}

export default {
  get safeAreaBottom() {
    const { screenHeight, safeArea = { bottom: screenHeight } } = getInfo();
    /**
     * 修复部分安卓机型下safeAreaBottom值错误
     * 已知安卓端最大值：30
     * 已知IOS端最大值：34
     * 已知桌面端最大值：0
     * 如果大于最大值+偏差值(5)，判定为safeArea错误，直接返回0
     */
    const maximum = (this.isAndroid ? 30 : this.isWindows ? 0 : 34) + 5;
    const safeAreaBottom = screenHeight - safeArea.bottom;

    return safeAreaBottom > maximum ? 0 : safeAreaBottom;
  },
  /**
   * tabbar 高度
   */
  get tabbarHeight() {
    // TODO: 测试安卓手机
    return (this.isIOS ? 48 : 54) + this.safeAreaBottom;
  },
  /**
   * 是否为全面屏
   * TODO: 测试
   */
  get isFullScreenPhone() {
    return this.safeAreaBottom !== 0;
  },
  /**
   * ios 设备
   */
  get isIOS() {
    const { system = {} } = getInfo();
    return system && system.startsWith('iOS');
  },
  /**
   * Android 设备
   */
  get isAndroid() {
    const { system = {} } = getInfo();
    return system && system.startsWith('Android');
  },
  /**
   * Windows 设备
   */
  get isWindows() {
    const { system = {} } = getInfo();
    return system && system.startsWith('Windows');
  },
  /**
   * 状态栏高度
   */
  get statusBarHeight() {
    const { statusBarHeight = 0 } = getInfo();
    return statusBarHeight;
  },
  /**
   * 导航栏高度
   */
  get navigationBarHeight() {
    // 状态栏高度，iPhone和Android的高度都是32 上下距离 iPhone为6 Android为8
    return this.isIOS ? 44 : 48;
  },
  /**
   * 导航栏和状态栏整体高度
   */
  get totalNavigationBarHeight() {
    return this.statusBarHeight + this.navigationBarHeight;
  },
  /**
   * 获取胶囊的宽度
   */
  get navigationCapsuleWidth() {
    //胶囊的宽度+距右边的距离
    return 94;
  },
};
