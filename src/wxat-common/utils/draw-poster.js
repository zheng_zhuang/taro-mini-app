import Taro from '@tarojs/taro';
import wxApi from './wxApi';
import store from '@/store';
import helper from './canvas-helper';

async function drawPoster(ctx, qrcode, cover, title) {
  const state = store.getState();
  let { nickName, avatarImgUrl } = state.base.loginInfo;
  nickName = nickName || state.base.userInfo.nickname;
  avatarImgUrl = avatarImgUrl || state.base.userInfo.avatarImgUrl || '';

  // 白色背景
  ctx.setFillStyle('#fff');
  ctx.fillRect(0, 0, 550, 806);

  if (/^https?:\/\//i.test(qrcode)) {
    const { path } = await wxApi.getImageInfo({ src: qrcode });
    qrcode = path;
  }

  if (/^https?:\/\//i.test(avatarImgUrl)) {
    const { path } = await wxApi.getImageInfo({ src: avatarImgUrl });
    avatarImgUrl = path;
  }
  // 圆形头像
  if (avatarImgUrl) {
    helper.drawCircleImage(ctx, 50 + 90 / 2, 30 + 90 / 2, 45, avatarImgUrl);
  }

  if (/^https?:\/\//i.test(cover)) {
    const { path, width, height } = await wxApi.getImageInfo({ src: cover });
    let _height = width * (4 / 5);
    let _width = width;
    if (_height > height) {
      _height = height;
      _width = _height * (5 / 4);
    }
    cover = path;

    ctx.drawImage(cover, (width - _width) / 2, (height - _height) / 2, _width, _height, 50, 150, 450, 360);
  }

  // 微信昵称
  ctx.setFontSize(28);
  ctx.setTextAlign('left'); // 文字居左
  ctx.setFillStyle('#333'); // 文字颜色：黑色
  ctx.fillText(nickName || '', 156, 55 + 28);

  // 海报标题
  ctx.setFontSize(28);
  ctx.setTextAlign('left');
  ctx.setFillStyle('#333');
  let ellipsisTitle = title;

  const _ctx = wxApi.createCanvasContext('myCanvas');
  _ctx.setFontSize(28);
  let metrics = _ctx.measureText(ellipsisTitle);
  while (true) {
    if (metrics.width < 450) break;
    ellipsisTitle = ellipsisTitle.slice(0, ellipsisTitle.length - (/\.\.\.$/.test(ellipsisTitle) ? 4 : 3)) + '...';
    metrics = _ctx.measureText(ellipsisTitle);
  }

  ctx.fillText(ellipsisTitle, 50, 540 + 28);

  // 二维码
  if (qrcode) {
    ctx.drawImage(qrcode, 350, 630, 146, 146);
  }

  // 品牌信息
  let { appLogoPath, appName } = state.base.appInfo || {};
  if (appName.length > 9) {
    appName = appName.slice(0, 8) + '...';
  }
  if (appLogoPath) {
    const { path } = await wxApi.getImageInfo({ src: appLogoPath });
    helper.drawCircleImage(ctx, 50 + 60 / 2, 716 + 60 / 2, 30, path);
  }
  if (appName) {
    ctx.setTextAlign('left');
    ctx.setFontSize(24);
    ctx.setFillStyle('#666666');

    const left = appLogoPath ? 118 : 50;
    ctx.fillText(appName, left, 729 + 24);
  }
}

export default drawPoster;
