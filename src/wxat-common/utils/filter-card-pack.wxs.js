const formatStatus = function (pocketStatus) {
  var text = '';
  // 0 '未开始', 1 '进行中', 2 '已售罄', 3 '已结束', 4 '已删除'
  switch (pocketStatus) {
    case 0:
      text = '活动未开始';
      break;
    case 1:
      text = '立即购买';
      break;
    case 2:
      text = '已售罄';
      break;
    case 3:
      text = '活动已结束';
      break;
    case 4:
      text = '活动已结束';
      break;
    default:
      text = '已售罄';
  }

  return text;
};

export default {
  formatStatus: formatStatus,
};
