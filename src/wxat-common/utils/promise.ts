// eslint-disable-next-line import/prefer-default-export
export function delay(time: number) {
  return new Promise(function (res) {
    setTimeout(res, time);
  });
}
