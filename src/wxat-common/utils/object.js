import cloneDeep from 'lodash/cloneDeep';
import objEntries from './obj-entries.js';

export default {
  /**
   * 深拷贝对象
   */
  clone (obj) {
    return cloneDeep(obj);
  },
  /**
   * 将object转换成url参数格式
   * @param {*} obj
   */
  toUrlParams (obj) {
    return objEntries
      .obj2KeyValueArray(obj)
      .map((item) => {
        let val = '';
        if (typeof item[1] === 'object') {
          val = JSON.stringify(item[1]);
        } else if (item[1] != null) {
          val = item[1];
        }
        return `${item[0]}=${val}`;
      })
      .join('&');
  },
  /**
   * 判断是否空对象
   * @param {*} obj
   */
  isEmptyObject (obj) {
    return obj == null || Object.keys(obj).length === 0;
  },
  /**
 * 根据对象生成getter/setter对象
 * @param {} obj
 * @example
 *
 * 每个对象有4个原属性：
 * obj: {
 *   val: '值',
 *   storage: 'storageKey' 配置后，对象的get、set将同步到wx storage
 *   get: () => {} 每次调用state.obj, 都会取该函数返回值，如果未定义，则默认返回obj.val || obj
 *   set: (val) => {} 每次调用state.obj = xx，都会调用该函数，如果定义了get但未定义set，表示该变量只读，不可写，每当变量改变，会向全局广播stateChange事件
 * }
 *
 * 可以简写为：obj: 'val'，等同于： obj: { val: 'val' }
 */
  toComputed (obj) {
    const originObj = this.clone(obj);
    for (const key of Object.keys(originObj)) {
      Object.defineProperty(obj, key, {
        get () {
          const item = originObj[key];
          if (item && typeof item.get === 'function') {
            return item.get.call(originObj);
          }
          if (item && item.storage) {
            return item.val || Taro.getStorageSync(item.storage);
          }
          return item && (item.val || item);
        },
        set (val) {
          const item = originObj[key];
          const emit = () => {
            event.emit('stateChange', {
              key,
              val,
            });
          };
          if (item && item.storage) {
            item.val = val;
            if (typeof val === 'object') {
              const _val = JSON.stringify(val);
              Taro.setStorageSync(item.storage, _val);
            } else {
              Taro.setStorageSync(item.storage, val);
            }
          }
          if (item && typeof item.set === 'function') {
            item.set.call(originObj, val);
            emit();
            return;
          }
          if (item && typeof item.get === 'function') {
            throw `无法修改变量${key}，因为该变量没有定义setter`;
          }
          if (item && item.val) {
            item.val = val;
            emit();
            return;
          }
          originObj[key] = val;
          emit();
        },
      });

      // 初始化，将storage的值同步到内存
      const item = originObj[key];
      if (item && item.storage) {
        const val = Taro.getStorageSync(key);
        if (val !== '') {
          try {
            item.val = JSON.parse(val);
          } catch (e) {
            item.val = val;
          }
        }
      }
    }
    return obj;
  },
  /**
   * 将wx异步函数包装成promise函数
   * @param {*} fn
   */
  wxPromisify (fn) {
    return function (obj = {}) {
      return new Promise((resolve, reject) => {
        obj.success = resolve;
        obj.fail = reject;
        fn(obj);
      });
    };
  },
};
