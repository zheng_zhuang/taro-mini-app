import wxApi from '@/wxat-common/utils/wxApi'; // import state from "../../state/index.js";
import store from '../../store';
import Taro from '@tarojs/taro';

// XXX 临时用一下 @raoqi
import {$navigateTo} from '@/wxat-common/utils/wxApi/navigate'; // import state from "../../state/index.js";
// console.log('---------------', _navigate)

export default {
  /**
   * 检测账号是否登录授权
   * @param forceAuth 是否强制授权,默认true，为true时会强制跳转到授权页面，否则只返回是否授权的状态
   * @param goBack 是否返回上一页,默认true，为false时会强制跳转到首页。
   * @return {boolean} true: 已授权；false: 未授权
   */
  checkAuth(forceAuth = true, goBack = true) {
    const {base} = store.getState();
    const isRegistered = !!base.registered;
    if (!isRegistered && forceAuth) {
      const data = {
        goBack: !!goBack,
      };

      // 记录返回路由
      // 主要用于公众号端，授权重定向后恢复
      if (process.env.WX_OA === 'true' && process.env.TARO_ENV == 'h5') {
        wxApi.setStorage({
          key: '__back_route__',
          data: window.location.hash ? window.location.hash.slice(1) : window.location.hash,
          success: (res) => {
            console.log('setStorage success', res)
          },
          fail: (res) => {
            console.log('setStorage fail', res)
          },
        });
      }

      $navigateTo({
        url: '/wxat-common/pages/wx-auth/index',
        data,
      });
    }
    return isRegistered;
  },
};
