import wxApi from './wxApi';

/**
 * 授权上报字段枚举
 * 参考文档：https://docs.qq.com/sheet/DVkNwTG9WT3ZrdEhj?tab=BB08J2
 *
  parm.lastp.page_title	页面标题
  parm.lastp.scene_type	来源场景类型
  parm.lastp.scene_id	场景id
  parm.lastp.act_type	活动类型  group coupun bargain red_packet seckill
  parm.lastp.act_id	活动id
  parm.lastp.act_name	活动名称
  parm.lastp.group_no	拼团no
  parm.lastp.item_type	商品类型
  parm.lastp.item_no	商品编号item_no
  parm.lastp.coupon_id	领取的优惠券的券id
  parm.lastp.bargain_initiate_id	砍价id
 */

const keyCollections = {
  // 允许传入的key
  allow: [
    '_pageTitle', // 页面标题(自动埋入)
    'sceneType', // 场景类型
    'sceneId', // 场景id
    'activityType', // 活动类型
    'activityId', // 活动id
    'activityName', // 活动名称
    'groupNo', // 拼团no
    'itemType', // 商品类型
    'itemNo', // 商品编号item_no
    'couponId', // 领取的优惠券的券id
    'bargainInitiateId', // 砍价id
    'itemName' // 商品名称
  ],
  // 账号授权上报的key
  auth: {
    _pageTitle: 'page_title',
    sceneType: 'scene_type',
    sceneId: 'scene_id',
    activityType: 'act_type',
    activityId: 'act_id',
    activityName: 'act_name',
    groupNo: 'group_no',
    itemType: 'item_type',
    itemNo: 'item_no',
    couponId: 'coupon_id',
    bargainInitiateId: 'bargain_initiate_id',
    itemName: 'item_name'
  },
  // 分享上报的key
  share: {
    sceneId: 'refBzId',
  },
};

interface ConvertKeyType {
  auth: Record<string, any>;
  share: Record<string, any>;
}

const convertKeyUtil = {
  buffer: {},
  convert(keys: Record<string, string>): Record<string, any> {
    const params = {};
    const buffer = this.buffer;

    for (const key in buffer) {
      if (keys[key]) params[keys[key]] = buffer[key];
    }

    this.buffer = {};
    return params;
  },
};

const convertKeyHandler = {
  get auth(): Record<string, any> {
    return convertKeyUtil.convert(keyCollections.auth);
  },

  get share(): Record<string, any> {
    return convertKeyUtil.convert(keyCollections.share);
  },
};

/**
 * 历史业务场景(栈)，后进后出，只保留最近10个
 */
const BZSceneHistory: Array<Record<string, any>> = [];

const navigationBarTitleCache = {};

const sceneInfoCache = {};

/**
 * 获取当前页面的路由及名称
 */
const getPageInfo = (): Record<string, string> => {
  const route = wxApi.$getCurrentPageRoute();
  const {
    config: { navigationBarTitleText: pageTitle } = { navigationBarTitleText: '' },
  } = wxApi.$getCurrentTaroPage();

  return {
    route,
    pageTitle,
  };
};

const checkHistoryBuild = (): void => {
  const route = wxApi.$getCurrentPageRoute();
  const firstItem = BZSceneHistory[0];

  if (!firstItem || firstItem.route !== route) {
    _buildBZSceneHistory();
  }
};

/**
 * 自动记录浏览信息
 */
export const _buildBZSceneHistory = (): void => {
  const { route, pageTitle } = getPageInfo();
  const firstItem = BZSceneHistory[0];

  if (firstItem && firstItem.route === route) return; // 浏览记录已经创建过了
  // 默认复制上一个页面的sceneInfo
  const copySceneInfo = sceneInfoCache[route];

  const historyItem = {
    route,
    accessTime: Date.now(),
    sceneInfo: {
      _pageTitle: navigationBarTitleCache[route] || pageTitle,
    },
  };

  historyItem.sceneInfo = { ...copySceneInfo, ...historyItem.sceneInfo };

  BZSceneHistory.unshift(historyItem);
  BZSceneHistory.length > 10 && (BZSceneHistory.length = 10);
};

/**
 * 订阅navigationBarTitle变化，更正pageTitle
 */
export const _subscribeNavigationBarTitleChangeForBZScene = (options: Record<string, any>): void => {
  const route = wxApi.$getCurrentPageRoute();
  const corresItem = BZSceneHistory.find((item) => item.route == route);

  if (corresItem) {
    corresItem.sceneInfo._pageTitle = options.title;
  }

  navigationBarTitleCache[route] = options.title;
};

/**
 * 暴露工具方法
 */
const util = {
  /**
   * 获取当前的业务场景
   */
  get get(): ConvertKeyType {
    checkHistoryBuild();

    convertKeyUtil.buffer = BZSceneHistory[0].sceneInfo;
    return convertKeyHandler;
  },

  /**
   * 获取上一个业务场景
   */
  get getPrev(): ConvertKeyType {
    checkHistoryBuild();

    convertKeyUtil.buffer = (BZSceneHistory[1] && BZSceneHistory[1].sceneInfo) || {};
    return convertKeyHandler;
  },

  /**
   * 手动埋入场景数据，如场景类型、场景ID等，字段参考上方枚举
   * @param sceneInfo 传入对象
   */
  add(sceneInfo: Record<string, any>): void {
    checkHistoryBuild();

    const route = wxApi.$getCurrentPageRoute();
    const firstItem = BZSceneHistory[0];
    const newSceneInfo = { ...firstItem.sceneInfo, ...sceneInfo };

    // 剔除非法的key
    for (const key in newSceneInfo) {
      if (!keyCollections.allow.includes(key)) {
        delete newSceneInfo[key];
      }
    }

    firstItem.sceneInfo = newSceneInfo;
    sceneInfoCache[route] = firstItem.sceneInfo;
  },
};

export default util;
