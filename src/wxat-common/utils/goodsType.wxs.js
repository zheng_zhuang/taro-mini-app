const filters = {
  // 截取商品类型
  // 商品itemNo的最后三位为商品的类型
  // @param itemNo
  cutGoodsType: function (itemNo) {
    if (!!itemNo) {
      return parseInt(itemNo.substring(itemNo.length - 3));
    }
  },
};

export default {
  goodsType: filters.cutGoodsType,
};
