const TEN_THOUSAND = 10000;
const THOUSAND = 1000;

function fixNum(text) {
  if (text.toFixed(1) == Math.floor(text)) {
    return Math.floor(text);
  } else {
    return text.toFixed(1);
  }
}

export default {
  trendNumSimplify(num) {
    let text = num,
      textStr = num + '';
    if (textStr.length >= 3) {
      if (text >= TEN_THOUSAND) {
        text = fixNum(text / TEN_THOUSAND) + 'w';
      } else if (text >= THOUSAND) {
        text = fixNum(text / THOUSAND) + 'k';
      }
    }
    return text;
  },
};
