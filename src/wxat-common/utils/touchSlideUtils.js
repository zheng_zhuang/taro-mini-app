/**
 * 滑动操作工具类，如滑动删除
 */
const deltaX = 10;
const touchStart = {
  startX: 0,
  startY: 0,
};
export default {
  touchS(e) {
    if (e.touches.length === 1) {
      touchStart.startX = e.touches[0].clientX;
      touchStart.startY = e.touches[0].clientY;
    }
  },
  /**
   *
   * @param e 手指移动事件
   * @param slideLimitWidth 要划出的最大距离
   * @param isSlided item是否已经是划出的状态
   * @return {string}
   */
  touchM(e) {
    if (e.touches.length === 1) {
      // 手指移动时水平方向位置
      const moveX = e.touches[0].clientX;
      const moveY = e.touches[0].clientY;
      // 手指起始点位置与移动期间的差值
      const disX = touchStart.startX - moveX;
      const disY = touchStart.startY - moveY;
      // 滑动距离小于10，认为无滑动
      if (Math.abs(disX) < deltaX || Math.abs(disX) < Math.abs(disY)) {
        return false;
      }
      return true;
    }
    return '';
  },
  touchE(e, slideLimitWidth, isSlided) {
    const slideResult = {
      txtStyle: '',
      isSlided: false,
    };
    if (e.changedTouches.length === 1) {
      // 手指移动结束后水平位置
      const endX = e.changedTouches[0].clientX;
      const endY = e.changedTouches[0].clientY;
      // 触摸开始与结束，手指移动的距离
      const disX = touchStart.startX - endX;
      const disY = touchStart.startY - endY;
      console.log('touchE disx', disX);
      if (Math.abs(disX) > deltaX && Math.abs(disX) > Math.abs(disY)) {
        // 如果距离小于删除按钮的1/3，不显示删除按钮
        if (disX > 0) {
          const shouldSlided = disX > slideLimitWidth / 3;
          slideResult.txtStyle = shouldSlided ? 'left:-' + slideLimitWidth / 2 + 'px;' : 'left:0px;';
          slideResult.isSlided = shouldSlided;
        } else {
          const shouldSlided = Math.abs(disX) < slideLimitWidth / 3;
          slideResult.txtStyle = shouldSlided ? 'left:-' + slideLimitWidth / 2 + 'px;' : 'left:0px;';
          slideResult.isSlided = shouldSlided;
        }
      } else {
        if (isSlided) {
          slideResult.txtStyle = 'left:-' + slideLimitWidth / 2 + 'px;';
          slideResult.isSlided = true;
        } else {
          slideResult.txtStyle = 'left:0px;';
          slideResult.isSlided = false;
        }
      }
    }
    return slideResult;
  },
};
