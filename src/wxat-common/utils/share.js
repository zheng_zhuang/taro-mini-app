import Taro from '@tarojs/taro';
import utils from './util';
import protectedMailBox from './protectedMailBox';
import wxApi from './wxApi';
import api from '../api/index';
import store from '@/store';
import {updateBaseAction} from '@/redux/base';
import {generateShareUrl} from '@/wxat-common/utils/platform/official-account.h5';

/**
 * Created by a123 on 2020/3/4.
 * @author trumpli<李志伟>
 */
const config = {
  useId: null,
  storeId: null,
  wxOpenId: null,
};

const serverKeyConvertToEtlKey = {
  refBz: '_ref_bz', //来源场景类型 [String]
  refUseId: '_ref_useId', //分享者Id [Number]
  refWxOpenId: '_ref_wxOpenId', //分享者 OpenId [String]
  refStoreId: '_ref_storeId', //来源门店Id [Number]
  refBzId: '_ref_bz_id', //来源业务ID [Number | String]
  refSceneName: '_ref_scene_name', //来源业务名称 [String]
  refBzName: '_ref_bz_name', //来源页面名称 [String]
  bzscene: 'bzscene', //来源场景类型 [Number] 即将废弃！！
};

const allowBuryKeys = {
  sk: 'sk',
  bzscene: 'bzscene',
  source: 'source',

  //data bury format
  _ref_bz: '_ref_bz',
  _ref_useId: '_ref_useId',
  _ref_storeId: '_ref_storeId',
  _ref_wxOpenId: '_ref_wxOpenId',
  _ref_bz_id: '_ref_bz_id',
  _ref_bz_name: '_ref_bz_name',
  _ref_scene_name: '_ref_scene_name',

  //server format
  refBz: 'refBz',
  refUseId: 'refUseId',
  refStoreId: 'refStoreId',
  refWxOpenId: 'refWxOpenId',
  refBzId: 'refBzId',
  refBzName: 'refBzName',
  refSceneName: 'refSceneName'
};

const ShareBZs = {
  SEARCH: 'search', //分销中心推广素材
  INTER_PURCHASE_CENTER: 'inter_purchase_center',//内购专场
  INTER_PURCHASE_CLASS_FIY: 'inter_purchase_class_fiy', //内购专场分类页
  GIFT_CARD_MINE_SENDING: 'gift_card_mine_sending', //我赠送礼品卡
  GIFT_CARD_SENDING_DETAIL: 'gift_card_sending_detail', //礼品卡赠送详情
  RED_PACKET_DETAIL: 'red_packet_detail', //红包详情
  SECKILL_DETAIL: 'seckill_detail', //秒杀活动详情
  CARD_DETAIL: 'card_detail', //卡项详情
  SERVE_DETAIL: 'serve_detail', //服务详情
  CUT_PRICE_DETAIL: 'cut_price_detail',//砍价详情
  CUT_PRICE_JOIN: 'cut_price_join', //我参与的砍价详情
  GOODS_DETAIL: 'goods_detail', //商品详情
  GOODS_LIST: 'goods_list', //商品列表
  GRAPH_DETAIL: 'graph_detail', //营销图文详情
  GROUP_DETAIL: 'group_detail', //拼团活动详情
  GROUP_JOIN: 'group_join', //我参与的拼团详情
  HOME: 'home', //首页
  LUCKY_DIAL: 'lucky_dial',//幸运大转盘
  TABBAR_INTER_PURCHASE: 'tabbar_inter_purchase', //tabbar内购专场
  COUPON_CENTER: 'coupon_center', //领券中心
  SECKILL_LIST: 'seckill_list', //秒杀活动列表
  FORM_TOOL: 'form_tool', //表单
  CUT_PRICE_LIST: 'cut_price_list', //砍价活动列表
  TASK_CENTER: 'task_center', //任务中心
  GROUP_LIST: 'group_list', //拼团列表
  COUPON_LIST: 'coupon_list', //我的优惠券
  CARD_PACK_DETAIL: 'card_pack_detail', //卡包详情
  E_BOOKS: 'ebooks', //电子画册
  MICRO_DECORATE: 'micro_decorate', //微装方案详情
  CATEGORY_DETAIL: 'category_detail', //商品分类
  MORE: 'more', //首页营销组件展示更多页
  EQUITY_CARD_DETAIL: 'equity_card_detail', //权益卡详情
  LIVE_LIST: 'live_list', //直播列表
  LIVE_PLAYER: 'live_player', //直播间
  GUIDE_SHARE_COUPON: 'guide_share_coupon', //导购专属优惠券
  GUIDE_MATERIAL_POSTER: 'guide_material_poster', //素材库海报
  GUIDE_MATERIAL_VIDEO: 'guide_material_video', //素材库视频
  GUIDE_MATERIAL_PDF: 'guide_material_pdf', //素材库PDF
  FISSION_RED_PACKET_BOOSTER: 'fission_red_packet_booster', //首页裂变红包好友助力
  FISSION_RED_PACKET_DETAIL: 'fission_red_packet_detail', //裂变红包好友助力详情
  FISSION_RED_PACKET_MINE: 'fission_red_packet_mine', //我的裂变红包
  PROMOTION_AMBASSADOR: 'promotion_ambassador', //推广大使
  UKEMPLOYEE_INVITATION: 'ukEmployee_invitation', //优客员工邀请
};

// !!!重要： 即将废弃，请使用 ShareBZs
const ShareBzscene = {
  POSTER: 1000, //海报
  ARTICLE: 1001, //获客文章
  GUIDE_SHARE_COUPON: 1002, //导购推券
  VIDEO: 1003, //推广视频
  PDF: 1004, //推广PDF
  CUT_PRICE: 1007, //推广砍价
  EQUITY_CARD: 1008, //推广权益卡
};

/**
 * 构建分享路径的公共参数
 * @param url  {string}
 * @param bz {string} 来源场景类型
 * @param bzName {string} 来源页面名称
 * @param [bzscene] {number} 来源场景类型（即将废弃）
 * @param [bzId] {string} 来源业务ID
 * @param [sceneName] {string} 来源业务名称
 * @returns {string}
 */
function buildShareUrlPublicArguments(shareDetail) {
  const {url, bz, bzscene, bzId, bzName} = shareDetail
  const add = [];
  const args = buildSharePublicArguments({...shareDetail, forServer: false});
  for (const key in args) add.push(key + '=' + args[key]);
  return url + (url.indexOf('?') > 0 ? '&' : '?') + add.join('&');
}

/**
 * @return Object
 * @param [bz] 来源场景类型
 * @param [forServer] 默认给埋点数据使用，否则给业务server用
 * @param [bzName] {string} 来源页面名称
 * @param [bzscene] {number} 来源场景类型（即将废弃）
 * @param [bzId] {string} 来源业务ID
 * @param [sceneName] {string} 来源业务名称
 */
function buildSharePublicArguments(shareDetail) {
  const {bz, forServer, bzscene, bzId, bzName, sceneName} = shareDetail;
  const arg = {};
  //来源场景类型
  if (!!bz) arg[forServer ? allowBuryKeys.refBz : allowBuryKeys._ref_bz] = bz;
  //来源页面名称
  if (!!bzName) arg[forServer ? allowBuryKeys.refBzName : allowBuryKeys._ref_bz_name] = bzName;
  //来源用户id
  if (!!config.useId) arg[forServer ? allowBuryKeys.refUseId : allowBuryKeys._ref_useId] = config.useId;
  //来源门店id
  if (!!config.storeId) arg[forServer ? allowBuryKeys.refStoreId : allowBuryKeys._ref_storeId] = config.storeId;
  //来源用户openId
  if (!!config.wxOpenId) arg[forServer ? allowBuryKeys.refWxOpenId : allowBuryKeys._ref_wxOpenId] = config.wxOpenId;
  //来源场景类型
  if (!!bzscene) {
    arg['bzscene'] = bzscene;
  }
  //设置来源业务ID
  if (!!bzId) arg[forServer ? allowBuryKeys.refBzId : allowBuryKeys._ref_bz_id] = bzId;
  //设置来源业务名称
  if (!!sceneName) arg[forServer ? allowBuryKeys.refSceneName : allowBuryKeys._ref_scene_name] = sceneName;

  arg['source'] = store.getState().base.source;

  return arg;
}

function buildShareCurrentPage() {
  const {route, options} = Taro.getCurrentPages().slice(-1)[0];
  const qs = Object.keys(options)
    .reduce((pre, key) => `${pre}&${key}=${options[key]}`, '')
    .slice(1);
  let path = route;
  if (qs) {
    path = path + '?' + qs;
  }

  return {
    path: buildShareUrlPublicArguments({url: path, bz: ''}),
  };
}

/**
 * 设置当前门店ID
 * @param storeId
 */
function setStoreId(storeId) {
  config.storeId = storeId || '';
}

/**
 * 设置当前用户ID
 * @param useId
 */
function setUseId(useId) {
  config.useId = useId || '';
}

/**
 * 设置当前用户的openId
 * @param openId
 */
function setOpenId(openId) {
  config.wxOpenId = openId || '';
}

function getUserId() {
  return config;
}

function isString(a) {
  'use strict';
  return typeof a === 'string' || a instanceof String;
}

//分享映射二维key
const MapperQrCode = {
  /**
   * 二维码映射key::从App.onLaunch or Page.onLoad option中提取
   * @param scene
   * @return {String}
   */
  getMapperKey(scene) {
    if (!!scene && isString(scene)) return utils.getQueryString('&' + decodeURIComponent(scene), 'sk');
    return '';
  },
  /**
   * 获取-二维码映射内容
   * @return {Object|''}
   */
  getMapperContent() {
    return protectedMailBox.read('LAUNCH_DATA');
  },
  /**
   * 邮寄-二维码映射内容
   * @param qrCodeScene
   */
  sendMapperContent(qrCodeScene) {
    const qrCodeSceneObject = JSON.parse(qrCodeScene);
    console.log('二维码信息解析::', qrCodeSceneObject);
    protectedMailBox.send(qrCodeSceneObject.page, 'LAUNCH_DATA', qrCodeSceneObject);
  },
  /**
   * 获取服务端-二维码映射内容
   * @param scene
   * @return {Promise<null|Object>}
   */
  async apiMapperContent(scene) {
    const sk = this.getMapperKey(scene);
    if (!sk) return null;
    try {
      return await wxApi.request({url: api.getShareSceneBySk + sk}).then((res) => {
        const data = JSON.parse(res.data);

        if (data.source) {
          setSource(data.source);
        }
        // 广告埋点
        setTimeout(() => {
          let baseInfo = store.getState().globalData;
          if (data.source == 1 && data.param) {
            let advInfo = JSON.parse(data.param);
            console.log("advInfo", advInfo)
            // let userInfo = baseInfo.userInfo == null ? {nickname: "", phone: ""} : baseInfo.userInfo;
            let userInfo = wxApi.getStorageSync("userInfo").userInfo;
            let storeInfo = baseInfo.currentStore;
            wxApi.request({
              url: api.goods.advLog,
              method: 'POST',
              data: {
                "order_number": advInfo.adOrderNo, //订单编号
                "user_nickname": userInfo.nickname, //用户昵称
                "user_phone": userInfo.phone, //用户手机号码
                "hotel_contract": advInfo.originOutStoreId, //酒店合同号
                "room_no": advInfo.roomId, //酒店房间
                "adv_flag": advInfo.adPosition, //刊位标识
                "goods_name": advInfo.itemName, //商品名称
                "store_name": storeInfo.name, //旅优选门店名称
                "store_address": storeInfo.address, //旅优选门店地址
                "poi_store_name": "", //POI门店名称
                "poi_store_address": "", //POI门店地址
                "poi_distance": "" //POI门店距离
              },
            })
          }
        }, 1000)

        return data;
      });
    } catch (xe) {
      return null;
    }
  },
};

// 判断是否外部分享
const isShareLinkEnter = {
  /**
   * 是否携带来源门店信息
   * @param options
   * @return {Boolean}
   */
  getRefStoreId(options) {
    return !!options._ref_storeId;
  },
};

/**
 * 来源
 * @param {String} source
 */
function setSource(source) {
  store.dispatch(updateBaseAction({source}));
}

/**
 * @returns {'wxwork' | 'wechat'}
 */
function getSource() {
  return store.getState().base.source;
}

export default {
  MapperQrCode,
  allowBuryKeys,
  serverKeyConvertToEtlKey,
  buildSharePublicArguments,
  buildShareUrlPublicArguments,
  buildShareCurrentPage,
  isShareLinkEnter,
  ShareBZs,
  ShareBzscene,
  setStoreId,
  setUseId,
  setOpenId,
  getUserId,
  setSource,
  getSource,
  generateShareUrl,
};
