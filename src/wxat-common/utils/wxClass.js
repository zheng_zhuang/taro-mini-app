/**
 * 小程序Page扩展
 */

import event from './event.js';
//下面这行不能删除，作用是给 App,Page,Components 增加埋点功能
// import wakeApi from '../sdks/buried/wkapi/1.2.1/index.js';
import objEntries from './obj-entries.js';

export const PAGE_TYPE = 0;
export const COMPONENT_TYPE = 1;

function getLifeCycleName (type) {
  const map = {
    [PAGE_TYPE]: {
      load: 'onLoad',
      unLoad: 'onUnload',
      onShow: 'onShow',
      onHide: 'onHide',
    },

    [COMPONENT_TYPE]: {
      load: 'attached',
      unLoad: 'detached',
    },
  };

  return map[type];
}

/**
 * 给Page增加事件收发的能力
 * @example
 * on: {
 *   test (params) {
 *     console.log('接收到test事件')
 *   }
 * },
 * onLoad () {
 *   this.$broadcast('test', '参数：广播了test事件')
 * }
 */
export function eventify (type, options) {
  const lifeCycleName = getLifeCycleName(type);
  const onLoad = options[lifeCycleName.load];
  const onUnload = options[lifeCycleName.unLoad];
  const onShow = options[lifeCycleName.onShow];
  const onHide = options[lifeCycleName.onHide];
  const events = {};

  options[lifeCycleName.load] = function (...args) {
    // 将options.on转换成event事件监听
    for (let [key, val] of objEntries.obj2KeyValueArray(options.on || {})) {
      const fn = val.bind(this);
      events[key] = fn;
      event.on(key, fn);
    }
    // 广播事件能力
    this.$broadcast = (key, data) => {
      event.emit(key, data);
    };

    onLoad && onLoad.call(this, ...args);
  };

  options[lifeCycleName.unLoad] = function () {
    // 卸载所有事件
    for (let [key, val] of objEntries.obj2KeyValueArray(events)) {
      event.off(key, val);
    }
    onUnload && onUnload.call(this);
  };

  /*监听Page的onShow和onHide方法，给data设置__isPageShow字段，判断页面是否为show的状态*/
  if (type === PAGE_TYPE) {
    options[lifeCycleName.onShow] = function () {
      this.setData({
        __isPageShow: true,
      });

      onShow && onShow.call(this);
    };
  }

  options[lifeCycleName.onHide] = function () {
    this.setData({
      __isPageShow: false,
    });

    onHide && onHide.call(this);
  };
}

/**
 * 给Page增加类似Vue Computed能力
 */
export function computedify (type, options) {
  if (!options.computed) return;

  const lifeCycleName = getLifeCycleName(type);
  const onLoad = options[lifeCycleName.load];

  options[lifeCycleName.load] = function (...args) {
    const { setData } = this;

    const setComputed = () => {
      const computed = {};
      for (let [key, fn] of objEntries.obj2KeyValueArray(options.computed)) {
        const val = fn.call(this);
        // computed未改变则不需要setData
        if (this.data[key] !== val) {
          computed[key] = fn.call(this);
        }
      }
      setData.call(this, computed);
    };

    // hack setData方法，在callback时重新计算computed
    Object.defineProperty(this, 'setData', {
      get () {
        return (data, callback) => {
          const _callback = (...args) => {
            setComputed();
            callback && callback.call(this, ...args);
          };
          setData.call(this, data, _callback);
        };
      },
    });

    // 初始化
    setComputed();
    onLoad.call(this, ...args);
  };
}

export default {
  Page (options) {
    eventify(PAGE_TYPE, options);
    computedify(PAGE_TYPE, options);
    return Page(options);
  },
  Component (options) {
    eventify(COMPONENT_TYPE, options);
    computedify(COMPONENT_TYPE, options);
    return Component(options);
  },
};
