

import wxApi from '@/wxat-common/utils/wxApi';

const returnPayTradeType = () => {
  // 1:微信小程序支付 3:微信公众号支付
  let payTradeType = 1
  if (process.env.WX_OA === 'true') {
    payTradeType = 3
  }
  return payTradeType
}

const hotelPay = (cbParams, options) => {
  const payParams = {
    timestamp: cbParams.timeStamp + '',
    nonceStr: cbParams.nonceStr,
    package: cbParams.package || 'prepay_id=' + cbParams.prepayId,
    signType: 'RSA',
    paySign: cbParams.paySign,
    appId: cbParams.appId
  }
  const payFn = process.env.WX_OA === 'true' ? wx.chooseWXPay : wxApi.requestPayment;
  payFn({
    ...payParams,
    success(e) {
      if (options && options.wxPaySuccess) {
        if (options.extConfig && options.extConfig.immediateCallback) {
          options.wxPaySuccess()
        } else {
          // 微信支付订单状态有延迟，延迟1秒加载
          setTimeout(() => {
            options.wxPaySuccess();
          }, 1000);
        }
      }
    },
    fail(e) {

      if (options && options.wxPayFail) {
        options.wxPayFail(e);
      }
    },
  });
}
export default {
  returnPayTradeType,
  hotelPay
}