import number from './number.js';
import floatNum from '../utils/float-num.js';

export default {
  /**
   * @param {Number} money 传入以分为单位的参数
   * @return 返回元为单位的参数
   */
  fen2Yuan(money, fixed = 2) {
    if (typeof money !== 'number' || isNaN(money)) return null;
    return number.separator(floatNum.floatDiv(money, 100).toFixed(fixed));
  },
  /**
   * @param {Number} money 传入以分为单位参数
   * @return 返回元为单位的参数， 不加分隔符
   */
  fen2YuanFixed(money, fixed = 2) {
    if (typeof money !== 'number' || isNaN(money)) return null;
    return floatNum.floatDiv(money, 100).toFixed(fixed);
  },
  /**
   * 处理多规格价格 - 截取返回最低 | 最高价格
   * @param {price} 价格
   * @param {position} 截取最低|最高位置
   * @return 返回单个价格
   */
  formatPrice(price, position) {
    let result = 0;
    if (price && price.indexOf('-') !== -1) {
      result = +price.split('-')[position]; // 划线价取最高价格
    } else {
      result = parseFloat(price);
    }
    return result;
  },
};
