export function qs2obj(str: string): Record<string, string> {
  return decodeURIComponent(str || '')
    .split('&')
    .map((kv) => {
      const [key, val] = kv.split('=');
      if (key) return { [key]: val };
      return {};
    })
    .reduce((pre, cur) => ({ ...pre, ...cur }), {});
}

export function obj2qs(obj: Record<string, string | number>): string {
  return Object.keys(obj)
    .reduce((qs, key) => `${qs}&${key}=${obj[key]}`, '')
    .slice(1);
}
