/**
 * @author shaorui
 * 从数组中找出最大值或者最小值
 * @ param target 目标数组
 * @ param {Function Boolean (obj1, obj2)}compartor 数组比较器，比较两个对象的大小，必须使用return obj1 < obj2格式
 * @ param isFindMax 是否是查询最大值
 * example:
 * persons: [{age: 18}, {age: 20}, {age: 25}]
 * findMaxOrMin(persons, (p1, p2) => {
 *  return p1.age < p2.age;
 * }, true);
 * 得到的结果就是列表中年龄最大的那一个。
 *
 * findMaxOrMin(persons, (p1, p2) => {
 *  return p1.age < p2.age;
 * }, false);
 * 得到的结果就是列表中年龄最小的那一个。
 */
const findMaxOrMin = function (target, compartor, isFindMax) {
  return target.reduce((obj1, obj2) => {
    if (isFindMax) {
      return compartor(obj1, obj2) ? obj2 : obj1;
    } else {
      return compartor(obj1, obj2) ? obj1 : obj2;
    }
  });
};

/**
 * 操作数组相关的工具类
 */
export default {
  /**
   * 查找最大值
   * @param target 目标数组
   * @param compartor 比较器，详细信息参考@link{#findMaxOrMin}
   */
  findMax(target, compartor) {
    return findMaxOrMin(target, compartor, true);
  },
  /**
   * 查找最小值
   * @param target 目标数组
   * @param compartor 比较器，详细信息参考@link{#findMaxOrMin}
   */
  findMin(target, compartor) {
    return findMaxOrMin(target, compartor, false);
  },
};
