/**
 * 模版消息上报
 */
import api from '../api/index.js';
import wxApi from './wxApi';
export default {
  /**
   * 上报模版消息
   * @param formId {String}
   */
  send(formId) {
    console.log('start send template message...');
    wxApi.request({
      url: api.tempMessage,
      loading: false,
      data: {
        formId: formId,
      },
    });
  },
};
