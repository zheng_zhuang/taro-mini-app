/**
 * 正则表达式
 */

/**
 * 手机正则
 * @see https://github.com/VincentSit/ChinaMobilePhoneNumberRegex
 */
export const PHONE_REG = /^(?:\+?86)?1(?:3\d{3}|5[^4\D]\d{2}|8\d{3}|7(?:[235-8]\d{2}|4(?:0\d|1[0-2]|9\d))|9[0-35-9]\d{2}|66\d{2})\d{6}$/;
