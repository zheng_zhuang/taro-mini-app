const prefix = 'https://online-material-1259575047.cos.ap-guangzhou.myqcloud.com';
const minTail = '?imageView2/2/w/100/h/100';
const midTail = '?imageView2/2/w/200/h/200';
const oneRowTwoTail = '?imageView2/2/w/400/h/400';
const sourceDomain = 'online-material-1259575047.cos.ap-guangzhou.myqcloud.com';
const cdnDomain = 'material.wakedata.com';
const expDomain = new RegExp('online-material-1259575047.cos.ap-guangzhou.myqcloud.com', 'g');
function cdn(url) {
  if (url) {
    return url.replace(sourceDomain, cdnDomain);
  }
}
export default {
  thumbnailMin(url) {
    if (!!url) {
      if (url.indexOf(prefix) === 0) url += minTail;
      url = cdn(url);
    }
    return url;
  },
  thumbnailMid(url) {
    if (!!url) {
      if (url.indexOf(prefix) === 0) url += midTail;
      url = cdn(url);
    }
    return url;
  },
  thumbnailOneRowTwo(url) {
    if (!!url) {
      if (url.indexOf(prefix) === 0) url += oneRowTwoTail;
      url = cdn(url);
    }
    return url;
  },
  cdn,
  richTextFilterCdnImage(text) {
    return text.replace(expDomain, cdnDomain);
  },
};
