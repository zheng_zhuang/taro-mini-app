const m = 10;

const k = 1000 * m;

const distance = (value) => {
  if (!value) {
    return '-';
  }

  if (value > k) {
    return Math.floor(value / k) + '.' + ((value % k) + '').substring(0, 1) + 'km';
  }

  return Math.round(value / m) + 'm';
};

export default distance;
