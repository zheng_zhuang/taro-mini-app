function toHour(t) {
  const over = t >= 24;
  if (over) {
    t -= 24;
  }
  const hour = parseInt(t);
  const min = 0 !== hour ? t % hour : 0;
  return (over ? '次日' : '') + (hour > 9 ? hour : '0' + hour) + ': ' + (min === 0 ? '00' : '30');
}

function toDecimalDays(num) {
  //周日 -> 周六
  let days = [!0, !0, !0, !0, !0, !0, !0],
    i = 0,
    j = days.length;
  for (; i < j; i++) {
    days[i] = (num >> i) % 2 === 1;
  }
  return days.reverse();
}

function toShowNames(num) {
  let array = toDecimalDays(num),
    SimpleDayNames = ['日', '一', '二', '三', '四', '五', '六'],
    names = [],
    i = 0,
    j = array.length;
  for (; i < j; i++) {
    if (!!array[i]) {
      names.push(SimpleDayNames[i]);
    }
  }
  return names.length > 0 ? '周' + names.join(' ') : '';
}

export default {
  computed(businessStartHour, businessEndHour, businessDayOfWeek) {
    if (businessStartHour == null || businessEndHour == null || !businessDayOfWeek) {
      return '- -';
    }
    return toShowNames(businessDayOfWeek) + ' ' + toHour(businessStartHour) + ' 至 ' + toHour(businessEndHour);
  },
};
