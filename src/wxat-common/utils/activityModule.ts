export enum TicketStatus {
    CANCEL = 0, // 已取消
    WAIT_VERIFY = 10, // 待验证
    AUTHENTICATED = 20, // 已验证
    REFUND_UNDER_REVIEW = 30, // 退票审核中
    TICKET_REFUNDED = 40, // 已退票
    REFUND_FAILED = 50, // 退款失败
    AUDIT_REJECTION = 60, // 审核拒绝
    NOT_IN_FORCE = 70, // 未生效
}

export const TicketStatusobj = {
    CANCEL: '已取消', // 已取消
    WAIT_VERIFY: '待验证', // 待验证
    AUTHENTICATED: '已验证', // 已验证
    REFUND_UNDER_REVIEW: '退票审核中', // 退票审核中
    TICKET_REFUNDED: '已退票', // 已退票
    REFUND_FAILED: '退款失败', // 退款失败
    AUDIT_REJECTION: '审核拒绝', // 审核拒绝
    NOT_IN_FORCE: '未生效' // 未生效
}