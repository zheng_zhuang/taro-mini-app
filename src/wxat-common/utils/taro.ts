/**
 * Taro 相关 API 封装
 */
/* eslint-disable import/prefer-default-export */
import Taro from '@tarojs/taro';

/**
 * 封装 setState 为 Promise
 * @param ctx
 * @param newState
 */
export function promisifySetState<State = any, Props = any, K extends keyof State = any>(
  ctx: Taro.Component<Props, State>,
  newState: ((prevState: Readonly<State>, props: Props) => Pick<State, K> | State) | (Pick<State, K> | State)
) {
  return new Promise((res) => {
    ctx.setState(newState, res);
  });
}
