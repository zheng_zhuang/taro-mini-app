import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import store from '@/store';
const formatTime = (date) => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':');
};

const formatNumber = (n) => {
  n = n.toString();
  return n[1] ? n : '0' + n;
};

const getQueryString = (url, name) => {
  const reg = new RegExp('(^|&|/?)' + name + '=([^&|/?]*)(&|/?|$)', 'i');
  const r = url.substr(1).match(reg);
  if (r != null) {
    return r[2];
  }
  return null;
};

const toString = Object.prototype.toString;

const formatParams = (params) => {
  const result = {};
  formatObject(result, params, '');
  return result;
};

const formatArrayParams = (params, key) => {
  const result = {};
  formatArray(result, params, key);
  return result;
};

function formatObject(result, obj, keyPath = '') {
  if (keyPath) {
    keyPath += '.';
  }
  for (const key in obj) {
    const value = obj[key];
    const valueType = toString.call(value);
    const newPath = `${keyPath}${key}`;
    if (valueType === '[object Array]') {
      formatArray(result, value, newPath);
    } else if (valueType === '[object Object]') {
      formatObject(result, value, newPath);
    } else if (value !== null && value !== undefined) {
      result[newPath] = value;
    }
  }
}

function formatArray(result, values, keyPath) {
  values.forEach((v, index) => {
    const valueType = toString.call(v);
    const newPath = `${keyPath}[${index}]`;
    if (valueType === '[object Array]') {
      formatArray(result, v, newPath);
    } else if (valueType === '[object Object]') {
      formatObject(result, v, newPath);
    } else if (v !== null && v !== undefined) {
      result[newPath] = v;
    }
  });
}

// 定义一个深拷贝函数  接收目标target参数
function deepClone(target) {
  // 定义一个变量
  let result;
  // 如果当前需要深拷贝的是一个对象的话
  if (typeof target === 'object') {
    // 如果是一个数组的话
    if (Array.isArray(target)) {
      result = []; // 将result赋值为一个数组，并且执行遍历
      for (const i in target) {
        // 递归克隆数组中的每一项
        result.push(deepClone(target[i]));
      }
      // 判断如果当前的值是null的话；直接赋值为null
    } else if (target === null) {
      result = null;
      // 判断如果当前的值是一个RegExp对象的话，直接赋值
    } else if (target.constructor === RegExp) {
      result = target;
    } else {
      // 否则是普通对象，直接for in循环，递归赋值对象的所有值
      result = {};
      for (const i in target) {
        result[i] = deepClone(target[i]);
      }
    }
    // 如果不是对象的话，就是基本数据类型，那么直接赋值
  } else {
    result = target;
  }
  // 返回最终结果
  return result;
}

function formatStringTime(time) {
  if (!time || time <= 0) {
    time = '00:00:00';
    return time;
  }
  time = time / 1000;

  const hourInt = parseInt(time / 3600);
  const hour = hourInt < 10 ? '0' + hourInt : hourInt;

  const minuteS = parseInt(time % 3600);
  const minuteInt = parseInt(minuteS / 60);
  const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

  const secondS = parseInt(minuteS % 60);
  const second = secondS < 10 ? '0' + secondS : secondS;

  return `${hour}:${minute}:${second}`;
}

// detail:只要列表中scan存在一个为true 则需要扫码
async function checkNeedScan(orderList) {
  orderList = orderList.map((e) => JSON.stringify(e));
  orderList = orderList.map((e) => JSON.parse(e));
  console.log(orderList);
  return new Promise((resolve, reject) => {
    resolve(
      orderList.some((e) => {
        return e.scan === true || e.scan === 1;
      })
    );
  });
}

// 兼容旧小程序跳转路径
function filterUrl(url) {
  const urlList = [
    '/wxat-common/pages/home/index',
    '/wxat-common/pages/classify/index',
    '/wxat-common/pages/tabbar-order-list/index',
    '/wxat-common/pages/tabbar-order/index',
    '/wxat-common/pages/tabbar-custom/index',
    '/wxat-common/pages/tabbar-custom1/index',
    '/wxat-common/pages/tabbar-custom2/index',
    '/wxat-common/pages/tabbar-custom3/index',
    '/wxat-common/pages/tabbar-custom4/index',
    '/wxat-common/pages/mine/index',
    '/wxat-common/pages/custom/index',
    '/wxat-common/pages/hotel-reservation-list/index',
    '/wxat-common/pages/service-classification-tree/index',
    '/wxat-common/pages/webview/index',
    '/wxat-common/pages/slot-page/index'
  ];
  if (!url.startsWith('/')) {
    url = '/' + url;
  }
  if (!urlList.includes(url)) {
    if (!url.startsWith('/sub-packages')) {
      // 如果不是迁包后保留的主包url
      url = url.replace('/wxat-common', '/sub-packages/moveFile-package');
    }
  }
  return url;
}

/**
 * 页面切换一些事件等的清除
 *
 */
function clearPageStuff() {
  // 滑道最顶部
  Taro.pageScrollTo({
    scrollTop: 0,
    duration: 0
  })
}

// 获取路径参数
function getUrlParam(name) {
  const reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)")
  if (process.env.TARO_ENV == 'h5') {
    const r = window.location.search.substr(1).match(reg)
    if (r != null) return unescape(r[2])
    return null
  } else {
    const path = Taro.getCurrentInstance().router.path;
    if (path.split("?")[1] == undefined) {
      return null
    }
    const r = path.split("?")[1].substr(1).match(reg)
    if (r != null) return unescape(r[2])
    return null
  }
}

// 判断当前时间是否在某个时间段内
function time_range(beginTime, endTime, nowTime) {
  if (!beginTime || !endTime) {
    return true;
  }
  // beginTime =  endTime代表24小时
  if (beginTime == endTime) {
    return true;
  }
  const strb = beginTime.split(":");
  const stre = endTime.split(":");
  if (!strb.length || !stre.length) {
    return false;
  }
  const b = new Date();
  const e = new Date();
  const n = nowTime || new Date();
  b.setHours(strb[0]);
  b.setMinutes(strb[1]);
  b.setSeconds(0);
  e.setHours(stre[0]);
  e.setMinutes(stre[1]);
  e.setSeconds(0);
  // endTime 小于 beginTime代表跨天
  if (e.getTime() < b.getTime()) {
    if (n.getTime() - b.getTime() < 0) {
      b.setDate(b.getDate() - 1)
    } else {
      e.setDate(e.getDate() + 1)
    }
  }
  if (n.getTime() - b.getTime() >= 0 && n.getTime() - e.getTime() <= 0) {
    return true;
  } else {
    return false;
  }
}

function changeOpacotyToRGBA (rgba, opacoty = 1) {
  const rgx = /^rgba\(((,?\s*\d+){3}).+$/
  const colorRGBA = rgba.replace(rgx, `rgba($1, ${opacoty})`)
  return colorRGBA
}

// 数据上报
function sensorsReportData (eventName = 'default_event_name', data= {}) {
  const state = store.getState();
  let reportData = {}
  if (wxApi.getStorageSync('filmQrCodeDetail')) {
    reportData = getFilmScanData()
  }
  reportData = {...reportData, ...data}
  Taro.getApp().sensors.track(eventName, reportData)
  const wxRegisterData = {
    storeId: state.base?.storeVO?.id || '',
    storeName: state.base?.storeVO?.name || '',
    openId: state.base?.loginInfo?.openId || '',
    phone: state.base?.loginInfo?.phone || '',
    unionId: state.base?.loginInfo?.unionId || '',
    userId: state.base?.loginInfo?.userId || '',
    wxAppId: state.base?.loginInfo?.wxAppId || '',
  }
  Taro.reportEvent(eventName, {
    ...reportData,
    ...wxRegisterData
  })
}

function getFilmScanData() {
  const filmQrCodeDetail = wxApi.getStorageSync('filmQrCodeDetail')
  if (filmQrCodeDetail) {
    return {
      apaas_store_id: filmQrCodeDetail.apaasStoreId,
      apaas_store_name: filmQrCodeDetail.apaasStoreName,
      hotel_contract: filmQrCodeDetail.hotelContract,
      merchant_id: filmQrCodeDetail.merchantId,
      auth_merchant_id: filmQrCodeDetail.authMerchantId,
      wk_store_id: filmQrCodeDetail.wkStoreId,
      auth_wk_app_id: filmQrCodeDetail.authWkAppId,
      qr_code_type: filmQrCodeDetail.qrCodeType,
      room_num: filmQrCodeDetail.roomNum,
      media_id: filmQrCodeDetail.mediaId
    }
  } else {
    return {
      apaas_store_id: '',
      apaas_store_name: '',
      hotel_contract: '',
      merchant_id: '',
      auth_merchant_id: '',
      wk_store_id: '',
      auth_wk_app_id: '',
      qr_code_type: '',
      room_num: '',
      media_id: '',
    }
  }
}

export default {
  formatTime,
  getQueryString,
  formatParams,
  deepClone,
  formatStringTime,
  formatArrayParams,
  checkNeedScan,
  filterUrl,
  clearPageStuff,
  getUrlParam,
  time_range,
  getFilmScanData,
  sensorsReportData
};

export {
  formatTime,
  getQueryString,
  formatParams,
  deepClone,
  formatStringTime,
  formatArrayParams,
  checkNeedScan,
  filterUrl,
  clearPageStuff,
  getUrlParam,
  time_range,
  changeOpacotyToRGBA
}
