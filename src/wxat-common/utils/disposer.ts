/**
 * 资源释放器
 */
export default class Disposer {
  private list: Array<() => void> = [];
  add(...fn: Array<() => void>) {
    this.list = this.list.concat(fn);
  }

  release() {
    if (this.list.length) {
      const copy = this.list.slice(0);
      this.list = [];
      copy.forEach((f) => f());
    }
  }
}
