// 由于微信那边只准小程序展示5个tabbar，但是我们要实现无需审核就可以发布，现在的实现方案是tabbar用首页，3个坑位，我的页面组成，不管小程序真是的tabbar页面有多少个，都展示5个，在自定义的tabbar底部导航组件做文章，非真正显示的永远没有入口的机会
// 防止命名冲突，pageMap所有的参数全部以$开头
export default {
  'pages/custom/index': {
    $componentId: 'custom',
    $options: {},
  },
  'pages/classify/index': {
    $componentId: 'classify',
    $options: {
      $title: '分类',
    },
  },
  'pages/tabbar-coupon-center/index': {
    $componentId: 'tabbar-coupon-center',
    $options: {
      $title: '领券中心',
    },
  },
  'pages/tabbar-cut-price-list/index': {
    $componentId: 'tabbar-cut-price-list',
    $options: {
      $title: '砍价列表',
    },
  },
  'pages/quick-buy/index': {
    $componentId: 'quick-buy',
    $options: {
      $title: '',
    },
  },
  'pages/tabbar-shopping-cart/index': {
    $componentId: 'tabbar-shopping-cart',
    $options: {
      $title: '',
    },
  },
  'pages/shopping-cart/index': {
    $componentId: 'tabbar-shopping-cart',
    $options: {
      $title: '',
    },
  },
  'pages/nearby-store/index': {
    $componentId: 'nearby-store',
    $options: {
      $title: '',
    },
  },
  'pages/tabbar-group-list/index': {
    $componentId: 'tabbar-group-list',
    $options: {
      $title: '',
    },
  },
  'pages/tabbar-order-list/index': {
    $componentId: 'tabbar-order-list',
    $options: {
      $title: '',
    },
  },
  'pages/order-list/index': {
    $componentId: 'tabbar-order-list',
    $options: {
      $title: '订单列表',
    },
  },
  'pages/tabbar-order/index': {
    $componentId: 'tabbar-order',
    $options: {
      $title: '订单',
    },
  },
  'pages/goods-list/index': {
    $componentId: 'goods-list',
    $options: {
      $title: '产品列表',
    },
  },
  'pages/tabbar-integral/index': {
    $componentId: 'tabbar-integral',
    $options: {
      $title: '',
    },
  },
  'pages/tabbar-internal-purchase/index': {
    $componentId: 'tabbar-internal-purchase',
    $options: {
      $title: '',
    },
  },
  'pages/tabbar-works/index': {
    $componentId: 'tabbar-works',
    $options: {
      $title: '',
    },
  }
};
