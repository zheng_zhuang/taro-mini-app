import Taro from '@tarojs/taro';
import store from '@/store';
import H5UrlEnum from '@/wxat-common/constants/H5UrlEnum.js';
import authHelper from '@/wxat-common/utils/auth-helper';
import { loadSDK, configSDK, getParams } from '@/wxat-common/utils/platform/official-account.h5';

const addQuery = (key, value, href = location.href) => {
  const re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
  const separator = href.indexOf('?') !== -1 ? '&' : '?';
  if (href.match(re)) {
    return href.replace(re, '$1' + key + '=' + value + '$2');
  } else {
    return href + separator + key + '=' + value;
  }
}
const addQueries = (kvPairs, href = location.href) => {
  const keys = Object.keys(kvPairs);
    let result = href;

  keys.forEach((key) => {
    const value = kvPairs[key];
    result = addQuery(key, value, result);
  });
  return result;
}
// 跳转易企住参数拼接
const getYqzParams = (extParams = {}) => {
  const sessionId = store.getState().base.sessionId || "";
  const env = process.env.TARO_ENV;
  const appId = store.getState().base.appId || store.getState().ext.appId;
  // const aid =''; const cid='';
  // if (env == 'h5') {
  //   const { appid, component_appid } = getParams();
  //   aid = appid;
  //   cid = component_appid;
  // }
  return {
    sessionId,
    appId,
    // aid,
    // cid,
    ...extParams
  }
}
// 跳转易企住参数拼接
const getYQZPath = (path = '', extParams) => {
  let url = `${H5UrlEnum.YQZ_H5_URL}${path}`;
  url = addQueries(getYqzParams(extParams), url)
  return url
}

const toJumpYQZ = (path, extParams) => {
  if (!authHelper.checkAuth()) return;
  // 环境判断
  const env = process.env.TARO_ENV;

  const url = getYQZPath(path, extParams)
  if (env == 'h5') {
    window.location.href = url;
  } else {
    Taro.redirectTo({url: `/sub-packages/server-package/pages/webview/index?url=${encodeURIComponent(url)}`});
  }
}

export {
  toJumpYQZ,
  getYQZPath
}
