const regYear = new RegExp('(y+)', 'i');

function getDate(timestamp) {
  if (timestamp) return new Date(timestamp);
  return new Date();
}

const filters = {
  formatMoney(number) {
    return !(number + '').includes('.')
      ? // 就是说1-3位后面一定要匹配3位
        (number + '').replace(/\d{1,3}(?=(\d{3})+$)/g, (match) => {
          return match + ',';
        })
      : (number + '').replace(/\d{1,3}(?=(\d{3})+(\.))/g, (match) => {
          return match + ',';
        });
  },
  //钱的处理
  //页面和服务端，汇率为100的 互相转换
  // @param money
  // @param isFromServer
  convertHundredMoneyUnit: function (mony, isFromServer, setAbs) {
    if (!!mony && setAbs) {
      mony = Math.abs(mony);
    }
    if (!!mony && isFromServer) {
      return parseFloat((mony / 100).toFixed(2));
    }
    return !!mony ? mony * 100 : 0;
  },
  convertDiscountUnit: function (discount, isFromServer) {
    if (!!discount && isFromServer) {
      return parseFloat((discount / 10).toFixed(2));
    }
    return !!discount ? discount * 10 : 0;
  },

  //获取date日期count天后的日期
  //date日期,不传为当前日期
  getDateStr: function (count, date) {
    let dd = getDate();
    if (date) {
      dd = getDate(date);
    }
    dd.setDate(dd.getDate() + count);
    const y = dd.getFullYear();
    const m = dd.getMonth() + 1 < 10 ? '0' + (dd.getMonth() + 1) : dd.getMonth() + 1; //获取当前月份的日期，不足10补0
    const d = dd.getDate() < 10 ? '0' + dd.getDate() : dd.getDate(); //获取当前几号，不足10补0
    return y + '-' + m + '-' + d;
  },

  //时间转换
  //日期格式转换，默认为yyyy-MM-dd
  // @param timestamp
  // @param format
  dateFormat: function (timestamp, format) {
    if (!format) {
      format = 'yyyy-MM-dd';
    }

    //兼容 '2021/04/12 13:41' , '2021-04-12 13:41'
    if (!!timestamp && Object.prototype.toString.call(timestamp) === '[object String]') {
      if (/-|\//.test(timestamp)) {
        timestamp = +new Date(timestamp.replace(/-/g, '/'));
      }
    }

    timestamp = parseInt(timestamp);
    const realDate = getDate(timestamp);

    function timeFormat(num) {
      return num < 10 ? '0' + num : num;
    }
    const date = [
      ['M+', timeFormat(realDate.getMonth() + 1)],
      ['d+', timeFormat(realDate.getDate())],
      ['h+', timeFormat(realDate.getHours())],
      ['m+', timeFormat(realDate.getMinutes())],
      ['s+', timeFormat(realDate.getSeconds())],
      ['q+', Math.floor((realDate.getMonth() + 3) / 3)],
      ['S+', realDate.getMilliseconds()],
    ];
    const reg1 = regYear.exec(format);
    // console.log(reg1[0]);
    if (reg1) {
      format = format.replace(reg1[1], (realDate.getFullYear() + '').substring(4 - reg1[1].length));
    }
    for (let i = 0; i < date.length; i++) {
      const k = date[i][0];
      const v = date[i][1];

      const reg2 = new RegExp('(' + k + ')').exec(format);
      if (reg2) {
        format = format.replace(reg2[1], reg2[1].length == 1 ? v : ('00' + v).substring(('' + v).length));
      }
    }
    return format;
  },

  indexOf: function (arr, value) {
    if (!arr || arr.indexOf(value) < 0) {
      return false;
    } else {
      return true;
    }
  },

  //格式化 分米 单位
  formatMeter: function (num) {
    if (!num) return '0m';

    if (num < 10000) {
      return Math.round(num / 10) + 'm';
    }

    return Math.floor(num / 10000) + '.' + ((num % 10000) + '').substring(0, 1) + 'km';
  },

  //格式化imgList
  imgListFormat: function (img) {
    if (!!img) {
      return img.split(',')[0];
    } else {
      return '';
    }
  },
  //格式化imgArr
  imgArrFormat: function (img) {
    var arr = img || '';
    return arr.split(',');
  },
  //格式化订单
  showOrderDesc: function (item) {
    if (item == '10') {
      return '待支付';
    }
    if (item == '20') {
      return '已支付';
    }
    if (item == '30') {
      return '待接单';
    }
    if (item == '40') {
      return '服务中';
    }
    if (item == '49') {
      return '待完成';
    }
    if (item == '60') {
      return '服务完成';
    }
    if (item == '-1') {
      return '取消订单';
    }
  },

  handleOpacity: function (val, opacity) {
    if (!val) return 'rgba(255,255,255)';
    if (val.split('')[0] === '#') {
      return filters.colorHex(val, opacity);
    }

    var res = val.split(',');
    res[res.length - 1] = opacity + ')';
    res = res.join(',');
    return res;
  },
  colorHex: function (sColor, opacity) {
    sColor = sColor.toLowerCase();
    //十六进制颜色值的正则表达式
    // 如果是16进制颜色
    if (sColor.length === 4) {
      var sColorNew = '#';
      for (var i = 1; i < 4; i += 1) {
        sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
      }
      sColor = sColorNew;
    }
    //处理六位的颜色值
    var sColorChange = [];
    for (var i = 1; i < 7; i += 2) {
      sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)));
    }
    var res = 'rgba(' + sColorChange.join(',') + ',1)';
    res = res.split(',');
    res[res.length - 1] = opacity + ')';
    res = res.join(',');
    return res;
  },
  /**
   * @name: cfn@floatAdd
   * @description: 解决两个数相加精度丢失问题
   * @param a
   * @param b
   * @returns {Number}
   */
  floatAdd: function (a, b) {
    let c, d, e;
    if (undefined == a || null == a || "" == a || isNaN(a)) {
      a = 0;
    }
    if (undefined == b || null == b || "" == b || isNaN(b)) {
      b = 0;
    }
    try {
      c = a.toString().split(".")[1].length;
    } catch (f) {
      c = 0;
    }
    try {
      d = b.toString().split(".")[1].length;
    } catch (f) {
      d = 0;
    }
    e = Math.pow(10, Math.max(c, d));
    return (filters.floatMul(a, e) + filters.floatMul(b, e)) / e;
  },
  /**
   * @name: cfn@floatSub
   * @description: 解决两个数相减精度丢失问题
   * @param a
   * @param b
   * @returns {Number}
   */
  floatSub: function (a, b) {
    let c, d, e;
    if (undefined == a || null == a || "" == a || isNaN(a)) {
      a = 0;
    }
    if (undefined == b || null == b || "" == b || isNaN(b)) {
      b = 0;
    }
    try {
      c = a.toString().split(".")[1].length;
    } catch (f) {
      c = 0;
    }
    try {
      d = b.toString().split(".")[1].length;
    } catch (f) {
      d = 0;
    }
    e = Math.pow(10, Math.max(c, d));
    return (filters.floatMul(a, e) - filters.floatMul(b, e)) / e;
  },
  /**
   * @name: cfn@floatMul
   * @description: 解决两个数相乘精度丢失问题
   * @param a
   * @param b
   * @returns {Number}
   */
  floatMul: function (a, b) {
    let c = 0,
      d = a.toString(),
      e = b.toString();
    try {
      c += d.split(".")[1].length;
    } catch (f) {
    }
    try {
      c += e.split(".")[1].length;
    } catch (f) {
    }
    return Number(d.replace(".", "")) * Number(e.replace(".", "")) / Math.pow(10, c);
  },
  /**
   * @name: cfn@floatDiv
   * @description: 解决两个数相除精度丢失问题
   * @param a
   * @param b
   * @returns
   */
  floatDiv: function (a, b) {
    let c; let d; let e = 0; let f = 0;
    try {
      e = a.toString().split(".")[1].length;
    } catch (g) {
    }
    try {
      f = b.toString().split(".")[1].length;
    } catch (g) {
    }
    return c = Number(a.toString().replace(".", "")), d = Number(b.toString().replace(".", "")), filters.floatMul(c / d, Math.pow(10, f - e));
  },
};

export default {
  formatMoney: filters.formatMoney,
  moneyFilter: filters.convertHundredMoneyUnit,
  discountFilter: filters.convertDiscountUnit,
  dateFormat: filters.dateFormat,
  getDateStr: filters.getDateStr,
  indexOf: filters.indexOf,
  formatMeter: filters.formatMeter,
  imgListFormat: filters.imgListFormat,
  imgArrFormat: filters.imgArrFormat,
  showOrderDesc: filters.showOrderDesc,
  handleOpacity: filters.handleOpacity,
  floatAdd: filters.floatAdd,
  floatSub: filters.floatSub,
  floatMul: filters.floatMul,
  floatDiv: filters.floatDiv,
};
