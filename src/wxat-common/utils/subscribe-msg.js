import Taro from '@tarojs/taro';
/**
 * 订阅消息
 */
import api from "../api";
import wxApi from './wxApi';
import subscribeEnum from '../constants/subscribeEnum.js';
import store from '@/store';

export default {
  /**
   * 订阅消息获取下发权限
   */
  sendMessage(Ids) {
    return new Promise((resolve, reject) => {
      const { messageTemplateId } = store.getState().globalData;
      const tmplIds = [];
      Ids.forEach((item) => {
        if (messageTemplateId[item]) {
          tmplIds.push(messageTemplateId[item]);
        }
      });

      if (!(tmplIds.length > 0)) {
        console.log('不发送订阅消息');
        resolve();
        return;
      }

      // 判断版本是否支持订阅消息
      const version = wxApi.getSystemInfoSync().SDKVersion;
      if (this.compareVersion(version, '2.4.4') >= 0) {
        console.log(tmplIds);
        wxApi.requestSubscribeMessage({
          tmplIds: tmplIds,
          success(res) {
            resolve(res);
            console.log('已授权接收订阅消息', res);
          },
          fail(res) {
            resolve();
            console.log('错误订阅消息', res);
          },
        });
      } else {
        resolve();
      }
    });
  },

  // 版本号比较
  compareVersion(v1, v2) {
    v1 = v1.split('.');
    v2 = v2.split('.');
    const len = Math.max(v1.length, v2.length);

    while (v1.length < len) {
      v1.push('0');
    }
    while (v2.length < len) {
      v2.push('0');
    }
    for (let i = 0; i < len; i++) {
      const num1 = parseInt(v1[i]);
      const num2 = parseInt(v2[i]);

      if (num1 > num2) {
        return 1;
      } else if (num1 < num2) {
        return -1;
      }
    }
    return 0;
  },

  /**
   * 订阅消息获取用户的当前设置
   */
  getSetting() {
    wxApi.getSetting({
      withSubscriptions: false,
      success(res) {
        console.log('用户的当前设置', res);
      },
      fail(res) {
        console.log('错误用户的当前设置', res);
      },
    });
  },
};
