import Taro from '@tarojs/taro';
import api from '@/wxat-common/api';
import wxApi from '@/wxat-common/utils/wxApi';
import store from '@/store';

// 避免无效的请求
const cacheMap = new Map();
const genKey = (lastSessionId, lastStoreId) => {
  return `lastSessionId=${lastSessionId}&lastStoreId=${lastStoreId}`;
};

let _shareUserId = ''

export default async function getShopGuideInfo(shareUserId) {
  _shareUserId = shareUserId ? shareUserId : _shareUserId
  const { getState } = store;
  const sessionId = getState().base.sessionId;
  const resuleKey = genKey(sessionId, (getState().base.currentStore || {}).id);
  const cache = cacheMap.get(resuleKey);
  if (cache) return cache;
  else cacheMap.clear();
  const res = await wxApi.request({
    url: api.card.shopCard + `?displayRule=1&shareUserId=${_shareUserId}`,
  });

  if (res.data) {
    const result = res.data;
    cacheMap.set(resuleKey, result);
    return result;
  }

  return null;
}
