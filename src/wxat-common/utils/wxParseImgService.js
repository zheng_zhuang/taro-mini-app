import Taro from '@tarojs/taro';
import wxApi from './wxApi';
import goodsTypeEnum from '../constants/goodsTypeEnum.js';

const imgService = {
  goods: function (dataset) {
    const goodsType = +dataset.goodstype;
    const itemNo = dataset.itemno;
    let url = '';
    switch (goodsType) {
      case goodsTypeEnum.PRODUCT.value:
        url = '/wxat-common/pages/goods-detail/index';
        // url = '/sub-packages/moveFile-package/pages/goods-detail/index';
        break;
      case goodsTypeEnum.SERVER.value:
        url = '/sub-packages/server-package/pages/serve-detail/index';
        break;
      case goodsTypeEnum.TIME_CARD.value || goodsTypeEnum.CHARGE_CARD.value:
        url = '/sub-packages/server-package/pages/card-detail/index';
        break;
    }

    if (!(url && itemNo)) return false;
    wxApi.$navigateTo({
      url: url,
      data: { itemNo },
    });
  },
  default: function (dataset, context) {
    const nowImgUrl = dataset.src;
    const previewImages = (context.imageUrls || [])
      // 去掉表情图片
      .filter(
        (i) => i !== 'https://mp.weixin.qq.com/mpres/zh_CN/htmledition/comm_htmledition/images/pic/common/pic_blank.gif'
      );

    wxApi.previewImage({
      current: nowImgUrl, // 当前显示图片的http链接
      urls: previewImages, // 需要预览的图片http链接列表
    });
  },
};

export default function wxParseImgService(dataset, context = {}) {
  const handle = imgService[dataset.type] ? imgService[dataset.type] : imgService.default;
  handle(dataset, context);
}
