// 计算 sku 是否禁用
export function calcDisabled(skuTreeList: any[], skuInfoList: any[], skuTreeId: number, treeValId: number): boolean {
  const curTree: Array<{ keyId: number; valId: number }> = skuTreeList.map(({ keyId, treeValList }) => {
    if (skuTreeId === keyId) return { keyId: skuTreeId, valId: treeValId };
    const { valId } = treeValList.find((v) => v.active) || {};
    return { keyId, valId };
  });

  // 没有任何 可选的 sku 组合
  if (curTree.some((c) => !c.valId)) {
    return true;
  }

  const curInfo = skuInfoList.find(({ skuInfoNames }) => {
    return skuInfoNames.every(({ keyId, valId }) => curTree.find((t) => t.keyId === keyId && t.valId === valId));
  });

  return !!curInfo && !!curInfo.notOptional;
}


//匹配选中的sku，找到对应的sku信息
export function gainSelectedSkuItemInfo(skuInfoList: Array<object> , selectSkuValIds: any[]){

  let targetKeyStr = '';
  for (let i = 0; i < selectSkuValIds.length; i++) {
    if (i >= selectSkuValIds.length - 1) {
      targetKeyStr += selectSkuValIds[i].treeValId;
      break;
    }
    targetKeyStr += selectSkuValIds[i].treeValId + '-';
  }

  if (targetKeyStr && skuInfoList && skuInfoList.length > 0) {
    
    for (let i = 0; i < skuInfoList.length; i++) {
      const skuInfoNames = skuInfoList[i].skuInfoNames;
      let keyStr = '';
      for (let j = 0; j < skuInfoNames.length; j++) {
        if (j >= skuInfoNames.length - 1) {
          keyStr += skuInfoNames[j].valId;
          break;
        }
        keyStr += skuInfoNames[j].valId + '-';
      }
      if(keyStr === targetKeyStr){
        return skuInfoList[i];
      }
    }
  }

}