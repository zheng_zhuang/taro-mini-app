
import { defaultLocation } from '@/wxat-common/constants/yqz';
import QQMapWX from '@/sub-packages/map-package/plugins/qqmap-wx-jssdk.min';
// 引入wx-jssdk
const QQMapSecretKey = 'VVPBZ-MIQ6F-QU4JF-NN543-DM4GH-DJBZF'; // 腾讯地图秘钥
const qqMapSdk = new QQMapWX({
  key: QQMapSecretKey,
});

// H5编译地址
const getCompileAddressByH5 = (latitude, longitude) => {
  return new Promise((resolve, reject) => {
    const url = 'https://api.map.baidu.com/reverse_geocoding/v3/'
    wxApi.$jsonp(url, {
        location: `${latitude},${longitude}'`,
        ak: 'tGeEkf70CzM4DEj5fSYPGT07RWqT4Z05',
        output: 'json',
        coordtype: 'wgs84ll'
      }
    )
      .then(res => {
        if (res.result) {
          const { addressComponent } = res.result
          const { district, province, city, adcode } = addressComponent
          const cityCode = adcode.replace(adcode.slice(4), '00')
          const data = {
            district, province, city, cityCode, lat: latitude, lng: longitude
          }
          resolve(data)
        } else {
          reject('格式化地理位置失败！');
        }
      }).catch(err => {
        reject('格式化地理位置失败！');
      })
  })
}

// 根据获取的位置经纬度做省市区转换
export const formatLocation = (latitude, longitude) => {
  if (!(latitude || longitude)) {
    const {cityName, provinceName, cityCode, lat, lng} = defaultLocation
    return new Promise((resolve, reject) => {
      resolve({
        city: cityName,
        province: provinceName,
        cityCode,
        lat,
        lng
      })
    })
  }
  if (process.env.TARO_ENV === 'h5') {
    return getCompileAddressByH5(latitude, longitude)
  }
  return new Promise((resolve, reject) => {
    qqMapSdk.reverseGeocoder({
      // 调用腾讯位置服务初始化对象的reverseGeocoder方法
      location: {
        latitude,
        longitude,
      },
      success: (res) => {
        if (res.result) {
          // 转换省市区成功
          const { city_code, nation_code } = res.result.ad_info;
          const { province, city, district } = res.result.address_component;
          const data = { province, city, district, cityCode: city_code.replace(nation_code, ''), lat: latitude, lng: longitude };
          resolve(data);
        } else {
          reject('格式化地理位置失败！');
        }
      },
      fail: (err) => {
        reject('格式化地理位置失败！');
      },
    });
  });
}