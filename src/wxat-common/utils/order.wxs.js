function getFreightLabel(itemList, expressType, deliveryWay, freightType, defaultLabel) {
  defaultLabel = defaultLabel || '含运费';

  if (expressType != deliveryWay.EXPRESS.value || !itemList || !itemList.length) {
    return defaultLabel;
  } else {
    const allFreightCollect = itemList.every(function (item) {
      return item.freightType === freightType.freightCollect;
    });

    return allFreightCollect ? '运费到付' : defaultLabel;
  }
}

export default {
  getFreightLabel,
};
