import wxApi from './wxApi';

export default {
  getImgRotate(filePath) {
    return new Promise((reject) => {
      wxApi.getImageInfo({
        src: filePath,
        success: (res) => {
          let rotate = 0;
          const sys = wxApi.getSystemInfoSync();
          if (sys.system.startsWith('Android')) {
            switch (res.orientation) {
              case 'right':
                rotate = 90;
                break;
              default:
                rotate = 0;
                break;
            }
          }
          reject(rotate);
        },
      });
    });
  },
};
