// @ts-check
import Taro from '@tarojs/taro';
import wxApi from '../utils/wxApi';
import store from '../../store';
import {updateBaseAction} from '../../redux/base';
import shareUtil from '../utils/share';
import authHelper from '../utils/auth-helper.js';
import { Log, bootRequest, buryInfo, setBuryInfo, getDecorateConfig, getTemplateConfig } from './utils';
import {getRealPath, templateHandler} from './handler';
import {getExtConfig} from './extConfig';
import api from '../api/index.js';
import checkOptions from '../utils/check-options';

/**
 * TODO: 重构
 */
async function wxLocation() {
  try {
    return await wxApi
      .getSetting()
      .then((res) => {
        // if (!res.authSetting['scope.userLocation']) {
        //   return wxApi.authorize({scope: 'scope.userLocation'}).then(
        //     (response) => {
        //       report.reportLocation(true);
        //       return wxApi.getLocation({type: 'gcj02'});
        //     },
        //     (error) => {
        //       report.reportLocation(false);
        //     }
        //   );
        // } else {
        // 超时时间长，影响体验，时间短，精度低
        return wxApi.getLocation({type: 'gcj02'});
        // }
      })
      .then((res) => {
        return {
          longitude: res.longitude,
          latitude: res.latitude,
        };
      })
      .catch((res) => {
        return null;
      });
  } finally {
  }
}

export function clear() {
}

/**
 * 微信小程序登录
 */
export async function login() {
  try {
    // 获取和更新配置
    /**
     * 请求地理位置、第三方授权信息、小程序登录code
     */
    const [location, _, res] = await Promise.all(
      [wxLocation(), getExtConfig(), wxApi.login()]
    );
    return await new Promise((resolve, reject) => {
      const {getState, dispatch} = store;
      const {base, ext} = getState();

      const query = (wxApi.getLaunchOptionsSync() || {}).query;
      let sk = buryInfo.sk || '';

      if (!sk && !!query && !!query.scheme && !!query.sk) {
        sk = query.sk;
      }


      const parameters = {
        //微信登录信息
        code: res.code,
        //商户基础信息
        epId: ext.epId,
        sellerId: ext.sellerId,
        appId: ext.appId,
        wxAppId: ext.maAppId,
        sellerTemplateId: ext.sellerTemplateId,
        // 分享者信息
        refStoreId: buryInfo._ref_storeId || '',
        refUserId: buryInfo._ref_useId || '',
        // 扫二维码进入，部分场景参数信息通过sz映射存储
        shareParamId: sk,
        // 从导购端分享
        shareSource: getState().base.source === 'wxwork' ? 1 : 0
      };

      // 处理门店id
      if (wxApi.getStorageSync('storeId')) {
        parameters.toStoreId = wxApi.getStorageSync('storeId')
      }

      if (location) {
        // 这里拼写错误
        const {longitude: longtitude, latitude} = location;
        // parameters.longtitude = longtitude;
        // parameters.latitude = latitude;
        dispatch(updateBaseAction({gps: {longtitude, latitude}}));
      }
      // 判断扫是从影视过来的，跳到影视落地页
      if (wxApi.getStorageSync('filmQrCodeDetail').authWkAppId) {
        parameters.appId = wxApi.getStorageSync('filmQrCodeDetail').authWkAppId;
      }
      // 旅优选登录接口
      bootRequest(parameters)
        .then((response) => {
          Log('x-login.bootRequest.then', response);
          try {
            // sz映射的分享信息，放入埋点信息 和 邮箱中(给分享的页面初始化使用)
            const {qrCodeScene} = response.data;
            if (!!qrCodeScene) {
              Log('start>loginV2.then.qrCodeScene=', qrCodeScene);
              const qrCodeSceneObject = JSON.parse(qrCodeScene);
              shareUtil.MapperQrCode.sendMapperContent(qrCodeScene);
              setBuryInfo({...qrCodeSceneObject});
            }

            // 将用户id存储到本地
            wxApi.setStorageSync('userId', response.data.userInfo.id);
            wxApi.setStorageSync('userInfo', response.data);
            // 组装数据
            // console.log("old config", response.data)
            // templateHandler(parameters, response.data);
            resolve(response.data.loginInfo);

            let forbidChangeStoreForGuideShare = false;
            const state = getState();
            if (state.base.appInfo) {
              forbidChangeStoreForGuideShare = state.base.appInfo.forbidChangeStoreForGuideShare;
            }

            const hasLogin = state.base.userInfo && state.base.userInfo.id;
            // 未登录 不查询 导购归属门店
            if (hasLogin && state.globalData.environment === "wxwork" && forbidChangeStoreForGuideShare) {
              if (buryInfo._ref_storeId) {
                shareUtil.setStoreId(buryInfo._ref_storeId);
                checkOptions.changeStore(buryInfo._ref_storeId);
              } else {
                const {path} = wxApi.getLaunchOptionsSync();

                // 绑定导购时 不查询归属门店
                if ((path || '').includes('miniprogram-auth')) return;

                wxApi.request({url: api.login.queryGuideStore}).then((res) => {
                  if (res.data && res.data.storeId) {
                    const storeId = res.data.storeId;
                    shareUtil.setStoreId(storeId);
                    // 切换门店
                    if (state.base.currentStore.id !== storeId) {
                      checkOptions.changeStore(storeId);
                    }
                  } else {
                    // 确保不会被 hideLoading 隐藏掉
                    setTimeout(() => Taro.showToast({icon: 'none', title: '没有归属门店'}));
                  }
                }).catch(() => {/* ignore */
                });
              }
            }


            // 任务中心邀约上报 必须推广 && shareId &&类型为新用户 && 任务Id && 任务类型为直播
            if (response.data.vip) {
              if (base.promotion && base.shareId && base.targetType && base.taskReferId && base.taskType == 2) {
                wxApi.navigateTo({
                  url: `plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=${base.taskReferId}`,
                });
              }
            } else {
              if (base.promotion && base.shareId && base.targetType && base.taskReferId && base.taskType == 2) {
                authHelper.checkAuth();
              }
            }

            // 获取商户装修配置

            getTemplateConfig({
              storeId: response.data.storeVO.id,
              apaasStoreId: wxApi.getStorageSync('filmQrCodeDetail').apaasStoreId || '',
              appId: response.data.storeVO.appId,
              isAll: true
            }).then(res => {
              let result = res.data;
              let configData = JSON.parse(result.config)
              let tabbarConfig = {list: []}
              configData.forEach((item, index) => {
                if (item.pageType === 0 || item.pageType === 3) {
                  configData[index].pageType = "custom"
                } else if (item.pageType === 1) {
                  configData[index].pageType = "mine"
                } else if (item.pageType === 10) {
                  configData[index].pageType = "system"
                }
                configData[index].config = item.PageConfig
                delete configData[index].PageConfig
                tabbarConfig.list.push(configData[index])
              })
              let theme = JSON.parse(result.theme)
              let params = {
                ...response.data
              }
              // 数据拼接 适配原数据格式
              // 主页装修
              params.maTemplateConfigDTO.homeConfig = (tabbarConfig.list[0].config)
              // tabbar导航
              let oldConfigFormat = tabbarConfig.list.map((item) => {
                return {
                  ...item,
                  navBackgroundColor: theme.themeColor,
                  navFontColor: theme.contentColor,
                  isAgentOperation: false,
                  able: false,
                  realPath: item.pagePath,
                  pagePath: getRealPath(item.pageType) || item.pagePath,
                  // syncId
                }
              })
              params.maTemplateConfigDTO.tabBarConfig = JSON.stringify({
                list: oldConfigFormat,
                tabbarShow: true,
                selectedColor: theme.themeColor
              })
              // 主题/我的
              let mineConfig = tabbarConfig.list.find(item => item.pageType == "mine")
              params.maTemplateConfigDTO.config = JSON.stringify({
                theme: {
                  sync: true,
                  value: "custom",
                  template: {
                    id: theme.themeMode,
                    navColor: theme.themeColor,
                    bgGradualChange1: theme.themeColor,
                    bgGradualChange2: theme.assistColor,
                    assistColor: theme.assistColor,
                    assistContentColor: theme.assistContentColor,
                    themeColor: theme.themeColor,
                    contentColor: theme.contentColor,
                    topBgColor: theme.topBgColor,
                    topContentColor: theme.topContentColor,
                  }
                },
                mineConfig: mineConfig ? {
                  sync: true,
                  value: JSON.parse(mineConfig.config)
                } : null
              })
              templateHandler(parameters, params);
            })



            // 获取商户装修配置 new

            // getDecorateConfig({
            //   storeId: response.data.storeVO.id,
            //   apaasStoreId: wxApi.getStorageSync('filmQrCodeDetail').apaasStoreId || '',
            //   // storeId: 12062,
            //   appId: response.data.storeVO.appId,
            // }).then(res => {
            //   if (res.data) {
            //     let result = res.data;
            //     let tabbarConfig = JSON.parse(result.tabBarConfig)
            //     let theme = JSON.parse(result.theme)
            //     // console.log("theme", theme)
            //     let params = {
            //       ...response.data
            //     }
            //     // 数据拼接 适配原数据格式
            //     // 主页装修
            //     params.maTemplateConfigDTO.homeConfig = (tabbarConfig.list[0].config)
            //     // tabbar导航
            //     let oldConfigFormat = tabbarConfig.list.map((item) => {
            //       return {
            //         ...item,
            //         navBackgroundColor: theme.themeColor,
            //         navFontColor: theme.contentColor,
            //         isAgentOperation: false,
            //         able: false,
            //         realPath: item.pagePath,
            //         pagePath: getRealPath(item.pageType) || item.pagePath,
            //         // syncId
            //       }
            //     })
            //     params.maTemplateConfigDTO.tabBarConfig = JSON.stringify({
            //       list: oldConfigFormat,
            //       tabbarShow: true,
            //       selectedColor: theme.themeColor
            //     })
            //     // 主题/我的
            //     let mineConfig = tabbarConfig.list.find(item => item.pageType == "mine")
            //     params.maTemplateConfigDTO.config = JSON.stringify({
            //       theme: {
            //         sync: true,
            //         value: "custom",
            //         template: {
            //           id: theme.themeMode,
            //           navColor: theme.themeColor,
            //           bgGradualChange1: theme.themeColor,
            //           bgGradualChange2: theme.assistColor,
            //           assistColor: theme.assistColor,
            //           assistContentColor: theme.assistContentColor,
            //           themeColor: theme.themeColor,
            //           contentColor: theme.contentColor,
            //           topBgColor: theme.topBgColor,
            //           topContentColor: theme.topContentColor,
            //         }
            //       },
            //       mineConfig: mineConfig ? {
            //         sync: true,
            //         value: JSON.parse(mineConfig.config)
            //       } : null
            //     })
            //     templateHandler(parameters, params);
            //     // templateHandlerNew(res.data, response.data);
            //   }
            // })

          } catch (error) {
            reject(error);
          }
        })
        .catch((response) => {
          let msg = '';
          if (!!response.data) {
            if (!!response.data.errorMessage) {
              msg = response.data.errorMessage;
            } else if (!!response.statusCode) {
              msg = 'statusCode:' + response.statusCode;
            }
          }
          if (!msg) {
            msg = response;
          }
          const error = new Error(msg);
          Log('x-login.bootRequest.catch', response, msg);
          reject(error);
        });
    });
  } catch (err) {
    console.error('wechat login failed', err);
    throw err;
  } finally {
  }
}
