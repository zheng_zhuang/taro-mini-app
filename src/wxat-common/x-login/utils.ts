import wxApi from '../utils/wxApi';
import api from "../api";
import shareUtil from '../utils/share';
import store from '../../store';
import {updateBaseAction} from '../../redux/base';
import {wkApi} from '@/sdks/buried/report/index';

export const buryInfo: Record<string, any> = {};

export const report = {
  location: () => {
  },
};

export function Log(...args) {
  // console.log(args, +new Date());
}

/**
 * @param appid 以appid为标记存入
 */
export const setLocalKey = (appid) => ({
  REGISTERED_KEY: `registered_{${appid}}_${process.env.HASH}`,
  SESSIONID_KEY: `sessionId_{${appid}}_${process.env.HASH}`,
  LOGIN_INFO_KEY: `loginInfo_{${appid}}`,
  WX_LOGIN_CODE_KEY: `LOGIN_WX_CODE_{${appid}}`,
});

/**
 * @param expired 以分钟计
 */
export function getCurrentTime(expired) {
  const timezone = 8
  const offsetGMT = new Date().getTimezoneOffset()
  const nowDate = new Date().getTime()
  const targetDate = new Date(nowDate + offsetGMT * 60 * 1000 + timezone * 60 * 60 * 1000).getTime()
  const timestamp = Number(String(targetDate).slice(0, 10))
  if (expired) {
    return (timestamp + expired * 60)
  } else {
    return timestamp
  }
}

/**
 * 更新 sessionId
 * @param id
 */
export function setSessionId(id?: string) {
  store.dispatch(updateBaseAction({sessionId: id}));
}

export function bootRequest(parameters) {

  parameters.longtitude = 113.3172;
  parameters.latitude = 23.08331;
  return wxApi.request({
    quite: true,
    loading: true,
    isLoginRequest: true, // 标识当前请求为login
    url: process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true' ? api.wxoa.login : api.login.loginV2,
    data: parameters,
    header: {},
  });
}

// 获取商户装修信息配置
export function getDecorateConfig(params) {
  return wxApi.request({
    loading: true,
    url: api.login.getDecorateConfig,
    data: params,
  })
}
// 获取商户装修信息配置New
export function getTemplateConfig(params) {
  return wxApi.request({
    loading: true,
    url: api.login.getTemplateConfig,
    method: 'POST',
    data: params,
  })
}


export function setBuryInfo(info) {
  info = info || {};
  for (const key in info) {
    if (shareUtil.allowBuryKeys.hasOwnProperty(key)) {
      buryInfo[key] = info[key];
    }
  }
  const bury = {};
  for (const sKey in buryInfo) {
    // 服务端key，上报时需要转换为埋点key
    if (sKey in shareUtil.serverKeyConvertToEtlKey) bury[shareUtil.serverKeyConvertToEtlKey[sKey]] = buryInfo[sKey];
    else {
      bury[sKey] = buryInfo[sKey];
    }
  }

  wkApi.updateRunTimeBzParam({bury});
}

export function getBuryInfo() {
  return buryInfo;
}
