/**
 * Created by a123 on 2020/11/17.
 * @author trumpli<李志伟>
 */
import store from '../../store/index';
import wxApi from '../utils/wxApi';
import api from '../api/index';
// import {updateBaseAction} from '../../redux/base';
import {updateGlobalDataAction} from '../../redux/global-data';
import updateCurrentStore from './store';
import pageLinkEnum from '../constants/pageLinkEnum';
import { NOOP_ARRAY } from '../utils/noop';
const cache = {
  decorateConfigurationHome: null,
  decorateConfigurationMine: null
};

export function getDecorateHomeData() {
  return cache.decorateConfigurationHome;
}

export function setDecorateHomeData(value: Array<Object | any>) {
  // @ts-ignore
  cache.decorateConfigurationHome = value;
}

export function getDecorateMineData() {
  return cache.decorateConfigurationMine;
}

export function setDecorateMineData(value: Array<Object | any>) {
  // @ts-ignore
  cache.decorateConfigurationMine = value;
}

// 切换门店时处理tabber路径
function normalizeTabbar (list) {

  const newList = (list || NOOP_ARRAY);

  const toTabbarCustom = (list) => {
    list.forEach((item, index) => {
      item.pagePath = `/wxat-common/pages/tabbar-custom${index + 1}/index`;
    });
  };

  // 按照规范，首尾固定为 home 或 mine
  // 但是历史原因，home 可能不是固定的，需要兼容
  // tabbar.length >=2
  if (newList[0].pagePath !== pageLinkEnum.common.home) {
    if (newList.some((i) => i.pagePath === pageLinkEnum.common.home)) {
      // 首页可能不是第一个
      toTabbarCustom(
        newList.filter((i) => i.pagePath !== pageLinkEnum.common.home && i.pagePath !== pageLinkEnum.common.mine)
      );
      // 但是 app.ts 设置的默认首页是 home, 需要进行 switchtab
      // wxApi.switchTab({ url: '/wxat-common/pages/tabbar-custom1/index' });
      return newList;
    } else {
      if (newList.length <= 4) {
        // 没关系，首页不必显示出来,
        toTabbarCustom(newList.slice(0, newList.length - 1));
        // 但是 app.ts 设置的默认首页是 home, 需要进行 switchtab
        // wxApi.switchTab({ url: '/wxat-common/pages/tabbar-custom1/index' });
        return newList;
      } else {
        // 第一个强制修改为首页
        newList[0].pagePath = newList[0].realPath = pageLinkEnum.common.home;
      }
    }
  }
  // 中间调整为 tabbar-custom
  toTabbarCustom(newList.slice(1, newList.length - 1));
  return newList;
}

/**
 * 获取不同门店的首页配置信息
 */
async function getMultiStorePageConfig(storeId) {
  
  const extData = store.getState().ext;
  const params = {
    appId: extData.appId,
    sellerTemplateId: extData.sellerTemplateId,
    storeId
  };
  return new Promise((resolve, reject) => {
    const apiCall = (params) => {
      wxApi
        .request({url: api.getUserConfig, loading: true, data: params})
        .then((res) => {
          
          let tabBarConfig = JSON.parse(res.data.tabBarConfig)
          tabBarConfig.list = normalizeTabbar(tabBarConfig.list)
          store.dispatch(updateGlobalDataAction({
            tabbars: tabBarConfig
          }));
          resolve(res.data);
        })
        .catch((error) => {
          reject(JSON.stringify(error));
        });
    };
    if (!extData.appId || !extData.sellerTemplateId) {
      wxApi.getExtConfig().then((res) => {
        params.appId = res.extConfig.appId;
        params.sellerTemplateId = res.extConfig.sellerTemplateId;
        apiCall(params);
      });
    } else {
      apiCall(params);
    }
  });
}


/**
 * 切换门店->更新首页/个人等动态配置数据
 * @param storeVo 将要切换的门店
 */
export function notifyStoreDecorateSwitch(storeVo) {
  return new Promise(async (resolve, reject) => {
    const storeId = !!storeVo ? storeVo.id : null;
    const {base, globalData} = store.getState();
    const homeChangeTime = globalData.homeChangeTime;
    const canCustomDecorate = !!base.appInfo && !!base.appInfo.canCustomDecorate;
    if (!storeId || !canCustomDecorate) {
      if (storeId)
        updateCurrentStore(storeVo);
      return resolve();
    }
    try {
      // @ts-ignore
      const {config, homeConfig} = await getMultiStorePageConfig(storeId);
      const xConfig = JSON.parse(config);
      //更新首页数据
      setDecorateHomeData(JSON.parse(homeConfig));
      //更新redux
      store.dispatch(updateGlobalDataAction({
        //首页变更
        homeChangeTime: homeChangeTime + 1,
        // 首页
        homeConfig: JSON.parse(homeConfig),
        //个人中心
        mineConfig: xConfig.mineConfig && xConfig.mineConfig.value
      }));
      //更新选中门店
      updateCurrentStore(storeVo);
      return resolve({homeChangeTime: homeChangeTime + 1, homeConfig: JSON.parse(homeConfig), mineConfig: xConfig.mineConfig && xConfig.mineConfig.value});
    } catch (xe) {
      console.log('notifyStoreSwitch::getMultiStorePageConfig exception = ', xe);
      reject(xe);
    }
  });
}

/**
 * 页面刷新->更新首页/个人等动态配置数据
 */
export function notifyStoreDecorateRefresh(storeVo) {
  return new Promise(async (resolve, reject) => {
    const {base, globalData} = store.getState();
    const storeId = !!base.currentStore && base.currentStore.id;
    const homeChangeTime = globalData.homeChangeTime;
    if (!storeId) {
      return resolve();
    }
    try {
      // @ts-ignore
      const {config, homeConfig} = await getMultiStorePageConfig(storeId);
      const xConfig = JSON.parse(config);
      
      //更新首页数据
      setDecorateHomeData(JSON.parse(homeConfig));
      //更新redux
      store.dispatch(updateGlobalDataAction({
        //首页变更
        homeChangeTime: homeChangeTime + 1,
        //个人中心
        mineConfig: xConfig.mineConfig && xConfig.mineConfig.value
      }));
      updateCurrentStore(storeVo);
      return resolve();
    } catch (xe) {
      console.log('notifyStoreSwitch::getMultiStorePageConfig exception = ', xe);
      reject(xe);
    }
  });
}
