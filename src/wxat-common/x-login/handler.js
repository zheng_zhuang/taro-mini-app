import store from '../../store';
import businessTime from '../utils/businessTime.js';
import industryEnum from '../constants/industryEnum.js';
import shareUtil from '../utils/share.js';
import wxApi from '@/wxat-common/utils/wxApi';
import {updateBaseAction} from '../../redux/base';
import {updateGlobalDataAction} from '../../redux/global-data';
import updateCurrentStore from './store';
import {NOOP_ARRAY} from '../utils/noop';
import pageLinkEnum from '../constants/pageLinkEnum';
import {setDecorateHomeData} from './decorate-configuration-store'

function normalizeTabbar(list) {

  const newList = (list || NOOP_ARRAY);


  const toTabbarCustom = (list) => {
    list.forEach((item, index) => {
      item.pagePath = `/wxat-common/pages/tabbar-custom${index + 1}/index`;
      // item.pagePath = `/wxat-common/pages/tabbar-custom-new/index`;
    });
  };

  // #新逻辑
  toTabbarCustom(
    newList.filter((i) => i.pagePath !== pageLinkEnum.common.home && i.pagePath !== pageLinkEnum.common.mine)
  );

  newList[0].pagePath = newList[0].realPath = pageLinkEnum.common.home;

  return newList

  // #旧逻辑
  // 按照规范，首尾固定为 home 或 mine
  // 但是历史原因，home 可能不是固定的，需要兼容
  // tabbar.length >=2
  // if (newList[0].pagePath !== pageLinkEnum.common.home) {
  //   if (newList.some((i) => i.pagePath === pageLinkEnum.common.home)) {
  //     // 首页可能不是第一个
  //     toTabbarCustom(
  //       newList.filter((i) => i.pagePath !== pageLinkEnum.common.home && i.pagePath !== pageLinkEnum.common.mine)
  //     );
  //     // 但是 app.ts 设置的默认首页是 home, 需要进行 switchtab
  //     wxApi.switchTab({url: '/wxat-common/pages/tabbar-custom1/index'});
  //     return newList;
  //   } else {
  //     if (newList.length <= 4) {
  //       // 没关系，首页不必显示出来,
  //       toTabbarCustom(newList.slice(0, newList.length - 1));
  //       // 但是 app.ts 设置的默认首页是 home, 需要进行 switchtab
  //       wxApi.switchTab({url: '/wxat-common/pages/tabbar-custom1/index'});
  //       return newList;
  //     } else {
  //       // 第一个强制修改为首页
  //       newList[0].pagePath = newList[0].realPath = pageLinkEnum.common.home;
  //     }
  //   }
}

// 中间调整为 tabbar-custom
// toTabbarCustom(newList.slice(1, newList.length - 1));
// return newList;
// }

/**
 * fix oldest tabBar config
 * @param tabbars
 */
function fixOldVersionTabBarPath(tabbars) {
  const list = tabbars.list;

  // 兼容老版本首页
  const customerHome = list[0];
  if (customerHome.pagePath === 'pages/home/index') {
    customerHome.pagePath = 'wxat-common/pages/home/index';
  }

  // 兼容老版本我的页面
  const minePage = list[list.length - 1];
  if (minePage.pagePath === 'pages/mine/index') {
    minePage.pagePath = 'wxat-common/pages/mine/index';
  }

  // list.forEach(item => {
  //   if (item.pagePath.indexOf('pages/custom/index?route=') === 0) {
  //     item.pagePath = 'wxat-common/' + item.pagePath
  //   }
  // })

}

// 新装修数据 页面路径适配
export function getRealPath(type) {
  if (type == "mine") {
    return "wxat-common/pages/mine/index"
  } else if (type == "custom") {
    return "wxat-common/pages/tabbar-custom/index"
  } else {
    return null
  }
}

export function templateHandler(parameters, response, scanId) {
  const {scoreAlias, maTemplateConfigDTO} = response;
  const {tabBarConfig, config, homeConfig} = maTemplateConfigDTO;
  const {getState, dispatch} = store;
  const globalData = getState().globalData;
  globalData.scoreName = scoreAlias || '积分';

  let _updateGlobalData = {};
  /**
   * Tabbar 转换
   */
  let tabbars;
  // tabBar
  if (tabBarConfig) {
    // 数据拆分过
    tabbars = JSON.parse(tabBarConfig);
  } else {
    tabbars = JSON.parse(config).tabBar;
  }
  // #旧逻辑 兼容老版本的tab数据
  // fixOldVersionTabBarPath(tabbars);

  // 让tabbar 尽快显示出来
  dispatch(updateGlobalDataAction({tabbars}));

  // redux 应该作为不可变数据，这样组件才能监听到变动
  tabbars = {...tabbars};

  // #旧逻辑
  tabbars.list = normalizeTabbar(tabbars.list);

  // 首页装修
  setDecorateHomeData(JSON.parse(homeConfig || '[]'));

  // 自定义页装修


  // 主题色配置信息
  if (config) {
    const xConfig = JSON.parse(config);
    if (xConfig) {
      const themeConfig = xConfig.theme;
      const mineConfig = xConfig.mineConfig && xConfig.mineConfig.value;
      _updateGlobalData = {
        homeChangeTime: globalData.homeChangeTime + 1,
        tabbars,
        themeConfig,
        mineConfig,
      };
    }
  } else {
    _updateGlobalData = {
      homeChangeTime: globalData.homeChangeTime + 1,
      tabbars
    };
  }

  // 原来的登录信息，currentStore是从选择门店合并进来的
  const {
    sessionId, // 登录sessionId
    vip, // 是否vip
    appInfo, // app信息
    userInfo, // 用户信息
    loginInfo, // 登录信息
    storeVO, // 当前选中门店
    stores, // 有多个门店可供选择时，当前可供选择的门店列表
  } = response;

  // console.log("storeVOstoreVO", storeVO)

  shareUtil.setUseId(userInfo.id);
  shareUtil.setOpenId(userInfo.wxOpenId);

  storeVO.businessTime = businessTime.computed(
    storeVO.businessStartHour,
    storeVO.businessEndHour,
    storeVO.businessDayOfWeek
  );

  const update = {
    userInfo: userInfo,
    loginInfo: loginInfo,
    appId: parameters.appId,
    vip: vip,
    pendingChooseStoreList: stores,
    appInfo: appInfo,
    registered: vip,
    tabbarReady: true,
    storeVO: storeVO,
  };

  // 已经注册过会员，不需要从微信那里获取用户信息
  if (vip) {
    update.wxUserInfo = {
      avatarUrl: userInfo.avatarImgUrl,
      city: userInfo.city,
      country: userInfo.country,
      gender: userInfo.gender,
      province: userInfo.province,
      nickName: userInfo.nickname,
    };
  }

  if (appInfo) {
    const specIndustry = industryEnum.findIndustryById(appInfo.industryId);
    if (specIndustry) {
      _updateGlobalData.industry = specIndustry.value;
    }
  }

  // fixme 该赋值需要放在最后，因为首页会监听sessionid的变化
  update.sessionId = sessionId;
  dispatch(updateBaseAction(update));
  dispatch(updateGlobalDataAction(_updateGlobalData));
  updateCurrentStore(storeVO, scanId);
}

/**
 * 处理异步获取的门店信息
 * @param {*} data
 */
export function storeHandler(data) {
  const {dispatch} = store;
  const {storeVO, stores} = data;

  if (storeVO) {
    storeVO.businessTime = businessTime.computed(
      storeVO.businessStartHour,
      storeVO.businessEndHour,
      storeVO.businessDayOfWeek
    );

    updateCurrentStore(storeVO);
  }

  dispatch(
    updateBaseAction({
      pendingChooseStoreList: stores,
    })
  );
}
