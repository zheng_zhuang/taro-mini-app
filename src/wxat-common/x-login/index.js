// @ts-check
import store from '../../store';
import { updateBaseAction } from '../../redux/base';
import * as utils from './utils';
import * as extConfig from './extConfig';
import wxApi from '../utils/wxApi';
import { login as letsLogin, clear as letsclear } from './login';
import Taro from '@tarojs/taro';

/**
 * 全局事件
 * 通过 Taro.eventCenter 可以监听到这些时间
 */
const EVENT_LOGINING = 'logining';
const EVENT_RELOGINING = 'reLogining';
const EVENT_LOGIN_SETUP = 'loginSetup';
const EVENT_LOGIN_SUCCESS = 'loginingSuccess';
const EVENT_LOGIN_FAILED = 'loginingFailed';

// 判断当前是否正在登录
let logining = false;
// 正在重新登录的流程中
let relogining = false;
// 初始化登录，只有初始化登录后，其他接口才能发起请求
let initialLogined = false;

/**
 * @typedef {Array<{resolve: Function, reject: Function}>} Queue
 */

/**
 * 登录请求等待队列
 * @type {Queue}
 */
let loginReqWaitList = [];

/**
 * @param {() => Queue} getQueue
 */
function pushQueue (getQueue) {
  return () => {
    return new Promise((res, rej) => {
      getQueue().push({ resolve: res, reject: rej });
    });
  };
}

/**
 * @param {() => Queue} getQueue
 */
function flushQueue (getQueue) {
  return (error, value) => {
    const queue = getQueue();
    if (error) {
      queue.forEach((i) => i.reject(error));
    } else {
      queue.forEach((i) => i.resolve(value));
    }
  };
}

const pushLoginQueue = pushQueue(() => loginReqWaitList);
const flushLoginQueue = flushQueue(() => {
  const queue = loginReqWaitList;
  loginReqWaitList = [];
  return queue;
});

function getLoginInfo () {
  const base = store.getState().base;
  return base && base.loginInfo;
}

function setLoginInfo (value) {
  if (value) {
    Taro.getApp().sensors.registerApp({
      open_id: value?.openId,
      phone: value?.phone,
      union_id: value?.unionId,
      user_id: value?.userId,
      store_id: value?.storeId,
      wx_app_id: value?.wxAppId,
    })
  }
  store.dispatch(updateBaseAction({ loginInfo: value }));
}

/**
 * 登录
 * @param {{
 *   initialLogin?: boolean // 初始化登录
 *   ignoreLoginInfo?: boolean // 忽略已有的 loginInfo
 * }} [options]
 */
async function login (options) {
  const { initialLogin, ignoreLoginInfo = false } = options || {};
  const loginInfo = getLoginInfo();

  // 已经有缓存信息并且不需要重新登录
  if (loginInfo && !ignoreLoginInfo) {
    return loginInfo;
  } else if (logining || (!initialLogin && !initialLogined)) { // 调用缓存的请求队列
    return pushLoginQueue();
  } else { // 登录处理
    try {
      logining = true;
      wxApi.eventCenter.trigger(EVENT_LOGINING);
      const loginInfo = await letsLogin();

      setLoginInfo(loginInfo);

      wxApi.eventCenter.trigger(EVENT_LOGIN_SUCCESS, loginInfo);
      // 初始化登录成功
      if (initialLogin) {
        wxApi.eventCenter.trigger(EVENT_LOGIN_SETUP, loginInfo);
        initialLogined = true;
      }

      flushLoginQueue(null, loginInfo);
      return loginInfo
    } catch (err) {
      setLoginInfo(null);
      wxApi.eventCenter.trigger(EVENT_LOGIN_FAILED, err);
      if (!initialLogin) {
        flushLoginQueue(err);
      }
      throw err;
    } finally {
      logining = false;
    }
  }
}

async function waitLogin () {
  const loginInfo = getLoginInfo();
  if (loginInfo) {
    return;
  }
  return pushLoginQueue();
}

/**
 * 清除登录信息
 */
function clear () {
  setLoginInfo(null);
  letsclear();
}

/**
 * 重新登录
 */
async function reLogin () {
  // 登录的过程中出现了会话失效
  // 在恢复缓存的会话时可能出现这种情况
  // 直接抛出错误，让登录流程强制重新授权。具体见 login.h5.js 的实现
  if (logining && !relogining) {
    clear();
    throw new Error('会话失效, 请重新登录');
  }

  // 应用未登录
  if (!initialLogined || relogining) {
    return pushLoginQueue();
  }

  try {
    relogining = true;

    clear();
    wxApi.eventCenter.trigger(EVENT_RELOGINING);
    return await login();
  } finally {
    relogining = false;
  }
}

export default {
  login,
  reLogin,
  waitLogin,
  clear,
  EVENT_LOGINING,
  EVENT_RELOGINING,
  EVENT_LOGIN_SUCCESS,
  EVENT_LOGIN_SETUP,
  EVENT_LOGIN_FAILED,
  ...utils,
  ...extConfig,
};
