import { updateExtAction } from '@/redux/ext';
import store from '@/store';
import wxApi from '../utils/wxApi';
import extJson from '../../../ext.json'
export interface ExtConfig {
  appId: number;
  epId: number;
  maAppId: string;
  sellerId: number;
  sellerTemplateId: number;
}

/**
 * 获取配置信息
 * @param force 强制更新
 */
// eslint-disable-next-line import/prefer-default-export
export async function getExtConfig(force = true): Promise<ExtConfig> {
  if (!force) {
    const { ext } = store.getState();
    if (ext) {
      return ext;
    }
  }
  try {
    let extConfig = {}
    if (process.env.NODE_ENV === 'development') {
      extConfig = extJson.ext
    } else {
      extConfig = (await wxApi.getExtConfig()).extConfig;
      // 默认模板
      if (extConfig == null || !('appId' in extConfig)) {
        extConfig = {
          "appId": 921,
          "epId": 2920,
          "maAppId": "wx052489e57286c518",
          "maAppName": "品牌商",
          "sellerId": 3145,
          "sellerTemplateId": 711
        };
      }
    }

    store.dispatch(updateExtAction(extConfig));

    return extConfig as ExtConfig;
  } finally {
  }
}
