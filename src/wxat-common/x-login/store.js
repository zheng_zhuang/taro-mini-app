import wxApi from '@/wxat-common/utils/wxApi';
import Taro from '@tarojs/taro';
// import base from './base.js'
import store from '@/store';
import base from '@/state';
import { updateBaseAction } from '@/redux/base';
import { updateGlobalDataAction } from '@/redux/global-data'
import shareUtil from '@/wxat-common/utils/share.js';
import report, { wkApi } from '@/sdks/buried/report/index';
import api from '../api/index';

/**
 * 全部店铺的id为-999
 * @type {number}
 */
const config = {
  oldStoreId: -1,
};

//raf
const raf = () => {
  if (typeof requestAnimationFrame === 'undefined') {
    return function (callback) {
      setTimeout(function () {
        callback();
      }, 17);
    };
  }
  return requestAnimationFrame;
};

const updateCurrentStore = (newStore, scanId) => {
  const { getState, dispatch } = store;
  const { base } = getState();

  const { oldStoreId } = config;
  const change = !!newStore.id && newStore.id !== oldStoreId;
  if (!change) {
    return;
  }

  //初次直接渲染
  const newUserInfo = { ...base.userInfo, storeId: newStore.id };
  if (oldStoreId === -1) {
    dispatch(
      updateBaseAction({
        currentStore: newStore,
        userInfo: newUserInfo,
        currentStoreViewVisible: true,
      })
    );
  } else if (oldStoreId !== -1 && !scanId) {
    // 添加判断条件,扫码进入不重复渲染
    //再次，间隔渲染
    dispatch(
      updateBaseAction({
        currentStore: newStore,
        userInfo: newUserInfo,
        currentStoreViewVisible: false,
      })
    );

    raf()(() => {
      dispatch(
        updateBaseAction({
          currentStoreViewVisible: true,
        })
      );
    });
  }

  dispatch(updateGlobalDataAction({ currentStore: newStore, sessionId: base.sessionId }))

  config.oldStoreId = newStore.id;
  shareUtil.setStoreId(newStore.id);
  wkApi.updateRunTimeBzParam({ store: newStore.id });

  report.user(base.loginInfo, newUserInfo);

  if (base.loginInfo) {
    const userPhone = base.loginInfo.phone;
    if (userPhone) {
      !!api.login.queryChargeOff && wxApi
        .request({
          url: api.login.queryChargeOff,
          loading: false,
          data: { userPhone },
        })
        .then((res) => {
          store.dispatch(updateBaseAction({ chargeOff: res.data }));
        });
    } else {
      store.dispatch(updateBaseAction({ chargeOff: false }));
    }
  }

  if (newStore) {
    Taro.getApp().sensors.registerApp({
      store_id: newStore?.id,
      store_name: newStore?.name
    })
  }
};

export default updateCurrentStore;
