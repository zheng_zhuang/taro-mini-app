// @ts-check
import store from '../../store';
import {updateBaseAction} from '../../redux/base';
import wxApi from '../utils/wxApi';
import {getParams, authGuide} from '../utils/platform/official-account.h5';
import {bootRequest, Log, setSessionId, setLocalKey, getDecorateConfig} from './utils';
import {getRealPath, templateHandler} from './handler';
import {getExtConfig} from './extConfig';
import {delay} from '../utils/promise';
import wxAppProxy from '../utils/wxAppProxy';
import api from '../api';
import Taro from "@tarojs/taro";
// import {login as mockLogin, mockData, mockExtConfig} from './mock-login'

const {getState, dispatch} = store;

const hash = process.env.TARO_ENV == 'h5' ? window.location.hash : Taro.getCurrentInstance().router.path;

/**
 * 引导用户注册
 * 在注册页面中调用该方法
 * 引导获取 userInfo, 页面或许新的 session 后后退回原来的页面
 */
export async function WXOARegister() {
  // 清理用户信息, 重新登录刷新 session
  clear();

  const url = authGuide('register');
  window.location.replace(url);
}

/**
 * 注册是否引导过了?
 */
export function isRegistered() {
  const params = getParams();
  const keys = setLocalKey(params.appid);
  return window.sessionStorage.getItem(keys.REGISTERED_KEY) === '1';
}

/**
 * 登陆前拿到当前定位获取最近店铺
 */
async function fetchLocation() {

  return wxApi.$getLocation().then(
    function (resData) {
      const {lng, lat} = resData.point;
      dispatch(updateBaseAction({gps: {longtitude: lng, latitude: lat}}));
    },
    function (err) {
    }
  );
}

/**
 * HTML5 公众号登录
 *
 * 要点:
 * - 用户会携带 appId + component_appId 打开页面
 * - sessionId 会持久化
 * - 会话失效或首次登录需要引导用于授权
 *  - 示例URL https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf15f463a14dcd698&redirect_uri=http%3A%2F%2Fcn-fs-dx.sakurafrp.com%3A12170%2F&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect
 */

/**
 * H5调试：授权跳转至oauthUrl。域名替换为本地。获取url的code进行微信授权登录
 */
async function WXOALogin() {
  // getExtConfig 请求
  const config = await getExtConfig();
  const params = getParams();


  if (params.appid == null) {
    console.error('登录失败：appId 为空');
    throw new Error(`登录失败：appId 为空`);
  }

  // TODO: 暂时未考虑分享场景
  const parameters = {
    code: params.code,
    epId: config.epId,
    appId: config.appId,
    wxMpAppId: params.appid,
    sellerId: config.sellerId,
    sellerTemplateId: config.sellerTemplateId,
  };
  const {longtitude, latitude} = store.getState().base.gps;
  parameters.longtitude = longtitude;
  parameters.latitude = latitude;
  const keys = setLocalKey(params.appid);
  const SESSIONID_KEY = keys.SESSIONID_KEY;
  const LOGIN_INFO_KEY = keys.LOGIN_INFO_KEY;
  const REGISTERED_KEY = keys.REGISTERED_KEY;
  const WX_LOGIN_CODE_KEY = keys.WX_LOGIN_CODE_KEY;

  const handleResponse = async (response, isRecovery) => {
    const {longtitude, latitude} = store.getState().base.gps;
    if ((process.env.TARO_ENV === 'h5' || process.env.WX_OA === 'true') && (!longtitude && !latitude)) {
      await fetchLocation();
    }
    // 将 sessionId 写入 cookie, 进行授权
    const responseData = response.data;
    const sessionId = responseData.sessionId;
    document.cookie = `sessionId=${sessionId};path=/;domain=${window.location.hostname}`;
    // 非会话恢复，需要进行一些持久化等操作
    if (!isRecovery) {
      // 缓存
      wxApi.setStorage({key: SESSIONID_KEY, data: sessionId});
      wxApi.setStorage({key: LOGIN_INFO_KEY, data: response});

      // 路由恢复
      if (params.state === 'register') {
        // 后退到注册页面
        window.sessionStorage.setItem(REGISTERED_KEY, '1');
      }
    }
    const scanId = hash.indexOf('scanId') != -1
    // templateHandler(parameters, responseData, scanId);
    if (!hash) {
      if (params.route) {
        wxApi.redirectTo({url: params.route});
      } else {
        wxApi.redirectTo({url: '/wxat-common/pages/home/index'});
      }
    }
    if (!isRecovery) {
      wxApi.setStorage({key: LOGIN_INFO_KEY, data: response});
    }

    // 获取商户装修配置 new
    getDecorateConfig({
      storeId: response.data.storeVO.id,
      // storeId: 12062,
      appId: response.data.storeVO.appId,
    }).then(res => {
      if (res.data) {
        let result = res.data;
        let tabbarConfig = JSON.parse(result.tabBarConfig)
        let theme = JSON.parse(result.theme)
        let params = {
          ...response.data
        }
        // 数据拼接 适配原数据格式
        // 主页装修
        params.maTemplateConfigDTO.homeConfig = (tabbarConfig.list[0].config)
        // tabbar导航
        let oldConfigFormat = tabbarConfig.list.map((item) => {
          return {
            ...item,
            navBackgroundColor: theme.themeColor,
            navFontColor: theme.contentColor,
            isAgentOperation: false,
            able: false,
            realPath: item.pagePath,
            pagePath: getRealPath(item.pageType) || item.pagePath,
            // syncId
          }
        })
        params.maTemplateConfigDTO.tabBarConfig = JSON.stringify({
          list: oldConfigFormat,
          tabbarShow: true,
          selectedColor: theme.themeColor
        })
        // 主题/我的
        // console.log("new config", tabbarConfig.list)
        let mineConfig = tabbarConfig.list.find(item => item.pageType == "mine")
        params.maTemplateConfigDTO.config = JSON.stringify({
          theme: {
            sync: true,
            value: "v2",
            template: {
              id: theme.themeMode,
              navColor: theme.themeColor,
              bgGradualChange1: theme.themeColor,
              bgGradualChange2: theme.assistColor
            }
          },
          mineConfig: mineConfig ? {
            sync: true,
            value: JSON.parse(mineConfig.config)
          } : null
        })
        templateHandler(parameters, params, scanId);
        // templateHandlerNew(res.data, response.data);
      }
    })


  };

  const sessionId = wxApi.getStorageSync(SESSIONID_KEY) || wxApi.getStorageSync(`sessionId_${config.appId}`);
  let forceRelogin = false;

  // 会话恢复
  if (sessionId) {
    // 提前保存 sessionId，因为登录过程中立即会有接口请求
    setSessionId(sessionId);
    const response = wxApi.getStorageSync(LOGIN_INFO_KEY);

    try {
      // 刷新首页配置
      const res = await wxAppProxy.getHomeConfig(parameters);

      const {
        data: loginUserInfo = {},
      } = await getLoginUserInfo();

      const {userInfo = {}, appInfo = {}} = loginUserInfo;
      const data = {
        vip: !!userInfo,
        userInfo: userInfo
      }
      wxApi.setStorage({key: LOGIN_INFO_KEY, data: data});
      Object.assign(response.data, {
        maTemplateConfigDTO: res.data,
        userInfo,
        appInfo,
        data
        // vip,
      });
      wxApi.setStorage({key: LOGIN_INFO_KEY, data: response});
      await handleResponse(response, true);
      wxApi.setStorageSync('userId', response.data.loginInfo.userId)
      wxApi.setStorageSync('userInfo', loginUserInfo)
      return response.data.loginInfo;
    } catch (err) {
      forceRelogin = true;
    }
  }

  window.sessionStorage.setItem(WX_LOGIN_CODE_KEY, params.code);

  // 授权后的跳转
  // 如果存在 hash 则说明 code 已经被使用过了
  if (!forceRelogin && params.code && params.code === window.sessionStorage.getItem(WX_LOGIN_CODE_KEY)) {
    try {
      Log('logining', parameters);
      if (wxApi.getStorageSync("scan_storeId")) {
        parameters.toStoreId = wxApi.getStorageSync("scan_storeId")
      }
      // 缓存 code，只能使用一次
      const response = await bootRequest(parameters);
      await handleResponse(response, false);
      wxApi.setStorageSync('userId', response.data.loginInfo.userId);
      wxApi.setStorageSync('userInfo', response.data);
      return response.data.loginInfo;
    } catch (err) {
      let msg = '';
      if (err.data) {
        if (err.data.errorMessage) {
          msg = err.data.errorMessage;
        } else if (err.statusCode) {
          msg = 'statusCode:' + err.statusCode;
        }
      }
      if (!msg) {
        msg = err.message || err;
      }
      const error = new Error(msg);

      const errMsg = JSON.parse(msg);
      // 登录后报code 已使用，重新授权一下
      if (errMsg && (errMsg.errcode === 40163 || errMsg.errcode === 40029)) {
        const url = authGuide('login', params.route);
        window.location.replace(url);
        await delay(100000);
        return;
      }

      Log('x-login.bootRequest.catch', err, msg);

      throw error;
    }
  } else {
    if (process.env.NODE_ENV === 'development') {
      const count = window.sessionStorage.getItem('loginCount');
      // if (count && parseInt(count) > 3) {
      //   throw new Error('登录次数超出限额');
      // }
      window.sessionStorage.setItem('loginCount', count ? `${parseInt(count) + 1}` : '0');
    }

    const url = authGuide('login', params.route);
    // debugger

    window.location.replace(url);
    await delay(100000);
  }
}

async function getLoginUserInfo() {
  return await wxApi.request({
    url: api.wxoa.getLoginUserInfo,
    loading: false,
    data: {},
  });
}

// 获取用户信息
export function getUserInfo() {
  wxApi.request({
    url: api.wxoa.getLoginUserInfo,
    loading: false,
    data: {},
  }).then(data => {
    wxApi.setStorageSync("userInfo", data.data);
  });
}

// 登录态清理
export function clear() {
  if (process.env.WX_OA === 'true') {
    const params = getParams();
    const keys = setLocalKey(params.appid);
    if (params.appid) {
      const SESSIONID_KEY = keys.SESSIONID_KEY;
      const LOGIN_INFO_KEY = keys.LOGIN_INFO_KEY;

      wxApi.removeStorageSync(SESSIONID_KEY);
      wxApi.removeStorageSync(LOGIN_INFO_KEY);
    }
  }
}

export function login() {
  if (process.env.TARO_ENV === 'h5' || process.env.WX_OA === 'true') {
    // return mockLogin()
    return WXOALogin();
  } else {
    throw new Error('登录失败')
  }

}
