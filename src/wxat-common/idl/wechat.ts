/**
 * @desc 微信授权信息结构体
 */
export interface IdlWeChatAuthProfile {
  userInfo?: IdlWeChatAuthUserInfo,
  errMsg?: string, //错误信息
  signature?: string,
  encryptData?: string,//加密信息
  iv?: string,//盐值
}

/**
 * @desc 微信授权信息中的 用户信息
 */
export interface IdlWeChatAuthUserInfo {
  avatarUrl?: string,
  nickName?: string,
  city?: string,
  country?: string,
  province?: string,
  gender?: string,
  language?: string
}

/**
 * @desc 微信授权信息中的 加密信息
 */
export interface IdlWeChatEncryptedInfo  {
  iv?: string,
  encryptData?: string
}
