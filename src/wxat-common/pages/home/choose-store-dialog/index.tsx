import React, { useEffect, FC, useRef } from 'react'; // @externalClassesConvered(AnimatDialog)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import './index.scss';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import { notifyStoreDecorateSwitch } from '@/wxat-common/x-login/decorate-configuration-store';
import { qs2obj } from '@/wxat-common/utils/query-string';

import getStaticImgUrl from '../../../constants/frontEndImgUrl'
const storeLabelType = {
  current: {
    type: 4,
    label: '当前门店',
  },

  lastGo: {
    type: 3,
    label: '去过该店',
  },

  nearest: {
    type: 2,
    label: '较近门店',
  },
};

// props的类型定义
export interface ChooseStoreDialogProps {
  onConfirmed: () => void;
}

/**
 * 选择门店弹窗
 * @param props
 */
const ChooseStoreDialog: FC<ChooseStoreDialogProps> = (props) => {
  const { onConfirmed } = props;
  const pendingChooseStoreList = useSelector((state) => state.base.pendingChooseStoreList);
  const tmpStyle = useTemplateStyle();
  const chooseStoreDialog = useRef<AnimatDialog>(null);

  const show = () => {
    if (chooseStoreDialog.current)
      chooseStoreDialog.current.show({
        scale: 1,
      });
  };

  const hide = () => {
    if (chooseStoreDialog.current) chooseStoreDialog.current.hide(true);
  };

  const onCancelClick = () => {
    hide();
    onConfirmed();
  };

  const handleChooseStore = (e) => {
    const chooseStore = e.currentTarget.dataset.store;
    wxApi
      .request({
        url: api.store.choose + '?storeId=' + chooseStore.id,
        loading: true,
      })
      .then(async () => {
        hide();
        await notifyStoreDecorateSwitch(chooseStore);
        onConfirmed();
      });
  };

  const handleToStoreList = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/store-list/index',
    });

    hide();
    onConfirmed();
  };

  const { forbid, source, environment } = useSelector((state: any) => {
    return {
      forbid: (state.base.appInfo || {}).forbidChangeStoreForGuideShare,
      source: state.base.source,
      environment: state.globalData.environment,
    };
  });

  useEffect(() => {
    let forbidChangeStore = forbid && (source === 'wxwork' || environment === 'wxwork');
    const query = wxApi.getLaunchOptionsSync().query || {};
    const options = qs2obj(query.scene || '');
    if (+options.force) {
      forbidChangeStore = true;
    }

    if (pendingChooseStoreList && pendingChooseStoreList.length > 1 && !forbidChangeStore) {
      show();
    } else {
      onConfirmed();
    }
  }, [pendingChooseStoreList]);

  return (
    <View
      data-fixme='03 add view wrapper. need more test'
      data-scoped='wk-wcc-ChooseStoreDialog'
      className='wk-wcc-ChooseStoreDialog'
    >
      <AnimatDialog
        ref={chooseStoreDialog}
        clickMaskHide={false}
        animClass='choose-store-dialog'
        modalClass='choose-store-modal'
      >
        <View className='choose-store-title'>请选择进入门店</View>
        <View className='store-item-wrap'>
          {!!pendingChooseStoreList &&
            pendingChooseStoreList.map((item, index) => {
              return (
                <View
                  className='store-item'
                  key={'pendingChooseStoreList' + index}
                  onClick={_fixme_with_dataset_(handleChooseStore, { store: item })}
                >
                  <Image className='store-location-icon' src={getStaticImgUrl.images.ic_location_dark_png} />
                  <View className='store-info-box'>
                    <View className='store-info-wrapper'>
                      <View className='store-name limit-line'>{item.name}</View>
                      {item.type === storeLabelType.current.type && (
                        <View
                          className='store-label'
                          style={_safe_style_(`border: 1px solid ${tmpStyle.btnColor};color:${tmpStyle.btnColor};`)}
                        >
                          {storeLabelType.current.label}
                        </View>
                      )}

                      {item.type === storeLabelType.lastGo.type && (
                        <View
                          className='store-label'
                          style={_safe_style_(`border: 1px solid ${tmpStyle.btnColor};color:${tmpStyle.btnColor};`)}
                        >
                          {storeLabelType.lastGo.label}
                        </View>
                      )}

                      {item.type === storeLabelType.nearest.type && (
                        <View
                          className='store-label'
                          style={_safe_style_(`border: 1px solid ${tmpStyle.btnColor};color:${tmpStyle.btnColor};`)}
                        >
                          {storeLabelType.nearest.label}
                        </View>
                      )}
                    </View>
                    <View className='store-info-wrapper'>
                      <View className='store-address limit-line'>{item.address}</View>
                    </View>
                  </View>
                  <View
                    className='store-choose-btn'
                    style={_safe_style_(`background:${tmpStyle.btnColor};`)}
                    onClick={_fixme_with_dataset_(handleChooseStore, { store: item })}
                  >
                    去下单
                  </View>
                </View>
              );
            })}
        </View>
        <View className='more-store-box' onClick={handleToStoreList}>
          <View>查看更多</View>
          <Image className='more-icon' src={getStaticImgUrl.images.rightIcon_png} />
        </View>
        <Image src={getStaticImgUrl.images.ic_del_png} className='cancel-icon' onClick={onCancelClick} />
      </AnimatDialog>
    </View>
  );
};

// 给props赋默认值
ChooseStoreDialog.defaultProps = {};

export default ChooseStoreDialog;
