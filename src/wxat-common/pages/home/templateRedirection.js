import wxApi from '../../utils/wxApi';
import protectedMailBox from '../../utils/protectedMailBox.js';
import pageLinkEnum from '../../constants/pageLinkEnum.js';

export default {
  /**
   * 判断是否从模板消息进入页面，是否需要重定向页面
   * @param {*} options
   */
  jumpPage(options) {
    let redirectUrl = null; // 重定向页面链接
    let redirectDataParamsName = null; // 重定向时自定义传参参数名
    let redirectDataParams = {}; // 重定向传参参数

    if (!!options.orderNo) {
      redirectUrl = pageLinkEnum.orderPkg.orderDetail; // 订单详情页面
      redirectDataParams = {
        orderNo: options.orderNo, // 订单号
      };
    } else if (!!options.refundOrderNo) {
      redirectUrl = pageLinkEnum.orderPkg.afterSale.refundDetail; // 退款详情页面
      redirectDataParamsName = 'refundDetailInfo';
      redirectDataParams = {
        refundOrderNo: options.refundOrderNo, // 退款订单号
      };
    } else if (!!options.groupNo) {
      redirectUrl = 'sub-packages/marketing-package/pages/group/join/index'; // 参团详情页面
      redirectDataParams = {
        groupNo: options.groupNo, // 拼团单号
      };
    } else if (!!options.cutPrice) {
      redirectUrl = 'sub-packages/mine-package/pages/cut-price/index'; // 我的砍价页面
    } else if (!!options.redPacketNo) {
      redirectUrl = 'sub-packages/marketing-package/pages/red-packet/index'; // 拆红包页面
      redirectDataParams = {
        redPacketNo: options.redPacketNo, // 拆红包单号
      };
    } else if (!!options.accountBalance) {
      redirectUrl = 'sub-packages/mine-package/pages/account-balance/index'; // 账户余额页面
    } else if (!!options.giftGard) {
      redirectUrl = 'sub-packages/marketing-package/pages/gift-card/mine/index'; // 礼品卡列表页面
    } else if (!!options.appointmentOrderNo) {
      redirectUrl = 'sub-packages/server-package/pages/appointment/details/index'; // 预约详情页面
      redirectDataParams = {
        orderNo: options.appointmentOrderNo, // 预约订单号
      };
    }

    if (!!redirectUrl) {
      // 判断是否为自定义传参
      if (!!redirectDataParamsName) {
        protectedMailBox.send(redirectUrl, redirectDataParamsName, redirectDataParams);
      }

      wxApi.$navigateTo({
        url: '/' + redirectUrl,
        data: redirectDataParams || {},
      });
    }
  },
};
