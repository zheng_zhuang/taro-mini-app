
import {$getRouter, _safe_style_} from '@/wk-taro-platform';

import '@/wxat-common/utils/platform';
import React, {ComponentClass, Component, createRef} from 'react';
import Taro from '@tarojs/taro';
import {connect} from 'react-redux';
import {View, Canvas, Block} from '@tarojs/components';

import hoc from '@/hoc/index';
import screen from '@/wxat-common/utils/screen';
import report from '@/sdks/buried/report/index.js';
import Tabbar from '@/wxat-common/components/custom-tabbar/tabbar';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';
import Custom from '@/wxat-common/components/custom';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';
import api from '../../api';
import HomeActivityDialog from '../../components/home-activity-dialog';
import CouponDialog from '../../components/coupon-dialog';
import ShareDialog from '../../components/share-dialog';
import shareUtil from '../../utils/share.js';
import Search from '@/wxat-common/components/search-button/index';
import template from '@/wxat-common/utils/template';
import HoverCart from '@/wxat-common/components/cart/hover-cart';
import {updateBaseAction} from '@/redux/base';
import {updateGlobalDataAction} from '@/redux/global-data';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import CustomerHeader from '@/wxat-common/components/customer-header';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import checkOptions from '@/wxat-common/utils/check-options';
import LinkType from '@/wxat-common/constants/link-type';
import {getDecorateHomeData, notifyStoreDecorateRefresh} from '@/wxat-common/x-login/decorate-configuration-store.ts';
import {WaitComponent} from '@/decorators/Wait';
import {qs2obj} from '@/wxat-common/utils/query-string';
import RoomCode from '@/wxat-common/components/room-code/index'
import scan from '@/wxat-common/utils/scanH5'


import './index.scss';


// # class写法
interface PageStateProps {
  homeChangeTime: number;
  maAppName: string;
  $global: Record<string, any>;
  ext: Record<string, any>;
  searchBox: boolean;
  templ: any;
  source: string;
  // environment: string;
  forbidChangeStore: boolean;
  appInfo: any;
  currentStore: any;
  // canCustomDecorate: boolean;
  dispatchUpdateBase: Function;
  dispatchUpdateGlobal: Function;

  refStoreId: any;
  statusNavBarHeight: any;
  statusBarHeight: any;
  navOpacity: any;
  navBgColor: any;
  id: any;
  qrcodeNo: any;
  tp: any;
  tabbar: any;
}

interface PageDispatchProps {
}

interface PageOwnProps {
}

interface PageState {
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface Index {
  props: IProps;
}

const mapStateToProps = (state) => {
  const appInfo = state.base.appInfo || {};
  return {
    homeChangeTime: state.globalData.homeChangeTime,
    maAppName: state.globalData.maAppName,
    ext: state.ext,
    searchBox: state.base.appInfo && !!state.base.appInfo.searchBox,
    templ: template.getTemplateStyle(),
    // canCustomDecorate: state.base.appInfo && state.base.appInfo.canCustomDecorate,
    currentStore: state.base.currentStore,

    // environment: state.globalData.environment,
    source: state.base.source,
    forbidChangeStore: appInfo.forbidChangeStoreForGuideShare || appInfo.useShareUserLocation,
    appInfo,
    tabbar: state.globalData.tabbars
  };
};

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateBase: (data) => dispatch(updateBaseAction(data)),
  dispatchUpdateGlobal: (data) => dispatch(updateGlobalDataAction(data)),
});

@connect(mapStateToProps, mapDispatchToProps, undefined, {forwardRef: true})
@hoc
class Index extends WaitComponent {
  $router = $getRouter();
  static defaultProps = {
    tabbar: null
  }

  state = {
    dialogShow: false,
    coupon: [],
    storeConfirmed: false, // 打开红包弹窗

    couponPageUrl: LinkType.navigate.MINE_COUPON,
    pendingSharedRedPacket: {},
    qrCodeParams: {
      verificationNo: null,
      verificationType: 5,
      maPath: LinkType.navigate.RED_PACKET_OPEN.substr(1),
    },

    bgColor: '#FFFFFF',
    bgImgLink: '',
    previewMinHeight: 0,
    // 首页配置
    homeConfig: null,
    shareParams: {},

    refStoreId: null,
    statusNavBarHeight: 0,
    statusBarHeight: 0,
    navOpacity: 0,
    id: null,
    qrcodeNo: null,
    tp: null,
    updateRoomCode: false,
    hotelId: null,

    store_id: null
  };

  RefShareDialogCMPT: Taro.RefObject<any> = createRef();

  onWait() {
    const options = this.$router.params || {};
    const qs = qs2obj(options.scene + '');
    this.setState({
      shareParams: options,
    });

    if (qs.storeId) {
      // checkOptions.changeStore(+qs.storeId);

      if (+qs.force) {
        // 不可以切门店
        this.props.dispatchUpdateBase({
          appInfo: {...this.props.appInfo, forbidChangeStoreForGuideShare: true},
          source: 'wxwork',
        });
      }
    }

    // 首次进入
    wxAppProxy.setNavColor('pages/home/index', 'wxat-common/pages/home/index');

    let taskType;
    let shareId;
    let taskReferId;
    // 二维码
    if (options.scene) {
      const scene = decodeURIComponent(options.scene);
      const params: any = {};
      scene.split('&').forEach(item => {
        const key = item.split('=')[0];
        const value = item.split('=')[1];
        params[key] = value;
      })
      const distributorId = params.distributorId;
      if (distributorId) {
        this.props.dispatchUpdateGlobal({distributorId});
      }

      taskReferId = params.r;
      shareId = params.s;
      taskType = params.t;
      this.state.id = params.id;
      this.state.qrcodeNo = params.qrcodeNo;
      this.state.tp = params.tp;
      this.setState({
        hotelId: params.pId ? params.pId : params.hId,
      })
    }

    this.getStatusNavBarHeight();
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.homeChangeTime !== this.props.homeChangeTime) {
      this.getHomeConfig()
    }
  }

  componentDidUpdate() {
    const {source, forbidChangeStore, currentStore} = this.props;
    const changeStore = source === 'wxwork' && forbidChangeStore;
    const {_ref_storeId} = this.$router.params;
    if (_ref_storeId && changeStore && +_ref_storeId !== currentStore.id) {
      checkOptions.changeStore(_ref_storeId);
    }
  }

  componentDidMount() {
    this.init()
  }

  componentWillUnmount = () => {
    this.setState = (state, callback) => {
      return;
    };
  }

  componentDidShow() {
    /**解决页面跳外链后页面重叠开始。只解决住中详情页返回首页的，不具普遍性。因为id不一样*/
    const pages = Taro.getCurrentPages() || [];
    const currentPage = pages[pages.length - 1] || null; // 当前页
    const pattr = currentPage?.path;
    if (pattr) {
      // 获取 div 标签
      let gh = document.getElementById(pattr)?.parentElement;
      // 获取 div 标签下的所有子节点
      const pObjs = gh?.children;
      if (pObjs) {
        for (let i = pObjs.length - 1; i >= 0; i--) { // 一定要倒序，正序是删不干净的，可自行尝试
          if (pObjs[i].getAttribute('id')?.indexOf('/wxat-common/pages/home/index') === -1) {
            pObjs[i].setAttribute('style', 'display:none;');
          }
        }
      }
    }
    /**解决页面跳外链后页面重叠结束。只解决住中详情页返回首页的，不具普遍性。因为id不一样*/


    // fixme 由于头条iPhone小程序每次显示tabbar页面时都会显示tabbar页面，所以需要手动延迟隐藏
    if (process.env.TARO_ENV === 'tt') {
      if (wxApi.hideTabBar) {
        setTimeout(() => {
          wxApi.hideTabBar();
        }, 1000);
      }
    }
    const homeConfig = getDecorateHomeData() as unknown as any[];

    if (homeConfig && !!homeConfig.length) {
      // 配置首页主题色/title
      const pageInfoModule = homeConfig.find((item) => item.id === 'pageInfoModule');
      if (pageInfoModule) {
        const {name} = pageInfoModule.config || {};
        if (name) {
          wxApi.setNavigationBarTitle({title: name});
        }
      }
      if (!this.state.homeConfig) {
        this.setState({homeConfig});
      }
    }
  }

  componentWillUnMount = () => {
    this.setState = (state, callback) => {
      return;
    };
  }

  onPullDownRefresh() {
    // 获取当前门店最新首页配置
    notifyStoreDecorateRefresh().finally(() => {
      wxApi.stopPullDownRefresh();
    });
  }

  getHomeConfig = () => {
    const homeConfig = getDecorateHomeData();
    if (homeConfig && !!homeConfig.length) {
      // 配置首页主题色/title
      const pageInfoModule = homeConfig.find((item) => {
        return item.id === 'pageInfoModule';
      });
      let target = {
        bgColor: '',
        bgImgLink: '',
        previewMinHeight: 0
      };
      if (pageInfoModule) {
        const {name, bgColor, bgImgLink, previewMinHeight} = pageInfoModule.config || {};
        if (name) {
          wxApi.setNavigationBarTitle({
            title: name, // 页面标题为路由参数
          });
        }
        if (bgColor) {
          target.bgColor = bgColor;
        }
        if (bgImgLink) {
          target.bgImgLink = bgImgLink;
        }
        if (previewMinHeight) {
          target.previewMinHeight = previewMinHeight;
        }
      }
      target = {...target, ...{homeConfig}};
      this.setState(target);
    }
  }
  init = async () => {
    this.getHomeConfig()
    const options = this.$router.params
    if (process.env.WX_OA === 'true' && options.id) {
      this.setState({
        id: options.id
      })
      const data: any = await scan.scanRoomCodeFunc(options)
      if (data && data.isScan) {
        this.setState({
          updateRoomCode: data.isScan
        })
      }
    }
    if (options.env === 'H5' && options.storeId) {
      scan.changeStore(options.storeId)
    }
  }

  onIsUpdateRoomCode = () => {
    this.setState({
      updateRoomCode: false
    })
  }

  // 邀请好友助力
  onShareAppMessage(options) {
    report.share(true);
    if (options.from === 'button') {
      this.RefShareDialogCMPT.current && this.RefShareDialogCMPT.current.hide();
      const shareRedPacket = this.state.pendingSharedRedPacket;
      const totalFee = shareRedPacket.totalFee || 0;

      const appName = this.props.maAppName || '';
      const path = shareUtil.buildShareUrlPublicArguments({
        url: LinkType.navigate.RED_PACKET_OPEN + '?redPacketNo=' + this.state.qrCodeParams.verificationNo,
        bz: shareUtil.ShareBZs.FISSION_RED_PACKET_BOOSTER,
        bzName: `裂变红包-${totalFee / 100} 元红包`,
        bzId: this.state.qrCodeParams.verificationNo,
        sceneName: `${totalFee / 100} 元红包`,
      });

      return {
        title: `${appName}送你 ${totalFee / 100} 元红包，点击领取>`,
        path,
        imageUrl: 'https://htrip-static.ctlife.tv/wk/ic-share-img.png',
        // imageUrl: '/sub-packages/marketing-package/images/red-packet/ic-share-img.png',
        success: function () {
          report.shareRedPacket(true);
        },
        fail: function () {
          report.share(false);
          report.shareRedPacket(false);
        },
      };
    } else {
      const path = shareUtil.buildShareUrlPublicArguments({
        url: '/' + wxApi.$getCurrentPageRoute(),
        bz: shareUtil.ShareBZs.HOME,
        bzName: '首页',
      });

      return {
        title: '',
        path,
        imageUrl: '',
        success: () => {
          wxApi
            .request({
              url: api.coupon.get_coupons,
              loading: true,
              checkSession: true,
              data: {
                eventType: 0,
              },
            })
            .then((res) => {
              if (res.data && res.data.length) {
                this.setState({
                  coupon: res.data || [],
                  dialogShow: true,
                });
              }
            });
        },
        fail: () => {
        },
      };
    }
  }

  toShare = ({luckyMoneyNo, luckyMoneyPlanId}) => {
    this.state.qrCodeParams.verificationNo = luckyMoneyNo;
    this.setState({
      qrCodeParams: this.state.qrCodeParams,
    });

    // 查询海报的文案和图片
    wxApi
      .request({
        url: api.redPacket.planInfo,
        data: {
          planId: luckyMoneyPlanId,
        },
      })
      .then((res) => {
        this.setState({
          pendingSharedRedPacket: res.data,
        });

        this.RefShareDialogCMPT.current && this.RefShareDialogCMPT.current.show();
      });
  };

  onStoreConfirmed = () => {
    this.setState({
      storeConfirmed: true,
    });
  };

  // 保存图片
  onSavePosterImage = () => {
    this.RefShareDialogCMPT.current && this.RefShareDialogCMPT.current.savePosterImage(this);
  };

  // 获取状态栏高度
  getStatusNavBarHeight = () => {
    const that = this
    wxApi.getSystemInfo({
      success(res) {
        // 获取状态栏高度
        const statusBarHeight = res.statusBarHeight
        // 得到右上角菜单的位置尺寸
        const menuButtonObject = wxApi.getMenuButtonBoundingClientRect();
        const {top, height} = menuButtonObject;

        // 计算导航栏的高度
        // 此高度基于右上角菜单在导航栏位置垂直居中计算得到
        const navBarHeight = height + (top - statusBarHeight) * 2;

        // 计算状态栏与导航栏的总高度
        const statusNavBarHeight = statusBarHeight + navBarHeight;

        that.setState({
          statusNavBarHeight,
          statusBarHeight
        });
      }
    })
  };

  /**
   * 页面滚动
   * @param {*} ev
   */
  onPageScroll = (e) => {
    const homeConfig = getDecorateHomeData();
    const height = (homeConfig[1].config.height / 2) || 60;
    this.setState({
      navOpacity: e.scrollTop / height,
    });
  };

  to = () => {
    wxApi.$navigateTo({
      url: `/sub-packages/scene-package/pages/scene-list/index`
    })
  };

  render() {
    const {
      dialogShow,
      coupon,
      storeConfirmed,
      couponPageUrl,
      pendingSharedRedPacket,
      qrCodeParams,
      bgColor,
      bgImgLink,
      previewMinHeight,
      homeConfig,
      shareParams,
      statusBarHeight,
      statusNavBarHeight,
      navOpacity,
      updateRoomCode,
      id,
      qrcodeNo,
      hotelId
    } = this.state;
    const {$global: {$alreadyLogin, $tabbarReady} = {}, searchBox, currentStore, tabbar} = this.props;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-wph-Home' className='wk-wph-Home'>
        <CustomerHeader
          style={_safe_style_('pointer-events: none;')}
          navOpacity={navOpacity}
          title={homeConfig && homeConfig[0].config.name}
          bgColor={tabbar && tabbar.list && tabbar.list[0].navBackgroundColor}
        />
        {/*{!!searchBox && (*/}
        {/*  <View*/}
        {/*    className="search-box"*/}
        {/*    style={_safe_style_(`padding-top: ${statusBarHeight}px;*/}
        {/*      height: ${statusNavBarHeight}px;*/}
        {/*      display: ${statusNavBarHeight == 0 ? 'none' : 'flex'}`)}*/}
        {/*  >*/}
        {/*    <Search currentStore={currentStore} navOpacity={navOpacity}></Search>*/}
        {/*  </View>*/}
        {/*)}*/}

        {/* 自定义页 */}
        <View style={_safe_style_(`

          background: ${bgColor} url(${bgImgLink}) no-repeat top left /100%;
          min-height: ${previewMinHeight}px;`
        )}>
          {!!homeConfig && <Block><Custom isTabbar isHome pageConfig={homeConfig} shareParams={shareParams}/>
            <HomeActivityDialog showPage='wxat-common/pages/home/index'></HomeActivityDialog>
          </Block>
          }
        </View>

        {/* 底部的导航菜单 */}
        <Tabbar pagePath={pageLinkEnum.common.home}/>

        <CouponDialog visible={dialogShow} coupon={coupon} eventType='0'/>
        {!!storeConfirmed && <HomeActivityDialog couponPageUrl={couponPageUrl} onToShare={this.toShare}/>}

        {/*  分享对话弹框  */}
        <ShareDialog
          childRef={this.RefShareDialogCMPT}
          posterData={pendingSharedRedPacket}
          posterType='red-packet'
          posterHeaderType={pendingSharedRedPacket.posterType || 1}
          posterTips={pendingSharedRedPacket.posterContent || '一起来拆红包呀'}
          posterLogo={pendingSharedRedPacket.posterLogo}
          onSave={this.onSavePosterImage}
          qrCodeParams={qrCodeParams}
        />

        {/* 2021/6/30 songjia 暂时隐藏首页弹出门店弹窗 */}
        {/* <ChooseStoreDialog onConfirmed={this.onStoreConfirmed} /> */}

        {/* 授权弹窗 */}
        <AuthPuop/>

        {/* shareCanvas必须放在page里，否则无法保存图片 */}
        <Canvas canvasId='shareCanvas' className='red-packet-share-canvas'/>

        {!!($alreadyLogin && $tabbarReady) && <HoverCart/>}

        <RoomCode onIsUpdateRoomCode={this.onIsUpdateRoomCode} hotelId={hotelId} codeId={id} qrCodeNumber={qrcodeNo}
                  updateRoomCode={updateRoomCode}></RoomCode>
      </View>
    );
  }
}

export default Index as ComponentClass<PageOwnProps, PageState>;
