import {DOMAIN} from '../../../config/config.js'

const yqzEnv = process.env.REQUEST_ENV || 'EKS'
const WKB_BASE_URL =
  process.env.TARO_ENV === 'h5' && process.env.NODE_ENV === 'development'
    ? process.env.API_SCOPE
    : DOMAIN[yqzEnv] + process.env.API_SCOPE;


const get = (tail) => {
  return WKB_BASE_URL + tail;
};

export default {
  WKB_BASE_URL,
  // 公众号相关
  wxoa: {
    // 获取 SDK 签名
    login: get('/oauth2/login/v1/login'),
    getLoginUserInfo: get('/auth/mp/userInfo/v1/getLoginUserInfo'),
    getSDKSignature: get('/auth/mp/userInfo/v1/getSdkSignature'),
    getExtConfig: get('/oauth2/login/v1/getExtConfig'),
    getUserInfo: get('/auth/mp/userInfo/v1/getWxUserInfo'),
    sendVerifyCode: get('/auth/user/send_register_code'),
    register: get('/auth/user/mp_register_with_phone'),
    // 自定义页h5二维码参数解析
    getWXOACustomParams: get('/qrcode/query_template_qrcode_scenes?'),
    // 普通商品详情、虚拟商品详情、组合商品详情h5二维码参数解析
    getWXOADetailParams: get('/qrcode/exchange')
  },

  poi: {
    location: `${WKB_BASE_URL}/poi/poi_to_location`,
  },

  user: {
    updateUser: `${WKB_BASE_URL}/auth/vip/user/update_user`,
    userCardIdForDetail: `${WKB_BASE_URL}/auth/vip/user/card_id`,
    waterScore: `${WKB_BASE_URL}/auth/vip/user/water_score`,
    isScoreAboutExpiry: `${WKB_BASE_URL}/auth/vip/user/is_score_about_expiry`,
    scoreIntroduction: `${WKB_BASE_URL}/auth/user/score_introduction`,
    isNewUser: `${WKB_BASE_URL}/auth/vip/user/is_new`, // 是否是新用户
    wxAuth: `${WKB_BASE_URL}/auth/user/wx_auth`,
    outStaff: `${WKB_BASE_URL}/auth/vip/out_staff`, // 获取企业员工信息
  },

  goods: {
    list: `${WKB_BASE_URL}/auth/wx_store/query/item_list`,
    detail: `${WKB_BASE_URL}/auth/wx_store/query/item_detail`,
    listWithItemNos: `${WKB_BASE_URL}/auth/wx_store/query/items`, // 根据itemNo list查询商品列表, 可传门店id，字段为chooseStoreId
    recommendList: `${WKB_BASE_URL}/auth/wx_store/recommend/items_with_sku`,
    detailSimilarityList: `${WKB_BASE_URL}/auth/wx_store/recommend/similarity_with_sku`, // 商品详情里相似推荐接口
    childCategory: `${WKB_BASE_URL}/auth/category/select/category/detail`,
    skuItem: `${WKB_BASE_URL}/auth/wx_store/query/sku/one`, // 根据barcode查询某个独立的sku信息
    stockQuery: `${WKB_BASE_URL}/auth/wx_store/query/item/stock`, // 根据barcodelist查询商品库存
    stockQueryNew: `${WKB_BASE_URL}/auth/wx_store/query/item/stock/combination`, // 新增的根据barcodelist或combinationBarcodeList查询商品库存
    skuListWitSpecItemNos: `${WKB_BASE_URL}/auth/wx_store/query/item/sku`, // 根据itemNo list查询商品sku列表, 可传门店id，字段为chooseStoreId
    itemWarehouseInfo: `${WKB_BASE_URL}/auth/wx_store/query/item_warehouse_info`, // 查询商品仓库信息
    integralQuery: `${WKB_BASE_URL}/auth/integral/shop/query/list`, // 查询积分商品
    depositRule: `${WKB_BASE_URL}/auth/wx_store/front_money/rule`, // 查询定金商品规则
    activity: `${WKB_BASE_URL}/auth/much_buy/more_discount/query/item/activity`, // 查询商品可以参加的活动
    // 广告埋点
    advLog: `${WKB_BASE_URL}/qrcode/add/track/Log`
  },

  card: {
    list: `${WKB_BASE_URL}/auth/wx_item/query/card_list`,
    detail: `${WKB_BASE_URL}/auth/wx_item/query/card_detail`,
    idForList: `${WKB_BASE_URL}/auth/wx_item/query/cards`,
    coupon: `${WKB_BASE_URL}/auth/vip/user/available_card`,
    bindMemberCard: `${WKB_BASE_URL}/auth/vip/user/bind_outer_user`, // 绑定实体会员卡账号密码
    bindPhoneMemberCard: `${WKB_BASE_URL}/auth/vip/user/phone_bind_user`, // 绑定实体会员卡手机号
    sendVerifyCode: `${WKB_BASE_URL}/auth/vip/user/send_verify_code`,
    shopCard: `${WKB_BASE_URL}/auth/client/guide/select`,
  },

  order: {
    unifiedOrder: `${WKB_BASE_URL}/auth/vip/order/unifiedOrder`,
    unifiedOrderNew: `${WKB_BASE_URL}/auth/vip/order/createOrderAfterCalPrice`,
    list: `${WKB_BASE_URL}/auth/vip/order/query_list`,
    detail: `${WKB_BASE_URL}/auth/vip/order/detail`,
    cancel: `${WKB_BASE_URL}/auth/vip/order/close`,
    confirmDelivery: `${WKB_BASE_URL}/auth/vip/order/confirmDelivery`,
    refund: `${WKB_BASE_URL}/auth/vip/order/refund`,
    stat: `${WKB_BASE_URL}/auth/vip/order/stat`,
    payOrder: `${WKB_BASE_URL}/auth/vip/order/pay`,
    createOrderAfterCalPrice: `${WKB_BASE_URL}/auth/vip/order/createOrderAfterCalPrice`,
    cal_price: `${WKB_BASE_URL}/auth/vip/order/cal_price`,
    cal_price_new: `${WKB_BASE_URL}/auth/vip/order/calPriceBeforeCreateOrder`,
    coupons: `${WKB_BASE_URL}/auth/vip/order/query_favorable`,
    refundList: `${WKB_BASE_URL}/auth/vip/order/refund/list`, // 售后订单列表查询
    query_logistics: `${WKB_BASE_URL}/auth/vip/order/logistics`, // 物流信息查询
    refundOrder: `${WKB_BASE_URL}/auth/vip/order/refund`, // 创建退款订单
    refundFillLogistics: `${WKB_BASE_URL}/auth/vip/order/refund/fill_logistics`, // 填写发货地址
    refundDetail: `${WKB_BASE_URL}/auth/vip/order/refund/detail`, // 售后订单详情查询
    queryRefundFee: `${WKB_BASE_URL}/auth/vip/order/query_refund_fee`, // 退款金额查询
    shippingCompanyList: `${WKB_BASE_URL}/auth/vip/order/shipping_company_list`, // 物流公司列表查询
    revertRefund: `${WKB_BASE_URL}/auth/vip/order/refund/revert`, // 撤销退款操作
    updateApplyRefund: `${WKB_BASE_URL}/auth/vip/order/refund/update`, // 修改退款申请操作
    updateAddress: `${WKB_BASE_URL}/auth/vip/order/address/update`, // 修改订单收货地址
    schedule: `${WKB_BASE_URL}/auth/delivery_local/dada/track`, // 查询配送进度url
    verification_order: `${WKB_BASE_URL}/auth/vip/order/query_list_v2`, // 待核销订单
    verification_coupon: `${WKB_BASE_URL}/auth/coupon/ticket/query_by_list`, // 待核销优惠券
    verification_choose: `${WKB_BASE_URL}/auth/vip/verification/choice`, // 选择核销返回后端状态
    withOthersPreferential: `${WKB_BASE_URL}/auth/vip/order/withOthersPreferential`, // 查询能否使用优费券红包
    valetOrderConfirm: `${WKB_BASE_URL}/auth/valet/order/confirm`, // 用户接收代客订单(查询代客订单详情)
    payValetOrder: `${WKB_BASE_URL}/auth/valet/order/unified_pay`, // 代客下单统一支付接口
  },
  hotelOrder: {
    list: `${WKB_BASE_URL}/auth/vip/order/hotel/getOrderList`, // 酒店订单列表
    hotelCheck: `${WKB_BASE_URL}/auth/vip/order/hotel/check`, // 确预定
    orderPay: `${WKB_BASE_URL}/auth/vip/order/pay`,  // 支付
    hotelCancelOrder: `${WKB_BASE_URL}/auth/vip/order/hotel/cancel`,  // 取消订单


  },

  logistics: {
    logistics: `${WKB_BASE_URL}/logistics/message/query`,
  },

  coupon: {
    ticketlist: `${WKB_BASE_URL}/auth/coupon/ticket/list`,
    del: `${WKB_BASE_URL}/auth/coupon/ticket/delete`,
    list: `${WKB_BASE_URL}/auth/coupon/list`,
    collect: `${WKB_BASE_URL}/auth/coupon/ticket/collect`,
    qrcode_collect: `${WKB_BASE_URL}/auth/coupon/ticket/qrcode/collect`, // 扫码领券接口
    auto_collect: `${WKB_BASE_URL}/auth/coupon/ticket/plan/auto_collect`,
    detail: `${WKB_BASE_URL}/auth/coupon/query`,
    detailByCode: `${WKB_BASE_URL}/auth/coupon/ticket/query_by_code`,
    tickdetail: `${WKB_BASE_URL}/auth/coupon/ticket/query`,
    coupons: `${WKB_BASE_URL}/auth/coupon/ticket/query_available`,
    integral: `${WKB_BASE_URL}/auth/coupon/ticket/collect_use_integral`,
    get_coupons: `${WKB_BASE_URL}/auth/coupon/plan/list`,
    plancollect: `${WKB_BASE_URL}/auth/coupon/ticket/plan/collect`,
    query_status: `${WKB_BASE_URL}/auth/coupon/list_sub`,
  },

  login: {
    login: `${WKB_BASE_URL}/login/login`,
    loginV2: `${WKB_BASE_URL}/login_v2/login_v2`,
    register: `${WKB_BASE_URL}/auth/user/register`,
    userDeal: `${WKB_BASE_URL}/auth/user/agreement/query_user_agreement`,

    // 查询导购归属门店
    queryGuideStore: `${WKB_BASE_URL}/auth/client/guide/get`,

    // 查询商户装修配置信息
    getDecorateConfig: `${WKB_BASE_URL}/ma/config/store/template`,
    getTemplateConfig: `${WKB_BASE_URL}/ma/config/template/config`,
  },

  // ios支付
  IosPay: {
    queryIosSetting: `${WKB_BASE_URL}/vip/order/queryIosSetting`, // IOS设置查询接口
  },

  // 获取页面配置数据
  getPageConfig: `${WKB_BASE_URL}/ma/config/page`,

  // 获取商城页面配置数据
  getMallMainPageConfig: `${WKB_BASE_URL}/templateMarket/selectBsTemplateHomePage`,

  // 获取用户配置文件
  getUserConfig: `${WKB_BASE_URL}/ma/config/template`,

  // 获取订阅消息模板id
  getTemplateID: `${WKB_BASE_URL}/auth/ma/sub/get_template_id`,

  insertRoomCode: `${WKB_BASE_URL}/auth/qrcode/insertRoomCode`,

  selectQrCodeNumber: `${WKB_BASE_URL}/qrcode/selectQrCodeNumber`,

  queryQrCodeScenes: `${WKB_BASE_URL}/qrcode/query_qrcode_scenes`,

  address: {
    list: `${WKB_BASE_URL}/auth/vip/user/address/list`,
    delete: `${WKB_BASE_URL}/auth/vip/user/address/delete`,
    default: `${WKB_BASE_URL}/auth/vip/user/address/default`,
    add: `${WKB_BASE_URL}/auth/vip/user/address/add`,
    update: `${WKB_BASE_URL}/auth/vip/user/address/update`,
  },

  userInfo: {
    detail: `${WKB_BASE_URL}/auth/vip/user/user_detail`,
    update: `${WKB_BASE_URL}/auth/vip/user/update_user`,
    balance: `${WKB_BASE_URL}/auth/vip/user/balance`,
    // 微信会员卡状态，用于判断是否提示用户将会员卡加入卡包
    cardStatus: `${WKB_BASE_URL}/auth/vip/user/wx_card_status`,
    // 用户权益信息
    userRight: `${WKB_BASE_URL}/auth/vip/user/user_right`,
    // 获取卡券签名
    cardSign: `${WKB_BASE_URL}/auth/vip/config/card_sign`,
    // 修改绑定手机号码
    changePhoneNum: `${WKB_BASE_URL}/auth/vip/user/change_phone_confirm`,
    // 发送验证码
    sendVCode: `${WKB_BASE_URL}/auth/vip/user/send_vcode`,
  },

  store: {
    list: `${WKB_BASE_URL}/auth/store/query_list`,
    nearest: `${WKB_BASE_URL}/auth/store/query_nearest`,
    nearby: `${WKB_BASE_URL}/auth/store/query_nearby`,
    nearby_New: `${WKB_BASE_URL}/auth/store/list/query_nearby`,
    choose: `${WKB_BASE_URL}/auth/store/choose`,
    queryCustomizeShop: `${WKB_BASE_URL}/auth/store/query_CustomizeShop`,
    choose_new: `${WKB_BASE_URL}/auth/store/choose/v2`, // 切换门店并返回门店详情
  },

  appointment: {
    servePerson: `${WKB_BASE_URL}/auth/scheduled/technician/query`,
    query: `${WKB_BASE_URL}/auth/scheduled/query`,
    scheduled: `${WKB_BASE_URL}/auth/scheduled/time/query`,
  },

  rooms: {
    inventory: `${WKB_BASE_URL}/auth/wx_store/query/hotel/stock`,
  },

  ticket: {
    query: `${WKB_BASE_URL}/auth/ticket/query`,
    face_bind: `${WKB_BASE_URL}/auth/vip/order/ticket_face_bind`,
  },

  bill: {
    // 获取流水账单，参数page, pageSize
    query: `${WKB_BASE_URL}/auth/vip/user/water_bill`,
  },

  activity: {
    list: `${WKB_BASE_URL}/auth/pt/query_activity`,
    groupList: `${WKB_BASE_URL}/auth/pt/query_group`,
    querySetting: `${WKB_BASE_URL}/auth/pt/query_setting`,
    cutSetting: `${WKB_BASE_URL}/auth/bargain/setting/select`,
  },

  group: {
    detail: `${WKB_BASE_URL}/auth/pt/query_group_detail`,
    queryOwn: `${WKB_BASE_URL}/auth/pt/query_own_group`,
    queryActivityByGroupNo: `${WKB_BASE_URL}/auth/pt/query_activity_by_group_no`, // 根据groupNo查询拼团详情
  },

  myCardList: `${WKB_BASE_URL}/auth/vip/user/card`,

  tempMessage: `${WKB_BASE_URL}/auth/user/add_formId`,

  // 获取二维码分享key对应的分享值
  getShareSceneBySk: `${WKB_BASE_URL}/auth/qrcode/param/get?serialNum=`,

  // 通过参数内容映射key，生成二维码
  generatorMapperQrCode: `${WKB_BASE_URL}/auth/qrcode/generator`,

  // 核销
  verification: {
    getQRCode: `${WKB_BASE_URL}/auth/vip/verification/getQRCode`,
    getQRCodeV2: `${WKB_BASE_URL}/auth/vip/verification/getNewQRCode`,
    getQrCodeUseUniqueKey: `${WKB_BASE_URL}/auth/vip/verification/getQrCodeUseUniqueKey`,
    verificate: `${WKB_BASE_URL}/auth/vip/verification/do`,
    status: `${WKB_BASE_URL}/auth/vip/verification/status/query`,
  },

  // 会员码
  membershipCode: {
    getBarCode: `${WKB_BASE_URL}/auth/vip/user/code_ma`,
  },

  // 购物车
  shopping_card: {
    add: `${WKB_BASE_URL}/auth/vip/shopping_cart/add`,
    queryList: `${WKB_BASE_URL}/auth/vip/shopping_cart/queryList`,
    remove: `${WKB_BASE_URL}/auth/vip/shopping_cart/remove`,
    update: `${WKB_BASE_URL}/auth/vip/shopping_cart/update`,
    batch_remove: `${WKB_BASE_URL}/auth/vip/shopping_cart/batch_remove`,
  },

  // 商品分类
  classify: {
    categoryType: `${WKB_BASE_URL}/auth/category/use/select`, // 分类类型查询
    categoryTypeV2: `${WKB_BASE_URL}/auth/category/use/select/v2`, // 分类类型查询（包括展示列数）
    category: `${WKB_BASE_URL}/auth/wx_store/query/category`, // 1级分类查询
    // categoryNew: `${WKB_BASE_URL}/auth/wx_store/query/category/new`, // 新增的1级分类查询
    categoryNew: `${WKB_BASE_URL}/auth/wx_store/query/category/new/v2`, // 新增的1级分类查询
    categoryRoot: `${WKB_BASE_URL}/auth/category/select/root`, // 多级分类查询root
    categoryChild: `${WKB_BASE_URL}/auth/category/select/category/detail`, // 多级分类的子分类查询
    categoryAll: `${WKB_BASE_URL}/auth/wx_store/query/category_all`,
    itemSku: `${WKB_BASE_URL}/auth/wx_store/query/item_list`,
    itemSkuList: `${WKB_BASE_URL}/auth/wx_store/query/item_sku_list`,

    // 携旅
    categoryFrontNew: `${WKB_BASE_URL}/auth/wx_store/query/category/new/v2`, // 多级分类查询root
    categoryChildNew: `${WKB_BASE_URL}/auth/category/select/category/detail/v2`, // 多级分类的子分类查询
  },

  // 裂变红包
  redPacket: {
    // 检查是否弹出红包弹窗
    status: `${WKB_BASE_URL}/auth/lucky_money/query/status`,
    // 新的红包，查询红包信息
    plan: `${WKB_BASE_URL}/auth/lucky_money/query/plan`,
    // 领取红包
    collect: `${WKB_BASE_URL}/auth/lucky_money/collect`,
    // 查询领取的红包信息
    record: `${WKB_BASE_URL}/auth/lucky_money/query/record`,
    // 参与别人发起的裂变红包
    join: `${WKB_BASE_URL}/auth/lucky_money/join`,
    list: `${WKB_BASE_URL}/auth/lucky_money/query/cash_coupon`,
    // 查询红包方案信息
    planInfo: `${WKB_BASE_URL}/auth/lucky_money/query/plan_info`,
    // 新红包列表
    listV2: `${WKB_BASE_URL}/auth/red_packet/queryList`,
    // 待拆红包列表
    UnZipList: `${WKB_BASE_URL}/auth/lucky_money/query/cash_coupon/v2`,
    // 余额
    balance: `${WKB_BASE_URL}/auth/vip/user/balance`,
    // 提现
    cashing: `${WKB_BASE_URL}/auth/red_packet/cash/cashing`,
    // 红包流水
    flow: `${WKB_BASE_URL}/auth/red_packet/cash/water_bill`,
    // 待提现红包
    count: `${WKB_BASE_URL}/auth/red_packet/count`,
  },

  // 评论
  comment: {
    count: `${WKB_BASE_URL}/auth/item_comment/item/comment_group_count`,
    query: `${WKB_BASE_URL}/auth/item_comment/query`,
    query_my: `${WKB_BASE_URL}/vip/auth/item_comment/query_my`,
    add: `${WKB_BASE_URL}/auth/vip/item_comment/insert`,
  },

  // 砍价api
  cut_price: {
    // 根据商品itemNo查询砍价列表
    itemNosToList: `${WKB_BASE_URL}/auth/bargain/list/itemNos`,
    // 根据商品itemNos及skuId查询砍价列表
    itemNosSkuIdToList: `${WKB_BASE_URL}/auth/bargain/list/itemNos_sku`,
    // 发起砍价
    initiate: `${WKB_BASE_URL}/auth/bargain/initiate`,
    // 帮砍
    assist: `${WKB_BASE_URL}/auth/bargain/assist`,
    assistList: `${WKB_BASE_URL}/auth/bargain/assist/list`,
    // 砍价活动列表
    list: `${WKB_BASE_URL}/auth/bargain/list`,
    // 砍价活动详情
    detail: `${WKB_BASE_URL}/auth/bargain/detail`,
    // 查询自己发起了多少砍价
    initiateList: `${WKB_BASE_URL}/auth/bargain/initiate/list`,
  },

  activityPopup: {
    query: `${WKB_BASE_URL}/auth/popup/query`,
  },

  marketingPopup: {
    query: `${WKB_BASE_URL}/auth/marketingpop/query`,
    collectCoupon: `${WKB_BASE_URL}/auth/marketingpop/collect_coupon`,
    recordImgClick: `${WKB_BASE_URL}/auth/marketingpop/record_img_click`,
  },

  giftCard: {
    theme: `${WKB_BASE_URL}/auth/wx_store/query/gift_card/theme`,
    gift_card: `${WKB_BASE_URL}/auth/vip/user/gift_card`,
    send_out: `${WKB_BASE_URL}/auth/vip/user/gift_card/send_out`,
    active: `${WKB_BASE_URL}/auth/vip/user/gift_card/activate`,
    card_detail: `${WKB_BASE_URL}/auth/vip/user/gift_card_detail`,
    card_pw: `${WKB_BASE_URL}/auth/vip/user/gift_card/pw`,
    giftCardBill: `${WKB_BASE_URL}/auth/vip/user/gift_card/bill`,
    cancelSend: `${WKB_BASE_URL}/auth/vip/user/gift_card/cancel_send_out`,
  },

  // 充值卡
  recharge: {
    payOrder: `${WKB_BASE_URL}/auth/vip/order/recharge`,
    isForbid: `${WKB_BASE_URL}/auth/store/query_tag_info`,
  },

  // 协议查询
  deal: `${WKB_BASE_URL}/auth/recharge/agreement/query/agreement`,

  // 专题活动
  specialActivity: {
    partitions: `${WKB_BASE_URL}/auth/topic_activity/topic_with_partitions`,
    detail: `${WKB_BASE_URL}/auth/topic_activity/items_by_partition`,
    reduce: `${WKB_BASE_URL}/auth/topic_activity/reduce`,
    // 根据topicId和itemCount请求商品列表
    limitItemList: `${WKB_BASE_URL}/auth/topic_activity/topic_with_items`,
  },

  // 上传图片
  upload: `${WKB_BASE_URL}/auth/vip/img/upload`,
  uploadNew: `${WKB_BASE_URL}/auth/vip/img/uploadSingle`,
  getUserSig: `${WKB_BASE_URL}/auth/vip/im/getUserSig`,
  getReceipt: `${WKB_BASE_URL}/invoice/generateQrCode`,
  imageToBase64: `${WKB_BASE_URL}/auth/common/imageToBase64`,
  // 下单有礼
  courtesy: {
    participate: `${WKB_BASE_URL}/auth/order/present/participate`, // 是否参加下单有礼
    list: `${WKB_BASE_URL}/auth/coupon/list_sub`, // 根据id列表查询优惠券
    // collect:`${WKB_BASE_URL}/auth/coupon/ticket/collect`,废弃的领取优惠券
    getOrderPresentCoupon: `${WKB_BASE_URL}/auth/order/present/getOrderPresentCoupon`, // 领取优惠券
  },
  // 分享有礼
  shareGift: {
    query: `${WKB_BASE_URL}/auth/share/present/queryByStoreId`, // 查看分享有礼活动详情
    addRecordUser: `${WKB_BASE_URL}/auth/share/present/addRecordUser`, // 新增用户活动记录
  },
  // 营销图文
  article: {
    list: `${WKB_BASE_URL}/auth/market_article/query/list`,
    availableList: `${WKB_BASE_URL}/auth/market_article/query_available`,
    detail: `${WKB_BASE_URL}/auth/market_article/query`,
    commentList: `${WKB_BASE_URL}/auth/market_article/comment/query`,
    addComment: `${WKB_BASE_URL}/auth/market_article/comment/add`,
    changeLike: `${WKB_BASE_URL}/auth/like/record/addLike`,
    deleteComment: `${WKB_BASE_URL}/auth/market_article/comment/delete`,
  },

  // 电子画册
  ebooks: {
    categoryList: `${WKB_BASE_URL}/auth/album_category/query/list`,
    ebooksList: `${WKB_BASE_URL}/auth/picture_album/query/list`,
    detail: `${WKB_BASE_URL}/auth/picture_album/query`,
  },

  // 幸运转盘
  luckyDial: {
    startGame: `${WKB_BASE_URL}/auth/lucky_turning/draw`,
    getActivityDetails: `${WKB_BASE_URL}/auth/lucky_turning/query`,
    getUserActivityMessage: `${WKB_BASE_URL}/auth/lucky_times/query`,
    getUserRecords: `${WKB_BASE_URL}/auth/turning_records/lucky/record`,
    addLuckTimes: `${WKB_BASE_URL}/auth/lucky_times/plus`,
  },

  // 签到有礼
  signIn: {
    getSignInDetail: `${WKB_BASE_URL}/auth/signed_detail/query`,
    getSignInList: `${WKB_BASE_URL}/auth/signed_records/query/list`,
    actionSignIn: `${WKB_BASE_URL}/auth/signed_records/sign`,
    getCoupon: `${WKB_BASE_URL}/auth/coupon/query`,
    hasSignIn: `${WKB_BASE_URL}/auth/signed_detail/onGoing`,
    getSignInSurplusDayCount: `${WKB_BASE_URL}/auth/signed_records/query/needDay`,
  },

  // 限时秒杀
  seckill: {
    getSeckillList: `${WKB_BASE_URL}/auth/seckill/query_activity_list`, // 查询秒杀活动列表
    querySetting: `${WKB_BASE_URL}/auth/seckill/query_setting`, // 查询商家秒杀设置
    queryActivityRemain: `${WKB_BASE_URL}/auth/seckill/query_activity_remain`, // 查询参与秒杀商品剩余数量
  },

  // 健康档案
  healthFile: {
    add: `${WKB_BASE_URL}/auth/phr/savePhr`,
    detail: `${WKB_BASE_URL}/auth/phr/getPhr`,
    bloodPressure: `${WKB_BASE_URL}/auth/phr/getPhrBloodPressureList`,
    bloodGlucose: `${WKB_BASE_URL}/auth/phr/getPhrBloodGlucoseList`,
    addBloodPressure: `${WKB_BASE_URL}/auth/phr/createPhrBloodPressure`,
    addBloodGlucose: `${WKB_BASE_URL}/auth/phr/createPhrBloodGlucose`,
    updateBloodGlucoseInit: `${WKB_BASE_URL}/auth/phr/updateBloodGlucoseIni`,
  },

  // 用药提醒
  medicineRemind: {
    list: `${WKB_BASE_URL}/auth/drugRemind/getListByAppIdUserId`,
    save: `${WKB_BASE_URL}/auth/drugRemind/save`,
    del: `${WKB_BASE_URL}/auth/drugRemind/delete`,
    stop: `${WKB_BASE_URL}/auth/drugRemind/updateStatus`,
    detail: `${WKB_BASE_URL}/auth/drugRemind/getByAppIdUserIdItemNo`,
    unit: `${WKB_BASE_URL}/auth/drugRemind/getUnitList`,
  },

  // 微订购
  microOrder: {
    checkPersonalInfo: `${WKB_BASE_URL}/auth/customizeOrder/checkPersonalInfo`,
    queryList: `${WKB_BASE_URL}/auth/customizeOrder/queryList`,
    add: `${WKB_BASE_URL}/auth/customizeOrder/createOrder`,
    cancel: `${WKB_BASE_URL}/auth/customizeOrder/cancelOrder`,
    query: `${WKB_BASE_URL}/auth/customizeOrder/query`,
    confirmReceipt: `${WKB_BASE_URL}/auth/customizeOrder/confirmReceipt`,
    cal_price: `${WKB_BASE_URL}/auth/customizeOrder/cal_price`, // 订购计算价格
    placeAnOrder: `${WKB_BASE_URL}/auth/customizeOrder/placeAnOrder`, // 订购计算价格
  },
  // 处方药
  drug: {
    user_list: `${WKB_BASE_URL}/auth/medicine/drug_user/list`,
    user_create: `${WKB_BASE_URL}/auth/medicine/drug_user/create`,
    user_update: `${WKB_BASE_URL}/auth/medicine/drug_user/update`,
    user_delete: `${WKB_BASE_URL}/auth/medicine/drug_user/delete`,
    user_detail: `${WKB_BASE_URL}/auth/medicine/drug_user/detail`,
    dictionary_label: `${WKB_BASE_URL}/auth/medicine/dictionary/label`,
    dictionary_icd: `${WKB_BASE_URL}/auth/medicine/dictionary/icd`,
    prescription_save: `${WKB_BASE_URL}/auth/prescription/save`,
    prescription_delete: `${WKB_BASE_URL}/auth/prescription/delete`,
    prescription_detail: `${WKB_BASE_URL}/auth/prescription/detail`,
    prescription_flow_detail: `${WKB_BASE_URL}/auth/prescription/flow/detail`,
    prescription_list: `${WKB_BASE_URL}/auth/prescription/own/list`,
  },

  //  分销
  distribution: {
    // 注册
    register: `${WKB_BASE_URL}/auth/vip/dis_person/register`,
    // 后去推广员信息
    selectRule: `${WKB_BASE_URL}/auth/vip/dis_rule/selectRule`,
    // 后去推广员信息cs/dis/rule/selectRule?appId=1059&activityType=0
    ruleSelectRule: `${WKB_BASE_URL}/dis/rule/selectRule`,
    // 分销中心数据报表
    dataReport: `${WKB_BASE_URL}/auth/vip/dis_person/dataReport`,
    // 提现
    cashOut: `${WKB_BASE_URL}/auth/vip/distribution/cash_out/apply`,
    // 推广素材
    popularizeMaterial: `${WKB_BASE_URL}/auth/distribution/item/queryList`,
    // 提现记录
    cashOutRecord: `${WKB_BASE_URL}/auth/vip/distribution/cash_out/queryList`,
    // 佣金明细
    commissionRecord: `${WKB_BASE_URL}/auth/vip/dis_commission/queryRecords`,
    // 查询分销员信息
    disPersonSelect: `${WKB_BASE_URL}/auth/vip/dis_person/select`,
    // 业绩排行榜
    performanceRank: `${WKB_BASE_URL}/auth/vip/dis_commission/rank`,
    myRank: `${WKB_BASE_URL}/auth/vip/dis_commission/myRank`,
    // 我的客户
    myCustomers: `${WKB_BASE_URL}/auth/vip/dis_customer/queryMyCustomers`,
    // 我的下级
    myJuniorStaff: `${WKB_BASE_URL}/auth/vip/dis_person/child/page`,
    // 查询内购商品类目列表
    queryCategory: `${WKB_BASE_URL}/auth/distribution/item/queryCategory`,
    // 查询我的推广人信息
    selectMyDistributor: `${WKB_BASE_URL}/auth/vip/dis_customer/selectMyDistributor`,
    // 绑定客户推广人关系
    bindCustomerRelation: `${WKB_BASE_URL}/auth/vip/dis_customer/bindCustomerRelation`,
    // 查询员工类型
    selectSingle: `${WKB_BASE_URL}/auth/vip/dis_person/selectSingle`,
    // 分销状态总体查询
    distributionState: `${WKB_BASE_URL}/auth/vip/dis_person/selectJoinedActivities`,
  },

  // 赠品
  giftActivity: {
    list: `${WKB_BASE_URL}/auth/vip/gift_activity/giftList`,
    giftDetail: `${WKB_BASE_URL}/auth/wx_store_gift/query/gift_detail`,
    giftRule: `${WKB_BASE_URL}/auth/vip/gift_rule/get`,
  },

  // 内购券
  internalVoucher: {
    // 获取可领取的内购券张数
    gainAvailableVoucherNum: `${WKB_BASE_URL}/auth/dis/voucher/gainAvailableVoucherNum`,
    // 领取内购券
    gainVoucher: `${WKB_BASE_URL}/auth/dis/voucher/gainVoucher`,
    // 获取过期时间
    getCleanTime: `${WKB_BASE_URL}/auth/dis/voucher/getCleanTime`,
    // 获取内购券记录
    getRecord: `${WKB_BASE_URL}/auth/dis/voucher/getRecord`,
    // 获取内购券剩余张数
    getVoucherNum: `${WKB_BASE_URL}/auth/dis/voucher/getVoucherNum`,
    // 获取平台赠送的开始时间和结束时间
    getSystemGiveTime: `${WKB_BASE_URL}/auth/dis/voucher/getSystemGiveTime`,
  },

  below: {
    face_score: `${WKB_BASE_URL}/auth/interact/face/query`, // 线下营销获取图片
    collect_coupon: `${WKB_BASE_URL}/auth/interact/face/collect_coupon`, // 线下营销领券
  },

  // 摄影行业
  photography: {
    // 摄影作品
    works: `${WKB_BASE_URL}/auth/photo/sample/queryList`,
    works_detail: `${WKB_BASE_URL}/auth/photo/sample/query`,
    formConfig: `${WKB_BASE_URL}/auth/clue/query_config`,
    clueAdd: `${WKB_BASE_URL}/auth/clue/add`,
    collectCoupon: `${WKB_BASE_URL}/auth/clue/collect_coupon`,
  },

  // 地产行业
  estate: {
    layoutList: `${WKB_BASE_URL}/auth/item_estate/layout/list`, // 户型列表接口
    layoutDetail: `${WKB_BASE_URL}/auth/item_estate/layout/detail`, // 户型详情接口
    agreement: `${WKB_BASE_URL}/auth/vip/app/estate/agreement`, // 认筹协议
    sendCode: `${WKB_BASE_URL}/auth/vip/order/estate/send/code`, // 发送验证码
    checkCode: `${WKB_BASE_URL}/auth/vip/order/estate/check/code`, // 校验验证码
    sold_query: `${WKB_BASE_URL}/auth/vip/order/estate/room/sold/query`, // 查询已售列表
    subSms: `${WKB_BASE_URL}/auth/vip/sub/sms`, // 订阅通知接口
  },

  agreement: {
    download: `${WKB_BASE_URL}/auth/vip/agreement/download`,
  },

  // 表单工具
  formTool: {
    getConfig: `${WKB_BASE_URL}/auth/client/form/get`,
    addSubmit: `${WKB_BASE_URL}/auth/client/form/submit/add`,
  },

  // 任务中心
  taskCenter: {
    getTaskList: `${WKB_BASE_URL}/auth/promotion/task/list`, // 获取任务列表
    getAwardList: `${WKB_BASE_URL}/auth/promotion/award/list`, // 获取奖励列表
    getInviteMemberList: `${WKB_BASE_URL}/auth/promotion/invite/list`, // 获取邀请的用户列表
    getShareId: `${WKB_BASE_URL}/auth/promotion/add/share`, // 添加推广任务分享记录
    addInviteRecord: `${WKB_BASE_URL}/auth/promotion/add/invite`,
  },

  // 代金卡包
  couponpocket: {
    couponpocketList: `${WKB_BASE_URL}/auth/couponpocket/query`,
    detail: `${WKB_BASE_URL}/auth/couponpocket/detail_v2`,
  },

  virtualGoods: {
    // list: `${WKB_BASE_URL}/itemVirtual/queryList`,
    list: `${WKB_BASE_URL}/auth/itemVirtual/queryList`,
    detail: `${WKB_BASE_URL}/auth/itemVirtual/getDetail`,
    getMyCard: `${WKB_BASE_URL}/auth/vip/itemVirtual/getMyCard`,
    unifiedVirtualOrder: `${WKB_BASE_URL}/auth/vip/order/unifiedVirtualOrder`,
    priceUnifiedVirtualOrder: `${WKB_BASE_URL}/auth/turning_records/collectItem`, // 幸运大转盘虚拟商品下单
    ticketHotel: `${WKB_BASE_URL}/auth/roomTicket/hotelInfoList`
  },
  // 促销活动
  much_buy: {
    queryList: `${WKB_BASE_URL}/auth/much_buy/more_discount/queryList`,
    query: `${WKB_BASE_URL}/auth/much_buy/more_discount/query`,
    item: `${WKB_BASE_URL}/auth/much_buy/more_discount/query/item`,
  },

  // 微装修
  micro_decorate: {
    labelList: `${WKB_BASE_URL}/scenemodel/queryall`,
    decorateList: `${WKB_BASE_URL}/auth/scenemodel/list`,
    detail: `${WKB_BASE_URL}/scenemodel/query`,
    detail_auth: `${WKB_BASE_URL}/auth/scenemodel/query`,
    recommend: `${WKB_BASE_URL}/scenemodel/recommend`,
    recommend_auth: `${WKB_BASE_URL}/auth/scenemodel/recommend`,
    like: `${WKB_BASE_URL}/auth/scenemodel/approval`,
    formList: `${WKB_BASE_URL}/auth/client/form/scene/model`,
    formDetail: `${WKB_BASE_URL}/auth/client/form/submit/get`,
  },

  // 推广大使
  ambassador: {
    apply: `${WKB_BASE_URL}/auth/ambassador/addGuidePromoter`,
    agreement: `${WKB_BASE_URL}/auth/ambassador/findServiceAgreement`,
    detail: `${WKB_BASE_URL}/auth/ambassador/queryGuidePromoterDetail`,
    activityList: `${WKB_BASE_URL}/auth/ambassador/queryPageList`,
    sumcommission: `${WKB_BASE_URL}/auth/promoter/sumcommission`,
    querycommissionlist: `${WKB_BASE_URL}/auth/promoter/querycommissionlist`,
    guideInfo: `${WKB_BASE_URL}/auth/ambassador/getEmployeeInfoById`,
    findGuideQrcodeUrl: `${WKB_BASE_URL}/auth/ambassador/findGuideQrcodeUrl`,
    addGuideQrcodeUrl: `${WKB_BASE_URL}/auth/ambassador/addGuideQrcodeUrl`,
    sendMemberBindMsg: `${WKB_BASE_URL}/auth/ambassador/sendMemberBindMsg`,
    checkMemberIsGuider: `${WKB_BASE_URL}/auth/ambassador/checkMemberIsGuider`,
  },

  live: {
    list: `${WKB_BASE_URL}/auth/we/live/list`,
    detail: `${WKB_BASE_URL}/auth/we/live/detail`,
    subscribe: `${WKB_BASE_URL}/auth/we/live/subscribe`,
    watch: `${WKB_BASE_URL}/auth/we/live/watch`,
    watchList: `${WKB_BASE_URL}/auth/we/live/watch/list`,
    like: `${WKB_BASE_URL}/auth/user/like`,
    queryGuide: `${WKB_BASE_URL}/auth/we/live/guide/card/get`,
    bindGuide: `${WKB_BASE_URL}/auth/we/live/watch/by/guide`,
  },

  // 送礼
  giveGift: {
    switch: `${WKB_BASE_URL}/auth/app/award/switch/query`,
    sendList: `${WKB_BASE_URL}/auth/vip/award/send_list`,
    sendDetail: `${WKB_BASE_URL}/auth/vip/award/send_detail`,
    receiveList: `${WKB_BASE_URL}/auth/vip/award/receive_list`,
    receiveDetail: `${WKB_BASE_URL}/auth/vip/award/receive_detail`,
    sendUnifiedOrder: `${WKB_BASE_URL}/auth/vip/award/unifiedOrder`,
    freight: `${WKB_BASE_URL}/auth/vip/award/calc_freight`,
    receiveUnifiedOrder: `${WKB_BASE_URL}/auth/vip/award/receive/unifiedOrder`,
    openGiftDetail: `${WKB_BASE_URL}/auth/vip/gift/userGift`,
    participateInLuckyDraw: `${WKB_BASE_URL}/auth/vip/gift/participateInLuckyDraw`,
  },

  // 消息模板
  messageTemplate: {
    getTemplateIdList: `${WKB_BASE_URL}/auth/vip/award/getTemplateIdList`,
    selectWxTemplateList: `${WKB_BASE_URL}/auth/vip/award/selectWxTemplateList`,
    lotteryNotification: `${WKB_BASE_URL}/auth/vip/award/lotteryNotification`,
  },

  // 住中服务
  livingService: {
    getPlatformList: `${WKB_BASE_URL}/auth/platform/getPlatformList`,
    getPlatformDetails: `${WKB_BASE_URL}/auth/platform/getPlatformDetails`,
    unifiedOrder: `${WKB_BASE_URL}/auth/vip/order/unifiedPlatformOrder`,
    unifiedPlatformOrderV1: `${WKB_BASE_URL}/auth/vip/order/unifiedPlatformOrderV1`,
    query_nearby: `${WKB_BASE_URL}/auth/store/query_nearby`,
    getHotelDetails: `${WKB_BASE_URL}/auth/platform/getHotelDetails`,
    getSpace: `${WKB_BASE_URL}/auth/platform/getSpace`,
    getplatformDetail: `${WKB_BASE_URL}/auth/vip/platformOrder/detail`,
    getplatformDetailNew: `${WKB_BASE_URL}/auth/vip/platformOrder/detailForMultiItem`,
    getplatformOrder: `${WKB_BASE_URL}/auth/vip/platformOrder/list`,
    getplatformOrderNew: `${WKB_BASE_URL}/auth/vip/platformOrder/listForMultiItem`,
    selectByRoomCodeWifi: `${WKB_BASE_URL}/auth/qrcode/selectByRoomCodeWifi`,
    getstatistics: `${WKB_BASE_URL}/auth/vip/platformOrder/statistics`, // 用户订单统计
    getFirstLevelList: `${WKB_BASE_URL}/auth/platform/getFirstLevelList`, // 查询服务分类列表
    getLevel2Data: `${WKB_BASE_URL}/auth/platform/getLevel2Data`, // 查询一级分类下的分类或服务事项

    getNewFir: `${WKB_BASE_URL}/auth/platform/selectStoreCategoryListByCategoryIds`, // 查询一级分类
    getNewSec: `${WKB_BASE_URL}/auth/platform/selectStoreCategoryLevel2ListByCategoryIds`, // 查询二级分类或服务事项
    getNewSto: `${WKB_BASE_URL}/auth/platform/selectItemServerStore`, // 查询服务事项
    getNowDate: `${WKB_BASE_URL}/vip/order/getNowDate`, // 获取服务器时间

    getServerItemStock: `${WKB_BASE_URL}/auth/getServerItemStock`, // 查询库存
    getType: `${WKB_BASE_URL}/auth/operate/customerParam`, // 查询添加二维码还是跳转客服
  },
  // 场地预定
  scence: {
    getSceneList: `${WKB_BASE_URL}/auth/platform/selectReservationItem`, // 查询场地预定列表
    getSceneDetail: `${WKB_BASE_URL}/auth/platform/getReservationDetails`, // 查询场地预定详情
    listReservationPriceSpec: `${WKB_BASE_URL}/auth/platform/listReservationPriceSpec`, // 查询场地预定规格详情
    selectEmployeeInfo: `${WKB_BASE_URL}/auth/platform/selectEmployeeInfo`, // 获取员工信息
    getSceneSpec: `${WKB_BASE_URL}/auth/platform/listReservationPriceSpec`, // 场地预定列表-规格
    getLockDay: `${WKB_BASE_URL}/auth/platform/getLockDay`, // 获取锁场日期
    getReservationSpec: `${WKB_BASE_URL}/auth/platform/getReservationSpec`, // 获取选择规格的价格
    getOrderDetail: `${WKB_BASE_URL}/auth/vip/platformOrder/detailForReservation`, // 场地预定列表下单详情
    ordercreateReservationOrder: `${WKB_BASE_URL}/auth/vip/order/createReservationOrder`, // 场景预定创建订单
  },
  // 虚拟商品IOS支付
  iosPay: {
    orderDetail: `${WKB_BASE_URL}/vip/order/detail`,
    orderPay: `${WKB_BASE_URL}/vip/order/pay`,
  },

  // 公众号引流
  officiaAccountFlow: {
    getImage: `${WKB_BASE_URL}/vip/order/getOerateConfig`,
  },
  // 开发票
  invoice: {
    insertInvoice: `${WKB_BASE_URL}/auth/bill/insert` // 提交表单
  },
  hotel: {
    getRecommendList: `${WKB_BASE_URL}/hotel/search/getHotelList` // 推荐酒店a
  },
  //   礼品相关
  prize: {
    list: `${WKB_BASE_URL}/auth/wx_item/query/card_list`,
  },
  // 二维码相关
  qrCode: {
    getDetail: `${WKB_BASE_URL}/movie/qrCode/getDetail`
  }
};
