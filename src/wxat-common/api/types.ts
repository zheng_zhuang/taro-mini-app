/**
 * API 响应类型声明
 */

/**
 * 普通响应
 */
export interface Res<T> {
  data: T;
  success: boolean;
  errorCode?: number;
  errorMessage?: string;
}

/**
 * 列表响应
 */
export interface ResList<T> {
  data: T[];
  success: boolean;
  errorCode?: number;
  errorMessage?: string;
  /**
   * 页码
   */
  pageNo: number;
  /**
   * 分页大小
   */
  pageSize: number;
  /**
   * 总数
   */
  totalCount: number;
}
