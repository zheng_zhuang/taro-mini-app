
import {DOMAIN} from '../../../config/config.js'

const yqzEnv = process.env.REQUEST_ENV || 'EKS'
const BASE_URL =
  process.env.TARO_ENV === 'h5' && process.env.NODE_ENV === 'development'
    ? process.env.API_SCOPE
    : DOMAIN[yqzEnv] + process.env.API_SCOPE;
export default {
  film: {
    config: `${BASE_URL}/ma/config/template/page/config`,
    // 获取影视会员有效期
    expireTime: `${BASE_URL}/auth/movie/expireTime`,
    // 影视终端放行接口
    terminalPass: `${BASE_URL}/auth/movie/terminalPass`,
    // 通过商品获取最近的有效活动
    queryRecentEnableActivityByItemNo: `${BASE_URL}/market_activity/queryRecentEnableActivityByItemNo`,
    // 批量获取活动商品信息
    getActivityByItemNos: `${BASE_URL}/market_activity/getActivityByItemNos`,
  }
}
