import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { clearPageStuff } from '@/wxat-common/utils/util'
import { Block, View, Image, Text, Button, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';

import wxApi from '../../utils/wxApi';
import api from "../../api";
import report from '@/sdks/buried/report/index.js';
import money from '../../utils/money.js';
import date from '../../utils/date.js';
import cdnResConfig from '../../constants/cdnResConfig.js';
import authHelper from '../../utils/auth-helper.js';
import subscribeMsg from '../../utils/subscribe-msg.js';
import subscribeEnum from '../../constants/subscribeEnum.js';
import {useSelector,useDispatch} from 'react-redux'
import {updateGlobalDataAction} from '@/redux/global-data'
import Dialog from "../dialog";

import './index.scss';

const ActivityNameEnum = {
  RED_PACKET: 'red-packet',
  RED_PACKET_PROGRESS: 'red-packet-progress',
  NEW_USER_COUPONS: 'new-user-coupons',
  OLD_USER_COUPONS: 'old-user-coupons',
  SIGN_IN_GIFT: "sign-in-gift",
  MARKETING_GIFT: "marketing-gift",
  MARKETING_COUPONS: "marketing-coupons",
};

let timer:any = null; //计时器

/**
 * 首页营销活动dialog
 */

interface IProps {
  couponPageUrl?: string;
  showPage?:any,
  onToShare?: (luckyMoneyNo: any, luckyMoneyPlanId: any) => void;
}

const HomeActivityDialog: FC<IProps> = ({ couponPageUrl, onToShare,showPage }) => {

  const [showActivity, setShowActivity] = useState(false); // 是否展示活动
  const [dialogWidth, setDialogWidth] = useState(''); // 弹窗的高度
  const [dialogRadius, setDialogRadius] = useState('');
  const [activityName, setActivityName] = useState(''); // 展示的活动名，根据该标识展示相应的活动
  const [activityData, setActivityData] = useState<Array<Record<string, any>> | any>([]); // 活动数据
  const [popupMaterialUrl, setPopupMaterialUrl] = useState(''); // 拆红包进度
  const [redPacketPopupFlag, setRedPacketPopupFlag] = useState(1);
  const [luckyMoneyPopup, setLuckyMoneyPopup] = useState({});
  const [redPacketPopup, setRedPacketPopup] = useState({});
  const [redPacketProgress, setRedPacketProgress] = useState('');
  const [marketingPopup, setRedMarketingPopup] = useState<any>(null);
  const [marketingCoupons, setRedMarketingCoupons] = useState<any>(null);
  const [totalDiscountFee, setTotalDiscountFee] = useState('');
  const [isDefault, setIsDefault] = useState(false);
  const hideMarketingDialogPageList = useSelector(state => state.globalData.hideMarketingDialogPageList)
  const dispatch=useDispatch()
  const [count,setCount] = useState<number>(0)
  // todo stateChange ...
  // on: {
  //   stateChange(data) {
  //     if (!!data && !!data.key) {
  //       if (data.key === 'currentStore') {
  //         //门店切换时，重新请求优惠信息
  //         this.checkActivity(true)
  //       }
  //     }
  //   }
  // },
  /**
   * 生命周期函数--监听页面卸载
   */
  useEffect(() => {
    // if(hideMarketingDialogPageList.includes(showPage)){
    //   return
    // }
    if(showPage === 'wxat-common/pages/home/index'){
      checkActivity();
    }else{
      checkMarketing()
    }

    clearPageStuff()
    return function cleanup() {
      clearInterval(timer);
    };
  }, [showPage]);
  Taro.useDidHide(()=>{
    clearInterval(timer);
  })
  Taro.useDidShow(()=>{
      if(marketingPopup && marketingPopup.surplusTime>0){
        timer = setInterval(()=>{
          if(marketingPopup.surplusTime>0){
            marketingPopup.surplusTime--
            setCount(()=>{
              return marketingPopup.surplusTime
            })
            setRedMarketingPopup(marketingPopup)
          }else{
            // 倒计时结束，隐藏弹窗
            setShowActivity(false)
            // 礼包弹窗和图片弹窗才上报,优惠券列表不上报
            if(activityName === ActivityNameEnum.MARKETING_GIFT){
              // 活动弹窗计时关闭埋点上报
              // report.eventAutoClose(data);
            }
            clearInterval(timer);
          }
        },1000)
      }
  })
  /**
   *
   * @param forceShow 是否强制展示红包弹窗
   */
  function checkActivity(forceShow = true) {

    wxApi
      .request({
        url: api.activityPopup.query,
        loading: false,
        quite: true,
        data:{
          showPage:'wxat-common/pages/home/index'
        }
      })
      .then((result) => {
        const activityMap = result.data;
        // todo
        // const activityMap = {
        //   batchSendCoupons:null,
        //   firstGift:null,
        //   signedDetailVO:null,
        //   luckyMoney:true
        // };
        if (activityMap) {
          if (
            activityMap.batchSendCoupons ||
            activityMap.firstGift ||
            activityMap.luckyMoney ||
            activityMap.signedDetailVO ||
            activityMap.luckyMoneyPopupVO ||
            activityMap.redPacketPopupVO ||
            activityMap.marketingPopInfo
          ) {
            if (activityMap.batchSendCoupons) {
              const firstFiveCoupons = activityMap.batchSendCoupons.slice(0, 4);
              const coupons = assembleCoupons(firstFiveCoupons);
              const _totalDiscountFee = sumDiscountFee(activityMap.batchSendCoupons);

              setShowActivity(true);
              setActivityName(ActivityNameEnum.OLD_USER_COUPONS);
              setActivityData(coupons);
              setTotalDiscountFee(_totalDiscountFee);
              setDialogRadius(Taro.pxTransform(30));
              setDialogWidth(Taro.pxTransform(600));
            }
            if (activityMap.firstGift) {
              const firstFiveCoupons = activityMap.firstGift;
              const coupons = assembleCoupons(firstFiveCoupons);
              const _totalDiscountFee = sumDiscountFee(activityMap.firstGift);

              setShowActivity(true);
              setActivityName(ActivityNameEnum.NEW_USER_COUPONS);
              setActivityData(coupons);
              setTotalDiscountFee(_totalDiscountFee);
              setDialogRadius(Taro.pxTransform(4));
              setDialogWidth(Taro.pxTransform(630));
              // 添加第一次进店有礼埋点
              report.showGift()
            }
            // 首页营销弹窗
            if ( activityMap.marketingPopInfo ) {
              const data = activityMap.marketingPopInfo;
              data.surplusTime = data.stayTime //倒计时剩余时间
              // 立即弹出
              if(data.popupType === 0){
                setMarketingPopup(data);
                setCount(data.surplusTime)
                // 活动弹窗曝光埋点上报
                report.eventExposure(data);
              }else{
                // 计时弹出
                setTimeout(()=>{
                  setMarketingPopup(data);
                  setCount(data.surplusTime)
                  // 活动弹窗曝光埋点上报
                  report.eventExposure(data);
                },data.popupTime * 1000)
              }
            }
            // 签到
            if (activityMap.signedDetailVO && activityMap.signedDetailVO.signedActivityId) {
              setShowActivity(true);
              setActivityName(ActivityNameEnum.SIGN_IN_GIFT);
              setDialogWidth(Taro.pxTransform(588));
              setActivityData({
                signedActivityId: activityMap.signedDetailVO.signedActivityId,
              });
            }

            if (forceShow && activityMap.luckyMoney) {
              const cdnPath = 'https://cdn.htrip.tv';
                const defaultImg = cdnResConfig.redPacket.redPacketBg;

              let _popupMaterialUrl = activityMap.luckyMoney.popupMaterialUrl || defaultImg;
                const _isDefault = _popupMaterialUrl.indexOf('bg-of-redpack-popup') != -1;

              if (_popupMaterialUrl.indexOf('http') === -1) {
                _popupMaterialUrl = cdnPath + _popupMaterialUrl;
              }

              setShowActivity(true);
              setActivityName(ActivityNameEnum.RED_PACKET);
              setDialogRadius(Taro.pxTransform(20));
              setDialogWidth(Taro.pxTransform(_isDefault ? 750 : 550));
              setIsDefault(_isDefault);
              setPopupMaterialUrl(_popupMaterialUrl);

              report.redPacketShow();
            }

            if (activityMap.luckyMoneyPopupVO || activityMap.redPacketPopupVO) {
              let _luckyMoneyPopup = {};
                let _redPacketPopup = {};
                let _redPacketPopupFlag = 1;

              if (activityMap.luckyMoneyPopupVO) {
                _luckyMoneyPopup = {
                  currentJoinNumber: activityMap.luckyMoneyPopupVO.currentJoinNumber || 0,
                  minRemain: activityMap.luckyMoneyPopupVO.minRemain || 0,
                  luckyMoneyNo: activityMap.luckyMoneyPopupVO.luckyMoneyNo,
                  redPacketNumber: activityMap.luckyMoneyPopupVO.redPacketNumber || 0,
                  luckyMoneyPlanId: activityMap.luckyMoneyPopupVO.luckyMoneyPlanId,
                };
              } else {
                _redPacketPopupFlag = 2;

                _redPacketPopup = {
                  cashWithdrawalType: activityMap.redPacketPopupVO.cashWithdrawalType,
                  maxRedPacketFee: money.fen2Yuan(activityMap.redPacketPopupVO.maxRedPacketFee, 2),
                };
              }

              setShowActivity(true);
              setActivityName(ActivityNameEnum.RED_PACKET_PROGRESS);
              setDialogRadius(Taro.pxTransform(20));
              setDialogWidth(Taro.pxTransform(484));
              setLuckyMoneyPopup(_luckyMoneyPopup);
              setRedPacketProgress(cdnResConfig.redPacket.redPacketProgress);
              setRedPacketPopup(_redPacketPopup);
              setRedPacketPopupFlag(_redPacketPopupFlag);
            }
          }
        }
      });
  }

  function assembleCoupons(coupons) {
    // 只取前5个展示
    return coupons.map((item) => {
      return {
        couponFee: money.fen2Yuan(item.discountFee, 2),
        discountFee: item.discountFee,
        minimumFee: money.fen2Yuan(item.minimumFee, 2),
        validTime: date.format(new Date(item.endTime)),
        name: item.name,
        couponSendAmount: item.couponSendAmount,
        couponCategory: item.couponCategory,
        goods:
          item.suitItemType === 1 && item.itemBriefs
            ? item.itemBriefs.reduce((pre, cur) => {
                return pre + cur.name;
              }, '')
            : '所有商品',
      };
    });
  }
  function sumDiscountFee(coupons) {
    return money.fen2Yuan(
      coupons.reduce((pre, cur) => {
        return pre + cur.discountFee;
      }, 0),
      2
    );
  }
  function onSignClick() {
    if (!authHelper.checkAuth()) {
      return;
    }
    // 签到放开授权限制
    setShowActivity(false);

    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/signIn/index',
      // url:'/sub-packages/moveFile-package/pages/signIn/index',
      data: {
        id: activityData.signedActivityId,
      },
    });
  }
  function onUnpackClick() {
    if (!authHelper.checkAuth()) {
      return;
    }
    setShowActivity(false);

    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/red-packet/index',
      success: function () {
        report.joinRedPacketActivity(true);
      },
      fail: function () {
        report.joinRedPacketActivity(false);
      },
    });
  }
  function onCancelClick() {
    // 营销弹窗
    if(marketingPopup){
      // 礼包弹窗和图片弹窗才上报,优惠券列表不上报
      if(activityName === ActivityNameEnum.MARKETING_GIFT){
        // 活动弹窗点击埋点上报
        report.eventManualClose(marketingPopup);
      }
      // 关闭弹窗
      setShowActivity(false)
      // 清空定时器
      if(timer){
        clearInterval(timer);
      }
      return
    }
    // 授权订阅消息
    const Ids = [subscribeEnum.COUPONS_ARRIVE.value,subscribeEnum.COUPONS_OVERDUE.value];
    subscribeMsg.sendMessage(Ids).then(() => {
      setShowActivity(false);
      report.closeGift();
    });
  }
  function handleGoToCouponsPage() {
    if (!authHelper.checkAuth()) {
      return;
    }
    setShowActivity(false);

    wxApi.$navigateTo({
      url: couponPageUrl,
      success: function () {},
      fail: function () {},
    });
    // 上报新客有礼点击领取数
    report.getGift()
  }

  function fakeCoupons() {
    const fakeData = [
      {
        name: '我是优惠券1',
        discountFee: 8000,
        minimumFee: 30000,
        endTime: 1545309869000,
        suitItemType: 1,
        itemBriefs: [{ name: '我是商品1' }],
      },

      {
        name: '我是优惠券2',
        discountFee: 8000,
        minimumFee: 30000,
        endTime: 1545309869000,
        suitItemType: 1,
        itemBriefs: [{ name: '我是商品我是商品我是商品我是商品我是商品2' }],
      },

      {
        name: '我是优惠券我是优惠券我是优惠券我是优惠券3',
        discountFee: 8000,
        minimumFee: 30000,
        endTime: 1545309869000,
        suitItemType: 1,
        itemBriefs: [{ name: '我是商品3' }],
      },

      {
        name: '我是优惠券4',
        discountFee: 8000,
        minimumFee: 30000,
        endTime: 1545309869000,
        suitItemType: 1,
        itemBriefs: [{ name: '我是商品4' }],
      },
    ];

    const _coupons = assembleCoupons(fakeData.slice(0, 5));
    const _totalDiscountFee = sumDiscountFee(fakeData.slice(0, 5));

    setShowActivity(true);
    setActivityName(ActivityNameEnum.NEW_USER_COUPONS);
    setActivityData(_coupons);
    setTotalDiscountFee(_totalDiscountFee);
    setDialogRadius(Taro.pxTransform(4));
    setDialogWidth(Taro.pxTransform(630));
  }
  // 红包
  function toShare() {
    setShowActivity(false);
    // this.triggerEvent('toShare', { ...this.data.luckyMoneyPopup });
    onToShare && onToShare({ ...luckyMoneyPopup });
  }
  function toUse() {
    let url = '/sub-packages/marketing-package/pages/red-packet/red-packet-list/index';

    if (redPacketPopup.cashWithdrawalType === 0) {
      url = '/wxat-common/pages/home/index';
    }

    setShowActivity(false);

    wxApi.$navigateTo({
      url: url,
      data: {},
    });
  }
  // 是否显示营销弹窗
  function checkMarketing(){
    wxApi
      .request({
        url: api.marketingPopup.query,
        loading: false,
        quite: true,
        data:{
          showPage:showPage //小程序页面uri
        }
      })
      .then(res => {
        const data = res.data
        data.surplusTime = data.stayTime //倒计时剩余时间
        // 立即弹出
        if(data.popupType === 0){
          setMarketingPopup(data);
          setCount(data.surplusTime)
          // 活动弹窗曝光埋点上报
          report.eventExposure(data);
        }else{
          // 计时弹出
          setTimeout(()=>{
            setMarketingPopup(data);
            setCount(data.surplusTime)
            // 活动弹窗曝光埋点上报
            report.eventExposure(data);
          },data.popupTime * 1000)
        }
      }).catch(err=>{
      });
  }
  // 设置弹窗数据
  function setMarketingPopup(data){
    // 礼包样式弹窗
    if(data.showType === 0){
      dispatch(updateGlobalDataAction({hideMarketingDialogPageList:[...hideMarketingDialogPageList,...data.showPageList]}))
    }
    setShowActivity(true)
    setActivityName(ActivityNameEnum.MARKETING_GIFT)
    setDialogWidth(data.showType === 0 ? "589rpx" : '480rpx')
    setRedMarketingPopup(data)
    timer = setInterval(()=>{
      if(data.surplusTime>0){
        data.surplusTime--
        setCount(()=>{
          return data.surplusTime
        })
        setRedMarketingPopup(data)
      }else{
        // 倒计时结束，隐藏弹窗
        setShowActivity(false)
        // 礼包弹窗和图片弹窗才上报,优惠券列表不上报
        if(activityName === ActivityNameEnum.MARKETING_GIFT){
          // 活动弹窗计时关闭埋点上报
          report.eventAutoClose(data);
        }
        clearInterval(timer);
      }
    },1000)
  }
  // 领取礼包
  function onGiftClick(){
    if (!authHelper.checkAuth()) {
      return;
    }
    wxApi
    .request({
      url: api.marketingPopup.collectCoupon,
      loading: false,
      data:{
        popupId: marketingPopup.id, //营销活动id
        showPage:showPage
      }
    })
    .then(res => {
      if(res.success){
        const data = res.data;
        // 活动弹窗点击埋点上报

        report.eventClick(marketingPopup);
        data.coupons = assembleCoupons(data.coupons);
        // 重置倒计时
        const newmarketingPopup = marketingPopup

        newmarketingPopup.surplusTime = newmarketingPopup.stayTime
        setRedMarketingPopup(newmarketingPopup)
        setCount(newmarketingPopup.surplusTime)
        setActivityName(ActivityNameEnum.MARKETING_COUPONS)
        setDialogWidth('602rpx')
        setRedMarketingCoupons(data)
      }
    }).catch(err=>{

    });
  }
  // 点击礼包弹窗了解更多
  function onCouponDetail(){
    clearInterval(timer);
    setShowActivity(false)
    // const app = getApp()
    // app.globalData.hideMarketingDialogPageList.push(this.data.marketingPopup.id)
    // 自定义页链接
    if(marketingCoupons.toPage.indexOf('pages/custom/index?route=') === 0){
      marketingCoupons.toPage = 'wxat-common/' + marketingCoupons.toPage
    }
    // 礼品卡商城
    if(marketingCoupons.toPage === 'wxat-common/pages/gift-card/bg-list/index'){
      marketingCoupons.toPage = '/sub-packages/marketing-package/pages/gift-card/bg-list/index'
    }
    wxApi.$navigateTo({
      url: marketingCoupons.toPage
    });
  }
  // 点击图片样式营销弹框
  function onMarketingImg(){
    clearInterval(timer);
    setShowActivity(false)
    wxApi
    .request({
      url: api.marketingPopup.recordImgClick,
      loading: false,
      quite: true,
      data:{
        popupId:marketingPopup.id, //营销活动id
        showPage: showPage
      }
    })
    .then(res => {

    })
    // 活动弹窗点击埋点上报
    report.eventClick(marketingPopup);
    // 自定义页链接
    if(marketingPopup.toPage.indexOf('pages/custom/index?route=') === 0){
      marketingPopup.toPage = 'wxat-common/' + marketingPopup.toPage
    }
    // 礼品卡商城
    if(marketingPopup.toPage === 'wxat-common/pages/gift-card/bg-list/index'){
      marketingPopup.toPage = '/sub-packages/marketing-package/pages/gift-card/bg-list/index'
    }
    wxApi.$navigateTo({
      url: marketingPopup.toPage
    });
  }

  const padding = {
    'padding-bottom': '60px'
  }

  const RedPacket = (
    <View className={`red-packet-container ${isDefault ? 'red-packet-default' : ''}`}>
      <Image className='popupMaterialUrl' src={popupMaterialUrl}></Image>
      <View className='unpack-info unpack-btn' onClick={onUnpackClick}></View>
      <Image className='cancel-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png" onClick={onCancelClick}></Image>
    </View>
  );

  const RedPacketProgress = (
    <View className='red-packet-progress-container'>
      <Image src={redPacketProgress} className='rpp-bg'></Image>
      {redPacketPopupFlag === 1 ? (
        <View className='rpp-content'>
          {luckyMoneyPopup.currentJoinNumber > 1 && (
            <View className='rpp-progress'>{luckyMoneyPopup.currentJoinNumber}个红包进行中</View>
          )}

          <View className={`rpp-tip ${luckyMoneyPopup.currentJoinNumber <= 1 ? 'mt120' : ''}`}>
            还差<Text className='rpp-orange'>{luckyMoneyPopup.minRemain}</Text>
            人即可拆包成功
          </View>

          <View className='rpp-ctrl'>
            <Button className='rpp-btn' onClick={toShare}>
              邀请好友帮我助力
            </Button>
            {!!luckyMoneyPopup.redPacketNumber && (
              <View className='extra-tip'>
                {luckyMoneyPopup.redPacketNumber}个红包已拆成功，
                <Text className='rpp-orange' onClick={toUse}>
                  去查看
                </Text>
              </View>
            )}
          </View>
        </View>
      ) : (
        <View className='rpp-content rpp-succeed'>
          <View className='rpp-progress'>恭喜</View>
          <View className='rpp-tip'>
            已成功瓜分
            <Text className='rpp-red'>{redPacketPopup.maxRedPacketFee}元</Text>
            红包
          </View>

          <View className='rpp-ctrl'>
            <Button className='rpp-btn' onClick={toUse}>
              去{redPacketPopup.cashWithdrawalType === 0 ? '使用' : '提现'}
            </Button>
          </View>
        </View>
      )}

      <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png" className='cancel-icon' onClick={onCancelClick}></Image>
    </View>
  );

  const SignInGift = (
    <View className='sign-in-container'>
      <Image className='sign-in-bg' src={cdnResConfig.signIn.sign_dialog_bg} />
      <View className='sign-in-title'>签到有礼</View>
      <View className='sign-in-desc'>各种惊喜好礼等你来拿！赶快来签到吧！</View>
      <View className='sign-in-btn' onClick={onSignClick}>
        去签到
      </View>
      <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png" className='cancel-icon' onClick={onCancelClick}></Image>
    </View>
  );

  const dotBgStyleColor = {
    background: activityName === ActivityNameEnum.NEW_USER_COUPONS ? '#ef3327' : '#FF3B84',
  };

  const CouponsItem =
    !!Array.isArray(activityData) &&
    activityData.map((coupon, index) => {
      return (
        <View className={`coupons-item`}>
          <View className='dot top-dot' style={_safe_style_(dotBgStyleColor)}></View>
          <View className='dot bottom-dot' style={_safe_style_(dotBgStyleColor)}></View>
          <View className='left child'>
            {coupon.couponCategory === 2 ? (
              <View className='amount-box'>
                <Text className='limit-line amount'>{coupon.discountFee / 10}</Text>
                <Text className='yuan'>折</Text>
              </View>
            ) : (
              <View className='amount-box'>
                <Text className='yuan'>￥</Text>
                <Text className='limit-line amount'>{coupon.couponFee}</Text>
              </View>
            )}

            {coupon.discountFee ? (
              <Text className='limit-line limit'>满{coupon.minimumFee}可用</Text>
            ) : (
              <Text className='limit-line limit'>无消费限制</Text>
            )}
          </View>
          <View className='right child'>
            <Text className='limit-line coupon-name'>{coupon.name}</Text>
            <Text className='limit-line limit-goods'>指定商品：{coupon.goods}</Text>
            {!!coupon.couponSendAmount && <Text className='limit-line limit-goods'>×{coupon.couponSendAmount}张</Text>}
            <Text className='limit-line valid-time'>有效期：{coupon.validTime}</Text>
          </View>
        </View>
      );
    });

  const NewUserCoupons = (
    <View className='new-user-coupons-container coupons-container'>
      {/* <Image className='new-user-coupons-header' src={cdnResConfig.gift.new}></Image> */}
      <Text className='new-user-coupons-money'>天降好礼</Text>
      {/* <scroll-View scroll-y="true" className="new-user-coupons-item-container  {{activityData.length <= 3 ? 'new-user-only-one' : ''}}">
      <template is="coupons-item" data="{{coupons: activityData, dotColor: '#EF3327'}}"></template>
      </scroll-View> */}
      <ScrollView
        scrollY={true}
        className={`new-user-coupons-item-container  ${activityData.length <= 3 ? 'new-user-only-one' : ''}`}
      >
        {CouponsItem}
      </ScrollView>
      <View className='new-user-coupons-footer coupons-footer'>
        <View className='triangle-box'>
          <View className='left-triangle'></View>
          <View className='right-triangle'></View>
        </View>
        <View className='rect'></View>
        <View className='detail-btn' onClick={handleGoToCouponsPage}>
          看看我的优惠券
        </View>
      </View>
      <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png" className='cancel-icon' onClick={onCancelClick}></Image>
    </View>
  );

  const OldUserCoupons = (
    <View className='old-user-coupons-container coupons-container'>
      <Image className='old-user-coupons-header' src={cdnResConfig.gift.old}></Image>
      <Text className='old-user-coupons-money'>天降好礼</Text>
      <View className={`old-user-coupons-item-container ${activityData.length <= 3 ? 'old-user-only-one' : ''}`}>
        {/* <template is="coupons-item" data="{{coupons: activityData, dotColor: '#FF3B84'}}"></template> */}
        {CouponsItem}
      </View>
      <View className='old-user-coupons-footer coupons-footer'>
        <View className='detail-btn' onClick={handleGoToCouponsPage}>
          看看我的优惠券
        </View>
      </View>
      <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png" className='cancel-icon' onClick={onCancelClick}></Image>
    </View>
  );

  const MarketingGift = (
    <View className={ marketingPopup?.showType === 1 ?'imgStyle marketing-container' : 'marketing-container'}>
      {/* 礼包样式 */}
      {
        marketingPopup?.showType === 0 ? <Block>
        <Image className="marketing-gift" src={cdnResConfig.marketing.gift} />
        <View className="marketing-gift-btn" onClick={onGiftClick} style={_safe_style_("backgroundImage:url('" + cdnResConfig.marketing.giftBtn + "')")}>
          立即领取
        </View>
        </Block> : <Image className="marketing-img" src={marketingPopup?.popImg} onClick={onMarketingImg} />
      }
      <View className="time">{count}秒</View>
      <Image src={cdnResConfig.marketing.close} className='cancel-icon' onClick={onCancelClick}></Image>
    </View>
  )

  const MarketingCoupons = (
    <View className="marketing-coupons" style={_safe_style_("backgroundImage:url('" + cdnResConfig.marketing.couponBg + "')")}>
      <View className="time">{count}秒</View>
      <ScrollView scroll-y="true" className="marketing-coupons-container">
        {
          marketingCoupons?.coupons.map((coupon,couponId)=>{
            return (
              <View
          className="marketing-coupons-item"
          style={_safe_style_('background-image:url('+ cdnResConfig.marketing.couponItemBg + ')')}
          key={couponId}
        >
          <View className="coupons-left">
            <View className="title">{coupon.name}</View>
            <View className="desc">指定商品：{coupon.goods}</View>
            <View className="desc">有效期：{coupon.validTime}</View>
          </View>
          <View className="coupons-right">
            {
              coupon.couponCategory === 2 ? <View className="money" >{coupon.discountFee / 10}<Text>折</Text></View> : <View className="money" ><Text>￥</Text>{coupon.couponFee}</View>
            }
            {
              coupon.discountFee ? <View className="desc" >满{coupon.minimumFee}可用</View> : <View className="desc" >无消费限制</View>
            }
          </View>
        </View>
            )
          })
        }
      </ScrollView>
      <Image src={cdnResConfig.marketing.close} className='cancel-icon' onClick={onCancelClick}></Image>
      <View className="marketing-coupons-btn" onClick={onCouponDetail}>了解更多</View>
    </View>
  )
  return (
    <View
      data-fixme='03 add view wrapper. need more test'
      data-scoped='wk-wch-HomeActivityDialog'
      className='wk-wch-HomeActivityDialog'
    >
      <Dialog
        visible={showActivity}
        borderRadius='dialogRadius'
        width={dialogWidth}
        maxWidth={dialogWidth}
        top='40%'
        showTitle={false}
        showFooter={false}
        contentBackgroundColor='transparent'
        contentBoxShadow='none'
        contenMargin='0 0'
        contentPadding='0 0'
        backOpacity='0.6015'
      >
        <Block>
          {activityName === ActivityNameEnum.RED_PACKET && RedPacket}
          {activityName === ActivityNameEnum.RED_PACKET_PROGRESS && RedPacketProgress}
          {activityName === ActivityNameEnum.SIGN_IN_GIFT && SignInGift}
          {activityName === ActivityNameEnum.NEW_USER_COUPONS && NewUserCoupons}
          {activityName === ActivityNameEnum.OLD_USER_COUPONS && OldUserCoupons}
          {activityName === ActivityNameEnum.MARKETING_GIFT && MarketingGift}
          {activityName === ActivityNameEnum.MARKETING_COUPONS && MarketingCoupons}
        </Block>
      </Dialog>
    </View>
  );
};

HomeActivityDialog.defaultProps = {
  couponPageUrl: '',
};

export default HomeActivityDialog;
