import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';
import imageUtils from '../../../utils/image.js';

type DecorateEbooksModuleProps = {
  dataSource: any;
  PADDING?: number | string;
  onClick?: (args: any) => void;
};

interface DecorateEbooksModule {
  props: DecorateEbooksModuleProps;
}

class DecorateEbooksModule extends Component {
  static defaultProps = {
    PADDING: 0,
  };

  state = {
    renderList: [] as any[],
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    const idString = this.getIds();
    if (!idString.length) {
      this.setState({ renderList: [] });
      return;
    }
    wxApi
      .request({
        url: api.ebooks.ebooksList,
        quite: true,
        data: {
          pageNo: 1,
          pageSize: 6,
          albumStatus: 1,
          idString,
        },
      })
      .then((res) => {
        const { data } = res;
        if (!res.success) return;
        if (!data || !data.length) {
          this.setState({
            renderList: [],
          });
        } else {
          (data || []).forEach((item) => {
            item.coverUrl = imageUtils.thumbnailOneRowTwo(item.coverUrl);
          });
          this.setState({ renderList: data });
        }
      })
      .catch((err) => {
        console.log('micro-decorate--', err);
      });
  }

  getIds = () => {
    const { dataSource } = this.props;

    if (!dataSource || !dataSource.data) return '';

    const ids = dataSource.data.map((item) => item.id).join(',') || '';
    return ids;
  };

  handleGoToDetail = ({ currentTarget }) => {
    const { onClick } = this.props;
    const args = { item: currentTarget.dataset.item };
    if (typeof onClick === 'function') {
      onClick(args);
    }
  };

  render() {
    const { PADDING } = this.props;
    const { renderList } = this.state;
    const paddingLeft = typeof PADDING === 'string' ? PADDING : PADDING + 'px';

    return (
      <View className='ebooks-module'>
        <ScrollView scroll-x='true'>
          <View className='ebooks-container' style={{ paddingLeft }}>
            {(renderList || []).map((item, index) => (
              <View className='ebooks-item' onClick={this.handleGoToDetail} data-item={item} key={index}>
                <View
                  className='item-img'
                  style={{ background: `transparent url(${item.coverUrl}) no-repeat center center / cover` }}
                />

                <View className='item-name'>{item.title}</View>
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default DecorateEbooksModule;
