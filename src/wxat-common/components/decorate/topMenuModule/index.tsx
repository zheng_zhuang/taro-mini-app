import React, { useState, useEffect, FC } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, ScrollView, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import SpecialActivityModule from '../special-activity-module/index';
import GoodsCategoryCollection from '../../goods-category-collection/index';
import Custom from '../../custom';
import './index.scss';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';

// props的类型定义
type ComponentProps = {
  dataSource: Record<string, any>;
};

let TopMenuModule: FC<ComponentProps> = ({ dataSource }) => {
  //提前解构props，减少getter的操作

  const [currentIndex, setCurrentIndex] = useState(0);
  const [linkType, setLinkType] = useState(null);
  const [pageConfig, setPageConfig] = useState<[] | null>([]);
  const [activityConfig, setActivityConfig] = useState({});
  const [scrollLeft, setScrollLeft] = useState(0);

  useEffect(() => {
    if (dataSource.data) {
      getCurrentIndexData(0);
    }
  }, [!!dataSource.data]);

  const handleTab = (e) => {
    onMove(e);
    getCurrentIndexData(parseInt(e.currentTarget.dataset.index));
  };

  const getCurrentIndexData = (index: number) => {
    const item = dataSource.data[index];
    const oldLinkTypeIsCustom = linkType === 'custom';

    setCurrentIndex(index);
    setLinkType(item.linkType);
    if (item.linkType && item.linkPage) {
      if (item.linkType === 'custom') {
        // 原配置为自定义页，数据进行一次清理
        if (oldLinkTypeIsCustom) {
          setPageConfig(null);
        }

        // 获取自定义的数据
        getPageConfig(item.linkPage);
      }
      if (item.linkType === 'activity') {
        // 获取自定义的数据
        setActivityConfig({ activityId: item.linkPage });
      }
    }
  };

  const getPageConfig = async (index: number | string) => {
    const res = await wxAppProxy.getPageConfig(index);
    setPageConfig(res);
  };

  // TODO: 重构，H5 不支持
  const scope = Taro.useScope && Taro.useScope();

  const onMove = (ev: any) => {
    const screenWidth = 375;

    if (!wxApi.canIUse('createSelectorQuery')) return;

    wxApi
      .createSelectorQuery()
      .in(scope)
      .select('#menu_item_' + ev.currentTarget.dataset.index)
      .boundingClientRect((rect) => {
        const width = rect.width;
        setScrollLeft(+ev.currentTarget.offsetLeft + (width - screenWidth) / 2);
      })
      .exec();
  };

  return (
    <View data-scoped='wk-cdt-TopMenuModule' className='wk-cdt-TopMenuModule top-menu'>
      {dataSource && dataSource.alignType === 'center' ? (
        <View>
          <View className='menu-list menu-list-center' style={_safe_style_('background-color:' + dataSource.bgColor)}>
            {(dataSource.data || NOOP_ARRAY).map((item, index) => {
              return (
                <View
                  key={index}
                  className='menu-item'
                  id={'menu_item_' + index}
                  onClick={_fixme_with_dataset_(handleTab, { index: index })}
                >
                  <Text
                    className='item-name'
                    style={_safe_style_('color:' + (dataSource.titleColor || 'rgba(255, 255, 255, 1)'))}
                  >
                    {item.title}
                  </Text>
                  {index === currentIndex && (
                    <View
                      className='indecator'
                      style={_safe_style_('background-color:' + (dataSource.titleColor || 'rgba(255, 255, 255, 1)'))}
                    ></View>
                  )}
                </View>
              );
            })}
          </View>
        </View>
      ) : (
        <ScrollView
          className='menu-list'
          style={_safe_style_('background-color:' + dataSource.bgColor)}
          scrollWithAnimation
          scrollLeft={scrollLeft}
          scrollX
        >
          {(dataSource.data || NOOP_ARRAY).map((item, index) => {
            return (
              <View
                key={index}
                className='menu-item'
                id={'menu_item_' + index}
                onClick={_fixme_with_dataset_(handleTab, { index: index })}
              >
                <Text
                  className='item-name'
                  style={_safe_style_('color:' + (dataSource.titleColor || 'rgba(255, 255, 255, 1)'))}
                >
                  {item.title}
                </Text>
                {index === currentIndex && (
                  <View
                    className='indecator'
                    style={_safe_style_('background-color:' + (dataSource.titleColor || 'rgba(255, 255, 255, 1)'))}
                  ></View>
                )}
              </View>
            );
          })}
        </ScrollView>
      )}

      {linkType === 'custom' && <Custom pageConfig={pageConfig}></Custom>}
      {!!(linkType === 'category' && dataSource.data[currentIndex]) && (
        <GoodsCategoryCollection categoryId={dataSource.data[currentIndex].linkPage}></GoodsCategoryCollection>
      )}

      {linkType === 'activity' && <SpecialActivityModule dataSource={activityConfig}></SpecialActivityModule>}
    </View>
  );
};

// 给props赋默认值
TopMenuModule.defaultProps = {
  dataSource: {},
};

export default TopMenuModule;
