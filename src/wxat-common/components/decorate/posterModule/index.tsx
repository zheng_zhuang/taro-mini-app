import React, {useEffect, useState, FC} from 'react';
import {_safe_style_, _fixme_with_dataset_} from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import {useSelector} from 'react-redux';
import {View, Image, Navigator} from '@tarojs/components';
// import timerBannerUtil from "./timerBannerUtil.js";
import jumpPath from '../utils/jump-path.js';
import wxApi from '../../../utils/wxApi';
import imageUtils from '../../../utils/image.js';
import reportConstants from '../../../../sdks/buried/report/report-constants.js';
import report from "../../../../sdks/buried/report";
import authHelper from '../../../utils/auth-helper.js';
import api from '@/wxat-common/api'
import './index.scss';
import OfficialAccountFlow from '@/wxat-common/components/decorate/officialAccountFlow/index'
import useToYQZ from '@/hooks/useToYQZ';
import util from '@/wxat-common/utils/util';


// 小程序装修模块-海报模块
// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
}

const mapStateToProps = ({globalData, base}) => ({
  tabbars: globalData.tabbars,
  userInfo: base.userInfo,
  loginInfo: base.loginInfo,
  currentStore: base.currentStore
});

const PosterModule: FC<ComponentProps> = ({dataSource}) => {
  const defaultComputeDataList: Array<Record<string, any>> = [];
  const [posterList, setPosterList] = useState(defaultComputeDataList);
  const [showOfficialAccountFlow, setShowOfficialAccountFlow] = useState(false);
  const [OfficialAccountImageUrl, setOfficialAccountImageUrl] = useState('');

  const {tabbars, userInfo, loginInfo, currentStore, gps} = useSelector(mapStateToProps);
  const {handleDecorateToQYZ} = useToYQZ()
  useEffect(() => {
    if (!!dataSource && !!dataSource.data) {
      const temp = (dataSource.data || []).map((item) => {
        return {...item, ...{src: imageUtils.cdn(item.src)}};
      });
      setPosterList(temp);
    }
  }, [dataSource]);

  const cancelFlow = () => {
    setShowOfficialAccountFlow(false)
  }

  const getImageInfo = () => {
    const params = {
      appId: userInfo.appId || loginInfo.appId,
      storeId: currentStore.id
    }

    wxApi
      .request({
        url: api.officiaAccountFlow.getImage,
        data: params,
        loading: false,
        quite: true,
      }).then((res) => {
      // console.log('getImageInfo', params, res)
      if (res.success && res.data && res.data.image) {
        // setOfficialAccountImageUrl('https://material-source.ctlife.tv/6dfa68d0-04b1-4cc0-8d57-9f1f45c82935.jpg')
        setOfficialAccountImageUrl(res.data.image)

      }
    })
  }

  const click = async (e) => {
    const item = e.target.dataset.item;
    const index = e.target.dataset.index;

    util.sensorsReportData('click_poster', {
      event_name: '海报点击',
      carousel_label: item.label,
      carousel_name: item.linkName,
      carousel_link_page: item.linkPage,
      carousel_type: item.linkPageKey,
    })
    // 易企住链接跳转
    if (item.linkPageKey && String(item.linkPageKey).includes('yqz')) {
      await handleDecorateToQYZ(item)
      return
    }

    if (item.linkPageKey === "officialAccountFlow") {
      getImageInfo()
      setShowOfficialAccountFlow(true)
      return
    }

    if (!!item.H5Link) {
      // window.location.href = item.H5Link;
      wxApi.$locationTo(item.H5Link)
      return;
    }

    if (item.openType === 'h5' && item.webLinkUrl) {
      // web-view H5链接

      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/custom-webview/index',
        data: {linkUrl: item.webLinkUrl},
      });

      return;
    }
    if (!(!item.linkPage || item.linkPage === undefined)) {
      if (item.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }
      if (item.linkPageKey == 'mate-tool') {
        const {list} = tabbars;
        const realPathIndex = list.findIndex(({realPath}) => realPath == item.linkPage);
        if (~realPathIndex) {
          return wxApi.switchTab({
            url: list[realPathIndex].pagePath,
          });
        }
      }
      // 判断是否拨打电话进来
      if (item.linkPageKey == "oneTouchDial" && item.linkOneTouchDial) {
        Taro.makePhoneCall({
          phoneNumber: item.linkOneTouchDial
        })
      }
      // 兼容场地预定路径迁移到主包
      if (item.linkPage == "sub-packages/marketing-package/pages/living-service/hotel-reservation-list/index") {
        item.linkPage = "wxat-common/pages/hotel-reservation-list/index"
      } else if (item.linkPage == "sub-packages/marketing-package/pages/living-service/goods-rental/index") {
        item.linkPage = "wxat-common/pages/service-classification-tree/index"
      }

      // 判断是否是住中服务进来的
      if (item.linkPageKey === "service-category" || item.serviceItemId || item.wifiCateGoryId) {
        if (item.linkPage === "wxat-common/pages/goods-detail/index") {
          item.linkPage = "sub-packages/marketing-package/pages/living-service/goods-rental-details/index"
        }
        wxApi.$navigateTo({
          url: jumpPath(item, '', reportConstants.SOURCE_TYPE.poster.key).url,
          data: {
            id: item.serviceItemId ? item.serviceItemId : item.serviceCateGoryId,
            sourceType: item.sourceType
          }
        })
      } else {
        wxApi.$navigateTo(jumpPath(item, '', reportConstants.SOURCE_TYPE.poster.key));
      }
    }
  };

  return (
    <View data-scoped='wk-cdp-PosterModule' className='wk-cdp-PosterModule poster-module'>
      {!!(!!posterList && !!posterList.length) && (
        <View
          style={_safe_style_(
            'margin:' + (dataSource.marginUpDown * 2 + 'px ' + dataSource.marginLeftRight * 2 + 'px')
          )}
        >
          {posterList.map((item, index) => {
            const key = `${item.src}${index.toString()}`
            return (
              <View key={key}>
                {item.linkPage === 'miniProgram' && !item.H5Link ? (
                  <Navigator target="miniProgram" appId={item.linkId} path={item.linkPath}>
                    <Image
                      className="poster 1"
                      src={item.src}
                      mode="widthFix"
                      style={_safe_style_(
                        'border-radius: ' +
                        (dataSource.radius + 'px') +
                        ';margin-top: ' +
                        (index === 0 ? '0' : dataSource.margin + 'px')
                      )}
                    ></Image>
                  </Navigator>
                ) : item.linkPage === 'oneTouchDial' ? (
                  <Image
                    className="poster 2"
                    src={item.src}
                    mode="widthFix"
                    onClick={_fixme_with_dataset_(click, {index: index, item: item})}
                    style={_safe_style_(
                      'border-radius: ' +
                      (dataSource.radius + 'px') +
                      ';margin-top: ' +
                      (index === 0 ? '0' : dataSource.margin + 'px')
                    )}
                  ></Image>
                ) : (
                  <Image
                    className="poster 3"
                    src={item.src}
                    mode="widthFix"
                    onClick={_fixme_with_dataset_(click, {index: index, item: item})}
                    style={_safe_style_(
                      'border-radius: ' +
                      (dataSource.radius + 'px') +
                      ';margin-top: ' +
                      (index === 0 ? '0' : dataSource.margin + 'px')
                    )}
                  ></Image>
                )}
              </View>
            );
          })}
        </View>
      )}

      {showOfficialAccountFlow && OfficialAccountImageUrl && (
        <OfficialAccountFlow
          imageUrl={OfficialAccountImageUrl}
          onCancel={cancelFlow}
        ></OfficialAccountFlow>
      )}
    </View>
  );
};

// 给props赋默认值
PosterModule.defaultProps = {
  dataSource: {},
};

export default PosterModule;
