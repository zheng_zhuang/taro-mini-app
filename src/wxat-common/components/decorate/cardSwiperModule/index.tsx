import React, { FC } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, ScrollView, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../utils/money.wxs.js';
// components/goods-module/index.js
// import wxApi from '../../../utils/wxApi';
// import api from '../../../api/index.js';

import './index.scss';

type ComponentProps = {
  dataSource?: Record<string, any>;
  dataList: Array<Record<string, any>>;
  bgImages: Array<string>;
  PADDING: number;
  onClick: (object) => any;
};

let CardSwiperModule: FC<ComponentProps> = ({ PADDING, dataList, bgImages, onClick }) => {
  const handleGoToCardDetail = (item) => {
    // const myEventDetail = {
    //   item: e.currentTarget.dataset.item
    // };
    // this.triggerEvent('click', myEventDetail);
    onClick && onClick(item);
  };
  return (
    <View data-scoped='wk-cdc-CardSwiperModule' className='wk-cdc-CardSwiperModule card-module'>
      <ScrollView scrollX>
        <View
          className='mine-card-container'
          style={{
            paddingLeft: Taro.pxTransform(PADDING),
          }}
        >
          {(dataList || []).map((item, index) => {
            return (
              <View
                className='mine-card-item'
                onClick={_fixme_with_dataset_(() => handleGoToCardDetail(item), { item: item })}
                key={index}
              >
                {!!bgImages[index] && <Image className='mine-card-bg' src={bgImages[index]} mode='aspectFill'></Image>}
                <View className='container'>
                  <View className='name'>{item.name}</View>
                  {!!item.giftAmount && (
                    <View className='discount'>{'送' + filters.moneyFilter(item.giftAmount, true)}</View>
                  )}

                  <View className='footer'>
                    <View className='validity'>{'￥' + filters.moneyFilter(item.salePrice, true)}</View>
                    <View className='validity'>{item.typeDesc}</View>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default CardSwiperModule;
