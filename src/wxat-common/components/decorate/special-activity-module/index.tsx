import React, { useState, useEffect, FC, useRef, useCallback } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import { useSelector } from '@tarojs/redux';
// npm-components/decorate/special-activity-module/index.js
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import report from '@/sdks/buried/report/index.js';
import GoodsItem from '../../industry-decorate-style/goods-item';
import './index.scss';

/**
 * 首页活动专题装修组件，支持横向滚动和纵向滚动的专区活动简版展示，同时也支持整个专区活动在首页展示，
 * 具体用法参考首页装修组件的用法，需要服务端传递一个dataSource JSON字符串，该JSON中的字段与pc web协定好，包括：
 * showPoster 是否展示海报，如果不展示海报则展示专题活动名
 * limitNum 限制展示的商品数，如果为null 或者0 则认为不限制
 * activityId 活动id
 * display 展示形式，enum horizon：水平滑动列表；vertical：垂直滚动列表
 */

// props的类型定义
// 给props赋默认值

const reportSource = reportConstants.SOURCE_TYPE.activity.key;

type configDataDefine = {
  marginUpDown: number; //上下边距
  marginLeftRight: number; //左右边距
  radius: number; //圆角
  showPoster: boolean; //是否显示图片
  topicName: string; //活动名
  limitNum: number; //限制条数
  activityId: number; //活动id
  display: string; //展示是垂直还是横向
  isComponent: boolean; //该字段表示是否以组件的形式展示，如果以组件的形式展示，支持配置，否则写死样式*!/
  topicImgUrl: string;
  topicColor: string;
  propName: string;
};

const defaultConfigData: configDataDefine = {
  marginUpDown: 0, //上下边距
  marginLeftRight: 10, //左右边距
  radius: 0, //圆角
  showPoster: true, //是否显示图片
  topicName: '', //活动名
  limitNum: 10, //限制条数
  activityId: 0, //活动id
  display: 'vertical', //展示是垂直还是横向
  isComponent: true, ///!*该字段表示是否以组件的形式展示，如果以组件的形式展示，支持配置，否则写死样式*!/
  topicImgUrl: '',
  topicColor: '',
  propName: '',
};

// type StateType = {
//   base: {
//     currentStore: Record<string, any>;
//   };
// };

type ComponentProps = {
  dataSource: Record<string, any>;
};

let SpecialActivityModule: FC<ComponentProps> = ({ dataSource }) => {
  //提前解构props，减少getter的操作
  const defaultGoodsList: Array<Record<string, any>> = [];
  const [goodsList, setGoodsList] = useState(defaultGoodsList);
  const [tmpStyle, setTmpStyle] = useState('');
  const [configData, setConfigData] = useState(defaultConfigData);
  const currentTopic = useRef(0);
  const currentItem = useRef({
    topicId: 0,
    topicName: '',
    display: 'vertical',
  });

  useEffect(() => {
    const { marginUpDown, marginLeftRight, radius } = dataSource;
    const style = `margin:${Taro.pxTransform((marginUpDown || 0) * 2)} ${Taro.pxTransform(
      (marginLeftRight || 0) * 2
    )};border-radius:${Taro.pxTransform((radius || 0) * 2)}`;
    const _configData: any = dataSource;
    if (dataSource) {
      if (dataSource.data) {
        _configData.topicName = dataSource.data.topicName;
        _configData.topicImgUrl = dataSource.data.topicImgUrl;
        _configData.topicColor = dataSource.data.topicColor;
      }
    }
    setConfigData(_configData);

    setTmpStyle(style);

    setTimeout(() => {
      report.showSpecialActivity();
    }, 100);

  }, [dataSource]);

  useEffect(() => {
    /**
     * 查询分类中的子数据
     */
    const queryItemListByActivityId = (topicId: number) => {
      if (!topicId) return;
      const param = {
        topicQueryList: [
          {
            topicId: topicId || configData.activityId,
            itemCount: configData.limitNum ? configData.limitNum : 1000,
          },
        ],
      };

      wxApi
        .request({
          url: api.specialActivity.limitItemList,
          method: 'POST',
          header: {
            'content-type': 'application/json', // 默认值
          },
          loading: false,
          quite: true,
          data: param.topicQueryList,
        })
        .then((res) => {
          if (res.success === true && res.data && res.data.length > 0) {
            const _goodsList = res.data[0].wxItemSkuList || [];
            const _configData = dataSource;
            _configData.topicName = res.data[0].topicName;
            _configData.topicImgUrl = res.data[0].topicImgUrl;
            _configData.topicColor = res.data[0].topicColor;
            if (currentTopic.current !== _configData.activityId) {
              currentTopic.current = _configData.activityId;
            }
            setConfigData((prevState) => ({ ...prevState, ..._configData }));
            setGoodsList(_goodsList);
          }
        });
    };

    currentItem.current.topicId = configData.activityId || 0;
    currentItem.current.display = configData.display || 'vertical';
    currentItem.current.topicName = configData.topicName;

    // 避免configData更新一次就触发一次 造成死循环
    if (configData.activityId !== currentTopic.current) {
      queryItemListByActivityId(configData.activityId);
    }
  }, [configData, currentTopic.current]);

  const handleToActivity = () => {
    const { topicId, topicName, display } = currentItem.current;
    report.clickSpecialActivity(topicId, topicName);
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/activity/index',
      data: {
        topicId,
        display,
      },
    });
  };

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cds-SpecialActivityModule'
      className='wk-cds-SpecialActivityModule'
    >
      {!!(goodsList && goodsList.length) && (
        <View className='special-activity-container' style={_safe_style_(tmpStyle)}>
          {(configData.showPoster || !configData.isComponent) && !!configData.topicImgUrl ? (
            <Image className='poster' onClick={handleToActivity} src={configData.topicImgUrl}></Image>
          ) : (
            <View className='title' onClick={handleToActivity}>
              {configData.topicName || ''}
            </View>
          )}

          {/* 默认vertical */}
          {!!(!configData.display || (configData.display === 'vertical' && !!(goodsList && goodsList.length))) && (
            <Block>
              {goodsList.map((item, index) => (
                <GoodsItem
                  goodsItem={item}
                  key={index}
                  reportSource={reportSource}
                  showDivider={index !== goodsList.length - 1}
                  display='vertical'
                />
              ))}

              {!!configData.limitNum && (
                <View className='all' onClick={handleToActivity}>
                  <Text>查看全部 &gt;</Text>
                </View>
              )}
            </Block>
          )}

          {!!(configData.display === 'horizon' && !!(goodsList && goodsList.length)) && (
            <View style={_safe_style_('overflow-x: auto;white-space:nowrap; display: flex;')}>
              {goodsList.map((item, index) => (
                <GoodsItem goodsItem={item} key={index} reportSource={reportSource} display='horizon' />
              ))}
            </View>
          )}

          {!!(configData.display === 'oneRowOne' && !!(goodsList && goodsList.length)) && (
            <View className='oneRowOne-container'>
              {goodsList.map((item, index) => (
                <View className='oneRowOne-item' key={index}>
                  <GoodsItem goodsItem={item} reportSource={reportSource} display='oneRowOne' />
                </View>
              ))}

              {!!configData.limitNum && (
                <View className='all' onClick={handleToActivity}>
                  <Text>查看全部 &gt;</Text>
                </View>
              )}
            </View>
          )}

          {!!(configData.display === 'oneRowTwo' && !!(goodsList && goodsList.length)) && (
            <View className='oneRowTwo-container'>
              {goodsList.map((item, index) => (
                <View className='oneRowTwo-item' key={index}>
                  <GoodsItem goodsItem={item} reportSource={reportSource} display='oneRowTwo' />
                </View>
              ))}

              {!!configData.limitNum && (
                <View className='all' onClick={handleToActivity}>
                  <Text>查看全部 &gt;</Text>
                </View>
              )}
            </View>
          )}

          {!!(configData.display === 'oneRowThree' && !!(goodsList && goodsList.length)) && (
            <View style={_safe_style_('overflow-x: auto;white-space:nowrap; display: flex; flex-wrap: wrap;')}>
              {goodsList.map((item, index) => (
                <View className='oneRowThree-item' key={index}>
                  <GoodsItem goodsItem={item} reportSource={reportSource} display='oneRowThree' />
                </View>
              ))}
            </View>
          )}
        </View>
      )}
    </View>
  );
};

SpecialActivityModule.defaultProps = {
  dataSource: {},
};

export default SpecialActivityModule;
