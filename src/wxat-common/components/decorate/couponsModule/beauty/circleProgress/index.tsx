import React, { useEffect, useState, FC } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { View, Canvas } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

type ComponentProps = {
  // require
  bgId: string;
  // require
  drawId: string;
  bgColor: string;
  drawColor: string;
  // rpx尺寸  cavas的高度和宽度就是size
  size: number;
  strokeWidth: number;
  progress: number;
};

const CircleProgress: FC<ComponentProps> = (props) => {
  const { bgId, drawId, bgColor, drawColor, size, strokeWidth, progress } = props;

  const [pxSize, setPxSize] = useState(0);

  const setRPXtoPX = (value) => {
    const screenWidth = wxApi.getSystemInfoSync().windowWidth;
    const scale = 375 / screenWidth;
    const px = value / scale / 2;
    setPxSize(px);
    // 绘制背景圆环
    drawCircleBg(bgId, px / 2, strokeWidth);
    // 绘制彩色圆环
    drawCircle(drawId, px / 2, strokeWidth, progress);
  };

  useEffect(() => {
    setRPXtoPX(size);
  }, [setRPXtoPX, size]);

  /*
   * 有关参数
   * id : canvas 组件的唯一标识符 canvas-id
   * x : canvas 绘制圆形的半径
   * w : canvas 绘制圆环的宽度
   */
  const drawCircleBg = (id, x, w) => {
    // 使用 wx.createContext 获取绘图上下文 ctx  绘制背景圆环
    const ctx = wxApi.createCanvasContext(id, this);
    ctx.setLineWidth(w);
    ctx.setStrokeStyle(bgColor);
    ctx.setLineCap('round');
    ctx.beginPath(); //开始一个新的路径
    //设置一个原点(x,y)，半径为r的圆的路径到当前路径 此处x=y=r
    ctx.arc(x, x, x - w, 0, 2 * Math.PI, false);
    ctx.stroke(); //对当前路径进行描边
    ctx.draw();
  };
  const drawCircle = (id, x, w, step) => {
    // 使用 wx.createContext 获取绘图上下文 context  绘制彩色进度条圆环
    // step = step / 2
    const context = wxApi.createCanvasContext(id, this);
    // gradient.addColorStop("0.5", "#40ED94");
    // gradient.addColorStop("1.0", "#5956CC");
    context.setLineWidth(w);
    context.setStrokeStyle(drawColor);
    context.setLineCap('round');
    context.beginPath(); //开始一个新的路径
    // step 从0到1为一周
    context.arc(x, x, x - w, -Math.PI / 2, ((step * 2) / 100) * Math.PI - Math.PI / 2, false);
    context.stroke(); //对当前路径进行描边
    context.draw();
  };
  return (
    <View className='circle_box' style={'width:' + pxSize + 'px;height:' + pxSize + 'px'}>
      <Canvas className='circle_bg' canvasId={bgId} style={'width:' + pxSize + 'px;height:' + pxSize + 'px'}></Canvas>
      <Canvas
        className='circle_draw'
        canvasId={drawId}
        style={'width:' + pxSize + 'px;height:' + pxSize + 'px'}
      ></Canvas>
      {this.props.children}
    </View>
  );
};

CircleProgress.defaultProps = {
  bgId: '',
  // require
  drawId: '',
  bgColor: '#e5e9f2',
  drawColor: '#f05c5c',
  // rpx尺寸  cavas的高度和宽度就是size
  size: 50,
  strokeWidth: 2,
  progress: 0,
};

export default CircleProgress;
