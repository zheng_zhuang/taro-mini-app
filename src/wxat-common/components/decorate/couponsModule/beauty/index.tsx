import React, { FC } from 'react';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import Circle from "./circleProgress";

import getStaticImgUrl from '../../../../constants/frontEndImgUrl'
import './index.scss';

interface ComponentProps {
  PADDING: number;
  dataList: Array<Record<string, any>>;
}

const CouponsBeautyModule: FC<ComponentProps> = ({ PADDING, dataList }) => {
  const clickReceive = (e) => {
    const myEventDetail = {
      item: e.currentTarget.dataset.item,
    };

    this.triggerEvent('receive', myEventDetail);
  };

  const gotoUse = (e) => {
    const myEventDetail = {
      item: e.currentTarget.dataset.item,
    };

    this.triggerEvent('use', myEventDetail);
  };

  return (
    dataList && (
      <View className='beauty-coupons-module-container' style={'padding: 0 ' + (PADDING / 2) + 'px'}>
        <Image
          className='coupons-module-header'
          src={getStaticImgUrl.images.coupons_header_png}
          mode='widthFix'
        ></Image>
        <View className='beauty-coupons-module-content'>
          {dataList.map((item, index) => {
            return (
              <View className='beauty-coupons-module-item' key={index} data-item={item} onClick={clickReceive}>
                <Image className='bg' src={getStaticImgUrl.images.coupons_bg_png}></Image>
                <View className='beauty-item-container'>
                  <View className='icon'>
                    <View className='icon-view'>{item.discountFee / 100}</View>
                    <View className='icon-view'>{'满' + item.minimumFee / 100 + '可用'}</View>
                  </View>
                  <View className='content'>
                    <View className='title'>{item.name}</View>
                    {item.couponType === 0 ? (
                      <View className='time-range'>{'有效期:' + item.fixedTerm + '天'}</View>
                    ) : (
                      <View className='time-range'>{'有效期:' + item.beginTime + '至' + item.endTime}</View>
                    )}

                    <View className='time-range'>
                      {'领取方式:' + (item.receiveMethod === 0 ? '免费' : item.integral + '积分')}
                    </View>
                  </View>
                  {!item.alreadyHave ? (
                    <View className='right111'>
                      <View className='progress right111-view'>
                        <Circle
                          bgId='circle_bg1'
                          drawId='circle_draw1'
                          size={66}
                          progress={item.percentage ? item.percentage : 0}
                        ></Circle>
                        <View className='value'>
                          <View>已抢</View>
                          <View>{(item.percentage ? item.percentage : 0) + '%'}</View>
                        </View>
                      </View>
                      <View className='right111-view'>立即领取</View>
                    </View>
                  ) : (
                    <View className='right-use' data-item={item} onClick={gotoUse}>
                      {/*<Image className='coupon-state' src='@/images/mine/Received.png'></Image>*/}
                      <Text className='coupon-used'>去使用</Text>
                    </View>
                  )}
                </View>
              </View>
            );
          })}
        </View>
      </View>
    )
  );
};

CouponsBeautyModule.defaultProps = {
  PADDING: 0,
  dataList: [],
};

export default CouponsBeautyModule;
