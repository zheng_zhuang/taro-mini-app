import React, { FC } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, ScrollView, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../../utils/money.wxs.js';
import couponEnum from '../../../../constants/couponEnum.js';
import subscribeMsg from '../../../../utils/subscribe-msg.js';
import subscribeEnum from '../../../../constants/subscribeEnum.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import './index.scss';

const decorateImg = cdnResConfig.decorate;

// props的类型定义
interface ComponentProps {
  _renderList: Array<Record<string, any>>;
  PADDING: number;
  onReceive: (object) => any;
  onUse: (object) => any;
}

const couponDisabledBg = cdnResConfig.coupon.disableBg;

const CouponsRetailModule: FC<ComponentProps> = ({ _renderList = [], PADDING, onReceive }) => {
  const clickReceive = (item) => {
    // const myEventDetail = {
    //   item: e.currentTarget.dataset.item
    //   // 授权订阅消息
    // };
    const Ids = [subscribeEnum.COUPONS_ARRIVE.value, subscribeEnum.COUPONS_OVERDUE.value];

    subscribeMsg.sendMessage(Ids).then(() => {
      // this.triggerEvent('receive', myEventDetail)
      onReceive && onReceive(item);
    });
  };

  const getCouponTypeLabel = (item) => {
    if (item.couponCategory === couponEnum.TYPE.freight.value) {
      return couponEnum.TYPE.freight.label;
    } else if (item.useScopeType === couponEnum.SCOPE.Universal.value) {
      return couponEnum.SCOPE.Universal.label;
    } else if (item.useScopeType === couponEnum.SCOPE.Online.value) {
      return couponEnum.SCOPE.Online.label;
    } else if (item.useScopeType === couponEnum.SCOPE.offline.value) {
      return couponEnum.SCOPE.offline.label;
    }
  };

  return (
    <ScrollView data-scoped='wk-dcr-Retail' className='wk-dcr-Retail' scrollX>
      {!!_renderList.length && (
        <View
          className='retail-coupons-module-container'
          style={{
            paddingLeft: Taro.pxTransform(PADDING),
          }}
        >
          {_renderList.map((item, index) => {
            return (
              <Block key={index}>
                <View className='retail-item' onClick={_fixme_with_dataset_(() => clickReceive(item), { item: item })}>
                  <View className={'retail-item-container ' + (item.canReceive ? '' : 'disabled')}>
                    {item.canReceive ? (
                      <Image
                        className="coupon-bg"
                        src={
                          item.couponBackgroundStyle
                            ? item.couponBackgroundUrl
                            : 'https://front-end-1302979015.file.myqcloud.com/images/c/images/decorate/coupon-default.png'
                        }
                      ></Image>
                    ) : (
                      <Image className="coupon-bg" src={couponDisabledBg}></Image>
                    )}

                    {getCouponTypeLabel(item) ? <View className='coupon-type'>{getCouponTypeLabel(item)}</View> : null}

                    {item.couponCategory === couponEnum.TYPE.freight.value && (
                      <Block>
                        {item.discountFee === 0 ? (
                          <View className='face-value'>免运费</View>
                        ) : (
                          <View className='face-value-with-money'>{filters.moneyFilter(item.discountFee, true)}</View>
                        )}
                      </Block>
                    )}

                    {item.couponCategory === couponEnum.TYPE.fullReduced.value && (
                      <Block>
                        <View className='face-value-with-money'>{filters.moneyFilter(item.discountFee, true)}</View>
                      </Block>
                    )}

                    {item.couponCategory === couponEnum.TYPE.discount.value && (
                      <Block>
                        <View className='face-value'>{filters.discountFilter(item.discountFee, true) + '折'}</View>
                      </Block>
                    )}

                    {item.minimumFee ? (
                      <View className='require-value'>
                        {'满' + filters.moneyFilter(item.minimumFee, true) + '元可用'}
                      </View>
                    ) : (
                      <View className='require-value'>无门槛</View>
                    )}
                  </View>
                </View>
              </Block>
            );
          })}
        </View>
      )}
    </ScrollView>
  );
};

// 给props赋默认值
CouponsRetailModule.defaultProps = {
  _renderList: [],
  PADDING: 0,
};

export default CouponsRetailModule;
