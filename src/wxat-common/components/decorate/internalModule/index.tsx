import React, { FC, useRef } from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View, Image, Text, Button } from '@tarojs/components';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import floatNum from '@/wxat-common/utils/float-num.js';
import api from '@/wxat-common/api/index.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';

import './index.scss';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const internalImg = cdnResConfig.internal;

const getPrice = (arg1, arg2) => {
  return floatNum.floatSub(arg1, arg2);
};

// props的类型定义
type ComponentProps = {
  dataList: any[];
  showNumberType?: number;
  PADDING?: number;
  onDeficiency: (object) => any;
  internalId?: string;
};

let InternalModule: FC<ComponentProps> = ({ dataList = NOOP_ARRAY, showNumberType, onDeficiency, internalId }) => {
  const checking = useRef(false);

  //获取内购券数量
  const getVoucherNum = async (item) => {
    const res = await wxApi.request({
      url: api.internalVoucher.getVoucherNum,
      loading: true,
      data: {},
    });

    const voucherNum = res.data || 0;
    //判断内购券是否充足
    if (voucherNum >= item.innerBuyCoupon) {
      goDetail(item);
    } else {
      onDeficiency(item);
    }
  };

  const goDetail = (item) => {
    checking.current = false;

    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        type: goodsTypeEnum.INTERNAL_BUY.value,
        itemNo: item.itemNo,
        activityId: item.id,
        distributorId: internalId,
      },
    });
  };

  const onClickGoodsItem = async (item) => {
    if (!authHelper.checkAuth()) {
      return;
    }

    //防止多次点击
    if (checking.current) {
      return;
    }

    try {
      checking.current = true;
      await getVoucherNum(item);
    } finally {
      checking.current = false;
    }
  };

  return (
    <View data-scoped='wk-cdi-InternalModule' className='wk-cdi-InternalModule goods-box'>
      {dataList.map((item) => {
        return (
          <View className='goods-item' key={item.id} onClick={() => onClickGoodsItem(item)}>
            <View className='goods-content'>
              <Image className='goods-content__image' src={item.thumbnail}></Image>
              <View className='goods-info'>
                <View className='goods-title limit-line line-2'>{item.name}</View>
                <View className='goods-price'>
                  <View className='internal-price'>
                    <View className='price limit-line'>
                      <Text className='price__text'>￥</Text>
                      {filters.moneyFilter(item.innerBuyPrice, true)}
                    </View>
                    <View className='goods-label'>内购价</View>
                  </View>
                  <View className='market-price'>
                    <View className='price limit-line'>
                      <Text className='price__text'>￥</Text>
                      {filters.moneyFilter(item.salePrice, true)}
                    </View>
                    <View className='goods-label'>市场价</View>
                  </View>
                </View>
              </View>
            </View>
            <View className='goods-rule'>
              <View className='rule'>
                {'需使用 ' +
                  item.innerBuyCoupon +
                  ' 张内购券，节省' +
                  filters.moneyFilter(getPrice(item.salePrice, item.innerBuyPrice), true) +
                  '元'}
              </View>
              <ButtonWithOpenType openType='share' className='share' item={item}>
                <Image className='share__image' src={internalImg.shareIcon}></Image>
                分享
              </ButtonWithOpenType>
            </View>
          </View>
        );
      })}
    </View>
  );
};

// 给props赋默认值
InternalModule.defaultProps = {
  dataList: [],
  showNumberType: 1,
  PADDING: 0,
};

export default InternalModule;
