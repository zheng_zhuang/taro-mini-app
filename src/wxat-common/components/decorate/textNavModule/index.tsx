import React, {FC} from 'react';
import {_safe_style_} from '@/wxat-common/utils/platform';
import {View, Image} from '@tarojs/components';
import {useSelector} from 'react-redux';
import Taro from '@tarojs/taro';
import jumpPath from '../utils/jump-path.js';
import reportConstants from '../../../../sdks/buried/report/report-constants';
import report from "../../../../sdks/buried/report";
import authHelper from '../../../utils/auth-helper';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';
import getStaticImgUrl from "../../../constants/frontEndImgUrl"
import useToYQZ from '@/hooks/useToYQZ';
import util from '@/wxat-common/utils/util';

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
  padding?: number;
  display?: string;
  reportSource?: string;
  onClick?: () => void;
  onJumpHandler?: (item) => void;
}

const TextNavModule: FC<ComponentProps> = (props) => {
  // 提前解构props，减少getter的操作
  const {dataSource, display, reportSource, onClick, onJumpHandler} = props;
  const {handleDecorateToQYZ} = useToYQZ()
  const click = async () => {
    const item = dataSource;
    if (item.linkPage != null) {
      // TODO:
      report.clickTextNavigation(reportSource || reportConstants.SOURCE_TYPE.text_nav.key, item.linkPage);
      if (item.linkPageKey && String(item.linkPageKey).includes('yqz')) {
        await handleDecorateToQYZ(item)
        return
      }

      if (item.customJump && onJumpHandler) {
        return onJumpHandler(item);
      }
      if (item.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }

      let tempDisplay = display;
      // 展示方式为滑动时，使用一行两个的展示形式
      if (tempDisplay === 'horizon') {
        tempDisplay = 'oneRowTwo';
      }

      // 判断是否是住中服务进来的
      if (item.linkPageKey === 'service-category' || item.serviceItemId || item.wifiCateGoryId) {
        if (item.linkPage === 'wxat-common/pages/goods-detail/index') {
          item.linkPage = 'sub-packages/marketing-package/pages/living-service/goods-rental-details/index';

        }
        wxApi.$navigateTo({
          url: jumpPath(
            item,
            display,
            reportSource || reportConstants.SOURCE_TYPE.text_nav.key
          ).url,
          data: {
            id: item.serviceItemId ? item.serviceItemId : item.serviceCateGoryId,
            sourceType: item.sourceType,
          },
        });
      } else {
        const {url} = jumpPath(
          item,
          display,
          reportSource || reportConstants.SOURCE_TYPE.text_nav.key
        )
        if (!url) {
          if (onClick) {
            onClick();
            return
          }
        }
        wxApi.$navigateTo({
          url: url,
          data: {type: 1, display},
        });
      }
    }

    if (onClick) {
      onClick();
    }
    util.sensorsReportData('product_list_more', {
      event_name: '商品列表查看更多'
    })
  };

  return (
    <View
      data-scoped='wk-cdt-TextNavModule'
      className='wk-cdt-TextNavModule text-nav-module'
      style={{
        // padding: Taro.pxTransform(padding),
        margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(dataSource.marginLeftRight * 2)}`,
        borderRadius: Taro.pxTransform(dataSource.radius * 2),
        backgroundColor: dataSource.bgColor,
      }}
    >
      <View
        className='text-nav-module-container'
        style={_safe_style_('backgroundColor: ' + dataSource.bgColor)}
        onClick={click}
      >
        <View
          className='text-nav-module-title'
          style={_safe_style_(
            'text-align:' +
            dataSource.textAlign +
            ';background-color:' +
            dataSource.backgroundColor +
            ';color:' +
            dataSource.titleColor +
            ';'
          )}
        >
          <View className='title'>{dataSource.title}</View>
        </View>
        {dataSource.showMore == 1 && (
          <View className='more'>
            <View style={_safe_style_('display: inline-block;color:' + dataSource.subTitleColor)}>
              {dataSource.subTitle}
            </View>
            <Image className='right-angle' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
          </View>
        )}
      </View>
    </View>
  );
};

// 给props赋默认值
TextNavModule.defaultProps = {
  dataSource: {},
  padding: 20,
  display: 'vertical',
  reportSource: reportConstants.SOURCE_TYPE.text_nav.key,
};

export default TextNavModule;
