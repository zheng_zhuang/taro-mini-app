import React, { useEffect, useState, FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import wxApi from '../../../utils/wxApi';
import imageUtils from '../../../utils/image.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import Textnavmodule from '../textNavModule';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import './index.scss';

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
  limit?: number;
  showNav?: boolean;
}

const VRModule: FC<ComponentProps> = ({ dataSource, limit, showNav }) => {
  const defaultComputeDataList: Array<Record<string, any>> = [];
  const [VRList, setVRList] = useState(defaultComputeDataList);

  useEffect(() => {
    if (!!dataSource && !!dataSource.data) {
      const temp = (dataSource.data || []).filter((item) => {
        return item.imgUrl && (item.imgUrl = imageUtils.cdn(item.imgUrl));
      });

      setVRList(temp.slice(0, limit));
    }
  }, [dataSource]);

  const jumpToVR = ({ index }, e) => {
    const VRLink = VRList[index].VRLink;

    if (!VRLink) return;

    protectedMailBox.send('sub-packages/marketing-package/pages/receipt/index', 'receiptURL', VRLink);

    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/receipt/index',
      data: {},
    });
  };

  const jumpMoreHandler = (item) => {
    protectedMailBox.send(item.linkPage, 'VRData', dataSource);

    wxApi.$navigateTo({
      url: item.linkPage,
      data: {},
    });
  };

  return (
    <View
      data-scoped='wk-cdp-VRModule'
      className='wk-cdp-VRModule'
      style={_safe_style_(
        'margin:' + (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') + ';'
      )}
    >
      {!!(!!dataSource.showNav && showNav) && (
        <Textnavmodule dataSource={dataSource.textNavSource} onJumpHandler={jumpMoreHandler}></Textnavmodule>
      )}

      {!!VRList.length && (
        <View>
          {VRList.map((item, index) => {
            return (
              <View
                className='vr-item'
                key={index}
                onClick={jumpToVR.bind(null, { index })}
                style={_safe_style_('border-radius: ' + (dataSource.radius + 'px'))}
              >
                <View className='img-box'>
                  <Image className='img' src={item.imgUrl} mode='aspectFill'></Image>
                  {!!item.VRLink && <Image className='vr-label' src={cdnResConfig.micro_decorate.VRLabel}></Image>}
                </View>
                {!!item.VRTitle && <View className='vr-title limit-line'>{item.VRTitle}</View>}
              </View>
            );
          })}
        </View>
      )}
    </View>
  );
};

// 给props赋默认值
VRModule.defaultProps = {
  dataSource: {},
  limit: 1,
  showNav: true,
};

export default VRModule;
