import React, { FC, useMemo } from 'react';
import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import filters from '../../../utils/money.wxs.js';
import MarketingItem from '../../marketing-item/index';
import './index.scss';

const SHOW_TYPE = {
  vertical: 1, // 经典列表
  horizon: 4, // 水平滑动
  rowOne: 0, // 一行一个
  rowTwo: 2, // 一行两个
  rowThree: 3, // 一行三个
};

// props的类型定义
type ComponentProps = {
  computeDataList: Array<Record<string, any>>;
  showType: number;
  type?: number;
  customContent?: boolean;
  onClick?: (object) => any;
};

/**
 * 处理多种样式的展示形式
 * 传container是父级class，item是子级class
 * @param {type} item or container
 * @returns className
 */
const getClassByShowType = (showType, type: string) => {
  switch (showType) {
    case SHOW_TYPE.vertical:
      return 'p-vertical-' + type;
    case SHOW_TYPE.horizon:
      return 'p-horizon-' + type;
    case SHOW_TYPE.rowOne:
      return 'p-oneRowOne-' + type;
    case SHOW_TYPE.rowTwo:
      return 'p-oneRowTwo-' + type;
    case SHOW_TYPE.rowThree:
      return 'p-oneRowThree-' + type;
  }

  return '';
};

let MarketingModule: FC<ComponentProps> = ({ computeDataList, showType, customContent, onClick }) => {
  const itemClass = useMemo(() => getClassByShowType(showType, 'item'), [showType]);
  const containerClass = useMemo(() => getClassByShowType(showType, 'container'), [showType]);

  // const go = e => {
  //   const myEventDetail = {
  //     item: e.detail.item
  //   };
  //   this.triggerEvent('click', myEventDetail);
  // };
  const handleGoToCardDetail = (item) => {
    // const myEventDetail = {
    //   item: e.currentTarget.dataset.item
    // };
    // this.triggerEvent('click', myEventDetail);
    onClick && onClick(item);
  };

  return (
    <View data-scoped='wk-cdm-MarketingModule' className='wk-cdm-MarketingModule module-container'>
      <View className={containerClass}>
        {computeDataList.map((item, index) => {
          return (
            <View className={itemClass} key={index}>
              <MarketingItem
                // onClick={go}
                onClick={() => handleGoToCardDetail(item)}
                marketingItem={item}
                showType={showType}
              ></MarketingItem>
            </View>
          );
        })}
      </View>
    </View>
  );
};

// 给props赋默认值
MarketingModule.defaultProps = {
  computeDataList: [],
  showType: 0,
  type: 0,
  customContent: false,
};

export default MarketingModule;
