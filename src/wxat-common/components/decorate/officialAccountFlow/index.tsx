import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import './index.scss'


class OfficialAccountFlow extends React.Component{
  static defaultProps = {
    imageUrl: '', // 公众号图片
    // dialog距离顶部的距离，支持rpx和百分比，属性值通css top
    top: '45%',
    content: '长按识别二维码关注我们',
    visible: false,
    backOpacity: '0.1', // 背景透明度
    contentBackgroundColor: '#f8ffff',
    contentBoxShadow: '0px 2px 12px 0px rgba(0,0,0,0.13)',
    borderRadius: '4px',
    width: '100%',
    maxWidth: '295px',
    // 内容的外边距
    contenMargin: '20px 0 57px 0',
    contentPadding: '0 15px',
    // 确认回调函数
    onConfirm: null,
    // 取消回调函数
    onCancel: null,
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  } 

  /**
   * 隐藏dialog
   */
     _cancelEvent = () => {
      const onCancel = this.props.onCancel;
      onCancel && onCancel();
    };


  render(){
    const {
      imageUrl,
      content,
      backOpacity,
      top,
      borderRadius,
      width,
      maxWidth,
      contentBackgroundColor,
      contentBoxShadow,
      contenMargin,
      contentPadding,
      visible,
    } = this.props;

    const dialogStyle = {
      top,
      width,
      maxWidth,
      borderRadius: borderRadius,
      backgroundColor: contentBackgroundColor,
      boxShadow: contentBoxShadow,
    };

    return (
      <View className="decorate-official-account-flow-view">
        <View className='wx-mask' style={{ opacity: backOpacity }} onClick={this._cancelEvent}></View>

        <View className='decorate-official-account-dialog' style={_safe_style_(dialogStyle)}>
          <Text className="title"> 关注公众号</Text>

          <Text className='cancel-icon' onClick={this._cancelEvent}>X</Text>

          <Image className="official-account-image" src={imageUrl}></Image>
          <Text  className='tips'>{content}</Text>
        
        </View>
      </View>
    )
  }
}


export default OfficialAccountFlow;
