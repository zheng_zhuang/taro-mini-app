import React, { FC, useMemo } from 'react';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import MarketingMoudle from '../marketingModule/index';
import './index.scss';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';

type ComponentProps = {
  PADDING: number;
  dataList: Array<Record<string, any>>;
  showGroupType: number;
  onClick?: (object) => any;
};

let CollageModule: FC<ComponentProps> = ({ showGroupType, dataList, onClick }) => {
  const computeDataList = useMemo(
    () =>
      dataList
        ? dataList.map((item) => {
            return {
              id: item.id,
              itemNo: item.itemNo,
              name: item.name,
              collageLabel: item.minPeople + '人团',
              tipLabel: item.minPeople + '人团',
              thumbnail: item.backgroundUrl || item.thumbnail,
              salePrice: item.minPrice * 100,
              labelPrice: item.salePrice,
              saleAmount: item.saleCount + '人已拼团',
              saleLabel: '拼团价：',
              sign: '拼',
              btn: '立即拼团',
            };
          })
        : NOOP_ARRAY,
    [dataList]
  );

  return (
    <View
      data-fixme='03 add view wrapper. need more test'
      data-scoped='wk-cdc-CollageModule'
      className='wk-cdc-CollageModule'
    >
      <MarketingMoudle computeDataList={computeDataList} showType={showGroupType} onClick={onClick}></MarketingMoudle>
    </View>
  );
};

CollageModule.defaultProps = {
  PADDING: 0,
  dataList: [],
  showGroupType: 0,
};

export default CollageModule;
