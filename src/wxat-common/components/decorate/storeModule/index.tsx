import React, { useEffect, useState, FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Block } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import login from "../../../x-login";
import config from '../utils/config.js';
import api from "../../../api";
import buyHub from '../../cart/buy-hub.js';
import cartHelper from '../../cart/cart-helper.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import template from '../../../utils/template.js';
import report from '@/sdks/buried/report/index.js';
import businessTime from '../../../utils/businessTime.js';
import { useSelector, useDispatch } from 'react-redux';
import './index.scss';
import { updateBaseAction } from '@/redux/base';
import { ITouchEvent } from '@tarojs/components/types/common';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import useTemplateStyle from '@/hooks/useTemplateStyle';

const decorateImg = cdnResConfig.decorate;

interface stateDefine {
  base: Record<string, any>;
}

// props的类型定义
interface ComponentProps {
  padding?: number;
  ableChange?: boolean;
  ignoreScanCode: boolean;
  dataSource: Record<string, any>;
}

const StoreModule: FC<ComponentProps> = (props) => {
  // //提前解构props，减少getter的操作
  const { padding, ableChange, ignoreScanCode, dataSource } = props;

  const currentStore = useSelector((state: stateDefine) => state.base.currentStore);
  const appInfo = useSelector((state: stateDefine) => state.base.appInfo);
  const storeList = useSelector((state: stateDefine) => state.base.storeList);
  const gps = useSelector((state: stateDefine) => state.base.gps);
  const dispatch = useDispatch();
  const updateBase = (data) => dispatch(updateBaseAction(data));

  const [showScanCode, setShowScanCode] = useState(false);
  const templ = useTemplateStyle({ autoSetNavigationBar: true });

  useEffect(() => {
    if (!currentStore.id) {
      login.waitLogin().then(() => {
        setInfo();
      });
    } else {
      setInfo();
    }
  }, [currentStore]);

  useEffect(() => {
    if (appInfo) {
      setShowScanCode(!!appInfo.scanCode);
    }
  }, [appInfo]);

  const setInfo = () => {
    if (!storeList || storeList.length === 0) {
      const { longtitude, latitude } = gps;
      const url = longtitude ? api.store.queryCustomizeShop : api.store.list;
      // if (latitude) {
      //   url += '&latitude=' + latitude;
      //   url += '&longtitude=' + longtitude;
      // }
      wxApi
        .request({ url})
        // wxApi.request({url: api.store.list})
        .then((res) => {
          if (res.success) {
            const list = (res.data || []).map((store) => ({
              ...store,
              businessTime: businessTime.computed(
                store.businessStartHour,
                store.businessEndHour,
                store.businessDayOfWeek
              ),
            }));

            updateBase({
              storeList: list,
            });
          }
        });
    }
    // report.firstStoreInfo(state.base.currentStore.id)
  };

  const clickLocation = (e: ITouchEvent) => {
    e.stopPropagation();
    report.clickStoreLocation();
    if (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true') {
      // wxApi.chooseLocation({
      //   latitude: parseFloat(currentStore.latitude),
      //   longitude: parseFloat(currentStore.longitude),
      //   name: currentStore.abbreviation || currentStore.name,
      //   address: currentStore.address,
      //   success: function (res) {
      //     const {latitude,longitude,address,name} = res
      //     window.location.href =`http://api.map.baidu.com/marker?location=${latitude},${longitude}&title=${address}&content=${name}&output=html`
      //   }
      // })
      wx.openLocation({
        latitude: parseFloat(currentStore.latitude),
        longitude: parseFloat(currentStore.longitude),
        name: currentStore.abbreviation || currentStore.name,
        address: currentStore.address,
        success: function (res) {
          console.log(res);
        }
      });
    }else{
        wxApi.openLocation({
          latitude: parseFloat(currentStore.latitude),
          longitude: parseFloat(currentStore.longitude),
          name: currentStore.abbreviation || currentStore.name,
          address: currentStore.address,
          success: function (res) {
            console.log(res);
          }
        });
    }
  };

  const clickPhone = (e: ITouchEvent) => {
    e.stopPropagation();

    report.clickStorePhone();
    wxApi.makePhoneCall({
      phoneNumber: currentStore.tel,
    });
  };

  const clickStore = () => {
    if (ableChange && storeList && storeList.length > 1) {
      wxApi.$navigateTo({
        url: '/sub-packages/moveFile-package/pages/store-list/index',
      });
    }
  };
  const clickScanCode = (e: ITouchEvent) => {
    e.stopPropagation();

    report.clickSweepCode();
    wxApi.$navigateTo({
      url: '/wxat-common/pages/scan-code/shopping-cart/index',
    });

    // wx.scanCode({
    //   success: (res) => {
    //     // 二维码图形
    //     // res.result
    //     const barCode = res.result;
    //     if (barCode) {
    //       // 查询商品
    //       this.queryGoodsFromBarCode(barCode);
    //     }
    //   }
    // })
  };

  const queryGoodsFromBarCode = (barCode) => {
    wxApi
      .request({
        url: api.goods.skuItem,
        data: {
          barcode: barCode,
          itemType: goodsTypeEnum.PRODUCT.value,
          status: 1,
          isShelf: 1,
        },
      })
      .then((res) => {
        if (res.success) {
          // 多做一层barcode和itemNo的判断，避免返回data，但是里面数据为空的情况
          if (res.data && res.data.barcode && res.data.itemNo && res.data.itemShelf) {
            // 通过条形码取到商品数据
            const item = buyHub.formatSku(res.data);
            joinCarts(item);
          } else {
            // 没拿到数据
            wxApi.showToast({
              title: '商品未上架',
              icon: 'none',
            });
          }
        }
      })
      .catch((err) => {
        console.log('queryVerificationStatus error -> ' + JSON.stringify(err));
      });
  };

  const joinCarts = (item) => {
    // 1、查询加入购物车的数量和库存的数的比较
    // 通过itemNo在购物车池子中拿数据
    // buyHub.map

    if (!cartHelper.isGettingCarts() && !cartHelper.isGettedCarts()) {
      wxApi.showToast({
        title: '正在请求购物车数据，请稍后重试',
        icon: 'none',
      });

      // 只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
      if (!cartHelper.isGettingCarts()) {
        cartHelper.getCarts(true);
      }
      return;
    }

    let isJoin = false; // 是否可以加入购物车
    if (item.stock > 0) {
      const obj = buyHub.map;
      const x = obj.hasOwnProperty(buyHub.uuid(item));
      if (x) {
        // 购物车有这个商品数据，且加入的数量  小于  库存数额
        const keys = Object.keys(obj);
        let i = keys.length;
        while (i--) {
          const key = keys[i];
          const cartItem = obj[key];
          if (cartItem.itemNo === item.itemNo) {
            if (cartItem.count < cartItem.stock) {
              isJoin = true;
              break;
            }
          }
        }
        // for (const key in obj) {
        // const cartItem = obj[key];
        // if (cartItem.itemNo === item.itemNo) {
        //   if (cartItem.count < cartItem.stock) {
        //     isJoin = true;
        //     break;
        //   }
        // }
        // }
      } else {
        // 购物车没有 且有库存 直接加
        isJoin = true;
      }
    }

    if (isJoin) {
      cartHelper.addOrUpdateCart(item, 1, true, (res) => {
        wxApi.showToast({
          title: '添加成功',
          icon: 'success',
        });
      });
    } else {
      wxApi.showToast({
        title: '商品库存不足',
        icon: 'none',
      });
    }
  };

  const templateStyle = template.getTemplateStyle();

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-cds-StoreModule' className='wk-cds-StoreModule'>
      {!!currentStore && (
        <View
          className='store-module'
          onClick={clickStore}
          style={_safe_style_(
            'margin:' +
              (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
              ';border-radius: ' +
              dataSource.radius +
              'px;'
          )}
        >
          <View
            className='store-img'
            style={_safe_style_(
              'background: transparent url(' + currentStore.logo + ') no-repeat center center / 100% 100%;'
            )}
          ></View>
          <View>
            <View className='store-name'>{currentStore.abbreviation || currentStore.name}</View>
            <View className='address'>{currentStore.address}</View>
          </View>
          <View className='right'>
            <Image className='right-icon' onClick={clickLocation} src={decorateImg.location}></Image>
            <Image className='right-icon' onClick={clickPhone} src={decorateImg.phone}></Image>
            {!!(!ignoreScanCode && showScanCode) && (
              <View
                className='scan-code-container'
                onClick={clickScanCode}
                style={_safe_style_('background-color: ' + templateStyle.btnColor)}
              >
                <Image className='right-icon1' src={decorateImg.scanCode}></Image>
              </View>
            )}
          </View>
        </View>
      )}
    </View>
  );
};
// 给props赋默认值
StoreModule.defaultProps = {
  // currentStore: {},
  // appInfo: {},
  // storeList: [],
  // gps: {},
  // updateBase: () => { },

  ableChange: false,
  padding: config.PADDING,
  ignoreScanCode: true,
  dataSource: {},
};

export default StoreModule;

// 下面的属于包装器  可以不用看
// type PageStateProps = {
//   currentStore: Record<string, any>
//   appInfo: Record<string, any>
//   storeList: Array<Record<string, any>>
//   gps: Record<string, any>
// }
// type PageDispatchProps = {
//   updateBase: (data) => void
// }
// type PageOwnProps = {
//   ableChange: boolean
//   padding: number
//   ignoreScanCode: boolean
//   dataSource: Record<string, any>
// }

// type PageState = {}

// interface StoreModuleWrap {
//   props: PageStateProps & PageDispatchProps & PageOwnProps
// }

// const mapStateToProps = state => ({
//   currentStore: state.base.currentStore,
//   appInfo: state.base.appInfo,
//   storeList: state.base.storeList,
//   gps: state.base.storeList,
// })

// const mapStateToDispatch = dispatch => ({
//   updateBase: data => dispatch(updateBaseAction(data))
// });
// @connect(mapStateToProps, mapStateToDispatch)
// class StoreModuleWrap extends Component {
//   render() {
//     return <StoreModule {...this.props}></StoreModule>
//   }
// }

// export default StoreModuleWrap as ComponentClass<PageOwnProps, PageState>
