import objectUtil from '../../../utils/object.js';
import utils from '../../../utils/util.js';

export default {
  page: {
    //商城主页
    "mall-main-page": {
      newPath: "wxat-common/pages/slot-page/index?mallMainPageId=",
      oldPath: "pages/custom/index?mallMainPageId=",
      specialAssemble: (newLinkPage, oldLinkPage) => {
        const route = utils.getQueryString(oldLinkPage, 'mallMainPageId');
        return newLinkPage += route;
      }
    },
    // 卡包详情
    'card-pack-detail': {
      newPath: 'sub-packages/marketing-package/pages/card-pack/detail/index',
      oldPath: 'wxat-common/pages/card-pack/detail/index',
    },
    // 卡包列表
    'card-pack-list': {
      newPath: 'sub-packages/marketing-package/pages/card-pack/list/index',
      oldPath: 'wxat-common/pages/card-pack/list/index',
    },
    // 微信授权
    'miniprogram-auth': {
      newPath: 'sub-packages/marketing-package/pages/miniprogram-auth/index',
      oldPath: 'wxat-common/pages/miniprogram-auth/index',
    },
    // 招募
    'receipt': {
      newPath: 'sub-packages/marketing-package/pages/receipt/index',
      oldPath: 'wxat-common/pages/receipt/index',
    },
    // 分销员招募
    'newreceipt': {
      newPath: 'sub-packages/distribution-package/pages/recruit/index',
      oldPath: 'wxat-common/pages/receipt/index',
    },
    // 会员码
    'member-code': {
      newPath: 'sub-packages/mine-package/pages/member-code/index',
      oldPath: 'wxat-common/pages/mine/member-code/index',
    },
    // 充值中心
    'recharge-center': {
      newPath: 'sub-packages/mine-package/pages/recharge-center/index',
      oldPath: 'wxat-common/pages/mine/recharge-center/index',
    },
    // "礼品卡商城"
    'gift-card-mall': {
      newPath: '/sub-packages/marketing-package/pages/gift-card/bg-list/index',
      oldPath: 'wxat-common/pages/gift-card/bg-list/index',
    },
    // "领券中心"
    'coupon-center': {
      newPath: 'sub-packages/mine-package/pages/coupon/index',
      oldPath: 'sub-packages/moveFile-package/pages/mine/coupon/index',
    },
    // "拼团列表"
    'group-list': {
      newPath: 'sub-packages/marketing-package/pages/group/list/index',
      oldPath: 'sub-packages/moveFile-package/pages/group/list/index',
    },
    // "拼团详情"
    'group-detail': {
      newPath: 'sub-packages/marketing-package/pages/group/detail/index?itemNo=',
      oldPath: 'wxat-common/pages/group/detail/index?itemNo=',
    },
    // 加入拼团
    'group-join': {
      newPath: 'sub-packages/marketing-package/pages/group/join/index',
      oldPath: 'sub-packages/moveFile-package/pages/group/join/index',
    },
    // "砍价列表"
    'cut-price-list': {
      newPath: 'sub-packages/marketing-package/pages/cut-price/list/index',
      oldPath: 'sub-packages/moveFile-package/pages/cut-price/list/index',
    },
    // "砍价详情"
    'cut-price-detail': {
      newPath: 'sub-packages/marketing-package/pages/cut-price/detail/index?activityId=',
      oldPath: 'sub-packages/moveFile-package/pages/cut-price/detail/index?activityId=',
    },
    // "参加砍价"
    'cut-price-join': {
      newPath: 'sub-packages/marketing-package/pages/cut-price/join/index',
      oldPath: 'wxat-common/pages/cut-price/join/index',
    },
    // "秒杀列表"
    'seckill-list': {
      newPath: 'sub-packages/marketing-package/pages/seckill/list/index',
      oldPath: 'wxat-common/pages/seckill/list/index',
    },
    // "秒杀详情"
    'seckill-detail': {
      newPath: 'sub-packages/marketing-package/pages/seckill/detail/index?itemNo=',
      oldPath: 'sub-packages/marketing-package/pages/seckill/detail/index?itemNo=',
    },
    // "商品列表"
    'product-list': {
      newPath: 'sub-packages/moveFile-package/pages/more/index?type=1',
      oldPath: 'wxat-common/pages/more/index?type=1',
    },
    // "商品分类"
    'product-category': {
      newPath: 'sub-packages/marketing-package/pages/category-detail/index?id=',
      oldPath: 'wxat-common/pages/category-detail/index?id=',
    },
    // "商品详情"
    'product-detail': {
      newPath: 'wxat-common/pages/goods-detail/index?itemNo=',
      oldPath: 'sub-packages/moveFile-package/pages/goods-detail/index?itemNo=',
    },
    // "图文列表"
    'graph-list': {
      newPath: 'sub-packages/marketing-package/pages/graph/list/index',
      oldPath: 'wxat-common/pages/graph/list/index',
    },
    // "图文详情"
    'graph-detail': {
      newPath: 'sub-packages/moveFile-package/pages/graph/detail/index?id=',
      oldPath: 'wxat-common/pages/graph/detail/index?id=',
    },
    // "赠品中心"
    'gift-center': {
      newPath: 'sub-packages/mine-package/pages/free-center/index',
      oldPath: '/sub-packages/moveFile-package/pages/mine/free-center/index',
    },
    // "赠品详情"
    'gift-detail': {
      newPath: 'wxat-common/pages/goods-detail/index?activityId=',
      oldPath: '/sub-packages/moveFile-package/pages/goods-detail/index?activityId=',
    },
    // "幸运转盘"
    'lucky-dial': {
      // newPath: 'sub-packages/marketing-package/pages/lucky-dial/index?id=',
      newPath: 'sub-packages/moveFile-package/pages/lucky-dial/index?id=',
      oldPath: 'sub-packages/moveFile-package/pages/lucky-dial/index?id=',
    },
    // "幸运转盘 中奖记录"
    'lucky-record': {
      newPath: 'sub-packages/marketing-package/pages/lucky-dial/lucky-record/index',
      oldPath: 'sub-packages/marketing-package/pages/lucky-dial/lucky-record/index',
    },
    // "专题活动"
    activity: {
      newPath: 'sub-packages/marketing-package/pages/activity/index?topicId=',
      oldPath: 'sub-packages/marketing-package/pages/activity/index?topicId=',
    },
    // "积分商城"
    integral: {
      newPath: 'sub-packages/moveFile-package/pages/mine/integral-mall/index',
      oldPath: 'sub-packages/moveFile-package/pages/mine/integral-mall/index',
    },
    'integral-index': {
      newPath: 'sub-packages/mine-package/pages/integral/index',
      oldPath: "wxat-common/pages/mine/integral/index",
    },
    // "附近门店"
    'nearby-store': {
      newPath: 'wxat-common/pages/store-list/index',
      oldPath: 'wxat-common/pages/store-list/index',
    },
    // "内购专区"
    'internal-purchase': {
      newPath: 'sub-packages/distribution-package/pages/internal-purchase/center/index',
      oldPath: 'sub-packages/distribution-package/pages/internal-purchase/center/index',
    },
    // "签到"
    'sign-in': {
      newPath: 'sub-packages/marketing-package/pages/signIn/index?id=',
      oldPath: 'wxat-common/pages/signIn/index?id=',
      specialAssemble: (newLinkPage, oldLinkPage) => {
        const signId = utils.getQueryString(oldLinkPage, 'id');
        return (newLinkPage += signId);
      }
    },

    // 美业行业特有的链接
    // "服务列表"
    'service-list': {
      newPath: 'sub-packages/marketing-package/pages/more/index?type=0',
      oldPath: 'wxat-common/pages/more/index?type=0',
    },
    // "服务分类"
    'server-category': {
      newPath: 'sub-packages/marketing-package/pages/category-detail/index?id=',
      oldPath: 'wxat-common/pages/category-detail/index?id=',
    },
    // "服务详情"
    'service-detail': {
      newPath: 'sub-packages/server-package/pages/serve-detail/index?itemNo=',
      oldPath: 'pages/serve-detail/index?itemNo=',
    },
    // "卡项列表"
    'card-list': {
      newPath: 'sub-packages/marketing-package/pages/more/index?type=4',
      oldPath: 'wxat-common/pages/more/index?type=4',
    },
    // "卡项详情"
    'card-detail': {
      newPath: 'sub-packages/server-package/pages/card-detail/index?itemNo=',
      oldPath: 'pages/card-detail/index?itemNo=',
    },
    'custom-page': {
      newPath: 'wxat-common/pages/slot-page/index',
      oldPath: 'wxat-common/pages/custom/index',
      specialAssemble: (newLinkPage, oldLinkPage) => {
        const queryString = oldLinkPage.split('?').slice(-1)[0];
        return `${newLinkPage}?$componentId=custom&${queryString}`;
      }
    }
  },
  getLinkPage(pageConfig) {
    const page = this.page;
    const copyPageConfig = objectUtil.clone(pageConfig);
    const { linkPage } = copyPageConfig;
    const linkPageKey = Object.keys(page).find((key) => {
      const pagePath = page[key];
      return pagePath.oldPath.indexOf(linkPage) !== -1 || linkPage.indexOf(pagePath.oldPath) !== -1;
    });
    console.log("linkPageKey",linkPageKey,page[linkPageKey])
    if (linkPage && linkPageKey && page[linkPageKey]) {
      const { newPath, specialAssemble } = page[linkPageKey];
      if (specialAssemble && typeof specialAssemble === 'function') {
        copyPageConfig.linkPage = specialAssemble(newPath, linkPage);
      } else {
        copyPageConfig.linkPage = newPath;
      }
    }
    return copyPageConfig;
  },
};
