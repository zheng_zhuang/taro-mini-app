import authHelper from "../../../utils/auth-helper.js";
import goodsTypeEnum from "../../../constants/goodsTypeEnum";
import Taro from '@tarojs/taro';
import pageLinkHandler from './pageLinkHandler'
const app = Taro.getApp();

function isOldPath (linkPage, oldPaths) {
  if (oldPaths && oldPaths.length) {
    const inPath = oldPaths.find(path => {
      return linkPage.indexOf(path) !== -1;
    });
    return !!inPath;
  } else {
    return false;
  }
}
export default (item, display, source) => {
  const pageConfig = pageLinkHandler.getLinkPage(item);
  const obj = {};
  const url = pageConfig.linkPage;
  //子包的页面以sub-packages开头
  /*if (
    item.linkPage.startsWith("sub-packages") ||
    item.linkPage.startsWith("/sub-packages")
  ) {
    if (!item.linkPage.startsWith("/")) {
      url += "/" + item.linkPage;
    }
  } else if (isOldPath(item.linkPage, app.globalData.oldServerPaths)) {
    //fixme 兼容之前首页装修组件里关于服务老的链接地址
    //私有路径，如果配有wxat-common，则剔除该路径前缀
    if (item.linkPage.startsWith("wxat-common")) {
      url = item.linkPage.replace("wxat-common", "sub-packages/server-package");
    } else {
      url += "/sub-packages/server-package/" + item.linkPage;
    }
  } else if (isOldPath(item.linkPage, app.globalData.oldMarketingPaths)) {
    //fixme 兼容之前首页装修组件里关于营销组件老的链接地址
    //私有路径，如果配有wxat-common，则剔除该路径前缀
    if (item.linkPage.startsWith("wxat-common")) {
      url = item.linkPage.replace(
        "wxat-common",
        "sub-packages/marketing-package"
      );
    } else {
      url += "/sub-packages/marketing-package/" + item.linkPage;
    }
  } else if (!item.linkPage.startsWith("wxat-common")) {
    //不是私有路径，如果没有wxat-common，则需要加上该前缀
    url += "/wxat-common/" + item.linkPage;
  } else {
    url += "/" + item.linkPage;
  }*/
  const x = url.split("?");
  obj.url = x[0];
  if (item.linkId === "group" || item.linkId === "haggle") {
    // url = url + item.itemNo + '&activityId=' + item.detailId
    obj.data = {
      activityId: item.detailId,
      itemNo: item.itemNo
    };
  } else if (item.linkId === "article") {
    obj.data = {
      id: item.detailId
    };
  } else if (item.linkId === "articleClassify") {
    obj.data = {
      categroy: item.detailId
    };
  } else if (item.linkId === "category") {
    obj.data = {
      id: item.detailId,
      name: item.detailText,
      type: goodsTypeEnum.PRODUCT.value
    };
  } else if (item.linkId === "server-category") {
    obj.data = {
      id: item.detailId,
      name: item.detailText,
      type: goodsTypeEnum.SERVER.value
    };
  } else if (item.linkId === "activity") {
    obj.data = {
      topicId: item.detailId
    };
  } else if (item.linkId === "luckyDial") {
    obj.data = {
      id: item.detailId
    };
  } else if (item.linkId === "seckill") {
    obj.data = {
      activityId: item.detailId,
      itemNo: item.itemNo,
      seckillStatus: item.status
    };
  } else if (item.linkId === "gift") {
    obj.data = {
      activityId: item.detailId,
      type: goodsTypeEnum.FREE_GOODS_ACTIVITY.value
    };
  } else if (item.linkId === "formTool") {
    obj.data = {
      formId: item.detailId
    }
  } else if (item.linkId === "micro-decorate") {
    obj.data = {
      id: item.id
    }
  } else if (item.linkId === "promotion") {
    obj.data = {
      activityId: item.detailId
    }
  } else {
    if (parseInt(item.linkType) === 1) {
      // url += item.detailId
      obj.data = {
        itemNo: item.detailId,
        source: source || ""
      };
    } else if (item.linkPageKey === "distribution-service" || item.linkPageKey === "scene-booking") {

      obj.data = {
        id: item.serviceCateGoryId ? encodeURIComponent(JSON.stringify(item.serviceCateGoryId)) : '',
        sourceType: item.sourceType
      };
    } else if (item.linkPageKey === "writeInvoice") {
      obj.data = {
        invoiceSwitch: item.invoiceSwitch,
        invoiceCheckList: JSON.stringify(item.invoiceCheckList),
      };
    } else {
      if (x[1]) {
        obj.data = {};
        const params = x[1].split("=");
        obj.data[params[0]] = params[1];
        obj.data.display = display;
      }
    }
  }
  // 跳转路径可携带参数，参数为 linkUseKeys 中指定的字段
  // 参数值为 dataSource.textNavSource 中对应字段的值
  if (!!item["linkUseKeys"]) {
    if (!obj.data) {
      obj.data = {};
    }
    (item["linkUseKeys"] || []).forEach(key => {
      obj.data[key] = item[key];
    });
  }

  // 判断是否从拼团组件头部标题点击更多跳转的页面，是则添加拼团子组件展示类型
  if (item.linkPage === "sub-packages/moveFile-package/pages/group/list/index") {
    if (item.showGroupType || item.showGroupType === 0) {
      if (item.showGroupType == 4) {
        item.showGroupType = 2;
      }
      obj.data = {
        showGroupType: item.showGroupType
      };
    }
  }

  // 秒杀
  if (
    item.linkPage === "sub-packages/marketing-package/pages/seckill/list/index"
  ) {

    if (item.showType || item.showType === 0) {
      if (item.showType == 4) {
        item.showType = 2;
      }
      obj.data = {
        showType: item.showType
      };
    }
  }
  // 砍价
  if (item.linkPage === "sub-packages/moveFile-package/pages/cut-price/list/index") {

    if (item.showType || item.showType == 0) {
      if (item.showType == 4) {
        item.showType = 2;
      }
      obj.data = {
        showType: item.showType
      };
    }
  }

  // 卡包路径调整，兼容旧数据，后续可删除
  if (["wxat-common/pages/card-pack/list/index", "wxat-common/pages/card-pack/detail/index"].includes(obj.url)) {
    obj.url = obj.url.replace('wxat-common', 'sub-packages/marketing-package')
  }


  return obj;
};
