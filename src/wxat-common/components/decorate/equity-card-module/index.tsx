import React, { FC, useEffect, useState } from 'react';
import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { BaseDecorateDataSource } from '../base';
import TextNavModule from '../../decorate/textNavModule/index';
import EquityCardItem, { EquityCard } from '@/wxat-common/components/equity-card-item';

type EquityCardModuleProps = {
  dataSource: (BaseDecorateDataSource & { data: string[] }) | null;
};

let EquityCardModule: FC<EquityCardModuleProps> = (props) => {
  const { showNav, textNavSource, data } = props.dataSource || {};

  const [list, setList] = useState<unknown[]>(data || []);

  useEffect(() => {
    const params = { type: 37, cardIdList: (data || []).map((i: any) => i.id).join(',') };
    wxApi
      .request({
        url: api.quityCard.list,
        data: params,
        loading: true,
      })
      .then((resp) => setList(resp.data || []));
  }, [data]);

  if (!props.dataSource) return null;

  return (
    <View>
      <View
        className='equity-card-module'
        style={{ backgroundColor: props.dataSource.backgroundColor || '#fff', paddingBottom: Taro.pxTransform(10) }}
      >
        {showNav ? <TextNavModule padding={0} dataSource={textNavSource as Record<string, any>} /> : null}

        <View className='list-container'>
          {list.map((item: EquityCard) => (
            <EquityCardItem item={item} key={item.itemNo} />
          ))}
        </View>
      </View>
    </View>
  );
};

EquityCardModule.defaultProps = {
  dataSource: null,
};

export default EquityCardModule;
