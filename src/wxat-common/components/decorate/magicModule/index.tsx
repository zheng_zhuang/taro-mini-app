import React, { useState, useMemo, useEffect } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Block, Navigator } from '@tarojs/components';
import OfficialAccountFlow from '@/wxat-common/components/decorate/officialAccountFlow/index'
import { useSelector } from 'react-redux';
import Taro from '@tarojs/taro';
import jumpPath from '../utils/jump-path.js';
import wxApi from '../../../utils/wxApi';
import api from '@/wxat-common/api'
import reportConstants from '@/sdks/buried/report/report-constants.js';
import report from '@/sdks/buried/report/index.js';
import authHelper from '../../../utils/auth-helper.js';
import './index.scss';
import useToYQZ from '@/hooks/useToYQZ';
// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
  padding: number;
}

const mapStateToProps = ({ base }) => ({
  userInfo: base.userInfo,
  loginInfo: base.loginInfo,
  currentStore: base.currentStore
});

const MagicModule = (props: ComponentProps) => {
  // 提前解构props，减少getter的操作
  const { dataSource } = props;
  const [showOfficialAccountFlow, setShowOfficialAccountFlow] = useState(false);
  const [OfficialAccountImageUrl, setOfficialAccountImageUrl] = useState('');
  const { userInfo, loginInfo, currentStore } = useSelector(mapStateToProps);
  const { handleDecorateToQYZ } = useToYQZ()

  useEffect(() => {
    setTimeout(() => {
      report.showBanner();
    }, 100);
  }, [dataSource]);

  const itemStyle = (item) => {
    const system = Taro.getSystemInfoSync()
    const margin = dataSource.margin * 2;
    const componentsHeight = system.windowHeight / 2;
    // const componentsPadding = PADDING;
    const componentsPadding = Math.max(dataSource.marginLeftRight * 2, 0);
    const componentsWidth = (system.windowWidth - componentsPadding) * 2;

    let width = (parseInt(item.width) / 100) * componentsWidth;
    let height = (parseInt(item.height) / 100) * componentsHeight;
    let top = (parseInt(item.top) / 100) * componentsHeight;
    let left = (parseInt(item.left) / 100) * componentsWidth;

    if (item.width !== '100%' && item.width !== '33.33%' && item.width !== '25%') {
      width = width - margin / 2;
    } else if (item.width === '33.33%') {
      width = (componentsWidth - margin * 2) / 3;
    } else if (item.width === '25%') {
      width = (componentsWidth - margin * 2) / 4;
    }

    if (item.height !== '100%') {
      height = height - margin / 2;
    }

    if (item.top !== '0%') {
      top = top + margin / 2;
    }

    if (item.left !== '0%' && item.left !== '33.33%' && item.left !== '66.66%' && item.left !== '75%') {
      left = left + margin / 2;
    } else if (item.left === '33.33%') {
      left = width + margin;
    } else if (item.left === '66.66%') {
      left = (width + margin) * 2;
    } else if (item.left === '75%') {
      left = componentsWidth - width;
    } else {
      left = 0;
    }
    if (item.wkSrc) {
      return {
        width: width,
        height: height,
        top: top,
        left: left,
        backgroundImage: 'url(' + item.src + ')',
      };
    }
  };

  const itemStyles = useMemo(() => {
    if (dataSource) {
      return (dataSource.data || []).map((x) => {
        return itemStyle(x);
      });
    }
    return [];
  }, [dataSource]);

  const cancelFlow = () => {
    setShowOfficialAccountFlow(false)
  }

  const getImageInfo = () => {
    const params = {
      appId: userInfo.appId || loginInfo.appId,
      storeId: currentStore.id
    }

    wxApi
    .request({
      url: api.officiaAccountFlow.getImage,
      data: params,
      loading: false,
      quite: true,
    }).then( (res) => {
      if(res.success && res.data && res.data.image){
        setOfficialAccountImageUrl(res.data.image)
      }
    })
  }

  const click = async (event) => {
    const item = event.currentTarget.dataset.item;
    const index = event.currentTarget.dataset.index;
    if (item.linkPageKey && String(item.linkPageKey).includes('yqz')) {
      await handleDecorateToQYZ(item)
      return
    }
    if(item.linkPageKey === "officialAccountFlow"){
      getImageInfo()
      setShowOfficialAccountFlow(true)
      return
    }

    if(!!item.H5Link) {
      // window.location.href = item.H5Link;
      wxApi.$locationTo(item.H5Link)
      return;
    }

    if (!(!item.linkPage || item.linkPage === undefined)) {
      report.clickCube(index, item.linkPage);
      if (item.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }
      // 判断是否拨打电话进来
      if (item.linkPageKey==="oneTouchDial" && item.linkOneTouchDial) {
        wxApi.makePhoneCall({
          phoneNumber: item.linkOneTouchDial
        })
      }
      // wxApi.$navigateTo(jumpPath(item, '', reportConstants.SOURCE_TYPE.magic_cube.key))
      // 兼容场地预定路径迁移到主包
      if(item.linkPage==="sub-packages/marketing-package/pages/living-service/hotel-reservation-list/index"){
        item.linkPage="wxat-common/pages/hotel-reservation-list/index"
      }else if(item.linkPage==="sub-packages/marketing-package/pages/living-service/goods-rental/index"){
        item.linkPage="wxat-common/pages/service-classification-tree/index"
      }
      // 判断是否是通过住中服务进来的
      if (item.linkPageKey==="service-category" || item.serviceItemId || item.wifiCateGoryId) {
        if(item.linkPage==="wxat-common/pages/goods-detail/index"){
          item.linkPage="sub-packages/marketing-package/pages/living-service/goods-rental-details/index"
        }
        wxApi.$navigateTo({
          url: jumpPath(item, '', reportConstants.SOURCE_TYPE.magic_cube.key).url,
          data: {
            id: item.serviceItemId ? item.serviceItemId : item.serviceCateGoryId,
            sourceType: item.sourceType
          }
        })
      } else {
        wxApi.$navigateTo(jumpPath(item, '', reportConstants.SOURCE_TYPE.magic_cube.key))
      }
    }
  };

  return (
    <View
      data-scoped='wk-cdm-MagicModule'
      className={`wk-cdm-MagicModule magic-module module-${dataSource.name}`}
      style={_safe_style_('margin: ' + (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px;') + 'grid-column-gap:' + dataSource.margin + 'px;' + 'grid-row-gap:' + dataSource.margin + 'px;')}
    >
      {dataSource?.data &&
        dataSource.data.map((item, index) => {
          return (
            <Block key={index}>
              {item.linkPage === 'miniProgram' ? (
                  <Navigator target="miniProgram" appId={item.linkId} path={item.linkPath}>
                    <View
                      className={`magic-module-image item-${index + 1}`}
                      style={_safe_style_(
                        itemStyles[index]
                          ? 'width:' +
                              itemStyles[index].width / 2 +
                              'px; height:' +
                              itemStyles[index].height / 2 +
                              'px; top:' +
                              itemStyles[index].top / 2 +
                              'px; left:' +
                              itemStyles[index].left / 2 +
                              'border-radius:' +
                              dataSource.radius +
                              'px;' +
                              'px; background-image:' +
                              itemStyles[index].backgroundImage
                          : ''
                      )}
                      onClick={_fixme_with_dataset_(click, { index: index, item: item })}
                    ></View>
                  </Navigator>
                ) : (
                  <View
                    className={`magic-module-image item-${index + 1}`}
                    style={_safe_style_(
                      itemStyles[index]
                        ? 'width:' +
                            itemStyles[index].width / 2 +
                            'px; height:' +
                            itemStyles[index].height / 2 +
                            'px; top:' +
                            itemStyles[index].top / 2 +
                            'px; left:' +
                            itemStyles[index].left / 2 +
                            'px; background-image:' +
                            itemStyles[index].backgroundImage +
                            ';border-radius:' +
                            dataSource.radius +
                            'px'
                        : ''
                    )}
                    onClick={_fixme_with_dataset_(click, { index: index, item: item })}
                  ></View>
                )}
            </Block>
          );
        })}

      {showOfficialAccountFlow && OfficialAccountImageUrl && (
        <OfficialAccountFlow
          imageUrl={OfficialAccountImageUrl}
          onCancel={cancelFlow}
        ></OfficialAccountFlow>
      )}
    </View>
  );
};

// 给props赋默认值
MagicModule.defaultProps = {
  dataSource: {},
  padding: 0,
};

export default MagicModule;
