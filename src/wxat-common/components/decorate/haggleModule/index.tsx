import React, { useMemo, FC } from 'react';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';
import MarketingMoudle from '../marketingModule/index';

import './index.scss';

// props的类型定义
interface ComponentProps {
  dataList: Array<Record<string, any>>;
  showType: number;
  onClick: (object) => any;
}

const HaggleModule: FC<ComponentProps> = ({ dataList, showType, onClick }) => {
  const tmpStyle = useTemplateStyle();
  const list = useMemo(() => {
    return (dataList || NOOP_ARRAY).map((item) => {
      return {
        id: item.id,
        name: item.activityName,
        thumbnail: item.thumbnail,
        salePrice: item.lowestPrice,
        labelPrice: item.salePrice,
        saleAmount:
          item.virtualPeople + item.bargainTimes > 9999
            ? '9999+'
            : item.virtualPeople + item.bargainTimes + '人正在砍价',
        saleLabel: '最低价：',
        topSign: '砍',
        btn: '立即砍价',
        type:item.wxItem.type
      };
    });
  }, [dataList]);

  return (
    <View
      data-fixme='03 add view wrapper. need more test'
      data-scoped='wk-cdh-HaggleModule'
      className='wk-cdh-HaggleModule'
    >
      <MarketingMoudle computeDataList={list} showType={showType} onClick={onClick}></MarketingMoudle>
    </View>
  );
};

// 给props赋默认值
HaggleModule.defaultProps = {
  dataList: [],
  showType: 0,
};

export default HaggleModule;
