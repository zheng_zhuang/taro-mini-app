import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

interface ComponentProps {
  dataSource: Record<string, any>;
}

const AssistLineModule: FC<ComponentProps> = ({ dataSource = {} }) => {
  return (
    <View data-scoped='wk-cda-AssistLineModule' className='wk-cda-AssistLineModule assist-blank-module'>
      <View
        className='assist-blank-module-line'
        style={_safe_style_(
          'border-top-style: ' +
            dataSource.style +
            ';border-top-color: ' +
            dataSource.lineColor +
            ';margin: ' +
            (dataSource.showMargin === 'false' ? 0 : 15) +
            'px'
        )}
      ></View>
    </View>
  );
};

export default AssistLineModule;
