export type BaseDecorateDataSource = {
  componentsPadding: number;
  radius: number;
  marginUpDown: number;
  marginLeftRight: number;
  data: unknown[];
  showNav: true;
  textNavSource?: {
    componentsPadding: number;
    fontSize: string | number;
    title: string;
    subTitle: string;
    titleColor: string;
    subTitleColor: string;
    linkPage: string;
    linkName: string;
    showMore: boolean;
  };
  backgroundColor: string;
  bgMode: string;
};
