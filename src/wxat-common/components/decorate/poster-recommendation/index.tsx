import React, { useState, useEffect, FC, useMemo } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import report from '@/sdks/buried/report/index.js';
import GoodsItem from '../../industry-decorate-style/goods-item';

import './index.scss';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';

/**
 * 首页活动专题装修组件，支持横向滚动和纵向滚动的专区活动简版展示，同时也支持整个专区活动在首页展示，
 * 具体用法参考首页装修组件的用法，需要服务端传递一个dataSource JSON字符串，该JSON中的字段与pc web协定好，包括：
 * showPoster 是否展示海报，如果不展示海报则展示专题活动名
 * limitNum 限制展示的商品数，如果为null 或者0 则认为不限制
 * activityId 活动id
 * display 展示形式，enum horizon：水平滑动列表；vertical：垂直滚动列表
 */

// props的类型定义
type ComponentProps = {
  dataSource: Record<string, any>;
  viewShow?: boolean;
};

const reportSource = reportConstants.SOURCE_TYPE.guess_like.key;

let PosterRecommendation: FC<ComponentProps> = ({ dataSource }) => {
  //提前解构props，减少getter的操作

  const {
    background,
    posterUrl = '',
    posterTitle = '',
    showPoster,
    display,
    limitNum = display === 'vertical' ? 4 : 10,
    hideAllBtn = false,
  } = dataSource;

  const [goodsList, setGoodsList] = useState<object[]>(NOOP_ARRAY);

  const style = useMemo(() => {
    if (dataSource) {
      const { marginUpDown, marginLeftRight, radius } = dataSource;
      return {
        margin: `${Taro.pxTransform((marginUpDown || 0) * 2)} ${Taro.pxTransform((marginLeftRight || 0) * 2)}`,
        border: Taro.pxTransform((radius || 0) * 2),
        background: background || 'transparent',
      };
    }
    return undefined;
  }, [dataSource]);

  useEffect(() => {
    console.log('will getRecommendGoodsList api   -> ', !!dataSource);
    if (!!dataSource) {
      apiGetUserDM();
    }
  }, [dataSource]);

  /**
   * 查询分类中的子数据
   */
  const apiGetUserDM = () => {
    const reqParam = {
      deviceName: (wxApi.getSystemInfoSync() || {}).model || '',
      pageSize: limitNum,
    };

    wxApi
      .request({
        url: api.goods.recommendList,
        loading: false,
        checkSession: true,
        quite: true,
        data: reqParam,
      })
      .then((res) => {
        //性能问题->暂时控制条数最多显示6条
        setGoodsList((res.data.wxItemDetailList || []).slice(0, 30));
      })
      .catch((err) => {
        console.log('getRecommendGoodsList error -> ' + JSON.stringify(err));
        // this.triggerEvent('load', false);
      });
  };

  // TODO  应该跳more的页面
  const clickAll = () => {
    console.log('跳转');
    
    report.clickPtoductGuess();
    wxApi.$navigateTo({
      url: 'sub-packages/moveFile-package/pages/more/index',
      data: { type: 1, display },
    });
  };

  const leftList = (goodsList || []).filter((_, index) => index % 2 === 0);
  const rightList = (goodsList || []).filter((_, index) => index % 2 !== 0);
  const isNotEmptyList = !!goodsList && !!goodsList.length;

  return (
    <View className='wk-cdp-PosterRecommendation'>
      {isNotEmptyList && (
        <View className='poster-recommendation' style={_safe_style_(style)}>
          {!!(showPoster && posterUrl) && <Image className='poster' src={posterUrl}></Image>}
          {!!(!showPoster && posterTitle) && (
            <View className={'title ' + (display === 'oneRowTwo' ? 'one-row-two-title' : '')}>{posterTitle || ''}</View>
          )}

          {display === 'vertical' && (
            <View className='vertical-container'>
              {' '}
              {
                <View>
                  {goodsList.map((item, index) => {
                    return (
                      <GoodsItem
                        goodsItem={item}
                        key={index}
                        reportSource={reportSource}
                        display='vertical'
                        showDivider={index !== goodsList.length - 1}
                      />
                    );
                  })}
                </View>
              }
            </View>
          )}

          {display === 'horizon' && (
            <View className='horizon-container'>
              {' '}
              {
                <Block>
                  {goodsList.map((item, index) => {
                    return <GoodsItem goodsItem={item} key={index} reportSource={reportSource} display='horizon' />;
                  })}
                </Block>
              }
            </View>
          )}

          {display === 'oneRowOne' && (
            <View className='oneRowOne-container'>
              {' '}
              {
                <Block>
                  {goodsList.map((item, index) => {
                    return (
                      <GoodsItem
                        className='oneRowOne-item'
                        goodsItem={item}
                        key={index}
                        reportSource={reportSource}
                        display='oneRowOne'
                      />
                    );
                  })}
                </Block>
              }
            </View>
          )}

          {display === 'oneRowTwo' && (
            <View className='oneRowTwo-container'>
              {' '}
              {
                <View style={{ width: '100%' }}>
                  <View className='list-wrap'>
                    <View className='left'>
                      {leftList.map((item, index) => {
                        return (
                          <View className='oneRowTwo-item' key={index}>
                            <GoodsItem goodsItem={item} reportSource={reportSource} display='oneRowTwo' />
                          </View>
                        );
                      })}
                    </View>
                    <View className='right'>
                      {rightList.map((item, index) => {
                        return (
                          <View className='oneRowTwo-item' key={index}>
                            <GoodsItem goodsItem={item} reportSource={reportSource} display='oneRowTwo' />
                          </View>
                        );
                      })}
                    </View>
                  </View>
                </View>
              }
            </View>
          )}

          {display === 'oneRowThree' && (
            <View className='oneRowThree-container'>
              {' '}
              {
                <Block>
                  {goodsList.map((item, index) => {
                    return (
                      <View className='oneRowThree-item' key={index}>
                        <GoodsItem
                          className='oneRowThree-item'
                          goodsItem={item}
                          reportSource={reportSource}
                          display='oneRowThree'
                        />
                      </View>
                    );
                  })}
                </Block>
              }
            </View>
          )}

          {display !== 'horizon' && !hideAllBtn && (
            <View className='all' onClick={clickAll}>
              <Text>查看全部 {'>'}</Text>
            </View>
          )}
        </View>
      )}
    </View>
  );
};

// 给props赋默认值
PosterRecommendation.defaultProps = {
  dataSource: {},
  viewShow: true,
};

export default PosterRecommendation;
