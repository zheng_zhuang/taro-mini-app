import React, { FC, useState, useImperativeHandle, useRef, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import jumpPath from '../utils/jump-path.js';
import reportConstants from '../../../../sdks/buried/report/report-constants';
// import report from '../../../../sdks/buried/report/index';
import authHelper from '../../../utils/auth-helper';
import wxApi from '@/wxat-common/utils/wxApi';
import AnimatDialog from '../../animat-dialog/index';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import './index.scss';

const customDialogClose = cdnResConfig.decorate.customDialogClose;

// props的类型定义
type ComponentProps = {
  dataSource: any;
  display?: string;
};

let CustomDialogModule: FC<ComponentProps> = (props) => {
  const { dataSource } = props;
  const customDialogRef = useRef<AnimatDialog>(null);

  useEffect(() => {
    if (dataSource.uniqueId === wxApi.getStorageSync(`cd_key_${dataSource.uniqueId}`)) return;
    !!dataSource && show();
  }, [dataSource]);

  const click = () => {
    if (dataSource.isRegistered && !authHelper.checkAuth()) return;

    if (dataSource.openType === 'h5' && dataSource.webLinkUrl) {
      // web-view H5链接
      wxApi.setStorageSync(`cd_key_${dataSource.uniqueId}`, dataSource.uniqueId);

      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/custom-webview/index',
        data: { linkUrl: dataSource.webLinkUrl },
      });

      hide();
      return;
    }
    if (dataSource.openType === 'minApp' && dataSource.linkPage != null) {
      if (dataSource.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }
      wxApi.setStorageSync(`cd_key_${dataSource.uniqueId}`, dataSource.uniqueId);

      wxApi.$navigateTo(jumpPath(dataSource, '', reportConstants.SOURCE_TYPE.customDialog.key));

      hide();
    }
  };

  const hide = () => {
    customDialogRef && customDialogRef.current && customDialogRef.current.hide(true);
  };

  const show = () => {
    customDialogRef &&
      customDialogRef.current &&
      customDialogRef.current.show({
        scale: 1,
      });
  };

  return (
    <View data-scoped='wk-cdt-CustomDialogModule' className='wk-cdt-CustomDialogModule custom-dialog-module'>
      <AnimatDialog ref={customDialogRef} animClass='custom-dialog'>
        <View className='custom-dialog-wraper'>
          {dataSource.coverImg && <Image className='custom-img' src={dataSource.coverImg} onClick={click} />}
          <Image className='close-btn' src={customDialogClose} onClick={hide}></Image>
        </View>
      </AnimatDialog>
    </View>
  );
};

// 给props赋默认值
CustomDialogModule.defaultProps = {
  dataSource: null,
};

export default CustomDialogModule;
