import React, { useState, FC, useMemo } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import OfficialAccountFlow from '@/wxat-common/components/decorate/officialAccountFlow/index'
import { useSelector } from 'react-redux';
import Taro from '@tarojs/taro';
import LinkType from '../../../constants/link-type.js';
import wxApi from '../../../utils/wxApi';
import api from '@/wxat-common/api'
import report from '@/sdks/buried/report/index.js';
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import useToYQZ from '@/hooks/useToYQZ';

const decorateImg = cdnResConfig.decorate;

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
}

const mapStateToProps = ({ base }) => ({
  userInfo: base.userInfo,
  loginInfo: base.loginInfo,
  currentStore: base.currentStore
});

const NavigationModule: FC<ComponentProps> = ({ dataSource }) => {
  // 提前解构props，减少getter的操作
  const [showOfficialAccountFlow, setShowOfficialAccountFlow] = useState(false);
  const [OfficialAccountImageUrl, setOfficialAccountImageUrl] = useState('');
  const { userInfo, loginInfo, currentStore } = useSelector(mapStateToProps);
  const { handleDecorateToQYZ } = useToYQZ()

  const style = useMemo(() => {
    if (dataSource) {
      const { marginUpDown, marginLeftRight, radius, backgroundColor } = dataSource;
      return `padding:${marginUpDown}px ${marginLeftRight}px;border-radius:${
        radius
      }px;background:${backgroundColor}`;
    }
    return '';
  }, [dataSource]);

  const [allShowed, setAllShowed] = useState(false);

  const clickToggleShow = () => {
    setAllShowed(!allShowed);
  };

  const cancelFlow = () => {
    setShowOfficialAccountFlow(false)
  }

  const getImageInfo = () => {
    const params = {
      appId: userInfo.appId || loginInfo.appId,
      storeId: currentStore.id
    }

    wxApi
    .request({
      url: api.officiaAccountFlow.getImage,
      data: params,
      loading: false,
      quite: true,
    }).then( (res) => {
      if(res.success && res.data && res.data.image){
        setOfficialAccountImageUrl(res.data.image)
      }
    })
  }

  /**
   * dataset.item => ["custom",JSON.stringify({id: "",name: "xx自定义"})]
   * dataset.item => ["activity",JSON.stringify({id: "",name: "xx专题也"})]
   * dataset.item => ["category",JSON.stringify({id: "",name: "xx分类"})]
   * @param event
   */
  const click = async (event) => {
    const item = event.currentTarget.dataset.item;

    if (item.linkPageKey && String(item.linkPageKey).includes('yqz')) {
      await handleDecorateToQYZ(item)
      return
    }

    if(!!item.link && (typeof(item.link[0]).includes("{")) && JSON.parse(item.link[0]).linkType=='writeInvoice') {
      wxApi.$navigateTo({
        url: "sub-packages/scene-package/pages/writeInvoice/index",
        data: {
          invoiceSwitch:item.invoiceSwitch,
          invoiceCheckList: JSON.stringify(item.invoiceCheckList),
        }
      })
      return;
    }

    if(!!item.H5Link) {
      // window.location.href = item.H5Link;
      wxApi.$locationTo(item.H5Link)
      return;
    }

    if (!!item.link && item.link.length) {
      const value = JSON.parse(item.link[item.link.length - 1]);
      const type = value.linkType || item.link[0]; /* 兼容以前老数据的格式 */
      report.clickNavModule(type, value.id, value.name);

      if(type === 'officialAccountFlow'){
        getImageInfo()
        setShowOfficialAccountFlow(true)
        return
      }

      const LinkTypeKeysArr = Object.keys(LinkType);
      const index = LinkTypeKeysArr.findIndex((node) => LinkType[node].value === type);
      if (!~index) return false;
      const link = LinkType[LinkTypeKeysArr[index]];
      ((link.checkStatus && link.checkStatus(value)) || Promise.resolve(true))
        .then((res) => {
          const url = link.getLink(value.id, value.name);
          if (!!url && res) {
            wxApi.$navigateTo({
              url: url,
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  // const {
  //   style,
  //   rowNum,
  //   allShowed,
  //   circularCorner,
  //   dataSource,
  //   list
  // } = this.data
  const { list, rowNum = 4, circularCorner = 0 } = dataSource;

  if (!list || !list.length) {
    return null;
  }

  return (
    <View
      data-scoped='wk-cdn-NavigationModule'
      className='wk-cdn-NavigationModule navigation-module'
      style={_safe_style_(style)}
    >
      {list.map((item, index) => {
        return (
          (allShowed ? true : index < rowNum * 2) && (
            <View
              className={'item-wrap ' + (rowNum === 4 ? 'item-row-4' : 'item-row-5')}
              onClick={_fixme_with_dataset_(click, { item: item })}
              key={index}
            >
              <View className='item'>
                {item.logo ? (
                  <Image
                    className='img'
                    src={item.logo}
                    style={_safe_style_('border-radius: ' + (circularCorner === 1 ? '100%' : '0'))}
                  ></Image>
                ) : (
                  <View className='img'></View>
                )}

                <View className='name' style={_safe_style_('color:' + dataSource.titleColor + ';')}>
                  {item.name || '－'}
                </View>
              </View>
            </View>
          )
        );
      })}
      {list.length > rowNum * 2 && (
        <View className='down-angle' onClick={clickToggleShow}>
          <Image src={decorateImg.downAngle} className={'down-angle-img ' + (allShowed ? 'reverse' : '')}></Image>
        </View>
      )}

      {showOfficialAccountFlow && OfficialAccountImageUrl && (
        <OfficialAccountFlow
          imageUrl={OfficialAccountImageUrl}
          onCancel={cancelFlow}
        ></OfficialAccountFlow>
      )}
    </View>
  );
};

// 给props赋默认值
NavigationModule.defaultProps = {
  dataSource: {},
};

export default NavigationModule;
