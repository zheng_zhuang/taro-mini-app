import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

type ComponentProps = {
  padding: number;
  dataList: Array<Record<string, any>>;
  onClick: (object) => any;
};

let GiftCardModule: FC<ComponentProps> = ({ padding, dataList, onClick }) => {
  const handleGoToCardDetail = (item) => {
    // const myEventDetail = {
    //   item: e.currentTarget.dataset.item
    // };
    // this.triggerEvent('click', myEventDetail);
    onClick && onClick(item);
  };

  return (
    <View data-scoped='wk-cdg-GiftCardModule' className='wk-cdg-GiftCardModule card-module'>
      <ScrollView scrollX>
        <View className='mine-card-container' style={_safe_style_('padding-left: ' + Taro.pxTransform(padding))}>
          {dataList.map((item, index) => {
            return (
              <View
                className='mine-card-item'
                onClick={() => handleGoToCardDetail(item)}
                // data-item={item}
                key={index}
                style={_safe_style_('background: transparent url(' + item.bgImg + ') no-repeat center center / cover')}
              ></View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

GiftCardModule.defaultProps = {
  padding: 0,
  dataList: [],
};

export default GiftCardModule;
