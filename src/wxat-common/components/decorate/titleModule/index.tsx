import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
  padding: number;
}

const TitleModule: FC<ComponentProps> = ({ dataSource }) => {
  // 提前解构props，减少getter的操作

  return (
    <View
      data-scoped='wk-cdt-TitleModule'
      className='wk-cdt-TitleModule title-module'
      style={_safe_style_(
        'margin:' +
          (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
          ';border-radius: ' +
          dataSource.radius +
          'px;'
      )}
    >
      <View
        className='title-module-text'
        style={_safe_style_(
            'text-align:' +
            dataSource.textAlign +
            ';background-color:' +
            dataSource.backgroundColor +
            ';color:' +
            dataSource.titleColor +
            ';'
        )}
      >
        {dataSource.title}
      </View>
    </View>
  );
};

// 给props赋默认值
TitleModule.defaultProps = {
  dataSource: {},
  padding: 0,
};

export default TitleModule;
