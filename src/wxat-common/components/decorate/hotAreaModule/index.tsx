import React, {useState, useEffect, FC} from 'react';
import {_safe_style_, _fixme_with_dataset_} from '@/wxat-common/utils/platform';
import {Block, View, Image, Text} from '@tarojs/components';
import Taro from '@tarojs/taro';
import {useSelector} from 'react-redux';
// wxat-common/components/decorate/hotAreaModule/index.js
import authHelper from '../../../utils/auth-helper.js';
import wxApi from '../../../utils/wxApi';
import imageUtils from '../../../utils/image.js';
import jumpPath from '../utils/jump-path.js';
// import reportConstants from '../../../sdks/buried/report/report-constants.js'
import report from '@/sdks/buried/report';
import api from '@/wxat-common/api'
import useToYQZ from '@/hooks/useToYQZ';
import './index.scss';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import {NOOP_ARRAY} from '@/wxat-common/utils/noop';
import OfficialAccountFlow from '@/wxat-common/components/decorate/officialAccountFlow/index'
import utils from '@/wxat-common/utils/util';
import util from '@/wxat-common/utils/util';

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
}

interface canvasStyleDefine {
  ableMaxWidth: number;
  ableMaxHeight: number;
  canvasWidth: number;
  canvasHeight: number;
  radius: number;
}

const defaultCanvasStyle: canvasStyleDefine = {
  ableMaxWidth: 0,
  ableMaxHeight: 0,
  canvasWidth: 0,
  canvasHeight: 0,
  radius: 0,
};

const mapStateToProps = ({globalData, base}) => ({
  tabbars: globalData.tabbars,
  userInfo: base.userInfo,
  loginInfo: base.loginInfo,
  currentStore: base.currentStore
});

const HaggleModule: FC<ComponentProps> = ({dataSource}) => {
  const [areas, setAreas] = useState<object[]>(NOOP_ARRAY);
  const [imageSrc, setImageSrc] = useState('');
  const [showTips, setShowTips] = useState(false);
  const [showOfficialAccountFlow, setShowOfficialAccountFlow] = useState(false);
  const [OfficialAccountImageUrl, setOfficialAccountImageUrl] = useState('');

  const [canvasStyle, setCanvasStyle] = useState(defaultCanvasStyle);
  const {userInfo, loginInfo, currentStore} = useSelector(mapStateToProps);
  const {handleDecorateToQYZ} = useToYQZ()
  useEffect(() => {
    if (dataSource) {
      initData(dataSource);
    }
  }, [dataSource]);

  const initData = (params) => {
    const {showTips, src, radius, width, height, marginLeftRight} = params;
    const rate = width / wxApi.getSystemInfoSync().screenWidth;
    const ableMaxWidth = ((width / 3) * 2)  - marginLeftRight * 2;
    const canvasWidth = ableMaxWidth;
    const canvasHeight = height / rate;
    setCanvasStyle({
      ableMaxWidth: ableMaxWidth * 2,
      ableMaxHeight: canvasHeight * 2,
      canvasWidth: canvasWidth * 2,
      canvasHeight: canvasHeight * 2,
      radius: radius * 2,
    });

    setAreas(initArea(params));
    if (src) {
      setImageSrc(imageUtils.cdn(src));
    }
    setShowTips(showTips === 1);
  };

  const initArea = (params) => {
    const {data, width, marginLeftRight, EditAreaMaxWidth} = params;
    if (!data || !data.length) return [];
    const viewData: Array<Record<string, any>> = [];
    const ableMaxWidth = wxApi.getSystemInfoSync().screenWidth - marginLeftRight * 2;
    const canvasWidth = ableMaxWidth;
    const scale = canvasWidth / EditAreaMaxWidth;
    data.forEach((item) => {
      const name = item.name || '';
      const top = scale * item.shape.top * 2;
      const left = scale * item.shape.left * 2;
      const width1 = scale * item.shape.width * 2;
      const height = scale * item.shape.height * 2;
      const nameLength = Math.min(250, 60 + name.length * 18);
      const style = `
        width:${width1 / 2}px;
        height:${height / 2}px;
        top:${top / 2}px;
        left:${left / 2}px;
        z-index: 2
        `;
      viewData.push({
        nameLength,
        style,
        name,
        id: item.id,
        line: {
          top: top + 25,
          height: 25 * 2,
          left: left + nameLength / 2,
        },
      });
    });
    return viewData;
  };

  const cancelFlow = () => {
    setShowOfficialAccountFlow(false)
  }

  const getImageInfo = () => {
    const params = {
      appId: userInfo.appId || loginInfo.appId,
      storeId: currentStore.id
    }

    wxApi
      .request({
        url: api.officiaAccountFlow.getImage,
        data: params,
        loading: false,
        quite: true,
      }).then((res) => {
      if (res.success && res.data && res.data.image) {
        setOfficialAccountImageUrl(res.data.image)
      }
    })
  }

  const onTapArea = async (e) => {
    const index = e.currentTarget.dataset.index;
    const item = dataSource.data[index];
    util.sensorsReportData('click_hot_zone', {
      event_name: '热区图点击',
      carousel_label: item.label,
      carousel_name: item.linkName,
      carousel_link_page: item.linkPage,
      carousel_type: item.linkPageKey,
    })
    // 调起扫码

    if (item.linkPageKey === 'scanCode') {
      scanCode()
      return
    }


    if (item && String(item.linkPageKey).includes('yqz')) {
      await handleDecorateToQYZ(item)
      return
    }

    if (item.linkPageKey === "officialAccountFlow") {
      getImageInfo()
      setShowOfficialAccountFlow(true)
      return
    }

    if (item.H5Link) {
      wxApi.$locationTo(item.H5Link)
      return;
    }

    let _targetObj = {}

    if (!(!item.linkPage || item.linkPage === undefined)) {
      if (item.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }

      // 判断是否拨打电话进来
      if (item.linkPageKey == "oneTouchDial" && item.linkOneTouchDial) {
        wxApi.makePhoneCall({
          phoneNumber: item.linkOneTouchDial
        })
      }

      // 兼容场地预定路径迁移到主包
      if (item.linkPage == "sub-packages/marketing-package/pages/living-service/hotel-reservation-list/index") {
        item.linkPage = "wxat-common/pages/hotel-reservation-list/index"
      } else if (item.linkPage == "sub-packages/marketing-package/pages/living-service/goods-rental/index") {
        item.linkPage = "wxat-common/pages/service-classification-tree/index"
      }

      _targetObj = {
        url: jumpPath(item, '', reportConstants.SOURCE_TYPE.poster.key).url,
        data: {
          id: item.serviceItemId ? encodeURIComponent(JSON.stringify(item.serviceItemId)) : item.serviceCateGoryId,
          sourceType: item.sourceType
        }
      }
      // 判断是否是住中服务进来的
      if (item.linkPageKey == "service-category" || item.serviceItemId || item.wifiCateGoryId) {
        if (item.linkPage == "sub-packages/moveFile-package/pages/goods-detail/index") {
          item.linkPage = "sub-packages/marketing-package/pages/living-service/goods-rental-details/index"
        }
        _targetObj = {
          url: jumpPath(item, '', reportConstants.SOURCE_TYPE.banner.key).url,
          data: {
            id: item.serviceItemId ? encodeURIComponent(JSON.stringify(item.serviceItemId)) : item.serviceCateGoryId,
            sourceType: item.sourceType
          }
        }
      } else {
        _targetObj = jumpPath(item, '', reportConstants.SOURCE_TYPE.hot_area.key)
      }
      // const reportObj = Object.assign({}, _targetObj, {index: index});
      wxApi.$navigateTo(_targetObj)
    }
  };

  const scanCode = async () => {
    const { result } = await wxApi.scanCode({
      scanType: ['qrCode'],
    });
    const qrCodeType = utils?.getQueryString(result, 'qrCodeType')
    const qrCodeId = utils?.getQueryString(result, 'qrCodeId')
    if(qrCodeType == 1) {
      await wxApi.request({
        url: api.qrCode.getDetail + `?qrCodeId=${qrCodeId}`,
        method: 'GET',
      }).then((res) => {
        if (res.data) {
          wxApi.setStorageSync('qrCodeId', qrCodeId);
          wxApi.setStorageSync('filmQrCodeDetail', res.data);
          wxApi.$redirectTo({
            url: '/sub-packages/film/index/index',
          })
        } else {
          wxApi.showModal({
            title: '提示',
            content: res.errorMessage,
            confirmText: "重新扫码",
            showCancel: false,
            success: function () {
              scanCode()
            }
          })
        }
      })
    }
  }

  const {ableMaxWidth, ableMaxHeight, canvasWidth, canvasHeight, radius} = canvasStyle;
  return (
    <View
      data-scoped='wk-cdh-HotAreaModule'
      className='wk-cdh-HotAreaModule hot-area-module'
      style={_safe_style_(
        `margin: ${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(dataSource.marginLeftRight * 2)} ;
        height: ${Taro.pxTransform(ableMaxHeight)}`
      )}
    >
      <Image
        style={_safe_style_(
          'border-radius:' + (radius / 2) + 'px;' +
          `height: ${Taro.pxTransform(canvasHeight)};
          width: 100%;
          display:block;`
        )}
        src={imageSrc}
      ></Image>
      {areas.map((item, index) => {
        return (
          <Block key={index}>
            {!!showTips && (
              <View
                className='line'
                style={_safe_style_(
                  'top:' + (item.line.top / 2) + 'px;left:' + (item.line.left / 2) + 'px;height:' + (item.line.height / 2) + 'px;'
                )}
              >
                <Text></Text>
              </View>
            )}

            {!!showTips && (
              <View
                style={_safe_style_(
                  'top:' + ((item.line.top + item.line.height - 30) / 2) + 'px;left:' + ((item.line.left - 15) / 2) + 'px;'
                )}
                className='circle center'
              >
                <View className='inner-circle'>
                  <Text></Text>
                </View>
              </View>
            )}

            <View
              style={_safe_style_(item.style)}
              className='area'
              onClick={_fixme_with_dataset_(onTapArea, {index: index})}
            >
              {!!showTips && (
                <View className='buy-tip' style={_safe_style_('width:' + (item.nameLength / 2) + 'px;')}>
                  <View className='start'>
                    <Text></Text>
                  </View>
                  <View className='spot' style={_safe_style_('width:' + ((item.nameLength - 44) / 2) + 'px;')}>
                    <Text>{item.name}</Text>
                  </View>
                </View>
              )}
            </View>
          </Block>
        );
      })}

      {showOfficialAccountFlow && OfficialAccountImageUrl && (
        <OfficialAccountFlow
          imageUrl={OfficialAccountImageUrl}
          onCancel={cancelFlow}
        ></OfficialAccountFlow>
      )}
    </View>
  );
};

// 给props赋默认值
HaggleModule.defaultProps = {
  dataSource: {},
};

export default HaggleModule;
