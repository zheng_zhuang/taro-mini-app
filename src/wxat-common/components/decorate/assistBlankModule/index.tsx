import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';

type ComponentProps = {
  dataSource: Record<string, any>;
};

let AssistBlankModule: FC<ComponentProps> = ({ dataSource }) => {
  return (
    <View
      className='assist-blank-module'
      style={{
        backgroundColor: dataSource && dataSource.backgroundColor,
        height: Taro.pxTransform(((dataSource && dataSource.height) || 0) * 2),
      }}
    ></View>
  );
};

export default AssistBlankModule;
