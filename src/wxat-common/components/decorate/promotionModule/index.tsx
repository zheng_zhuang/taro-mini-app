import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import report from '../../../../sdks/buried/report/index.js';
import GoodsItem from '../../industry-decorate-style/goods-item/index';
import TextNavModule from '../textNavModule/index';
import './index.scss';

import reportConstants from '../../../../sdks/buried/report/report-constants.js';
import protectedMailBox from '../../../utils/protectedMailBox.js';

class PromotionModule extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    dataSource: {}, //传dataSource只是取dataSource里面的id  进行再次查询
    reportSource: '',
    display: 'vertical',
    onClick: null,
  };

  /**
   * 组件的初始数据
   */
  state = {
    renderList: {},
    PADDING: '0',
    defaultReportSource: reportConstants.SOURCE_TYPE.product.key,
    attributeName: '',
    itemClass: null,
    containerClass: null,
    list: [], //促销活动列表
  };

  componentDidMount() {
    const { dataSource } = this.props;
    const itemClass = this.getClassByShowType('item');
    const containerClass = this.getClassByShowType('container');

    this.setState({
      itemClass: itemClass,
      containerClass: containerClass,
    });

    if (dataSource && dataSource.data) {
      this.getPromotion();
    }
  }

  getClassByShowType(type) {
    const showType = new Map([
      ['vertical', 'vertical-' + type],
      ['horizon', 'horizon-' + type],
      ['oneRowOne', 'oneRowOne-' + type],
      ['oneRowTwo', 'oneRowTwo-' + type],
      ['oneRowThree', 'oneRowThree-' + type],
    ]);

    const { dataSource, display: propDisplay } = this.props;
    const display = dataSource && dataSource.display;
    return showType.get(display || propDisplay);
  }

  //查询促销活动列表
  getPromotion() {
    const { dataSource } = this.props;
    let data = dataSource.data;
    if (!data || !data.length) {
      return;
    }
    data = data.map((item) => {
      return item.id;
    });
    let idList = data.join(',');
    wxApi
      .request({
        url: api.much_buy.queryList,
        data: { idList },
      })
      .then((res) => {
        this.setState(
          {
            list: res.data,
          },

          () => {
            this.getList();
          }
        );
      });
  }

  //查询商品列表
  getList() {
    const { dataSource } = this.props;
    const { list } = this.state;
    const data = list;
    if (data && data.length > 0) {
      let itemNos = data[0].id;
      this.setState({
        attributeName: data[0].ruleName,
      });

      let params = {
        moreDiscountActivityId: itemNos,
        pageNo: 1,
        pageSize: dataSource.limitNum,
      };

      wxApi
        .request({
          url: api.much_buy.item,
          data: params,
        })
        .then((res) => {
          let obj = {};
          Object.keys(dataSource).forEach((key) => {
            if (key === 'data') {
              obj[key] = res.data;
              const goodsExposure = [];
              (obj[key] || []).forEach((item) => {
                if (!!item.wxItem.itemNo) {
                  goodsExposure.push(item.wxItem.itemNo);
                }
              });
              if (!!goodsExposure && !!goodsExposure.length) {
                report.goodsExposure(goodsExposure.join(','));
              }
            } else {
              obj[key] = dataSource[key];
            }
          });

          if (!dataSource.hasOwnProperty('display')) {
            const showRectType = parseInt(dataSource.showRectType);
            if (showRectType === 1) {
              obj.display = 'vertical';
            } else if (showRectType === 1) {
              obj.display = 'oneRowTwo';
            }
          }

          obj.type = 'product';

          this.setState({
            renderList: obj,
          });
        })
        .catch((error) => {
          console.log('get product: error: ' + JSON.stringify(error));
        });
    }
  }
  onItemClick = (detail) => {
    return !this.props.onClick ? false : this.props.onClick(detail) || true;
  };

  handleToPromotion = () => {
    const { dataSource } = this.props;
    const { list } = this.state;
    dataSource.data = list;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/promotion/index',
    });

    protectedMailBox.send('sub-packages/marketing-package/pages/promotion/index', 'dataSource', dataSource);
  };

  render() {
    const { dataSource, reportSource } = this.props;
    const { renderList, PADDING, containerClass, itemClass, defaultReportSource, attributeName, list } = this.state;
    return (
      !!(!!list && !!list.length) && (
        <View
          data-scoped='wk-cdp-PromotionModule'
          className='wk-cdp-PromotionModule product-module'
          style={{
            padding: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 20)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2 || 20
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
            background: dataSource.backgroundColor || (renderList.display === 'vertical' ? 'rgba(255,255,255,1)' : ''),
          }}
        >
          {!!dataSource.showPoster && (
            <View className='img-box' onClick={this.handleToPromotion}>
              {(dataSource.posterData || []).map((item, index) => {
                return <Image className='img-box__image' mode='aspectFill' key={index} src={item.src}></Image>;
              })}
            </View>
          )}

          {!dataSource.showPoster && (
            <TextNavModule
              dataSource={dataSource.textNavSource}
              padding={PADDING}
              onClick={this.handleToPromotion}
            ></TextNavModule>
          )}

          <View className={containerClass}>
            {(renderList.data || []).map((item) => {
              return (
                <View className={itemClass} key={item && item.wxItem ? item.wxItem.itemNo : index}>
                  <GoodsItem
                    goodsItem={item}
                    onClick={this.onItemClick}
                    reportSource={reportSource ? reportSource : defaultReportSource}
                    display={renderList.display}
                    attributeName={attributeName}
                  ></GoodsItem>
                </View>
              );
            })}
          </View>
        </View>
      )
    );
  }
}

export default PromotionModule;
