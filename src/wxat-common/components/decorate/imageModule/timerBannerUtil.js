export default {
  /**
   * 定时计算banner中的数量
   * @param resizeBannerTimer 重新计算
   * @param originBanners 原来的banner列表
   */
  scheduleRecountBanner(resizeBannerTimer, originBanners, callback) {
    resizeBannerTimer && clearInterval(resizeBannerTimer);
    return setInterval(() => {
      callback(this.filterBannersByTimeLimit(originBanners));
    }, 5000);
  },

  /**
   * 过滤掉不在日期范围之内的banner
   * 限制条件：dateRange和durations是串行的，必须同时满足，durations中的时间段是并行的，只要满足一个即可
   * {"showTimeLimit":true,"dateLimit":false,"dateRange":{"start":"2018-12-04T16:00:00.000Z","end":"2018-12-27T16:00:00.000Z"},"durations":[{"weekLimit":true,"weeks":[0,1,2],"timeLimit":true,"timeList":[{"timeRange":{"start":"2018-12-29T09:49:33.000Z","end":"2018-12-29T09:55:38.000Z"}},{"timeRange":{"start":"2018-12-29T09:50:50.000Z","end":"2018-12-29T09:52:53.000Z"}}]},{"weekLimit":false,"weeks":[0],"timeLimit":true,"timeList":[{"timeRange":{"start":"2018-12-29T09:50:03.000Z","end":"2018-12-29T10:52:05.000Z"}}]}]};
   * @param banners
   * @returns {*} 符合条件的banner列表
   */
  filterBannersByTimeLimit(banners) {
    return banners.filter((item) => {
      if (!item.timerLimit || !item.timerLimit.showTimeLimit) {
        /*如果没有时间限制，直接返回true*/
        return true;
      } else {
        const date = new Date();
        const currentTimestamp = date.getTime();
        const dateRange = item.timerLimit.dateRange;
        const durations = item.timerLimit.durations;

        /**
         * 是否命中周期时间限制条件
         * @returns {boolean}
         */
        function isHitDuration() {
          if (durations && durations.length) {
            //符合一个周期条件即可
            return durations.some((dur) => {
              const weeks = dur.weeks;
              if (weeks && weeks.length) {
                //检测当前日期是否在week列表里
                const inWeek = weeks.some((week) => {
                  return week === date.getDay();
                });
                if (!inWeek && dur.weekLimit) {
                  /*不在week列表里,设置了星期的限制直接返回false*/
                  return false;
                } else {
                  /*在week列表里，继续判断是否在时间列表里*/
                  const timeList = dur.timeList;
                  if (dur.timeLimit && timeList && timeList.length) {
                    const curHMS = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(); //时、分、秒
                    return timeList.some((timeRange) => {
                      if (timeRange.start && timeRange.end) {
                        const startDate = new Date(dateRange.start);
                        const startTimeHMS =
                          startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds(); //时、分、秒;
                        const endDate = new Date(timeRange.end);
                        const endTimeHMS = endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds(); //时、分、秒;
                        return curHMS <= endTimeHMS && curHMS >= startTimeHMS;
                      } else if (timeRange.start) {
                        const startDate = new Date(dateRange.start);
                        const startTimeHMS =
                          startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds(); //时、分、秒;
                        return curHMS >= startTimeHMS;
                      } else if (timeRange.end) {
                        const endDate = new Date(timeRange.end);
                        const endTimeHMS = endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds(); //时、分、秒;
                        return curHMS <= endTimeHMS;
                      } else {
                        return true;
                      }
                    });
                  } else {
                    return true;
                  }
                }
              } else {
                return true;
              }
            });
          } else {
            return true;
          }
        }

        function isHitDateRange() {
          if (item.dateLimit) {
            if (dateRange.start && dateRange.end) {
              const startDate = new Date(dateRange.start).getTime();
              const endDate = new Date(dateRange.end).getTime();
              return currentTimestamp <= endDate && currentTimestamp >= startDate;
            } else if (dateRange.start) {
              const startDate = new Date(dateRange.start).getTime();
              return currentTimestamp >= startDate;
            } else if (dateRange.end) {
              const endDate = new Date(dateRange.end).getTime();
              return currentTimestamp <= endDate;
            } else {
              return true;
            }
          } else {
            return true;
          }
        }

        if ((!item.timerLimit.dateLimit || !dateRange) && (!durations || !durations.length)) {
          /*没有日期和周期限制，直接返回true*/
          return true;
        } else if (!item.timerLimit.dateLimit || !dateRange) {
          /*如果没有日期限制，表明只有周期限制*/
          //这里是周期限制
          return isHitDuration();
        } else if (!durations || !durations.length) {
          /*如果没有周期限制，表明只有日期限制*/
          //这里是日期限制
          return isHitDateRange();
        } else {
          /*同时有日期和周期限制*/
          if (isHitDateRange()) {
            //符合一个周期条件即可
            return isHitDuration();
          } else {
            return false;
          }
        }
      }
    });
  },
};
