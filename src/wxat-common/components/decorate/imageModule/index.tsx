import React, {useState, FC, useCallback, useMemo, useEffect} from 'react';
import {_fixme_with_dataset_} from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import {useSelector} from 'react-redux';
import {Block, View, ScrollView, Swiper, SwiperItem, Navigator} from '@tarojs/components';
import OfficialAccountFlow from '@/wxat-common/components/decorate/officialAccountFlow/index'
import {NOOP_ARRAY} from '@/wxat-common/utils/noop';
import timerBannerUtil from './timerBannerUtil.js';
import jumpPath from '../utils/jump-path.js';
import wxApi from '../../../utils/wxApi';
import api from '@/wxat-common/api'
import imageUtils from '../../../utils/image.js';
import reportConstants from '../../../../sdks/buried/report/report-constants.js';
import report from '../../../../sdks/buried/report';
import authHelper from '../../../utils/auth-helper.js';

import sty from './index.module.scss';
import useToYQZ from '@/hooks/useToYQZ';
import util from '@/wxat-common/utils/util';

// 小程序装修-轮播图模块
interface ComponentProps {
  dataSource: Record<string, any>;
  showOfficialAccountFlow?: boolean;
  OfficialAccountImageUrl?: string;
}

const cdnImage = (src) => {
  return imageUtils.cdn(src);
};

const mapStateToProps = ({globalData, base}) => ({
  tabbars: globalData.tabbars,
  userInfo: base.userInfo,
  loginInfo: base.loginInfo,
  currentStore: base.currentStore
});

const ImageModule: FC<ComponentProps> = ({dataSource}) => {
  const [swiperCurrent, setSwiperCurrent] = useState(0);
  const {tabbars, userInfo, loginInfo, currentStore} = useSelector(mapStateToProps);
  const [showOfficialAccountFlow, setShowOfficialAccountFlow] = useState(false);
  const [OfficialAccountImageUrl, setOfficialAccountImageUrl] = useState('');
  const {handleDecorateToQYZ} = useToYQZ()

  useEffect(() => {
    setTimeout(() => {
      report.showBanner();
    }, 100);
  }, [dataSource]);
  const bannerList = useMemo(() => {
    if (dataSource && dataSource.data) {
      return dataSource.data.map((i) => {
        return {...i, src: cdnImage(i.src)};
      });
    }

    return NOOP_ARRAY;
  }, [dataSource && dataSource.data]);

  const filteredList = useMemo(() => {
    const filtered = timerBannerUtil.filterBannersByTimeLimit(bannerList);
    if (filtered.length !== bannerList.length) {
      return filtered;
    }
    return bannerList;
  }, [swiperCurrent, bannerList]);

  const cancelFlow = () => {
    setShowOfficialAccountFlow(false)
  }

  const getImageInfo = () => {
    const params = {
      appId: userInfo.appId || loginInfo.appId,
      storeId: currentStore.id
    }

    wxApi
      .request({
        url: api.officiaAccountFlow.getImage,
        data: params,
        loading: false,
        quite: true,
      }).then((res) => {
      if (res.success && res.data && res.data.image) {
        setOfficialAccountImageUrl(res.data.image)
      }
    })
  }

  const handleClick = async (e) => {
    const item = e.target.dataset.item;
    const index = e.target.dataset.index;
    util.sensorsReportData('click_carousel', {
      event_name: '轮播图点击',
      carousel_label: item.label,
      carousel_name: item.linkName,
      carousel_link_page: item.linkPage,
      carousel_type: item.linkPageKey,
    })
    if (item.linkPageKey && String(item.linkPageKey).includes('yqz')) {
      await handleDecorateToQYZ(item)
      return
    }

    if (item.linkPageKey === "officialAccountFlow") {
      getImageInfo()
      setShowOfficialAccountFlow(true)
      return
    }

    if (!!item.H5Link) {
      // window.location.href = item.H5Link;
      wxApi.$locationTo(item.H5Link)
      return;
    }

    if (item.openType === 'h5' && item.webLinkUrl) {
      // web-view H5链接

      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/custom-webview/index',
        data: {linkUrl: item.webLinkUrl},
      });

      return;
    }
    if (!(!item.linkPage || item.linkPage === undefined)) {
      report.clickBanner(index, item.linkPage);
      if (item.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }
      if (item.linkId === 'product' && !item.detailId) {
        return;
      }
      if (item.linkPageKey == 'mate-tool') {
        const {list} = tabbars;
        const realPathIndex = list.findIndex(({realPath}) => realPath == item.linkPage);
        if (~realPathIndex) {
          return wxApi.switchTab({
            url: list[realPathIndex].pagePath,
          });
        }
      }
      // 判断是否拨打电话进来
      if (item.linkPageKey === "oneTouchDial" && item.linkOneTouchDial) {
        wxApi.makePhoneCall({
          phoneNumber: item.linkOneTouchDial
        })
      }
      // wxApi.$navigateTo(jumpPath(item, '', reportConstants.SOURCE_TYPE.banner.key))
      // 兼容场地预定路径迁移到主包
      if (item.linkPage === "sub-packages/marketing-package/pages/living-service/hotel-reservation-list/index") {
        item.linkPage = "wxat-common/pages/hotel-reservation-list/index"
      } else if (item.linkPage === "sub-packages/marketing-package/pages/living-service/goods-rental/index") {
        item.linkPage = "wxat-common/pages/service-classification-tree/index"
      }
      // 判断是否是住中服务进来的
      if ((item.linkPageKey === "service-category" && item.serviceCateGoryId && item.serviceCateGoryId.length) || item.serviceItemId || item.wifiCateGoryId) {
        if (item.linkPage === "wxat-common/pages/goods-detail/index") {
          item.linkPage = "sub-packages/marketing-package/pages/living-service/goods-rental-details/index"
        }
        wxApi.$navigateTo({
            url: jumpPath(item, '', reportConstants.SOURCE_TYPE.banner.key).url,
            data: {
              id: item.serviceItemId ? item.serviceItemId : item.serviceCateGoryId,
              sourceType: item.sourceType
            }
          }
        );
      } else {
        wxApi.$navigateTo(jumpPath(item, '', reportConstants.SOURCE_TYPE.banner.key))
      }
    }
  };

  const onSwiperChange = useCallback((evt) => {
    setTimeout(() => {
      setSwiperCurrent(evt.detail.current);
    })

  }, []);

  return (
    <View
      className={sty.imageModule}
      style={{
        margin: Taro.pxTransform(dataSource.marginUpDown * 2) + ' ' + Taro.pxTransform(dataSource.marginLeftRight * 2),
        borderRadius: Taro.pxTransform(dataSource.radius * 2),
      }}
    >
      <ScrollView
        scrollY
        className='page-body'
        // onScrollToLower={this.loadMore}
      >
        {!!(filteredList && filteredList.length) && (
          <Swiper
            indicatorDots={filteredList.length > 1}
            onChange={onSwiperChange}
            autoplay
            interval={5000}
            duration={500}
            circular
            current={swiperCurrent}
            className='advertisement-play-swiper'
            style={{height: Taro.pxTransform(dataSource.height * 2)}}
            indicatorActiveColor='#ffffff'
            indicatorColor='rgb(255, 255, 255, 0.4)'
          >
            {filteredList.map((item, index) => {
              return (
                <Block key={item.src + index}>
                  <SwiperItem key={index} className={sty.advertisementPlayItem}>
                    {item.linkPage !== 'miniProgram' ? (
                      <View
                        className={sty.image}
                        style={{
                          backgroundImage: `url(${item.src})`,
                          height: Taro.pxTransform(dataSource.height * 2),
                          borderRadius: Taro.pxTransform(dataSource.radius * 2),
                        }}
                        onClick={_fixme_with_dataset_(handleClick, {item: item, index: index})}
                      ></View>
                    ) : (
                      <Navigator key={index} target="miniProgram" appId={item.linkId} path={item.linkPath}>
                        <View
                          className={sty.image}
                          style={{
                            backgroundImage: `url(${item.src})`,
                            height: Taro.pxTransform(dataSource.height * 2),
                            borderRadius: Taro.pxTransform(dataSource.radius * 2),
                          }}
                          onClick={_fixme_with_dataset_(handleClick, {item: item, index: index})}
                        ></View>
                      </Navigator>
                    )}
                  </SwiperItem>
                </Block>
              );
            })}
          </Swiper>
        )}
      </ScrollView>

      {showOfficialAccountFlow && OfficialAccountImageUrl && OfficialAccountFlow && (
        <OfficialAccountFlow
          imageUrl={OfficialAccountImageUrl}
          onCancel={cancelFlow}
        ></OfficialAccountFlow>
      )}
    </View>
  );
};

ImageModule.defaultProps = {
  dataSource: {},
};

export default ImageModule;
