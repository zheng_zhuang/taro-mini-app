import React, { useState, useEffect, FC } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import withWeapp from '../../customConfigMap/node_modules/@tarojs/with-weapp';
// import wxClass from '../../../utils/wxClass.js';
// import config from '../utils/config.js';
import template from '../../../utils/template.js';
import seckillEnum from '../../../constants/seckillEnum.js';

import MarketingMoudle from '../marketingModule/index';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';

// props的类型定义
type ComponentProps = {
  _renderList: Array<Record<string, any>>;
  // PADDING: number
  showType: number;
};

let SeckillModule: FC<ComponentProps> = ({ _renderList, showType }) => {
  const defaultComputeDataList: Array<Record<string, any>> = [];
  const [computeDataList, setcomputeDataList] = useState(defaultComputeDataList);
  const [tmpStyle, setTmpStyle] = useState('');

  useEffect(() => {
    getTemplateStyle();
  }, []);

  useEffect(() => {
    if (!!_renderList && _renderList.length) {
      getComputeDataList();
    }
  }, [_renderList]);

  const getComputeDataList = () => {
    const data = _renderList;
    const seckillEnumStatus = seckillEnum.STATUS;
    const list = data.map((item) => {
      const getPrice = item.priceRange.split('~');
      const minPrice = getPrice[0];
      return {
        id: item.id,
        itemNo: item.itemNo,
        status: item.status,
        name: item.name,
        thumbnail: item.thumbnail,
        tipLabel: item.remainTitle + item.displayTime,
        salePrice: minPrice * 100,
        labelPrice: item.salePrice,
        saleAmount: '已秒' + item.salesVolume + '件',
        saleLabel: '秒杀价：',
        sign: '秒',
        btn:
          item.currentStatus === seckillEnumStatus.NOT_STARTED.value
            ? '即将开抢'
            : item.currentStatus === seckillEnumStatus.ON_GOING.value
            ? '立即秒杀'
            : '原价购买',
        type:item.type
      };
    });
    setcomputeDataList(list);
  };

  const go = (e) => {
    const item = e;
    console.log('e', e);
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/seckill/detail/index',
      data: {
        itemNo: item.itemNo || null,
        activityId: item.id || null,
        seckillStatus: item.status || 0,
      },
    });
  };

  //获取模板配置
  const getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    setTmpStyle(templateStyle);
  };

  return (
    <View
      data-fixme='03 add view wrapper. need more test'
      data-scoped='wk-cds-SeckillModule'
      className='wk-cds-SeckillModule'
    >
      <MarketingMoudle computeDataList={computeDataList} showType={showType} onClick={go}></MarketingMoudle>
    </View>
  );
};

// 给props赋默认值
SeckillModule.defaultProps = {
  _renderList: [],
  // PADDING: 0,
  showType: 0,
};

export default SeckillModule;
