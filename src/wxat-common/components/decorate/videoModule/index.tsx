import React from 'react';
import { _safe_style_, createVideoRef } from '@/wxat-common/utils/platform';
import { View, Video } from '@tarojs/components';
import Taro from '@tarojs/taro';

import './index.scss';
import { connect } from 'react-redux';
import { updateBaseAction } from '@/redux/base';

interface VideoModuleProps {
  dataSource: Record<string, any>;
}

interface VideoModule {
  props: VideoModuleProps;
}

const mapStateToProps = (state) => ({
  currentVideoId: state.base.currentVideoId,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchCurrentVideo: (id: string) => {
    dispatch(updateBaseAction({ currentVideoId: id }));
  },
});

let uid = 0;

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class VideoModule extends React.Component {
  state = {
    video: null,
    videoStyle: '',
    isPlay: false,
    isShowControls: true,
  };

  uuid = `dvideo-${uid++}-${Math.random().toString(16).slice(2)}`;
  videoCtx = createVideoRef(this.uuid, this);
  _observer;
  componentDidMount() {
    if (!!this.props.dataSource && !!this.props.dataSource.data) {
      const { data, radius } = this.props.dataSource;
      this.setState({
        video: data,
        videoStyle: radius ? 'border-radius:' + radius / 2 + 'px;' : '',
      });
    }
    // this.scrollPlay();
  }

  componentDidUpdate(pre) {
    if (pre.currentVideoId !== this.props.currentVideoId) {
      this.setCurrentVideo();
    }
  }

  scrollPlay = () => {
    // 出现在视口就播放
    this._observer = Taro.createIntersectionObserver(this.$scope, { thresholds: [1], observeAll: true });
    this._observer.relativeToViewport().observe('.video-module', (res) => {

      if (res.intersectionRatio === 1) {
        this.setState({ isShowControls: false });
        this.videoCtx.current && this.videoCtx.current.play();
      } else {
        this.videoCtx.current && this.videoCtx.current.pause();
      }
    });
  };

  handleVideoClick = () => {
    this.setState({ isShowControls: true });
  };

  handleVideoPlay = () => {
    this.setState({ isPlay: true });
    this.props.dispatchCurrentVideo(this.uuid);
  };

  handleVideoPause = () => {
    this.setState({ isPlay: false });
  };

  setCurrentVideo = () => {
    const { currentVideoId } = this.props;
    if (currentVideoId !== this.uuid) {
      this.videoCtx.current && this.videoCtx.current.pause();
      this.setState({ isPlay: false });
    }
  };

  render() {
    const { video, videoStyle, isShowControls } = this.state as Record<string, any>;
    const { dataSource } = this.props;

    return (
      <View data-scoped='wk-cdv-VideoModule' className='wk-cdv-VideoModule video-module'>
        {!!video && (
          <View
            style={_safe_style_(
              'margin:' + (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px')
            )}
          >
            <Video
              src={video.cdnUrl}
              style={_safe_style_('width: 100%;' + videoStyle)}
              ref={this.videoCtx}
              id={this.uuid}
              controls={isShowControls}
              onClick={this.handleVideoClick}
              onPlay={this.handleVideoPlay}
              onPause={this.handleVideoPause}
            ></Video>
          </View>
        )}
      </View>
    );
  }
}

export default VideoModule;
