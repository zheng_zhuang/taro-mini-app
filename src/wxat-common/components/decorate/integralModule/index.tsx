import React, { FC } from 'react';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import './index.scss';

export interface IntegralItem {
  itemName: string;
  thumbnail: string;
  exchangeIntegral: string;
  salePrice: number;
  labelPrice: number;
  itemSalesVolume: number;
}

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
  _renderList: IntegralItem[];
  showNumberType?: number;
  PADDING?: number;
  scoreName?: string;
}

const IntegralModule: FC<ComponentProps> = ({ scoreName, dataSource, _renderList, showNumberType }) => {
  // 提前解构props，减少getter的操作

  const onClickGoodsItem = (e) => {
    const item = e.currentTarget.dataset.item;
    wxApi.$navigateTo({
      // url: '/sub-packages/moveFile-package/pages/goods-detail/index',
      url:'/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: item.itemNo,
        type: goodsTypeEnum.INTEGRAL.value,
        activityId: item.id,
      },
    });
  };
  return (
    !!(dataSource && dataSource.data && dataSource.data.length) && (
      <View data-scoped='wk-cdi-IntegralModule' className='wk-cdi-IntegralModule collage-module-container'>
        {showNumberType === 1 ? (
          <View>
            {_renderList.map((item, index) => {
              return (
                <View
                  className='container'
                  key={index}
                  onClick={_fixme_with_dataset_(onClickGoodsItem, { item: item })}
                >
                  <Image className='img' mode='aspectFill' src={item.thumbnail}></Image>
                  <View className='content'>
                    <View className='title'>{item.itemName}</View>
                    <View className='salePrice'>
                      {item.exchangeIntegral + scoreName + '+' + filters.moneyFilter(item.salePrice, true) + '元'}
                    </View>
                    <View className='footer-box'>
                      <Text className='labelPrice'>{'￥' + filters.moneyFilter(item.labelPrice, true)}</Text>
                      <Text className='count'>{'已售' + item.itemSalesVolume}</Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        ) : (
          <View className='wrapper'>
            {_renderList.map((item, index) => {
              return (
                <View
                  className='col-2-container'
                  key={index}
                  onClick={_fixme_with_dataset_(onClickGoodsItem, { item: item })}
                >
                  <Image className='img' mode='aspectFill' src={item.thumbnail}></Image>
                  <View className='col-2-content'>
                    <View className='title'>{item.itemName}</View>
                    <View className='salePrice'>
                      {item.exchangeIntegral + scoreName + '+' + filters.moneyFilter(item.salePrice, true) + '元'}
                    </View>
                    <View className='footer-box'>
                      <Text className='count'>{'已售' + item.itemSalesVolume}</Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        )}
      </View>
    )
  );
};

// 给props赋默认值
IntegralModule.defaultProps = {
  dataSource: {},
  _renderList: [],
  showNumberType: 1,
  PADDING: 0,
  scoreName: '积分',
};

export default IntegralModule;
