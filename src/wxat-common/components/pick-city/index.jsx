import React from 'react';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import constants from '../../constants/index';
import wxApi from '../../utils/wxApi';
import api from '../../api/index';
import login from '../../x-login/index';
import businessTime from '../../utils/businessTime';
import report from '../../../sdks/buried/report/index';
import LoadMore from '../load-more/load-more';
import './index.scss';

import { connect } from 'react-redux';

const loadMoreStatusEnum = constants.order.loadMoreStatus;

const mapStateToProps = (state) => ({
  gps: state.base.gps,
  storeVO: state.base.storeVO,
});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class PickCity extends React.Component {
  static defaultProps = {
    // 门店过滤列表id字符串
    storeIds: null,
    //选择门店回调
    onPickStore: null,
  };

  state = {
    address: '',
    title: '',
    longlat: {},
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    storeList: [],
    focus: true,
    keyWords: '',
    currentIndex: -1,
    storeVO: {},
    loading: true,
  };

  initialize = false;
  ignoreAutoLocation = false;
  pageNo = 1;
  pageSize = 20;
  hasMore = true;

  findTheItem(storeId) {
    const { storeList } = this.state;
    return (
      storeList.find((item) => {
        return item.id === storeId;
      }) || {}
    );
  }

  //点击打开指定位置的地图
  clickLocation(e) {
    const { latitude, longitude, name, address } = this.findTheItem(e.currentTarget.dataset['id']);
    !!latitude &&
      wxApi.openLocation({
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        name,
        address,
      });
  }

  //点击直接电话商家
  clickPhone(e) {
    const { tel } = this.findTheItem(e.currentTarget.dataset['id']);
    tel &&
      wxApi.makePhoneCall({
        phoneNumber: tel,
      });
  }

  //选中当前店铺
  clickItem(e) {
    const storeId = e.currentTarget.dataset['id'];
    const currentIndex = e.currentTarget.dataset.index;

    report.clickStoreInfo(storeId);

    this.setState({
      currentIndex,
    });

    if (this.props.onPickStore) {
      this.props.onPickStore(this.findTheItem(storeId));
    }
  }

  clickChooseLocation() {
    this.ignoreAutoLocation = true;
    wxApi
      .getSetting()
      .then((res) => {
        if (!res.authSetting['scope.userLocation']) {
          return wxApi
            .authorize({
              scope: 'scope.userLocation',
            })
            .then(
              (res) => {
                report.reportLocation(true);
                return wxApi.chooseLocation();
              },
              (error) => {
                report.reportLocation(false);
              }
            );
        } else {
          return wxApi.chooseLocation();
        }
      })
      .then((location) => {
        if (!!location.address) {
          this.setState({
            address: location.address,
            title: location.name,
            longlat: {
              longitude: location.longitude,
              latitude: location.latitude,
            },
          });

          this.pageNo = 1;
          this.ignoreAutoLocation = true;
          this.api();
        } else {
          this.ignoreAutoLocation = false;
        }
      });
  }

  //定位经纬度＋地址解析
  wxLocation() {
    return wxApi
      .getSetting()
      .then((res) => {
        if (!res.authSetting['scope.userLocation']) {
          return wxApi
            .authorize({
              scope: 'scope.userLocation',
            })
            .then(() => {
              return wxApi.getLocation({ type: 'gcj02', isHighAccuracy: true, highAccuracyExpireTime: 500 });
            });
        } else {
          return wxApi.getLocation({ type: 'gcj02', isHighAccuracy: true, highAccuracyExpireTime: 500 });
        }
      })
      .then((res) => {
        const longitude = res.longitude,
          latitude = res.latitude;
        this.setState({
          longlat: {
            longitude: longitude,
            latitude: latitude,
          },
        });

        return wxApi
          .request({
            url: api.poi.location + '?lng=' + longitude + '&lat=' + latitude,
            loading: true,
          })
          .then((res) => {
            const data = res.data || {};
            this.setState({
              address: data.address || '',
              title: data.title || '',
            });
          });
      });
  }

  initRender() {
    if (!this.initialize && !this.ignoreAutoLocation) {
      this.initialize = true;
      this.wxLocation().finally(() => {
        this.initialize = false;
        this.pageNo = 1;
        this.api();
      });
    }
  }

  onRetryLoadMore() {
    this.api();
  }

  loadMore() {
    if (this.hasMore) {
      this.api();
    }
  }

  refreshList() {
    this.pageNo = 1;
    this.hasMore = true;
    this.setState({
      keyWords: '',
    });

    this.api();
  }

  api(keyWords) {
    const { gps, storeIds } = this.props;
    const { longlat } = this.state;
    if (this.pageNo > 1) {
      this.setState({
        loadMoreStatus: loadMoreStatusEnum.LOADING,
      });
    }
    this.setState({
      loading: true,
    });

    const { longitude, latitude } = longlat;
    let url = api.store.queryCustomizeShop + '?pageNo=' + this.pageNo + '&pageSize=' + this.pageSize;
    if (!!latitude) {
      url += '&longtitude=' + longitude;
      url += '&latitude=' + latitude;
    } else {
      if (!!gps.latitude) {
        url += '&latitude=' + gps.latitude;
        url += '&longtitude=' + gps.longtitude;
      }
    }

    // 门店过滤列表id字符串
    if (!!storeIds) {
      url += '&storeIds=' + storeIds;
    }

    if (!!keyWords) {
      url += '&storeName=' + keyWords;
    }

    login
      .login()
      .then((res) => {
        return wxApi.request({
          url,
          loading: true,
        });
      })
      .then((res) => {
        const { storeList } = this.state;
        const newData = res.data || [];
        const list = this.pageNo === 1 ? newData : storeList.concat(newData);
        if (!!list && !!list.length) {
          list.forEach((store) => {
            store.businessTime = businessTime.computed(
              store.businessStartHour,
              store.businessEndHour,
              store.businessDayOfWeek
            );
          });
        }

        if (!list) {
          this.setState({
            storeList: list || [],
          });
        } else {
          this.setState({
            storeList: list || [],
            loadMoreStatus: loadMoreStatusEnum.HIDE,
          });

          this.hasMore = newData.length >= this.pageSize;
          this.pageNo = this.pageNo + 1;
        }
      })
      .catch((error) => {
        this.setState({
          loadMoreStatus: loadMoreStatusEnum.ERROR,
        });
      })
      .finally(() => {
        wxApi.stopPullDownRefresh();
        this.setState({
          loading: false,
        });
      });
  }

  enterFocus(options) {
    this.setState({
      focus: true,
    });
  }

  loseFocus(options) {
    //fixme 需要加延迟操作，否则无法执行历史记录的点击事件
    setTimeout(() => {
      this.setState({
        focus: false,
      });
    }, 300);
  }

  confrim(options) {
    this.setState({
      focus: false,
    });

    this.pageNo = 1;
    const keyWords = options.detail.value;
    this.api(keyWords);
  }

  render() {
    const { focus, keyWords, storeVO, currentIndex, storeList, loadMoreStatus, loading } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-wcp-PickCity' className='wk-wcp-PickCity'>
        <View className='input-view'>
          <Image className='icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/search-icon.png"></Image>
          <Input
            className='input-search'
            focus={focus}
            placeholderClass='input-search-placeholder'
            placeholder='请输入城市名称'
            onConfirm={this.confrim.bind(this)}
            value={keyWords}
            confirmType='search'
            onFocus={this.enterFocus.bind(this)}
            onBlur={this.loseFocus.bind(this)}
          ></Input>
        </View>
        {!!(!!storeVO && storeVO.city) && (
          <View className='layout-pos'>
            <View className='store-title'>您当前在</View>
            <View
              className='city-item active'
              onClick={_fixme_with_dataset_(this.clickItem.bind(this), { index: '0', id: storeVO.id })}
            >
              {storeVO.city}
            </View>
          </View>
        )}

        <View className='store'>
          {storeList.map((store, index) => {
            return (
              <View
                key={store.id}
                className={'city-item limit-line ' + (currentIndex === index ? 'active' : '')}
                onClick={_fixme_with_dataset_(this.clickItem.bind(this), { index: index, id: store.id })}
              >
                {store.city}
              </View>
            );
          })}
        </View>
        {!!(!loading && storeList.length === 0) && <View className='store-search'>暂无该区域信息</View>}
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>
      </View>
    );
  }
}

export default PickCity;
