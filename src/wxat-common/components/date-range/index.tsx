import React, { FC, useMemo } from 'react';
import Taro from '@tarojs/taro';
import { Picker, View, Image } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import date from '@/wxat-common/utils/date';
import getStaticImgUrl from "../../constants/frontEndImgUrl"
import s from './style.module.scss';

export interface DateRangeProps {
  value: [Date, Date];
  onChange: (valeu: [Date, Date]) => void;
}

const FORMAT = 'yyyy-MM-dd';

const DateRange: FC<DateRangeProps> = (props) => {
  const { value, onChange } = props;
  const normalized = useMemo(() => {
    if (value) {
      const start = value[0] > value[1] ? value[1] : value[0];
      const end = value[0] > value[1] ? value[0] : value[1];

      return {
        start: date.format(start, FORMAT),
        end: date.format(end, FORMAT),
      };
    }
    return {};
  }, [value]);

  const setValue = (v, i) => {
    const d = new Date(v);
    const nvalue = [...value];
    nvalue[i] = d;
    onChange(nvalue);
  };

  return (
    <View className={s.container}>
      <Picker mode='date' value={normalized.start} onChange={(e) => setValue(e.detail.value, 0)}>
        <View className={s.item}>
          {normalized.start}
          <Image className={s.icon} src={getStaticImgUrl.images.rightIcon_png} />
        </View>
      </Picker>
      至
      <Picker mode='date' value={normalized.end} onChange={(e) => setValue(e.detail.value, 1)}>
        <View className={s.item}>
          {normalized.end}
          <Image className={s.icon} src={getStaticImgUrl.images.rightIcon_png} />
        </View>
      </Picker>
    </View>
  );
};

export default DateRange;
