import guideImg from './guide.png';
import shareUK from './shareUK.png';
import buttonImg from './button.png';

export default function show(e) {
  const el = document.createElement('div');
  if(e.type == 'shareYouke'){
    el.innerHTML = `<div class="wx-share-guide" style="position: fixed; width: 100vw; top: 0; height: 100vh; left: 0; background-color: rgba(0, 0, 0, 0.581); z-index: 10000;">
    <div style="position: absolute; top: 0; right: 0; width:68% ">
     <img  src="${shareUK}" style="max-width: 100%;" />     
    </div>
   </div>`;
  }else{
    el.innerHTML = `<div class="wx-share-guide" style="position: fixed; width: 100vw; top: 0; height: 100vh; left: 0; background-color: rgba(0, 0, 0, 0.581); z-index: 10000;">
    <div style="position: absolute; top: 0; right: 0; width: 58% ">
     <img src="${guideImg}" style="max-width: 100%;" />
     <img class="close" src="${buttonImg}" style="display: block; max-width: 100%" />
    </div>
   </div>`;
  }


  el.addEventListener('click', (e) => {
    if (e.target && e.target.className === 'close') {
      el.remove();
    }
  });

  document.body.appendChild(el);
}
