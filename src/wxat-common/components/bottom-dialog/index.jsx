import React from 'react'; // @hostExternalClassesConvered(BottomDialog)
// @externalClassesConvered(AnimatDialog)
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import AnimatDialog from '../animat-dialog/index';
import './index.scss';

@hoc
class BottomDialog extends React.Component {
  //todo: remove, dont modify
  // 外部样式类
  static externalClasses = ['custom-class'];

  //显示对话框
  showModal() {
    this.animatDialogCOMPT &&
      this.animatDialogCOMPT.show({
        translateY: 300,
      });
  }
  //隐藏对话框
  hideModal() {
    this.animatDialogCOMPT && this.animatDialogCOMPT.hide();
  }

  refAnimatDialogCOMPT = (node) => (this.animatDialogCOMPT = node);

  //隐藏事件传递给父组件
  maskHide = () => {
    if (this.props.onHide) {
      this.props.onHide();
    }
  };

  render() {
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-wcb-BottomDialog'
        className='wk-wcb-BottomDialog'
      >
        <AnimatDialog
          ref={this.refAnimatDialogCOMPT}
          animClass='commodity_attr_box'
          customClass={this.props.customClass}
          onHide={this.maskHide}
        >
          {this.props.children}
        </AnimatDialog>
      </View>
    );
  }
}

export default BottomDialog;
