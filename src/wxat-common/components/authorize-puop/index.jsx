import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Button, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import login from '@/wxat-common/x-login/index';
import report, { wkApi } from '@/sdks/buried/report/index';
import { updateBaseAction } from '@/redux/base';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import AuthInfoBtn from '@/wxat-common/components/auth-user/AuthInfoBtn';

import './index.scss';

const wxIcon = cdnResConfig.auth.wxIcon;

const mapStateToProps = (state) => {
  return {
    sessionId: state.base.sessionId,
    wxUserInfo: state.base.wxUserInfo,
    userInfo: state.base.userInfo,
  };
};

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateBase: (data) => dispatch(updateBaseAction(data)),
});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class AuthorizePuop extends React.Component {
  state = {
    ableShow: false,
    isAuth: false,
  };

  everRender = false;
  componentDidMount() {
    if (!this.everRender && !!this.props.sessionId) this.check();
  }

  componentDidUpdate(preProps) {
    if (preProps.sessionId !== this.props.sessionId) {
      this.check();
    }
  }

  // getUserInfo = (e) => {
  //   const { userInfo } = e.detail;
  //   const { dispatchUpdateBase } = this.props;

  //   if (userInfo) {
  //     dispatchUpdateBase({ wxUserInfo: userInfo });
  //     wkApi.updateRunTimeBzParam({ user: userInfo });
  //     //增加埋点上报 - 授权用户信息-成功
  //     report.reportAuthOperation(true, 0);
  //   } else {
  //     //增加埋点上报 - 授权用户信息-失败
  //     report.reportAuthOperation(false, 0);
  //   }
  //   this.setState({ isAuth: true });
  // };
  handleUserProfile = (detail) => {
    const { userInfo } = detail;
    const { dispatchUpdateBase } = this.props;

    if (userInfo) {
      dispatchUpdateBase({ wxUserInfo: userInfo });
      wkApi.updateRunTimeBzParam({ user: userInfo });
      //增加埋点上报 - 授权用户信息-成功
      report.reportAuthOperation(true, 0);
    } else {
      //增加埋点上报 - 授权用户信息-失败
      report.reportAuthOperation(false, 0);
    }
    this.setState({ isAuth: true });
  };

  check() {
    const { wxUserInfo, userInfo } = this.props;
    const hasWxUserInfo = !!wxUserInfo;
    const ableShow =
      // 没有获取过信息
      !hasWxUserInfo &&
      //从未注册
      !userInfo.id &&
      //从分享链接进入
      !!login.getBuryInfo()._ref_useId;

    this.setState({ ableShow });
    this.everRender = true;
  }

  render() {
    const { ableShow, isAuth } = this.state;

    return (
      // <View
      //   data-fixme='02 block to view. need more test'
      //   data-scoped='wk-wca-authorizePuop'
      //   className='wk-wca-authorizePuop'
      // >
      //   {!!(ableShow && !isAuth) && (
      //     <View className='wx-popup'>
      //       <Button className='auth-text' openType='getUserInfo' onGetuserinfo={this.getUserInfo}>
      //         授权头像昵称
      //       </Button>
      //     </View>
      //   )}
      // </View>
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wca-authorizePuop'
        className='wk-wca-authorizePuop'
      >
        {!!(ableShow && !isAuth) && (
          <View className='wx-popup'>
            <View className='bottom-container'>
              <Image className='wx-icon' mode='aspectFill' src={wxIcon}></Image>
              <View className='tips'>为了给您提供更好的服务</View>
              <View className='tips'>小程序申请获得您的头像、昵称</View>
              {/* <Button className='auth-btn' openType='getUserInfo' onGetuserinfo={this.getUserInfo}>
              授权
              </Button> */}
              <AuthInfoBtn text='授权' hideBorder onUserInfoCall={this.handleUserProfile} my-class='auth-btn' />
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default AuthorizePuop;
