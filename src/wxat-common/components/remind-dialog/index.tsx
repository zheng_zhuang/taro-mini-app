import React, { FC } from 'react';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';

import './index.scss';

// props的类型定义
type ComponentProps = {
  // dialog距离顶部的距离，支持rpx和百分比，属性值通css top
  top: string;
  // 是否展示标题
  showTitle: boolean;
  // 是否展示底部按钮
  showFooter: boolean;
  // 弹窗标题
  // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型
  // 属性初始值（可选），如果未指定则会根据类型选择一个
  title: string;
  message: string;
  // 弹窗内容
  content: string;
  // 弹窗取消按钮文字
  cancelText: string;
  // 弹窗确认按钮文字
  confirmText: string;
  visible: boolean;
  //弹窗slot内容名称
  slotContent: string;
  //title距离顶部的高度
  titleTop: string;
  //背景透明度
  backOpacity: string;
  contentBackgroundColor: string;
  contentBoxShadow: string;
  borderRadius: string;
  width: string;
  maxWidth: string;
  //内容的外边距
  contenMargin: string;
  contentPadding: string;
};

const RemindDialog: FC<ComponentProps> = (props) => {
  const { visible } = props;
  /*
   * 公有方法
   */

  const preventTouchMove = () => {
    //阻止滑动
  };

  return (
    visible && (
      <View className='wx_dialog_container' onTouchMove={preventTouchMove}>
        <View className='wx-dialog'>
          <View className='wx-dialog-content'>
            点击 “<Image className='img' src="https://bj.bcebos.com/htrip-mp/static/app/images/wxat-common/icon.png"></Image>”
            添加到我的小程序，微信首页下拉即可快速访问店铺
          </View>
        </View>
      </View>
    )
  );
};

// 给props赋默认值
RemindDialog.defaultProps = {
  // dialog距离顶部的距离，支持rpx和百分比，属性值通css top
  top: '35%',
  // 是否展示标题
  showTitle: true,
  // 是否展示底部按钮
  showFooter: true,
  // 弹窗标题
  title: '提示',
  message: '',
  // 弹窗内容
  content: '弹窗内容',
  // 弹窗取消按钮文字
  cancelText: '取消',
  // 弹窗确认按钮文字
  confirmText: '确定',
  visible: false,
  //弹窗slot内容名称
  slotContent: 'content',
  //title距离顶部的高度
  titleTop: '0px',
  //背景透明度
  backOpacity: '0.5',
  contentBackgroundColor: '#f8ffff',
  contentBoxShadow: '0px 4px 24px 0px rgba(0,0,0,0.13)',

  borderRadius: '8px',
  width: '100%',
  maxWidth: '590px',
  //内容的外边距
  contenMargin: '40px 0 50px 0',
  contentPadding: '0 30px',
};

export default RemindDialog;
