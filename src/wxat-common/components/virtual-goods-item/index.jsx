import { Block, View, Image, Text, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
// import withWeapp from '@tarojs/with-weapp';
import filters from '../../utils/money.wxs.js';
import format from '../../utils/filter-card-pack.wxs.js';
// import wxClass from '../../utils/wxClass.js';
import goodsTypeEnum from '../../constants/goodsTypeEnum.js';
import template from '../../utils/template.js';
import store from '@/store';
import { connect } from 'react-redux';
/*import HorizonTmpl from '../../../imports/HorizonTmpl.js';
import OneRowThreeTmpl from '../../../imports/OneRowThreeTmpl.js';
import OneRowTwoTmpl from '../../../imports/OneRowTwoTmpl.js';
import ClassicListTmpl from '../../../imports/ClassicListTmpl.js';
import BigPictureModeTmpl from '../../../imports/BigPictureModeTmpl.js';*/
import './index.scss';
const DisplayType = {
  BIGPICTURE: 'bigPictureMode',
};

const app = Taro.getApp();
/**
 * 商品item组件
 * 支持横向和纵向两种展示形式
 * 所需参数参考properties
 */

/*@withWeapp({

  attached() {
    this.getTemplateStyle(); // 获取主题模板配置

    this.setData({
      industry: app.globalData.industry,
    });
  },

  methods: {
    getTemplateStyle() {
      const templateStyle = template.getTemplateStyle();
      this.setData({
        tmpStyle: templateStyle,
      });
    },
    onClickGoodsItem() {
      this.triggerEvent('click', this.data.virtualGoodsItem);
    },
  },
})*/
const mapStateToProps = (state) => {
  return {
    iosSetting:state.globalData.iosSetting,
    isIOS:state.globalData.isIOS
  };
};
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class VirtualGoodsItem extends React.Component {

  static defaultProps = {
    display: {
      /*商品item展示类型，enum: horizon:横向展示的商品item，供垂直列表展示使用 vertical:垂直展示的商品item，供横向滑动列表展示使用*/
      type: String,
      value: DisplayType.BIGPICTURE,
    },

    showDivider: {
      /*是否显示底部分割线,可根据列表是否时最后一个元素控制分割线是否展示，此属性只有在display=='horizon'有效*/
      type: Boolean,
      value: true,
    },

    virtualGoodsItem: {
      /*商品item*/
      type: Object,
      value: {},
    },

    reportSource: {
      type: String,
      value: '',
    },
    onClick: null,
  };
  state = {
    tmpStyle: {}, // 主题模板配置
    DisplayType,
    isMember: store.getState().globalData.scoreName,
    itemSalePrice: 0,
    itemLabelPrice: 0,
    itemMemberPrice: null,
    itemStock: 0,
    picTags: null,
    descTags: null,
    miniPriceFont: false,
    /*价格的字体是否显示小字体*/
    defaultTagCss:
      'height:28rpx;\n' +
      'background:rgba(101,126,255,0.18);\n' +
      'border:2rpx solid rgba(101,126,255,0.18);' +
      'font-size:20rpx;\n' +
      'font-family:PingFangSC-Light;\n' +
      'font-weight:300;\n' +
      'color:rgba(101,126,255,1);\n' +
      'line-height:28rpx;',
    industry: store.getState().globalData.industry,
    goodsTypeEnum,
  };

  UNSAFE_componentWillMount() {
    this.getTemplateStyle(); // 获取主题模板配置
    this.setState({
      industry: store.getState().globalData.industry,
    });
  }

  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  onClickGoodsItem =() => {
    // this.triggerEvent('click', this.data.virtualGoodsItem);
    this.props.onClick(this.props.virtualGoodsItem)
  }



  render() {
    const {  tmpStyle } = this.state;
    const { virtualGoodsItem , display,isIOS,iosSetting } = this.props;
    // 一行1个 大图模式
    const _renderBigPictureMode = (
      <View className="n-cards-item-container big-picture-mode" onClick={this.onClickGoodsItem}>
        <View className="cards-img-box">
          <Image mode="aspectFill" src={ virtualGoodsItem.thumbnail }/>
        </View>
        <View className="card-detail">
          <View className="title-wraper">
            <Text className="ellipsis title">{ virtualGoodsItem.name }</Text>
          </View>
          <View className="ellipsis content">{ virtualGoodsItem.subtitle }</View>
          <View>
            <View className="price" style={{color:tmpStyle.btnColor}}>
              <Text className="price-symbol">￥</Text>
              { filters.moneyFilter(virtualGoodsItem.salePrice, true) }
            </View>
          </View>
          {
            isIOS && iosSetting?.payType ?  <button className="btn-red" style={{background:tmpStyle.btnColor}}>{
              iosSetting?.buttonCopywriting || '立即购买'
            }</button>
            :
            <button className="btn-red" style={{background:tmpStyle.btnColor}}>立即购买</button>
          }
        </View>
      </View>
    );

    // 单行形式(左图右文) 经典列表

    const _renderClassicList = (
      <View className="n-cards-item-container classic-list" onClick={this.onClickGoodsItem}>
        <View className="cards-img-box">
          <Image className="classic-list-img" mode="aspectFill" src={ virtualGoodsItem.thumbnail }/>
        </View>
        <View className="card-detail">
          <View className="more-ellipsis title-wraper">
            <Text className="title">{ virtualGoodsItem.name }</Text>
          </View>
          {(virtualGoodsItem.privilgeCardName && <View className="more-ellipsis title-wraper" >
            <Text className="title">{ virtualGoodsItem.privilgeCardName }</Text>
          </View>)}
          <View>
            <View>
              <View className="price"  style={{color:tmpStyle.btnColor}}>
                <Text className="price-symbol">￥</Text>
                { filters.moneyFilter(virtualGoodsItem.salePrice, true) }
              </View>
            </View>
          </View>
          {
            isIOS && iosSetting?.payType ?  <button className="btn-red" style={{background:tmpStyle.btnColor}}>{
              iosSetting?.buttonCopywriting || '立即购买'
            }</button> :
            <button className="btn-red" style={{background:tmpStyle.btnColor}}>立即购买</button>
          }

        </View>
      </View>
    );

    const _renderOneRowTwo = (
      <View className="n-cards-item-container one-row-two" onClick={this.onClickGoodsItem}>
        <View className="cards-img-box">
          <Image mode="aspectFill" src={ virtualGoodsItem.thumbnail }/>
        </View>
        <View className="card-detail">
          <View className="more-ellipsis title-wraper">
            <Text className="title">{ virtualGoodsItem.name }</Text>
          </View>
          <View className="card-detail-item">
            <View className="one-row-two-detail">
              <View className="price" style={{color:tmpStyle.btnColor}}>
                <Text className="price-symbol">￥</Text>
                { filters.moneyFilter(virtualGoodsItem.salePrice, true) }
              </View>
            </View>
            <View>
              {
                isIOS && iosSetting?.payType ?  <button className="btn-red" style={{background:tmpStyle.btnColor}}>{
                  iosSetting?.buttonCopywriting || '立即购买'
                }</button> :  <button className="btn-red" style={{background:tmpStyle.btnColor}}>立即购买</button>
              }

            </View>
          </View>
        </View>
      </View>
    );

    const _renderOneRowwThree = (
      <View className="n-cards-item-container one-row-three" onClick={this.onClickGoodsItem}>
        <View className="cards-img-box">
          <Image mode="aspectFill" src={ virtualGoodsItem.thumbnail }/>
        </View>
        <View className="card-detail">
          <Text className="ellipsis title">{ virtualGoodsItem.name }</Text>
          <View className="price" style={{color:tmpStyle.btnColor}}>
            <Text className="price-symbol">￥</Text>
            { filters.moneyFilter(virtualGoodsItem.salePrice, true) }
          </View>
        </View>
      </View>
    );

    const _renderHorizon = (
      <View className="n-cards-item-container horizon horizon-container-item" onClick={this.onClickGoodsItem}>
        <View className="cards-img-box">
          <Image mode="aspectFill" src={ virtualGoodsItem.thumbnail }/>
        </View>
        <View className="card-detail">
          <Text className="ellipsis title">{ virtualGoodsItem.name }</Text>
          <View className="price" style={{color:tmpStyle.btnColor}}>
            <Text className="price-symbol">￥</Text>
            { filters.moneyFilter(virtualGoodsItem.salePrice, true) }
          </View>
        </View>
      </View>
    );

    return (
      <View
        data-fixme="02 block to View. need more test"
        data-scoped="wk-swcv-VirtualGoodsItem"
        className="wk-swcv-VirtualGoodsItem"
      >
        {/*  一行1个 大图模式  */}
        {/*  单行形式(左图右文) 经典列表  */}
        {/*  一行两个  */}
        {/*  一行三个  */}
        {/*  滑动  */}
          {/* {_renderFunc[display]()} */}
          {display === 'bigPictureMode' && _renderBigPictureMode}
          {display === 'classicList' && _renderClassicList}
          {display === 'oneRowTwo' && _renderOneRowTwo}
          {display === 'oneRowThree' && _renderOneRowwThree}
          {display === 'horizon' && _renderHorizon}
      </View>
    );
  }
}

export default VirtualGoodsItem;
