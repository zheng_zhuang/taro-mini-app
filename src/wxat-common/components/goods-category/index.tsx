import '@/wxat-common/utils/platform';
import React, { ComponentClass, RefObject, Component, createRef } from 'react';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import GoodsFilter from '../goods-filter/index';
import GoodsListSingleColumn from '../goods-list-single-column/index';
import './index.scss';

type PageOwnProps = {
  categoryId: number;
};

type PageState = {};

interface GoodsCategory {
  props: PageOwnProps;
}

class GoodsCategory extends Component {
  static defaultProps = {
    categoryId: 0,
  };

  // todo:
  // goodsFilterCMPT: RefObject<GoodsFilter> | null
  goodsFilterCMPT: any = createRef();
  goodsListCMPT: Taro.RefObject<any> = createRef();
  state = {
    sortCondition: {
      inStock: null,
      sortType: null,
      sortField: null,
    },
  };

  onFilterChange = ({ inStock, sortField, sortType }) => {
    if (this.goodsListCMPT.current) {
      this.setState({ sortCondition: { inStock, sortField, sortType } });
    }
  };

  // 加载更多，传递函数
  loadMore = () => {
    if (this.goodsListCMPT.current) {
      this.goodsListCMPT.current.loadMore();
    }
  };

  onCategoryChanged = () => {
    if (this.goodsFilterCMPT.current) {
      this.goodsFilterCMPT.current.resetSortArrayOnCategoryIdChange();
    }
  };

  render() {
    const { categoryId } = this.props;
    const { sortCondition } = this.state;

    return (
      <View data-scoped='wk-wcg-GoodsCategory' className='wk-wcg-GoodsCategory goods-category-container'>
        <GoodsFilter ref={this.goodsFilterCMPT} onFilterChange={this.onFilterChange} />
        <GoodsListSingleColumn ref={this.goodsListCMPT} categoryId={categoryId} sortCondition={sortCondition} />
      </View>
    );
  }
}

export default GoodsCategory as ComponentClass<PageOwnProps, PageState>;
