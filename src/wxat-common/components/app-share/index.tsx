import React, { useEffect } from 'react';
import Taro from '@tarojs/taro';
import wxApi, { ShareAppMessageParams } from '@/wxat-common/utils/wxApi';
import { Block } from '@tarojs/components';
import { usePage } from '@/wxat-common/utils/platform';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';

export interface AuthShareProps extends ShareAppMessageParams {
  deps?: any[];
  params?: ShareAppMessageParams | (() => ShareAppMessageParams);
}

/**
 * App 分享信息组件封装
 * 注意：实验性, 随时会废弃, 尽量不要使用
 */
let AppShare = (props: AuthShareProps) => {
  const { title, path, imageUrl, deps = NOOP_ARRAY } = props;
  const page = usePage();

  // 设置
  useEffect(() => {
    if (page) {
      const payload = props.params ? (typeof props.params === 'function' ? props.params() : props.params) : props;
      wxApi.$setShareAppMessage(payload, page);
      console.info('正在设置分享信息: ', payload);
    }
  }, [page, title, path, imageUrl, ...deps]);

  // 清理
  useEffect(() => {
    if (page) {
      return () => {
        wxApi.$setShareAppMessage(null, page);
      };
    }
  }, [page]);

  return <Block />;
};

export default AppShare;
