import React, { FC, useState, useEffect, useRef } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';

import api from '../../api/index.js';
import wxApi from '../../utils/wxApi';

import NavigationModule from '../decorate/navigationModule/index';
import GoodsCategory from '../goods-category/index';
import './index.scss';

// props的类型定义
interface IProps {
  categoryId: number;
}

interface INavigationConfig {
  rowNum: number;
  list: any[];
}

let GoodsCategoryCollection: FC<IProps> = ({ categoryId }) => {
  const [navigationConfig, setNavigationConfig] = useState<INavigationConfig>({
    rowNum: 5,
    list: [],
  });

  const [topImage, setTopImage] = useState({
    imgUrl: '',
    linkUrl: '',
  });

  const refGoodsCategoryCMPT = useRef<any>(null);

  useEffect(() => {
    apiGetCategory();
  }, []);

  useEffect(() => {
    handleCategoryIdChange();
  }, [categoryId]);

  const handleCategoryIdChange = () => {
    console.log('handleCategoryIdChange --------->', categoryId);
    apiGetCategory();

    refGoodsCategoryCMPT.current && refGoodsCategoryCMPT.current.onCategoryChanged();
  };

  const loadMore = () => {
    refGoodsCategoryCMPT.current && refGoodsCategoryCMPT.current.loadMore();
  };

  const clickTopImage = () => {
    if (!topImage.linkUrl) {
      return;
    }
    const url = '/wxat-common/pages/goods-detail/index';
    const itemNo = _getImgUrlValueParams(topImage.linkUrl, 'detailId');
    wxApi.$navigateTo({
      url,
      data: {
        itemNo,
      },
    });
  };

  //分解后台返回的商品图片字符串中的参数
  const _getImgUrlValueParams = (str, name) => {
    const reName = str.split(name + '=')[1];
    if (!reName) {
      return '';
    }
    return reName.split('&')[0] || '';
  };

  const apiGetCategory = () => {
    const params = {
      id: categoryId,
    };

    wxApi
      .request({
        url: api.goods.childCategory,
        data: params,
      })
      .then((res) => {
        //console.log('apiGetChildCategory ----->', JSON.stringify(res.data));
        composeData(res.data);
      })
      .catch((error) => {
        console.log('apiGetChildCategory error: ', error);
      });
  };

  const composeData = (data) => {
    const topImage = {
      imgUrl: data.imgUrl,
      linkUrl: data.imgUrlValue,
    };

    const childrenCategory = data.childrenCategory || [];
    const categories: any[] = [];
    childrenCategory.forEach((item) => {
      const child: {
        logo: string;
        name: string;
        link: any[];
      } = {
        logo: '',
        name: '',
        link: [],
      };

      child.logo = item.imgUrl;
      child.name = item.name;

      // link 组装
      child.link.push('category');
      const obj = {
        id: item.id,
        name: item.name,
      };

      child.link.push(JSON.stringify(obj));

      categories.push(child);
    });

    const config = {
      rowNum: 5,
      circularCorner: 1,
      marginLeftRight: 10,
      radius: 8,
      list: categories,
    };

    setTopImage(topImage);
    setNavigationConfig(config);

    console.log('----------->', navigationConfig);
  };

  return (
    <View
      data-scoped='wk-wcg-GoodsCategoryCollection'
      className='wk-wcg-GoodsCategoryCollection goods-category-collection'
    >
      {!!topImage.imgUrl && (
        <Block>
          <Image className='top-image' mode='center' src={topImage.imgUrl} onClick={clickTopImage} />
        </Block>
      )}

      {!!(navigationConfig.list && navigationConfig.list.length > 0) && (
        <View className='navigator'>
          <NavigationModule dataSource={navigationConfig}></NavigationModule>
        </View>
      )}

      <View className='goods-category'>
        <GoodsCategory
          // id='goods-category'
          ref={refGoodsCategoryCMPT}
          categoryId={categoryId}
        ></GoodsCategory>
      </View>
    </View>
  );
};

// 给props赋默认值
GoodsCategoryCollection.defaultProps = {
  categoryId: 0,
};

export default GoodsCategoryCollection;
