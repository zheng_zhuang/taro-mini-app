import { View } from '@tarojs/components'
import { FC } from '@tarojs/taro';
import style from '@/wxat-common/components/HPActionSheet/index.module.scss';
import classNames from 'classnames';
import { _safe_style_ } from '@/wxat-common/utils/platform';

interface IProps {
  show: boolean;
  children: any,
  onHide: () => void,
  customStyle?: any,
}
const HPActionSheet: FC<IProps> = (
  {
    show,
    children,
    onHide,
    customStyle = ''
  }
) => {

  return (
    <View>
      {
        show && (<View className={style.mark} onClick={onHide}></View>)
      }
      <View
        className={classNames(style.actionSheet, show && style.actionSheetShow)}
        style={_safe_style_(show && customStyle)}
      >
        {children}
      </View>
    </View>
  )
}

export default HPActionSheet;
