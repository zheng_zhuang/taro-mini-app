import React, { useEffect, useState, FC, useMemo } from 'react';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import { Block, View, ScrollView, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import utilDate from '../../utils/date.js';
import './index.scss';

// props的类型定义
type ComponentProps = {
  appointmentTimes: Array<Record<string, any>>;
  limitDay: number;
};

const AppointmentTimeSelect: FC<ComponentProps> = ({ appointmentTimes, limitDay }) => {
  const tmpStyle = useTemplateStyle();
  const [activeDate, setActiveDate] = useState(() => dates[0]);
  const [activeTime, setActiveTime] = useState('');
  const [times, setTimesx] = useState([]);
  const dates = useMemo(() => {
    const dates: Array<{ date: string; display: string; week: string; day: number }> = [];
    const weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
    const now = new Date();
    for (let i = 0; i < limitDay; i++) {
      const newDate = utilDate.addDays(now, i);
      const day = newDate.getDay();
      dates.push({
        date: utilDate.format(newDate, 'yyyy/MM/dd hh:mm:ss'),
        display: utilDate.format(newDate, 'MM.dd'),
        week: i === 0 ? '今天' : weeks[day],
        day: day,
      });
    }
    return dates;
  }, [limitDay]);

  useEffect(() => {
    if (appointmentTimes.length > 0) {
      setTimes(activeDate);
      triggerDataChanged();
    }
  }, [appointmentTimes]);

  /**
   * 根据选择日期，获取可预约时间
   */
  const setTimes = (activeDate) => {
    let times = [];
    let finded;
    if (activeDate) {
      const now = new Date();
      const selectedDate = utilDate.format(new Date(activeDate.date), 'yyyy/MM/dd');
      const nowDate = utilDate.format(now, 'yyyy/MM/dd');
      const currentTime = utilDate.format(now, 'hh:mm');
      const isToday = nowDate === selectedDate;

      // 有指定日期，设置时间选择
      let week = activeDate.day - 1;
      week = week < 0 ? 6 : week;
      if (appointmentTimes.length > week) {
        times = (appointmentTimes[week] || []).filter((t) => {
          if (isToday) {
            // 今天，过滤不可预约时间
            return canOrderTime(t, currentTime, nowDate);
          }
          return true;
        });
        finded = times.find((time) => time === activeTime);
        if (!finded && times.length) {
          finded = times[0];
        }
      }
    }
    setTimesx(times);
    setActiveTime(finded || '');
  };
  /**
   * 是否满足预约时间
   * range {String} 9:00~10:00
   * time {String} 10:00
   */
  const canOrderTime = (range, time, nowDate) => {
    const times = range.split('~');
    if (times[1] === '0:00' || times[1] === '00:00') {
      return true;
    }

    const start = new Date(`${nowDate} ${times[0]}:00`).getTime();
    const current = new Date(`${nowDate} ${time}:00`).getTime();
    return start > current;
  };
  const onChangeDate = (e) => {
    setActiveDate(e.currentTarget.dataset.item);
    setTimes(e.currentTarget.dataset.item);
    triggerDataChanged();
  };
  const onChangeTime = (e) => {
    setActiveTime(e.currentTarget.dataset.item);
    triggerDataChanged();
  };

  const triggerDataChanged = () => {
    // FIXME: 怎么没触发?
    // triggerEvent('change', {
    //   activeDate: activeDate,
    //   activeTime: activeTime
    // });
  };

  return (
    <Block>
      <View className='scroll-container'>
        <ScrollView className='scroll-view_H' scrollX style='width: 100%'>
          {dates.map((item, index) => {
            return (
              <View
                className={'appointment-date ' + (item.display === activeDate.display ? 'active-date' : '')}
                key={'display' + index}
                data-item={item}
                onClick={onChangeDate}
                style={`background:${item.display === activeDate.display ? tmpStyle.btnColor : ''};`}
              >
                <Text className='date'>{item.display}</Text>
                <Text className='week'>{item.week}</Text>
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View className='time-container'>
        {times.map((item, index) => {
          return (
            <View
              className={'appointment-time ' + (item === activeTime ? 'active-time' : '')}
              key={'time-container' + index}
              data-item={item}
              onClick={onChangeTime}
              style={`background:${
                item === activeTime ? tmpStyle.btnColor + ';border-color:' + tmpStyle.btnColor : ''
              };`}
            >
              {item}
            </View>
          );
        })}
        {times.length === 0 && <View className='time-tip'>休息...</View>}
      </View>
    </Block>
  );
};

// 给props赋默认值
AppointmentTimeSelect.defaultProps = {
  appointmentTimes: [],
  limitDay: 15,
};

export default AppointmentTimeSelect;
