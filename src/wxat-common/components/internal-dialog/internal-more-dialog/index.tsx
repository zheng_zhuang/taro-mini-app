import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type'; // @externalClassesConvered(AnimatDialog)
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc';
import { connect } from 'react-redux';

import filters from '@/wxat-common/utils/money.wxs.js';
import template from '@/wxat-common/utils/template.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import distributionEnum from '@/wxat-common/constants/distributionEnum.js';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';

import s from './index.module.scss';

const internalImg = cdnResConfig.internal;
const mapStateToProps = (state) => ({
  allState: state,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class InternalMoreDialog extends React.Component {
  static defaultProps = {
    appLogoPath: '',
    height: 0,
  };

  state = {
    cdnResConfig,
    tmpStyle: {},
    pendingChooseStoreList: null,
    user: 'share',
    systemTime: null,
    isGain: false,
    giveNum: 1,
  };

  private internalMoreDialogRef = React.createRef<AnimatDialog>();

  constructor(props) {
    super(props);

    this.getTemplateStyle();
    this.getSystemGiveTime();
  }

  handleTryShareToFriend = () => {};

  //显示海报
  handleShowPosterDialog = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    this.hide();
    // this.triggerEvent('create')
    this.props.onCreate();
  };

  //检验系统发放
  checkSystem() {
    return wxApi
      .request({
        url: api.internalVoucher.gainAvailableVoucherNum,
        data: {
          type: distributionEnum.GAINTYPE.SYSTEM_GIVE,
        },
      })
      .then((res) => {
        this.setState({
          giveNum: res.data || 0,
        });
      })
      .catch((error) => {});
  }

  //获取平台赠送的开始时间和结束时间
  getSystemGiveTime() {
    const params = {};
    return wxApi
      .request({
        url: api.internalVoucher.getSystemGiveTime,
        data: params,
      })
      .then((res) => {
        if (res.data) {
          const systemTime = res.data;
          console.log('合理安徽省大赛的挥洒', systemTime);
          let isGain = false;
          const time = new Date().getTime();
          if (systemTime.systemGiveTimeS < time && time < systemTime.systemGiveTimeE) {
            isGain = true;
            if (this.isUserRegistered()) {
              this.checkSystem();
            }
          } else {
            isGain = false;
          }
          this.setState({
            isGain: isGain,
            systemTime: res.data,
          });
        }
      })
      .catch((error) => {});
  }

  //用户是否注册过
  isUserRegistered() {
    return !!this.props.allState.base.registered;
  }

  //领取
  handleGet = () => {
    if (!this.state.isGain) {
      return wxApi.showToast({
        title: '未到领取时间',
        icon: 'none',
      });
    }
    const params = {
      type: distributionEnum.GAINTYPE.SYSTEM_GIVE,
    };

    return wxApi
      .request({
        url: api.internalVoucher.gainVoucher,
        data: params,
      })
      .then((res) => {
        if (res.data) {
          this.hide();
          this.setState({ giveNum: 0 });
          // FIXME: 传递数量
          // this.triggerEvent('gain', res.data)
          this.props.onGain();
        }
      })
      .catch((error) => {});
  };

  show = () => {
    // eslint-disable-next-line
    this.internalMoreDialogRef.current!.show({
      scale: 1,
    });
  };

  hide = () => {
    // eslint-disable-next-line
    this.internalMoreDialogRef.current!.hide();
  };

  handleMore = () => {
    this.hide();
    wxApi.$navigateTo({
      url: '/sub-packages/distribution-package/pages/internal-purchase/record/index',
      data: {},
    });
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { systemTime, isGain, giveNum, user } = this.state;
    const { height, appLogoPath } = this.props;
    return (
      <AnimatDialog
        clickMaskHide
        ref={this.internalMoreDialogRef}
        animClass={s['internal-more-dialog']}
        modalClass={s['choose-store-modal']}
        customClass={height ? s.bottom : ''}
      >
        <View className={s.title}>获取更多内购券，抢超值内购商品</View>
        <View className={s['item-box']}>
          <View className={s['item-info']}>
            <View className={s['logo']}>
              <Image className={s['logo__image']} src={appLogoPath}></Image>
            </View>
            <View className={s['info']}>
              <View className={s['info__text']}>领取专属内购券</View>
              {!systemTime ? (
                <View className={s['info__desc']}>未开启系统派送</View>
              ) : isGain ? (
                <View className={s['info__desc']}>
                  {filters.dateFormat(systemTime.systemGiveTimeE, 'MM-dd hh:mm ') + '结束发放'}
                </View>
              ) : (
                <View className={s['info__desc']}>
                  {systemTime &&
                    systemTime.systemGiveTimeS &&
                    filters.dateFormat(systemTime.systemGiveTimeS, 'MM-dd hh:mm ') + '限时发放'}
                </View>
              )}
            </View>
          </View>
          <View onClick={this.handleGet} className={`${s.btn} ${systemTime && isGain && giveNum ? '' : s.opacity}`}>
            立即领取
          </View>
        </View>
        <View className={s['item-box']}>
          <View className={s['item-info']}>
            <Image className={s['img']} src={internalImg.wechat}></Image>
            <View className={s['info']}>
              <View className={s['info__text']}>邀请好友助力</View>
              <View className={s['info__desc']}>好友助力后各得内购券</View>
            </View>
          </View>
          <ButtonWithOpenType
            buttonId='power'
            className={`${s.btn} ${s.green}`}
            openType='share'
            onClick={_fixme_with_dataset_(this.handleTryShareToFriend, { user: user })}
          >
            立即邀请
          </ButtonWithOpenType>
        </View>
        {process.env.TARO_ENV === 'weapp' && (
          <View className={s['item-box']}>
            <View className={s['item-info']}>
              <Image className={s['img']} src={internalImg.poster}></Image>
              <View className={s['info']}>
                <View className={s['info__text']}>助力海报</View>
                <View className={s['info__desc']}>分享朋友圈邀请好友助力</View>
              </View>
            </View>
            <View className={`${s.btn} ${s.red}`} onClick={this.handleShowPosterDialog}>
              立即生成
            </View>
          </View>
        )}

        <View className={s['check-more']} onClick={this.handleMore}>
          查看内购券记录
        </View>
      </AnimatDialog>
    );
  }
}

export default InternalMoreDialog;
