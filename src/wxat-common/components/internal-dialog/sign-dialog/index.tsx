import React, { useRef, FC, useMemo, useEffect } from 'react'; // @externalClassesConvered(AnimatDialog)
import '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import cdnResConfig from '../../../constants/cdnResConfig.js';

import AnimatDialog from '../../animat-dialog/index';

import './index.scss';

export interface SignDialogProps {
  visible: boolean;
  title: string;
  num: number;
  time: number | string;
  onHide?: () => void;
  onConfirmed?: () => void;
}

let SignDialog: FC<SignDialogProps> = (props) => {
  const { title, num, time, visible } = props;
  const animatDialogRef = useRef<AnimatDialog>(null);

  const { hour, minute, second } = useMemo(() => {
    // @ts-expect-error
    const _time = parseInt(time) / 1000;
    const hourInt = Math.floor(_time / 3600);
    const hour = hourInt < 10 ? '0' + hourInt : hourInt;

    const minuteS = Math.floor(_time % 3600);
    const minuteInt = Math.floor(minuteS / 60);
    const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

    const secondS = Math.floor(minuteS % 60);
    const second = secondS < 10 ? '0' + secondS : secondS;
    return {
      hour: hour,
      minute: minute,
      second: second,
    };
  }, [time]);

  const show = () => {
    animatDialogRef.current &&
      animatDialogRef.current.show({
        scale: 1,
      });
  };

  const hide = () => {
    animatDialogRef.current && animatDialogRef.current.hide(true);
    props.onHide && props.onHide();
  };

  const onCancelClick = () => {
    hide();
    props.onConfirmed && props.onConfirmed();
  };

  useEffect(() => {
    if (visible) {
      show();
    } else {
      hide();
    }
  }, [visible]);

  return (
    <View
      data-fixme='03 add view wrapper. need more test'
      data-scoped='wk-cis-SignDialog'
      className='wk-cis-SignDialog'
    >
      <AnimatDialog clickMaskHide ref={animatDialogRef} animClass='sign-dialog' modalClass='choose-store-modal'>
        <View className='sign-title'>{title}</View>
        <View className='sub-title'>{'送你 ' + num + ' 张 [内购券]'}</View>
        <View className='img-box'>
          <Image className='img-box__image' src={cdnResConfig.internalBuy.couponBg}></Image>
          <View className='sign-dialog__container'>
            <View className='sign-dialog__content'>
              <View>员工亲友内购券</View>
              <View className='desc'>时效有限，请尽快使用</View>
            </View>
            <View className='count'>
              {num}
              <Text className='unit'>张</Text>
            </View>
          </View>
          <View className='time'>
            <View className='box'>{hour}</View>
            <View className='point'>:</View>
            <View className='box'>{minute}</View>
            <View className='point'>:</View>
            <View className='box'>{second}</View>
            <View className='text'>后过期</View>
          </View>
        </View>
        <View className='btn' onClick={onCancelClick}>
          我知道了
        </View>
        <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png" className='cancel-icon' onClick={onCancelClick}></Image>
      </AnimatDialog>
    </View>
  );
};

SignDialog.defaultProps = {
  title: '欢迎回到员工亲友内购专场',
  num: 1,
  time: 0,
};

export default SignDialog;
