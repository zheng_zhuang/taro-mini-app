import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type'; // @externalClassesConvered(AnimatDialog)
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';

import filters from '../../../utils/money.wxs.js';
import template from '../../../utils/template.js';
import wxApi from '../../../utils/wxApi';
import cdnResConfig from '../../../constants/cdnResConfig.js';

import './index.scss';

import AnimatDialog from '@/wxat-common/components/animat-dialog';

class CheckCoupon extends React.Component {
  static defaultProps = {
    title: {
      type: String,
      value: '内购券不足',
    },

    num: {
      type: Number,
      value: 1,
    },

    type: {
      type: Number,
      value: 1,
    },

    goods: {
      type: Object,
      value: {},
    },
  };

  state = {
    cdnResConfig,
    tmpStyle: {},
    pendingChooseStoreList: null,
  };

  private checkVouponDialogCMPT: any;
  refCheckVouponDialogCMPT = (node) => (this.checkVouponDialogCMPT = node);
  attached() {
    this.getTemplateStyle();
  }
  show() {
    // this.selectComponent('#check-coupon-dialog').show({
    //                                                     scale: 1
    //                                                   })
    this.checkVouponDialogCMPT.show({
      scale: 1,
    });
  }

  hide() {
    // this.selectComponent('#check-coupon-dialog').hide(true)
    this.checkVouponDialogCMPT.hide();
  }

  onCancelClick = () => {
    this.hide();
  };

  handleOriginal = () => {
    this.hide();
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: this.props.goods.itemNo,
      },
    });
  };

  handleShare() {
    this.hide();
    // this.triggerEvent('create')
    this.props.onCreate();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { title, goods, type } = this.props;
    if (goods == null) {
      return null;
    }

    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-cic-CheckCouponDialog'
        className='wk-cic-CheckCouponDialog'
      >
        <AnimatDialog
          id='check-coupon-dialog'
          clickMaskHide={true}
          ref={this.refCheckVouponDialogCMPT}
          animClass='check-coupon-dialog'
          modalClass='choose-store-modal'
        >
          <View className='sign-title'>{title.value}</View>
          <View className='sub-title'>邀请好友助力，好友助力后各得内购券</View>
          <View className='img-box'>
            <Image className='img-box__image' src={goods.thumbnail}></Image>
          </View>
          <View className='price'>
            内购价：
            <Text>{goods && goods.innerBuyPrice && filters.moneyFilter(goods.innerBuyPrice, true) + '元'}</Text>
          </View>
          <ButtonWithOpenType
            className='btn'
            openType='share'
            onClick={_fixme_with_dataset_(this.onCancelClick, { user: 'share' })}
          >
            邀请好友助力
          </ButtonWithOpenType>
          {!!(type && process.env.TARO_ENV === 'weapp') && (
            <View className='btn-org' onClick={this.handleShare.bind(this)}>
              生成助力海报
            </View>
          )}

          <View className='text' onClick={this.handleOriginal}>
            原价购买
          </View>
          <Image
            src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-del.png"
            className='cancel-icon'
            onClick={this.onCancelClick}
          ></Image>
        </AnimatDialog>
      </View>
    );
  }
}

export default CheckCoupon;
