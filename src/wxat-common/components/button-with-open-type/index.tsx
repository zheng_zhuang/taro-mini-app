import React, { FC } from 'react';
import Taro from '@tarojs/taro';
import { Button } from '@tarojs/components';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { ButtonProps } from '@tarojs/components/types/Button';
import { ITouchEvent } from '@tarojs/components/types/common';
import wxApi from '@/wxat-common/utils/wxApi';
import './index.scss';

type SupportedProps = 'openType' | 'onGetUserInfo';
export interface ButtonWithOpenTypeProps extends Pick<ButtonProps, SupportedProps> {
  className?: string;
  style?: React.CSSProperties;
  onClick?: (event: ITouchEvent) => any;
  buttonId?: string;
  item?: any;
  type?:string;
}
// 用于拦截处理按钮业务
let ButtonWithOpenType: FC<ButtonWithOpenTypeProps> = (props) => {
  const { openType, onGetUserInfo, className, style, onClick, buttonId, item ,type} = props;

  const handleClick = (e: ITouchEvent) => {
    console.log("123",e,type)
    e.type = type
    e.stopPropagation();
    onClick && onClick(e);

    // 公众号兼容
    if (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true') {
      // 分享给朋友
      if (openType === 'share') {
        wxApi.__shareAppMessage__(e);
      }
    }
  };

  const handleUserProfile = () => {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        onGetUserInfo && onGetUserInfo(res);
      },
    });
  };

  // FIXME: Taro 小程序端原生组件暂时不支持 spread props, 所以必须一个一个显式传入
  return openType === 'getUserInfo' ? (
    <Button
      className={className}
      style={_safe_style_(style)}
      data-item={item}
      onClick={handleUserProfile}
      id={buttonId}
    >
      {props.children}
    </Button>
  ) : (
    <Button
      openType={openType}
      onGetUserInfo={onGetUserInfo}
      className={className}
      style={_safe_style_(style)}
      data-item={item}
      onClick={handleClick}
      id={buttonId}
    >
      {props.children}
    </Button>
  );
};

export default ButtonWithOpenType;
