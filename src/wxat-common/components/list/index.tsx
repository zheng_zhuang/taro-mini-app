import {_fixme_with_dataset_} from '@/wk-taro-platform';
import {View, Image, Text} from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../utils/money.wxs.js';
import './index.scss';

export interface ListProps {
  list: [],
  name: '',
  onToDetail: (value: string) => void
}

class List extends React.Component<ListProps> {

  state = {}

  showDetail(e) {
    const isOpen = e.target.dataset.item.isOpen;
    const index = e.target.dataset.index;
    console.log(index);
    const current = `list[${index}].isOpen`;
    this.setState({
      [current]: !isOpen,
    });
  }

  toDetail = (e) => {
    let obj = e.currentTarget.dataset.item;
    this.props.onToDetail(obj.orderNo)
  }

  render() {
    const {list} = this.props;
    return (
      <View
        data-scoped="wk-swcl-List"
        className="wk-swcl-List wrap"
      >
        {list ?
          list.map((item: any, index) => {
            return (
              <View
                className="list-wrap"
                key={item.index}
                onClick={_fixme_with_dataset_(this.toDetail, {item: item})}>
                <View className="item-top">
                  <Image src={filters.imgListFormat(item.img4)}></Image>
                  <View className="item-detail-right">
                    <Text>{item.serverName4 || item.reservationName}</Text>
                    <Text>{'规格：' + (item.spec || item.specUnit)}</Text>
                    <Text>{'时间：' + filters.dateFormat(item.reservationTimeStart * 1000, 'yyyy-MM-dd hh:mm')} - {filters.dateFormat(item.reservationTimeEnd * 1000, 'yyyy-MM-dd hh:mm')}</Text>
                    <View className="money">
                      <Text>
                        {'￥' +
                          (item.totalPrice
                            ? item.totalPrice / 100
                            : item.minSalePrice == item.maxSalePrice
                              ? filters.moneyFilter(item.minSalePrice || 0, true)
                              : filters.moneyFilter(item.minSalePrice || 0, true) +
                              '-' +
                              filters.moneyFilter(item.maxSalePrice || 0, true))}
                      </Text>
                      {!item.isOpen ? (
                        <Image
                          onClick={_fixme_with_dataset_(this.toDetail, {item: item})}
                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                        ></Image>
                      ) : (
                        <Image
                          onClick={_fixme_with_dataset_(this.toDetail, {item: item})}
                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/top-arrow.png"
                        ></Image>
                      )}
                    </View>
                  </View>
                </View>
                {item.formExt4 ? (
                  <View
                    className={
                      'item-bottom ' +
                      (item.isOpen === true ? 'open' : '') +
                      ' ' +
                      (item.isOpen === false ? 'close' : '')
                    }
                  >
                    {item.parseFormExt4 ?
                      item.parseFormExt4.map(el => {
                        return <Text key={el.label}>{`${el.label}：${el.value}`}</Text>;
                      }) : ''}
                  </View>
                ) : (
                  <View
                    className={
                      'item-bottom ' +
                      (item.isOpen === true ? 'open' : '') +
                      ' ' +
                      (item.isOpen === false ? 'close' : '')
                    }
                  >
                    {item.formExtParse ?
                      item.formExtParse.map(ele => {
                        return <Text key={ele.item.label}>{`${ele.label}：${ele.value}`}</Text>;
                      }) : ''}
                  </View>
                )}
              </View>
            );
          }) : ''}
      </View>
    );
  }
}

export default List;
