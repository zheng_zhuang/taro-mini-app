import React, { useEffect, useState, FC } from 'react';
import { Block, View, RadioGroup, Radio } from '@tarojs/components';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import filters from '../../utils/money.wxs.js';
import constants from '../../constants/index.js';
import template from '../../utils/template.js';
import './index.scss';

// props的类型定义
type ComponentProps = {
  visible: boolean;
};

const RefundNow: FC<ComponentProps> = ({ visible }) => {
  const [tmpStyle, setTmpStyle] = useState('');
  const [refundMessage, setRefundMessage] = useState(1);
  const [visibleStatus, setVisibleStatus] = useState(() => visible);

  useEffect(() => {
    getTemplateStyle();
  }, []);

  //获取模板配置
  const getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    setTmpStyle(templateStyle);
  };

  const handlePayGradeChange = (e) => setRefundMessage(e.detail.value);

  const handlerSure = () => {
    let RefundMessageText = '';
    if (refundMessage === 1) {
      RefundMessageText = '我不想要了';
    } else {
      RefundMessageText = '已与商家达成一致，取消订单。';
    }

    setVisibleStatus(false);
    // triggerEvent('Refund-event', { refundMessage: RefundMessageText });
  };
  const hide = () => setVisibleStatus(false);

  return (
    <Block>
      {/* components/goods-attribute/index.wxml */}
      {visibleStatus && (
        <View>
          <View className='wx-mask' style={'opacity:' + backOpacity + ';'} onClick={hide}></View>
          <View className='pop-goods-info'>
            <View className='pop-goods-close' onClick={hide}></View>
            <View className='radio-item'>
              <RadioGroup className='radio-group' onChange={handlePayGradeChange}>
                <View>
                  <Radio color={tmpStyle.btnColor} value='1' checked={refundMessage == 1}>
                    我不想要了
                  </Radio>
                </View>
                <View>
                  <Radio color={tmpStyle.btnColor} style='margin-left:-45rpx;' value='2' checked={refundMessage == 2}>
                    已与商家达成一致，取消订单。
                  </Radio>
                </View>
              </RadioGroup>
            </View>
            <View className='popup-join-btn' onClick={handlerSure} style={'background:' + tmpStyle.btnColor}>
              提交并取消
            </View>
          </View>
        </View>
      )}
    </Block>
  );
};

// 给props赋默认值
RefundNow.defaultProps = {
  visible: false,
};

export default RefundNow;
