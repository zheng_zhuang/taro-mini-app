// @scoped
import { View } from '@tarojs/components';
import Taro, { Config } from '@tarojs/taro';
import React, { ComponentClass, Component } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';
import Tabbar from '@/wxat-common/components/custom-tabbar/tabbar';
import PickStore from '@/wxat-common/components/pick-store/index';
import checkOptions from '@/wxat-common/utils/check-options';
import './index.scss';

// type PageStateProps = {}

// type PageDispatchProps = {}

type PageOwnProps = {};

type PageState = {};

interface NearbyStore {
  props: PageOwnProps;
}

// const mapStateToProps = (state) => ({
// });

// const mapDispatchToProps = dispatch => ();

// @connect(mapStateToProps, mapDispatchToProps)

class NearbyStore extends Component {
  componentDidMount() {
    const params =
      wxAppProxy.getNavColor('pages/nearby-store/index') ||
      wxAppProxy.getNavColor('wxat-common/pages/nearby-store/index');
    if (params) {
      wxApi.setNavigationBarColor({
        frontColor: params.navFrontColor || '#000000',
        backgroundColor: params.navBackgroundColor || '#ffffff',
      });
    }

    this.initRender();
  }

  RefCMPT = React.createRef();

  componentDidShow() {}

  componentDidHide() {}

  onPickStore = (info) => {
    checkOptions.changeStore(info.id).then((res) => {
      wxApi.switchTab({
        url: '/wxat-common/pages/home/index',
      });
    });
  };

  initRender = () => {
    const refs = this.RefCMPT.current;
    if (refs) {
      refs.initRender();
    }
  };

  onPullDownRefresh() {
    const refs = this.RefCMPT.current;
    if (refs) {
      refs.refreshList();
    }
  }

  onReachBottom() {
    const refs = this.RefCMPT.current;
    if (refs) {
      refs.loadMore();
    }
  }

  render() {
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-swpn-NearbyStore'
        className='wk-swpn-NearbyStore'
      >
        <Tabbar></Tabbar>
        <PickStore ref={this.RefCMPT} onPickStore={this.onPickStore}></PickStore>
      </View>
    );
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default NearbyStore as ComponentClass<PageOwnProps, PageState>;
