import React, { FC } from 'react';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import screen from '@/wxat-common/utils/screen';

export interface SafeAreaPlaceholderProps {}

let SafeAreaPlaceholder: FC<SafeAreaPlaceholderProps> = (props) => {
  return <View style={{ height: Taro.pxTransform(screen.safeAreaBottom) }}></View>;
};

export default SafeAreaPlaceholder;
