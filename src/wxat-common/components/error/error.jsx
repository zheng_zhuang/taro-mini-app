import React from 'react';
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './error.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const commonImg = cdnResConfig.common;

class Error extends React.Component {
  static defautProps = {
    message: '发生了一些错误',
    isDialog: false,
    //点击重试回调
    onRetry: null,
  };

  onErrorClick = () => {
    const onRetry = this.props.onRetry;
    onRetry && onRetry();
  };

  render() {
    const { isDialog, message } = this.props;
    return (
      <View
        data-scoped='wk-wce-Error'
        className={'wk-wce-Error ' + ('com-error ' + (isDialog != null && isDialog ? 'dialog' : ''))}
        onClick={this.onErrorClick}
      >
        <Image className='bg' src={commonImg.error}></Image>
        <View className='message'>{message}</View>
      </View>
    );
  }
}

export default Error;
