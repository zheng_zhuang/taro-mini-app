import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import wxApi from '../../utils/wxApi';
import template from '../../utils/template';
import authHelper from '../../utils/auth-helper';
import report from '../../../sdks/buried/report/index';
import './index.scss';

import { connect } from 'react-redux';
import screen from '@/wxat-common/utils/screen';

const mapStateToProps = (state) => ({
  tabbars: state.globalData.tabbars,
  environment: state.globalData.environment,
  currentStore: state.base.currentStore,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class BuyNow extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    // 分享按钮
    showShare: true,
    // 微信的分享功能
    immediateShare: true,
    // 客服
    showService: false,
    // 购物车
    showCart: true,
    // 直接购买
    showBuyDirect: false,
    // 直接购买按钮文字
    buyDirectText: '原价购买',
    // 我要咨询
    showContact: false,
    // 咨询按钮文字
    contactText: '我要咨询',
    // 是否禁止点击
    disable: false,
    // 是否渐变背景
    isGradualChange: false,
    operationText: '立即购买',
    defaultText: '立即购买',

    //立即购买回调
    onBuyNow: null,
    // 预订清单回调
    onBookingList: null,
    //加入购物车回调
    onShoppingCart: null,
    //直接购买回调
    onBuyDirect: null,
    //咨询回调
    onContact: null,
    //分享回调
    onShare: null,

    // 支持线上销售 0，线下销售 1，无库存售卖 2
    salesType: 0,
    //是否是配送服务
    isDistributionServices: false
  };

  /**
   * 组件的初始数据
   */
  state = {
    tmpStyle: {}, // 主题模板配置
  };

  componentDidMount() {
    this.getTemplateStyle();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  handleClickPhone = () => {
    report.clickStorePhone();
    wxApi.makePhoneCall({
      phoneNumber: this.props.currentStore.tel,
    });
  };

  /**
   * 立即购买
   */
  handleClickBuyNow = () => {
    const { disable, operationText } = this.props;
    if (!authHelper.checkAuth()) {
      return;
    }
    if (disable) {
      wxApi.showToast({
        icon: 'none',
        title: operationText,
      });
    } else {
      const onBuyNow = this.props.onBuyNow;
      onBuyNow && onBuyNow();
    }
  };

  /**
   * 加入购物车
   */
  handleClickShoppingCart = () => {
    const { disable, operationText } = this.props;
    if (!authHelper.checkAuth()) {
      return;
    }
    if (disable) {
      wxApi.showToast({
        icon: 'none',
        title: operationText,
      });
    } else {
      const onShoppingCart = this.props.onShoppingCart;
      onShoppingCart && onShoppingCart();
    }
  };
  /**
   * 加入预订清单
   */
  handleClickBookingList = () =>{
    const { disable, operationText } = this.props;
    if (!authHelper.checkAuth()) {
      return;
    }
    if (disable) {
      wxApi.showToast({
        icon: 'none',
        title: operationText,
      });
    } else {
      const onBookingList = this.props.onBookingList;
      onBookingList && onBookingList();
    }
  }
  /**
   * 直接购买
   */
  handleClickBuyDirect = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    const onBuyDirect = this.props.onBuyDirect;
    onBuyDirect && onBuyDirect();
  };

  /**
   * 我要咨询
   */
  handleContact = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    const onContact = this.props.onContact;
    onContact && onContact();
  };

  /**
   * 返回首页
   */
  handleClickHome = () => {
    const { tabbars } = this.props;
    let url = '/wxat-common/pages/home/index';
    if (tabbars && tabbars.list) {
      const tabbarList = tabbars.list;
      url = tabbarList[0].pagePath;
    }
    wxApi.$navigateTo({
      url: url,
    });
  };

  /**
   * 联系商家
   */
  clickPhone() {
    wxApi.makePhoneCall({
      phoneNumber: this.props.currentStore.tel,
    });
  }

  /**
   * 分享
   */
  handleShareClick = () => {
    const onShare = this.props.onShare;
    onShare && onShare();
  };

  render() {
    const {
      immediateShare,
      showShare,
      showService,
      showCart,
      showBuyDirect,
      buyDirectText,
      showContact,
      contactText,
      disable,
      disabled,
      isGradualChange,
      operationText,
      defaultText,
      salesType,
      environment,
      isDistributionServices
    } = this.props;
    const { tmpStyle } = this.state;
    const shouldShowContact = process.env.TARO_ENV === 'weapp' && showContact;
    return (
      <View data-scoped='wk-wcb-BuyNow' className='wk-wcb-BuyNow'>
        <View className='btn-placeholder' />
        {
        !isDistributionServices && (
        <View className={'buy-now-container ' + (screen.isFullScreenPhone ? 'fix-full-screen' : '')}>
          <View className='image-btn-box'>
            <View className='image-btn' onClick={this.handleClickHome}>
              <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-home.png"></Image>
              <Text className='image-btn-text'>首页</Text>
            </View>
            {!!showShare && (
              <View className='image-btn'>
                {immediateShare ? (
                  <ButtonWithOpenType openType='share' className='btn-share immediateShare'>
                    <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-share.png"></Image>
                    <Text className='btn-share-text'>分享</Text>
                  </ButtonWithOpenType>
                ) : (
                  <Button className='btn-share noImmdiateShare' onClick={this.handleShareClick}>
                    <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-share.png"></Image>
                    <Text className='btn-share-text'>分享</Text>
                  </Button>
                )}
              </View>
            )}

            {!!showService && (
              <View className='image-btn' onClick={this.handleClickPhone}>
                <Image className='btn-icon' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-service.png"></Image>
                <Text className='image-btn-text'>客服</Text>
              </View>
            )}
          </View>
          {environment === 'wxwork' ? (
            // 导购分享
            <View>
              <ButtonWithOpenType
                openType='share'
                className='btn wxwork-share'
                style={_safe_style_(
                  'background:' + (!disabled && isGradualChange ? tmpStyle.bgGradualChange : tmpStyle.btnColor) + ';'
                )}
              >
                分享
              </ButtonWithOpenType>
            </View>
          ) : salesType === 1 ? (
            // 到店咨询
            <View className='btn offline-btn'>到店咨询</View>
          ) : (
            // 加购
            <View className='btn-box'>
              {!!showCart && (
                <View
                  className='btn btn-other'
                  style={_safe_style_('background:' + tmpStyle.btnColor1 + ';')}
                  onClick={this.handleClickShoppingCart}
                >
                  <Text style={_safe_style_('color:' + tmpStyle.assistContentColor + ';')}>加入购物车</Text>
                </View>
              )}

              {!!showBuyDirect && (
                <View
                  className='btn btn-other'
                  style={_safe_style_('background:' + tmpStyle.bgColor1 + ';')}
                  onClick={this.handleClickBuyDirect}
                >
                  <Text  style={_safe_style_('color:' + tmpStyle.assistContentColor + ';')}>{buyDirectText}</Text>
                </View>
              )}

              {!!shouldShowContact && (
                <Button
                  className='btn btn-other'
                  openType='contact'
                  style={_safe_style_('background:' + tmpStyle.btnColor1 + ';')}
                  onClick={this.handleContact}
                >
                  <Text style={_safe_style_('color:' + tmpStyle.assistContentColor + ';')}>{contactText}</Text>
                </Button>
              )}

              <View
                className={
                  'btn ' +
                  (disable ? 'btn-disabled ' : '') +
                  ' ' +
                  (!disabled && isGradualChange ? 'btn-gradualChange ' : '')
                }
                style={_safe_style_(
                  'background:' +
                    (!disabled && isGradualChange ? tmpStyle.bgGradualChange : tmpStyle.btnColor) +
                    ';width: ' +
                    (showCart || showBuyDirect || shouldShowContact ? '220rpx' : '440rpx')
                )}
                onClick={this.handleClickBuyNow}
              >
                <Text style={_safe_style_('color:' + tmpStyle.contentColor + ';')}>{disable ? operationText : defaultText}</Text>
              </View>
            </View>
          )}
        </View>
          )
        }
        {isDistributionServices && (
          <View className="distributionBtn">
            <View
              className="btn btn-other"
              style={_safe_style_('background:' + tmpStyle.btnColor1 + ';')}
              onClick={this.handleClickBookingList}
            >
              <Text style={_safe_style_('color:' + tmpStyle.assistContentColor + ';')}>预订清单</Text>
            </View>
            <View
              className={
                'btn ' +
                (disable ? 'btn-disabled ' : '') +
                ' ' +
                (!disabled && isGradualChange ? 'btn-gradualChange ' : '')
              }
              style={_safe_style_(
                'background:' +
                  (!disabled && isGradualChange ? tmpStyle.bgGradualChange : tmpStyle.btnColor) +
                  ';width: ' +
                  (showCart || showBuyDirect || showContact ? '220rpx' : '440rpx')
              )}
              onClick={this.handleClickBuyNow}
            >
              <Text style={_safe_style_('color:' + tmpStyle.contentColor + ';')}>{disable ? operationText : '立即下单'}</Text>
            </View>
          </View>
        )}

      </View>
    );
  }
}

export default BuyNow;
