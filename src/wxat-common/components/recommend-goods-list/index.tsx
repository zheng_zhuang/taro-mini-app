import '@/wxat-common/utils/platform';
import React, { ComponentClass } from 'react';
import { View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../api/index.js';
import wxApi from '../../utils/wxApi';
import imageUtils from '../../utils/image.js';
import reportConstants from '../../../sdks/buried/report/report-constants.js';
import ProductModule from '../base/productModule/index';
import './index.scss';

type PageOwnProps = {
  label: string;
  itemNo: string;
  list: Array<Record<string, any>>;
};

type PageState = {
  goodsList: Array<Record<string, any>>;
};

interface RecommendGoodsList {
  props: PageOwnProps;
}

const reportSource = reportConstants.SOURCE_TYPE.recommend.key;

class RecommendGoodsList extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    label: '推荐列表',
    itemNo: '',
    list: [],
  };

  /**
   * 组件的初始数据
   */
  state = {
    goodsList: [],
  };

  componentDidMount() {
    if (this.props.list && this.props.list.length) {
      this.setState({
        goodsList: this.props.list,
      });
    } else {
      this.getRecommendGoodsList();
    }
  }

  getRecommendGoodsList = () => {
    const reqParam = {
      itemNo: this.props.itemNo,
      pageSize: 6,
    };

    wxApi
      .request({
        url: api.goods.detailSimilarityList,
        quite: true,
        loading: false,
        checkSession: true,
        data: reqParam,
      })
      .then((res) => {
        const goodsList = res.data.wxItemDetailList || [];
        goodsList.forEach((item) => {
          item.thumbnailOneRowTwo = imageUtils.thumbnailOneRowTwo(
            item.thumbnail || (item.wxItem ? item.wxItem.thumbnail : '')
          );
        });
        this.setState({ goodsList });
      })
      .catch((err) => {});
  };

  render() {
    const { label } = this.props;
    const { goodsList } = this.state;
    return (
      !!(goodsList && goodsList.length) && (
        <View data-scoped='wk-wcr-RecommendGoodsList' className='wk-wcr-RecommendGoodsList recommend-container'>
          <Text className='label'>{label}</Text>
          <ProductModule
            display='oneRowTwo'
            reportSource={reportSource}
            list={goodsList}
            backgroundColor='none'
          ></ProductModule>
        </View>
      )
    );
  }
}

export default RecommendGoodsList as ComponentClass<PageOwnProps, PageState>;
