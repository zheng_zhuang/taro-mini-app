import React from 'react';
import {Block, View, Image} from '@tarojs/components';
import {_safe_style_} from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import template from '../../utils/template';
import './index.scss';
import wxApi from '@/wxat-common/utils/wxApi';

import getStaticImgUrl from '../../constants/frontEndImgUrl'
import {connect} from 'react-redux';


interface ComponentProps {
  navOpacity: number;
  title: string | null;
  bgColor: any;
  isLeftSlot?: Boolean;
  isTitle?: Boolean;
  style?: any
}

interface ComponentState {
  statusBarHeight: number,
  navBarHeight: number,
  menuRight: number,
  menuBotton: number,
  menuHeight: number,
  rightWidth: number, // 胶南到右侧的距离
  pagesNumber: number,
  tmpStyle: any
}

const mapStateToProps = (state) => ({
  themeConfig: state.globalData.themeConfig && state.globalData.themeConfig.template,
});
const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, {forwardRef: true})


class CustomerHeader extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    navOpacity: 0,
    title: '',
    bgColor: '#fff',
    isLeftSlot: false,
    isTitle: false,
    style: null
  };

  state = {
    statusBarHeight: 0,
    navBarHeight: 0,
    menuRight: 0,
    menuButton: 0,
    menuHeight: 0,
    rightWidth: 0, // 胶南到右侧的距离
    pagesNumber: 0,
    tmpStyle: null,
    isWeapp: process.env.TARO_ENV === 'weapp'
  };

  componentDidMount() {
    this.getTemplateStyle();
    this.init();
    this.setState({
      pagesNumber: wxApi.getCurrentPages().length,
    });
  }

  // 获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  init = () => {
    // 获取系统信息
    const systemInfo = wxApi.getSystemInfoSync();
    // 胶囊按钮位置信息
    const menuButtonInfo = wxApi.getMenuButtonBoundingClientRect();
    const rightWidth = menuButtonInfo.width + 5;
    const statusBarHeight = systemInfo.statusBarHeight;
    // 导航栏高度 = 状态栏到胶囊的间距（胶囊距上距离-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
    const navBarHeight =
      (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
    const menuRight = systemInfo.screenWidth - menuButtonInfo.right;
    const menuButton = menuButtonInfo.top - systemInfo.statusBarHeight;
    const menuHeight = menuButtonInfo.height;

    this.setState({
      statusBarHeight,
      navBarHeight,
      menuRight,
      menuButton,
      menuHeight,
      rightWidth,
    });
  };

  toLink = () => {
    if (this.state.pagesNumber > 1) {
      wxApi.navigateBack();
    } else {
      wxApi.switchTab({
        url: '/wxat-common/pages/home/index',
      });
    }
  };

  render() {
    const {
      navBarHeight,
      statusBarHeight,
      rightWidth,
      pagesNumber,
      menuHeight,
      menuRight,
      menuButton,
      isWeapp
    } = this.state;
    const {navOpacity, bgColor, isLeftSlot, title, isTitle, style, themeConfig} = this.props;
    return (
      <View>
        {isWeapp && <View
          data-scoped='wk-swcc-CustomerHeader'
          className='wk-swcc-CustomerHeader custom-header'
          style={_safe_style_('height:' + navBarHeight + 'px;' + 'pointer-events:' + style?.pointerEvents)}
        >
          {/*style={_safe_style_('height:' + navBarHeight + 'px;opacity:' + navOpacity + ';background-color: ' + bgColor)}*/}
          <View
            className='custom-bar-background'
            style={_safe_style_('height:' + navBarHeight + 'px;opacity:' + navOpacity + ';background-color: ' + (themeConfig ? themeConfig.topBgColor : bgColor))}
          ></View>
          <View
            className='custom-content'
            style={_safe_style_(
              'height:' + navBarHeight + 'px;padding-top:' + statusBarHeight + 'px;padding-right: ' + rightWidth + 'px'
            )}
          >
            <View className='left-slot' style={_safe_style_('width:' + rightWidth + 'px;')}>
              {isLeftSlot ? (
                this.props.children
              ) : (
                <View className='default' onClick={this.toLink}>
                  {pagesNumber > 1 ? (
                    <Image className="image" src={getStaticImgUrl.wxatCommon.iconBackLink_svg}></Image>
                  ) : (
                    <Image className="image" src={getStaticImgUrl.wxatCommon.iconHomeLink_svg}></Image>
                  )}
                </View>
              )}
            </View>
            {!isTitle ? (
              <Block>
                <View
                  className='text'
                  style={_safe_style_(
                    'height:' +
                    menuHeight +
                    'px; min-height:' +
                    menuHeight +
                    'px; line-height:' +
                    menuHeight +
                    'px; left:' +
                    menuRight +
                    'px; bottom:' +
                    menuButton +
                    'px;opacity:' +
                    navOpacity +
                    ';color:' +
                    (themeConfig ? themeConfig.topContentColor : "")
                  )}
                >
                  {title}
                </View>
              </Block>
            ) : (
              <Block>
                <View
                  className='text'
                  style={_safe_style_(
                    'height:' +
                    menuHeight +
                    'px; min-height:' +
                    menuHeight +
                    'px; line-height:' +
                    menuHeight +
                    'px; left:' +
                    menuRight +
                    'px; bottom:' +
                    menuButton +
                    'px'
                  )}
                >
                  {title}
                </View>
              </Block>
            )}
          </View>
        </View>}
      </View>

    );
  }
}

export default CustomerHeader;
