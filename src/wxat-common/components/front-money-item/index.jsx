import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../utils/money.wxs';
import DeposiRuleDialog from "../deposi-rule-dialog";
import './index.scss';
import getStaticImgUrl from '../../constants/frontEndImgUrl';

class FrontMoneyItem extends React.Component {
  static defaultProps = {
    frontMoney: 0,
    leftRightMargin: 30,
    leftRightPadding: 0,
  };

  // 定金弹窗
  showDialog = () => {
    this.deposiRuleDialogCOMPT && this.deposiRuleDialogCOMPT.show();
  };

  refDeposiRuleDialogCOMPT = (node) => (this.deposiRuleDialogCOMPT = node);

  render() {
    const { leftRightMargin, leftRightPadding, frontMoney } = this.props;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcf-FrontMoneyItem'
        className='wk-wcf-FrontMoneyItem'
      >
        <View
          className='front-money-item'
          style={_safe_style_('margin: 0 ' + leftRightMargin / 2 + 'px; padding: 0 ' + leftRightPadding / 2 + 'px')}
          onClick={this.showDialog}
        >
          <View>{'定金 ￥' + filters.moneyFilter(frontMoney, true)}</View>
          <Image className='right-arrow' src={getStaticImgUrl.images.rightIcon_png}></Image>
        </View>
        <DeposiRuleDialog ref={this.refDeposiRuleDialogCOMPT}></DeposiRuleDialog>
      </View>
    );
  }
}

export default FrontMoneyItem;
