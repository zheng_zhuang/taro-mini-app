import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';
import React, { ComponentClass } from 'react';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl';
const SORT_TYPE = {
  DESC: 'desc',
  ASC: 'asc',
};

interface GoodsFilterProps {
  onFilterChange: Function;
  onViewChange?: Function;
  view?: string;
  showViewIcon?: boolean;
}

interface GoodsFilterState {}
type IProps = GoodsFilterProps & GoodsFilterState;
interface GoodsFilter {
  props: IProps;
}

// const IMG_OF_NORMAL = require('https://front-end-1302979015.file.myqcloud.com/images/c/images/sort/normal.png');
// const IMG_OF_DESC = require('https://front-end-1302979015.file.myqcloud.com/images/c/images/sort/desc.png');
// const IMG_OF_ASC = require('https://front-end-1302979015.file.myqcloud.com/images/c/images/sort/asc.png');
const IMG_OF_NORMAL = getStaticImgUrl.sort.sort_normal_png;
const IMG_OF_DESC = getStaticImgUrl.sort.sort_desc_png;
const IMG_OF_ASC = getStaticImgUrl.sort.sort_asc_png;

class GoodsFilter extends React.Component {
  static defaultProps = {
    onFilterChange: () => {},
    onViewChange: () => {},
    view: 'vertical',
    showViewIcon: false,
  };

  state = {
    sortArray: [
      {
        name: '有货',
        id: 0,
        icon: getStaticImgUrl.sort.sort_off,
        status: 0,
      },

      {
        name: '价格',
        id: 'salePrice',
        icon: getStaticImgUrl.sort.sort_normal_png,
        status: 0,
      },

      {
        name: '销量',
        id: 'salesVolume',
        icon: getStaticImgUrl.sort.sort_normal_png,
        status: 0,
      },
    ],

    sortField: null,
    sortType: null,
    inStock: null,
  };

  resetSortArray = (sortArray) => {
    sortArray[1].status = 0;
    sortArray[1].icon = getStaticImgUrl.sort.sort_normal_png;
    sortArray[2].status = 0;
    sortArray[2].icon = getStaticImgUrl.sort.sort_normal_png;
  };

  /**
   * 点击排序按钮
   */
  clickSortBtn = (e) => {
    const { sortArray } = this.state;
    const index = parseInt(e.currentTarget.dataset.index);
    const sourceSort = sortArray[index].status;
    let sortField = sortArray[index].id;
    let targetStatus;
    let inStock = null;
    let sortType = null;
    // 有货只有两种状态，
    if (index === 0) {
      if (sourceSort === 0) {
        inStock = 1;
        sortArray[index].status = inStock;
        sortArray[index].icon = getStaticImgUrl.sort.sort_on_png;
      } else {
        inStock = 0;
        sortArray[index].status = inStock;
        sortArray[index].icon = getStaticImgUrl.sort.sort_off_png;
      }
      this.setState(
        {
          inStock,
          sortArray,
        },

        () => {
          this.filterTrigger();
        }
      );
    } else {
      this.resetSortArray(sortArray);
      targetStatus = sourceSort === 2 ? 0 : sourceSort + 1;
      sortType = targetStatus === 1 ? SORT_TYPE.ASC : targetStatus === 2 ? SORT_TYPE.DESC : null;
      if (targetStatus === 0) {
        sortField = null;
        sortType = null;
      }
      sortArray[index].status = targetStatus;
      sortArray[index].icon =
        sortType === SORT_TYPE.ASC ? IMG_OF_ASC : sortType === SORT_TYPE.DESC ? IMG_OF_DESC : IMG_OF_NORMAL;
      this.setState({ sortField, sortType, sortArray }, () => {
        this.filterTrigger();
      });
    }
  };

  // 条件改变 触发查询
  filterTrigger = () => {
    const { onFilterChange } = this.props;
    const { inStock, sortField, sortType } = this.state;
    onFilterChange &&
      onFilterChange({
        inStock,
        sortField,
        sortType,
      });
  };

  resetSortArrayOnCategoryIdChange = () => {
    this.filterTrigger();
  };

  handleChangeView = () => {
    const view = this.props.view === 'vertical' ? 'oneRowTwo' : 'vertical';
    this.props.onViewChange && this.props.onViewChange({ view });
  };

  render() {
    const { sortArray } = this.state;
    const { view, showViewIcon } = this.props;

    const VIEW_LOOKUP = {
      vertical: cdnResConfig.search.viewList,
      oneRowTwo: cdnResConfig.search.viewPic,
    };

    const $viewIcon = showViewIcon ? (
      <View className='view-icon' onClick={this.handleChangeView}>
        <Image className='icon' src={VIEW_LOOKUP[view || 'vertical']} />
      </View>
    ) : null;

    return (
      <View data-scoped='wk-wcg-GoodsFilter' className='wk-wcg-GoodsFilter goods-filter-container'>
        <View className='filter'>
          {sortArray.map((item, index) => {
            return (
              <View
                className='filter-view'
                key={index}
                onClick={_fixme_with_dataset_(this.clickSortBtn, { index: index })}
              >
                <View className={item.status > 0 ? 'sort-title-on' : 'sort-title'}>{item.name}</View>
                <Image className='sort-image' src={item.icon}></Image>
              </View>
            );
          })}
          {$viewIcon}
        </View>
        <View className='line'></View>
      </View>
    );
  }
}

export default GoodsFilter as ComponentClass<GoodsFilterProps, GoodsFilterState>;
