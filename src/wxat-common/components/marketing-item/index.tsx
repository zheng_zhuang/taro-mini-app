import React, { FC, useState, useEffect } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';
import filters from '@/wxat-common/utils/money.wxs';
import template from '@/wxat-common/utils/template';
// import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import './index.scss';

// const decorateImg = cdnResConfig.decorate;

const SHOW_TYPE = {
  vertical: 1, // 经典列表
  horizon: 4, // 水平滑动
  rowOne: 0, // 一行一个
  rowTwo: 2, // 一行两个
  rowThree: 3, // 一行三个
};

// props的类型定义
interface ComponentProps {
  PADDING?: number;
  marketingItem: Record<string, any>;
  showType: number;
  customContent?: boolean;
  onClick?: (object) => any;
  renderOneRowTwoContent?: any;
  renderOneRowThreeContent?: any;
  renderHorizonContent?: any;
}

const MarketingMoudle: FC<ComponentProps> = (props) => {
  const { marketingItem, showType, customContent, onClick } = props;
  const [tmpStyle, setTmpStyle] = useState({});
  const iosSetting = useSelector((state) => state.globalData && state.globalData.iosSetting);
  const isIOS = useSelector((state) => state.globalData && state.globalData.isIOS);
  useEffect(() => {
    getTemplateStyle();
  }, []);

  const go = (e) => {
    // const myEventDetail = {
    //   item: e.detail.item
    // };
    // this.triggerEvent('click', myEventDetail);
    // onClick && onClick(marketingItem);
    onClick?.(marketingItem);
  };

  // 获取模板配置
  const getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    setTmpStyle(templateStyle);
  };
  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-wcm-MarketingItem'
      className='wk-wcm-MarketingItem'
    >
      {/*  垂直列表  */}
      {showType == SHOW_TYPE.vertical ? (
        <View className='vertical-container' onClick={_fixme_with_dataset_(go, { item: marketingItem })}>
          <View
            className='v-marketing-img'
            style={{
              backgroundImage: `url(${marketingItem.thumbnail})`,
            }}
          ></View>
          <View className='v-marketing-content'>
            <View className='v-marketing-title'>
              <View className='limit-line line-2'>
                {!!marketingItem.collageLabel && (
                  <Text
                    className='v-marketing-sign'
                    style={{
                      backgroundColor: tmpStyle.bgColor,
                      color: tmpStyle.btnColor,
                      borderColor: tmpStyle.btnColor,
                    }}
                  >
                    {marketingItem.collageLabel}
                  </Text>
                )}

                {marketingItem.name}
              </View>
              {!!(marketingItem.labelList && !!marketingItem.labelList.length) && (
                <View className='label-box'>
                  {marketingItem.labelList.map((label, index) => {
                    return (
                      <View
                        className='label-item'
                        key={index}
                        style={{
                          color: tmpStyle.btnColor,
                        }}
                      >
                        {label.name}
                      </View>
                    );
                  })}
                </View>
              )}
            </View>
            <View className='v-activity-info'>
              <View className='v-info-left'>
                <View className='v-price-box'>
                  {!!marketingItem.salePrice && (
                    <Text
                      className='v-real-sale common-money'
                      style={{
                        color: tmpStyle.btnColor,
                      }}
                    >
                      {filters.moneyFilter(marketingItem.salePrice, true)}
                    </Text>
                  )}

                  {!!marketingItem.labelPrice && (
                    <Text className='v-origin-sale common-money'>
                      {filters.moneyFilter(marketingItem.labelPrice, true)}
                    </Text>
                  )}
                </View>
                {!!marketingItem.saleAmount && (
                  <View className='v-sale-amount'>
                    <Text>{marketingItem.saleAmount}</Text>
                  </View>
                )}
              </View>
              <View className='v-info-right'>
                {
                  isIOS && iosSetting?.payType && marketingItem.type == 41 ? <View className="m-btn" style={_safe_style_('background:' + (marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor))}>{iosSetting?.buttonCopywriting}</View> :
                  <View
                  className='m-btn'
                  style={{
                    background: marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor,
                  }}
                >
                  {marketingItem.btn}
                </View>
                }

              </View>
            </View>
          </View>
        </View>
      ) : showType == SHOW_TYPE.horizon ? (
        <View onClick={_fixme_with_dataset_(go, { item: marketingItem })} className='horizon-container'>
          <View
            className='h-marketing-img'
            style={{
              backgroundImage: `url(${marketingItem.thumbnail})`,
            }}
          ></View>
          {!!marketingItem.topSign && (
            <View className='haggle-icon'>
              <Image
                className='haggle-img'
                src='https://front-end-1302979015.file.myqcloud.com/images/c/images/decorate/haggle-icon.png'
              ></Image>
              <Text className='haggle-sign'>{marketingItem.topSign}</Text>
            </View>
          )}

          <View className='h-marketing-content'>
            <View className='h-marketing-title limit-line'>{marketingItem.name}</View>
            {customContent ? (
              <Block>{props.renderHorizonContent}</Block>
            ) : (
              <Block>
                <View className='h-activity-info'>
                  <View className='h-price-box'>
                    {!!marketingItem.sign && <View className='h-sign'>{marketingItem.sign}</View>}
                    <Text
                      className='h-real-sale common-money'
                      style={{
                        color: tmpStyle.btnColor,
                      }}
                    >
                      {filters.moneyFilter(marketingItem.salePrice, true)}
                    </Text>
                  </View>
                </View>
              </Block>
            )}
          </View>
        </View>
      ) : showType == SHOW_TYPE.rowOne ? (
        <View
          className='n-marketing-item-container oneRowOne'
          onClick={_fixme_with_dataset_(go, { item: marketingItem })}
        >
          <View
            className='n-marketing-img'
            style={{
              backgroundImage: `url(${marketingItem.thumbnail})`,
            }}
          >
            {!!marketingItem.tipLabel && (
              <View
                className='n-marketing-sign'
                style={{
                  background: tmpStyle.bgGradualChange,
                }}
              >
                <Text className='n-sign-label'>{marketingItem.sign}</Text>
                {marketingItem.tipLabel}
              </View>
            )}
          </View>
          <View className='n-marketing-content'>
            <View className='n-marketing-title limit-line line-2'>
              <Text className='limit-line line-2'>{marketingItem.name}</Text>
              {!!(marketingItem.labelList && !!marketingItem.labelList.length) && (
                <View className='label-box'>
                  {marketingItem.labelList.map((label, index) => {
                    return (
                      <View
                        className='label-item'
                        key={index}
                        style={{
                          color: tmpStyle.btnColor,
                        }}
                      >
                        {label.name}
                      </View>
                    );
                  })}
                </View>
              )}
            </View>
            <View className='n-activity-info'>
              <View className='n-info-left'>
                <View className='n-price-box'>
                  {!!marketingItem.salePrice && (
                    <Text
                      className='n-real-sale n-money'
                      style={{
                        color: tmpStyle.btnColor,
                      }}
                    >
                      {marketingItem.saleLabel}
                      <Text className='unit'>￥</Text>
                      {filters.moneyFilter(marketingItem.salePrice, true)}
                    </Text>
                  )}

                  {!!marketingItem.labelPrice && (
                    <Text className='n-origin-sale n-money'>
                      {'￥' + filters.moneyFilter(marketingItem.labelPrice, true)}
                    </Text>
                  )}
                </View>
                <View className='n-sale-amount'>{marketingItem.saleAmount}</View>
              </View>
              <View className='n-info-right'>
                {
                  isIOS && iosSetting?.payType && marketingItem.type == 41 ? <View  className="m-btn" style={_safe_style_('background:' + (marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor))}>{iosSetting?.buttonCopywriting}</View> :
                  <View
                  className='m-btn'
                  style={{
                    background: marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor,
                  }}
                >
                  {marketingItem.btn}
                </View>
                }

              </View>
            </View>
          </View>
        </View>
      ) : showType == SHOW_TYPE.rowTwo ? (
        <View
          className='n-marketing-item-container oneRowTwo'
          onClick={_fixme_with_dataset_(go, { item: marketingItem })}
        >
          <View
            className='n-marketing-img'
            style={{
              backgroundImage: `url(${marketingItem.thumbnail})`,
            }}
          >
            {!!marketingItem.tipLabel && (
              <View
                className='n-marketing-sign'
                style={{
                  background: tmpStyle.bgGradualChange,
                }}
              >
                <Text className='n-sign-label'>{marketingItem.sign}</Text>
                {marketingItem.tipLabel}
              </View>
            )}
          </View>
          <View className='n-marketing-content'>
            <View className='n-marketing-title limit-line line-2'>{marketingItem.name}</View>
            {customContent ? (
              <Block>{props.renderOneRowTwoContent}</Block>
            ) : (
              <Block>
                <View className='n-sale-amount'>{marketingItem.saleAmount}</View>
                <View className='n-activity-info'>
                  <View className='n-two-box'>

                    <View
                      className='n-two-price common-money'
                      style={{
                        color: tmpStyle.btnColor,
                        borderColor: tmpStyle.btnColor,
                      }}
                    >
                      {filters.moneyFilter(marketingItem.salePrice, true)}
                    </View>
                    {
                      isIOS && iosSetting?.payType && marketingItem.type == 41 ? <View style={_safe_style_('background:' + (marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor))} className="n-two-btn">{iosSetting?.buttonCopywriting}</View> :
                      <View
                      style={_safe_style_('background: ' + (marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor))}
                      // style={{
                      //   background: marketingItem.disabledBtn ? '#E1E1E1' : tmpStyle.btnColor,
                      // }}
                      className='n-two-btn'
                    >
                      {marketingItem.btn}
                    </View>
                    }

                  </View>
                </View>
              </Block>
            )}
          </View>
        </View>
      ) : showType == SHOW_TYPE.rowThree ? (
        <View
          onClick={_fixme_with_dataset_(go, { item: marketingItem })}
          className='n-marketing-item-container oneRowThree'
        >
          <View
            className='n-marketing-img'
            style={{
              backgroundImage: `url(${marketingItem.thumbnail})`,
            }}
          ></View>
          {!!marketingItem.topSign && (
            <View className='haggle-icon'>
              <Image
                className='haggle-img'
                src='https://front-end-1302979015.file.myqcloud.com/images/c/images/decorate/haggle-icon.png'
              ></Image>
              <Text className='haggle-sign'>{marketingItem.topSign}</Text>
            </View>
          )}

          <View className='n-marketing-content'>
            <View className='n-marketing-title limit-line'>{marketingItem.name}</View>
            {customContent ? (
              <Block>{props.renderOneRowThreeContent}</Block>
            ) : (
              <Block>
                <View className='n-activity-info'>
                  <View className='n-price-box'>
                    {!!marketingItem.sign && <View className='n-sign'>{marketingItem.sign}</View>}
                    <Text
                      className='n-real-sale common-money'
                      style={{
                        color: tmpStyle.btnColor,
                      }}
                    >
                      {filters.moneyFilter(marketingItem.salePrice, true)}
                    </Text>
                  </View>
                </View>
              </Block>
            )}
          </View>
        </View>
      ) : (
        <View>未知 showType 类型: {showType}</View>
      )}
    </View>
  );
};

// 给props赋默认值
MarketingMoudle.defaultProps = {
  PADDING: 0,
  marketingItem: {},
  showType: 0,
  customContent: false,
};

export default MarketingMoudle;
