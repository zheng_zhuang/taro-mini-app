import React from 'react';
import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type'; // @externalClassesConvered(Empty)
import { WithPageLifecycles, _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import api from '../../../api';
import utils from '../../../utils/util.js';
import template from '../../../utils/template.js';
import authHelper from '../../../utils/auth-helper.js';
import cdnResConfig from '../../../constants/cdnResConfig.js';
import canvasHelper from '../../../utils/canvas-helper.js';
import distributionEnum from '../../../constants/distributionEnum.js';
import timer from '../../../utils/timer.js';
import constants from '../../../constants';
import industryEnum from '../../../constants/industryEnum.js';
import StoreModule from '@/wxat-common/components/decorate/storeModule/index';
import PostersDialog from '../../posters-dialog';
import SignDialog from '../../internal-dialog/sign-dialog';
import CheckCouponDialog from '../../internal-dialog/check-coupon-dialog';
import InternalMoreDialog from '../../internal-dialog/internal-more-dialog';
import InternalModule from '../../decorate/internalModule';
import LoadMore from '../../load-more/load-more';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import hoc from '@/hoc';
import { connect } from 'react-redux';
import './index.scss';
import { WaitComponent } from '@/decorators/Wait';

const internalImg = cdnResConfig.internal;
const LoadMoreStatus = constants.order.loadMoreStatus;
const mapStateToProps = (state) => ({
  allState: state,
  registered: state.base.registered,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
@WithPageLifecycles
class internalList extends WaitComponent {
  static defaultProps = {
    height: 0,
    distImgUrl: '',
    registered: false,
    internalId: '',
  };

  /**
   * 页面的初始数据
   */
  state = {
    distributionEnum,
    cdnResConfig,
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: LoadMoreStatus.HIDE,
    dateTime: {
      startTime: null,
      endTime: null,
    },

    appLogoPath: null,
    internalBg: null,
    wxUserInfo: {},
    dataList: [],
    posterDialogVisible: false,
    posterType: 'internal',
    qrCodeUrl: null,
    posterSaveImageData: null,
    cleanTime: 0, // 过期时间
    labelTime: '后过期',
    voucherNum: 0, // 内购券剩余张数,
    title: '欢迎回到员工亲友内购专场',
    num: 0,
    goods: null,
    signDialogVisible: false,
  };

  refPoster = React.createRef();
  private listDialogCOMPT: any;
  private checkCouponDialogCOMPT: any;

  refListDialogCOMPT = (node) => (this.listDialogCOMPT = node);
  refCheckCouponDialogCOMPT = (node) =>
    (this.checkCouponDialogCOMPT = node); /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    this.getList();
    this.getTemplateStyle();
    this.setInternalBg();
  }

  async onWait() {
    if (this.props.registered) {
      // 第一次进入
      const isFirst = wxApi.getStorageSync('isFirstENTER');
      if (!isFirst) {
        this.checkGain(distributionEnum.GAINTYPE.FIRST_ENTER);
      }
    }
  }

  async componentDidUpdate(preProps) {
    if (preProps.registered !== this.props.registered) {
      // 第一次进入
      const isFirst = wxApi.getStorageSync('isFirstENTER');
      if (!isFirst) {
        this.checkGain(distributionEnum.GAINTYPE.FIRST_ENTER);
      }
    }
  }

  componentWillUnmount() {
    // 删除定时器
    if (this.state._thredId) {
      timer.deleteQueue(this.state._thredId);
      this.state._thredId = null;
    }
  }

  componentDidShow() {
    if (this.isUserRegistered()) {
      this.checkGain(distributionEnum.GAINTYPE.DAILY_LOGIN);
      this.getVoucherNum();
      this.getCleanTime();
      this.setState({
        appLogoPath: this.props.allState.base.appInfo.appLogoPath,
        wxUserInfo: {
          wxNickname: this.props.allState.base.wxUserInfo.nickName,
          avatarImgUrl: this.props.allState.base.wxUserInfo.avatarUrl,
        },
      });
    }
  }

  componentDidHide() {
    // 删除定时器
    if (this.state._thredId) {
      timer.deleteQueue(this.state._thredId);
      this.state._thredId = null;
    }
  }

  onReachBottom() {
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: LoadMoreStatus.LOADING,
          pageNo: this.state.pageNo + 1,
        },

        () => {
          this.getList();
          console.log('sad');
        }
      );
    }
  }

  setInternalBg() {
    let internalBg = cdnResConfig.internalBuy.internalBg;
    if (industryEnum.isSpecApp(industryEnum.appTag.jianpai)) {
      internalBg = cdnResConfig.internalBuy.jianpaiInternalBg;
    }
    this.setState({
      internalBg,
    });
  }

  handleRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: LoadMoreStatus.LOADING,
    });

    this.getList();
  };

  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  showDialog = () => {
    console.log('11111111111asdasd');
    if (!authHelper.checkAuth()) {
      return;
    }
    console.log('进入了11111111111asda111111111111sd');
    // this.selectComponent('#dialog').show()
    this.listDialogCOMPT.show();
  };

  handleShowDialog = (e) => {
    this.setState({ title: '领取内购券成功', num: e }, () => {
      this.setState({ signDialogVisible: true });
      // 领取完刷新一次
      this.getVoucherNum();
    });
    // this.selectComponent('#sign-dialog').show()
  };

  create() {}
  // 显示海报
  handleShowPosterDialog() {
    if (!authHelper.checkAuth()) {
      return;
    }
    if (this.refPoster.current) {
      this.refPoster.current.showDialog();
    }
    this.setState(
      {
        posterDialogVisible: true,
        qrCodeUrl: '',
      },

      () => {
        const pages = Taro.getCurrentPages(); // 获取加载的页面
        const currentPage = pages[pages.length - 1]; // 获取当前页面的对象
        // 内购页面url
        const url = 'sub-packages/distribution-package/pages/internal-purchase/center/index';
        let verificationNo = '';
        if (this.props.allState.base.userInfo) {
          verificationNo = this.props.allState.base.userInfo.id;
          const distributorId = wxApi.getStorageSync('internalId');
          if (distributorId) {
            verificationNo += '_' + distributorId;
          }
        }
        wxApi
          .request({
            url: api.verification.getQRCode,
            loading: true,
            data: {
              verificationType: 5,
              maPath: url,
              verificationNo: verificationNo,
            },
          })
          .then((res) => {
            if (this.refPoster.current) {
              this.refPoster.current.showDialog();
            }
            this.setState({
              posterDialogVisible: true,
              qrCodeUrl: res.data.qrCode,
            });
          });
      }
    );
  }

  // 用户是否注册过
  isUserRegistered() {
    return !!this.props.allState.base.registered;
  }

  // 分类
  handleClassfiy = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/distribution-package/pages/internal-purchase/classfiy/index',
      data: {},
    });
  };

  /**
   * 获取内购券商品
   */
  getList(isStart) {
    if (isStart) {
      this.state.pageNo = 1;
      this.state.hasMore = true;
    }
    this.setState({ error: false });
    const params = {
      activityType: 1,
      pageNo: this.state.pageNo,
      pageSize: 10,
    };

    wxApi
      .request({
        url: api.distribution.popularizeMaterial,
        data: params,
      })
      .then((res) => {
        const data = res.data || [];
        let dataList;
        if (this.isLoadMoreRequest()) {
          dataList = this.state.dataList.concat(data);
        } else {
          dataList = data;
        }
        console.log('dataList============', dataList);
        const hasMore = dataList.length < res.totalCount;
        this.setState({
          dataList,
          hasMore,
        });
      });
  }

  // 获取过期时间
  getCleanTime() {
    const params = {};
    return wxApi
      .request({
        url: api.internalVoucher.getCleanTime,
        data: params,
      })
      .then((res) => {
        // this.triggerEvent('time', res.data || 0)
        console.log('asdasdsadsad');
        this.props.onTime(res.data || 0);
        this.setState(
          {
            cleanTime: res.data || 0,
          },

          () => {
            this.timerRender();
          }
        );
      })
      .catch((error) => {});
  }

  timerRender() {
    this.update();
    // 执行定时器
    this.state._thredId = timer.addQueue(() => {
      this.update();
    });
  }

  update() {
    const cleanTime = this.state.cleanTime - 1000;
    const labelTime = utils.formatStringTime(cleanTime);
    this.setState(
      {
        cleanTime: cleanTime,
        labelTime: labelTime + '后过期',
      },

      () => {
        const isNoEnd = cleanTime === 0;
        // 删除定时器
        if (isNoEnd && this.state._thredId) {
          timer.deleteQueue(this.state._thredId);
          this.state._thredId = null;
          this.setState(
            {
              labelTime: '已过期',
            },

            () => {}
          );

          // 删除定时器刷新一次
          this.getVoucherNum();
        }
      }
    );
  }

  // 获取内购券数量
  getVoucherNum() {
    const params = {};
    return wxApi
      .request({
        url: api.internalVoucher.getVoucherNum,
        data: params,
      })
      .then((res) => {
        this.setState({
          voucherNum: res.data || 0,
        });
      })
      .catch((error) => {});
  }

  // 领取
  gainVoucher(type) {
    return wxApi
      .request({
        url: api.internalVoucher.gainVoucher,
        data: {
          type: type,
        },
      })
      .then((res) => {
        if (res.data) {
          if (type === distributionEnum.GAINTYPE.FIRST_ENTER || type === distributionEnum.GAINTYPE.DAILY_LOGIN) {
            this.setState({ title: '欢迎回到员工亲友内购专场', num: res.data });
            if (type === distributionEnum.GAINTYPE.FIRST_ENTER) {
              wxApi.setStorageSync('isFirstENTER', 1);
              this.setState({
                title: '欢迎来到员工亲友内购专场',
                num: res.data,
              });
            }
            this.setState({ signDialogVisible: true });
          }
          // 领取完刷新一次
          this.getVoucherNum();
        }
      })
      .catch((error) => {});
  }

  // 检验可领取的内购券张数
  checkGain(type) {
    return wxApi
      .request({
        url: api.internalVoucher.gainAvailableVoucherNum,
        data: {
          type: type || distributionEnum.GAINTYPE.DAILY_LOGIN,
        },
      })
      .then((res) => {
        if (res.data) {
          this.gainVoucher(type);
        }
      })
      .catch((error) => {});
  }

  showDeficiency(e) {
    console.log('展示', e);
    this.setState({ goods: e }, () => {
      console.log('展示');
      this.checkCouponDialogCOMPT.show();
    });
    // this.selectComponent('#check-coupon-dialog').show()
  }

  /**
   * 保存海报图片到本地
   */
  handleSaveImage = (e) => {
    this.setState(
      {
        posterSaveImageData: e,
      },

      () => {
        // this.triggerEvent('save')
        this.props.onSave();
      }
    );
  };

  handleSaveInternalImage = (context) => {
    wxApi.showLoading({
      title: '正在保存图片...',
    });

    console.log('cu', this.state.posterSaveImageData);
    const { user } = this.state.posterSaveImageData;
    const canvasContext = wxApi.createCanvasContext('shareCanvas', context);
    const imageRequests = [
      wxApi.getImageInfo({
        src: user.headUrl,
      }),

      wxApi.getImageInfo({
        src: cdnResConfig.internalBuy.posterBg,
      }),
    ];

    if (this.state.qrCodeUrl) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: this.state.qrCodeUrl,
        })
      );
    }
    Promise.all(imageRequests)
      .then((res) => {
        // 背景
        canvasHelper.drawFillRect(canvasContext, 0, 0, 275, 345, '#fff');
        // 头像
        canvasHelper.drawCircleImage(canvasContext, 138, 45, 30, res[0].path);

        // 标题
        canvasHelper.drawText(canvasContext, '送你亲友内购券，品牌好货尽享低价', 26, 100, {
          textAlign: 'start',
          fillStyle: '#323232',
          fontSize: 14,
        });

        // 内购券图片
        canvasHelper.drawImage(canvasContext, 26, 110, 224.5, 133.5, res[1].path);

        // 二维码
        if (this.state.qrCodeUrl && res[2]) {
          canvasHelper.drawImage(canvasContext, 102.5, 234, 69, 69, res[2].path);
        }

        // 文案
        canvasHelper.drawText(canvasContext, '长按识别小程序码，立即领取', 66.5, 313, {
          textAlign: 'start',
          fillStyle: '#A6A6A6',
          fontSize: 10,
        });

        canvasContext.draw(false, () => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'shareCanvas',
              },

              context
            )
            .then((result) => {
              wxApi.$saveImage(result.tempFilePath);
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      })
      .catch((error) => {
        wxApi.hideLoading();
      });
  };

  handleSignDialogHide = () => {
    this.setState({ signDialogVisible: false });
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
  }

  render() {
    const {
      internalBg,
      appLogoPath,
      wxUserInfo,
      dataList,
      error,
      loadMoreStatus,
      voucherNum,
      labelTime,
      title,
      num,
      cleanTime,
      posterDialogVisible,
      posterType,
      qrCodeUrl,
      goods,
      signDialogVisible,
    } = this.state;
    const { distImgUrl, height, internalId } = this.props;
    return (
      <View data-scoped='wk-cti-InternalList' className='wk-cti-InternalList'>
        <View className='internal-list'>
          <Image src={internalBg} className='poster'></Image>
          <View className='logo'>
            <Image className='logo__image' src={appLogoPath}></Image>
          </View>
          <View className='hover-box'>
            <View className='desc'>由内部员工，经过好友分享给我</View>
            <View className='link-box'>
              <View className='link'>
                <Image className='head-icon' src={distImgUrl}></Image>
              </View>
              <View className='graphics'></View>
              <View className='link'>
                <View className='point'>
                  <Text className='link__text'></Text>
                  <Text className='link__text'></Text>
                  <Text className='link__text'></Text>
                </View>
              </View>
              <View className='graphics'></View>
              <View className='link'>
                <Image className='head-icon' src={wxUserInfo.avatarImgUrl}></Image>
              </View>
              <View className='graphics'></View>
              <ButtonWithOpenType openType='share' className='link shadow'>
                <Image className='head-icon' src={internalImg.share}></Image>
              </ButtonWithOpenType>
            </View>
          </View>
          <View className='store-module'>
            <StoreModule></StoreModule>
          </View>
          <View className='title'>
            <Image className='title__image' src={internalImg.titleBg}></Image>
            <View className='text'>员工亲友专供商品</View>
          </View>
          {!!(!!dataList && dataList.length == 0) && <Empty message='敬请期待...'></Empty>}
          {error ? (
            <Error></Error>
          ) : (
            <InternalModule
              internalId={internalId}
              dataList={dataList}
              onDeficiency={this.showDeficiency.bind(this)}
            ></InternalModule>
          )}

          <LoadMore status={loadMoreStatus} onRetry={this.handleRetryLoadMore}></LoadMore>
          <View style={_safe_style_('height:' + (200 + height) / 2 + 'px;')}></View>
          <View className='coupon-box' style={_safe_style_('bottom:' + (height + 16) / 2 + 'px')}>
            <View className='my-coupon'>
              <View className='count'>
                <Image className='my-coupon__image' src={internalImg.couponIcon}></Image>
                <Text>{voucherNum + '张内购券'}</Text>
              </View>
              <View className='coupon-time'>{labelTime}</View>
            </View>
            <View className='btn-more' onClick={this.showDialog}>
              获取更多
            </View>
            <View className='classfiy' onClick={this.handleClassfiy}>
              <Image className='classfiy__image' src={internalImg.classfiyIcon}></Image>
              <View className='classfiy__view'>分类</View>
            </View>
          </View>
        </View>
        <InternalMoreDialog
          ref={this.refListDialogCOMPT}
          id='dialog'
          appLogoPath={appLogoPath}
          height={height}
          onCreate={this.handleShowPosterDialog.bind(this)}
          onGain={this.handleShowDialog}
        ></InternalMoreDialog>
        <SignDialog
          visible={signDialogVisible}
          onHide={this.handleSignDialogHide}
          title={title}
          num={num}
          time={cleanTime}
        ></SignDialog>
        {/*  生成海报对话弹框  */}
        <PostersDialog
          visible={posterDialogVisible}
          posterType={posterType}
          qrCode={qrCodeUrl}
          childRef={this.refPoster}
          onSave={this.handleSaveImage}
        ></PostersDialog>
        <CheckCouponDialog
          ref={this.refCheckCouponDialogCOMPT}
          id='check-coupon-dialog'
          goods={goods}
          onCreate={this.handleShowPosterDialog.bind(this)}
        ></CheckCouponDialog>
      </View>
    );
  }
}

export default internalList;
