// @externalClassesConvered(AnimatDialog)
// @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import React, { ComponentClass, Component } from 'react';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';

// pages/mine/account-balance/index.tsx

import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api';
import template from '@/wxat-common/utils/template.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import AuthUser from '@/wxat-common/components/auth-user';
import AnimatDialog from '@/wxat-common/components/animat-dialog';
import IntegralModule from '@/wxat-common/components/decorate/integralModule';
import Error from '@/wxat-common/components/error/error';
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import { connect } from 'react-redux';

const integralBg = cdnResConfig.integral.integralBg;

interface PageStateProps {
  wxUserInfo: any;
  userInfo: any;
  registered: any;
  scoreName?: string;
}

interface PageDispatchProps {}

interface PageOwnProps {}
interface PageState {
  score: number;
  pageNo: number;
  scoreIntroduction: string;
  error: boolean;
  wxUserInfos: Record<string, any>;
  dataList: Array<Record<string, any>>;
  dataSource: Record<string, any>;
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const mapStateToProps = (state) => ({
  wxUserInfo: state.base.wxUserInfo,
  userInfo: state.base.userInfo,
  registered: state.base.registered,
  scoreName: state.globalData.scoreName || '积分',
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class IntegralList extends Component<IProps, PageState> {
  constructor(props) {
    super(props);
    this.state = {
      score: 0,
      pageNo: 1,
      scoreIntroduction: '',
      error: false,
      wxUserInfos: {},
      dataList: [],
      dataSource: {},
    };
  }

  componentDidMount() {
    // this.onUserInfoReady()
    this.attached();
  }

  stateChange = (data) => {
    // 切换店铺
    const { wxUserInfo, userInfo } = this.props;
    if (data) {
      if (data.key === 'registered') {
        if (wxUserInfo) {
          this.setState({
            wxUserInfos: {
              wxNickname: wxUserInfo.nickName,
              avatarImgUrl: wxUserInfo.avatarUrl,
            },
          });
        } else if (userInfo) {
          this.setState({
            wxUserInfos: {
              wxNickname: userInfo.nickname,
              avatarImgUrl: userInfo.avatarImgUrl,
            },
          });
        }
        this.getBalance();
      }
    }
  };
  getScoreIntroduction = () => {
    return wxApi
      .request({
        url: api.user.scoreIntroduction,
        data: {},
      })
      .then((res) => {
        this.setState({
          scoreIntroduction: res.data || '无',
        });
      })
      .catch((error) => {});
  };
  /**
   *  获取积分
   **/
  getBalance = () => {
    return wxApi
      .request({
        url: api.userInfo.balance,
        data: {},
      })
      .then((res) => {
        this.setState({
          score: res.data.score,
        });
      })
      .catch((error) => {
        console.log('score: error: ' + JSON.stringify(error));
      });
  };
  onUserInfoReady = () => {
    this.getScoreIntroduction();
    this.getBalance();
  };
  attached = () => {
    const { wxUserInfo } = this.props;
    this.getList();
    this.getTemplateStyle();
    if (this.isUserRegistered()) {
      this.getScoreIntroduction();
      this.getBalance();
      this.setState({
        wxUserInfos: {
          wxNickname: wxUserInfo.nickName,
          avatarImgUrl: wxUserInfo.avatarUrl,
        },
      });
    }
  };

  // 用户是否注册过
  isUserRegistered = () => {
    return !!this.props.registered;
  };

  /**
   * 获取积分商品列表
   */
  getList = () => {
    const params = {
      type: 22,
      pageNo: this.state.pageNo,
      pageSize: 10,
    };

    wxApi
      .request({
        url: api.goods.integralQuery,
        data: params,
      })
      .then((res) => {
        console.log(res);
        const dataList = res.data || [];
        this.setState({
          dataList,
          dataSource: res,
        });
      });
  };
  handleToDetail = () => {
    wxApi.$navigateTo({
      url: 'sub-packages/moveFile-package/pages/mine/integral/index',
    });
  };
  handleToMore = () => {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/integral/goods/index',
    });
  };
  handleToRecord = () => {
    wxApi.showToast({
      title: '敬请期待',
      icon: 'none',
    });
  };

  showInstruction = () => {
    this.childsCMTP.show({ scale: true });
  };
  hideInstruction = () => {
    this.childsCMTP.hide();
  };
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
  };

  private childsCMTP: any;
  refChildsCMTP = (node) => (this.childsCMTP = node);

  render() {
    const { score, dataList, error, scoreIntroduction, wxUserInfos, dataSource } = this.state;
    const { scoreName } = this.props;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-cti-IntegralList'
        className='wk-cti-IntegralList'
      >
        <AuthUser onReady={this.onUserInfoReady}>
          <View style={_safe_style_('position: relative;padding-bottom: 212rpx;')}>
            <View className='header'>
              <Image src={integralBg} className='integral-bg'></Image>
              <View className='header-container'>
                <View className='header-title'>
                  <View>
                    {!!wxUserInfos.avatarImgUrl && <Image className='head-icon' src={wxUserInfos.avatarImgUrl}></Image>}
                  </View>
                  <View className='head-msg'>
                    {'Hi，' + (wxUserInfos.wxNickname || '未知') + '，欢迎来到' + scoreName + '商城'}
                  </View>
                </View>
                <View className='header-content'>
                  <Text className='integral'>{score}</Text>
                  <View className='header-btn'>
                    <Text onClick={this.showInstruction}>{scoreName}规则</Text>
                    <Text style={_safe_style_('margin-left: 124rpx;')} onClick={this.handleToDetail}>
                      {scoreName}明细
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View className='integral-goods'>
              <View style={_safe_style_('text-align: center;margin-bottom: 30rpx;')}>
                <Text className='integral-title'>兑换商品</Text>
              </View>
              {!!(!!dataList && dataList.length == 0) && <Empty message='敬请期待...'></Empty>}
              {error ? (
                <Error></Error>
              ) : (
                <IntegralModule
                  scoreName={scoreName}
                  _renderList={dataList}
                  showNumberType={2}
                  dataSource={dataSource}
                ></IntegralModule>
              )}

              {!!(dataList && dataList.length) && (
                <View className='more' onClick={this.handleToMore}>
                  查看更多
                </View>
              )}
            </View>
            <View className='footer'>
              <Text className='footer-dosc'>{scoreName}商城</Text>
            </View>
          </View>
        </AuthUser>
        <AnimatDialog animClass='instruction' ref={this.refChildsCMTP}>
          <View className='instruction-body'>
            <View className='instruction-body__title'>{scoreName}规则</View>
            <Text className='instruction-body__view'>{scoreIntroduction}</Text>
            <View className='instruction-body__btn' onClick={this.hideInstruction}>
              确定
            </View>
          </View>
        </AnimatDialog>
      </View>
    );
  }
}
export default IntegralList;
