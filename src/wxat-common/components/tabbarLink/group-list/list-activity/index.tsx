import React from 'react';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';
import CollageModule from '@/wxat-common/components/decorate/collageModule/index';
import './index.scss';

class ListActivity extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    activityList: [],
    showGroupType: 0,
    PADDING: 0,
  };

  state = {
    tmpStyle: {},
  }; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    this.getTemplateStyle();
  }

  /**
   * 组件的方法列表
   */
  onGroup = (e) => {
    const item = e;
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/group/detail/index',
      data: {
        activityId: item.id,
        itemNo: item.itemNo,
      },
    });
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { showGroupType, activityList, PADDING } = this.props;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-tgl-ListActivity'
        className='wk-tgl-ListActivity'
      >
        <CollageModule padding={PADDING} dataList={activityList} showGroupType={showGroupType} onClick={this.onGroup} />
      </View>
    );
  }
}

export default ListActivity;
