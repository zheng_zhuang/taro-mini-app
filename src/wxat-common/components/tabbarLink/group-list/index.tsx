import React, { Component } from 'react'; // @externalClassesConvered(Empty)
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';
import login from '../../../x-login/index.js';
import constants from '../../../constants/index.js';
import template from '../../../utils/template.js';
import ListActivity from './list-activity/index';
import LoadMore from '../../load-more/load-more';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import './index.scss';

const loadMoreStatus = constants.order.loadMoreStatus;
type DefaultProps = {
  showGroupType: number;
};


class GroupLists extends Component {
  /**
   * 组件的属性列表
   */
  // 父组件传过来的拼团活动展示类型，showGroupType:0：上图下文(默认展示的类型)，showGroupType:1：左图右文
  static defaultProps: DefaultProps = {
    showGroupType: 0,
  };

  state = {
    activityList: null,
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
  };
  isLoading = true
  onPullDownRefresh = () => {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.listActivityList(true);
      }
    );
  };

  onReachBottom = () => {
    if (this.state.hasMore && !this.isLoading) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.listActivityList();
    }
  };

  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.listActivityList();
  };

  listActivityList = (isFromPullDown?) => {
    this.setState({
      error: false,
    });

    login.login().then(() => {
      wxApi
        .request({
          url: api.activity.list,
          loading: true,
          data: {
            pageNo: this.state.pageNo,
            pageSize: 10,
          },
        })
        .then((res) => {
          console.log(res);
          let listSouce = this.state.activityList || [];
          if (this.isLoadMoreRequest()) {
            listSouce = listSouce.concat(res.data || []);
          } else {
            listSouce = res.data || [];
          }
          if (listSouce.length === res.totalCount) {
            this.setState({
              hasMore: false,
            });
          }
          this.setState({
            activityList: listSouce || [],
            pageNo: this.state.pageNo + 1,
            loadMoreStatus: loadMoreStatus.HIDE
          },()=>{
            this.isLoading = false
          });
        })
        .catch((error) => {
          console.log('err======>', error);
          if (this.isLoadMoreRequest()) {
            this.setState({
              loadMoreStatus: loadMoreStatus.ERROR,
            });
          } else {
            this.setState({
              error: true,
            });
          }
        })
        .finally(() => {
          if (isFromPullDown) {
            wxApi.stopPullDownRefresh();
          }
        });
    });
  };
  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }; /*请尽快迁移为 componentDidMount 或 constructor*/
  UNSAFE_componentWillMount() {
    this.listActivityList();
    this.getTemplateStyle();
  }

  render() {
    const { activityList, error, loadMoreStatus } = this.state;
    console.log(loadMoreStatus, 'loadMoreStatus ====>>>');

    const { showGroupType } = this.props;
    return (
      <View data-scoped='wk-ctg-GroupList' className='wk-ctg-GroupList serve-list'>
        {!!(!!activityList && activityList.length === 0 && !error) && (
          <Empty iconClass='empty-icon' message='敬请期待...' />
        )}

        {!!error && <Error></Error>}
        {!!activityList && (
          <View className='group'>
            <ListActivity activityList={activityList} showGroupType={showGroupType} />
          </View>
        )}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore} />
      </View>
    );
  }
}

export default GroupLists;
