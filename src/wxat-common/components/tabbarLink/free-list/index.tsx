import React, { Component } from 'react'; // @externalClassesConvered(AnimatDialog)
// @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// pages/mine/account-balance/index.js
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';

import template from '../../../utils/template.js';
import authHelper from '../../../utils/auth-helper.js';
import AnimatDialog from '../../animat-dialog/index';
import FreeModule from '../../base/freeModule/index';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import { connect } from 'react-redux';
import './index.scss';

const mapStateToProps = (state: any) => ({
  globalData: state.globalData,
  base: state.base,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })

class FreeList extends Component {
  /**
   * 页面的初始数据
   */
  state = {
    score: 0,
    hasMore: true,
    billList: [],
    scoreIntroduction: '',
    IntroductionName: '',
    error: false,
    dateTime: {
      startTime: null,
      endTime: null,
    },

    avatarImgUrl: null,
    userName: null,
    memberLevel: null,
    dataList: [],
  };

  pageNo = 1;
  isLoading = true
  freeInsDialogRef = React.createRef<AnimatDialog>();

  stateChange = (data) => {
    //切换店铺
    if (data) {
      if (data.key === 'registered') {
        if (this.props.base.wxUserInfo) {
          this.setState({
            avatarImgUrl: this.props.base.wxUserInfo.avatarUrl,
            userName: this.props.base.wxUserInfo.nickName,
            memberLevel: this.props.base.userInfo.levelName,
          });
        } else if (this.props.base.userInfo) {
          this.setState({
            avatarImgUrl: this.props.base.userInfo.avatarUrl,
            userName: this.props.base.userInfo.nickName,
            memberLevel: this.props.base.userInfo.levelName,
          });
        }
        this.getBalance();
      }
    }
  };

  showInstruction = () => {
    this.freeInsDialogRef.current!.show({
      scale: true,
    });
  };

  hideInstruction = () => {
    this.freeInsDialogRef.current!.hide();
  };

  /**
   * 获取积分商品列表
   */
  getList = () => {
    // this.setState({ error: false })
    const params = {
      pageNo: this.pageNo,
      pageSize: 10,
    };

    wxApi
      .request({
        url: api.giftActivity.list,
        data: params,
      })
      .then((res) => {
        const { dataList } = this.state;
        const newList = res.data || [];
        let totalList = dataList;
        if (this.pageNo > 1) {
          totalList = totalList.concat(newList);
        } else {
          totalList = newList;
        }
        if (totalList.length >= res.totalCount) {
          this.setState({
            hasMore: false,
          });
        }
        this.pageNo = this.pageNo + 1;
        this.setState({
          dataList: totalList,
          error: false,
        },()=>{
          this.isLoading = false
        });
      });
  };

  showInstruction = () => {
    this.freeInsDialogRef.current!.show({
      scale: true,
    });
  };

  getScoreIntroduction = () => {
    //todo替换成赠品的规则，等待后端接口
    return wxApi
      .request({
        url: api.giftActivity.giftRule,
        data: {},
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            IntroductionName: res.data.name || '赠品规则',
            scoreIntroduction: res.data.desc || '无',
          });
        }
      })
      .catch((error) => {});
  };

  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    this.getList();
    this.getScoreIntroduction();
    if (!authHelper.checkAuth()) {
      return;
    }

    this.getTemplateStyle();

    this.setState({
      avatarImgUrl: this.props.base.wxUserInfo.avatarUrl,
      userName: this.props.base.wxUserInfo.nickName,
      memberLevel: this.props.base.userInfo.levelName,
    });
  }

  onReachBottom() {
    const { hasMore } = this.state;
    if (hasMore && !this.isLoading) {
      this.getList();
    }
  }

  render() {
    const {
      avatarImgUrl,
      userName,
      memberLevel,
      dataList,
      error,
      hasMore,
      IntroductionName,
      scoreIntroduction,
    } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-ctf-FreeList' className='wk-ctf-FreeList'>
        <View style={_safe_style_('position: relative;padding-bottom: 212rpx;')}>
          <View className='header'>
            <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/wxat-common/free-bg.png" className='free-bg' />
            <View className='header-container'>
              <View className='header-userinfo'>
                <View>{!!avatarImgUrl && <Image className='head-icon' src={avatarImgUrl} />}</View>
                <View className='header-user'>
                  <View className='header-name'>{userName || '未知'}</View>
                  <View className='header-member'>{memberLevel || '未知'}</View>
                </View>
              </View>
              <View>
                <View onClick={this.showInstruction} className='label'>
                  规则说明
                </View>
                {/*  <View class="label">领取记录</View>  */}
              </View>
            </View>
          </View>
          <View className='free-goods'>
            <View style={_safe_style_('text-align: center;margin-bottom: 30rpx;')}>
              <Text className='title'>赠品兑换</Text>
            </View>
            {!!(!!dataList && dataList.length == 0) && <Empty message='敬请期待...'></Empty>}
            {error ? (
              <Error></Error>
            ) : !!dataList && dataList.length > 0 ? (
              <FreeModule list={dataList} showType={2} />
            ) : null}
          </View>
          {!hasMore && (
            <View className='footer'>
              <Text className='footer-dosc'>没有更多了</Text>
            </View>
          )}
        </View>
        <AnimatDialog animClass='instruction' ref={this.freeInsDialogRef}>
          <View className='instruction-body'>
            <View className='title'>{IntroductionName}</View>
            <View className='view'>{scoreIntroduction}</View>
            <View className='btn' onClick={this.hideInstruction}>
              确定
            </View>
          </View>
        </AnimatDialog>
      </View>
    );
  }
}

export default FreeList;
