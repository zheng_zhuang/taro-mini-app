
import { View, Image } from '@tarojs/components';
import { _safe_style_, _fixme_with_dataset_, PageStyle } from '@/wxat-common/utils/platform';

import React, { Component, ComponentClass } from 'react';
import TabHotel from "../TabHotel";
import Empty from '@/wxat-common/components/empty/empty';
import filters from '@/wxat-common/utils/money.wxs';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { toJumpYQZ } from '@/wxat-common/utils/yqz';

import constants from '@/wxat-common/constants/index';
import HotelOperation from "../operation/HotelOperation";
import "./index.scss";


const loadMoreStatus = constants.order.loadMoreStatus;
interface PageStateProps {
  isOpenInvoice: boolean;
  industry: string;
  style?: React.CSSProperties;
  iosSetting: any;
  tmpStyle?: Record<string, any>; // 模板配置样式
  mineConfig?: Record<string, any>;
}
interface PageState {
  statusType: any; // 订单类型
  orderList: Array<Record<string, any>>; // 订单列表
  pageNo: number;
  loadMoreStatus: number; // 更多状态
  totalCount: number;
}
type IProps = PageStateProps 

interface HotelList {
  props: IProps;
}
class HotelList extends Component {
  static defaultProps = {
    isOpenInvoice: false,
    industry: '',
  };

  state = {
    orderList: [],
    statusType: '',
    pageNo: 1,
    hasMore: false,
    loadMoreStatus: loadMoreStatus.HIDE,
    totalCount: 0,

  }

  componentDidMount() {
    this.getHotelList(true)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!!nextProps.mineConfig && !this.props.mineConfig) {
      // fixme 由于H5刷新，再获取模板之前渲染页面
      this.getHotelList(true)
    }
  }

  getDaysBetween = (dateString1, dateString2) => {
    const startDate = Date.parse(dateString1);
    const endDate = Date.parse(dateString2);
    if (startDate > endDate) {
      return 0;
    }
    if (startDate == endDate) {
      return 1;
    }
    const days = (endDate - startDate) / (1 * 24 * 60 * 60 * 1000);
    return days;
  }

  // 格式化日期，只显示月份和日
  MonthDayformat(date) {
    const Dtime = new Date(date);
    const Dtimemonth = Dtime.getMonth() + 1;
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}月${Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
      }日`;
  }

  // 获取当前是周几
  getWeekDate(dateDay) {
    const now = new Date(dateDay);
    const day = now.getDay();
    const weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
    const week = weeks[day];
    return week;
  }

  // 过滤状态
  filtersStatus(status) {
    const list = {
      // 10: '待支付',
      // '-3': '支付超时',
      // '20': '确认中',
      // '30': '已确认',
      // '-1': '已取消',
      // '-10': '已支付取消确认中',
      // '-4':'确认拒绝',
      // '-20':'退款完成',
      // '60':'已完成',
      // '93':'待审核',
      10: "待支付",
      '-3': "支付超时",
      30: "等待酒店确认",
      40: "预订成功",
      '-4': "已取消",
      '-1': "已取消",
      '-20': "已取消",
      60: "已入住"
    }
    return list[status]
  }

  // 向下刷新
  onPullDownRefresh = () => {
    this.setState({
      pageNo: 1
    })
    this.getHotelList(false);
  };

  useReachBottom = () => {
    console.log('onReachBottom')
  }

  // 向上拉伸刷新
  onReachBottom = () => {
    // 判断是否允许加载更多
    const { totalCount, orderList } = this.state
    if (orderList.length < totalCount) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
        pageNo: 1
      });

      this.getHotelList(false);
    }
  };

  statusTypeProps = (type) => {
    this.setState({
      statusType: type.status,
      pageNo: 1,
      orderList: []

    },
      () => {
        this.getHotelList(true)
      }
    )
  }

  getHotelList = async (loading) => {
    const params = {
      pageSize: 20,
      pageNo: this.state.pageNo,
      orderStatus: this.state.statusType,
      orderType: 1,// 个人
    }
    const apiUrl = api.hotelOrder.list
    try {
      const res = await wxApi.request({ url: apiUrl, loading: loading, data: params, quite: true});
      const data = res.data || []
      if (this.state.pageNo === 1) {
        this.setState({
          orderList: [...data]
        })
      } else {
        const list = this.state.orderList.concat(data)
        this.setState({
          orderList: list
        })
      }
    } catch (error) {
    }
  };

  toYqz = async (item) => {
    const path = 'subPackagesOrder/orderDetails/index';
    wxApi.showLoading({
      title: '正为你跳转',
    });
    const params = {
      orderId: item.orderNo,
    }
    wxApi.hideLoading();
    toJumpYQZ(path, params);
  };

  updateList = () => {
    this.setState({
      pageNo: 1,
      orderList: []

    },
      () => {
        this.getHotelList(true)
      }
    )
  }

  render() {
    const { orderList } = this.state;
    const { tmpStyle } = this.props;
    return (
      <View data-fixme='02 block to view. need more test'
        data-scoped='wk-cto-OrderList'
        className='wk-cto-OrderList' style={_safe_style_(PageStyle(this.props.style))}>
        <View className='order-list-container'>
        <TabHotel statusTypeProps={this.statusTypeProps} tmpStyle={tmpStyle}></TabHotel>
        {!!orderList && orderList.length === 0 ? (
          <Empty message="暂无订单"></Empty>
        ) : (
            <View className='hotel-list order-list-body'>
            {
              orderList.map((item, index) => {
                return (
                  <View className='hotel-list-item' key={index}>
                    <View className='item-header'>
                      <View className='orderNo'>订单号：{item.orderNo}</View>
                      <View className='orderTime'>{item.createTime}</View>
                    </View>
                    <View className='item-wrapper' onClick={() => {this.toYqz(item)}}>
                      <View className='img-box'>
                        <Image
                          src={item.hotelMainPic}
                          className='item-img'
                        ></Image>
                      </View>
                      <View className='item-info'> 
                        <View className='hotel-content'>
                          <View className='hotel-content-left'>
                            <View className='hotel-name ellipsis'>
                              {item.hotelName}
                            </View>
                            <View className='hotel-room ellipsis'>{item.roomName}</View>

                          </View>
                          <View className='hotel-content-right'>
                            <View className='status' style={_safe_style_(`color: ${tmpStyle.btnColor}`)}>{this.filtersStatus(item.orderStatus)}</View>
                            <View className='price' style={_safe_style_(`color: ${tmpStyle.btnColor}`)}>
                              {'￥' + filters.moneyFilter(item.realPayPrice, true)}
                            </View>
                          </View>
                        </View>
                        <View className='hotel-checktime'>
                          {this.MonthDayformat(item.checkInDate)} - {this.MonthDayformat(item.checkOutDate)}
                          {this.getDaysBetween(item.checkInDate, item.checkOutDate)}晚/
                          {item.roomNum}间
                        </View>
                      </View>
                    </View>
                    <View className='order-controls-btn'>
                      <HotelOperation tmpStyle={tmpStyle} orderInfo={item} updateList={() => { this.updateList() }}></HotelOperation>
                    </View>
                  </View>
                )
              })
            }
          </View>
        )}
        </View>
      </View>
    );
  }
}

export default HotelList as ComponentClass<PageState>;