import { View } from '@tarojs/components';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import React, { Component } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import checkHotel from "@/wxat-common/utils/checkHotel";
import commonWxPay from "@/wxat-common/utils/commonPay";
import { toJumpYQZ } from '@/wxat-common/utils/yqz';
import { useSelector, connect } from 'react-redux';
import dateUtil from '@/wxat-common/utils/date';
import "./operation.scss";

const mapStateToProps = ({ base }) => ({
  gps: base.gps
});
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class HotelOperation extends Component {
  state = {
  }

/* 控制操作显示 */

  isShowControls = (orderInfo) => {
    return orderInfo.orderStatus === 10 || orderInfo.orderStatus === 30 || orderInfo.orderStatus !==10
  }

/* 支付 */
  rePay = async (item) => {
    try {
      const { metaData, checkInDate, checkOutDate, sourceType } = item
      const { priceItems, ratePlanId, roomNum, sellRoomId, supplyCode, thirdHotelId, thirdRoomId } = JSON.parse(metaData)
      const paramsJson = {
        thirdHotelId,
        thirdRoomId,
        sellRoomId, // 携程房型号
        roomNum, // 总预订房间数
        supplyCode, // TMC供应商编码
        ratePlanId,
        checkInDate,
        checkOutDate,
        priceItems,
        sourceType: sourceType,
      }
      const result = await checkHotel(paramsJson);
      console.log(result);
      if (!result.reservation) {
        wxApi.showToast({
          title: result.decs || '酒店不可预订，请联系客服',
          icon: "none",
        });
        return
      }
      const payParams = {
        orderNo: item.orderNo,
        payChannel: 1, // 1微信支付 3余额支付
        payTradeType: commonWxPay.returnPayTradeType() //  1:微信小程序支付 3:微信公众号支付

      }
      const apiUrl = api.hotelOrder.orderPay;
      const { data = {} } = await wxApi.request({ url: apiUrl, loading: true, data: payParams, })
      const params = {
        ...data,
        orderNo: data.orderNoText,
      }
      const opstions = {
        wxPaySuccess: () => {
          this.toYqz('pages/order/orderStatus', { orderPayStatus: 1, orderId: item.orderNo })
        },
        wxPayFail: () => {
          this.toYqz('subPackagesOrder/orderDetails/index', { isOrderEnterprise: false, orderId: item.orderNo })
        }
      }
      commonWxPay.hotelPay(params, opstions)
    } catch (error) {
      console.log('error: ', error);
    }
  }

 /* 取消订单 */
  toCancellationOrder = (row) => {
    if (row.orderStatus === 10) {  
      wxApi.showModal({
        title: '是否确认取消',
        content: '确认取消么',
        success: async (res) => {
          if (res.confirm) {
            const cancelParams = {
              orderNo: row.orderNo,
              operationType: 2,
            };
            try {
              const apiUrl = api.hotelOrder.hotelCancelOrder;
              await wxApi.request({ url: apiUrl, method: 'post', loading: true, data: cancelParams })
              const path = 'subPackagesOrder/cancel/cancelOrderStatus';
              const params = {
                user_id: row.userId,
                item_hotel_no: row.itemHotelNo
              }
              this.toYqz(path, params)
            } catch (error) {
            }
            
          }
        },
      });
    } else {
      const path = 'subPackagesOrder/cancel/cancellationOrder';
      const params = {
        user_id: row.user_id,
        item_hotel_no: row.itemHotelNo,
        real_pay_price: row.realPayPrice,
        isShow: true,
        orderId: row.orderNo
      }
      this.toYqz(path, params)
    }
  }

  toYqz = async (path, params) => {
    wxApi.showLoading({
      title: '正为你跳转',
    });
    wxApi.hideLoading();
    toJumpYQZ(path, params);
  };

  toHotelDetail = ({ itemHotelNo }) => {
    const { longtitude, latitude } = this.props.gps;
    const query = {
      lat: latitude,
      lng: longtitude,
      start_date: dateUtil.getDay(0),
      end_date: dateUtil.getDay(1),
      id: itemHotelNo
    };
    toJumpYQZ('hotel/hotelDetail/hotelDetail', query)
  }

  render() {
    const { orderInfo,tmpStyle } = this.props;
    return (
      <View className='' >
        {this.isShowControls(orderInfo) && (
          <View className='controls'>
            {orderInfo.orderStatus === 10 && (<View className='controls-btn' onClick={() => { this.rePay(orderInfo) }} style={_safe_style_(`color: #fff; background: ${tmpStyle.btnColor}; border: none;`)}> 立即支付 </View>)}
            {orderInfo.orderStatus === 10 && (<View className='controls-btn' onClick={() => { this.toCancellationOrder(orderInfo)}}> 取消 </View>)}
            {orderInfo.orderStatus === 30 && (<View className='controls-btn' onClick={() => { this.toCancellationOrder(orderInfo) }}> 取消订单 </View>)}
            {orderInfo.orderStatus !== 10 && (<View className='controls-btn' onClick={() => { this.toHotelDetail(orderInfo) }} > 再次预订 </View>)}
          </View>
        ) }
      </View>
    );
  }
}

export default HotelOperation;