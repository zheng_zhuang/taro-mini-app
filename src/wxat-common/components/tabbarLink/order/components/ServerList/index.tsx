import { View } from '@tarojs/components';
import React from 'react';
import OrderList from '@/wxat-common/components/tabbarLink/serviceorder-list/index';
import './index.scss';

class OrderList1 extends React.Component {

  onLoad() {
  }

  render() {
    return (
      <View
        data-fixme="03 add view wrapper. need more test"
        data-scoped="wk-mplo-OrderList"
        className="wk-mplo-OrderList"
      >
        <OrderList fromPage="serveOrder"></OrderList>
      </View>
    );
  }
}

export default OrderList1;
