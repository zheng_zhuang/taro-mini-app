import { View } from '@tarojs/components';
import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import "./tab.scss";

interface ComponentProps {
  fnStatusType: Function,
  tmpStyle: Record<string, any>;
  currentIndex: number;
}
interface ComponentState {
  activeIndex: number;
  tabList: Record<string, any>;
}

class TabHead extends React.Component<ComponentProps, ComponentState> {
  state = {
    activeIndex: 0,
    tabList: ['客房', '商城', '服务']
  };

  handlTab(index) {
    this.props.fnStatusType(index)
  }

  render() {
    const { tabList } = this.state;
    const { tmpStyle, currentIndex } = this.props;
    return (
      <View >
        <View className='tab-head'>
          {
            tabList.map((item, index) => {
              return (
                <View className={`tab-item ${index === currentIndex ? 'active' : ''}`} style={_safe_style_(`color: ${index === currentIndex ? tmpStyle.btnColor:''}`)} key={index} onClick={() =>this.handlTab(index)}>
                  {item}
                  {index === currentIndex ? <View className='line' style={_safe_style_(`background: ${index === currentIndex ? tmpStyle.btnColor : ''}`)}></View>:'' }
                </View>
              )
            })
          }
        </View>
      </View>
    );
  }
}

export default TabHead;