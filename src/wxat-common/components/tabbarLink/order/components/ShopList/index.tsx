// @externalClassesConvered(AnimatDialog)
// @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_, PageStyle } from '@/wxat-common/utils/platform';
import React, { ComponentClass } from 'react';
import { View, Image, Text } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import { NOOP_ARRAY } from '@/wxat-common/utils/noop';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import api from '@/wxat-common/api/index';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import constants from '@/wxat-common/constants/index';
import deliveryWay from '@/wxat-common/constants/delivery-way';
import Empty from '@/wxat-common/components/empty/empty';
import Error from '@/wxat-common/components/error/error';
import errorCodeFilter from '@/wxat-common/constants/error-code-filter';
import filters from '@/wxat-common/utils/money.wxs';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum';
import industryEnum from '@/wxat-common/constants/industryEnum';
import LoadMore from '@/wxat-common/components/load-more/load-more';
import mineConfig from '@/wxat-common/constants/mineConfig';
import orderWxs from '@/wxat-common/utils/order.wxs';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum';
import pay from '@/wxat-common/utils/pay';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import subscribeEnum from "@/wxat-common/constants/subscribeEnum";
import subscribeMsg from "@/wxat-common/utils/subscribe-msg";
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl';
import IosPayLimitDialog from '@/wxat-common/components/iospay-limit-dialog/index'
import './index.scss';

const commonImg = cdnResConfig.common;
const ORDER_STATUS = constants.order.orderStatus;
const loadMoreStatus = constants.order.loadMoreStatus;
const PAY_CHANNEL = constants.order.payChannel;
const CARD_TYPE = constants.card.type;
const COMMENT_Status = constants.comment.sellerReplyStatus;
const freightType = constants.goods.freightType;
const timeCardBg = getStaticImgUrl.goods.cardMinCount_png; // 次数卡背景图片
const chargeCardBg = getStaticImgUrl.goods.rechargeCard_png;
// 充值卡背景图片
const coupon = CARD_TYPE.coupon;
const bgGroupFullImg = cdnResConfig.group.bgGroupFullImg;

interface PageStateProps {
  isOpenInvoice: boolean;
  industry: string;
  style?: React.CSSProperties;
  iosSetting: any
}

interface PageDispatchProps { }

interface PageOwnProps { }

interface PageState {
  orderTabsName: {}; // 订单切换tab类型名称
  statusType: Array<Record<string, any>>; // 订单类型
  statusValue: Array<Record<string, any>>; // 订单类型状态码数组
  currentType: Record<string, any>; // 当前订单列表类型
  orderList: Array<Record<string, any>>; // 订单列表
  orderNo: string; // 订单号
  error: boolean; // 是否显示错误页面
  loadMoreStatus: number; // 更多状态
  tmpStyle: Record<string, any>; // 模板配置样式
  timeCardBg: string, // 次数卡背景图片
  chargeCardBg: string, // 充值卡背景图片
  orderTabConfig: object,
  memberError: boolean // 是否显示会员未登录页面
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface OrderList {
  props: IProps;
}

const mapStateToProps = (state) => ({
  isOpenInvoice: state.base.appInfo ? state.base.appInfo.useInvoice || false : false,
  industry: state.globalData.industry,
  mineConfig: state.globalData.mineConfig ? state.globalData.mineConfig.find((item) => {
    return item.id === 'mineOrderModule';
  }) : null,
  iosSetting: state.globalData.iosSetting,
});

/**
 * 订单列表
 */
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
//
class OrderList extends React.Component {
  static defaultProps = {
    isOpenInvoice: false,
    industry: '',
  };

  iosPayLimitDialog = React.createRef<any>()
  state = {
    orderTabsName: {
      after_sale: "售后",
      all: "全部",
      wait_pay: "待支付",
      wait_receive: "待收货",
      wait_shipping: "待发货",
      yqz_hotel_order: "订房订单",
    }, // 订单切换tab类型名称
    statusType: [], // 订单类型
    statusValue: ORDER_STATUS.valueList, // 订单类型状态码数组
    currentType: ORDER_STATUS.labelList[0], // 当前订单列表类型
    orderList: [], // 订单列表
    orderNo: '', // 订单号
    error: false, // 是否显示错误页面
    loadMoreStatus: loadMoreStatus.HIDE, // 更多状态
    tmpStyle: {}, // 模板配置样式
    timeCardBg: getStaticImgUrl.goods.cardMinCount_png, // 次数卡背景图片
    chargeCardBg: getStaticImgUrl.goods.rechargeCard_png, // 充值卡背景图片
    orderTabConfig: null,
    memberError: false // 是否显示会员未登录页面
  };

  pageNo = 1; // 当前列表页面数，用于加载更多
  hasMore = true; // 是否允许加载更多
  RefCPMT: Taro.RefObject<any> = React.createRef();

  componentDidMount() {
    this.initOrderTabBar()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!!nextProps.mineConfig && !this.props.mineConfig) {
      // fixme 由于H5刷新，再获取模板之前渲染页面
      this.initOrderTabBar()
    }
  }

  // 虚拟商品 - ios 获取 url
  fetchIosVirtualPayUrl = (orderNo) => {
    wxApi.request({
      url: api.order.payOrder,
      method: 'GET',
      loading: true,
      data: {
        orderNo: orderNo,
        payChannel: 1,// 微信支付
        payTradeType: 4
      }
    }).then((res) => {
      // console.log('fetchIosVirtualPayUrl', orderNo, res)
      if (res.success && res.data) {
        const { payUrl = '' } = res.data
        if (payUrl) {
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/payH5-submit-success/index',
            data: {
              H5PayUrl: encodeURIComponent(payUrl),
            }
          })
        } else {
          wxApi.showToast({
            title: `支付链接获取失败`
          })
        }
      } else {
        wxApi.showToast({
          title: res.errorMessage
        })
      }
    })
  }

  initOrderTabBar() {
    // fixme 由于组件里无法根据this.$router.params拿到参数，需要通过currentPage的方式获取options
    const { params } = Taro.getCurrentInstance().router
    // const { tabType, config } = params;
    // const orderTabs = ['wait_pay', 'wait_shipping', 'wait_receive', 'after_sale', 'all'];
    const statusType = [
      { value: "all", label: "全部" },
      { value: "wait_pay", label: "待付款" },
      { value: "wait_shipping", label: "待发货" },
      { value: "wait_receive", label: "待收货" },
      { value: "after_sale", label: "售后" }]
    const currentType = statusType[0];
    this.setState(
      {
        currentType,
        statusType,
      },
      () => {
        this.listOrders()
      }

    );
    this.getTemplateStyle(); // 获取模板配置
  }

  componentDidShow() {
    this.onPullDownRefresh();
  }

  componentDidHide() { }

  // 向下刷新
  onPullDownRefresh = () => {
    this.pageNo = 1;
    this.hasMore = true;
    this.listOrders(true);
  };

  // 向上拉伸刷新
  onReachBottom = () => {
    // 判断是否允许加载更多
    if (this.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      });

      this.listOrders(false);
    }
  };

  // 加载更多
  onRetryLoadMore = () => {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    });

    this.listOrders(false);
  };

  /**
   * 查询订单列表
   * @param {*} fromPullDown 是否为下拉刷新，即是否获取第一页的数据
   */
  listOrders = (fromPullDown?: boolean) => {
    this.getListOrdersRequest()
      .then((res) => {
        // 判断是否为下拉刷新，即是否获取第一页的数据
        if (fromPullDown) {
          wxApi.stopPullDownRefresh(); // 停止页面下拉刷新，隐藏页面顶部下来loading框
        }

        const dataList = res.data || [];
        this.assembleOrderList(dataList); // 调配订单操作按钮
        const orderList = this.isLoadMoreRequest() && !fromPullDown ? this.state.orderList.concat(dataList) : dataList;
        this.hasMore = orderList.length < res.totalCount;
        this.pageNo = this.hasMore ? this.pageNo + 1 : this.pageNo; // 当前列表页面数+1，用于加载下一页

        this.setState({
          error: false, // 是否显示错误页面
          orderList: this.calcCount(orderList), // 订单列表
          loadMoreStatus: loadMoreStatus.HIDE,
        });
      })
      .catch((error) => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else if (error.data.errorMessage == '尚未注册成会员') {
          this.setState({
            memberError: true, // 是否显示会员未登录页面
          });
        } else {
          this.setState({
            error: true, // 是否显示错误页面
          });
        }
      });
  };

  // 查询配送进度
  sreach = (order, e) => {
    e.stopPropagation();
    wxApi
      .request({
        url: api.order.schedule,
        loading: true,
        data: {
          orderId: order.orderNo,
        },
      })
      .then((res) => {
        if (res.data) {
          protectedMailBox.send('wxat-common/pages/receipt/index', 'receiptURL', res.data);
          wxApi.$navigateTo({
            url: '/wxat-common/pages/receipt/index',
            data: {
              title: '进度查询',
            },
          });
        }
      });
  };

  /**
   * 获取订单列表
   */
  getListOrdersRequest = () => {
    let requestUrl = api.order.list; // 订单列表查询接口

    // 接口请求参数
    let params = {
      pageNo: this.pageNo,
      pageSize: 10,
      // storeId: state.base.currentStore.id,
    };
    const currentType = this.state.currentType?.value;
    if (currentType === 'after_sale') {
      // 售后订单
      requestUrl = api.order.refundList; // 售后订单列表查询接口
      params.itemTypeList = '1,37';
    } else {
      const theStatus = this.state.statusValue.find((item) => {
        return item.value === currentType;
      });
      if (theStatus && theStatus.label) {
        theStatus.label.forEach((item, index) => {
          params = {
            ...params,
            ...{
              ['orderStatusList[' + index + ']']: item,
            },
          };
        });
      } else {
        params = {
          ...params,
          ...{
            'excludeOrderStatusList[0]': ORDER_STATUS.cancel,
          },
        };
      }
    }

    return wxApi.request({
      url: requestUrl,
      loading: true,
      data: params,
    });
  };

  // 计算单个订单商品总量
  calcCount = (orderList) => {
    return (orderList || NOOP_ARRAY).map((item) => {
      let count = 0;
      if (item.itemList) {
        for (const i of item.itemList) {
          count += i.itemCount;
        }
        return {
          ...item,
          count,
        };
      }
      return item;
    });
  };

  /**
   * 调配订单操作按钮
   * @param {*} orderList 订单列表
   */
  assembleOrderList = (orderList) => {
    if (orderList && orderList.length) {
      const { industry } = this.props;
      orderList.forEach((item) => {
        // 显示评价按钮
        Object.assign(item, {
          showCommentBtn:
            item.orderStatus === ORDER_STATUS.success &&
            industry !== industryEnum.type.beauty.value &&
            item.itemType !== goodsTypeEnum.CARDPACK.value, // 不是代金卡包订单
        });
        // 显示修改收货地址按钮
        Object.assign(item, {
          showUpdateAddressBtn:
            item.itemType === goodsTypeEnum.PRODUCT.value &&
            item.expressType == deliveryWay.EXPRESS.value && // 快递订单才能修改收货地址
            (item.orderStatus === ORDER_STATUS.waitPay ||
              item.orderStatus === ORDER_STATUS.paid ||
              item.orderStatus === ORDER_STATUS.confirm),
        });

        // 显示取消订单按钮
        Object.assign(item, {
          showCancelBtn: item.orderStatus === ORDER_STATUS.waitPay,
        });

        // 显示付款按钮
        Object.assign(item, {
          showPayBtn: item.orderStatus === ORDER_STATUS.waitPay,
        });

        // 显示确认收货及查看物流按钮
        Object.assign(item, {
          showConfirmDeliveredBtn: item.orderStatus === ORDER_STATUS.shipping,
        });

        // 显示申请开票按钮
        Object.assign(item, {
          showReceiptBtn:
            this.props.isOpenInvoice &&
            !item.hasInvoice &&
            item.orderStatus === ORDER_STATUS.success &&
            item.itemType === goodsTypeEnum.PRODUCT.value,
        });

        // 显示订单维度按钮操作区域
        Object.assign(item, {
          showOperationBtnBox:
            item.showUpdateAddressBtn ||
            item.showCancelBtn ||
            item.showPayBtn ||
            item.showConfirmDeliveredBtn ||
            item.showReceiptBtn,
        });

        // 待医院审核改成：处方审核中
        if (item.orderStatus === 92) {
          Object.assign(item, {
            orderStatusDesc: '处方审核中',
          });
        }
        // 待人工审核改成：卖家确认中
        if (item.orderStatus === 93) {
          Object.assign(item, {
            orderStatusDesc: '卖家确认中',
          });
        }
      });
    }
  };

  // 是否为加载更多请求
  isLoadMoreRequest = () => {
    return this.pageNo > 1;
  };

  /**
   * 点击tab切换，获取订单列表
   * @param e
   */
  handleListOrders = (e) => {
    const curType = e.currentTarget.dataset.item;
    this.pageNo = 1;
    this.hasMore = true;
    this.setState(
      {
        currentType: curType,
        orderList: [], // 切换tab，重新查询订单列表之前，先清空原有的订单列表
      },
      () => {
        this.listOrders(false);
      }
    );
  };

  /**
   * 点击进入订单详情
   * @param {*} e
   */
  handleGoToDetail = (e) => {
    const order = e.currentTarget.dataset.order;
    const index = e.currentTarget.dataset.index;
    const params = {
      orderNo: order.orderNo,
    };

    wxApi.$navigateTo({
      url: pageLinkEnum.orderPkg.orderDetail,
      data: params,
    });
  };

  /**
   * 评价功能操作
   * @param {*} e
   */
  handleEvaluation = (item, order, e) => {
    e.stopPropagation();
    item.itemType = order.itemType;
    const params = {
      orderNo: order.orderNo,
      item: JSON.stringify(item),
    };

    this.pageNo = 1;
    wxApi.$navigateTo({
      url: '/wxat-common/pages/evaluation/index',
      data: params,
    });
  };

  /**
   * 点击修改订单收货地址
   * @param e
   */
  handleUpdateAddress(order, e) {
    e.stopPropagation();
    const orderNo = order.orderNo || null;
    protectedMailBox.send('wxat-common/pages/address/update/index', 'orderNo', orderNo);
    wxApi.$navigateTo({
      url: '/wxat-common/pages/address/update/index',
    });
  }

  /**
   * 点击取消订单
   * @param e
   */
  handleCancelOrder = (order, e) => {
    e.stopPropagation();
    const orderNo = order.orderNo || null;
    wxApi.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.cancel,
              loading: {
                title: '正在取消订单',
              },

              data: {
                orderNo,
              },
            })
            .then((res) => {
              this.pageNo = 1;
              return this.getListOrdersRequest();
            })
            .then((res) => {
              const orderList = res.data || [];
              this.assembleOrderList(orderList); // 调配订单操作按钮
              this.setState({
                orderList: this.calcCount(orderList),
              });
            })
            .catch((error) => { });
        }
      },
    });
  };

  /**
   * 点击付款
   * @param e
   */
  handlePay = (order, e) => {
    e.stopPropagation();
    const itemList = order.itemList || null;
    const orderNo = order.orderNo || null;
    const itemType = order.itemType || null;

    const that = this;
    const _iosSetting = this.props.iosSetting
    if (_iosSetting?.payType && itemType == goodsTypeEnum.VIRTUAL_GOODS.value) {
      if (_iosSetting.payType === 2) {
        this.fetchIosVirtualPayUrl(orderNo)
        return
      } else if (_iosSetting.payType === 3) {
        this.iosPayLimitDialog.current.showIospayLimitDialog();
        return
      }
    }
    if (itemType == CARD_TYPE.charge || itemType === CARD_TYPE.gift) {
      pay.payByOrder(orderNo, PAY_CHANNEL.WX_PAY, {
        wxPaySuccess() {
          that.pageNo = 1;
          that.listOrders(true);
        },
        fail(error) {
          console.log(error);
        },
      });
    } else {
      let payChannel = '';
      if (itemType == CARD_TYPE.cardPack || itemType === CARD_TYPE.equityCard) {
        payChannel = PAY_CHANNEL.WX_PAY;
      }
      subscribeMsg.sendMessage([subscribeEnum.ORDER_SEND.value]).then(() => {
        pay.payByOrder(orderNo, payChannel, {
          success(res) {
            that.pageNo = 1;
            that.listOrders(true);
          },
          fail(res) {
            if (res.data.errorCode === errorCodeFilter.groupFullCode.value) {
              // 打开参团人数已满提示对话弹框
              that.handleGroupFullDialog();
            } else if (res.data.errorCode === errorCodeFilter.seckillRemainLackCode.value) {
              // 秒杀活动商品剩余数量不足提示
              wxApi.showToast({
                title: '活动商品剩余数量不足',
                icon: 'none',
              });

              // 定时刷新订单列表
              const timeOut = setTimeout(() => {
                if (timeOut) {
                  clearTimeout(timeOut);
                }
                that.pageNo = 1;
                that.listOrders(true); // 刷新订单列表
              }, 1500);

            } else {
              wxApi.showToast({
                title: res.data.errorMessage,
                icon: 'none',
              });

              setTimeout(() => {
                that.pageNo = 1;
                that.listOrders(true); // 刷新订单列表
              }, 1500);
            }
          },
          wxPaySuccess() {
            that.pageNo = 1;
            that.listOrders(true);
          },
          originParams: {
            itemDTOList: itemList, // 埋点上报需要商品像信息
          },
        });
      })
    }
  };

  /**
   * 打开参团人数已满提示对话弹框
   */
  handleGroupFullDialog = () => {
    if (this.RefCPMT.current && this.RefCPMT.current.show) {
      this.RefCPMT.current.show({ scale: 1 });
    }
  };

  /**
   * 关闭参团人数已满提示对话弹框
   */
  handleCloseGroupFullDialog = () => {
    if (this.RefCPMT.current && this.RefCPMT.current.hide) {
      this.RefCPMT.current.hide();
    }

    this.pageNo = 1;
    this.listOrders(true); // 刷新订单列表
  };

  /**
   * 点击发起拼团，跳转拼团列表页面
   */
  handleGoToGroupList = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/group/list/index',
    });
  };

  /**
   * 查看物流
   * @param {*} e
   */
  handleLogistics = (order, e) => {
    e.stopPropagation();
    const orderNo = order.orderNo || null;
    protectedMailBox.send('wxat-common/pages/logistics/order-logistics/index', 'orderNo', orderNo);
    wxApi.$navigateTo({
      url: '/wxat-common/pages/logistics/order-logistics/index',
    });
  };

  /**
   * 确认收货
   */
  handleConfirmDelivery = (e) => {
    const order = e.currentTarget.dataset.order;
    const orderNo = order.orderNo || null;
    wxApi.showModal({
      title: '请确定已收到货物',
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.order.confirmDelivery,
              loading: {
                title: '正在通知商家',
              },

              data: {
                orderNo,
              },
            })
            .then((res) => {
              this.pageNo = 1;
              return this.getListOrdersRequest();
            })
            .then((res) => {
              const orderList = res.data || [];
              this.assembleOrderList(orderList); // 调配订单操作按钮
              this.setState({
                orderList: this.calcCount(orderList),
              });
            })
            .catch((error) => { });
        }
      },
    });
  };

  /**
   * 点击申请开票
   * @param {*} e
   */
  handleReceipt = (order, e) => {
    e.stopPropagation();
    const orderNo = order.orderNo || null;
    wxApi
      .request({
        url: api.getReceipt,
        data: {
          orderNo: orderNo,
        },
      })
      .then((res) => {
        const receiptURL = res.data;
        protectedMailBox.send('wxat-common/pages/receipt/index', 'receiptURL', receiptURL);
        wxApi.$navigateTo({
          url: '/wxat-common/pages/receipt/index',
          data: orderNo,
        });
      });
  };

  // 获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { currentType, tmpStyle, statusType, orderTabsName, error, orderList, loadMoreStatus, memberError } = this.state;

    const { isOpenInvoice, industry, iosSetting } = this.props;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-cto-OrderList'
        className='wk-cto-OrderList new-order-list'
        style={_safe_style_(PageStyle(this.props.style))}
      >
        <View className='order-list-container'>

          <View className='status-box'>
            {statusType.map((item, index) => {
              return (
                <View
                  onClick={_fixme_with_dataset_(this.handleListOrders, { item: item })}
                  className={'status-label ' + (item.value === currentType.value ? 'active' : '')}
                  key={index}
                  style={_safe_style_(
                    item.value === currentType.value
                      ? `color:${tmpStyle.btnColor}; background:none;`
                      : ''
                  )}
                >
                  {orderTabsName[item.value] || item.label}
                  <View className='active-bg' style={_safe_style_(
                    item.value === currentType.value
                      ? 'background:' + tmpStyle.btnColor
                      : ''
                  )}></View>
                </View>
              );
            })}
          </View>
          <View className='order-list-body'>
            {/*  错误页面提示  */}
            {error ? (
              <Error></Error>
            ) : memberError ? (
              <Empty message="尚未注册成会员"></Empty>
            ) : !!orderList && orderList.length == 0 ? (
              <Empty message="暂无订单"></Empty>
            ) : (
              <View className={((!(orderList && orderList.length)) ? 'global-hidden ' : '') + 'order-list'}>
                {orderList.map((order, idx) => {
                  return (
                    <View className='tab' key={order.orderNo}>
                      <View className='order-info'>
                        <View className='orderNo'>{'订单号: ' + order.orderNo}</View>
                        {!!order.orderTime && <View className='orderTime'>{order.orderTime}</View>}
                      </View>
                      {/*  商品信息  */}
                      <View className='item-box'>
                        {order.itemList.map((item, index) => {
                          return (
                            <View
                              className='item-tab'
                              onClick={_fixme_with_dataset_(this.handleGoToDetail, { order: order, index: idx })}
                              key={index}
                            >
                              <View className='item-info-box'>
                                <View className='img-box'>
                                  {item.thumbnail ? (
                                    <Image
                                      src={
                                        order.itemType === goodsTypeEnum.EQUITY_CARD.value
                                          ? (item.thumbnail || '').split(',')[0]
                                          : item.thumbnail
                                      }
                                      className='item-img'
                                    ></Image>
                                  ) : (
                                    <Image
                                      src={
                                        order.itemType === 4
                                          ? chargeCardBg
                                          : order.itemType === coupon
                                            ? timeCardBg
                                            : ''
                                      }
                                      className='item-img'
                                    ></Image>
                                  )}

                                  {/*  预售标志  */}
                                  {!!order.preSell && <View className='pre-sale-label'>预售</View>}
                                </View>
                                {/*  商品详情  */}
                                <View className='item-info'>
                                  <View>
                                    <View className='item-name limit-line line-2'>
                                      {item.drugType == 1 && (
                                        <Text
                                          className='item-drug'
                                          style={{
                                            backgroundColor: tmpStyle.btnColor,
                                          }}
                                        >
                                          处方药
                                        </Text>
                                      )}

                                      {item.itemName ? item.itemName : ''}
                                    </View>
                                    {/*  商品规格  */}
                                    {!!item.itemAttribute && (
                                      <View className='item-attribute'>{item.itemAttribute || ''}</View>
                                    )}
                                  </View>
                                  <View>
                                    <View className='item-count-price'>
                                      {order.itemType !== 43 && (
                                        <View className="item-price">
                                          {'￥' + filters.moneyFilter(item.salePrice, true)}
                                        </View>
                                      )}

                                      {order.itemType !== 43 && (
                                        <View className="item-count">{'x ' + item.itemCount}</View>
                                      )}
                                    </View>

                                    {/*  商品数量及单价  */}
                                    {/*  定金  */}
                                    {!!order.frontMoneyItemOrder && (
                                      <View className='front-money' style={_safe_style_('color: ' + tmpStyle.btnColor)}>
                                        {'￥' + filters.moneyFilter(order.payFee, true) + '（定金）'}
                                      </View>
                                    )}
                                  </View>
                                </View>
                                {/*  订单状态  */}
                                {index === 0 && (
                                  <View className='orderStatus-box' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                                    {currentType?.value !== 'after_sale' && order.orderStatusDesc ? (
                                      <View className='order-status'>{order.orderStatusDesc}</View>
                                    ) : (
                                      currentType.value === 'after_sale' &&
                                      order.statusDesc && <View className='order-status'>{order.statusDesc}</View>
                                    )}

                                    {/*  普通订单  */}
                                    {/*  售后订单  */}
                                    {/*  同城配送订单  */}
                                    {order.expressType == deliveryWay.CITY_DELIVERY.value &&
                                      order.orderStatus === ORDER_STATUS.distribution ? (
                                      <View
                                        className='order-status'
                                        style={_safe_style_('padding:14rpx 0;')}
                                        onClick={this.sreach.bind(this, order)}
                                      >
                                        进度查询
                                      </View>
                                    ) : order.groupLeaderPromFee || order.groupPromFee ? (
                                      <View className='order-status' style={_safe_style_('padding:14rpx 0;')}>
                                        拼团订单
                                      </View>
                                    ) : (
                                      order.itemType === goodsTypeEnum.EQUITY_CARD.value && (
                                        <View className='order-status' style={_safe_style_('padding:14rpx 0;')}>
                                          权益卡订单
                                        </View>
                                      )
                                    )}

                                    {/*  拼团订单  */}
                                    {/*  已开票  */}
                                    {!!(
                                      isOpenInvoice &&
                                      order.hasInvoice &&
                                      order.orderStatus === ORDER_STATUS.success
                                    ) && <View className='order-status'>已开票</View>}
                                  </View>
                                )}
                              </View>
                              <View style={_safe_style_('display: flex;justify-content: flex-end;')}>
                                {!!(order.showCommentBtn && item.comment === COMMENT_Status.NOREPLY) && (
                                  <View className='comment-box'>
                                    <View
                                      className='btn btn-light'
                                      onClick={this.handleEvaluation.bind(this, item, order)}
                                      style={_safe_style_(
                                        'border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor
                                      )}
                                    >
                                      评价
                                    </View>
                                  </View>
                                )}
                              </View>
                            </View>
                          );
                        })}
                      </View>
                      {/*  商品总价信息  */}
                      <View className='total-price'>
                        {'共' +
                          order.count +
                          '件商品 合计：￥' +
                          filters.moneyFilter(order.payFee, true) +
                          '(' +
                          orderWxs.getFreightLabel(order.itemList, order.expressType, deliveryWay, freightType) +
                          ')'}
                      </View>

                      {/*  预计发货时间  */}
                      {!!(order.expectShippingDate && order.orderStatus === ORDER_STATUS.confirm) && (
                        <View className='expect'>{'预计发货时间：' + order.expectShippingDate}</View>
                      )}

                      {/*  订单维度按钮操作  */}
                      {!!order.showOperationBtnBox && (
                        <View className='operation-btn-box'>
                          {!!order.showUpdateAddressBtn && (
                            <View className='btn' onClick={this.handleUpdateAddress.bind(this, order)}>
                              修改收货地址
                            </View>
                          )}

                          {!!order.showCancelBtn && (
                            <View className='btn' onClick={this.handleCancelOrder.bind(this, order)}>
                              取消订单
                            </View>
                          )}

                          {!!order.showPayBtn && (
                            <View
                              className='btn btn-light'
                              onClick={this.handlePay.bind(this, order)}
                              style={_safe_style_(
                                'border-color:' +
                                tmpStyle.btnColor +
                                ';color:#fff; background-color:' +
                                tmpStyle.btnColor
                              )}
                            >
                              {order.itemType === goodsTypeEnum.ESTATE.value ? '立即认筹' : iosSetting?.payType && order.itemType == goodsTypeEnum.VIRTUAL_GOODS.value && iosSetting?.buttonCopywriting ? iosSetting?.buttonCopywriting : '付款'}
                            </View>
                          )}

                          {!!order.showConfirmDeliveredBtn && (
                            <View
                              className='btn btn-light'
                              onClick={this.handleLogistics.bind(this, order)}
                              style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor)}
                            >
                              查看物流
                            </View>
                          )}

                          {!!order.showConfirmDeliveredBtn && (
                            <View
                              className='btn btn-light'
                              onClick={_fixme_with_dataset_(this.handleConfirmDelivery, { order: order })}
                              style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor)}
                            >
                              确认收货
                            </View>
                          )}

                          {!!order.showReceiptBtn && (
                            <View
                              className='btn btn-light'
                              onClick={this.handleReceipt.bind(this, order)}
                              style={_safe_style_('border-color:' + tmpStyle.btnColor + ';color:' + tmpStyle.btnColor)}
                            >
                              申请开票
                            </View>
                          )}

                          {/* ------取件码----- */}
                          {!!order.robotFetchCode && (
                            <View className="pickUpCode">
                              取件码：<Text>{order.robotFetchCode}</Text>
                            </View>
                          )}
                        </View>
                      )}
                    </View>
                  );
                })}
              </View>
            )}

            {/*  暂无订单提示  */}
            {/*  订单列表  */}
            {/*  加载更多  */}
            <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
          </View>
        </View>
        {/*  参团人数已满提示对话弹框  */}
        <AnimatDialog ref={this.RefCPMT} animClass='group-full-dialog'>
          <View className='group-full'>
            <Image className='bg-group-full' src={bgGroupFullImg}></Image>
            <View className='group-full-content'>
              <View className='group-full-title'>参团人数已满</View>
              <View className='group-full-text'>快去发起自己的拼团吧</View>
            </View>
            <View className='btn-group-full' onClick={this.handleGoToGroupList}>
              发起拼团
            </View>
          </View>
          <Image
            className="icon-group-full-close"
            src={getStaticImgUrl.images.close_png}
            onClick={this.handleCloseGroupFullDialog}
          ></Image>
        </AnimatDialog>
        <IosPayLimitDialog ref={this.iosPayLimitDialog} tipsCopywriting={iosSetting?.tipsCopywriting}></IosPayLimitDialog>
      </View>
    );
  }
}

export default OrderList as ComponentClass<PageOwnProps, PageState>;
