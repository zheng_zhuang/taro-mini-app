import React, { Component, ComponentClass } from 'react';
import PropTypes from 'prop-types';
import { View } from '@tarojs/components';
import { _safe_style_, _fixme_with_dataset_, PageStyle } from '@/wxat-common/utils/platform';
import "./tab.scss";

interface PageState {
  statusTab: Array<Record<string, any>>;
  statusType:any
}

class Tab extends Component {
  state = {
    statusType: '',
    statusTab: [
      { name: '全部',status:''},
      { name: '待支付',status:10},
      { name: '待确认',status:30},
      { name: '已确认',status:40},
    ]
    
  }

  handlTab(val,index) {
    this.setState({
      statusType: val.status
    })
    this.props.statusTypeProps(val)
  }
  
  render() {
    const { statusType, statusTab } = this.state
    const { tmpStyle } = this.props;
    return (
      <View>
        <View className='status-tab'>
          {
            statusTab.map((item,index) => {
              return(
                <View 
                className={`status-Tab-item ${item.status === statusType?'active':''}`} 
                  style={_safe_style_(
                    item.status === statusType
                      ? `color:${tmpStyle.btnColor}; background:none;`
                      : ''
                  )}
                key={index} 
                onClick={()=>this.handlTab(item)}
                >
                  {item.name}
                <View className='active-bg' style={_safe_style_(
                  item.status === statusType
                    ? 'background:' + tmpStyle.btnColor
                    : ''
                )}></View>
                </View>
              )
            })
          }
        </View>
      </View>
    );
  }
}

export default Tab as ComponentClass<PageState,any>;