import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React from 'react';
import TabHead from "./components/TabHead";
import HotelList from "./components/HotelList";
import ShopList from "./components/ShopList";
import ServiceList from '@/wxat-common/components/tabbarLink/serviceorder-list/index';
import template from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';

interface ComponentProps {
}
interface ComponentState {
  currentIndex: number;
  tmpStyle: Record<string, any>;
}
class OrderPage extends React.Component<ComponentProps, ComponentState> {
  state = {
    currentIndex:0,
    tmpStyle: {}, // 模板配置样式
  }

  componentDidMount() {
    this.getTemplateStyle()
    const { params } = Taro.getCurrentInstance().router
    const { tabType } = params;
    const indexs = {
      yqz_hotel_order: 0,
      mall_order: 1,
      service_order: 2
    }
    this.setState({
      currentIndex: indexs[tabType] || 0
    })
  }

  fnStatusType = (index) => {
    this.setState({
      currentIndex: index
    })
    
  }

  // 获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { currentIndex, tmpStyle } = this.state;
    
    return (
      <View>
        <TabHead fnStatusType={this.fnStatusType} currentIndex={currentIndex} tmpStyle={tmpStyle}></TabHead>
        
        {
          currentIndex === 0 && <HotelList tmpStyle={tmpStyle} />
        }
        {
          currentIndex === 1 && <ShopList tmpStyle={tmpStyle} />
        }
        {
          currentIndex === 2 && <ServiceList tabClass />
        }
      </View>
    );
  }
}

export default OrderPage;