import { $getRouter } from '@/wk-taro-platform';
import React from 'react'; // @scoped
import Taro from '@tarojs/taro';
import { View, Canvas } from '@tarojs/components';
import hoc from '@/hoc';
import { connect } from 'react-redux';

import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import template from '@/wxat-common/utils/template.js';
import distributionEnum from '@/wxat-common/constants/distributionEnum.js';
import utils from '@/wxat-common/utils/util.js';
import shareUtil from '@/wxat-common/utils/share.js';
import InternalList from '@/wxat-common/components/tabbarLink/internal-list';
import ShareDialog from '@/wxat-common/components/internal-dialog/share-dialog';
import report from '@/sdks/buried/report/index';
import screen from '@/wxat-common/utils/screen';
import './index.scss';

const mapStateToProps = (state) => ({
  allState: state,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc

class PurchCenter extends React.Component {
  $router = $getRouter();
  /**
   * 页面的初始数据
   */
  state = {
    userId: null,
    friendUserId: null,
    num: 1,
    distributorId: null,
    internalId: null,
    time: 0,
    distImgUrl: null,
    avatarImgUrl: null,
    tabbarHeight: screen.tabbarHeight,
    shareDialogVisible: false,
  };

  internalListRef = React.createRef<InternalList>();

  //用户是否注册过
  get isUserRegistered() {
    return !!this.props.allState.base.registered;
  } /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    const options = this.$router.params;
    if (!!options.scene) {
      const scene = '?' + decodeURIComponent(options.scene);
      const itemParam = utils.getQueryString(scene, 'No');
      //获取分享内购id
      let distributorId = utils.getQueryString(scene, 'distributorId');
      let userId = '';
      if (itemParam) {
        const itemParamArr = itemParam.split('_');
        userId = itemParamArr[0];
        if (itemParamArr[1]) {
          distributorId = itemParamArr[1];
        }
      }
      this.setState({
        distributorId: distributorId,
        userId: userId,
      });
    }
    if (options.userId) {
      this.setState({
        userId: options.userId,
        avatarImgUrl: options.avatarImgUrl,
      });
    }
    if (!!options.distributorId) {
      const distributorId = options.distributorId;
      if (distributorId) {
        wxApi.setStorageSync('internalId', distributorId);
        this.setState({ distributorId: distributorId });
      }
    }
    if (!!options.distImgUrl) {
      this.setState({
        distImgUrl: options.distImgUrl,
      });
    }
    this.getTemplateStyle();
  }

  componentDidShow() {
    this.componentShow();
  }

  onReachBottom() {
    this.internalListRef.current && this.internalListRef.current.onReachBottom();
  }

  /**
   * 分享小程序功能
   * @returns {{title: string, desc: string, path: string, imageUrl: string, success: success, fail: fail}}
   */
  onShareAppMessage(res) {
    let user = null;
    let item = null;

    if (res.target && res.target.dataset) {
      if (res.target.dataset.item) {
        item = res.target.dataset.item;
      }
    }

    if (res.target && res.target.id === 'power') {
      user = {
        id: this.props.allState.base.userInfo.id,
        avatarImgUrl: this.props.allState.base.userInfo.avatarImgUrl,
      };
    }
    report.share(true);
    let title = '送你亲友内购券，品牌好货尽享底价';
    //内购页面url
    let url = '/sub-packages/distribution-package/pages/internal-purchase/center/index';
    //是否是助力
    if (user) {
      url += '?userId=' + user.id + '&avatarImgUrl=' + user.avatarImgUrl;
    }
    //是否是商品分享
    if (item) {
      url = '/wxat-common/pages/goods-detail/index';
      url += '?itemNo=' + item.itemNo + '&type=28&activityId=' + item.id;
      title = item.name;
    }
    if (this.state.internalId) {
      if (!user && !item) {
        url += '?distributorId=' + this.state.internalId + '&distImgUrl=' + this.state.distImgUrl;
      } else {
        url += '&distributorId=' + this.state.internalId + '&distImgUrl=' + this.state.distImgUrl;
      }
    }
    const path = shareUtil.buildShareUrlPublicArguments({
      url,
      bz: item ? shareUtil.ShareBZs.GOODS_DETAIL : shareUtil.ShareBZs.INTER_PURCHASE_CENTER,
      bzName: `内购专场-${item ? title : '内购券'}`,
    });

    console.log('sharePath => ', path);
    return {
      title: title,
      path,
      fail: function (res) {
        report.share(false);
        // 转发失败
        console.log('转发失败:' + JSON.stringify(res));
      },
    };
  }

  componentShow() {
    if (this.isUserRegistered) {
      //如果分销id不为空绑定之后查询头像，为空直接查询
      if (this.state.distributorId) {
        this.bindCustomerRelation();
      } else {
        this.selectMyDistributor();
      }
    }
    this.internalListRef.current && this.internalListRef.current.onReachBottom();
  }

  handleGetTime = (time) => {
    this.setState({
      time,
    });
  };

  //查询内购链路,绑定关系
  selectMyDistributor() {
    return wxApi
      .request({
        url: api.distribution.selectMyDistributor,
        data: {
          activityType: distributionEnum.activityType.internal,
        },
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            distImgUrl: res.data.avatar,
            distributorId: res.data.id,
          });

          wxApi.setStorageSync('internalId', res.data.id);
        }
        //绑定最近的员工
        this.selectSingle();
      })
      .catch((error) => {
        //绑定最近的员工
        this.selectSingle();
      });
  }

  //查询是否为内购员
  selectSingle() {
    return wxApi
      .request({
        url: api.distribution.selectSingle,
        data: {
          activityType: distributionEnum.activityType.internal,
        },
      })
      .then((res) => {
        if (res.data) {
          wxApi.setStorageSync('internalId', res.data.id);
          if (!this.state.distImgUrl) {
            this.setState({
              distImgUrl: res.data.avatar,
            });
          }
          this.setState({
            internalId: res.data.id,
          });
        } else {
          this.setState({
            internalId: this.state.distributorId,
          });

          wxApi.setStorageSync('internalId', this.state.distributorId);
        }
      })
      .catch((error) => {});
  }

  //设置内购链路
  bindCustomerRelation() {
    return wxApi
      .request({
        url: api.distribution.bindCustomerRelation,
        data: {
          distributorId: this.state.distributorId,
          activityType: distributionEnum.activityType.internal,
        },
      })
      .then((res) => {
        this.selectMyDistributor();
      })
      .catch((error) => {
        this.selectMyDistributor();
      });
  }

  //检验可领取的内购券张数
  checkGain() {
    return wxApi
      .request({
        url: api.internalVoucher.gainAvailableVoucherNum,
        data: {
          type: distributionEnum.GAINTYPE.FRIEND_HELP,
          userId: this.state.userId,
          friendUserId: this.props.allState.base.loginInfo.userId,
        },
      })
      .then((res) => {
        if (res.data) {
          this.setState({
            shareDialogVisible: true,
            num: res.data,
          });
        }
      })
      .catch((error) => {});
  }

  //领取
  handleGet() {
    const params = {
      userId: this.state.userId,
      friendUserId: this.props.allState.base.loginInfo.userId,
      type: distributionEnum.GAINTYPE.FRIEND_HELP,
    };

    return wxApi
      .request({
        url: api.internalVoucher.gainVoucher,
        data: params,
      })
      .then((res) => {
        if (res.data) {
          wxApi.showToast({
            title: '领取成功',
            icon: 'none',
          });

          this.internalListRef.current!.getVoucherNum();
        }
      })
      .catch((error) => {});
  }

  // 保存图片
  handleSavePosterImage(e) {
    this.internalListRef.current!.handleSaveInternalImage(this.state);
  }

  shareDialogShow = () => {
    this.setState({ shareDialogVisible: true });
  };

  handleShareDialogHide = () => {
    this.setState({ shareDialogVisible: false });
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
  }
  render() {
    const { distImgUrl, num, time, avatarImgUrl, shareDialogVisible, tabbarHeight, internalId } = this.state as Record<
      string,
      any
    >;

    return (
      <View data-scoped='wk-wcti-InternalPurchase' className='wk-wcti-InternalPurchase'>
        <InternalList
          ref={this.internalListRef}
          onSave={this.handleSavePosterImage.bind(this)}
          onTime={this.handleGetTime}
          height={tabbarHeight * 2 + 16}
          distImgUrl={distImgUrl}
          internalId={internalId}
        ></InternalList>
        <ShareDialog
          visible={shareDialogVisible}
          num={num}
          time={time}
          avatarImgUrl={avatarImgUrl}
          onConfirmed={this.handleGet}
          onHide={this.handleShareDialogHide}
        ></ShareDialog>
      </View>
    );
  }
}

export default PurchCenter;
