import {_fixme_with_dataset_, _safe_style_} from '@/wk-taro-platform';
import {Block, View, Image, Text, ScrollView} from '@tarojs/components';
import React from 'react';
import filters from '../../../utils/money.wxs.js';
import wxApi from '../../../utils/wxApi';
import api from '../../../api';
import constants from '../../../constants';
import template from '../../../utils/template';
import industryEnum from '../../../constants/industryEnum';
import goodsTypeEnum from '../../../constants/goodsTypeEnum';
import deliveryWay from '../../../constants/delivery-way';
import pay from '@/wxat-common/utils/pay';
import List from '../../list';
import LoadMore from '../../load-more/load-more';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import store from '@/store';
import classNames from 'classnames';
import './index.scss';
import {getFreeLabel} from '@/wxat-common/utils/service'
import { changeOpacotyToRGBA } from '@/wxat-common/utils/util'

const ORDER_STATUS = constants.order.serviceStatus;
const loadMoreStatus = constants.order.loadMoreStatus;

const storeData = store.getState();

export interface ServiceorderListProps {
  fromPage: string;
  tabClass: boolean;
}

class ServiceorderList extends React.Component<ServiceorderListProps> {

  // 设置 props 默认值
  static defaultProps = {
    fromPage: ''
  }

  state = {
    industryEnum, // 行业类型常量
    industry: storeData.globalData.industry, // 行业类型
    orderTabsName: {}, // 订单切换tab类型名称
    statusType: ORDER_STATUS.labelList, // 订单类型
    currentType: ORDER_STATUS.labelList[0], // 当前订单列表类型
    orderList: [], // 订单列表
    error: false, // 是否显示错误页面
    pageNo: 1, // 当前列表页面数，用于加载更多
    hasMore: true, // 是否允许加载更多
    loadMoreStatus: loadMoreStatus.HIDE, // 更多状态
    tmpStyle: {  // 模板配置样式
      btnColor: '',
      bgColor: ''
    },
    isOpenInvoice: false
  }

  // 生命周期函数--监听页面加载
  componentDidMount() {
    this.setState({
      industry: storeData.globalData.industry,
    });
    this.getTemplateStyle(); // 获取模板配置
    this.onPullDownRefresh()
  }

  // 向下刷新
  onPullDownRefresh() {
    this.setState({
      pageNo: 1,
      hasMore: true,
    }, () => {
      this.listOrders(true);
    });
  }

  // 向上拉伸刷新
  onReachBottom() {
    // 判断是否允许加载更多
    if (this.state.hasMore) {
      this.setState({
        loadMoreStatus: loadMoreStatus.LOADING,
      }, () => {
        this.listOrders(false);
      });
    }
  }

  /**
   * 查询订单列表
   * @param {*} fromPullDown 是否为下拉刷新，即是否获取第一页的数据
   */
  listOrders(fromPullDown) {
    this.getListOrdersRequest()
      .then((res) => {
        // 判断是否为下拉刷新，即是否获取第一页的数据
        if (fromPullDown) {
          wxApi.stopPullDownRefresh(); // 停止页面下拉刷新，隐藏页面顶部下来loading框
        }

        const dataList = res.data || [];
        this.assembleOrderList(dataList); // 调配订单操作按钮
        const orderList = this.isLoadMoreRequest() && !fromPullDown ? this.state.orderList.concat(dataList) : dataList;
        const hasMore = orderList.length < res.totalCount ? true : false;
        const pageNo = hasMore ? this.state.pageNo + 1 : this.state.pageNo; // 当前列表页面数+1，用于加载下一页
        orderList.forEach((item) => {
          item.serverPlatformItemList = item.serverPlatformItemList.map((el) => {
            return {
              ...el,
              isOpen: '',
              parseFormExt4: el.formExt4 ? JSON.parse(el.formExt4) : [],
              parseFormExt: el.formExt ? JSON.parse(el.formExt) : [],
            };
          });
        });
        this.setState({
          error: false, // 是否显示错误页面
          orderList, // 订单列表
          pageNo, // 当前列表页面数，用于加载更多
          hasMore, // 是否允许加载更多
          loadMoreStatus: loadMoreStatus.HIDE,
        });

        this.getCount(); // 计算单个订单商品总量
      })
      .catch((error) => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatus.ERROR,
          });
        } else {
          this.setState({
            error: true, // 是否显示错误页面
          });
        }
      });
  }

  // 点击场景预定列表进入详情
  toDetail(e) {
    wxApi.$navigateTo({
      url: '/sub-packages/scene-package/pages/scene-order-detail/index',
      data: {
        orderNo: e,
      },
    });
  }

  // 获取订单列表
  getListOrdersRequest() {
    const requestUrl = api.livingService.getplatformOrderNew; // 订单列表查询接口
    const currentType = this.state.currentType.value;
    // 接口请求参数
    const params = {
      categoryType: 1,
      pageNo: this.state.pageNo,
      pageSize: 10,
      orderStatus: currentType,
      storeId: store.getState().base.currentStore.id || '',
    };

    // console.log('fetch ...', params)
    return wxApi.request({
      url: requestUrl,
      loading: true,
      data: params,
    });
  }

  // 计算单个订单商品总量
  getCount() {
    if (this.state.orderList) {
      this.state.orderList.forEach((items: any) => {
        items.count = 0;
        if (items.itemList) {
          items.itemList.forEach((item) => {
            items.count += item.itemCount;
          });
        }
      });
      this.setState({
        orderList: this.state.orderList,
      });
    }
  }

  /**
   * 调配订单操作按钮
   * @param {*} orderList 订单列表
   */
  assembleOrderList(orderList) {
    if (orderList && orderList.length) {
      orderList.forEach((item) => {
        // 显示评价按钮
        Object.assign(item, {
          showCommentBtn:
            item.orderStatus === ORDER_STATUS.success &&
            this.state.industry !== this.state.industryEnum.type.beauty.value &&
            item.itemType !== goodsTypeEnum.CARDPACK.value, // 不是代金卡包订单
        });
        // 显示修改收货地址按钮
        Object.assign(item, {
          showUpdateAddressBtn:
            item.itemType === goodsTypeEnum.PRODUCT.value &&
            item.expressType === deliveryWay.EXPRESS.value && // 快递订单才能修改收货地址
            item.itemType !== goodsTypeEnum.ESTATE.value && // 不是楼盘订单
            (item.orderStatus === ORDER_STATUS.waitPay ||
              item.orderStatus === ORDER_STATUS.paid ||
              item.orderStatus === ORDER_STATUS.confirm),
        });
        // 显示取消订单按钮
        Object.assign(item, {
          showCancelBtn: item.orderStatus === ORDER_STATUS.waitPay,
        });
        // 显示付款按钮
        Object.assign(item, {
          showPayBtn: item.orderStatus === ORDER_STATUS.waitPay,
        });
        // 显示确认收货及查看物流按钮
        Object.assign(item, {
          showConfirmDeliveredBtn:
            item.orderStatus === ORDER_STATUS.shipping && item.itemType !== goodsTypeEnum.ESTATE.value, // 不是楼盘订单
        });
        // 显示申请开票按钮
        Object.assign(item, {
          showReceiptBtn:
            this.state.isOpenInvoice &&
            !item.hasInvoice &&
            item.orderStatus === ORDER_STATUS.success &&
            item.itemType === goodsTypeEnum.PRODUCT.value,
        });
        // 显示订单维度按钮操作区域
        Object.assign(item, {
          showOperationBtnBox:
            item.showUpdateAddressBtn ||
            item.showCancelBtn ||
            item.showPayBtn ||
            item.showConfirmDeliveredBtn ||
            item.showReceiptBtn,
        });
        // 待医院审核改成：处方审核中
        if (item.orderStatus === 92) {
          Object.assign(item, {
            orderStatusDesc: '处方审核中',
          });
        }
        // 待人工审核改成：卖家确认中
        if (item.orderStatus === 93) {
          Object.assign(item, {
            orderStatusDesc: '卖家确认中',
          });
        }
        // 设置楼盘订单状态描述
        if (item.itemType === goodsTypeEnum.ESTATE.value) {
          Object.assign(item, {
            estateOrderStatusDesc:
              item.orderStatus === ORDER_STATUS.waitPay
                ? '待认筹'
                : item.orderStatus === ORDER_STATUS.success
                  ? '交易完成'
                  : '认筹成功',
          });
        }
      });
    }
  }

  // 是否为加载更多请求
  isLoadMoreRequest() {
    return this.state.pageNo > 1;
  }

  // 加载更多
  onRetryLoadMore() {
    this.setState({
      loadMoreStatus: loadMoreStatus.LOADING,
    }, () => {
      this.listOrders(false);
    });
  }

  /**
   * 点击tab切换，获取订单列表
   * @param e
   */
  handleListOrders = (e) => {
    const curType = e.currentTarget.dataset.item;
    this.setState({
      currentType: curType,
      pageNo: 1,
      hasMore: true,
      orderList: null, // 切换tab，重新查询订单列表之前，先清空原有的订单列表
    }, () => {
      this.listOrders(false);
    });
  }

  /**
   * 点击进入订单详情
   * @param {*} e
   */
  handleGoToDetail = (e) => {
    const orderNo = e.currentTarget.dataset.order.orderNo;
    const categoryType = e.currentTarget.dataset.order.categoryType;
    if (categoryType === 5) {
      wxApi.$navigateTo({
        url: '/sub-packages/order-package/pages/order-details/index',
        data: {
          orderNo,
          categoryType,
        },
      });
    } else if (categoryType === 3) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/order-list-detail/index',
        data: {
          orderNo,
          categoryType,
        },
      });
    } else {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/serviceorder-details/index',
        data: {
          orderNo,
          categoryType,
        },
      });
    }
  }

  /**
   * 点击付款
   * @param e
   */
  handlePay = (e) => {
    const orderNo = e.currentTarget.dataset.order.orderNo;
    pay.payByOrder(orderNo, null, {
      wxPaySuccess: () => {
        wxApi.showToast({
          icon: 'success',
          title: '支付成功',
          mask: true,
          complete: () => {
            this.listOrders(false)
          },
        });
      },
      wxPayFail: () => {
        this.listOrders(false)
      },
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  onScrollToLower() {
    this.onReachBottom()
  }

  // 查看上传的图片
  previewUploadImage(previewUrls, e) {
    e.stopPropagation()
    wxApi.previewImage({
      urls: previewUrls
    });
  }

  render() {
    const {
      currentType,
      tmpStyle,
      statusType,
      orderTabsName,
      error,
      orderList,
      loadMoreStatus,
    } = this.state;
    const {fromPage, tabClass} = this.props;
    return (
      <ScrollView
        scrollY
        data-scoped="wk-wcts-ServiceorderList wk-cto-OrderList"
        className="wk-wcts-ServiceorderList order-list-container"
        onScrollToLower={this.onScrollToLower.bind(this)}
      >
        <View className={classNames(tabClass ? 'custom-tab' : 'status-box')}>
          {statusType ?
            statusType.map((item, index) => {
              return (
                <View
                  onClick={_fixme_with_dataset_(this.handleListOrders, {item: item})}
                  className={'status-label ' + (item.value === currentType.value ? 'active' : '')}
                  key={index}
                  style={_safe_style_(
                    item.value === currentType.value
                      ? tabClass ? `color: ${tmpStyle.btnColor};background-color: ${changeOpacotyToRGBA(tmpStyle.btnColor, 0.1)}` : `color: ${tmpStyle.btnColor};border-color: ${tmpStyle.btnColor}`
                      : ''
                  )}
                >
                  {orderTabsName[item.value] || item.label}
                </View>
              );
            }) : ''}
        </View>
        {!!error ? (
          <Error></Error>
        ) : !!orderList && orderList.length === 0 ? (
          <Empty message="暂无订单"></Empty>
        ) : (
          <View className="order-list">
            {orderList ?
              orderList.map((item: any) => {
                return (
                  <View className="tab" key={item.orderNo}>
                    <View className="order-info">
                      <View className="orderNo">{`订单号: ${item.orderNo}`}</View>
                      {item.createTime && (
                        <View className="orderTime">{filters.dateFormat(item.createTime, 'yyyy-MM-dd hh:mm:ss')}</View>
                      )}
                    </View>
                    {item.categoryType == 4 ? (
                      <List
                        list={item.serverPlatformItemList}
                        name={item.categoryName}
                        onToDetail={val=>(this.toDetail(val))}
                      ></List>
                    ) : (
                      <Block>
                        {item.serverPlatformItemList ?
                          item.serverPlatformItemList.map(el => {
                            return (
                              <View key={el.updateTime}>
                                <View className="item-box">
                                  <View
                                    className="item-tab"
                                    onClick={_fixme_with_dataset_(this.handleGoToDetail, {order: item})}
                                    key="index"
                                  >
                                    <View className="item-info-box">
                                      <View className="img-box">
                                        <Image src={filters.imgListFormat(el.img)} className="item-img"></Image>
                                      </View>
                                      <View className="item-info">
                                        <View>
                                          <View className="item-name limit-line line-2">
                                            {el.serverName ? el.serverName : '--'}
                                          </View>
                                        </View>
                                        <View>
                                          {item.salePrice ? (
                                            <View className="item-count-price">
                                              <View className="item-price">
                                                {`￥${filters.moneyFilter(el.salePrice, true)}`}
                                              </View>
                                            </View>
                                          ) : ''}
                                        </View>
                                      </View>
                                      {fromPage ? (
                                        <View className="right-info">
                                          <View>{`x${el.itemCount}`}</View>
                                          <View>{`押金：￥${filters.moneyFilter(el.deposit, true)}`}</View>
                                          <View>{el.needPayFee == 1 ? `售价：￥${filters.moneyFilter(el.amount, true)}` : getFreeLabel(el.showType)}</View>
                                        </View>
                                      ) : ''}
                                    </View>
                                  </View>
                                </View>
                              </View>
                            );
                          }) : ''}
                      </Block>
                    )}

                    <View className="orderStatus-box-new" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      {filters.showOrderDesc(item.orderStatus)}
                    </View>
                    {item.payFee ? (
                      <View className="total-price">
                        {`合计：￥${filters.moneyFilter(item.payFee, true)}`}
                        {item.deposit ? <Text>（含押金）</Text> : ''}
                      </View>
                    ) : ''}

                    <View className="operation-btn-box">
                      {item.categoryType == 3 ? (
                        <View className="operation-text" style={_safe_style_(`background-color: ${tmpStyle.bgColor}`)}>
                          <View
                            className="operation-imgBox"
                            style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                          >
                            <Image
                              className="operation-img"
                              src="https://front-end-1302979015.file.myqcloud.com/images/hotel_reservation.svg"
                              mode="aspectFit"
                            ></Image>
                          </View>
                          <Text style={_safe_style_('color:' + tmpStyle.btnColor)}>酒店预订</Text>
                        </View>
                      ) : ''}

                      {item.orderStatus == 10 ? (
                        <View
                          className="btn"
                          onClick={_fixme_with_dataset_(this.handlePay, {order: item})}
                          style={_safe_style_(
                            'border-color:' +
                            tmpStyle.btnColor +
                            ';background-color:' +
                            tmpStyle.btnColor +
                            ';color:#fff;'
                          )}
                        >
                          付款
                        </View>
                      ) : ''}
                      {!!item.robotFetchCode && (
                        <View className="pickUpCode">
                          取件码：<Text>{item.robotFetchCode}</Text>
                        </View>
                      )}
                    </View>
                  </View>
                );
              }) : ''}
      </View>
  )}

  {/*  暂无订单提示  */
  }
  {/*  订单列表  */
  }
  {/*  加载更多  */
  }
    <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
  </ScrollView>
  );
  }
  }

  export default ServiceorderList;
