import React, {useState, useEffect, useRef} from 'react'; // @externalClassesConvered(Empty)
// @externalClassesConvered(BottomDialog)
import {_safe_style_, _fixme_with_dataset_, PageStyle} from '@/wxat-common/utils/platform';
import {View, Image, Text, Radio, Input, Label} from '@tarojs/components';
import Taro from '@tarojs/taro';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl';
import filters from '../../../utils/money.wxs';
import api from "../../../api";
import wxApi from '../../../utils/wxApi';
import template from '../../../utils/template';
import buyHub from "../../cart/buy-hub";
import cartHelper from "../../cart/cart-helper";
import screen from '../../../utils/screen';
import protectedMailBox from '../../../utils/protectedMailBox';
import pageLinkEnum from '../../../constants/pageLinkEnum';
import './index.scss';

import {useSelector} from 'react-redux';
import wxAppProxy from '../../../utils/wxAppProxy';
import BottomDialog from '@/wxat-common/components/bottom-dialog/index';
import Empty from '../../empty/empty';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import util from '@/wxat-common/utils/util';

const commonImg = cdnResConfig.common;

const ShoppingCart = (props) => {
  const {isTabbar} = props;
  const [tabbarHeight, setTabbarHeight] = useState(screen.tabbarHeight);
  const [isTabbarPage, setIsTabbarPage] = useState(isTabbar || false);
  const [userReady, setUserReady] = useState(false);
  const [list, setList] = useState([]);
  const [storeName, setStoreName] = useState('');
  const [allChecked, setAllChecked] = useState(false);
  const [count, setCount] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [editMode, setEditMode] = useState(false);
  const [tmpStyle, setTmpStyle] = useState({});
  const [checked, setChecked] = useState(null); // 促销活动选中索引
  const [currItem, setCurrItem] = useState(null); // 促销活动选中的商品
  const [promotionList, setPromotionList] = useState([]); // 促销活动选中的商品

  const currentStore = useSelector((state) => state.base.currentStore);
  const registered = useSelector((state) => state.base.registered);
  const themeConfig = useSelector((state) => state.globalData.themeConfig);

  const checkedMapRef = useRef({
    checkedMap: {},
    editCheckedMap: {},
    editMode: false,
    userReady: false,
  });
  const refBottomDialogCMPT = useRef(null);
  wxApi.setNavigationBarTitle({title: '购物车'})
  useEffect(() => {
    const params =
      wxAppProxy.getNavColor('pages/shopping-cart/index') ||
      wxAppProxy.getNavColor('wxat-common/pages/shopping-cart/index');

    if (params) {
      wxApi.setNavigationBarColor({
        frontColor: params.navFrontColor || '#000000',
        backgroundColor: params.navBackgroundColor || '#ffffff',
      });
    }
    // 1、绑定监听
    buyHub.hub.onMapChange(onCartMapChange);

    return () => {
      buyHub.hub.offMapChange(onCartMapChange);
    };
  }, []);

  useEffect(() => {
    if (currentStore) {
      setStoreName(currentStore.abbreviation || currentStore.name);
      if (userReady) {
        initCartData();
      }
    }
  }, [currentStore]);

  useEffect(() => {
    if (registered) {
      setUserReady(true);
      checkedMapRef.current.userReady = true;
      if (currentStore) {
        initCartData();
      }
    }
  }, [registered]);

  useEffect(() => {
    if (themeConfig) {
      setTmpStyle(getTemplateStyle());
    }
  }, [themeConfig]);

  useEffect(() => {
    reState();
  }, [editMode]);

  function onShow() {
    if (editMode) {
      handlerCancelEdit();
    }
  }

  function initCartData() {
    const newList = [];
    cartHelper.getCarts(
      true,
      (_) => {
        buyHub.mapIndex.forEach((key) => {
          const item = buyHub.map[key];
          newList.push(item);
          if (item._checked) {
            checkedMapRef.current.checkedMap[item.id] = {...item};
          }
          if (item._editChecked) {
            checkedMapRef.current.editCheckedMap[item.id] = {...item};
          }
        });
        _computeCheck([...newList], editMode);
      },
      {loading: true, quite: false}
    );
  }

  const onCartMapChange = () => {
    if (checkedMapRef.current.userReady) {
      const newList = [];
      buyHub.mapIndex.forEach((key) => {
        const item = buyHub.map[key];
        newList.push(item);
        if (item._checked) {
          checkedMapRef.current.checkedMap[item.id] = item;
        }
        if (item._editChecked && !checkedMapRef.current.editCheckedMap[item.id]) {
          checkedMapRef.current.editCheckedMap[item.id] = item;
        }
      });
      _computeSort(newList);
      _computeCheck([...newList], checkedMapRef.current.editMode);
      // 查询促销活动
      getActivity(newList);
    }
  };

  function _computeCheck(newList, newEditMode) {
    // 编辑购物车模式和结算模式选中分离
    const theCheckedMap = newEditMode ? checkedMapRef.current.editCheckedMap : checkedMapRef.current.checkedMap;
    let totalPrice = 0;
    let count = 0;
    (newList || []).forEach((item) => {
      if (newEditMode) {
        item._editChecked = false;
      } else {
        item._checked = false;
      }
    });
    if (newList && newList.length) {
      Object.keys(theCheckedMap).forEach((key) => {
        let exist = false;
        newList.forEach((item) => {
          if (item.id === parseInt(key)) {
            exist = true;
            if (newEditMode) {
              item._editChecked = true;
            } else {
              item._checked = true;
              totalPrice += item.price * item.count;
              count += item.count;
            }
          }
        });
        if (!exist) {
          delete theCheckedMap[key];
        }
      });
    } else {
      setEditMode(false);
      checkedMapRef.current.editMode = false;
      checkedMapRef.current.checkedMap = {};
      checkedMapRef.current.editCheckedMap = {};
    }
    setCount(count);
    setTotalPrice(totalPrice);

    _computeSort(newList);
    setAllChecked(Object.keys(theCheckedMap).length === newList.length);
  }

  // 排序分组
  function _computeSort(newList) {
    if (newList && newList.length) {
      // 排序
      newList = newList.sort((a, b) => {
        const val1 = a.mpActivityId || 0;
        const val2 = b.mpActivityId || 0;
        if (val1 <= val2) {
          if (val1 === val2) {
            return a.updateTime - b.updateTime;
          }
          return -1;
        } else {
          return 1;
        }
      });
    }
    setList([...newList]);
  }

  /**
   * 查询商品可以参加的活动
   *
   */
  function getActivity(newList) {
    if (!newList || !newList.length) {
      return;
    }
    const mpMToolItemDTOS = [];
    newList.forEach((item) => {
      mpMToolItemDTOS.push({
        itemNo: item.itemNo,
        skuId: item.skuId,
        categoryId: item.categoryId,
      });
    });

    wxApi
      .request({
        url: api.goods.activity,
        loading: false,
        method: 'POST',
        header: {
          'content-type': 'application/json', // 默认值
        },
        data: {
          mpMToolItemDTOS,
        },
      })
      .then((res) => {
        const promotionList = res.data;
        newList.forEach((item, index) => {
          const promotion = promotionList[item.itemNo];
          item.promotionList = promotion;
          if (promotion && promotion.length && !item.mpActivityId) {
            item.mpActivityType = promotion[0].activityType;
            item.mpActivityId = promotion[0].activityCode;
            item.mpActivityName = promotion[0].name;
            item.ruleName = promotion[0].ruleName;
            // 编辑购物车模式和结算模式选中分离
            const theCheckedMap = editMode ? checkedMapRef.current.editCheckedMap : checkedMapRef.current.checkedMap;
            theCheckedMap[item.id] = item;
          }
        });
        _computeSort(newList);
      })
      .catch((error) => {
      });
  }

  function handlerGetCoupon() {
    // 购物车打开领券中心，点击去使用时，直接回退
    protectedMailBox.send('sub-packages/mine-package/pages/coupon/index', 'directBack', true);
    wxApi.$navigateTo({
      url: '/sub-packages/mine-package/pages/coupon/index',
    });
  }

  // 去结算
  function handlerPay() {
    const skuList = [];
    let item;
    Object.keys(checkedMapRef.current.checkedMap).forEach((key) => {
      if (!!(item = checkedMapRef.current.checkedMap[key]) && !!item.id) {
        skuList.push({
          itemCount: item.count,
          itemNo: item.itemNo,
          barcode: item.barcode,
          name: item.name,
          pic: item.thumbnail,
          skuId: item.skuId,
          skuTreeNames: item.skuInfo,
          salePrice: item.price,
          freight: item.freight || 0,
          noNeedPay: item.noNeedPay || false,
          reportExt: item.reportExt,
          mpActivityType: item.mpActivityType,
          mpActivityId: item.mpActivityId,
          mpActivityName: item.mpActivityName,
          drugType: item.drugType,
        });
      }
    });
    if (!skuList.length) {
      wxApi.showToast({
        title: '未选中商品',
        icon: 'none',
      });
    } else {
      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', skuList);
      wxApi.$navigateTo({
        url: pageLinkEnum.orderPkg.payOrder,
      });
    }
    util.sensorsReportData('shopping_cart_settlement', {
      event_name: '购物车结算',
      product_list: JSON.stringify(skuList)
    })
  }

  // 删除购物车选中纪录
  function handlerRemove() {
    const idList = [];
    Object.keys(checkedMapRef.current.editCheckedMap).forEach((key) => {
      if (checkedMapRef.current.editCheckedMap[key].id) {
        idList.push(checkedMapRef.current.editCheckedMap[key].id);
      }
    });
    if (!idList.length) {
      wxApi.showToast({
        title: '未选中商品',
        icon: 'none',
      });
    } else {
      wxApi.showModal({
        title: '温馨提示:',
        content: '确定删除选中的购物车商品?',
        cancelText: '取消',
        confirmText: '确定',
        success: (res) => {
          if (res.confirm) {
            apiBatchRemove(idList);
          }
        },
      });
    }
  }

  // 取消编辑
  function handlerCancelEdit() {
    setEditMode(false);
    checkedMapRef.current.editMode = false;
  }

  // 开启编辑
  function handlerStartEdit() {
    setEditMode(true);
    checkedMapRef.current.editMode = true;
  }

  // 单条sku，点击check按钮
  function handlerCheck({index}, e) {
    e.stopPropagation();
    const item = list[index];
    // 编辑购物车模式和结算模式选中分离
    const theCheckedMap = editMode ? checkedMapRef.current.editCheckedMap : checkedMapRef.current.checkedMap;
    if (theCheckedMap.hasOwnProperty(item.id)) {
      delete theCheckedMap[item.id];
    } else {
      theCheckedMap[item.id] = item;
    }
    _computeCheck([...list], checkedMapRef.current.editMode);
  }

  // 点击全选
  function handlerAllCheck() {
    // 编辑购物车模式和结算模式选中分离
    const theCheckedMap = editMode ? checkedMapRef.current.editCheckedMap : checkedMapRef.current.checkedMap;
    list.forEach((item) => {
      if (allChecked) {
        delete theCheckedMap[item.id];
      } else {
        theCheckedMap[item.id] = item;
      }
    });
    _computeCheck([...list], editMode);
  }

  function onInsertOrUpdateEnd(res) {
    if (!res.data) {
      if (res.errorCode === cartHelper.cartCode.LOW_STOCK) {
        wxApi.showToast({
          title: '库存不足',
          icon: 'none',
        });
      } else {
        wxApi.showToast({
          title: '添加失败',
          icon: 'none',
        });
      }
    }
  }

  // 减少商品件数
  function handleDecreaseBuyNum({index}, e) {
    const item = list[index];
    if (!cartHelper.isGettingCarts() && !cartHelper.isGettedCarts()) {
      wxApi.showToast({
        title: '正在请求购物车数据，请稍后重试',
        icon: 'none',
      });

      // 只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
      if (!cartHelper.isGettingCarts()) {
        cartHelper.getCarts(true);
      }
      return;
    }
    cartHelper.addOrUpdateCart(item, 1, false, (res) => {
      onInsertOrUpdateEnd(res);
    });
  }

  // 增加商品件数
  function handleIncreaseBuyNum({index}) {
    const item = list[index];
    // fixme 保留库存不足的逻辑
    if (item.count < item.stock) {
      if (!cartHelper.isGettingCarts() && !cartHelper.isGettedCarts()) {
        wxApi.showToast({
          title: '正在请求购物车数据，请稍后重试',
          icon: 'none',
        });

        // 只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
        if (!cartHelper.isGettingCarts()) {
          cartHelper.getCarts(true);
        }
        return;
      }
      cartHelper.addOrUpdateCart(item, 1, true, (res) => {
        onInsertOrUpdateEnd(res);
      });
    } else {
      wxApi.showToast({
        title: '商品库存不足',
        icon: 'none',
      });
    }
  }

  function reState() {
    _computeCheck([...list], editMode);
  }

  /**
   * 购物车删除
   * @param idList {ArrayList<Long>>}
   */
  function apiBatchRemove(idList) {
    cartHelper.deleteGoods(idList, (res) => {
      if (res.data === true) {
        wxApi.showToast({
          title: '删除成功',
        });

        checkedMapRef.current.editCheckedMap = {};
      }
    });
  }

  function click({itemNo}) {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: itemNo,
      },
    });
  }

  function handleToActivity(e) {
    const item = e.currentTarget.dataset.item;
    const data = [{id: item.mpActivityId, name: item.ruleName}];
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/promotion/index',
    });

    protectedMailBox.send('sub-packages/marketing-package/pages/promotion/index', 'dataSource', {data});
  }

  // 显示促销弹窗
  function showModal(e) {
    const index = e.currentTarget.dataset.index;
    const item = list[index];
    setCurrItem(item);
    setChecked(item.mpActivityId);
    setPromotionList(item.promotionList);
    refBottomDialogCMPT.current.showModal();
  }

  function hideModal(e) {
    refBottomDialogCMPT.current.hideModal();
  }

  // 更换促销，隐藏促销弹窗
  function handlePromotion(e) {
    const item = e.currentTarget.dataset.item;
    const newList = list.concat([]);
    newList.forEach((goods) => {
      if (currItem.id === goods.id) {
        goods.mpActivityType = item.activityType;
        goods.mpActivityId = item.activityCode;
        goods.mpActivityName = item.name;
        goods.ruleName = item.ruleName;
      }
    });
    _computeSort(newList);
    refBottomDialogCMPT.current.hideModal();
  }

  // 获取模板配置
  function getTemplateStyle() {
    return template.getTemplateStyle();
  }

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cts-ShoppingCart'
      className='wk-cts-ShoppingCart'
      style={_safe_style_(PageStyle(props.style))}
    >
      {!!(!!list && list.length === 0) && (
        <Empty
          icon={commonImg.retailEmpty}
          message='购物车还是空的哦'
          messageStyle={{fontSize: Taro.pxTransform(24)}}
        ></Empty>
      )}

      {!!(!!list && list.length > 0) && (
        <View className='cart'>
          <View className='header'>
            <View className='flex-left'>
              <Image className='icon' src={getStaticImgUrl.shoppingCart.nameSign_png}></Image>
              <Text className='name'>{storeName}</Text>
            </View>
            <View className='flex-right'>
              {!!editMode && (
                <Text className='text' onClick={handlerCancelEdit}>
                  完成
                </Text>
              )}

              {!editMode && (
                <Image
                  className='group'
                  onClick={handlerGetCoupon}
                  src={getStaticImgUrl.shoppingCart.group_png}
                ></Image>
              )}

              {!editMode && (
                <Text className='text' onClick={handlerGetCoupon}>
                  领券
                </Text>
              )}

              {!editMode && (
                <Text className='text' style={_safe_style_('margin-left:30rpx;')} onClick={handlerStartEdit}>
                  编辑
                </Text>
              )}
            </View>
          </View>
          {/* 购物车列表 */}
          <View className='item-list'>
            {list.map((item, index) => {
              return (
                <View className='item-box' key={item.id}>
                  {/* 促销活动 */}
                  {!!(
                    item.promotionList &&
                    item.promotionList.length &&
                    (index === 0 || item.mpActivityId !== list[index - 1].mpActivityId)
                  ) && (
                    <View className='promtoion-box'>
                      <View className='promtoion-type'>满减</View>
                      <View className='promtoion-text'>{item.ruleName}</View>
                      <View className='look' onClick={_fixme_with_dataset_(handleToActivity, {item: item})}>
                        <Text className='look__text'>再逛逛</Text>
                        <Image className='look__image' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
                      </View>
                    </View>
                  )}

                  <View className='item' onClick={handlerCheck.bind(null, {index})}>
                    <Radio
                      color={tmpStyle.btnColor}
                      value={editMode ? item._editChecked : item._checked}
                      checked={editMode ? item._editChecked : item._checked}
                    ></Radio>
                    <View className='thumbnail-box'>
                      <Image
                        className='thumbnail'
                        src={item.thumbnail}
                        modes='aspectFill'
                        onClick={(e) => {
                          e.stopPropagation();
                          click({itemNo: item.itemNo});
                        }}
                      ></Image>
                      {!!item.preSell && <View className='pre-sale-label'>预售</View>}
                    </View>
                    <View className='info'>
                      <View className='name'>
                        {!!item.drugType && (
                          <Text className='drug-tag' style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                            处方药
                          </Text>
                        )}

                        {item.name}
                      </View>
                      <View className='attrs'>{item.skuInfo}</View>
                      <View className='price-count'>
                        <View className='price' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                          {'￥' + filters.moneyFilter(item.price, true)}
                        </View>
                        <View className='num-box'>
                          <View
                            className={'num-decrease left-radius ' + (item.count == 0 ? 'num-disable' : '')}
                            onClick={(e) => {
                              e.stopPropagation();
                              handleDecreaseBuyNum({index});
                            }}
                          >
                            -
                          </View>
                          <View className='num-input'>
                            <Input className='input-cpt' type='number' value={item.count} disabled></Input>
                          </View>
                          {/* fixme 保留库存不足的逻辑 */}
                          <View
                            className={
                              'num-increase right-radius ' + (item.count == item.itemStock ? 'num-disable' : '')
                            }
                            onClick={(e) => {
                              e.stopPropagation();
                              handleIncreaseBuyNum({index});
                            }}
                          >
                            +
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                  {/* fixme 换促销 */}
                  {!!(item.promotionList && item.promotionList.length > 1) && (
                    <View className='change-box' onClick={_fixme_with_dataset_(showModal, {index: index})}>
                      <Text>换促销</Text>
                      <Image className='promtoion-more' src={getStaticImgUrl.images.rightAngleGray_png}></Image>
                    </View>
                  )}
                </View>
              );
            })}
          </View>
          <View
            className='cart-bottom'
            style={{bottom: isTabbarPage ? tabbarHeight + 'px' : Taro.pxTransform(screen.safeAreaBottom)}}
          >
            <View className='flex-left' onClick={handlerAllCheck}>
              <Radio color={tmpStyle.btnColor} value={allChecked} checked={allChecked}></Radio>
              <Text className='toggle'>全选</Text>
            </View>
            <View className='flex-right'>
              {!editMode && <Text className='count-tip'>合计：</Text>}
              {!editMode && (
                <Text className='money' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  {'￥' + filters.moneyFilter(totalPrice, true)}
                </Text>
              )}

              {!editMode && (
                <View className='pay-btn' onClick={handlerPay} style={_safe_style_('background:' + tmpStyle.btnColor)}>
                  {'结算(' + count + ')'}
                </View>
              )}

              {!!editMode && (
                <View className='remove' onClick={handlerRemove}>
                  删除
                </View>
              )}
            </View>
          </View>
        </View>
      )}

      {/* 选择优惠对话框 */}
      <BottomDialog ref={refBottomDialogCMPT} customClass='bottom-dialog-custom-class'>
        <View className='promotion-title'>换促销</View>
        {/* 优惠类型 */}
        <View className='coupon-box'>
          {promotionList &&
            promotionList.map((item, index) => {
              return (
                <View
                  className={['coupon-item', checked === item.activityCode ? 'checked' : '']}
                  key={item.id}
                  onClick={_fixme_with_dataset_(handlePromotion, {item: item})}
                >
                  <View className='promotion-name'>{item.ruleName}</View>
                  <View className='promotion-time'>
                    {item.startTime} - {item.endTime}
                  </View>
                </View>
              );
            })}
        </View>
        <View className='bottom-comfrim' onClick={hideModal}>
          确定
        </View>
      </BottomDialog>
    </View>
  );
};
export default ShoppingCart;
