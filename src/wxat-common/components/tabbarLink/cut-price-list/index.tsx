import React, { Component } from 'react'; // @externalClassesConvered(Empty)
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';
import login from '../../../x-login/index.js';
import constants from '../../../constants/index.js';
import template from '../../../utils/template.js';
import ActivityList from './activity-list/index';
import LoadMore from '../../load-more/load-more';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import './index.scss';

const loadMoreStatus = constants.order.loadMoreStatus;


class CutPriceList extends Component {
  static defaultProps = {
    showType: 0,
  };

  state = {
    customKey: null,
    pageConfig: null,
    activityList: [],
    error: false,
    pageNo: 1,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
  };

  componentDidMount() {
    this.getTemplateStyle();
    this.listActivityList();
  }

  onPullDownRefresh = () => {
    this.setState(
      {
        pageNo: 1,
        hasMore: true,
      },

      () => {
        this.listActivityList(true);
      }
    );
  };

  onReachBottom = () => {
    if (!this.state.activityList.length) return;
    if (this.state.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatus.LOADING,
        },

        () => {
          this.listActivityList();
        }
      );
    }
  };

  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatus.LOADING,
      },

      () => {
        this.listActivityList();
      }
    );
  };

  listActivityList = (isFromPullDown) => {
    this.setState({
      error: false,
    });

    login.login().then(() => {
      wxApi
        .request({
          url: api.cut_price.list,
          loading: true,
          data: {
            pageNo: this.state.pageNo,
            pageSize: 10,
            status: 1,
          },
        })
        .then((res) => {
          console.log(res);
          let activityList = this.state.activityList;
          if (this.isLoadMoreRequest()) {
            activityList = activityList.concat(res.data || []);
          } else {
            activityList = res.data || [];
          }
          if (activityList.length === res.totalCount) {
            this.setState({
              hasMore: false,
            });
          }
          this.setState({
            activityList: activityList,
            pageNo: this.state.pageNo + 1,
            loadMoreStatus: loadMoreStatus.HIDE,
          });
        })
        .catch((error) => {
          console.log(error);
          /*if (this.isLoadMoreRequest()) {
                                                                                                this.setState({
                                                                                                  loadMoreStatus: loadMoreStatus.ERROR
                                                                                                });
                                                                                              } else {
                                                                                                this.setState({
                                                                                                  error: true
                                                                                                });
                                                                                              }*/
        })
        .finally(() => {
          if (isFromPullDown) {
            wxApi.stopPullDownRefresh();
          }
        });
    });
  };

  isLoadMoreRequest = () => {
    return this.state.pageNo > 1;
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  render() {
    const { activityList, error, loadMoreStatus } = this.state;
    const { showType } = this.props;
    return (
      <View data-scoped='wk-ctc-CutPriceList' className='wk-ctc-CutPriceList serve-list'>
        <Image className='banner' src='https://image.res.meizu.com/image/dss-wshop/30fd8b3f5220472084b22c13e295a21cz' />
        {!!(!!activityList && activityList.length === 0 && !error) && (
          <Empty iconClass='empty-icon' message='暂无砍价活动'></Empty>
        )}

        {!!error && <Error></Error>}
        {!!(!!activityList && activityList.length > 0) && (
          <View className='group'>
            <ActivityList activityList={activityList} showType={showType}></ActivityList>
          </View>
        )}

        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default CutPriceList;
