import React from 'react';
import { View } from '@tarojs/components';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import template from '@/wxat-common/utils/template.js';
import HaggleModule from '@/wxat-common/components/decorate/haggleModule/index';
import './index.scss';

class ActivityList extends React.Component {
  static defaultProps = {
    activityList: [],
    showType: 0,
  };

  state = {
    tmpStyle: {},
  };

  attached() {
    this.getTemplateStyle();
  }

  /**
   * 组件的方法列表
   */
  onGoDetail = (e) => {
    const item = e;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/cut-price/detail/index',
      data: {
        activityId: item.id,
        detail: item,
      },
    });
  };
  //获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { showType, activityList } = this.props;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-tca-ActivityList'
        className='wk-tca-ActivityList'
      >
        <HaggleModule
          showType={showType}
          dataList={activityList}
          onClick={this.onGoDetail}
          style='margin-bottom:40rpx;'
        />
      </View>
    );
  }
}

export default ActivityList;
