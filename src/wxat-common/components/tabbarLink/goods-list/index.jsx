import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';
import login from '../../../x-login/index.js';
import constants from '../../../constants/index.js';
import screen from '../../../utils/screen';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import util from '../../../utils/util.js';
// import Tabbar from '../../custom-tabbar/tabbar'
import LoadMore from '../../load-more/load-more';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import ProductModule from '../../base/productModule';
import wxAppProxy from '../../../utils/wxAppProxy';
import GoodsListFilter from '@/wxat-common/components/goods-list-filter/index';
import displayEnum from '@/wxat-common/constants/displayEnum';

const loadMoreStatusEnum = constants.order.loadMoreStatus;

@hoc

class GoodsList extends React.Component {
  static defaultProps = {
    type: goodsTypeEnum.PRODUCT.value,
  };

  state = {
    tabbarHeight: screen.tabbarHeight,
    error: false,
    productList: null,
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    sortType: null,
    sortField: null,
    display: displayEnum.VERTICAL,
  };

  pageNo = 1;
  hasMore = true;

  componentDidMount() {
    wxAppProxy.setNavColor('pages/goods-list/index', 'wxat-common/pages/goods-list/index');
    this.listGoodsList();
  }

  onPullDownRefresh() {
    this.pageNo = 1;
    this.hasMore = true;
    this.listGoodsList(true);
  }

  onReachBottom() {
    if (this.hasMore) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatusEnum.LOADING,
        },

        () => {
          this.listGoodsList();
        }
      );
    }
  }

  onRetryLoadMore = () => {
    this.setState(
      {
        loadMoreStatus: loadMoreStatusEnum.LOADING,
      },

      () => {
        this.listGoodsList();
      }
    );
  };

  onFilterChange = (detail) => {
    const { sortField, sortType } = detail;
    this.setState(
      {
        sortType,
        sortField,
      },

      () => {
        this.pageNo = 1;
        this.hasMore = true;
        this.listGoodsList(true);
      }
    );
  };

  handleToggleDisplay = () => {
    const { display } = this.state;
    if (display === displayEnum.ONE_ROW_TWO) {
      this.setState({ display: displayEnum.VERTICAL });
    } else {
      this.setState({ display: displayEnum.ONE_ROW_TWO });
    }
  };

  listGoodsList(isFromPullDown) {
    let { productList } = this.state;
    const { sortType, sortField } = this.state;
    this.setState({
      error: false,
    });

    login.login().then(() => {
      const params = {
        status: 1,
        type: this.props.type,
        pageNo: this.pageNo || 1,
        pageSize: 10,
        sortType,
        sortField,
      };

      // 判断请求的商品类型为产品时，则删除请求参数中的type，改用typeList包含产品类型及组合商品类型
      if (params.type === goodsTypeEnum.PRODUCT.value) {
        params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];
      }

      wxApi
        .request({
          url: api.classify.itemSkuList,
          loading: true,
          data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
        })
        .then((res) => {
          if (this.isLoadMoreRequest()) {
            productList = productList.concat(res.data || []);
          } else {
            productList = res.data || [];
          }
          this.hasMore = productList.length < res.totalCount;
          this.pageNo++;
          this.setState({
            productList,
            loadMoreStatus: loadMoreStatusEnum.HIDE,
          });
        })
        .catch((error) => {
          if (this.isLoadMoreRequest()) {
            this.setState({
              loadMoreStatus: loadMoreStatusEnum.ERROR,
            });
          } else {
            this.setState({
              error: true,
            });
          }
        })
        .finally(() => {
          if (isFromPullDown) {
            wxApi.stopPullDownRefresh();
          }
        });
    });
  }
  isLoadMoreRequest() {
    return this.pageNo > 1;
  }

  render() {
    const { tabbarHeight, productList, error, loadMoreStatus, display } = this.state;
    return (
      <Block>
        <View>
          <View className='goods-list' style={_safe_style_('padding-bottom: ' + tabbarHeight + 'px;')}>
            {!error && (
              <GoodsListFilter
                onFilterChange={this.onFilterChange}
                showViewIcon
                view={display}
                onViewChange={this.handleToggleDisplay}
              ></GoodsListFilter>
            )}

            {!!(!!productList && productList.length === 0) && <Empty message='暂无产品'></Empty>}
            {!!error ? (
              <Error></Error>
            ) : (
              <View className='product-list'>
                {!!(productList && productList.length) && <ProductModule list={productList} display={display} />}
                <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
              </View>
            )}
          </View>
          {/*<Tabbar></Tabbar>*/}
        </View>
      </Block>
    );
  }
}

export default GoodsList;
