// @externalClassesConvered(Empty)
import React, { ComponentClass, Component } from 'react';
import { View, Image, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import api from "../../../api";
import wxApi from '../../../utils/wxApi';
import constants from "../../../constants";
import CouponModule from '../../couponModule';
import LoadMore from '../../load-more/load-more';
import Error from '../../error/error';
import Empty from '../../empty/empty';
import './index.scss';


const loadMoreStatus = constants.order.loadMoreStatus;

interface StateToProps {
  appId: string;
  registered: boolean;
  globalData: any
}

interface PageOwnProps {
  directBack?: boolean;
}

interface PageState {}

type IProps = StateToProps & PageOwnProps & PageState;

interface CouponCenter {
  props: IProps;
}

const mapStateToProps = (state) => ({
  appId: state.ext.appId,
  registered: state.base.registered,
  globalData: state.globalData
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class CouponCenter extends Component {
  state = {
    pageNo: 1,
    hasMore: true,
    couponList: [],
    error: false,
    loadMoreStatus: loadMoreStatus.LOADING
  };

  componentDidMount() {
    this.listCouponList();
  }

  handlerScroll = () => {
    if (!this.state.hasMore) return false
    this.setState(
      {
        pageNo: this.state.pageNo + 1
      },
      () => {
        this.listCouponList()
      }
    )
  }


  onRetryLoadMore = () => {
    this.listCouponList()
  }

  listCouponList() {
    this.setState(
      {
        error: false,
        hasMore: true,
        loadMoreStatus: loadMoreStatus.LOADING,
      },
      () => {
        wxApi
        .request({
          url: api.coupon.list,
          loading: false,
          checkSession: true,
          data: {
            pageSize: 10,
            pageNo: this.state.pageNo,
            appId: this.props.appId
          }
        })
        .then((res) => {
          let couponList = this.state.couponList.concat(res.data || [])

          if (couponList.length === res.totalCount && !!res.totalCount) {
            this.setState({
              hasMore: false,
              loadMoreStatus: loadMoreStatus.BASELINE
            })
          }
          if (!res.totalCount) {
            this.setState({
              loadMoreStatus: loadMoreStatus.HIDE
            })
          }
          this.setState({
            couponList,
          })
        })
        .catch(() => {
          this.setState({
            error: true,
            loadMoreStatus: loadMoreStatus.HIDE
          });
        })
      }
    )
  }

  handleGetCoupon = (detail) => {
    if (this.props.registered) {
      const couponNo = detail.couponNo;
      const receiveMethod = detail.receiveMethod;
      if (receiveMethod) {
        this.getIntegralCoupon(couponNo);
      } else {
        this.getFreeCoupon(couponNo);
      }
    } else {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/wx-auth/index',
      });
    }
  };

  getFreeCoupon(couponNo) {
    const reqData = {
      couponId: couponNo,
    };

    wxApi
      .request({
        url: api.coupon.collect,
        loading: true,
        checkSession: true,
        data: reqData,
      })
      .then(() => {
        const list = this.state.couponList.map(item => {
          if (item.id === couponNo) item.alreadyHave = 1
          return item
        })
        this.setState(
          {
            couponList: list
          }
        )
        wxApi.showToast({
          image: "https://bj.bcebos.com/htrip-mp/static/app/images/mine/success.png",
          title: '领取成功'
        });
      })
      .catch((err) => {
        wxApi.showToast({
          title: err.errorMessage
        })
      })
  }

  getIntegralCoupon(couponNo) {
    const reqData = {
      couponId: couponNo,
    };

    wxApi.showModal({
      title: `是否消耗${this.props.globalData.scoreName}兑换？`,
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.coupon.integral,
              loading: true,
              checkSession: true,
              data: reqData,
            })
            .then(() => {
              // const list = this.state.couponList.map(item => {
              //   if (item.id === couponNo) item.alreadyHave = 1
              //   return item
              // })
              // this.setState(
              //   {
              //     couponList: list
              //   }
              // )
              this.setState({
                pageNo:1
              },()=>{
                this.listCouponList()
                wxApi.showToast({
                  image: "https://bj.bcebos.com/htrip-mp/static/app/images/mine/success.png",
                  title: '领取成功'
                });
              })
            })
            .catch(() => {
              // wxApi.showToast({
              //   title: '领取失败'
              // })
            })
        }
      },
    });
  }

  render() {
    const { directBack } = this.props;
    const { couponList, error, loadMoreStatus } = this.state;

    return (
      <ScrollView scrollY={true} scrollWithAnimation scrollAnchoring style={{'height': '100%', 'overflow-anchor': 'auto', '-webkit-overflow-scrolling': 'touch'}} onScrollToLower={this.handlerScroll}>
        <View data-scoped='wk-ctc-CouponCenter' className='wk-ctc-CouponCenter couponList'>
          {!error && !couponList.length && !loadMoreStatus &&  <Empty message='敬请期待...'></Empty>}
          {error && <Error></Error>}
          <View>
            {
              !error && !!couponList.length && (
                <>
                  <Image className='coupon-bg' height="auto" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/roll-center.png"></Image>
                  <CouponModule
                    list={couponList}
                    directBack={directBack}
                    isMine={false}
                    onCouponEvent={this.handleGetCoupon}/>
                </>
              )
            }
            <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default CouponCenter as ComponentClass<PageOwnProps, PageState>;
