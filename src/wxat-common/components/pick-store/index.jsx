import React from 'react'; /* eslint-disable react/sort-comp */
import classNames from 'classnames';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image, Input, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import constants from '../../constants/index.js';
import wxApi from '../../utils/wxApi';
import api from '../../api/index.js';
import login from '../../x-login/index.js';
import businessTime from '../../utils/businessTime.js';
import report from '../../../sdks/buried/report/index.js';
import distance from '../../utils/distance';
import LoadMore from '../load-more/load-more';
import './index.scss';
import { connect } from 'react-redux';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl'
import { updateBaseAction } from '../../../redux/base';

const decorateImg = cdnResConfig.decorate;
const loadMoreStatusEnum = constants.order.loadMoreStatus;

const mapStateToProps = (state) => ({
  source: state.base.source,
  appInfo: state.base.appInfo || {},
  environment: state.globalData.environment,
  gps: state.base.gps,
  currentStore: state.base.currentStore,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateGps: (obj) => {
    dispatch(updateBaseAction(obj));
  },
});
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class PickStore extends React.Component {
  static defaultProps = {
    // 门店过滤列表id字符串
    storeIds: null,
    //选择门店回调
    onPickStore: null,
  };

  state = {
    address: '',
    title: '',
    longlat: {},
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    storeList: [],
    focus: true,
    keyWords: '',
    loading: true,
    isLocation:false
  };

  initialize = true;
  ignoreAutoLocation = false;
  pageNo = 1;
  pageSize = 20;
  hasMore = true;

  findTheItem(storeId) {
    const { storeList } = this.state;
    return (
      storeList.find((item) => {
        return item.id === storeId;
      }) || {}
    );
  }

  //点击打开指定位置的地图
  clickLocation(e) {
    const { latitude, longitude, name, address } = this.findTheItem(e.currentTarget.dataset['id']);
    !!latitude &&
      wxApi.openLocation({
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        name,
        address,
      });
  }

  //点击直接电话商家
  clickPhone(e) {
    const { tel } = this.findTheItem(e.currentTarget.dataset['id']);
    tel &&
      wxApi.makePhoneCall({
        phoneNumber: tel,
      });
  }

  //选中当前店铺
  clickItem = (e) => {
    const storeId = e.currentTarget.dataset['id'];
    report.clickStoreInfo(storeId);
    if (this.props.onPickStore) {
      this.props.onPickStore(this.findTheItem(storeId));
    }
  };

  clickChooseLocation() {
    this.ignoreAutoLocation = true;
    console.log('clickChooseLocation getSetting start');
    this.setState({
      isLocation:true
    })
    wxApi
      .getSetting()
      .then((res) => {
        if (!res.authSetting['scope.userLocation']) {
          return wxApi
            .authorize({
              scope: 'scope.userLocation',
            })
            .then(
              (res) => {
                report.reportLocation(true);
                return wxApi.chooseLocation();
              },
              (error) => {
                report.reportLocation(false);
              }
            );
        } else {
          return wxApi.chooseLocation();
        }
      })
      .then((location) => {
        if (!!location.address) {
          this.setState(
            {
              address: location.address,
              title: location.name,
              longlat: {
                longitude: location.longitude,
                latitude: location.latitude,
              },
            },

            () => {
              this.props.dispatchUpdateGps({ gps: { longtitude:location.longitude, latitude: location.latitude }})
              this.ignoreAutoLocation = true;
              this.pageNo = 1;
              this.getStoreList();
            }
          );
        } else {
          this.ignoreAutoLocation = false;
        }
      }).finally((e)=>{
        this.setState({
          isLocation:false
        })
      });
  }

  /* 定位 */
  getLocationData() {
    let _this = this
    return wxApi.$getLocation().then(function (resData) {
      const { lng, lat } = resData.point
      _this.props.dispatchUpdateGps({ gps: { longtitude: lng, latitude: lat } })
      console.log('定位成功')
      return _this.getLocationAdress(resData)
    }, function (err) {
      console.log('定位失败')
      return _this.locationError(err)
    })
  }

  getLocationAdress(resData) {
    const { lng, lat } = resData.point
    return wxApi
    .request({
      url: api.poi.location + '?lng=' + lng + '&lat=' + lat,
      loading: true,
    })
    .then((res) => {
      const data = res.data || {};
      this.setState({
        address: data.address || '',
        title: data.title || '',
      });
    });
  }

  getCompileAddress(resData) {
    let url = 'https://api.map.baidu.com/reverse_geocoding/v3/'
    wxApi.$jsonp(url, {
        location: `${resData.latitude},${resData.longitude}'`,
        ak: 'tGeEkf70CzM4DEj5fSYPGT07RWqT4Z05',
        output: 'json',
        coordtype: 'wgs84ll'
      }
    )
      .then(res => {
        console.log('解析成功')
        console.log(res)
        const {formatted_address,location}=res.result
        const {lng,lat}=location
        wxApi
        .request({
          url: api.poi.location + '?lng=' + lng + '&lat=' + lat,
          loading: true,
        })
        .then((res) => {
          const data = res.data || {};
          this.setState({
            address: data.address || '',
            title: data.title || '',
          });
        });
      })
  }
  locationError() {
    alert('无法定位到您的位置,请手动选择哦')
  }
  //定位经纬度＋地址解析
  wxLocation() {
    return wxApi
      .getSetting({ type: 'highAccuracyExpireTime' })
      .then((res) => {
        if(process.env.TARO_ENV == 'h5') {
          return this.getLocationData()
        } else {
          if (!res.authSetting['scope.userLocation']) {
            return wxApi
              .authorize({
                scope: 'scope.userLocation',
              })
              .then(() => {
                // TODO: 待验证，待整理
                wx.ready(() =>{
                  wx.getLocation({
                    type: "gcj02",
                    success:(res) => {
                      const longitude = res.longitude,
                        latitude = res.latitude;
                      this.props.dispatchUpdateGps({ gps: { longtitude:longitude, latitude } })
                      this.setState({
                        longlat: {
                          longitude: longitude,
                          latitude: latitude,
                        },
                      });
                      wxApi
                        .request({
                          url: api.poi.location + '?lng=' + longitude + '&lat=' + latitude,
                          loading: true,
                        })
                        .then((res) => {
                          const data = res.data || {};
                          this.setState({
                            address: data.address || '',
                            title: data.title || '',
                          });
                        });

                    },
                    cancel: function (res) {
                    },
                  })
                });
              });
          } else {
            //开启高精度定位，2.9版本以上支持，超时时间越长精度越准，超时时间短为了不影响体验
            wx.ready(() =>{
              wx.getLocation({
                type: "gcj02",
                success:(res) =>{
                  const longitude = res.longitude,
                  latitude = res.latitude;
                  this.props.dispatchUpdateGps({ gps: { longtitude:longitude, latitude }})
                  this.setState({
                    longlat: {
                      longitude: longitude,
                      latitude: latitude,
                    },
                  });
                  console.log(this);
                  wxApi
                    .request({
                      url: api.poi.location + '?lng=' + longitude + '&lat=' + latitude,
                      loading: true,
                    })
                    .then((res) => {
                      console.log(res,'打印一下数据');
                      const data = res.data || {};
                      this.setState({
                        address: data.address || '',
                        title: data.title || '',
                      });
                    });
                },
                cancel: function (res) {
                },
              })
            });
          /* console.log(wxApi.getLocation({ type: 'gcj02', isHighAccuracy: true, highAccuracyExpireTime: 500 }));
            return wxApi.getLocation({ type: 'gcj02', isHighAccuracy: true, highAccuracyExpireTime: 500 });*/
          }
        }
      })
     /* .then((res) => {

      });*/
  }



  async initRender() {
    if (!this.ignoreAutoLocation) {
      this.wxLocation().finally(() => {
        this.initialize = false;
        this.pageNo = 1;
        this.getStoreList();
      });
    }
  }

  onRetryLoadMore() {
    if (!this.initialize) this.getStoreList();
  }

  loadMore() {
    if (this.hasMore && !this.initialize) {
      this.getStoreList();
    }
  }

  refreshList() {
    if (this.initialize) return;

    this.pageNo = 1;
    this.hasMore = true;
    this.setState({
      keyWords: '',
    });

    this.getStoreList();
  }

  getStoreList(keyWords) {
    const { gps, storeIds } = this.props;
    const { longlat } = this.state;
    if (this.pageNo > 1) {
      this.setState({
        loadMoreStatus: loadMoreStatusEnum.LOADING,
      });
    }
    this.setState({
      loading: true,
    });

    const { longitude, latitude } = longlat;
    let url = api.store.queryCustomizeShop + '?pageNo=' + this.pageNo + '&pageSize=' + this.pageSize;
    if (!!latitude) {
      url += '&longtitude=' + longitude;
      url += '&latitude=' + latitude;
    } else {
      if (gps) {
        url += '&latitude=' + gps.latitude;
        url += '&longtitude=' + gps.longtitude;
      }
    }

    // 门店过滤列表id字符串
    if (!!storeIds) {
      url += '&storeIds=' + storeIds;
    }

    if (!!keyWords) {
      url += '&storeName=' + keyWords;
    }

    login
      .login()
      .then((res) => {
        return wxApi.request({
          url,
          loading: true,
        });
      })
      .then((res) => {
        const { storeList } = this.state;
        const newData = res.data || [];
        const list = this.pageNo === 1 ? newData : storeList.concat(newData);
        if (!!list && !!list.length) {
          list.forEach((store) => {
            store.businessTime = businessTime.computed(
              store.businessStartHour,
              store.businessEndHour,
              store.businessDayOfWeek
            );
          });
        }
        if (!list) {
          this.setState({
            storeList: list || [],
          });
        } else {
          this.setState({
            storeList: list || [],
            loadMoreStatus: loadMoreStatusEnum.HIDE,
          });

          this.hasMore = newData.length >= this.pageSize;
          this.pageNo = this.pageNo + 1;
        }
      })
      .catch((error) => {
        this.setState({
          loadMoreStatus: loadMoreStatusEnum.ERROR,
        });
      })
      .finally(() => {
        wxApi.stopPullDownRefresh();
        this.setState({
          loading: false,
        });
      });
  }

  enterFocus(options) {
    this.setState({
      focus: true,
    });
  }

  loseFocus(options) {
    //fixme 需要加延迟操作，否则无法执行历史记录的点击事件
    setTimeout(() => {
      this.setState({
        focus: false,
      });
    }, 300);
  }

  confrim(options) {
    this.setState({
      focus: false,
    });

    this.pageNo = 1;
    const keyWords = options.detail.value;
    this.getStoreList(keyWords);
  }

  showForbidChangeStoreTips = () => {
    wxApi.showToast({ icon: 'none', title: '当前门店不可切换' });
  };

  render() {
    const { focus, keyWords, title, address, storeList, loadMoreStatus, loading,isLocation } = this.state;

    const { appInfo, source, environment, currentStore } = this.props;
    const forbidChangeStore =
      appInfo.forbidChangeStoreForGuideShare && (source === 'wxwork' || environment === 'wxwork');

    const $storeList = storeList.map((store) => {
      const disabled = forbidChangeStore && currentStore.id !== store.id;
      return (
        <View className={classNames('store', { disabled })} key={store.id}>
          <View
            onClick={disabled ? this.showForbidChangeStoreTips : _fixme_with_dataset_(this.clickItem, { id: store.id })}
          >
            <View className='name overflow-text'>{store.abbreviation || store.name}</View>
            <View className='address overflow-text'>{store.address}</View>
            <View className='time overflow-text'>{'营业时间：' + store.businessTime}</View>
          </View>
          <View className='sign'>
            <View className='pos'>
              <Image
                className='loc-icon'
                onClick={_fixme_with_dataset_(this.clickLocation.bind(this), { id: store.id })}
                src={decorateImg.location}
              ></Image>
              <View className='distance'>{distance(store.distance)}</View>
            </View>
            <View className='phone'>
              <Image
                className='phone-icon'
                onClick={_fixme_with_dataset_(this.clickPhone.bind(this), { id: store.id })}
                src={decorateImg.phone}
              ></Image>
            </View>
          </View>
        </View>
      );
    });
    const $picker = (
      <View>
        <View className='store-title'>附近门店</View>
        {$storeList}
        {!!(!loading && storeList.length === 0) && <View className='store-search'>暂无搜索结果</View>}
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore.bind(this)}></LoadMore>
      </View>
    );

    return (
      <ScrollView
        scrollY
        data-fixme='02 block to view. need more test' data-scoped='wk-wcp-PickStore' className='wk-wcp-PickStore'>
        {process.env.TARO_ENV == 'h5' ? <div id="allmap"></div> : null}
        {
          !isLocation ? <View className='input-view'>
          <Image className='icon' src={getStaticImgUrl.images.search_icon_png}></Image>
          <Input
            className='input-search'
            focus={focus}
            placeholderClass='input-search-placeholder'
            placeholder='搜索附近门店'
            onConfirm={this.confrim.bind(this)}
            value={keyWords}
            confirmType='search'
            onFocus={this.enterFocus.bind(this)}
            onBlur={this.loseFocus.bind(this)}
          ></Input>
        </View> : ''
        }

        <View className='layout-pos'>
          <View className='store-title'>当前位置</View>
          <View className='al'>
            <View className='left flex-column'>
              <View className='name overflow-text'>{title || '正在定位...'}</View>
              <View className='address overflow-text'>{address || ''}</View>
            </View>
            <View className='right flex-column' onClick={this.clickChooseLocation.bind(this)}>
              <Image className='location-img' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/gun-location.png"></Image>
              <View className='location'>手动定位</View>
            </View>
          </View>
        </View>
        {$picker}
      </ScrollView>
    );
  }
}

export default PickStore;
