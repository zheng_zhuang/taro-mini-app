import React from 'react'; // @scoped
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';

import '../shared.scss';

class RetailItem extends React.Component {
  static defaultProps = {
    display: '',
    isPreSell: false,
    descTags: [],
    attributeName: '',
    itemSalePrice: '',
    itemLabelPrice: '',
    tmpStyle: {},
  };

  render() {
    const { picTags, itemStock, isProduct, item, defaultTagCss, attributeName, tmpStyle, isPreSell } = this.props;
    return (
      <View className='wk-cig-GoodsItem'>
        {isPreSell && <View className='pre-sale-label'>预售</View>}
        {!!attributeName && (
          <View className='pre-pro-label' style={_safe_style_('background:' + tmpStyle.btnColor)}>
            {attributeName}
          </View>
        )}

        {!!(isProduct && (!itemStock || itemStock < 0)) && <View className='gi__no-goods'>补货中</View>}
        {!!(!!picTags && !!picTags.length) && (
          <View className='gi__tag-box'>
            {picTags.map((tag) => (
              <View key={tag.tagName} className='gi__tag' style={_safe_style_(item.tagCss || defaultTagCss)}>
                {tag.tagName}
              </View>
            ))}
          </View>
        )}
      </View>
    );
  }
}

export default RetailItem;
