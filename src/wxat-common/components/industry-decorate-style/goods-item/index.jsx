import React from 'react'; /* eslint-disable react/sort-comp */
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { Image, Text, View } from '@tarojs/components';
import classNames from 'classnames';
import wxApi from '../../../utils/wxApi';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import template from '../../../utils/template.js';
import displayEnum from '../../../constants/displayEnum';

import GoodsStockTmpl from "./goods-stock-tmpl";
import CartCount from "../../cart/cart-count";
import filters from '../../../utils/money.wxs';
import imageUtils from '../../../utils/image';
import constants from '@/wxat-common/constants';

import './shared.scss';
import './index.scss';

const { salesTypeEnum } = constants;

const tagPosition = {
  PIC: 1 /* 标签位置在图片上 */,
  DESC: 0 /* 标签位置在描述信息上 */,
};

const dealPrice = (price, position) => {
  let result = 0;
  if (price && price.indexOf('-') !== -1) {
    result = +price.split('-')[position]; // 划线价取最高价格
  } else {
    result = parseFloat(price);
  }
  return result;
};

const isPriceRange = (price) => {
  return !!price && (price + '').indexOf('-') !== -1;
};

const getDisplayDealPrice = (price) => {
  if (!price || ('' + price).indexOf('-') === -1) return price;
  const prices = price.split('-');
  prices.forEach((item, index) => {
    prices[index] = parseFloat(item);
  });
  return prices.join('-');
};

class GoodsItem extends React.Component {
  static defaultProps = {
    /* 商品item展示类型，enum: horizon:横向展示的商品item，供垂直列表展示使用 vertical:垂直展示的商品item，供横向滑动列表展示使用 */
    display: displayEnum.VERTICAL,
    /* 是否显示底部分割线,可根据列表是否时最后一个元素控制分割线是否展示，此属性只有在display=='horizon'有效 */
    showDivider: true,
    goodsItem: {},
    reportSource: '',
    /* size为样式的大小，分类中用的是小样式， default/small */
    itemSize: '',
    attributeName: '',
    // item点击回调
    onClick: null, // 如果有回调，并返回true。则直接跳转商品详情
  };

  state = {
    tmpStyle: null, // 主题模板配置
    itemSalePrice: 0,
    itemLabelPrice: 0,
    itemStock: 0,
    itemSalesType: 0,
    displayPrice: '',
    picTags: null,
    descTags: null,
    itemSalePriceRange: 0,
    showCartCount: false,
    showDiscount: false,
    thumbnail: '',
    thumbnailMid: '',
    thumbnailOneRowTwo: '',
    // 销量
    itemSalesVolume: 0,
    // 是否预售
    isPreSell: false,
    // 商品名称
    name: false,
    // 商品子名称
    subName: false,
  }; /* 请尽快迁移为 componentDidMount 或 constructor */

  UNSAFE_componentWillMount() {
    if (!!this.props.goodsItem && !!this.props.goodsItem.wxItem && !this.state.tmpStyle) {
      this.filterItems(this.props.goodsItem.wxItem);
    }
  }

  /**
   * 过滤普通商品列表
   */
  filterItems(wxItem) {
    const _update = {};

    if (!!wxItem.itemTagList && !!wxItem.itemTagList.length) {
      _update.picTags = wxItem.itemTagList.filter((tag) => {
        return tag.tagPosition === tagPosition.PIC;
      });
      _update.descTags = wxItem.itemTagList.filter((tag) => {
        return tag.tagPosition === tagPosition.DESC;
      });
    }

    const skuSaleLowPrice = dealPrice(wxItem.salePriceRange, 0);
    const skuLabelHighPrice = dealPrice(wxItem.labelPriceRange, 1);
    let itemSalePriceRange = 0;

    if (isPriceRange(wxItem.salePriceRange)) itemSalePriceRange = getDisplayDealPrice(wxItem.salePriceRange);

    _update.itemSalePriceRange = itemSalePriceRange;
    _update.itemSalePrice = wxItem.salePrice;

    _update.itemSkuLowSalePrice = skuSaleLowPrice;
    _update.itemSkuHighSalePrice = skuLabelHighPrice;

    _update.itemLabelPrice = wxItem.labelPrice;

    _update.itemStock = wxItem.itemStock;
    _update.isProduct = wxItem.type === goodsTypeEnum.PRODUCT.value;

    // 获取主题模板配置
    _update.tmpStyle = template.getTemplateStyle();

    _update.itemSalesType = wxItem.salesType;
    _update.displayPrice = wxItem.displayPrice;

    // 显示购物车数量
    _update.showCartCount =
      !wxItem.frontMoneyItem &&
      (wxItem.type === goodsTypeEnum.PRODUCT.value || wxItem.type === goodsTypeEnum.COMBINATIONITEM.value);

    // 是否预售
    _update.isPreSell = !!wxItem.preSell;
    // 商品名称
    _update.name = wxItem.name;
    // 商品子名称
    _update.subName = wxItem.subName;

    // 显示折扣价
    _update.showDiscount =
      !itemSalePriceRange &&
      _update.itemSkuLowSalePrice < _update.itemSkuHighSalePrice &&
      _update.itemSalePrice <= _update.itemLabelPrice;

    // 到店咨询 不显示 划线价 及 加入购物车 按钮
    if (_update.itemSalesType === salesTypeEnum.OFFLINE) {
      _update.showCartCount = false;
      _update.showDiscount = false;
    }

    _update.thumbnailMid = imageUtils.thumbnailMid(wxItem.thumbnailMid || wxItem.thumbnail);
    _update.thumbnailOneRowTwo = imageUtils.thumbnailOneRowTwo(wxItem.thumbnailOneRowTwo || wxItem.thumbnail);
    _update.thumbnail = imageUtils.thumbnailMid(wxItem.thumbnail);

    _update.itemSalesVolume = wxItem.itemSalesVolume || 0;

    this.setState({ ..._update });
  }

  /**
   * 点击商品item
   */
  handleClickGoodsItem = () => {
    const wxItem = this.props.goodsItem.wxItem;
    const itemNo = wxItem.itemNo;
    const onClick = this.props.onClick;
    !!onClick &&
      !!onClick({
        detail: {
          name: wxItem.name,
          itemNo,
        },
      });
      let url ="/wxat-common/pages/goods-detail/index";
      const goodsItem = this.props.goodsItem;
      const goodsType = goodsItem.wxItem
        ? goodsItem.wxItem.type
        : goodsItem.type;
      switch (goodsType) {
        case goodsTypeEnum.PRODUCT.value: // 产品类型
          url = "/wxat-common/pages/goods-detail/index";
          break;
        case goodsTypeEnum.ROOMS.value: // 客房类型
          url = "/wxat-common/pages/goods-detail/index";
          break;
        case goodsTypeEnum.TICKETS.value: // 票务类型
          url = "/wxat-common/pages/goods-detail/index";
          break;
        case goodsTypeEnum.COMBINATIONITEM.value: // 组合商品类型
          url = "/wxat-common/pages/goods-detail/index";
          break;
        case goodsTypeEnum.SERVER.value: // 服务类型
          url = "/sub-packages/server-package/pages/serve-detail/index";
          break;
        case goodsTypeEnum.TIME_CARD.value || goodsTypeEnum.CHARGE_CARD.value: // 卡项或充值卡类型
          url = "/sub-packages/server-package/pages/card-detail/index";
          break;
          case goodsTypeEnum.VIRTUAL_GOODS.value : // 虚拟卡商品
          url = "/sub-packages/marketing-package/pages/virtual-goods/detail/index";
          break;
      }
    wxApi.$navigateTo({
      url: url,
      data: {
        itemNo,
        source: this.props.reportSource,
      },
    });
  };

  render() {
    const {
      picTags,
      descTags,
      itemSkuLowSalePrice,
      itemSkuHighSalePrice,
      itemSalePrice,
      itemLabelPrice,
      itemSalesType,
      displayPrice,
      itemStock,
      isProduct,
      tmpStyle,
      itemSalePriceRange,
      showCartCount,
      showDiscount,
      thumbnailMid,
      thumbnailOneRowTwo,
      thumbnail,
      isPreSell,
      name,
      subName,
      itemSalesVolume,
    } = this.state;

    const { goodsItem, display, attributeName, reportSource, itemSize, className } = this.props;
    if (!tmpStyle) {
      return null;
    }

    return (
      <View className={['wk-cig-GoodsItem ', `${className || ''}`]} >
        {/* 横向展示的商品item，供垂直列表使用 */}
        {display === displayEnum.VERTICAL && (
          <View
            className={classNames('h-goods-item-container', itemSize ? 'h-goods-item-' + itemSize : '')}
            onClick={this.handleClickGoodsItem}
          >
            <View className='h-goods-img-box'>
              <Image className='h-goods-img' mode='aspectFill' src={thumbnailMid} />
              <GoodsStockTmpl
                isPreSell={isPreSell}
                picTags={picTags}
                itemStock={itemStock}
                isProduct={isProduct}
                attributeName={attributeName}
                tmpStyle={tmpStyle}
              />
            </View>

            <View className='h-goods-info-container'>
              <View className='h-goods-desc-box'>
                <View className='h-goods-title limit-line line-2'>
                  {
                    (goodsItem.wxItem.sourceType === 1 || goodsItem.wxItem.sourceType === 4) && (
                      // 显示自营商品的标签
                      <View className="xsd">
                        <Image className="xsd1" mode="aspectFit"
                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/zysp1.svg" />
                      </View>
                    )
                  }
                  <Text>{name}</Text>
                </View>
                {!!(!!descTags && !!descTags.length) && (
                  <View style={_safe_style_('font-size: 0;margin-bottom: 5px')}>
                    {descTags.map((tagItem) => {
                      return (
                        <View key={tagItem.tagName} className='h-tag' style={_safe_style_(tagItem.tagCss)}>
                          {tagItem.tagName}
                        </View>
                      );
                    })}
                  </View>
                )}

                {!!subName && <View className='h-goods-sub-title limit-line'>{subName}</View>}
              </View>

              <View className='h-price-container'>
                <View className='h-price-box'>
                  <View className='h-price' style={{ color: tmpStyle.btnColor }}>
                    {itemSalesType !== salesTypeEnum.OFFLINE ? (
                      <Text>
                        <Text className='unit'>￥</Text>
                        {itemSalePriceRange || itemSkuLowSalePrice || filters.moneyFilter(itemSalePrice || 0, true)}
                      </Text>
                    ) : (
                      <Text className='offline-price'>{displayPrice || '到店咨询'}</Text>
                    )}

                    {!!showDiscount && (
                      <Text className='gi__label-discount'>
                        ￥{itemSkuHighSalePrice || filters.moneyFilter(itemLabelPrice || 0, true)}
                      </Text>
                    )}
                  </View>
                  <View className='h-goods-desc'>{itemSalesVolume}人已购买</View>
                </View>
                {showCartCount && (
                  <CartCount
                    width={itemSize ? '22' : ''}
                    height={itemSize ? '22' : ''}
                    left={itemSize ? '10' : ''}
                    bottom={itemSize ? 14 : ''}
                    spu={goodsItem}
                    reportSource={reportSource}
                  />
                )}
              </View>
            </View>
          </View>
        )}

        {/* 纵向展示的商品item，供横向滑动列表使用 */}
        {display === displayEnum.HORIZON && (
          <View className='v-goods-item-container' onClick={this.handleClickGoodsItem}>
            <View className='v-goods-img-box'>
              <Image className='v-goods-img' mode='aspectFill' src={thumbnailMid} />

              <GoodsStockTmpl
                isPreSell={isPreSell}
                picTags={picTags}
                itemStock={itemStock}
                isProduct={isProduct}
                attributeName={attributeName}
                tmpStyle={tmpStyle}
              />
            </View>

            <View className='v-goods-content'>
              <View className='v-goods-title limit-line'>
                {
                  (goodsItem.wxItem.sourceType === 1 || goodsItem.wxItem.sourceType === 4) && (
                    // 显示自营商品的标签
                    <View className="xsd">
                      <Image className="xsd1" mode="aspectFit"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/zysp1.svg" />
                    </View>
                  )
                }
                <Text>{name}</Text>
              </View>
              <View className='v-price-container'>
                <View className='v-price' style={{ color: tmpStyle.btnColor }}>
                  {itemSalesType !== salesTypeEnum.OFFLINE ? (
                    <View>
                      ￥{itemSalePriceRange || itemSkuLowSalePrice || filters.moneyFilter(itemSalePrice || 0, true)}
                    </View>
                  ) : (
                    <View className='offline-price'>{displayPrice || '到店咨询'}</View>
                  )}
                </View>
              </View>
            </View>
          </View>
        )}

        {/* 一行1个 */}
        {display === displayEnum.ONE_ROW_ONE && (
          <View className='n-goods-item-container oneRowOne' onClick={this.handleClickGoodsItem}>
            <View className='n-goods-img-box'>
              <Image className='n-goods-img' mode='aspectFill' src={thumbnailMid} />

              <GoodsStockTmpl
                isPreSell={isPreSell}
                picTags={picTags}
                itemStock={itemStock}
                isProduct={isProduct}
                attributeName={attributeName}
                tmpStyle={tmpStyle}
              />
            </View>

            <View className='n-goods-title limit-line line-2'>
              {
                (goodsItem.wxItem.sourceType === 1 || goodsItem.wxItem.sourceType === 4) && (
                  // 显示自营商品的标签
                  <View className="xsd">
                    <Image className="xsd1" mode="aspectFit"
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/images/zysp1.svg" />
                  </View>
                )
              }
              <Text>{name}</Text>
            </View>
            {!!subName && <View className='n-goods-sub-title limit-line'>{subName}</View>}

            <View className='n-price-container'>
              <View className='n-price-box'>
                <View className='n-price' style={{ color: tmpStyle.btnColor }}>
                  {itemSalesType !== salesTypeEnum.OFFLINE ? (
                    <Text>
                      <Text className='unit'>￥</Text>
                      {itemSalePriceRange || itemSkuLowSalePrice || filters.moneyFilter(itemSalePrice || 0, true)}
                    </Text>
                  ) : (
                    <Text className='offline-price'>{displayPrice || '到店咨询'}</Text>
                  )}

                  {!!showDiscount && (
                    <Text className='gi__label-discount'>
                      ￥{itemSkuHighSalePrice || filters.moneyFilter(itemLabelPrice || 0, true)}
                    </Text>
                  )}
                </View>
                <View className='n-goods-desc'>{itemSalesVolume}人已购买</View>
              </View>

              {showCartCount && (
                <View className='cart'>
                  <CartCount  left='50' bottom='56' spu={goodsItem} reportSource={reportSource} />
                </View>
              )}
            </View>
          </View>
        )}

        {/* 一行2个 */}
        {/* 使用一行两个的样式时，需要包裹一层container 具体使用可以参考poster-recommendation或者recommend-goods-list组件 */}

        {display === displayEnum.ONE_ROW_TWO && (
          <View className='retail n-goods-item-container oneRowTwo' onClick={this.handleClickGoodsItem}>
            <View className='n-goods-img-box'>
              <Image className='n-goods-img' mode='aspectFill' src={thumbnailOneRowTwo} />
              <GoodsStockTmpl
                isPreSell={isPreSell}
                picTags={picTags}
                itemStock={itemStock}
                isProduct={isProduct}
                attributeName={attributeName}
                tmpStyle={tmpStyle}
              />
            </View>
            <View className='n-goods-info-box'>
              <View className='n-goods-title limit-line line-2'>
                {
                  (goodsItem.wxItem.sourceType === 1 || goodsItem.wxItem.sourceType === 4) && (
                    // 显示自营商品的标签
                    <View className="xsd">
                      <Image className="xsd1" mode="aspectFit"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/zysp1.svg" />
                    </View>
                  )
                }
                <Text>{name}</Text>
              </View>
              {!!subName && <View className='n-goods-sub-title limit-line'>{subName}</View>}

              <View className='n-price-container'>
                <View className='n-price-box'>
                  <View className='n-price' style={{ color: tmpStyle.btnColor }}>
                    {itemSalesType !== salesTypeEnum.OFFLINE ? (
                      <View>
                        ￥{itemSalePriceRange || itemSkuLowSalePrice || filters.moneyFilter(itemSalePrice || 0, true)}
                      </View>
                    ) : (
                      <View className='offline-price'>{displayPrice || '到店咨询'}</View>
                    )}

                    {showCartCount && (
                      <View className={classNames(['cart', { 'hide-discount': !showDiscount }])}>
                        <CartCount spu={goodsItem} reportSource={reportSource} />
                      </View>
                    )}
                  </View>
                  <View className='footer-content'>
                    {showDiscount ? (
                      <Text className='gi__label-discount'>
                        ￥{itemSkuHighSalePrice || filters.moneyFilter(itemLabelPrice || 0, true)}
                      </Text>
                    ) : null}
                    <View className='n-goods-desc'>{itemSalesVolume}人已购买</View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        )}

        {/* 一行3个 */}
        {display === displayEnum.ONE_ROW_THREE && (
          <View className='n-goods-item-container oneRowThree' onClick={this.handleClickGoodsItem}>
            <View className='n-goods-img-box'>
              <Image className='n-goods-img' mode='aspectFill' src={thumbnail} />

              <GoodsStockTmpl
                isPreSell={isPreSell}
                picTags={picTags}
                itemStock={itemStock}
                isProduct={isProduct}
                attributeName={attributeName}
                tmpStyle={tmpStyle}
              />
            </View>

            <View className='n-goods-title limit-line'>
              {
                (goodsItem.wxItem.sourceType === 1 || goodsItem.wxItem.sourceType === 4) && (
                  // 显示自营商品的标签
                  <View className="xsd">
                    <Image className="xsd1" mode="aspectFit"
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/images/zysp1.svg" />
                  </View>
                )
              }
              <Text>{name}</Text>
            </View>

            <View className='n-price-container'>
              <View className='n-price-box'>
                <View className='n-price' style={{ color: tmpStyle.btnColor }}>
                  {itemSalesType !== salesTypeEnum.OFFLINE ? (
                    <View>
                      ￥{itemSalePriceRange || itemSkuLowSalePrice || filters.moneyFilter(itemSalePrice || 0, true)}
                    </View>
                  ) : (
                    <View className='offline-price'>{displayPrice || '到店咨询'}</View>
                  )}
                </View>
              </View>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default GoodsItem;
