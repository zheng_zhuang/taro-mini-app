import React, { FC, useState, useEffect } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../utils/money.wxs.js';
import wxApi from '../../utils/wxApi';

import './index.scss';

type IProps = {
  dataSource?: Record<string, any>;
  coupon: Array<string>;
  eventType: string;
  payOrderNo?: string;
  visible: boolean;
  backOpacity?: string;
  onClose?: () => void;
};

let CouponDialog: FC<IProps> = ({ coupon, visible, onClose }) => {
  const [_visible, setVisible] = useState(visible);

  useEffect(() => {
    if (_visible !== visible) {
      setVisible(visible);
    }
  }, [visible]);

  function preventTouchMove() {
    //阻止滑动
  }

  function onGotoUse() {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/home/index',
    });
  }

  //隐藏弹框
  function hideDialog() {
    setVisible(false);
  }

  //展示弹框
  function showDialog() {}

  /**
   * 隐藏dialog
   */
  function handleClose() {
    hideDialog();
    // this.triggerEvent('handleCouponDialog');
    onClose && onClose();
  }

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-wcc-CouponDialog'
      className='wk-wcc-CouponDialog'
    >
      {!!_visible && (
        <View className='share-gift' onTouchMove={preventTouchMove}>
          <View className='mask'></View>
          {/*  优惠券  */}
          <View className='wrapper'>
            <View className='coupon_list'>
              <View className='inner_wrapper'>
                {!!coupon.length && (
                  <Block>
                    {coupon.map((item, index) => {
                      return (
                        <View className='coupon_item_wrapper' key={item.id}>
                          <View className='coupon_item_amount'>
                            {item.couponCategory === 2 ? (
                              <View className='item_amount_wrapper'>
                                <View className='item item_amount'>
                                  {filters.discountFilter(item.discountFee, true)}
                                </View>
                                <View className='item item_type'>折</View>
                              </View>
                            ) : (
                              <View className='item_amount_wrapper'>
                                <View className='item item_amount'>{filters.moneyFilter(item.discountFee, true)}</View>
                                <View className='item item_type'>元</View>
                              </View>
                            )}

                            <View className='item_condition'>
                              {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                            </View>
                          </View>
                          {/*  detail  */}
                          <View className='coupon_item_detail'>
                            <View className='detail_title limit-line'>{item.name}</View>
                            <View className='detail_exp'>{'有效期至:' + filters.dateFormat(item.endTime)}</View>
                            {!!item.couponSendAmount && (
                              <View className='detail_count'>{'×' + item.couponSendAmount + '张'}</View>
                            )}
                          </View>
                          {/*  circle  */}
                          <View className='coupon_item_link' onClick={onGotoUse}>
                            去使用
                          </View>
                        </View>
                      );
                    })}
                  </Block>
                )}
              </View>
            </View>
            {/*  优惠券标题  */}
            <View className='coupon_title_wrapper'>
              <View className='coupon_title'></View>
              <View className='coupon_title'></View>
              <View className='coupon_title'>
                <View className='main_title'>你有1个红包大礼</View>
                <View className='sub_title'>快快领取下单使用吧！</View>
              </View>
            </View>
            {/*  关闭按钮  */}
            <View onClick={handleClose} className='button'></View>
          </View>
        </View>
      )}
    </View>
  );
};

CouponDialog.defaultProps = {
  dataSource: {},
  coupon: [],
  eventType: '0',
  visible: false,
  backOpacity: '0.7',
};

export default CouponDialog;
