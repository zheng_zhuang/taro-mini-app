import React from 'react'
import { connect } from 'react-redux';
import hoc from '@/hoc';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import store from '@/store';
import { updateGlobalDataAction } from '@/redux/global-data';
import { View, Input } from '@tarojs/components';
import './index.scss'
const app = store.getState()
interface IProps{
  updateRoomCode:Boolean,
  codeId:any,
  qrCodeNumber?:any,
  hotelId?:any,
  currentStore?:any,
  roomCode?:any,
  onIsUpdateRoomCode:Function
}
const mapStateToProps = (state) => ({
  currentStore: state.globalData.currentStore,
  roomCode:state.globalData.roomCode
});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class RoomCode extends React.Component<IProps> {
  inputRoomCode =({ detail: { value } })=>{
    store.dispatch(updateGlobalDataAction({
      roomCode: value
    }));
  }
  updateRoomCode=()=>{
    if (!this.props.roomCode) {
     this.props.onIsUpdateRoomCode()
    }
    wxApi.request({
      url: api.insertRoomCode,
      isLoginRequest: true,
      method: 'POST',
      data: {
        id: this.props.codeId,
        hotelId: this.props.hotelId,
        qrCodeNumber: this.props.qrCodeNumber,
        roomCode: this.props.roomCode,
      },
    })
    wxApi.showToast({
      title: '绑定成功',
      icon: 'success',
      duration: 1500,
    })
    this.props.onIsUpdateRoomCode()
  }
  render(){
    const {updateRoomCode,currentStore} = this.props
    return(
      updateRoomCode ? <View className="room-code">
        <View className="room-code-container">
          <View></View>
          <View className="current-name">{currentStore?.name}</View>
          <Input className="input" placeholder="请输入房间号" placeholder-style="color:#999" onInput={this.inputRoomCode} />
          <View className="room" onClick={this.updateRoomCode}>绑定房间号</View>
        </View>
       </View> : null
    )
  }
}
export default RoomCode
