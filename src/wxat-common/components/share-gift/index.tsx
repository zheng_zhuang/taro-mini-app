import React, { FC, useState, useEffect } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';

import filters from '../../utils/money.wxs.js';
import wxApi from '../../utils/wxApi';
import api from '../../api/index.js';
import cdnResConfig from '../../constants/cdnResConfig.js';

import './index.scss';

const _cdnResConfig = cdnResConfig.share_gift;

type IProps = {
  onClose: () => void;
};

type StateType = {
  base: {
    currentStore: {
      id: number;
      appId: number;
    };
  };
};

interface RewardDetails {
  name: string;
  suitItemType: number;
  discountFee: number;
  minimumFee: number;
}

/**
 * 分享有礼
 */
let ShareGift: FC<IProps> = ({ onClose }) => {
  const [ticketData, setTicketData] = useState();
  const [pointData, setPointData] = useState();
  const [integralAmount, setIntegralAmount] = useState<number>();
  const [visible, setVisible] = useState(false);
  const [rewardDetails, setRewardDetails] = useState<RewardDetails>({
    name: '',
    suitItemType: 0,
    discountFee: 0,
    minimumFee: 0,
  });

  const currentStore = useSelector((state: StateType) => state.base.currentStore);

  useEffect(() => {
    console.log('shareGift attched');
    getShareGift(); //调查看分享有礼活动详情接口
  }, [currentStore]);

  const getShareGift = () => {
    console.log('getShareGift request', currentStore.id, currentStore.appId);
    const params = {
      per_appId: currentStore.appId,
      storeId: currentStore.id,
    };

    wxApi
      .request({
        url: api.shareGift.query,
        loading: false,
        data: params,
      })
      .then((res) => {
        console.log('getShareGift then', res);
        //查询优惠券详情
        if (res.data && res.data.length) {
          addRecord({
            id: res.data[0].id,
            integralAmount: res.data[0].integralAmount,
            couponId: res.data[0].couponTicketId,
          });
        }
        //无详情-关闭分享有礼
        else {
          // triggerEvent('closeDialog');
          onClose && onClose();
        }
      })
      .catch((err) => {
        console.log('getShareGift catch', err);
        onClose && onClose();
        console.error('查看分享有礼详情err', err);
      });
  };

  const addRecord = ({ id, integralAmount, couponId }: { id?: number; integralAmount?: number; couponId?: number }) => {
    const params = {
      sharePresentInfoId: id != null ? id : null,
      shareReward: integralAmount ? 1 : 2, //1-积分 2-优惠券
      rewardDetails: integralAmount ? integralAmount : couponId,
    };

    // 积分数
    if (integralAmount) {
      setIntegralAmount(integralAmount);
    }

    console.log('addRecord request', params);
    wxApi
      .request({
        url: api.shareGift.addRecordUser,
        method: 'POST',
        loading: false,
        quite: true,
        data: params,
      })
      .then((res) => {
        console.log('addRecord then', res);
        if (!res.success || !res.data) {
          onClose && onClose();
          return;
        }

        setVisible(true);
        //积分 1
        if (res.data.shareReward == 1) {
          //显示积分信息

          setRewardDetails(res.data.rewardDetails);
          setPointData(res.data);
          setTicketData(undefined);
        }
        //优惠券 2
        else if (res.data.shareReward == 2) {
          let _rewardDetails = JSON.parse(res.data.rewardDetails);
          //限制优惠券名字过长的问题
          let name = _rewardDetails.name || '';
          if (name.length > 8) {
            _rewardDetails.name = name.substr(0, 8) + '...';
          }
          //显示优惠券信息
          setRewardDetails(_rewardDetails);
          setPointData(undefined);
          setTicketData(res.data);
        } else {
          onClose && onClose();
        }
      })
      .catch((err) => {
        console.log('addRecord catch', err);
        onClose && onClose();
      });
  };

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-wcs-ShareGift' className='wk-wcs-ShareGift'>
      {!!visible && (
        <View className='share-gift'>
          <View className='mask'></View>
          {/* 优惠券 */}
          {!!ticketData && (
            <View className='share-container'>
              <Image className='share-container-img' src={_cdnResConfig.bg}></Image>
              <Image className='share-float' src={_cdnResConfig.float}></Image>
              <Text className='share-title'>分享有礼</Text>
              <Text className='share-content'>恭喜您！获得一张优惠券</Text>
              <View className='share-ticket'>
                <Image className='share-ticket-img' src={_cdnResConfig.ticket}></Image>
                <View className='share-left'>
                  <Text className='share-ticket-title'>{rewardDetails.name}</Text>
                  <Text className='share-category'>
                    {'指定商品：' +
                      (rewardDetails.suitItemType == 0
                        ? '所有商品'
                        : rewardDetails.suitItemType == 1
                        ? '部分商品'
                        : '品类券')}
                  </Text>
                  <Text className='share-date'></Text>
                </View>
                <View className='share-right'>
                  <Text className='share-amount'>{'¥' + filters.moneyFilter(rewardDetails.discountFee, true)}</Text>
                  <Text className='share-condition'>
                    {rewardDetails.minimumFee
                      ? '满' + filters.moneyFilter(rewardDetails.minimumFee, true) + '￥可用'
                      : '无消费门槛'}
                  </Text>
                </View>
              </View>
              <Button className='share-btn-know' onClick={onClose}>
                知道了
              </Button>
              <View className='share-close' onClick={onClose}>
                X
              </View>
            </View>
          )}

          {/* 积分 */}
          {!!pointData && (
            <View className='share-container'>
              <Image className='share-container-img' src={_cdnResConfig.bg}></Image>
              <Image className='share-float' src={_cdnResConfig.float}></Image>
              <Text className='share-title'>分享有礼</Text>
              <Text className='share-content'>{'恭喜您！获得+' + integralAmount + '积分'}</Text>
              <View className='share-point'>
                <Image className='share-point-img' src={_cdnResConfig.point}></Image>
              </View>
              <Button className='share-btn-know' onClick={onClose}>
                知道了
              </Button>
              <View className='share-close' onClick={onClose}>
                X
              </View>
            </View>
          )}
        </View>
      )}
    </View>
  );
};

export default ShareGift;
