function toCamel(name: string) {
  return name.replace(/\-(\w)/, (_, ch: string) => {
    return ch.toUpperCase();
  });
}

export default function styleToObj(style = '') {
  const pairArr = (style || '').trim().split(';');
  const kvArr = pairArr
    .map((p) => p.trim())
    .filter((p) => p)
    .map((pair) => pair.split(/:(?=[\s\w#]+)/));
  const result = kvArr.reduce((obj, [key, value]) => {
    obj[toCamel(key).trim()] = (value || '').trim();
    return obj;
  }, {});
  return result;
}
