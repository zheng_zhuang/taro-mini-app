import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro, { VideoContext } from '@tarojs/taro';
import styleToObj from './utils/styleToObj';
import { View } from '@tarojs/components';
import Parse from './Parse';
import { ITouchEvent } from '@tarojs/components/types/common';
import wxApiProxy from '@/wxat-common/utils/wxAppProxy';
import wxApi from '@/wxat-common/utils/wxApi';
import wxParseImgService from '@/wxat-common/utils/wxParseImgService';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox';

import ParseContext from './context';

export type ParseWrapperProps = {
  parseContent?: any;
};

let ParseWrapper = (props: ParseWrapperProps) => {
  const { parseContent = {} } = props;
  const { styleStr = '', nodes = [] } = parseContent || {};

  let playStatus = 'pause';
  let lastId: string;
  let lastContext: VideoContext;

  const handleVideoTap = (e: ITouchEvent, ctx: VideoContext) => {
    const curId = e.currentTarget.id;
    if (playStatus === 'pause') {
      ctx.play();
      playStatus = 'play';

      lastId = curId;
      lastContext = ctx;
    } else if (playStatus === 'play' && lastId === curId) {
      ctx.pause();
      playStatus = 'pause';
    } else if (playStatus === 'play' && lastId !== curId) {
      lastContext && lastContext.pause();
      ctx.play();

      lastId = curId;
      lastContext = ctx;
    }
  };

  const handleMiniprogramTap = (e: ITouchEvent) => {
    const url = e.currentTarget.dataset.path as string;
    const classifyPath = 'wxat-common/pages/classify/index';
    const cartPath = 'wxat-common/pages/tabbar-shopping-cart/index';
    if (url === classifyPath) {
      wxApiProxy.jumpToClassify(undefined, undefined);
    } else if (url === cartPath) {
      wxApiProxy.jumpToCart(undefined, undefined);
    } else {
      wxApi.$navigateTo({ url });
    }
  };

  const handleImageTap = (e: ITouchEvent) => {
    const { dataset } = e.currentTarget;
    if (dataset && dataset.class && /icon-img/.test(dataset.class)) {
      return;
    }
    wxParseImgService(dataset, parseContent);
  };

  const handleGoVRTap = (ev: ITouchEvent) => {
    console.log(ev);
    const vrLink = ev.currentTarget.dataset.vrlink;

    protectedMailBox.send('sub-packages/marketing-package/pages/receipt/index', 'receiptURL', vrLink);

    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/receipt/index',
      data: {},
    });
  };

  return (
    <ParseContext.Provider
      value={{ ...parseContent, handleVideoTap, handleMiniprogramTap, handleImageTap, handleGoVRTap }}
    >
      <View style={_safe_style_(styleToObj(styleStr))}>
        {nodes.map((node) => (
          <Parse key={node.index} node={node} />
        ))}
      </View>
    </ParseContext.Provider>
  );
};

export default ParseWrapper;
