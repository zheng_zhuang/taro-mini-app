import React, { useContext, FC } from 'react';
import { useVideoRef, _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import styleToObj from '../utils/styleToObj';
import { View, Video } from '@tarojs/components';
import classNames from 'classnames';
import { ParseProps } from './parse';
import { ITouchEvent } from '@tarojs/components/types/common';
import ParseContext from '../context';
import ParseMiniprogram from './ParseMiniprogram';

let ParseIframe: FC<ParseProps> = (props) => {
  const { classStr, tag, styleStr, src, attr = {} } = props;
  const { handleVideoTap } = useContext(ParseContext);
  const videoCtx = useVideoRef('wxparse-iframe');

  const handleVideoTapWrap = (e: ITouchEvent) => {
    const ctx = videoCtx.current;
    if (ctx) {
      handleVideoTap && handleVideoTap(e, ctx);
    }
  };

  return (
    <View className={classNames(classStr, `wxParse-${tag}`)} style={_safe_style_(styleToObj(styleStr))}>
      {!!attr['data-miniprogram-appid'] ? (
        <ParseMiniprogram {...props} />
      ) : (
        <Video
          className={classNames()}
          style={{ width: '100%' }}
          id='wxparse-iframe'
          src={src || attr.src}
          onClick={handleVideoTapWrap}
          ref={videoCtx}
        ></Video>
      )}
    </View>
  );
};

ParseIframe.defaultProps = {
  classStr: '',
  tag: '',
  styleStr: '',
  index: -1,
  attr: {},
};

ParseIframe.options = {
  styleIsolation: 'shared',
};

export default ParseIframe;
