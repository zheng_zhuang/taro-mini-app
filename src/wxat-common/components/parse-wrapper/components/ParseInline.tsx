import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import classNames from 'classnames';
import styleToObj from '../utils/styleToObj';
import Parse from '../Parse';
import type { ParseProps } from './parse';

let ParseInline: FC<ParseProps> = (props) => {
  const { classStr, tag, tagType, styleStr, nodes } = props;
  return (
    <View
      className={classNames(classStr, `wxParse-${tag}`, `wxParse-${tagType}`)}
      style={_safe_style_(styleToObj(styleStr))}
    >
      {nodes ? nodes.map((node) => <Parse key={node.index} node={node} />) : null}
    </View>
  );
};

ParseInline.defaultProps = {
  classStr: '',
  tag: '',
  tagType: '',
  styleStr: '',
  nodes: [],
};

ParseInline.options = {
  styleIsolation: 'shared',
};

export default ParseInline;
