import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import classNames from 'classnames';
import { View } from '@tarojs/components';
import styleToObj from '../utils/styleToObj';
import Parse from '../Parse';
import { ParseProps } from './parse';

let ParseLink: FC<ParseProps> = (props) => {
  const { tag, classStr, styleStr, nodes = [], attr = {} } = props;
  return (
    <View
      className={classNames(`wxParse-inline`, classStr, `wxParse-${tag}`)}
      style={_safe_style_(styleToObj(styleStr))}
      data-src={attr['href']}
    >
      {nodes ? nodes.map((node) => <Parse key={node.index} node={node} />) : null}
    </View>
  );
};

ParseLink.defaultProps = {
  tag: '',
  classStr: '',
  styleStr: '',
  nodes: [],
  attr: {},
};

ParseLink.options = {
  styleIsolation: 'shared',
};

export default ParseLink;
