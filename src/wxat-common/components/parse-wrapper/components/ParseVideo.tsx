import React, { useContext, FC } from 'react';
import { useVideoRef, _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { Video, View } from '@tarojs/components';
import classNames from 'classnames';
import styleToObj from '../utils/styleToObj';
import { ParseProps } from './parse';
import { ITouchEvent } from '@tarojs/components/types/common';
import ParseContext from '../context';

let ParseVideo: FC<ParseProps> = (props) => {
  const { classStr, tag, styleStr, attr = {} } = props;

  const { handleVideoTap } = useContext(ParseContext);
  const videoCtx = useVideoRef('wxparse-video');

  const handleVideoTapWrap = (e: ITouchEvent) => {
    const ctx = videoCtx.current;
    if (ctx) {
      ctx.pause();
      handleVideoTap && handleVideoTap(e, ctx);
    }
  };

  return (
    <View style={_safe_style_(styleToObj(styleStr))} className={classNames(classStr, `wxParse-${tag}`)}>
      <Video
        className={classNames(classStr, `wxParse-${tag}`)}
        id='wxparse-video'
        style={_safe_style_(styleToObj(styleStr))}
        src={attr['src']}
        onClick={handleVideoTapWrap}
        ref={videoCtx}
      />
    </View>
  );
};

ParseVideo.defaultProps = {
  classStr: '',
  tag: '',
  styleStr: '',
  attr: {},
};

ParseVideo.options = {
  styleIsolation: 'shared',
};

export default ParseVideo;
