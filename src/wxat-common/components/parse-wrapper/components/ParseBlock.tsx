import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import classNames from 'classnames';
import Parse from '../Parse';
import { View } from '@tarojs/components';
import styleToObj from '../utils/styleToObj';

import { ParseProps } from './parse';

let ParseBlock: FC<ParseProps> = (props) => {
  const { tag, tagType, classStr, styleStr, nodes = [] } = props;
  return (
    <View
      className={classNames(classStr, `wxParse-${tag}`, `wxParse-${tagType}`)}
      style={_safe_style_(styleToObj(styleStr))}
    >
      {nodes ? nodes.map((node) => <Parse key={node.index} node={node} />) : null}
    </View>
  );
};

ParseBlock.options = {
  styleIsolation: 'shared',
};

ParseBlock.defaultProps = {
  tag: '',
  tagType: '',
  classStr: '',
  styleStr: '',
  nodes: [],
};

export default ParseBlock;
