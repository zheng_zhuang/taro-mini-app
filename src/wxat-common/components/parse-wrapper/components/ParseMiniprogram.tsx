import React, { useContext, FC } from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import { ParseProps } from './parse';
import ParseContext from '../context';

let ParseMiniprogram: FC<ParseProps> = (props) => {
  const { attr = {} } = props;
  const type = attr['data-miniprogram-wk-type'];

  const { handleMiniprogramTap } = useContext(ParseContext);

  if (type === 'card') {
    return (
      <View
        style={{
          border: '1px solid rgba(226, 226, 226, 1)',
          boxSizing: 'border-box',
          width: '100%',
          background: '#fff',
          boxShadow: '0px 0px 6rpx 1px rgba(240, 240, 240, 0.5)',
          borderRadius: Taro.pxTransform(6),
          margin: '0 auto',
        }}
        onClick={_fixme_with_dataset_(handleMiniprogramTap, { path: attr['data-miniprogram-path'] })}
      >
        <View className='minapp-wrapper-inner'>
          <View
            style={{
              margin: Taro.pxTransform(20),
              fontSize: Taro.pxTransform(32),
              fontFamily: 'PingFangSC-Regular, PingFang SC',
              fontWeight: 400,
              color: 'rgb(102, 102, 102)',
            }}
          >
            {attr['data-miniprogram-title']}
          </View>
          <View
            style={{
              margin: Taro.pxTransform(20),
              backgroundImage: `url(${attr['data-miniprogram-imageurl']})`,
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
              backgroundSize: 'contain',
              height: Taro.pxTransform(436),
              boxSizing: 'border-box',
            }}
          ></View>
          <View
            style={{
              height: Taro.pxTransform(74),
              padding: `0 ${Taro.pxTransform(10)}`,
              lineHeight: Taro.pxTransform(74),
              borderTop: `${Taro.pxTransform(1)} solid rgb(226, 226, 226)`,
              boxSizing: 'border-box',
              fontSize: Taro.pxTransform(24),
              fontFamily: 'PingFangSC-Regular, PingFang SC',
              fontWeight: 400,
              color: `rgba(153, 153, 153, 1)`,
            }}
          >
            <View className='miniprogram-card-icon' />
            小程序
          </View>
        </View>
      </View>
    );
  } else if (type === 'text') {
    return (
      <View
        className='minapp-wrapper'
        onClick={_fixme_with_dataset_(handleMiniprogramTap, { path: attr['data-miniprogram-path'] })}
      >
        <View className='minapp-wrapper-inner'>
          <View className='miniprogram-text-icon' />
          <View
            style={{
              display: 'inline-block',
              margin: 0,
              verticalAlign: 'middle',
              paddingLeft: Taro.pxTransform(15),
              color: '#4379ed',
            }}
          >
            {attr['data-miniprogram-title']}
          </View>
        </View>
      </View>
    );
  } else if (type === 'image') {
    return (
      <View
        className='minapp-wrapper'
        onClick={_fixme_with_dataset_(handleMiniprogramTap, { path: attr['data-miniprogram-path'] })}
      >
        <View className='minapp-wrapper-inner'>
          <View className='miniprogram-image-icon' />
          <Image
            src={attr['data-miniprogram-imageurl']}
            mode='widthFix'
            style={{ margin: 0, padding: 0, width: '100%', height: 'auto' }}
          />
        </View>
      </View>
    );
  }
  return null;
};

ParseMiniprogram.defaultProps = {
  attr: {},
};

ParseMiniprogram.options = {
  styleIsolation: 'shared',
};

export default ParseMiniprogram;
