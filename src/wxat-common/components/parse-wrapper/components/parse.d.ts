export type TextItem = {
  text: string;
  node: string;
  baseSrc: string;
  index: string;
};

export type ParseProps = {
  tagType?: string;
  tag?: string;
  classStr?: string;
  attr?: Record<string, string>;
  from?: string;
  styleStr?: string;
  width?: string;
  imgIndex?: string;
  nodes?: any[];
  index?: number;
  src?: string;
  textArray?: Array<TextItem>;
};
