import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import classNames from 'classnames';
import Parse from '../Parse';
import styleToObj from '../utils/styleToObj';
import { ParseProps } from './parse';

let ParseLi: FC<ParseProps> = (props) => {
  const { classStr, styleStr, nodes } = props;

  return (
    <View className={classNames('wxParse-li-text', classStr)} style={_safe_style_(styleToObj(styleStr))}>
      <View className={classNames('wxParse-li-inner', classStr)}>
        <View className={classNames('wxParse-li-text', classStr)}>
          {nodes ? nodes.map((node) => <Parse key={node.index} node={node} />) : null}
        </View>
      </View>
    </View>
  );
};

ParseLi.defaultProps = {
  classStr: '',
  styleStr: '',
  nodes: [],
};

ParseLi.options = {
  styleIsolation: 'shared',
};

export default ParseLi;
