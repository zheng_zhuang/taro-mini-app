import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import classNames from 'classnames';
import styleToObj from '../utils/styleToObj';
import { ParseProps } from './parse';

let ParseText: FC<ParseProps> = (props) => {
  const { styleStr, textArray } = props;

  return (
    <View className={classNames('wxParse-inline')} style={_safe_style_(styleToObj(styleStr))}>
      {!!textArray
        ? textArray.map((item, index) => (
            <Text key={`${props.index}.${index}`}>
              {item.node === 'text' && <Text key={item.text}>{item.text}</Text>}
              {item.node === 'element' && <Image className='WxEmojiView wxEmoji' src={item.baseSrc + item.text} />}
            </Text>
          ))
        : null}
    </View>
  );
};

ParseText.defaultProps = {
  styleStr: '',
  textArray: [],
};

ParseText.options = {
  styleIsolation: 'shared',
};

export default ParseText;
