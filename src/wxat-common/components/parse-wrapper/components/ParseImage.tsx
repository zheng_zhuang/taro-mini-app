import React, { useContext, FC, useState } from 'react';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { Image } from '@tarojs/components';
import classNames from 'classnames';
import styleToObj from '../utils/styleToObj';
import { ParseProps } from './parse';
import ParseContext from '../context';
import wxApi from '@/wxat-common/utils/wxApi';

let ParseImage: FC<ParseProps> = (props) => {
  const { classStr = '', tag = '', attr = {}, from = '', styleStr = '', width = '', imgIndex = '' } = props;

  const { handleImageTap, handleGoVRTap } = useContext(ParseContext);
  const [imageWidth, setImageWidth] = useState(null);

  const realWindowWidth = wxApi.getSystemInfoSync().windowWidth;

  const imgLoaded = (e) => {
    if (from) {
      const recal = wxAutoImageCal(e.detail.width, e.detail.height);

      setImageWidth(recal.imageWidth);
    }
  };

  // 计算视觉优先的图片宽高
  const wxAutoImageCal = (originalWidth, originalHeight) => {
    //获取图片的原始长宽
    let windowWidth = 0,
      autoWidth = 0,
      autoHeight = 0;

    const results: Record<string, any> = {};
    const padding = (30 / 750) * realWindowWidth;
    windowWidth = realWindowWidth - 2 * padding;
    //判断按照那种方式进行缩放
    // console.log("windowWidth" + windowWidth);
    if (originalWidth > windowWidth) {
      //在图片width大于手机屏幕width时候
      autoWidth = windowWidth;
      // console.log("autoWidth" + autoWidth);
      autoHeight = (autoWidth * originalHeight) / originalWidth;
      // console.log("autoHeight" + autoHeight);
      results.imageWidth = autoWidth;
      results.imageheight = autoHeight;
    } else {
      //否则展示原来的数据
      results.imageWidth = originalWidth;
      results.imageheight = originalHeight;
    }
    return results;
  };

  if (attr['data-vrlink']) {
    return (
      <Image
        src={attr['src']}
        mode='widthFix'
        style={{ width: width || imageWidth + 'px', ...styleToObj(styleStr), maxWidth: '100%' }}
        onClick={_fixme_with_dataset_(handleGoVRTap, { vrlink: attr['data-vrlink'] })}
        onLoad={imgLoaded}
      />
    );
  }

  return (
    <Image
      className={classNames(classStr, `wxParse-${tag}`)}
      src={attr['src']}
      onClick={_fixme_with_dataset_(handleImageTap, {
        itemno: attr['data-itemno'],
        type: attr['data-type'] || 'default',
        goodstype: attr['data-goodstype'],
        src: attr['src'],
        from: from,
        idx: imgIndex,
        class: classStr,
      })}
      onLoad={imgLoaded}
      mode='widthFix'
      style={{ width: width || imageWidth + 'px', maxWidth: '100%', ...styleToObj(styleStr) }}
    />
  );
};

ParseImage.defaultProps = {
  classStr: '',
  tag: '',
  attr: {},
  from: '',
  styleStr: '',
  width: '',
  imgIndex: '',
};

ParseImage.options = {
  styleIsolation: 'shared',
};

export default ParseImage;
