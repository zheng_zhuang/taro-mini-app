import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import ParseIframe from './components/ParseIframe';
import ParseVideo from './components/ParseVideo';
import ParseText from './components/ParseText';
import ParseBlock from './components/ParseBlock';
import ParseInline from './components/ParseInline';
import ParseImage from './components/ParseImage';
import ParseLi from './components/ParseLi';
import ParseBr from './components/ParseBr';
import ParseLink from './components/ParseLink';
import ParseMiniprogram from './components/ParseMiniprogram';
import './wxParse.scss';

export type WXParseProps = {
  node: any;
};

let Parse: FC<WXParseProps> = (props) => {
  const { node } = props;

  if (!node) return null;
  if (node.node === 'element') {
    if (node.tag === 'mp-miniprogram') {
      return <ParseMiniprogram {...node} />;
    } else if (node.tag === 'img') {
      return <ParseImage {...node} />;
    } else if (node.tag === 'br') {
      return <ParseBr />;
    } else if (node.tag === 'iframe') {
      return <ParseIframe {...node} />;
    } else if (node.tag === 'video') {
      return <ParseVideo {...node} />;
    } else if (node.tag === 'li') {
      return <ParseLi {...node} />;
    } else if (node.tag === 'a') {
      return <ParseLink {...node} />;
    } else if (node.tagType == 'block') {
      return <ParseBlock {...node} />;
    } else if (node.tagType == 'inline') {
      return <ParseInline {...node} />;
    }
  } else if (node.node === 'text') {
    return <ParseText {...node} />;
  }
  return null;
};

Parse.options = {
  styleIsolation: 'shared',
};

export default Parse;
