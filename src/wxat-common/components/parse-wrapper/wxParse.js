/**
 * author: Di (微信小程序开发工程师)
 * organization: WeAppDev(微信小程序开发论坛)(http://weappdev.com)
 *               垂直微信小程序开发交流社区
 *
 * github地址: https://github.com/icindy/wxParse
 *
 * for: 微信小程序富文本解析
 * detail : http://weappdev.com/t/wxparse-alpha0-1-html-markdown/184
 */

/**
 * utils函数引入
 **/
import HtmlToJson from '../../lib/wxParse/html2json';

/**
 * 主函数入口区
 **/
function wxParse(
  type = 'html',
  data = '<div style="text-align: center">数据不能为空</div>',
  bindName = 'wxParseData',
  imagePadding
) {
  let transData = {}; //存放转化后的数据
  if (type == 'html') {
    transData = HtmlToJson.html2json(data, bindName);
  }
  transData.view = {};
  transData.view.imagePadding = 0;
  if (typeof imagePadding != 'undefined') {
    transData.view.imagePadding = imagePadding;
  }

  return transData;
}

export default wxParse;
