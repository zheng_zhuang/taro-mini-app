import React from 'react'; // @externalClassesConvered(BottomDialog)
import { connect } from 'react-redux';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import constants from "../../constants";
import GoodsAttribute from '../goods-attribute';
import GoodsAttributeNew from '../goods-attribute-new';
import BottomDialog from '../bottom-dialog';

const mapStateToProps = (state) => {
  return { newSkuItem: (state.base.appInfo || {}).newSkuItem };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class ChooseAttributeDialog extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    goodsType: constants.goods.type.product,
    goodsDetail: null, // 商品详情
    // 限购类型标题，有则传到子组件以便展示限购数量
    limitAmountTitle: null,
    showStock: true,
    newSkuItem: false,
    isDistributionServices: false,
    bookingList: false,
  };
  
  showAttributDialog() {
    this.bottomDialogCOMPT && this.bottomDialogCOMPT.showModal();
  }

  hideAttributeDialog() {
    this.bottomDialogCOMPT && this.bottomDialogCOMPT.hideModal();
  }

  handlerChoose(info) {
    const { isDistributionServices , bookingList } = this.props
    if( isDistributionServices ){
        info.isDistributionServices = 1
    }
    if( bookingList ){
      info.bookingList = 1
    }
    const onChoose = this.props.onChoose;
    onChoose && onChoose(info);
    this.hideAttributeDialog();
  }

  handleClosePopup() {
    this.hideAttributeDialog();
  }

  refBottomDialogCOMPT = (node) => (this.bottomDialogCOMPT = node);

  render() {
    const { goodsType, goodsDetail, limitAmountTitle, showStock, newSkuItem } = this.props;
    return (
      <BottomDialog ref={this.refBottomDialogCOMPT}>
        {newSkuItem ? (
          <GoodsAttributeNew
            goodsType={goodsType}
            goodsDetail={goodsDetail}
            limitAmountTitle={limitAmountTitle}
            showStock={showStock}
            onChoose={this.handlerChoose.bind(this)}
            onClosePopup={this.handleClosePopup.bind(this)}
          ></GoodsAttributeNew>
        ) : (
          <GoodsAttribute
            goodsType={goodsType}
            goodsDetail={goodsDetail}
            limitAmountTitle={limitAmountTitle}
            showStock={showStock}
            onChoose={this.handlerChoose.bind(this)}
            onClosePopup={this.handleClosePopup.bind(this)}
          ></GoodsAttribute>
        )}
      </BottomDialog>
    );
  }
}

export default ChooseAttributeDialog;
