import React, { FC, useEffect, useRef } from 'react';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import '@/wxat-common/utils/platform';

export interface DebounceButtonProps {
  className?: string;
  style?: React.CSSProperties;
  onClick: () => void;
  // 节流时间, 默认 3000
  time?: number;
}

let DebounceButton: FC<DebounceButtonProps> = (props) => {
  const timer = useRef<any>();
  const loading = useRef(false);
  const handleClick = () => {
    const defaultTime = props.time || 3000;
    const start = (time) => {
      timer.current = setTimeout(() => {
        loading.current = false;
      }, time);
    };

    if (loading.current) {
      // 继续延长
      clearTimeout(timer.current);
      start(defaultTime / 2);
      return;
    }

    loading.current = true;
    start(defaultTime);
    props.onClick();
  };

  useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  return (
    <View onClick={handleClick} className={props.className} style={props.style}>
      {props.children}
    </View>
  );
};

DebounceButton.options = {
  virtualHost: true,
};

export default DebounceButton;
