import React, { FC, useState, useImperativeHandle, useRef } from 'react'; // @externalClassesConvered(AnimatDialog)
import '@/wxat-common/utils/platform';
import { View, Image, Button, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';

import ButtonWithOpenType from '@/wxat-common/components/button-with-open-type';
import authHelper from '../../utils/auth-helper.js';
import api from "../../api";
import wxApi from '../../utils/wxApi';
import canvasHelper from '../../utils/canvas-helper.js';
import money from '../../utils/money.js';
import cdnResConfig from '../../constants/cdnResConfig.js';
import PostersDialog from "../posters-dialog";
import AnimatDialog from "../animat-dialog";
import shareUtil from '../../utils/share';
import './index.scss';
import SafeAreaPlaceholder from "../safe-area-placeholder";

import getStaticImgUrl from '../../constants/frontEndImgUrl'
import util from '@/wxat-common/utils/util';
const POSTER_TYPE = {
  goods: 'goods',
  redPacket: 'red-packet',
  internal: 'internal',
};

const POSTER_HEADER_TYPE = {
  avatar: 1,
  logo: 2,
};

// props的类型定义
interface IProps {
  posterData?: Record<string, any> | null;
  visible?: boolean;
  /**
   * 海报的二维码请求参数：
   * verificationNo: itemNo,
   * verificationType: 5,
   * maPath: 'wxat-common/pages/goods-detail/index'
   */
  qrCodeParams: Record<string, any> | null;
  /**
   * 是否为新的二维码接口，新的二维码接口请求结构有变
   * 接口变更为/auth/vip/verification/getNewQRCode
   * 直接传参sence和maPath，获取时也是直接获取
   */
  isNewQrCode?: boolean;
  posterType?: string;
  /**
   * 海报上面的提示语
   */
  posterTips: string;
  /**
   * 海报顶部logo，默认展示微信头像
   */
  posterLogo?: string;
  /**
   * 海报头部类型
   */
  posterHeaderType?: number;
  /**
   * 海报主图
   */
  posterImage?: string;
  /**
   * 海报图下面的名字
   */
  posterName?: string;
  posterItemNo?: string;
  /**
   * 海报优惠价格，已经转化过成元的价格
   */
  salePrice?: string;
  /**
   * 海报原价，已经转化过成元的价格
   */
  labelPrice?: string;
  /**
   * 海报优惠价标签
   */
  posterSalePriceLabel?: string;
  /**
   * 海报优惠价标签
   */
  posterLabelPriceLabel?: string;
  onSave: () => any;
  childRef?: any;
  /**
   * 二维码生成方式，使用映射ID的方式创建
   */
  uniqueShareInfoForQrCode?: object | null;
  /**
   * 是否需要展示海报原价
   */
  showLabelPrice?: boolean;
}

const ShareDialog: FC<IProps> = ({
  posterData,
  visible,
  isNewQrCode,
  qrCodeParams,
  posterType,
  posterHeaderType,
  posterLogo,
  posterTips,
  posterImage,
  posterName,
  posterItemNo,
  salePrice,
  labelPrice,
  posterSalePriceLabel,
  posterLabelPriceLabel,
  onSave,
  childRef,
  showLabelPrice,
  uniqueShareInfoForQrCode,
}) => {
  const [posterDialogVisible, setPosterDialogVisible] = useState(false);
  const [qrCodeUrl, setQrCodeUrl] = useState('');
  const posterSaveImageDataRef = useRef();
  const [UKtype, setUKtype] = useState('');
  const shareDialogRef = useRef<any>(null);

  // 需要暴露给外面使用的函数
  useImperativeHandle(childRef, () => ({
    hide,
    show,
    savePosterImage,
  }));

  // 创建海报
  const handleShowPosterDialog = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    getShareDialog().hide();

    setPosterDialogVisible(false);
    setQrCodeUrl('');

    if (uniqueShareInfoForQrCode) {
      apiQRCodeByUniqueKey();
    } else if (isNewQrCode) {
      apiQRCodeByScene();
    } else {
      apiQRCodeByParameter();
    }
    util.sensorsReportData('share_poster', {
      event_name: '分享海报',
      prod_item_name: posterName,
      prod_item_no: posterItemNo
    })
  };

  /**
   * 保存海报图片到本地
   */
  const handleSaveImage = (e) => {
    posterSaveImageDataRef.current = e;
    onSave && onSave();
  };
  /**
   * 直接关闭分享弹窗
   */
  const handleCloseShare = () => {
    hide();
  };
  const hide = () => {
    getShareDialog().hide(true);
  };
  const show = (e) => {
    if(e =='shareYouke'){
      setUKtype(e)
    }
    getShareDialog().show({
      scale: 1,
    });
  };

  const savePosterImage = (context) => {
    if (posterType === POSTER_TYPE.goods) {
      saveGoodsPosterImage(context);
    } else if (posterType === POSTER_TYPE.redPacket) {
      saveRedPacketPosterImage(context);
    }
  };

  const saveRedPacketPosterImage = (context) => {
    wxApi.showLoading({
      title: '正在保存图片...',
    });

    const { user } = posterSaveImageDataRef.current;
    const canvasContext = wxApi.createCanvasContext('shareCanvas', context);
    const imageRequests = [
      wxApi.getImageInfo({
        src: user.headUrl,
      }),

      wxApi.getImageInfo({
        src: cdnResConfig.redPacket.shareBg,
      }),
    ];

    if (qrCodeUrl) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: qrCodeUrl,
        })
      );
    }
    if (posterHeaderType === POSTER_HEADER_TYPE.logo && posterLogo) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: posterLogo,
        })
      );
    }
    Promise.all(imageRequests)
      .then((res) => {
        // 背景
        canvasHelper.drawImage(canvasContext, 0, 0, 275, 345, res[1].path);
        // canvasHelper.drawFillRect(canvasContext, 0, 0, 275, 345, '#fff');
        if (posterHeaderType === POSTER_HEADER_TYPE.avatar) {
          // 头像
          canvasHelper.drawCircleImage(canvasContext, 37, 22.5, 20, res[0].path);

          // 用户名
          canvasHelper.drawText(canvasContext, user.name, 64.5, 16, {
            textAlign: 'start',
            fillStyle: '#373A44',
            fontSize: 14,
          });

          // 红色三角
          canvasHelper.drawFillLine(
            canvasContext,
            [
              {
                x: 64.5,
                y: 32,
              },

              {
                x: 68.5,
                y: 30,
              },

              {
                x: 68.5,
                y: 34,
              },
            ],

            '#F0556C'
          );

          // 红色方框
          canvasHelper.drawFillRect(canvasContext, 68.5, 22, 150, 20, '#F0556C');

          // 拼团、砍价 商品详情文案
          canvasHelper.drawText(canvasContext, posterTips, 80, 37, {
            textAlign: 'start',
            fillStyle: '#ffffff',
            fontSize: 13,
          });
        } else if (posterHeaderType === POSTER_HEADER_TYPE.logo) {
          // 有二维码：顶部logo放在第四个图片，无二维码，顶部logo放在第三个
          if (qrCodeUrl && res[3] && res[3].path) {
            canvasHelper.drawImage(canvasContext, 17, 0, 240, 45, res[3].path);
          } else if (!qrCodeUrl && res[2] && res[2].path) {
            canvasHelper.drawImage(canvasContext, 17, 0, 240, 45, res[3].path);
          } else {
            canvasHelper.drawFillRect(canvasContext, 17, 0, 240, 45, '#fff');
          }
        }

        const totalWidth = 275;
        let name = posterData.name;
        if (name && name.length > 15) {
          name = name.substr(0, 15) + '...';
        }
        // 红包名
        canvasHelper.drawText(canvasContext, name, 40, 90, {
          textAlign: 'start',
          fillStyle: '#DF9958',
          fontSize: 15,
          totalWidth: 200,
        });

        // 红包金额
        const redPacketMoney = money.fen2YuanFixed(posterData.totalFee, true) + '元';
        canvasHelper.drawText(canvasContext, redPacketMoney, 0, 130, {
          textAlign: 'start',
          fillStyle: '#E53232',
          fontSize: 29,
          totalWidth: totalWidth,
        });

        // 活动日期label
        const label = '活动截止日期:';
        canvasHelper.drawText(canvasContext, label, 0, 205, {
          textAlign: 'start',
          fillStyle: '#ffffff',
          fontSize: 14,
          totalWidth: totalWidth,
        });

        // 活动日期
        const date = posterData.endTime;
        canvasHelper.drawText(canvasContext, date, 0, 225, {
          textAlign: 'start',
          fillStyle: '#ffffff',
          fontSize: 14,
          totalWidth: totalWidth,
        });

        // 二维码
        if (qrCodeUrl && res[2]) {
          canvasHelper.drawCircleImage(canvasContext, 140, 280, 38.5, res[2].path);
        }

        canvasContext.draw(false, (res) => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'shareCanvas',
              },

              context
            )
            .then((res) => {
              return wxApi.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
              });
            })
            .then((res) => {
              wxApi.showToast({
                title: '已保存到相册',
              });
            })
            .catch((error) => {
              console.log('saveImage error: ', error);
              wxApi.showToast({
                title: '保存失败',
                icon: 'none',
              });
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      })
      .catch((error) => {
        wxApi.hideLoading();
      });
  };

  const saveGoodsPosterImage = (context) => {
    wxApi.showLoading({
      title: '正在保存图片...',
    });

    const { user } = posterSaveImageDataRef.current;
    const canvasContext = wxApi.createCanvasContext('shareCanvas', context);
    const imageRequests = [
      wxApi.getImageInfo({
        src: user.headUrl,
      }),

      wxApi.getImageInfo({
        src: posterImage,
      }),
    ];

    if (qrCodeUrl) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: qrCodeUrl,
        })
      );
    }
    if (posterHeaderType === POSTER_HEADER_TYPE.logo && posterLogo) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: posterLogo,
        })
      );
    }

    Promise.all(imageRequests)
      .then((res) => {
        // 背景
        canvasHelper.drawFillRect(canvasContext, 0, 0, 260, 415, '#fff');
        if (posterHeaderType === POSTER_HEADER_TYPE.avatar) {
          // 头像
          canvasHelper.drawCircleImage(canvasContext, 42.5, 22.5, 20, res[0].path);

          // 用户名
          canvasHelper.drawText(canvasContext, user.name, 70, 16, {
            textAlign: 'start',
            fillStyle: '#373A44',
            fontSize: 14,
          });

          // 红色三角
          canvasHelper.drawFillLine(
            canvasContext,
            [
              {
                x: 70,
                y: 32,
              },

              {
                x: 74,
                y: 30,
              },

              {
                x: 74,
                y: 34,
              },
            ],

            '#F0556C'
          );

          // 红色方框
          canvasHelper.drawFillRect(canvasContext, 74, 22, 140, 20, '#F0556C');

          // 拼团、砍价 商品详情文案
          canvasHelper.drawText(canvasContext, posterTips, 80, 37, {
            textAlign: 'start',
            fillStyle: '#ffffff',
            fontSize: 13,
          });
        } else if (posterHeaderType === POSTER_HEADER_TYPE.logo) {
          // 有二维码：顶部logo放在第四个图片，无二维码，顶部logo放在第三个
          if (qrCodeUrl && res[3] && res[3].path) {
            canvasHelper.drawImage(canvasContext, 22.5, 0, 215, 45, res[3].path);
          } else if (!qrCodeUrl && res[2] && res[2].path) {
            canvasHelper.drawImage(canvasContext, 22.5, 0, 215, 45, res[2].path);
          } else {
            canvasHelper.drawImage(canvasContext, 22.5, 0, 215, 45, res[3].path);
          }
        }

        // 商品图片
        canvasHelper.drawImage(canvasContext, 22.5, 48.5, 215, 215, res[1].path);

        // 商品名
        canvasHelper.drawText(canvasContext, posterName, 22.5, 280, {
          textAlign: 'start',
          fillStyle: '#373A44',
          fontSize: 14,
          maxWidth: 215,
        });

        // 活动方框
        canvasHelper.drawFillRect(canvasContext, 22.5, 334.5, 45, 18.5, '#FFD9D9');
        canvasHelper.drawText(canvasContext, posterSalePriceLabel, 27.5, 348, {
          textAlign: 'start',
          fillStyle: '#E53232',
          fontSize: 11,
          bold: true,
        });

        // 价格
        canvasHelper.drawText(canvasContext, '￥' + salePrice, 22.5, 380, {
          textAlign: 'start',
          fillStyle: '#F0556C',
          fontSize: 17,
          bold: true,
        });

        // 原价
        if (showLabelPrice) {
          canvasHelper.drawText(canvasContext, posterLabelPriceLabel + '￥' + labelPrice, 24.5, 400, {
            textAlign: 'start',
            fillStyle: '#A8A8A8',
            fontSize: 12,
          });
        }

        if (qrCodeUrl && res[2]) {
          canvasHelper.drawImage(canvasContext, 174, 332, 69, 69, res[2].path);
        }

        canvasContext.draw(false, () => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'shareCanvas',
              },

              context
            )
            .then((res) => {
              return wxApi.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
              });
            })
            .then(() => {
              wxApi.showToast({
                title: '已保存到相册',
              });
            })
            .catch((error) => {
              console.log('saveImage error: ', error);
              wxApi.showToast({
                title: '保存失败',
                icon: 'none',
              });
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      })
      .catch(() => {
        wxApi.hideLoading();
      });
  };

  // 获取分享对话弹框
  const getShareDialog = () => {
    return shareDialogRef.current;
  };

  const handleShareToFriend = () => {
    util.sensorsReportData('share_friend', {
      event_name: '分享朋友圈',
      prod_item_name: posterName,
      prod_item_no: posterItemNo
    })
    hide();
  };

  const apiQRCodeByUniqueKey = () => {
    const publicArguments = shareUtil.buildSharePublicArguments({
      bz: '',
      forServer: true,
    });

    const scene = Object.assign({}, publicArguments, uniqueShareInfoForQrCode);
    wxApi
      .request({
        url: api.generatorMapperQrCode,
        loading: true,
        data: {
          shareParam: JSON.stringify(scene),
          urlPath: scene.page,
        },
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data);
      });
  };

  const apiQRCodeByParameter = () => {
    wxApi
      .request({
        url: api.verification.getQRCode,
        loading: true,
        data: qrCodeParams,
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data.qrCode);
      });
  };

  const apiQRCodeByScene = () => {
    wxApi
      .request({
        url: api.verification.getQRCodeV2,
        loading: true,
        data: {
          maPath: qrCodeParams!.maPath,
          scene: qrCodeParams!.verificationNo,
        },
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data.qrCode);
      });
  };

  return (

    <View data-fixme='02 block to view. need more test' data-scoped='wk-wcs-ShareDialog' className='wk-wcs-ShareDialog'>
      <AnimatDialog ref={shareDialogRef} animClass='share-dialog'>
        <View className='share-dialog'>
          <View className='share-item'>
            <ButtonWithOpenType openType='share' type={UKtype} className='share-btn' onClick={handleShareToFriend}>
              <View className='image-block'>
                <Image className='share-img' src={getStaticImgUrl.group.share_png}></Image>
              </View>
              <Text>分享给朋友</Text>
            </ButtonWithOpenType>
          </View>
          {process.env.TARO_ENV === 'weapp' && (
            <View className='share-item'>
              <Button className='share-btn' onClick={handleShowPosterDialog}>
                <View className='image-block'>
                  <Image className='share-img' src={getStaticImgUrl.group.poster_png}></Image>
                </View>
                <View>生成商品海报</View>
              </Button>
            </View>
          )}

          <Button className='close' onClick={handleCloseShare}>
            关闭
            <SafeAreaPlaceholder />
          </Button>
        </View>
      </AnimatDialog>
      {/*  生成海报对话弹框  */}
      <PostersDialog
        visible={posterDialogVisible}
        posterType={posterType}
        redPacket={posterType === POSTER_TYPE.redPacket ? posterData : null}
        posterHeaderType={posterHeaderType}
        posterTips={posterTips}
        posterLogo={posterLogo}
        posterSalePriceLabel={posterSalePriceLabel}
        posterLabelPriceLabel={posterLabelPriceLabel}
        posterName={posterName}
        posterImage={posterImage}
        salePrice={salePrice}
        labelPrice={labelPrice}
        qrCode={qrCodeUrl}
        onSave={handleSaveImage}
        showLabelPrice={showLabelPrice}
      ></PostersDialog>
    </View>
  );
};

ShareDialog.defaultProps = {
  visible: false,
  posterData: null,
  qrCodeParams: null,
  isNewQrCode: false,
  posterType: POSTER_TYPE.goods,
  posterTips: '向您推荐了这个商品',
  posterLogo: '',
  posterHeaderType: POSTER_HEADER_TYPE.avatar,
  posterImage: '',
  posterName: '',
  salePrice: '',
  labelPrice: '',
  posterSalePriceLabel: '优惠价',
  posterLabelPriceLabel: '原价',
  showLabelPrice: true,
  uniqueShareInfoForQrCode: null,
};

export default ShareDialog;
