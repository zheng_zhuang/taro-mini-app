import { _fixme_with_dataset_, _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import './index.scss';
import React, { ComponentClass } from 'react';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
// import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl';

const SORT_TYPE = {
  DESC: 'desc',
  ASC: 'asc',
};

interface GoodsListFilterProps {
  onFilterChange: Function;
  onViewChange?: Function;
  view?: string;
  showViewIcon?: boolean;
  loading?: boolean;
}

interface GoodsListFilterState {}
type IProps = GoodsListFilterProps & GoodsListFilterState;
interface GoodsListFilter {
  props: IProps;
}

const IMG_OF_NORMAL = cdnResConfig.common.sortNormal;
// const IMG_OF_DESC = 'https://front-end-1302979015.file.myqcloud.com/images/c/images/sort/sort_desc.png';
// const IMG_OF_ASC = 'https://front-end-1302979015.file.myqcloud.com/images/c/images/sort/sort_asc.png';

@hoc
class GoodsListFilter extends React.Component {
  static defaultProps = {
    onFilterChange: () => {},
    onViewChange: () => {},
    view: 'vertical',
    showViewIcon: false,
  };

  state = {
    sortArray: [
      {
        name: '有货',
        id: 'default',
        icon: null,
        status: 0,
      },

      {
        name: '销量',
        id: 'salesVolume',
        icon: IMG_OF_NORMAL,
        status: 0,
      },

      {
        name: '价格',
        id: 'salePrice',
        icon: IMG_OF_NORMAL,
        status: 0,
      },
      
      // {
      //   name: '上新',
      //   id: 'createTime',
      //   icon: null,
      //   status: 0,
      // },

    ],

    sortField: null,
    sortType: null,
  };

  resetSortArray = (sortArray) => {
    sortArray[0].status = 0;
    sortArray[1].status = 0;
    sortArray[2].status = 0;
    sortArray[2].icon = IMG_OF_NORMAL;
    // sortArray[3].status = 0;
  };

  /**
   * 点击排序按钮
   */
  clickSortBtn = (e) => {
    const { sortArray } = this.state;
    const { loading } = this.props;
    
    const index = parseInt(e.currentTarget.dataset.index);
    const sourceSort = sortArray[index].status;
    let sortField = sortArray[index].id;
    let targetStatus;
    let inStock:any = null;
    let sortType = null;
    // 请求列表的时候禁止点击
    if (loading) {
      return
    }

    this.resetSortArray(sortArray);

    // 选中有货
    if (index === 0) {
      if (sourceSort === 0) {
        sortArray[index].status = (inStock = 1);
        // sortArray[index].icon = getStaticImgUrl.sort.sort_on_png;
      } else {
        sortArray[index].status = (inStock = 0);
        // sortArray[index].icon = getStaticImgUrl.sort.sort_off_png;
      }
      sortField = null;
      sortType = null;
      sortArray[index].status = 1;
    } else if (index === 1) {
      // 选中销量
      targetStatus = sourceSort === 2 ? 1 : sourceSort + 1;
      sortType = targetStatus === 1 ? SORT_TYPE.ASC : targetStatus === 2 ? SORT_TYPE.DESC : null;
      sortArray[index].status = targetStatus;
    } else if (index === 2) {
      // 选中价格
      targetStatus = sourceSort === 2 ? 1 : sourceSort + 1;
      sortType = targetStatus === 1 ? SORT_TYPE.ASC : targetStatus === 2 ? SORT_TYPE.DESC : null;
      sortArray[index].status = targetStatus;
    } else {
      sortArray[index].status = 1;
      sortType = SORT_TYPE.DESC;
    }
    this.setState(
      {
        inStock,
        sortArray,
        sortField,
        sortType,
      },

      () => {
        this.filterTrigger();
      }
    );
  
  };

  // 条件改变 触发查询
  filterTrigger = () => {
    const { onFilterChange } = this.props;
    const { sortField, sortType, inStock } = this.state;
    onFilterChange &&
      onFilterChange({
        inStock,
        sortField,
        sortType,
      });

  };

  resetSortArrayOnCategoryIdChange = () => {
    this.filterTrigger();
  };

  handleChangeView = () => {
    const view = this.props.view === 'vertical' ? 'oneRowTwo' : 'vertical';
    this.props.onViewChange && this.props.onViewChange({ view });
  };

  render() {
    const { sortArray } = this.state;
    const {
      view,
      showViewIcon,
      $global: { $tmpStyle },
    } = this.props;

    const VIEW_LOOKUP = {
      vertical: cdnResConfig.search.viewList,
      oneRowTwo: cdnResConfig.search.viewPic,
    };

    const $viewIcon = showViewIcon ? (
      <View className='view-icon' onClick={this.handleChangeView}>
        <Image className='icon' src={VIEW_LOOKUP[view || 'vertical']} />
      </View>
    ) : null;

    return (
      <View data-scoped='wk-wcg-GoodsFilter' className='wk-wcg-GoodsFilter goods-filter-container'>
        <View className='filter'>
          {sortArray.map((item, index) => {
            return (
              <View
                className='filter-view'
                key={index}
                onClick={_fixme_with_dataset_(this.clickSortBtn, { index: index })}
              >
                <View
                  style={_safe_style_('color: ' + (item.status > 0 ? $tmpStyle.btnColor || '#666666' : '#666666'))}
                  className={item.status > 0 ? 'sort-title-on' : 'sort-title'}
                >
                  {item.name}
                </View>
                {item &&
                  (item.id === 'salePrice'||item.id === 'salesVolume') &&
                  (item.status === 0 ? (
                    <Image className='sort-image' src={item.icon}></Image>
                  ) : item.status === 1 ? (
                    <View
                      style={_safe_style_('border-bottom-color: ' + $tmpStyle.btnColor)}
                      className='icon-arrow-up'
                    ></View>
                  ) : (
                    <View
                      style={_safe_style_('border-top-color: ' + $tmpStyle.btnColor)}
                      className='icon-arrow-down'
                    ></View>
                  ))}
                  {/* <View className={item.status > 0 ? 'sort-title-on' : 'sort-title'}>{item.name}</View>
                  <Image className="sort-image" src={item.icon}></Image> */}
              </View>
            );
          })}
          {/* {$viewIcon} */}
        </View>
        <View className='line'></View>
      </View>
    );
  }
}

export default GoodsListFilter as ComponentClass<GoodsListFilterProps, GoodsListFilterState>;
