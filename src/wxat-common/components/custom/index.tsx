// @externalClassesConvered(Empty)
import {$getRouter, PageStyle, _safe_style_} from '@/wxat-common/utils/platform';
import {Block, View, Button} from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/custom/index.js
import wxApi from '../../utils/wxApi';
import template from '../../utils/template.js';
import './index.scss';
import React, {ComponentClass} from 'react';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';
import {connect} from 'react-redux';

import Empty from '../empty/empty';
import ImageModule from '../decorate/imageModule';
import VirtualGoodsModule from '../base/virtualGoodsModule';
import CardPackModule from '../base/cardPackModule';
import DialogModule from '../base/dialogModule';
import HotAreaModule from '../decorate/hotAreaModule';
import SeriesModule from '../base/seriesModule';
import ShopGuideModule from '../base/shopGuideModule';
import LiveModule from '../base/liveModule';
import FreeModule from '../base/freeModule';
import IntegralModule from '../base/integralModule';
import SeckillModule from '../base/seckillModule';
import LuckyDialModule from '../base/luckyDialModule';
import GraphicsModule from '../base/graphicsModule';
import GiftCardModule from '../base/giftCardModule';
import PosterRecommendModule from '../decorate/poster-recommendation';
import ActivityModule from '../decorate/special-activity-module';
import TopMenuModule from '../decorate/topMenuModule';
import NavigationModule from '../decorate/navigationModule';
import HaggleModule from '../base/haggleModule';
import CouponsModule from '../base/couponsModule';
import CollageModule from '../base/collageModule';
import ProductModule from '../base/productModule';
import ServeModule from '../base/serveModule';
import CardSwiperModule from '../base/cardSwiperModule';
import SearchButton from '../search';
import StoreModule from '../decorate/storeModule';
import AssistLineModule from '../decorate/assistLineModule';
import AssistBlankModule from '../decorate/assistBlankModule';
import TextNavModule from '../decorate/textNavModule';
import YqzModule from "../base/yqzModule";
import TitleModule from '../decorate/titleModule';
import MagicModule from '../decorate/magicModule';
import PosterModule from '../decorate/posterModule';
import VideoModule from '../decorate/videoModule';
import PromotionModule from '../decorate/promotionModule';
import VRModule from '../decorate/VRModule';
import CustomDialogModule from '../decorate/custom-dialog-moudle';
import CampaignModule from '../base/activityModule';
import HotelReservation from '../base/hotelReservation';
import RecommendHotelModule from '../base/recommendHotelModule';
import ServiceClassificationTree from '../base/service-classification-tree';
import ServiceDistributionList from '../base/service-distribution-list';
import EbooksModule from '../base/ebooksModule';
import MicroDecorateModule from '../base/microDecorateModule';
import {qs2obj} from '@/wxat-common/utils/query-string';
import checkOptions from '@/wxat-common/utils/check-options';
import {WaitComponent} from '@/decorators/Wait';
import EquityCardModule from '../decorate/equity-card-module';
import scan from '@/wxat-common/utils/scanH5'
import url from '@/wxat-common/api/index'
import Icalendar from '@/wxat-common/components/calendar';

interface PageStateProps {
  sessionId: string;
  $global: Record<string, any>;
  currentStoreViewVisible: boolean;
  searchBox: boolean;
  currentStore: object;
  style?: React.CSSProperties;
}

interface PageDispatchProps {
}

interface PageOwnProps {
  pageConfig?: Array<Record<string, any>> | null;
  keyPath?: string;
  // 是否是tabbar
  isTabbar?: boolean;
  // 是否主页
  isHome?: boolean;
  shareParams?: object;
  scanId?: string;

  customCodeData?: any
}

interface PageState {
  PADDING: number;
  templateStyle: object;
  key2pageConfig: Array<Record<string, any>> | null;
  defalutTime: object;
  showCalendar: Boolean;
}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface Custom {
  props: IProps;
}

const mapStateToProps = (state) => ({
  sessionId: state.base.sessionId,
  currentStoreViewVisible: !!state.base.currentStoreViewVisible,
  searchBox: state.base.appInfo && !!state.base.appInfo.searchBox,
  currentStore: state.base.currentStore,
});

@connect(mapStateToProps, undefined, undefined, {forwardRef: true})
class Custom extends WaitComponent {
  $router = $getRouter()
  static defaultProps = {
    keyPath: '',
    pageConfig: null,
    isTabbar: false,
    isHome: false,
    shareParams: null,
    customCodeData: null
  };

  state = {
    statusNavBarHeight: 32,
    statusBarHeight: 32,
    loaded: false,
    PADDING: 20,
    templateStyle: {},
    key2pageConfig: null,
    defalutTime: {
      inTime: '',
      outTime: '',
    },
    showCalendar: false
  };
  yqzModuleRef = React.createRef();
  getCalendarDialog = () => {
    return this.yqzModuleRef.current as any;
  };
  getUrlParams = () => {
    let url;
    if (process.env.TARO_ENV == 'h5') {
      url = window.location.hash
    } else {
      url = Taro.getCurrentInstance().router.path;
    }
    let path: any = decodeURIComponent(url);
    path = path.substring(path.lastIndexOf('?') + 1)
    path = path.split('&')

    const params = {}
    for (let i = 0; i < path.length; i++) {
      const val = path[i].split('=')
      params[val[0]] = val[1]
    }
    return params
  }

  fetchScanCode = async () => {
    const options = this.getUrlParams()
    if (process.env.WX_OA === 'true' && options?.id) {
      this.setState({
        codeId: options.id
      })
      const data: any = await scan.scanRoomCodeFunc(options)
      if (data && data.isScan) {
        this.setState({
          updateRoomCode: data.isScan
        })
      }
      if (data && data.customCodeData) {
        this.setState({
          customCodeData: data.customCodeData
        })
      }
      return data
    }
    return null
  }

  onWait = async () => {
    // const data = await this.fetchScanCode()
    const {keyPath = ''} = this.props;
    // const keyPath = 'pages/custom/index?route=a20bb3792a7a40cbba2065c666d1d1cb' || this.props.keyPath ;
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor)
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    this.setState({
      templateStyle,
    });
    if (keyPath && !this.props.customCodeData) {
      this.getPageConfigFromKey(keyPath);
    } else if (this.props.customCodeData && (this.props.customCodeData.id || this.props.customCodeData.pId)) {
      // this.getPageConfigFromKey(keyPath)
      this.getPageConfigFromKey(this.props.customCodeData)

    } else {
      const {
        path,
        query: {scene},
      } = wxApi.getLaunchOptionsSync();
      // 自定义页二维码
      if (path.includes('custom/index') && scene) {
        this.init();
      }
    }
  }

  getPageConfigFromKey = (key) => {
    const options = this.$router.params
    if (options.mallMainPageId) {
      wxApi.request({
        url: url.getMallMainPageConfig,
        data: {id: options.mallMainPageId},
        method: 'post',
        header: {
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8' // 默认值
        }
      }).then((response) => {
        const res = JSON.parse(response.data.config)
        try {
          res.forEach((item: any, index: number) => {
            // 产品列表 数据压缩 (
            // 1: 只保留itemNo字段，
            // )
            if (item.id === 'productModule') {
              const newList: Object[] = [];
              item.config.data.forEach((goods) => {
                const itemNo = goods.itemNo;
                newList.push({itemNo});
              });
              res[index].config.data = newList;
            }
          });
        } catch (xe) {
        }
        // 页面标题为路由参数
        const name = res[0].config.name;
        const pages = Taro.getCurrentPages();

        const currentPage = pages[pages.length - 1]; // 获取当前页面的对象
        if (!!name && currentPage && (currentPage.route ? !currentPage.route.includes('pages/home/index') : '')) {
          wxApi.setNavigationBarTitle({title: name});
        }
        this.setState({
          key2pageConfig: res
        });
      }).catch(() => {
        this.setState({loaded: true})
      });
    } else {
      wxAppProxy
        .getPageConfig(key)
        .then((res) => {
          try {
            res.forEach((item: any, index: number) => {
              // 产品列表 数据压缩 (
              // 1: 只保留itemNo字段，
              // )
              if (item.id === 'productModule') {
                const newList: Object[] = [];
                item.config.data.forEach((goods) => {
                  const itemNo = goods.itemNo;
                  newList.push({itemNo});
                });
                res[index].config.data = newList;
              }
            });
          } catch (xe) {
          }
          // 页面标题为路由参数
          const name = res[0].config.name;
          const pages = Taro.getCurrentPages();

          const currentPage = pages[pages.length - 1]; // 获取当前页面的对象
          if (!!name && currentPage && (currentPage.route ? !currentPage.route.includes('pages/home/index') : '')) {
            wxApi.setNavigationBarTitle({title: name});
          }
          this.setState({
            key2pageConfig: res,
          });
        })
        .catch(() => {
          this.setState({loaded: true})
        });
    }

  };

  componentDidMount() {
    this.onWait()
  }

  componentWillUnmount = () => {
    this.setState = (state, callback) => {
      return;
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.customCodeData !== prevProps.customCodeData || (this.props.keyPath !== prevProps.keyPath && prevProps.customCodeData)) {
      this.getPageConfigFromKey(this.props.customCodeData)
    }
  }

  toIndex = () => {
    wxApi.switchTab({
      url: '/wxat-common/pages/home/index',
    });
  };

  init = async () => {
    const {query} = wxApi.getLaunchOptionsSync();

    if (query && query.scene) {
      const {scene} = await checkOptions.checkOnLoadOptions(query);

      let params = scene;
      if (typeof scene === 'string') {
        params = qs2obj(scene);
      }
      if (typeof params === 'string' || 'pId' in params) {
        this.getPageConfigFromKey(params);
      }
    }
  };

  // 易企住日期触发
  handleDateCallBack = (defalutTime) => {
    this.setState({
      defalutTime,
      showCalendar: true
    })
  }
  // 日期选择回调
  closeDialog = (e) => {
    if (e.ishasValue) {
      this.getCalendarDialog().closeDialog(e)
    }
    this.setState({
      showCalendar: false
    });
  }

  render() {
    const {
      pageConfig,
      keyPath,
      isHome,
      isTabbar,
      currentStoreViewVisible,
      shareParams,
      searchBox,
      style
    } = this.props;
    const {
      templateStyle, PADDING, key2pageConfig, loaded, statusNavBarHeight, statusBarHeight,
      defalutTime,
      showCalendar
    } = this.state;
    const pageConfigArray = pageConfig || key2pageConfig || [];

    return (currentStoreViewVisible && (
      <View
        data-scoped='wk-wcc-Custom'
        className='wk-wcc-Custom custom'
        style={_safe_style_(PageStyle(this.props.style))}
      >
        {(!pageConfigArray.length && !isHome && !keyPath) || loaded ? (
          <View className='error'>
            <View className='error-context'>
              <Empty message='敬请期待...'/>
              {!isTabbar && (
                <Button
                  onClick={this.toIndex}
                  className='error-btn'
                  style={_safe_style_('background: ' + templateStyle.btnColor)}
                >
                  返回首页
                </Button>
              )}
            </View>
          </View>
        ) : (
          <View>
            {/* 搜索栏 */}
            {/*{!!searchBox && (*/}
            {/*  <View*/}
            {/*    className="search-box"*/}
            {/*    style={_safe_style_(`padding-top: ${statusBarHeight - statusBarHeight}px;*/}
            {/*        height: ${statusNavBarHeight}px;*/}
            {/*        display: flex`)}*/}
            {/*  >*/}
            {/*    <SearchButton></SearchButton>*/}
            {/*  </View>*/}
            {/*)}*/}
            <View style={_safe_style_(
              `background:${pageConfigArray && pageConfigArray.length != 0 && (pageConfigArray[0].config.linkUrl ? 'url(' + pageConfigArray[0].config.linkUrl + ') no-repeat top left /100%' : pageConfigArray[0].config.bgColor)};
             height:100%
              `
            )}>
              {pageConfigArray.map((item, index) => {
                return (
                  <Block key={`${index}-${item.id}`}>
                    {/*  易企住组件装修  */}
                    {(item.id === 'yqzModule') && (
                      <YqzModule dataSource={item.config} dateCall={this.handleDateCallBack} ref={this.yqzModuleRef}/>
                    )}
                    {/*  轮播  */}
                    {!!(item.id === 'imageModule' && item.config.data && item.config.data.length) && (
                      <ImageModule dataSource={item.config}/>
                    )}

                    {/*  海报  */}
                    {!!(item.id === 'posterModule' && item.config.data && item.config.data.length) && (
                      <PosterModule dataSource={item.config}/>
                    )}

                    {/*  营销图文  */}
                    {!!(item.id === 'articleModule' && item.config.data && item.config.data.length) && (
                      <GraphicsModule dataSource={item.config}/>
                    )}

                    {item.id === 'titleModule' && <TitleModule dataSource={item.config} padding={PADDING}/>}

                    {/*  商品  */}
                    {!!(item.id === 'productModule' && item.config.data && item.config.data.length) && (
                      <ProductModule dataSource={item.config}/>
                    )}

                    {/*  服务  */}
                    {!!(item.id === 'serveModule' && item.config.data && item.config.data.length) && (
                      <ServeModule dataSource={item.config}/>
                    )}

                    {/*  文本导航  */}
                    {item.id === 'textNavModule' && <TextNavModule dataSource={item.config} padding={PADDING}/>}

                    {/*  卡项  */}
                    {!!(item.id === 'cardSwiperModule' && item.config.data && item.config.data.length) && (
                      <CardSwiperModule dataSource={item.config}/>
                    )}

                    {/*  礼品卡  */}
                    {!!(item.id === 'giftCardModule' && item.config.data && item.config.data.length) && (
                      <GiftCardModule dataSource={item.config}/>
                    )}

                    {/*  店铺信息  */}
                    {item.id === 'storeModule' && (
                      <StoreModule ableChange dataSource={item.config} ignoreScanCode={false}/>
                    )}

                    {/*  辅助空白  */}
                    {!!(item.id === 'assistBlankModule' && item.config) && (
                      <AssistBlankModule dataSource={item.config}/>
                    )}

                    {/*  辅助线  */}
                    {!!(item.id === 'assistLineModule' && item.config) &&
                      <AssistLineModule dataSource={item.config}/>}

                    {/*  拼团  */}
                    {!!(item.id === 'collageModule' && item.config.data && item.config.data.length) && (
                      <CollageModule dataSource={item.config}/>
                    )}

                    {/*  优惠券  */}
                    {!!(item.id === 'couponsModule' && item.config.data && item.config.data.length) && (
                      <CouponsModule dataSource={item.config}/>
                    )}

                    {/*  砍价  */}
                    {!!(item.id === 'haggleModule' && item.config.data && item.config.data.length) && (
                      <HaggleModule dataSource={item.config}/>
                    )}

                    {/*  导航图标  */}
                    {!!(item.id === 'navigationModule' && item.config.list && item.config.list.length) && (
                      <NavigationModule dataSource={item.config}/>
                    )}

                    {/*  顶部菜单  */}
                    {!!(item.id === 'topMenuModule' && item.config.data && item.config.data.length) && (
                      <TopMenuModule dataSource={item.config}/>
                    )}

                    {/*  专题活动  */}
                    {!!(item.id === 'activityModule') && <ActivityModule dataSource={item.config}/>}

                    {/*  酒店预订  */}
                    {item.id === 'hotelReservationModule' && (
                      <HotelReservation dataSource={item.config}/>
                    )}

                    {/*  猜你喜欢  */}
                    {!!(item.id === 'posterRecommendModule' && item.config) && (
                      <PosterRecommendModule dataSource={item.config}/>
                    )}

                    {/*  幸运转盘  */}
                    {!!(item.id === 'luckyModule' && item.config.data && item.config.data.id) && (
                      <LuckyDialModule dataSource={item.config}/>
                    )}

                    {/*  秒杀  */}
                    {!!(item.id === 'seckillModule' && item.config.data && item.config.data.length) && (
                      <SeckillModule dataSource={item.config}/>
                    )}

                    {/*  积分商城  */}
                    {!!(item.id === 'integralModule' && item.config.data && item.config.data.length) && (
                      <IntegralModule dataSource={item.config}/>
                    )}

                    {/*  赠品专区  */}
                    {item.id === 'freeModule' && <FreeModule dataSource={item.config}/>}
                    {/*  导购名片  */}
                    {item.id === 'shopCardModule' && (
                      <ShopGuideModule dataSource={item.config} shareParams={shareParams}/>
                    )}

                    {!!(item.id === 'seriesModule' && item.config.data && item.config.data.length) && (
                      <SeriesModule dataSource={item.config}/>
                    )}

                    {/*  直播  */}
                    {!!(item.id === 'liveModule' && item.config.data && item.config.data.src) && (
                      <LiveModule dataSource={item.config}/>
                    )}

                    {/*  悬浮框  */}
                    {item.id === 'dialogModule' && <DialogModule dataSource={item.config}/>}
                    {/*  卡包  */}
                    {!!(item.id === 'cardPackModule' && item.config.data && item.config.data.length) && (
                      <CardPackModule dataSource={item.config}/>
                    )}

                    {/* 视频 */}
                    {!!(item.id === 'videoModule' && item.config.data) && <VideoModule dataSource={item.config}/>}

                    {/* 促销活动 */}
                    {!!(item.id === 'promotionModule' && item.config.data && item.config.data.length) && (
                      <PromotionModule dataSource={item.config}/>
                    )}

                    {/* 微装 */}
                    {!!(item.id === 'microDecorateModule' && item.config.data && item.config.data.length) && (
                      <MicroDecorateModule dataSource={item.config}/>
                    )}

                    {/* 电子画册 */}
                    {!!(item.id === 'ebooksModule' && item.config.data && item.config.data.length) && (
                      <EbooksModule dataSource={item.config}/>
                    )}

                    {/* 魔方 */}
                    {!!(item.id === 'magicModule' && item.config.data && item.config.data.length) && (
                      <MagicModule dataSource={item.config} padding={PADDING}/>
                    )}

                    {/* 热区图 */}
                    {/* && item.config.data && item.config.data.length */}
                    {!!(item.id === 'hotAreaModule') && (
                      <HotAreaModule dataSource={item.config}/>
                    )}

                    {/* VR展示 */}
                    {!!(item.id === 'VRModule' && item.config.data && item.config.data.length) && (
                      <VRModule dataSource={item.config}/>
                    )}

                    {/* 权益卡 */}
                    {!!(item.id === 'equityCardModule' && item.config.data && item.config.data.length) && (
                      <EquityCardModule dataSource={item.config}/>
                    )}

                    {/* 自定义弹窗展示 */}
                    {item.id === 'customDialogModule' && <CustomDialogModule dataSource={item.config}/>}
                    {/* 预约活动 */}
                    {!!(item.id === 'campaignModule' && item.config.list && item.config.list.length) && (
                      <CampaignModule dataSource={item.config}></CampaignModule>
                    )}
                    {/* 虚拟权益*/}
                    {!!(item.id === "virtualGoodsModule" && item.config.data && item.config.data.length) && (
                      <VirtualGoodsModule dataSource={item.config}/>
                    )}

                    {/*  配送服务列表组件装修  */}
                    {(item.id === 'serviceCategoryListModule' && item.config.type == 1) && (
                      <ServiceClassificationTree dataSource={item.config}/>
                    )}

                    {/*  住中服务列表组件装修  */}
                    {(item.id === 'serviceCategoryListModule' && item.config.type == 5) && (
                      <ServiceDistributionList dataSource={item.config}/>
                    )}

                    {/*  推荐酒店  */}
                    {item.id === 'yqzRecommendHotelModule' && (
                      <RecommendHotelModule dataSource={item.config}/>
                    )}
                  </Block>
                );
              })}
            </View>

            <Icalendar
              className='calendar'
              inTime={defalutTime.inTime}
              outTime={defalutTime.outTime}
              showcalendar={showCalendar}
              onMyevent={this.closeDialog}
            ></Icalendar>
          </View>
        )}
      </View>
    ));
  }
}

export default Custom as ComponentClass<PageOwnProps, PageState>;
