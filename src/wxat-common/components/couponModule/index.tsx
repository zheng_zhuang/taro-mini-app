import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { Block, View, Text, Image, Button, ScrollView } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../utils/money.wxs.js';
import utilDate from '../../utils/date.js';
import wxApi from '../../utils/wxApi';
import constants from '../../constants/index.js';
import touchSlideUtils from '../../utils/touchSlideUtils.js';
import report from '@/sdks/buried/report/index.js';
import couponEnum from '../../constants/couponEnum.js';
import subscribeMsg from '../../utils/subscribe-msg.js';
import subscribeEnum from '../../constants/subscribeEnum.js';
import authHelper from '../../utils/auth-helper.js';
import jumpPath from '../decorate/utils/jump-path.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import Circle from '../circleProgress/index';
import Dialog from '../dialog/index';
import { connect } from 'react-redux';
import './index.scss';
const app = Taro.getApp();

const mapStateToProps = (state) => ({
  globalData: state.globalData,
})

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class CouponModule extends React.Component {

  state = {
    couponEnum,
    couponList: [],
    delBtnWidth: 150,
    status: constants.coupon.status,
    isShow: false,
    dialogShow: false,
    isStoreBriefs: false,
    moreBriefs: null,
    // environment === 'wxwork': 企业微信中运行
    environment: this.props.globalData.environment,
  }

  componentDidMount () {
    this.getCouponList(this.props.list);
  }

  componentWillReceiveProps(nextProps) {
    this.getCouponList(nextProps.list);
  }

  touchS = (e) => {
    touchSlideUtils.touchS(e);
  }

  touchM = (e) => {
    //获取手指触摸的是哪一项
    const index = e.currentTarget.dataset.index;
    const list = this.state.couponList;
    const isSlided = touchSlideUtils.touchM(e);

    list[index].isSlided = isSlided;

    //更新列表的状态
    this.setState({
      couponList: list,
    });
  }

  touchE = (e) => {
    //获取手指触摸的是哪一项
    const index = e.currentTarget.dataset.index;
    const list = this.state.couponList;
    const slideResult = touchSlideUtils.touchE(e, this.state.delBtnWidth, list[index].isSlided);

    if (!slideResult.txtStyle) {
      return;
    }
    list[index].txtStyle = slideResult.txtStyle;
    list[index].isSlided = slideResult.isSlided;
    //更新列表的状态
    this.setState({
      couponList: list,
    });
  }

  getCouponList = (list) => {
    if (list && !!list.length) {
      const items = [];
      list.forEach((item) => {
        let endTime = new Date(item.endTime);
        let beginTime = new Date(item.beginTime);
        item.endTime = utilDate.format(endTime, 'yyyy-MM-dd');
        item.beginTime = utilDate.format(beginTime, 'yyyy-MM-dd');
        item.percentage = Math.round((item.issueAmount / item.quantity) * 100);
        if (item.quantity === 0) {
          item.percentage = 100;
        }
        if (item.discountFee > 9900 || item.discountFee < 10) {
          item.fontStyle = 'font-size:47rpx;';
        }
        if (item.discountFee === 0) {
          item.mTop = 'margin-top:30rpx;';
        }

        if (item.useScopeType === 1) {
          item.useScopeTypeDesc = '线上商城';
        } else if (item.useScopeType === 2) {
          item.useScopeTypeDesc = '线下门店';
        } else {
          item.useScopeTypeDesc = '通用券';
        }

        item.txtStyle = '';
        items.push(item);
      });
      this.setState(
        {
          couponList: items,
        },
        () => {
          this.showToggle()
        }
      )
    }
  }

  handleUsed = (item) => {
    let couponNo = item.id
    const type = item.useScopeType

    if (type === 1) {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/home/index',
      });

      return;
    }

    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/mine/coupon/detail/index',
      data: { couponInfoId: couponNo },
    })
  }

  handleToList = (e) => {
    let couponNo = e.currentCouponTicketIdList
    let targetUrlConfig = e.targetUrlConfig;
    const type = e.useScopeType;
    if (targetUrlConfig !== '') {
      targetUrlConfig = JSON.parse(targetUrlConfig);
    }
    if (targetUrlConfig && targetUrlConfig[0].linkPage) {
      this.onTapArea(targetUrlConfig);
    } else {
      //如果来源是购物车，立即使用时，直接返回上一层页面即可
      if (this.props.directBack) {
        Taro.navigateBack({
          delta: 1,
        });
      } else if (type === 2) {
        // 线下券跳转到核销码页面
        if (couponNo && couponNo.length !== 0) {
          wxApi.$navigateTo({
            url: '/sub-packages/moveFile-package/pages/mine/coupon/detail/index',
            data: { couponInfoId: couponNo[0] },
          });
        } else {
          wxApi.$navigateTo({
            url: '/wxat-common/pages/home/index',
          });
        }
      } else if (app && app.jumpToClassify) {
        wxApi.$navigateTo({
          url: '/wxat-common/pages/home/index',
        });
      } else {
        wxApi.$navigateTo({
          url: '/wxat-common/pages/home/index',
        });
      }
    }
  }

  handleGetCoupon = (e) => {
    let receiveMethod = e.type;
    let item = e.item;
    report.clickGetCoupon(false, item.id);
    // 授权订阅消息
    const Ids = [subscribeEnum.COUPONS_ARRIVE.value, subscribeEnum.COUPONS_OVERDUE.value];
    subscribeMsg.sendMessage(Ids).then(() => {
      const { onCouponEvent } = this.props;
      if (receiveMethod === 0) {
        onCouponEvent &&
          onCouponEvent({
            couponNo: item.id,
          });
      } else {
        onCouponEvent &&
          onCouponEvent({
            couponNo: item.id,
            receiveMethod: 1,
          });
      }
    });
    // 测试 const Ids = [subscribeEnum.COUPONS_ARRIVE.value];
    // subscribeMsg.sendMessage(Ids).then(() => {
    //   if (receiveMethod === 0) {
    //     this.triggerEvent('coupon-event', {
    //       couponNo: item.id,
    //     });
    //   } else {
    //     this.triggerEvent('coupon-event', {
    //       couponNo: item.id,
    //       receiveMethod: 1,
    //     });
    //   }
    // })
  }

  handleRemove = (id) => {
    this.props.onRemove(id)
  }

  handleClick = (e) => {
    const index = e.currentTarget.dataset.index;
    const list = this.state.couponList;
    list[index].isShow = !list[index].isShow;

    this.setState({
      couponList: list,
    })
  }

  showToggle = () => {
    const list = this.state.couponList;

    list.forEach((item, index) => {
      if (item.itemBriefs) {
        const { count, briefs } = this.getCount(item.itemBriefs);
        item.briefsStr = briefs;

        if (count > 16) {
          list[index].isMore = true;
        }
      } else if (item.categoryBriefs) {
        const { count, briefs } = this.getCount(item.categoryBriefs);
        item.briefsStr = briefs;
        if (count > 16) {
          list[index].isMore = true;
        }
      }
      if (item.storeBriefs) {
        const { count, briefs } = this.getCount(item.storeBriefs);
        item.storeBriefsStr = briefs;
        if (count > 16) {
          // list[index].isMore = true;
          list[index].isStoreMore = true;
        }
      }
    });

    this.setState({
      couponList: list,
    })
  }

  getCount = (data) => {
    let count = 0;
    let briefs = '';
    data.forEach((brief, index) => {
      if (brief.name) {
        count += brief.name.length;
        briefs += brief.name;
        if (index < data.length - 1) {
          briefs += '、';
        }
      }
    });
    return { count, briefs };
  }

  handleDialog = (e) => {
    const moreBriefs = e.currentTarget.dataset.moreBriefs;
    const isStoreBriefs = e.currentTarget.dataset.isStoreBriefs;
    this.setState({
      isStoreBriefs: !!isStoreBriefs,
      moreBriefs,
      dialogShow: true,
    })
  }

  handleCancel = () => {
    this.setState({
      dialogShow: false
    })
  }

  onTapArea = (e) => {
    const index = 0;
    const item = e[index];
    if (!(!item.linkPage || item.linkPage === undefined)) {
      if (item.linkId === 'sign_in' && !authHelper.checkAuth()) {
        return;
      }
      //判断是否拨打电话进来
      if (item.linkPageKey == 'oneTouchDial' && item.linkOneTouchDial) {
        Taro.makePhoneCall({
          phoneNumber: item.linkOneTouchDial,
        });
      }
      //兼容场地预定路径迁移到主包
      if (item.linkPage == 'sub-packages/marketing-package/pages/living-service/hotel-reservation-list/index') {
        item.linkPage = 'wxat-common/pages/hotel-reservation-list/index';
      } else if (item.linkPage == 'sub-packages/marketing-package/pages/living-service/goods-rental/index') {
        item.linkPage = 'wxat-common/pages/service-classification-tree/index';
      }
      //判断是否是住中服务进来的
      if (item.linkPageKey == 'service-category' || item.serviceItemId || item.wifiCateGoryId) {
        if (item.linkPage == 'sub-packages/moveFile-package/pages/goods-detail/index') {
          item.linkPage = 'sub-packages/marketing-package/pages/living-service/goods-rental-details/index';
        }
        wxApi.$navigateTo({
          url: jumpPath(item, '', reportConstants.SOURCE_TYPE.banner.key).url,
          data: {
            id: item.serviceItemId ? encodeURIComponent(JSON.stringify(item.serviceItemId)) : item.serviceCateGoryId,
            sourceType: item.sourceType,
          },
        });
      } else {
        wxApi.$navigateTo(jumpPath(item, '', reportConstants.SOURCE_TYPE.hot_area.key));
      }
    }
  }

  render () {
    const { couponEnum, couponList, environment, dialogShow, isStoreBriefs, moreBriefs } = this.state
    const { isMine } = this.props
    const { scoreName } = this.props.globalData
    return (
      <View data-scoped="wk-swcc-CouponModule" className="wk-swcc-CouponModule coupon-module  top">
        {!!isMine && (
          <Block>
            {
              !!couponList.length && couponList.map((item, index) => {
                return (
                  <View className="coupon-list" key={'coupon-lis-' + index}>
                    <View className={'coupon-item ' + (item.isSlided ? 'touch-move-active' : '')}>
                      <View
                        className="coupon-touch"
                        onTouchStart={_fixme_with_dataset_(this.touchS, { index: index })}
                        onTouchMove={_fixme_with_dataset_(this.touchM, { index: index })}
                        onTouchEnd={_fixme_with_dataset_(this.touchE, { index: index })}
                      >
                        <View className="coupon-detail-box">
                          <View className="coupon-detail">
                            {/*  <image class='coupon-img' wx:if="{{item.status !== 1}}" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/none.png"></image>
                                                                                                                                                                                                                                                                                                                                                 <image class='coupon-img' wx:else src="https://front-end-1302979015.file.myqcloud.com/images/c/images/coupon/full.png"></image>  */}
                            <View className="coupon-content">
                              <View style={_safe_style_('position: relative;')}>
                                <View className="coupon-price">
                                  {item.status !== 1 ? (
                                    <View
                                      className={
                                        'price-box discount-box ' +
                                        (item.couponCategory === couponEnum.TYPE.discount.value ? 'discount-box' : '')
                                      }
                                    >
                                      {item.couponCategory === couponEnum.TYPE.freight.value ||
                                        item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                                        <Block>
                                          <Text className="coupon-bigprice noneColor">￥</Text>
                                          <Text className="coupon-bigprice noneColor">
                                            {filters.moneyFilter(item.discountFee, true)}
                                          </Text>
                                        </Block>
                                      ) : (
                                        item.couponCategory === couponEnum.TYPE.discount.value && (
                                          <Block>
                                            <Text className="coupon-bigprice noneColor">
                                              {filters.discountFilter(item.discountFee, true)}
                                            </Text>
                                            <Text className="coupon-bigprice noneColor">折</Text>
                                          </Block>
                                        )
                                      )}
                                    </View>
                                  ) : item.couponCategory === couponEnum.TYPE.freight.value ? (
                                    <View className="price-box price-box discount-box">
                                      {item.discountFee === 0 ? (
                                        <Text>免运费</Text>
                                      ) : (
                                        <View>
                                          <Text className="coupon-bigprice noneColor">￥</Text>
                                          <Text className="coupon-bigprice freightColor">
                                            {filters.moneyFilter(item.discountFee, true)}
                                          </Text>
                                        </View>
                                      )}
                                    </View>
                                  ) : item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                                    <View className="price-box price-box discount-box">
                                      <Text className="coupon-bigprice noneColor">￥</Text>
                                      <Text className="coupon-bigprice">
                                        {filters.moneyFilter(item.discountFee, true)}
                                      </Text>
                                    </View>
                                  ) : (
                                    item.couponCategory === couponEnum.TYPE.discount.value && (
                                      <View className="price-box discount-box">
                                        <Text className="coupon-bigprice">
                                          {filters.discountFilter(item.discountFee, true)}
                                        </Text>
                                        <Text className="coupon-bigprice">折</Text>
                                      </View>
                                    )
                                  )}
                                </View>
                                {item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                                  <Image
                                    mode="aspectFill"
                                    className="coupon-type-img"
                                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/full-reduction.svg"
                                  ></Image>
                                ) : item.couponCategory === couponEnum.TYPE.freight.value ? (
                                  <Image
                                    mode="aspectFill"
                                    className="coupon-type-img"
                                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/freight.svg"
                                  ></Image>
                                ) : (
                                  <Image
                                    mode="aspectFill"
                                    className="coupon-type-img"
                                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/discount.svg"
                                  ></Image>
                                )}

                                {/*  <view class='coupon-info'>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             </view>  */}
                              </View>
                              <View className="row-item">
                                <View className="coupon-title ">{item.name}</View>
                                <View className="coupon-time">
                                  {'有效期：' +
                                    (item.couponType === 0
                                      ? '领取后 ' + item.fixedTerm + ' 天有效'
                                      : item.beginTime + ' 至 ' + item.endTime)}
                                </View>
                                {item.minimumFee === 0 ? (
                                  <View className="gray">无门槛</View>
                                ) : (
                                  <View className="gray">
                                    {'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}
                                  </View>
                                )}
                              </View>
                              <View className="right-content">
                                {item.status === 3 ? (
                                  <View className="coupon-status">已过期</View>
                                ) : item.status === 2 ? (
                                  <View className="coupon-status">已使用</View>
                                ) : (
                                  <View
                                    className="coupon-btn"
                                    onClick={() => {this.handleUsed(item)}}
                                  >
                                    立即使用
                                  </View>
                                )}

                                <Image
                                  onClick={_fixme_with_dataset_(this.handleClick, {
                                    index: index,
                                  })}
                                  className={'icon ' + (item.isShow === true ? 'icon-show' : 'icon-hidden')}
                                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                ></Image>
                              </View>
                            </View>
                          </View>
                          {/*  右侧隐藏的删除按钮，左滑出现  */}
                          <View
                            onClick={() => {this.handleRemove(item.id)}}
                            className="coupon-del"
                          >
                            删除
                          </View>
                        </View>
                        {item.isShow && (
                          <View className="coupon-rule coupon-rule-list">
                            <View className="rule-content">
                              <View className="col-item">
                                <Text className="col-item-title">优惠说明：</Text>
                                {item.minimumFee === 0 ? (
                                  <View>无门槛</View>
                                ) : (
                                  <View>{'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}</View>
                                )}
                              </View>
                              <View className="col-item">
                                <Text className="col-item-title">适用范围：</Text>
                                {item.itemBriefs ? (
                                  <View className="limit" id="limit">
                                    {item.briefsStr}
                                    {item.isMore && (
                                      <View
                                        className={
                                          'more-item ' +
                                          (item.quantity > 0 && item.issueAmount === item.quantity
                                            ? 'noneColor'
                                            : 'redColor')
                                        }
                                        onClick={_fixme_with_dataset_(
                                          this.handleDialog,
                                          { moreBriefs: item.itemBriefs }
                                        )}
                                      >
                                        更多
                                      </View>
                                    )}
                                  </View>
                                ) : item.categoryBriefs ? (
                                  <View className="limit">
                                    {item.briefsStr}
                                    {item.isMore && (
                                      <View
                                        className={
                                          'more-item ' +
                                          (item.quantity > 0 && item.issueAmount === item.quantity
                                            ? 'noneColor'
                                            : 'redColor')
                                        }
                                        onClick={_fixme_with_dataset_(
                                          this.handleDialog,
                                          { moreBriefs: item.categoryBriefs }
                                        )}
                                      >
                                        更多
                                      </View>
                                    )}
                                  </View>
                                ) : (
                                  <View>全场通用</View>
                                )}
                              </View>
                              {item.storeBriefs && item.storeBriefs.length && (
                                <View className="col-item">
                                  <Text className="col-item-title">适用门店：</Text>
                                  {item.storeBriefs && (
                                    <View className="limit" id="limit">
                                      {item.storeBriefsStr}
                                      {item.isStoreMore && (
                                        <View
                                          className={
                                            'more-item ' +
                                            (item.quantity > 0 && item.issueAmount === item.quantity
                                              ? 'noneColor'
                                              : 'redColor')
                                          }
                                          onClick={_fixme_with_dataset_(
                                            this.handleDialog,
                                            { isStoreBriefs: true, moreBriefs: item.storeBriefs }
                                          )}
                                        >
                                          更多
                                        </View>
                                      )}
                                    </View>
                                  )}
                                </View>
                              )}

                              <View className="col-item">
                                <Text className="col-item-title">使用须知：</Text>
                                <View className="col-item-desc">{item.rules || '最终解释权归商家所有'}</View>
                              </View>
                            </View>
                          </View>
                        )}
                      </View>
                    </View>
                  </View>
                );
              })}
          </Block>
        )}

        {/*  我的优惠券模块  */}
        {!isMine && (
          <Block>
            {!!couponList.length &&
              couponList.map((item, index) => {
                return (
                  <View className="coupon-list" key={'coupon-list-2-' + index}>
                    <View className="coupon-item">
                      <View className="coupon-detail" data-item={item} data-type={item.receiveMethod}>
                        <View className="coupon-content">
                          <View style={_safe_style_('position: relative;')}>
                            <View className="coupon-price" style={_safe_style_(item.mTop)}>
                              {item.quantity > 0 && item.issueAmount === item.quantity ? (
                                <View
                                  className={
                                    'price-box discount-box  ' +
                                    (item.couponCategory === couponEnum.TYPE.discount.value ? 'discount-box' : '')
                                  }
                                >
                                  {item.couponCategory === couponEnum.TYPE.freight.value ||
                                    item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                                    <Block>
                                      <Text className="coupon-bigprice noneColor">￥</Text>
                                      <Text className="coupon-bigprice noneColor">
                                        {filters.moneyFilter(item.discountFee, true)}
                                      </Text>
                                    </Block>
                                  ) : (
                                    item.couponCategory === couponEnum.TYPE.discount.value && (
                                      <Block>
                                        <Text className="coupon-bigprice noneColor">
                                          {filters.discountFilter(item.discountFee, true)}
                                        </Text>
                                        <Text className="coupon-bigprice noneColor">折</Text>
                                      </Block>
                                    )
                                  )}
                                </View>
                              ) : item.couponCategory === couponEnum.TYPE.freight.value ? (
                                <View className="price-box discount-box freightColor">
                                  {item.discountFee === 0 ? (
                                    <Text style={_safe_style_('font-size: 36rpx;')}>免运费</Text>
                                  ) : (
                                    <View>
                                      <Text>￥</Text>
                                      <Text
                                        className="coupon-bigprice freightColor"
                                        style={_safe_style_(item.fontStyle)}
                                      >
                                        {filters.moneyFilter(item.discountFee, true)}
                                      </Text>
                                    </View>
                                  )}
                                </View>
                              ) : (
                                <View
                                  className={
                                    'price-box discount-box ' +
                                    (item.couponCategory === couponEnum.TYPE.discount.value ? 'discount-box' : '')
                                  }
                                >
                                  {item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                                    <Block>
                                      <Text className="coupon-bigprice">￥</Text>
                                      <Text className="coupon-bigprice">
                                        {filters.moneyFilter(item.discountFee, true)}
                                      </Text>
                                    </Block>
                                  ) : (
                                    item.couponCategory === couponEnum.TYPE.discount.value && (
                                      <Block>
                                        <Text className="coupon-bigprice">
                                          {filters.discountFilter(item.discountFee, true)}
                                        </Text>
                                        <Text className="coupon-bigprice">折</Text>
                                      </Block>
                                    )
                                  )}
                                </View>
                              )}

                              {/*  <view class="gray {{item.quantity > 0 && item.issueAmount === item.quantity ? 'noneColor' : 'redColor'}}"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               wx:else>满{{filters.moneyFilter(item.minimumFee,true)}}可用</view>  */}
                            </View>
                            {item.couponCategory === couponEnum.TYPE.fullReduced.value ? (
                              <Image
                                mode="aspectFill"
                                className="coupon-type-img"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/full-reduction.svg"
                              ></Image>
                            ) : item.couponCategory === couponEnum.TYPE.freight.value ? (
                              <Image
                                mode="aspectFill"
                                className="coupon-type-img"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/freight.svg"
                              ></Image>
                            ) : (
                              <Image
                                mode="aspectFill"
                                className="coupon-type-img"
                                src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/discount.svg"
                              ></Image>
                            )}

                            {/*  <view class='coupon-info'> <view class='coupon-title limit-line line-2'>{{item.name}}</view></view>  */}
                          </View>
                          <View className="row-item">
                            <View className="coupon-title ">{item.name}</View>
                            <View className="coupon-time">
                              {'有效期：' +
                                (item.couponType === 0
                                  ? '领取后 ' + item.fixedTerm + ' 天有效'
                                  : item.beginTime + ' 至 ' + item.endTime)}
                            </View>
                            {item.minimumFee === 0 ? (
                              <View className="gray">无门槛</View>
                            ) : (
                              <View className="gray">{'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}</View>
                            )}
                          </View>
                          <View className="right-content">
                            {item.alreadyHave === 1 ? (
                              <View>
                                {environment === 'wxwork' ? (
                                  <Button className="coupon-btn" openType="share">
                                    去分享
                                  </Button>
                                ) : (
                                  <Button
                                    className="coupon-btn"
                                    onClick={() => {this.handleToList(item)}}
                                  >
                                    立即使用
                                  </Button>
                                )}
                              </View>
                            ) : item.quantity > 0 && item.issueAmount === item.quantity ? (
                              <View className="coupon-status">已抢完</View>
                            ) : item.quantity > 0 && !item.alreadyHave ? (
                              <View className="coupon-box">
                                <View className="right111">
                                  <View className="progress">
                                    {!dialogShow && (
                                      <Circle
                                        bgId="circle_bg1"
                                        drawId="circle_draw1"
                                        size="96"
                                        strokeWidth="5"
                                        bgColor="#FFEEE7"
                                        progress={item.percentage}
                                      ></Circle>
                                    )}

                                    <View className="value">
                                      <Text>已领</Text>
                                      <Text>{item.percentage + '%'}</Text>
                                    </View>
                                  </View>
                                </View>
                                {environment === 'wxwork' ? (
                                  <Button className="coupon-btn" openType="share">
                                    去分享
                                  </Button>
                                ) : !item.receiveMethod ? (
                                  <View
                                    className="coupon-btn"
                                    onClick={() => {this.handleGetCoupon({ item: item, type: item.receiveMethod })} }
                                  >
                                    免费领取
                                  </View>
                                ) : (
                                  <View
                                    className="coupon-btn"
                                    onClick={() => {this.handleGetCoupon({ item: item, type: item.receiveMethod })} }
                                  >
                                    {item.integral + scoreName + '兑换'}
                                  </View>
                                )}
                              </View>
                            ) : (
                              <View>
                                {environment === 'wxwork' ? (
                                  <Button className="coupon-btn" openType="share">
                                    去分享
                                  </Button>
                                ) : !item.receiveMethod ? (
                                  <View
                                    className="coupon-btn"
                                    onClick={() => {this.handleGetCoupon({ item: item, type: item.receiveMethod })} }
                                  >
                                    免费领取
                                  </View>
                                ) : (
                                  <View
                                    className="coupon-btn"
                                    onClick={() => {this.handleGetCoupon({ item: item, type: item.receiveMethod })} }
                                  >
                                    {item.integral + scoreName + '兑换'}
                                  </View>
                                )}
                              </View>
                            )}

                            {/*  无限制发放量  */}
                            <Image
                              onClick={_fixme_with_dataset_(this.handleClick, {
                                index: index,
                              })}
                              className={'icon ' + (item.isShow === true ? 'icon-show' : 'icon-hidden')}
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                            ></Image>
                          </View>
                        </View>
                      </View>
                      {item.isShow && (
                        <View
                          className="coupon-rule"
                          onClick={_fixme_with_dataset_(this.handleClick, {
                            index: index,
                          })}
                        >
                          <View className="rule-content">
                            <View className="col-item">
                              <Text className="col-item-title">优惠说明：</Text>
                              {item.minimumFee === 0 ? (
                                <View>无门槛</View>
                              ) : (
                                <View>{'满' + filters.moneyFilter(item.minimumFee, true) + '可用'}</View>
                              )}
                            </View>
                            <View className="col-item">
                              <Text className="col-item-title">适用范围：</Text>
                              {item.itemBriefs ? (
                                <View className="limit" id="limit">
                                  {item.briefsStr}
                                  {item.isMore && (
                                    <View
                                      className={
                                        'more-item ' +
                                        (item.quantity > 0 && item.issueAmount === item.quantity
                                          ? 'noneColor'
                                          : 'redColor')
                                      }
                                      onClick={_fixme_with_dataset_(
                                        this.handleDialog,
                                        { moreBriefs: item.itemBriefs }
                                      )}
                                    >
                                      更多
                                    </View>
                                  )}
                                </View>
                              ) : item.categoryBriefs ? (
                                <View className="limit">
                                  {item.briefsStr}
                                  {item.isMore && (
                                    <View
                                      className={
                                        'more-item ' +
                                        (item.quantity > 0 && item.issueAmount === item.quantity
                                          ? 'noneColor'
                                          : 'redColor')
                                      }
                                      onClick={_fixme_with_dataset_(
                                        this.handleDialog,
                                        { moreBriefs: item.categoryBriefs }
                                      )}
                                    >
                                      更多
                                    </View>
                                  )}
                                </View>
                              ) : (
                                <View>全场通用</View>
                              )}
                            </View>
                            {item.storeBriefs && item.storeBriefs.length && (
                              <View className="col-item">
                                <Text className="col-item-title">适用门店：</Text>
                                {item.storeBriefs && (
                                  <View className="limit" id="limit">
                                    {item.storeBriefsStr}
                                    {item.isStoreMore && (
                                      <View
                                        className={
                                          'more-item ' +
                                          (item.quantity > 0 && item.issueAmount === item.quantity
                                            ? 'noneColor'
                                            : 'redColor')
                                        }
                                        onClick={_fixme_with_dataset_(
                                          this.handleDialog,
                                          { isStoreBriefs: true, moreBriefs: item.storeBriefs }
                                        )}
                                      >
                                        更多
                                      </View>
                                    )}
                                  </View>
                                )}
                              </View>
                            )}

                            <View className="col-item">
                              <Text className="col-item-title">使用须知：</Text>
                              <View className="col-item-desc">{item.rules || '最终解释权归商家所有'}</View>
                            </View>
                          </View>
                        </View>
                      )}
                    </View>
                  </View>
                );
              })}
          </Block>
        )}

        <Dialog
          visible={dialogShow}
          showTitle={false}
          showFooter={false}
          backOpacity={0.5}
          onCancel={this.handleCancel}
          maxHeight="600rpx">
            <Block>
              <View>
                <ScrollView
                  scrollY
                  style={_safe_style_('width:100%;text-align: left;max-height:500rpx;overflow: auto;')}
                >
                  {isStoreBriefs ? '适用门店：' : '适用范围：'}
                  {moreBriefs && (
                    <View style={_safe_style_('margin-top: 10rpx;')} id="limit">
                      <Block>
                        {moreBriefs &&
                          moreBriefs.map((brief, index) => {
                            return (
                              <View className="brief-item" key={'brief-item-' + index}>
                                {brief.name}
                                {index < moreBriefs.length - 1 && <Text>、</Text>}
                              </View>
                            );
                          })}
                      </Block>
                    </View>
                  )}
                </ScrollView>
              </View>
            </Block>
        </Dialog>
      </View>
    );
  }
}

export default CouponModule;
