import { $getRouter } from '@/wxat-common/utils/platform'
import React from 'react'
import AnimatDialog from '@/wxat-common/components/animat-dialog/index'
import { Block, View, Image } from '@tarojs/components';
// interface IState{
//   tipsCopywriting:string
// }
import './index.scss'
interface IProps{
  tipsCopywriting:string
}
class IosPayLimitDialog extends React.Component<IProps>{
  state:{
    tipsCopywriting:'十分抱歉，由于微信相关规范限制，您暂时无法购买'
  }
  $router = $getRouter()
  animatDialog = React.createRef<any>()
  componentDidMount(){
    // const options = this.$router.params
    // if (options.tipsCopywriting) {
    //   this.setState({
    //     tipsCopywriting: options.tipsCopywriting
    //   })
    // }
  }
   /**
   * 打开提示弹窗
   */
    showIospayLimitDialog=()=>{
      this.animatDialog.current.show({
        scale: 1,
      });
    }

    /**
     * 关闭提示弹窗
     */
    hideIospayLimitDialog=()=>{
      this.animatDialog.current.hide();
    }

    render(){
      return(
        <AnimatDialog  ref={this.animatDialog} id="iospay-limit-dialog" animClass="iospay-limit-dialog">
          <View className="content">
            <View className="title">温馨提示</View>
            <Image className="img" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/order/iospay-limit-img.png"/>
            <View className="tips">{this.props.tipsCopywriting}</View>
            <View className="ios-pay-btn" onClick={this.hideIospayLimitDialog}>知道了</View>
          </View>
        </AnimatDialog>
      )
    }
}
export default IosPayLimitDialog
