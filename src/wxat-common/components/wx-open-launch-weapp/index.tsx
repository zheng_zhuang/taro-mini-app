import { View, Button, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import './index.scss';

interface WxOpenWeapp {
  props: {
    username: string, // 小程序原始id，gh开头 - 必填
    path: string, // 跳转路径
    styles: string, // 样式
    slotTel: any, // 跳转按钮slot
  }
}

class WxOpenWeapp extends React.Component {
  componentDidMount() { 
    const btn = document.getElementById('launch-btn');

    btn?.addEventListener('ready', function (e) {
    });

    btn?.addEventListener('launch', function (e) {
    });

    btn?.addEventListener('error', function (e) {
    });
  }

  render() {
    const { username , path, slotTel, styles } = this.props;
    const _style = {
      display: 'block'
    }
    return (
      <wx-open-launch-weapp
        id="launch-btn"
        class="wk-WxOpenWeapp"
        username={username}
        path={path}>
          <script type="text/wxtag-template" style={_style}>
            <style>{ styles }</style>
            { slotTel }
          </script>
      </wx-open-launch-weapp>
    );
  }
}

export default WxOpenWeapp;
