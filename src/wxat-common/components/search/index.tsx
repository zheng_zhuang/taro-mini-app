import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../utils/wxApi';
import './index.scss';

import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl'
// props的类型定义
const Search: FC = () => {
  /**
   * 点击搜索按钮
   */
  const clickSearch = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/search/index',
    });
  };

  return (
    <View data-scoped='wk-wcs-Search' className='wk-wcs-Search search-container'>
      <View className='search-btn' onClick={clickSearch}>
        <Image className='icon' src={getStaticImgUrl.images.search_icon_png}></Image>
        <View className='search-tip'>搜搜你想要的商品</View>
      </View>
    </View>
  );
};

// 给props赋默认值
Search.defaultProps = {};

export default Search;
