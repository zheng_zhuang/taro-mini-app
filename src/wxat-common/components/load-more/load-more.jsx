import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './load-more.scss';

// BASELINE 到底了,
const STATUS = { HIDE: 0, LOADING: 1, ERROR: 2, BASELINE: 3 };

class LoadMore extends React.Component {
  static defaultProps = {
    loadingText: '加载中...',
    loadingBtnText: '我可是有底线的...',
    errorText: '加载失败, 点击重试',
    status: 0,
    //点击重试事件回调
    onRetry: null,
  };

  onErrorClickRetry = () => {
    const onRetry = this.props.onRetry;
    onRetry && onRetry();
  };

  render() {
    const { loadingText, status, errorText, loadingBtnText } = this.props;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-wcl-LoadMore' className='wk-wcl-LoadMore'>
        {
          status === STATUS.LOADING && (
            <View className='list-loading'>
              <Text>{loadingText}</Text>
            </View>
          )
        }
        {status === STATUS.ERROR && (
          <View className='list-error' onClick={this.onErrorClickRetry}>
            {errorText}
          </View>
        )}

        {/* 有底线的 */}
        {status === STATUS.BASELINE && <View className='list-loading'>{loadingBtnText}</View>}
      </View>
    );
  }
}

export default LoadMore;
