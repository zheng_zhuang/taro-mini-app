import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, Button, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';

class AuthInfoBtn extends React.Component {
  static defaultProps = {
    style: `width:280rpx;height:80rpx;background:rgba(38,193,127,1);border-radius:10rpx;margin:0;font-size:28rpx;font-family:PingFangSC;font-weight:400;color:rgba(255,255,255,1);`,
    hideBorder: true,
    text: '',
    //用户信息回调
    onUserInfoCall: null,
  };

  static options = {
    addGlobalClass: true,
  };

  static externalClasses = ['my-class'];

  state = {
    canIUseGetUserProfile: false,
  };

  componentDidMount() {
    //基础库 2.10.4 开始支持,版本兼容getUserProfile，不支持则使用getUserInfo
    if (wx.getUserProfile) {
      this.setState({
        canIUseGetUserProfile: true,
      });
    }
  }
  handleUserProfile = () => {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        const { onUserInfoCall } = this.props;
        onUserInfoCall && onUserInfoCall(res);
      },
    });
  };

  handlerWxUserInfoCall = (e) => {
    const { onUserInfoCall } = this.props;
    onUserInfoCall && onUserInfoCall(e.detail);
  };

  render() {
    const { style, hideBorder, text } = this.props;
    return (
      <Block>
        {this.canIUseGetUserProfile ? (
          <Button
            onClick={this.handleUserProfile}
            type='primary'
            style={_safe_style_(style)}
            className={'auth-btn ' + (hideBorder ? 'none-border' : '') + ' my-class'}
          >
            <Text>{text}</Text>
          </Button>
        ) : (
          <Button
            openType='getUserInfo'
            onGetuserinfo={this.handlerWxUserInfoCall}
            type='primary'
            style={_safe_style_(style)}
            className={'auth-btn ' + (hideBorder ? 'none-border' : '') + ' my-class'}
          >
            <Text>{text}</Text>
          </Button>
        )}
      </Block>
    );
  }
}

export default AuthInfoBtn;
