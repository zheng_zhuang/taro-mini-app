import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, Button, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';

class AuthPhoneBtn extends React.Component {
  static defaultProps = {
    style: `width:280rpx;height:80rpx;background:rgba(38,193,127,1);border-radius:10rpx;margin:0;font-size:28rpx;font-family:PingFangSC;font-weight:400;color:rgba(255,255,255,1);`,
    hideBorder: true,
    text: '',
    //手机号回调
    onPhoneNumberCall: null,
  };

  handlerWxPhoneNumberCall = (e) => {
    const { onPhoneNumberCall } = this.props;
    onPhoneNumberCall && onPhoneNumberCall(e.detail);
  };

  render() {
    const { style, hideBorder, text } = this.props;
    return (
      <Block>
        <Button
          openType='getPhoneNumber'
          onGetphonenumber={this.handlerWxPhoneNumberCall}
          type='primary'
          style={_safe_style_(style)}
          className={'auth-btn ' + (hideBorder ? 'none-border' : '')}
        >
          <Text>{text}</Text>
        </Button>
      </Block>
    );
  }
}

export default AuthPhoneBtn;
