import React from 'react'; // @externalClassesConvered(AnimatDialog)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Button, View, Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import classnames from 'classnames';
import api from '../../api/index';
import wxApi from '../../utils/wxApi';
import report from '../../../sdks/buried/report/index';
import login from '../../x-login/index';
import { isRegistered, WXOARegister } from '@/wxat-common/x-login/login.h5';
import './index.scss';
import { updateBaseAction } from '../../../redux/base';
import { connect } from 'react-redux';
import shareUtil from '../../utils/share';
import { PHONE_REG } from '@/wxat-common/utils/regexp';

const LOCAL_WX_INFO_KEY = 'wx-info-11081';
// 验证码超时时间，秒
const VERIFY_TIMEOUT = 60;
const sceneConst = {
  AREA: 'area',
  BTN: 'btn',
  CUSTOM_BTN: 'custom_btn',
};

const mapStateToProps = (state) => ({
  registered: state.base.registered,
  userInfo: state.base.userInfo,
  wxUserInfo: state.base.wxUserInfo,
  loginInfo: state.base.loginInfo,
  promotion: state.base.promotion,
  shareId: state.base.shareId,
  targetType: state.base.targetType,
  taskReferId: state.base.taskReferId,
  taskType: state.base.taskType,
  newUser: state.base.newUser,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateBase: (data) => dispatch(updateBaseAction(data)),
});

/**
 * 公众号端已经获取了用户信息，所以仅需要绑定手机号码
 */
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class AuthUser extends React.Component {
  static defaultProps = {
    //是否强制刷新用户信息
    forceUpdate: false,
    //使用场景
    scene: sceneConst.AREA,
    //自定义按钮的样式style
    custom_style: '',
    //自定义按钮的描述
    custom_btn_desc: '',
    showSlot: false,
    hideBorder: false,
    /**
     * 是否是授权的单独页面
     */
    isAuthPage: false,
    /**
     * 是否强制回到首页
     */
    isForceHome: false,
    //ready回到
    onReady: null,
  };

  /**
   * 组件的初始数据
   */
  state = {
    viewAlreadyDone: false,
    showRequirePhone: false, //显示授权获取电话信息,
    phone: '',
    phoneValidated: false,
    verify: '',
    verifyTimeout: 0,

    btnStyle:
      'width:280rpx;' +
      'height:80rpx;' +
      'background:rgba(38,193,127,1);' +
      'border-radius:10rpx;margin:0;' +
      'font-size:28rpx;' +
      'font-family:PingFangSC;' +
      'font-weight:400;' +
      'color:rgba(255,255,255,1);',
  };

  timer = null;
  loading = false;
  readyIsCall = false;

  /**
   * 是否可以返回，微信公众号授权重定向后是无法返回的
   */
  get canGoback() {
    return wxApi.$canGoback();
  }

  async componentDidMount() {
    await login.waitLogin();
    this.initCheck();
  }

  componentDidUpdate(preProps) {
    const { isAuthPage, registered } = this.props;
    if (isAuthPage) {
      if (!preProps.registered && registered) {
        this.judge();
      }
    }
  }

  componentWillUnmount() {
    if (this.timer != null) {
      window.clearTimeout(this.timer);
      this.timer = null;
    }
  }

  async initCheck() {
    const { registered } = this.props;
    const registeredYet = registered;
    if (registeredYet) {
      this.viewDone();
      return;
    }

    // 检查是否引导过注册
    // if (process.env.WX_OA === 'true') {
    //   if (!isRegistered()) {
    //     // 引导获取userInfo
    //     WXOARegister();
    //     return;
    //   }
    // }

    // 获取用户信息
    try {
      const userInfo = await wxApi.getUserInfo();
      this.setState(
        {
          userInfo: userInfo.userInfo,
        },

        () => {
          this.judge();
        }
      );
    } catch (err) {
      console.error('get UserInfo fail', err);
    }
  }

  judge() {
    const { registered } = this.props;
    const { viewAlreadyDone } = this.state;
    const showRequirePhone = !registered;
    console.log('showRequirePhone', showRequirePhone, 'viewAlreadyDone', viewAlreadyDone);

    if (viewAlreadyDone) {
      return;
    }

    //其次需要电话授权
    if (showRequirePhone) {
      this.setState({
        showRequirePhone: true,
      });
    } else {
      this.report();
    }
  }

  viewDone() {
    this.setState({
      showRequirePhone: false,
      viewAlreadyDone: true,
    });
    console.log('onReady已调用=====', !this.readyIsCall)
    if (!this.readyIsCall) {
      const onReady = this.props.onReady;
      onReady && onReady();
      this.readyIsCall = true
    }
  }

  report = async () => {
    const { loginInfo, registered, userInfo: storeUserInfo } = this.props;
    const { userInfo } = this.state;
    report.user(loginInfo, userInfo);
    wxApi.setStorageSync(LOCAL_WX_INFO_KEY, userInfo);

    try {
      this.loading = true;

      if (!registered) {
        const res = await this.apiRegister(userInfo);
        this.props.dispatchUpdateBase({
          userInfo: { ...storeUserInfo, ...{ id: res.data.id, phone: res.data.phone } },
          loginInfo: { ...loginInfo, ...{ userId: res.data.id } },
          wxUserInfo: userInfo,
          registered: true,
        });

        shareUtil.setUseId(res.data.id);
        this.viewDone();
        this.apiUpdateUser(userInfo, false);

        //上报用户画像信息
        const { wxUnionId, wxOpenId, phone } = res.data || {};
        if (!!wxOpenId) {
          report.user(
            {
              unionId: wxUnionId || '',
              openId: wxOpenId || '',
              phone: phone || '',
            },

            userInfo
          );
        }
        report.user(loginInfo, userInfo);
        report.reportAuthOperation(true, 1);
      } else {
        this.viewDone();
      }
      console.log(11111)
      // localStorage.clear()
      // sessionStorage.clear()
      Taro.clearStorage();

    } catch (err) {
      const message = err.message || (err.data && err.data.errorMessage);
      wxApi.showToast({ title: `用户注册失败: ${message}`, icon: 'none' });
      console.error(err);
    } finally {
      console.log(2222)
      this.loading = false;
    }
  };

  apiUpdateUser(userInfo, ignoreGender) {
    const params = {
      avatarImgUrl: userInfo['avatarUrl'],
      wxNickname: userInfo['nickName'],
      city: userInfo['city'],
      country: userInfo['country'],
      province: userInfo['province'],
      language: userInfo['language'],
      deviceName: 'unknown',
      network: '4g',
    };

    if (!ignoreGender) {
      Object.assign(params, {
        gender: userInfo['gender'],
      });
    }
    wxApi
      .request({
        url: api.user.updateUser,
        data: params,
      })
      .then((res) => {
        this.updateLocalUserInfo(ignoreGender);
      });
  }

  updateLocalUserInfo(ignoreGender) {
    const { userInfo: storeUserInfo, wxUserInfo: storeWxUserInfo = {} } = this.props;
    const { userInfo } = this.state;
    storeUserInfo.avatarImgUrl = userInfo.avatarUrl;
    storeUserInfo.city = userInfo.city;
    storeUserInfo.country = userInfo.country;
    if (!ignoreGender) {
      storeUserInfo.gender = userInfo.gender;
    }

    storeUserInfo.nickname = userInfo.nickName;
    storeUserInfo.province = userInfo.province;

    storeWxUserInfo.avatarUrl = userInfo.avatarUrl;
    storeWxUserInfo.city = userInfo.city;
    storeWxUserInfo.country = userInfo.country;
    if (!ignoreGender) {
      storeWxUserInfo.gender = userInfo.gender;
    }
    storeWxUserInfo.nickName = userInfo.nickName;
    storeWxUserInfo.province = userInfo.province;
    this.props.dispatchUpdateBase({
      userInfo: { ...storeUserInfo },
      wxUserInfo: { ...storeWxUserInfo },
    });
  }

  apiRegister(userInfo) {
    const params = {
      avatarImgUrl: userInfo['avatarUrl'],
      nickname: userInfo['nickName'],
      city: userInfo['city'],
      country: userInfo['country'],
      province: userInfo['province'],
      gender: userInfo['gender'],
      language: userInfo['language'],
      // TODO: 验证号码
      phone: this.state.phone,
      verificationCode: this.state.verify,
    };

    const userMessage = login.getBuryInfo();
    if (userMessage._ref_useId) {
      params.shareUserId = userMessage._ref_useId;
    }

    return wxApi.request({
      url: api.wxoa.register,
      method: 'POST',
      data: params,
    });
  }

  startTimer = () => {
    this.timer = setTimeout(() => {
      this.timer = null;
      const remain = this.state.verifyTimeout - 1;
      this.setState({ verifyTimeout: remain });
      if (remain > 0) {
        this.startTimer();
      }
    }, 1000);
  };

  handlePhoneInput = (e) => {
    const value = e.detail.value;
    const validated = !!value.match(PHONE_REG);
    this.setState({ phone: e.detail.value, phoneValidated: validated });
  };

  handleVerifyInput = (e) => {
    this.setState({ verify: e.detail.value });
  };

  handleSendVerify = async () => {
    if (this.state.verifyTimeout > 0) {
      return;
    }

    if (!this.state.phoneValidated) {
      wxApi.showToast({ title: '请输入正确的手机号码', icon: 'none' });
      return;
    }

    // 开始计时器
    this.setState({ verifyTimeout: VERIFY_TIMEOUT }, this.startTimer);

    // 发送验证码
    try {
      await wxApi.request({
        url: api.wxoa.sendVerifyCode,
        data: {
          phone: this.state.phone,
        },
      });

      wxApi.showToast({ title: '验证码已发送', icon: 'none' });
    } catch (err) {
      wxApi.showToast({ title: '验证码发送失败', icon: 'none' });
      console.error(err);
    }
  };

  handleAccept = () => {
    if (this.loading) {
      return;
    }

    if (!this.state.phoneValidated) {
      wxApi.showToast({ title: '请输入正确的手机号码', icon: 'none' });
      return;
    }

    if (this.state.verify == null || this.state.verify.trim() === '') {
      wxApi.showToast({ title: '请输入验证码', icon: 'none' });
      return;
    }

    this.report();
  };

  handleReject = () => {
    wxApi.showModal({
      title: '提示',
      content: '这将导致您无法使用部分功能，确定继续?',
      confirmText: '继续',
      showCancel: true,
      success: (option) => {
        if (option.confirm) {
          // 公众号跳转到首页
          if (process.env.WX_OA === 'true' && !this.canGoback) {
            wxApi.$redirectTo({
              url: '/wxat-common/pages/home/index',
            });

            return;
          }

          const { isAuthPage, isForceHome } = this.props;
          if (isAuthPage && !isForceHome) {
            wxApi.$navigateBack();
          } else {
            wxApi.$navigateTo({
              url: '/wxat-common/pages/home/index',
            });
          }
        }
      },
    });
  };

  render() {
    const { showSlot } = this.props;
    const { btnStyle, showRequirePhone } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-wca-AuthUser' className='wk-wca-AuthUser'>
        {/*  手机:默认使用授权区域  */}
        {!!showRequirePhone && (
          <View className='auth-info'>
            <View className='auth-title'>授权提示</View>
            <View className='auth-desc'>请授权您的信息以便获得更好的使用体验</View>

            <View>
              <View className='auth-input'>
                <Input
                  className='auth-input__input'
                  placeholder='手机号码'
                  value={this.state.phone}
                  onInput={this.handlePhoneInput}
                />
              </View>
              <View className='auth-input'>
                <Input
                  className='auth-input__input'
                  placeholder='验证码'
                  value={this.state.verify}
                  onInput={this.handleVerifyInput}
                />

                <Button
                  className={classnames('auth-input__btn', {
                    'auth-input__btn--disabled': !this.state.phoneValidated || this.state.verifyTimeout > 0,
                  })}
                  onClick={this.handleSendVerify}
                >
                  {this.state.verifyTimeout > 0 ? this.state.verifyTimeout + ' s' : '发送验证码'}
                </Button>
              </View>
            </View>

            <View className='auth-btn-box'>
              <Button
                className='auth-btn none-border'
                type='primary'
                style={_safe_style_(btnStyle)}
                onClick={this.handleAccept}
              >
                绑定
              </Button>
              <View className='auth-reject-btn' onClick={this.handleReject} style={{ marginLeft: 5 }}>
                拒绝
              </View>
            </View>
          </View>
        )}

        {!showRequirePhone && (showSlot ? this.props.renderContent : this.props.children)}
      </View>
    );
  }
}

export default AuthUser;
