import React from 'react'; /* eslint-disable react/sort-comp */
// @externalClassesConvered(AnimatDialog)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../api/index';
import wxApi from '../../utils/wxApi';
import report, { wkApi } from '../../../sdks/buried/report/index';
import login from '../../x-login/index';
import AuthInfoBtn from './AuthInfoBtn';
import AuthPhoneBtn from './AuthPhoneBtn';
import './index.scss';
import { updateBaseAction } from '../../../redux/base';
import { connect } from 'react-redux';
import shareUtil from '../../utils/share';
import LinkType from '../../constants/link-type.js';
import BZSceneUtil from '@/wxat-common/utils/bz-scene-util';

const LOCAL_WX_INFO_KEY = 'wx-info-11081';
const sceneConst = {
  AREA: 'area',
  CUSTOM_BTN: 'custom_btn',
};

//wxUserInfo & userInfo & registered 来源登录接口
//wxUserInfo 来源也可能是 首页获取头像交互入口

const mapStateToProps = (state) => ({
  sessionId: state.base.registered,
  registered: state.base.registered,
  wxUserInfo: state.base.wxUserInfo,
  loginInfo: state.base.loginInfo,
  promotion: state.base.promotion,
  shareId: state.base.shareId,
  targetType: state.base.targetType,
  taskReferId: state.base.taskReferId,
  taskType: state.base.taskType,
  newUser: state.base.newUser,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateBase: (data) => dispatch(updateBaseAction(data)),
});

export default
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class AuthUser extends React.Component {
  static defaultProps = {
    //使用场景
    scene: sceneConst.AREA,
    //自定义按钮的样式style
    custom_style: '',
    //自定义按钮的描述
    custom_btn_desc: '',
    //是否隐藏边框
    hideBorder: false,
    //是否是授权的单独页面
    isAuthPage: false,
    //是否强制回到首页
    isForceHome: false,
    //ready回到
    onReady: null,
  };

  state = {
    //
    viewAlreadyDone: false,
    //是否显示授权获取用户信息
    showRequireInfo: true,
    //是否显示授权获取电话信息
    showRequirePhone: false,
  };

  // 来源业务场景信息
  BZSceneInfo = {};

  /**
   * @param {IdlWeChatEncryptedInfo | null}
   */
  tmpPhoneEncryptedInfo = null;
  /**
   * @param {IdlWeChatEncryptedInfo | null}
   */
  tmpStoreUserEncryptedInfo = null;
  /**
   * @param {IdlWeChatAuthUserInfo | null}
   */
  tmpStoreWxUserInfo = null;

  //初始化授权检查逻辑
  initAuthLogic() {
    this._initAuthYet = true;
    //已注册过的用户，执行退出逻辑
    if (!!this.props.wxUserInfo && !!this.props.registered) this.exitAuthLogic();
    else this.checkAuthStatus();
  }

  //login之后只进入一次 授权逻辑交互
  componentDidMount() {
    if (!!this.props.sessionId && !this._initAuthYet) this.initAuthLogic();
    this.BZSceneInfo = BZSceneUtil.getPrev.auth;
  }

  //同componentDidMount
  componentDidUpdate(prevProps) {
    if (prevProps.sessionId !== this.props.sessionId && !!this.props.sessionId && !this._initAuthYet)
      this.initAuthLogic();
  }

  checkAuthStatus() {
    if (this.state.viewAlreadyDone) return;
    //优先 授权 用户信息
    if (!this.props.wxUserInfo && !this.tmpStoreWxUserInfo)
      this.setState({ showRequirePhone: false, showRequireInfo: true });
    //其次需要电话授权
    else if (!this.props.registered && !this.tmpPhoneEncryptedInfo)
      this.setState({ showRequirePhone: true, showRequireInfo: false });
    else this.report();
  }

  /**
   * 微信手机信息授权回调
   * @param detail {IdlWeChatEncryptedInfo}
   **/
  handlerWxPhoneNumberCall = (detail) => {
    //防重复操作
    if (!!this.tmpPhoneEncryptedInfo && !!this.tmpPhoneEncryptedInfo.encryptData) return;

    const { encryptedData, iv } = detail || {};
    if (!!encryptedData && !!iv) {
      this.tmpPhoneEncryptedInfo = {
        encryptData: encryptedData,
        iv,
      };

      this.checkAuthStatus();
    } else {
      report.reportAuthOperation(false, 1);
    }
  };

  /**
   * 微信用户信息授权回调
   * @param detail {IdlWeChatAuthProfile}
   **/
  handlerWxUserInfoCall = (detail) => {
    //防重复操作
    if (!!this.tmpStoreWxUserInfo && !!this.tmpStoreWxUserInfo.nickName) return;

    const { encryptedData, iv } = detail || {};
    if (!!encryptedData && !!iv) {
      this.tmpStoreUserEncryptedInfo = {
        encryptData: encryptedData,
        iv,
      };

      this.tmpStoreWxUserInfo = detail.userInfo;
      wkApi.updateRunTimeBzParam({ user: this.tmpStoreWxUserInfo });
      report.reportAuthOperationV2(true, 0, this.BZSceneInfo);
      this.checkAuthStatus();
    } else {
      report.reportAuthOperation(false, 0);
    }
  };

  //标记登录授权完成
  exitAuthLogic() {
    this.setState({ showRequirePhone: false, showRequireInfo: false, viewAlreadyDone: true });
    const onReady = this.props.onReady;
    onReady && onReady();
  }

  report() {
    const { loginInfo, registered, wxUserInfo } = this.props;

    //新数据本地保存
    const userInfo = wxUserInfo || this.tmpStoreWxUserInfo;
    wxApi.setStorageSync(LOCAL_WX_INFO_KEY, userInfo);

    //防止重复点击
    if (this.reporting) return;
    this.reporting = true;

    //注册过不再重复处理
    if (!!registered) return this.exitAuthLogic();

    //用户信息的db结构
    const dbUserInfo = {
      ...userInfo,
      ...{
        //转换 微信字段与中台不一致字段
        avatarImgUrl: userInfo.avatarUrl,
        nickname: userInfo.nickName,
      },
    };

    Taro.showLoading({ title: '注册中', mask: true });

    this.apiRegister(dbUserInfo, this.tmpPhoneEncryptedInfo, this.tmpStoreUserEncryptedInfo)
      .finally(() => {
        if (this.reporting) {
          this.reporting = false;
          Taro.hideLoading();
        }
      })
      .then((res) => {
        //更新获取的信息
        const userId = res.data.id;
        const phone = res.data.phone || '';
        const openId = res.data.wxOpenId || '';
        const unionId = res.data.wxUnionId || '';

        //保存用户信息
        dbUserInfo.id = userId;
        dbUserInfo.phone = phone;
        AuthUser.apiUpdateUser(dbUserInfo);

        //更新全局用户信息
        this.props.dispatchUpdateBase({
          userInfo: dbUserInfo,
          loginInfo: { ...loginInfo, userId },
          wxUserInfo: userInfo,
          registered: true,
        });

        shareUtil.setUseId(res.data.id);

        //上报用户画像信息
        report.user(
          { ...loginInfo, unionId, openId, phone },
          {
            ...userInfo,
            id: res.data.id,
            storeId: res.data.storeId,
          }
        );

        report.reportRegister(this.BZSceneInfo);
        report.reportAuthOperationV2(true, 1, this.BZSceneInfo);

        if (this.reporting) {
          this.reporting = false;
          Taro.hideLoading();
        }
        this.exitAuthLogic();
      });
  }

  // call中台注册微信用户
  async apiRegister(dbUserInfo, phoneEncryptedInfo, userEncryptedInfo) {
    const params = {
      ...dbUserInfo,
      infoEncryptData: userEncryptedInfo.encryptData,
      infoIv: userEncryptedInfo.iv,

      encryptData: phoneEncryptedInfo.encryptData,
      iv: phoneEncryptedInfo.iv,
    };

    const userMessage = login.getBuryInfo();

    if (userMessage._ref_useId) {
      params.shareUserId = userMessage._ref_useId;
    }

    const data = await wxApi.request({
      url: api.login.register,
      method: 'POST',
      data: params,
    });

    const { promotion, shareId, targetType, taskReferId, taskType, newUser } = this.props;

    if (promotion && shareId && targetType && newUser && taskType == 1) {
      AuthUser.addInviteRecord(shareId);
    } else if (promotion && shareId && targetType && taskReferId && taskType == 2) {
      AuthUser.addInviteRecord(shareId, taskReferId);
    }

    return data;
  }

  static apiUpdateUser(dbUserInfo) {
    const info = {
      deviceName: (wxApi.getSystemInfoSync() || {}).model || '',
      network: '',
    };

    wxApi.getNetworkType({
      success(e) {
        !!e && !!e['networkType'] && (info.network = e['networkType']);
      },
      complete() {
        wxApi.request({
          url: api.user.updateUser,
          data: { ...dbUserInfo, ...info },
        });
      },
    });
  }

  //任务中心邀请上报
  static addInviteRecord(shareId, referId) {
    const data = { shareId };
    if (referId) {
      data.referId = referId;
    }
    wxApi.request({
      url: api.taskCenter.addInviteRecord,
      data,
      method: 'POST',
    });
  }

  onRejectAuth = () => {
    const { isAuthPage, isForceHome } = this.props;
    if (isAuthPage && !isForceHome) {
      wxApi.$navigateBack();
    } else {
      wxApi.$navigateTo({
        url: LinkType.navigate.HOME,
      });
    }
  };

  render() {
    const { hideBorder, scene, custom_btn_desc, custom_style } = this.props;
    const { showRequireInfo, showRequirePhone } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-wca-AuthUser' className='wk-wca-AuthUser'>
        {/*  用户信息:默认使用授权区域  */}
        {!!(showRequireInfo && scene === sceneConst.AREA) && (
          <View className='auth-info'>
            <View className='auth-title'>授权提示</View>
            <View className='auth-desc'>请授权您的信息以便获得更好的使用体验</View>
            <View className='auth-label'>该程序将获取以下授权</View>
            <View className='auth-content-box'>
              <View className='auth-dot'></View>
              <View>获取你的昵称、头像、性别、地区及位置</View>
            </View>

            <View className='auth-btn-box'>
              <View className='auth-reject-btn' onClick={this.onRejectAuth}>
                拒绝
              </View>
              <AuthInfoBtn text='同意授权' hideBorder onUserInfoCall={this.handlerWxUserInfoCall} />
            </View>
          </View>
        )}

        {/*  用户信息:自定义授权按钮  */}
        {!!(showRequireInfo && scene === sceneConst.CUSTOM_BTN) && (
          <AuthInfoBtn
            hideBorder={hideBorder}
            text={custom_btn_desc}
            style={custom_style}
            onUserInfoCall={this.handlerWxUserInfoCall}
          />
        )}

        {/*  手机:默认使用授权区域  */}
        {!!(showRequirePhone && scene === sceneConst.AREA) && (
          <View className='auth-info'>
            <View className='auth-title'>授权提示</View>
            <View className='auth-desc'>请授权您的信息以便获得更好的使用体验</View>
            <View className='auth-label'>该程序将获取以下授权</View>
            <View className='auth-content-box'>
              <View className='auth-dot'></View>
              <View>获取你的手机号</View>
            </View>

            <View className='auth-btn-box'>
              <View className='auth-reject-btn' onClick={this.onRejectAuth}>
                拒绝
              </View>
              <AuthPhoneBtn text='获取手机号码' hideBorder onPhoneNumberCall={this.handlerWxPhoneNumberCall} />
            </View>
          </View>
        )}

        {/*  手机:自定义授权按钮  */}
        {!!(showRequirePhone && scene === sceneConst.CUSTOM_BTN) && (
          <AuthPhoneBtn
            hideBorder={hideBorder}
            text={custom_btn_desc ? custom_btn_desc : '微信用户快捷登录'}
            style={custom_style}
            onPhoneNumberCall={this.handlerWxPhoneNumberCall}
          />
        )}

        {!showRequireInfo && !showRequirePhone && this.props.children}
      </View>
    );
  }
}
