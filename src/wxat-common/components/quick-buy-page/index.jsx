import React from 'react';
import { PageStyle, _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import screen from '@/wxat-common/utils/screen';
import Classify from '@/wxat-common/components/classify/index';
import { connect } from 'react-redux';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';

import './index.scss';

const mapStateToProps = (state) => ({
  currentStoreViewVisible: state.base.currentStoreViewVisible,
  globalData: state.globalData,
});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })

class QuickBuyPage extends React.Component {
  state = {
    showClassify: true,
    reportSource: reportConstants.SOURCE_TYPE.quick_buy.key,
    navHeight: 0,
    navBackgroundColor: '',
  };

  componentDidMount() {
    this.setState({
      navHeight: screen.totalNavigationBarHeight,
    });

    wxAppProxy.setNavColor('pages/quick-buy/index', 'wxat-common/pages/quick-buy/index');
    const params = wxAppProxy.getNavColor('wxat-common/pages/quick-buy/index');
    this.setNavColor(params);
  } /*请尽快迁移为 componentDidUpdate*/

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const { currentStoreViewVisible, globalData } = this.props;
    if (currentStoreViewVisible !== nextProps.currentStoreViewVisible) {
      this.setState({
        showClassify: nextProps.currentStoreViewVisible,
      });
    } else if (globalData !== nextProps.globalData) {
      this.setState({
        navHeight: screen.totalNavigationBarHeight,
      });
    }
  }

  onReachBottom() {
    this.classifyCOMPT && this.classifyCOMPT.onScrollViewReachBottom();
  }

  setNavColor(params) {
    if (params) {
      this.setState({
        navBackgroundColor: params.navBackgroundColor || '#ffffff', // 必写项
      });
    }
  }

  refClassifyCOMPT = (node) => (this.classifyCOMPT = node);

  // config = {
  //   navigationStyle: 'custom',
  //   navigationBarTitleText: '产品分类'
  // }

  render() {
    const { navHeight, navBackgroundColor, reportSource, showClassify } = this.state;
    return (
      <View
        data-scoped='wk-wpq-QuickBuy'
        className='wk-wpq-QuickBuy page'
        style={_safe_style_(PageStyle(this.props.style))}
      >
        {!!showClassify && (
          <Classify
            ref={this.refClassifyCOMPT}
            navBackgroundColor={navBackgroundColor}
            isQuickBuyOpen
            reportSource={reportSource}
          ></Classify>
        )}
      </View>
    );
  }
}

export default QuickBuyPage;
