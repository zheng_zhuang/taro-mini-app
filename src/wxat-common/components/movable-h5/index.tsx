import { View } from '@tarojs/components'
import { Component } from 'react'
import './index.scss'

class MovableViewH5 extends Component {

  // 边距
  static defaultProps = {
    margin: 10,
    top: 0,
    // left 与 right 只能一个有值，其中一个必是auto
    left: 0,
    right: 'auto'
  }

  state = {
    x: this.props.left !== 'auto' ? this.props.margin + this.props.left + 'px' : 'auto',
    y: this.props.top + this.props.margin + 'px'
  }

  // 移动事件
  handleTouchMove = (e) => {
    const margin = this.props.margin
    const h = e.target.offsetHeight
    const w = e.target.offsetWidth
    let x = e.touches[0].clientX - w / 2
    let y = e.touches[0].clientY- h / 2
    const innerHeight = e.view.innerHeight
    const innerWidth = e.view.innerWidth
    console.log(w,'wwwwwwwwwwwwww');
    console.log(e,'eeeeeeeeeeeeeeeeeee');
    
    console.log(x,'xxxxxxxxxxxxxxxx');
    console.log(y,'yyyyyyyyyyyyyyy');

    if (x <= margin) {
      x = margin
    }

    if (y <= margin) {
      y = margin
    }
    if ((y + h + margin) >= innerHeight) {
      y = innerHeight - h - margin
    }

    if ((x + w + margin) >= innerWidth) {
      x = innerWidth - w - margin
    }

    this.setState(
      {
        x: x + 'px',
        y: y + 'px'
      }
    )
    return false
  }


  // 离开屏幕事件
  handleTouchEnd = ({target, changedTouches, view}) => {
    const margin = this.props.margin
    const innerWidth = view.innerWidth / 2
    let w = target.offsetWidth
    const x = changedTouches[0].clientX
    const right = view.innerWidth - w - margin
    if (x < innerWidth) {
      this.setState(
        {
          x: margin + 'px'
        }
      )
    } else {
      this.setState(
        {
          x: right + 'px'
        }
      )
    }
  }

  // 调用父组件传进来的方法
  handlerClick = (e) => {
    e.stopPropagation()
    this.props.passClick()
  }

  render () {
    const { x, y } = this.state
    return (
      <View className="move-h5-wrap" style={{top: y, left: x, right: this.props.right}} onTouchMove={this.handleTouchMove} onTouchEnd={this.handleTouchEnd}>
        <View className="mask" onClick={this.handlerClick}></View>
        {this.props.children}
      </View>  
    )
  }
}

export default MovableViewH5