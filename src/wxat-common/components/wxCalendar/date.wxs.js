function isSigned(signedDateList, year, mouth, day) {
  day = day + 1;
  mouth = (mouth + '').length > 1 ? mouth + '' : '0' + mouth;
  day = (day + '').length > 1 ? day + '' : '0' + day;
  var now = year + mouth + day;
  var flag = false;
  for (var i = 0; i < signedDateList.length; i++) {
    if (signedDateList[i] == now) {
      flag = true;
    }
  }
  return flag ? 'signed' : '';
}

function getcurTimeStr(cur_year, cur_month, index, thisDate, firstPeriodDay, signedDateList) {
  var curTime =
    cur_year +
    '/' +
    (cur_month > 9 ? cur_month : '0' + cur_month) +
    '/' +
    (index + 1 > 9 ? index + 1 : '0' + (index + 1));
  var neverSigned = true;
  for (var i = 0; i < signedDateList.length; i++) {
    var time = signedDateList[i] + '';
    var year = time.slice(0, 4);
    var mouth = time.slice(4, 6);
    var day = time.slice(6, 8);
    time = year + '/' + mouth + '/' + day;
    if (time < curTime) {
      neverSigned = false;
    }
  }
  return curTime > thisDate || curTime < firstPeriodDay || neverSigned ? 'future' : '';
}

export default {
  isSigned,
  getcurTimeStr,
};
