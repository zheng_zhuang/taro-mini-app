import React, { FC, useState, useEffect, useMemo } from 'react';
import { _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Text, View, Block, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';

import utilDate from './date.wxs.js';

import './index.scss';

const weeks_ch = ['天', '一', '二', '三', '四', '五', '六'];

const formatNumber = (n) => {
  n = n.toString();
  return n[1] ? n : '0' + n;
};

const formatTime = (date) => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  return [year, month, day].map(formatNumber).join('/');
};

type IProps = {
  dataTags: Record<string, any>;
  signedDateList: Array<string>;
  firstPeriodDay: String;
};

let WXCalendar: FC<IProps> = ({ firstPeriodDay, signedDateList, dataTags }) => {
  const [hasEmptyGrid, setHasEmptyGrid] = useState(false);
  const [cur_year, set_cur_year] = useState<number>(0);
  const [cur_month, set_cur_month] = useState<number>(0);
  const [cur_date, set_cur_date] = useState<string>('');
  const [today, setToday] = useState('');
  const [todayIndex, setTodayIndex] = useState(0);
  const thisDate = useMemo(() => formatTime(new Date()), []);
  const [empytGrids, setEmpytGrids] = useState<number[]>([]);
  const [days, setDays] = useState<number[]>([]);

  useEffect(() => {
    setNowDate();
  }, []);

  const dateSelectAction = (cur_day, cur_day_str) => {
    let curMonthStr = cur_month + '';
    let curDayStr = cur_day + 1 + '';
    if (curDayStr.length === 1) {
      curDayStr = '0' + curDayStr;
    }
    if (curMonthStr.length === 1) {
      curMonthStr = '0' + curMonthStr;
    }
    const date = cur_year + curMonthStr + curDayStr;
    const isSigned = !!~signedDateList.findIndex((item) => date == item);
    if (!signedDateList.filter((item) => item < date).length) return false;
    if (cur_day_str <= thisDate && !isSigned && cur_day_str >= firstPeriodDay) {
      let curDate = cur_year + '-' + cur_month + '-' + cur_day;
      const timestamp = new Date(cur_year + '-' + cur_month + '-' + (cur_day + 1));

      setTodayIndex(cur_day);
      set_cur_date(curDate);

      Taro.eventCenter.trigger('signIn-changeSelectedDate', date, timestamp);
    }

    console.log(`点击的日期:${cur_year}年${cur_month}月${cur_day + 1}`);
  };

  function setNowDate() {
    const date = new Date();
    const _cur_year = date.getFullYear();
    const _cur_month = date.getMonth() + 1;
    const _todayIndex = date.getDate() - 1;
    console.log(`日期：${_todayIndex}`);
    // const weeks_ch = ["天", "一", "二", "三", "四", "五", "六"];
    calculateEmptyGrids(_cur_year, _cur_month);
    calculateDays(_cur_year, _cur_month);
    const _cur_date = _cur_year + '-' + _cur_month + '-' + _todayIndex;
    set_cur_year(_cur_year);
    set_cur_month(_cur_month);
    set_cur_date(_cur_date);
    setToday(_cur_date);
    setTodayIndex(_todayIndex);
  }

  function getThisMonthDays(year, month) {
    return new Date(year, month, 0).getDate();
  }
  function getFirstDayOfWeek(year, month) {
    return new Date(Date.UTC(year, month - 1, 1)).getDay();
  }
  function calculateEmptyGrids(year, month) {
    const firstDayOfWeek = getFirstDayOfWeek(year, month);
    let _empytGrids: number[] = [];
    if (firstDayOfWeek > 0) {
      for (let i = 0; i < firstDayOfWeek; i++) {
        _empytGrids.push(i);
      }

      setEmpytGrids(_empytGrids);
      setHasEmptyGrid(true);
    } else {
      setHasEmptyGrid(false);
      setEmpytGrids([]);
    }
  }

  function calculateDays(year, month) {
    let _days: number[] = [];

    const thisMonthDays = getThisMonthDays(year, month);

    for (let i = 1; i <= thisMonthDays; i++) {
      _days.push(i);
    }

    setDays(_days);
  }
  function handleCalendar(e) {
    const handle = e.currentTarget.dataset.handle;

    if (handle === 'prev') {
      let newMonth = cur_month - 1;
      let newYear = cur_year;
      if (newMonth < 1) {
        newYear = cur_year - 1;
        newMonth = 12;
      }

      calculateDays(newYear, newMonth);
      calculateEmptyGrids(newYear, newMonth);

      set_cur_year(newYear);
      set_cur_month(newMonth);
    } else {
      let newMonth = cur_month + 1;
      let newYear = cur_year;
      if (newMonth > 12) {
        newYear = cur_year + 1;
        newMonth = 1;
      }

      calculateDays(newYear, newMonth);
      calculateEmptyGrids(newYear, newMonth);

      set_cur_year(newYear);
      set_cur_month(newMonth);
    }
  }
  function updateCalendar(opt) {
    let { newMonth, newYear } = opt;

    if (!newMonth || !newYear) {
      let today = new Date();
      newMonth = today.getMonth() + 1;
      newYear = today.getFullYear();
    }

    calculateDays(newYear, newMonth);
    calculateEmptyGrids(newYear, newMonth);

    set_cur_year(newYear);
    set_cur_month(newMonth);
  }

  return (
    <View data-scoped='wk-wcw-WxCalendar' className='wk-wcw-WxCalendar canlendarBgView'>
      <View className='canlendarView'>
        <View className='canlendarTopView'>
          <View className='leftBgView' onClick={_fixme_with_dataset_(handleCalendar, { handle: 'prev' })}>
            <View className='leftView'>{'<'}</View>
          </View>
          <View className='centerView'>
            {cur_year || '--'}-{cur_month || '--'}
          </View>
          <View className='rightBgView' onClick={_fixme_with_dataset_(handleCalendar, { handle: 'next' })}>
            <View className='rightView'>></View>
          </View>
        </View>
        <View className='line'></View>
        <View className='weekBgView'>
          {weeks_ch.map((item, index) => (
            <View className='weekView' key={item}>
              {item}
            </View>
          ))}
        </View>
        <View className='dateBgView'>
          {!!hasEmptyGrid && empytGrids.map((empy, idx) => <View className='dateEmptyView' key={idx}></View>)}

          {days.map((d, i) => {
            const timeStr = utilDate.getcurTimeStr(cur_year, cur_month, i, thisDate, firstPeriodDay, signedDateList);

            const sunday = (i + empytGrids.length) % 7 == 0 ? 'sunday' : '';
            const dateSelectView = cur_year + '-' + cur_month + '-' + i == cur_date ? 'dateSelectView' : '';
            const _today = cur_year + '-' + cur_month + '-' + i == today ? 'today' : '';
            const isSigned = utilDate.isSigned(signedDateList, cur_year, cur_month, i);
            return (
              <View
                className='dateView'
                key={i}
                onClick={() =>
                  dateSelectAction(
                    i,
                    cur_year +
                      '/' +
                      (cur_month > 9 ? cur_month : '0' + cur_month) +
                      '/' +
                      (i + 1 > 9 ? i + 1 : '0' + (i + 1))
                  )
                }
              >
                <View className={`datesView ${timeStr} ${sunday} ${dateSelectView} ${_today} ${isSigned}`}>
                  <Text>{d}</Text>
                  {!!dataTags[cur_year + '-' + cur_month + '-' + (i + 1)] && (
                    <Block>
                      <Image className='tag' />
                    </Block>
                  )}
                </View>
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
};

WXCalendar.defaultProps = {
  firstPeriodDay: '',
  signedDateList: [],
  dataTags: {},
};

export default WXCalendar;
