import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import React, { ReactElement } from 'react';
import './index.scss';

type PopoverProps = {
  position: string;
  visible: boolean;
  renderOutlet: ReactElement;
  renderContent: ReactElement;
};

type IProps = PopoverProps;

interface Popover {
  props: IProps;
}

class Popover extends React.Component {
  static defaultProps = {
    position: 'top',
    visible: false,
  };

  static options = {
    multipleSlots: true,
  };

  render() {
    const { position, visible } = this.props;

    return (
      <View data-scoped='wk-wcp-Popover' className={'wk-wcp-Popover ' + ('popover-wrap ' + position)}>
        <View className='outlet'>{this.props.renderOutlet}</View>
        {/*  弹出部分  */}
        <View className={'content ' + (visible ? '' : 'hide')}>{this.props.renderContent}</View>
      </View>
    );
  }
}

export default Popover;
