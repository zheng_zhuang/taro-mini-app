import React, { FC, useState, useImperativeHandle, useRef, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';

import authHelper from '@/wxat-common/utils/auth-helper.js';
import api from '@/wxat-common/api/index.js';
import wxApi from '@/wxat-common/utils/wxApi';
import canvasHelper from '@/wxat-common/utils/canvas-helper.js';
import shareUtil from '@/wxat-common/utils/share.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';


// import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import AuthUser from '@/wxat-common/components/auth-user';
import constants from '@/wxat-common/constants';

import './index.scss';

const { salesTypeEnum } = constants;

const POSTER_TYPE = {
  goods: 'goods',
  redPacket: 'red-packet',
};

const POSTER_HEADER_TYPE = {
  avatar: 1,
  logo: 2,
};

const POSTER_BG = [
  cdnResConfig.goods.posterBgOne,
  cdnResConfig.goods.posterBgTwo,
  cdnResConfig.goods.posterBgThree,
  cdnResConfig.goods.posterBgFour,
];

// props的类型定义
interface IProps {
  posterData?: Record<string, any> | null;
  visible?: boolean;
  /**
   * 海报的二维码请求参数：
   * verificationNo: itemNo,
   * verificationType: 5,
   * maPath: 'wxat-common/pages/goods-detail/index'
   */
  qrCodeParams: Record<string, any> | null;
  /**
   * 是否为新的二维码接口，新的二维码接口请求结构有变
   * 接口变更为/auth/vip/verification/getNewQRCode
   * 直接传参sence和maPath，获取时也是直接获取
   */
  isNewQrCode?: boolean;
  posterType?: string;
  /**
   * 海报上面的提示语
   */
  posterTips: string;
  /**
   * 海报主图
   */
  posterImage?: string;
  /**
   * 海报图下面的名字
   */
  posterLogo?: string;
  posterHeaderType?: number;
  posterName?: string;
  /**
   * 海报优惠价格，已经转化过成元的价格
   */
  salePrice?: string;
  /**
   * 海报原价，已经转化过成元的价格
   */
  labelPrice?: string;
  /**
   * 是否需要展示海报原价
   */
  showLabelPrice?: boolean;
  /**
   * 海报优惠价标签
   */
  posterSalePriceLabel?: string;
  /**
   * 海报优惠价标签
   */
  posterLabelPriceLabel?: string;
  /**
   * 头像旁推荐语
   */
  recommendTips?: string;
  /**
   * 海报背景图
   */
  backgroundImage?: string;
  onSave: () => any;
  childRef?: any;
  /**
   * 二维码生成方式，使用映射ID的方式创建
   */
  uniqueShareInfoForQrCode?: object | null;
  // 正常商品 0; 到店咨询 1; 无库存售卖 2;
  salesType: number;
  /**
   * 线下商品价格展示
   */
  displayPrice: string | null;
}

const ShareDialog: FC<IProps> = ({
  isNewQrCode,
  qrCodeParams,
  posterType,
  posterTips,
  posterImage,
  posterName,
  posterHeaderType,
  posterLogo,
  salePrice,
  labelPrice,
  showLabelPrice,
  posterSalePriceLabel,
  recommendTips,
  backgroundImage,
  onSave,
  childRef,
  uniqueShareInfoForQrCode,
  salesType,
  displayPrice,
}) => {
  const [posterDialogVisible, setPosterDialogVisible] = useState(false);
  const [qrCodeUrl, setQrCodeUrl] = useState('');
  // const posterSaveImageDataRef = useRef();
  const [user, setUser] = useState({});
  const wxUserInfo = useSelector((state) => state.base.wxUserInfo);

  const shareDialogRef = useRef<any>(null);

  const shareIcon = 'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tool/share.png';
  const saveIcon = 'https://front-end-1259575047.file.myqcloud.com/miniprogram/staff/images/tool/save.png';
  const posterMask = cdnResConfig.goods.posterMask;
  const [posterBgImg, setPosterBgImg] = useState('');
  // const [posterSaveImageData, setPosterSaveImageData] = useState(null);
  const [showModalStatus, setShowModalStatus] = useState(false);

  // 需要暴露给外面使用的函数
  useImperativeHandle(childRef, () => ({
    hide,
    show,
    savePosterImage,
  }));

  useEffect(() => {
    if (backgroundImage) {
      setPosterBgImg(backgroundImage);
    } else {
      setPosterBgImg(POSTER_BG[Math.floor(Math.random() * 4)]);
    }
  }, []);

  // 创建海报
  const handleShowPosterDialog = () => {
    if (!authHelper.checkAuth()) {
      return;
    }
    // getShareDialog().hide();

    setPosterDialogVisible(false);
    setQrCodeUrl('');

    if (uniqueShareInfoForQrCode) apiQRCodeByUniqueKey();
    else if (isNewQrCode) apiQRCodeByScene();
    else apiQRCodeByParameter();
  };

  const apiQRCodeByUniqueKey = () => {
    const publicArguments = shareUtil.buildSharePublicArguments({
      bz: '',
      forServer: true,
    });

    const scene = Object.assign({}, publicArguments, uniqueShareInfoForQrCode);
    wxApi
      .request({
        url: api.generatorMapperQrCode,
        loading: true,
        data: {
          shareParam: JSON.stringify(scene),
          urlPath: scene.page,
        },
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data);
      });
  };

  const apiQRCodeByParameter = () => {
    wxApi
      .request({
        url: api.verification.getQRCode,
        loading: true,
        data: qrCodeParams,
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data.qrCode);
      });
  };

  const apiQRCodeByScene = () => {
    wxApi
      .request({
        url: api.verification.getQRCodeV2,
        loading: true,
        data: {
          maPath: qrCodeParams.maPath,
          scene: qrCodeParams.verificationNo,
        },
      })
      .then((res) => {
        setPosterDialogVisible(true);
        setQrCodeUrl(res.data.qrCode);
      });
  };

  /**
   * 保存海报图片到本地
   */
  const handleSaveImage = (e) => {
    onSave && onSave();
  };
  /**
   * 直接关闭分享弹窗
   */
  const handleCloseShare = () => {
    hide();
  };
  const hide = () => {
    // getShareDialog() && getShareDialog().hide(true);
    onClickMaskHide();
  };
  const show = () => {
    // getShareDialog() && getShareDialog().show({
    //   scale: 1,
    // });
    setShowModalStatus(true);
    // 创建海报
    handleShowPosterDialog();
  };

  const savePosterImage = (context) => {
    if (posterType === POSTER_TYPE.goods) {
      saveGoodsPosterImage(context);
    }
  };

  const saveGoodsPosterImage = (context) => {
    wxApi.showLoading({
      title: '正在保存图片...',
    });

    // const { user } = posterSaveImageDataRef.current;
    const canvasContext = wxApi.createCanvasContext('shareCanvas', context);
    const imageRequests = [
      wxApi.getImageInfo({
        src: posterBgImg,
      }),

      wxApi.getImageInfo({
        src: user.headUrl,
      }),

      wxApi.getImageInfo({
        src: (posterImage || '').replace(/^https|http/, 'https'),
      }),
    ];

    if (qrCodeUrl) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: qrCodeUrl,
        })
      );
    }

    if (posterHeaderType === POSTER_HEADER_TYPE.logo && posterLogo) {
      imageRequests.push(
        wxApi.getImageInfo({
          src: posterLogo,
        })
      );
    }

    Promise.all(imageRequests)
      .then((res) => {
        // 背景
        canvasHelper.drawImage(canvasContext, 0, 0, 292.5, 427, res[0].path);
        if (posterHeaderType === POSTER_HEADER_TYPE.avatar) {
          // 头像文案背景
          canvasHelper.roundRect(canvasContext, 18, 18, 220.5, 40, 18, 'rgba(255, 255, 255, .6)');

          // 三角形
          canvasHelper.drawFillLine(
            canvasContext,
            [
              {
                x: 125,
                y: 58,
              },

              {
                x: 130,
                y: 64,
              },

              {
                x: 135,
                y: 58,
              },
            ],

            'rgba(255, 255, 255, .6)'
          );

          // 用户名+推荐文案 (设计需求昵称大于12省略)
          canvasContext.setFontSize(10);
          let userNameWidth = canvasContext.measureText(`${user.name}`).width || 0;
          if (userNameWidth > 130) {
            userNameWidth = 110;
            canvasHelper.drawText(canvasContext, `${user.name}`, 63.5, 32, {
              textAlign: 'start',
              fillStyle: '#999999',
              fontSize: 10,
              maxWidth: 130,
              rowLength: 1,
            });

            canvasHelper.drawText(canvasContext, `${recommendTips}`, userNameWidth + 63.5, 32, {
              textAlign: 'start',
              fillStyle: '#999999',
              fontSize: 10,
            });
          } else {
            canvasHelper.drawText(canvasContext, `${user.name}${recommendTips}`, 63.5, 32, {
              textAlign: 'start',
              fillStyle: '#999999',
              fontSize: 10,
            });
          }

          // 海报头部标题
          canvasHelper.drawText(canvasContext, posterTips, 63.5, 50, {
            textAlign: 'start',
            fillStyle: '#000000',
            fontSize: 11,
          });
        } else if (posterHeaderType === POSTER_HEADER_TYPE.logo) {
          // 有二维码：顶部logo放在第五个图片，无二维码，顶部logo放在第四个
          if (qrCodeUrl && res[4] && res[4].path) {
            canvasHelper.drawImage(canvasContext, 18, 18, 215, 45, res[4].path);
          } else if (!qrCodeUrl && res[3] && res[3].path) {
            canvasHelper.drawImage(canvasContext, 18, 18, 220.5, 45, res[3].path);
          } else {
            canvasHelper.drawImage(canvasContext, 18, 18, 220.5, 45, res[3].path);
          }
        }

        // 白色矩形背景
        canvasHelper.roundRect(canvasContext, 18, 67.5, 220.5, 330, 18, '#fff');
        // 头像
        if (posterHeaderType === POSTER_HEADER_TYPE.avatar)
          canvasHelper.drawCircleImage(canvasContext, 38, 38, 20, res[1].path);
        // 商品图片
        canvasHelper.drawImage(canvasContext, 32, 81, 194, 198, res[2].path);

        if (salesType !== salesTypeEnum.OFFLINE) {
          // 优惠价粉色方框
          canvasHelper.roundRect(canvasContext, 33.5, 290, 40.5, 18, 2, '#ffd9da');
          // 优惠价文案
          canvasHelper.drawText(canvasContext, posterSalePriceLabel, 37, 303, {
            textAlign: 'start',
            fillStyle: '#c33f3e',
            fontSize: 11,
          });
        }
        // 售价
        canvasHelper.drawText(
          canvasContext,
          salesType !== salesTypeEnum.OFFLINE ? `￥${salePrice}` : displayPrice || '到店咨询',
          salesType !== salesTypeEnum.OFFLINE ? 82 : 33.5,
          305,
          {
            textAlign: 'start',
            fillStyle: '#000000',
            fontSize: 14,
          }
        );

        // 标价(标价超过8位不展示)
        if (
          salesType !== salesTypeEnum.OFFLINE &&
          showLabelPrice &&
          ((labelPrice || '') + '').length < 9 &&
          ((salePrice || '') + '').length < 9
        ) {
          const salePriceWidth = canvasContext.measureText(`￥${salePrice}`).width || 0;
          const labelPriceWidth = canvasContext.measureText(`${labelPrice}`).width || 0;
          canvasHelper.drawText(canvasContext, `￥${labelPrice}`, 86 + salePriceWidth, 305, {
            textAlign: 'start',
            fillStyle: '#999999',
            fontSize: 11,
          });

          // 划线
          canvasHelper.drawStrokeLine(
            canvasContext,
            [
              {
                x: 86 + salePriceWidth,
                y: 301.5,
              },

              {
                x: 86 + salePriceWidth + labelPriceWidth,
                y: 301.5,
              },
            ],

            '#999999',
            0.5
          );
        }
        // 商品名
        canvasHelper.drawText(canvasContext, posterName, 37, 335.5, {
          textAlign: 'start',
          fillStyle: '#000000',
          fontSize: 12,
          maxWidth: 120,
        });

        canvasHelper.drawText(canvasContext, '长按识别小程序码', 38, 380, {
          textAlign: 'start',
          fillStyle: '#999999',
          fontSize: 10,
        });

        // 二维码
        if (qrCodeUrl && res[3]) {
          canvasHelper.drawImage(canvasContext, 176, 320, 54, 54, res[3].path);
        }

        canvasContext.draw(false, () => {
          wxApi
            .canvasToTempFilePath(
              {
                canvasId: 'shareCanvas',
              },

              context
            )
            .then((res) => {
              return wxApi.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
              });
            })
            .then(() => {
              wxApi.showToast({
                title: '已保存到相册',
              });
            })
            .catch((error) => {
              console.log('saveImage error: ', error);
              wxApi.showToast({
                title: '保存失败',
                icon: 'none',
              });
            })
            .finally(() => {
              wxApi.hideLoading();
            });
        });
      })
      .catch(() => {
        wxApi.hideLoading();
      });
  };

  // 获取分享对话弹框
  const getShareDialog = () => {
    return shareDialogRef.current;
  };

  const userInfoReady = () => {
    setUser({
      name: wxUserInfo ? wxUserInfo.nickName : '',
      headUrl: wxUserInfo ? wxUserInfo.avatarUrl : '',
    });
  };

  const onClickMaskHide = () => {
    setShowModalStatus(false);
    setPosterDialogVisible(false);
    setQrCodeUrl('');
    // setPosterSaveImageData(null);
  };

  const preventTouchMove = (e) => {
    e.preventDefault();
    e.stopPropagation();

  };

  const preventTap = (e) => {
    e.stopPropagation();
  };

  const labelPriceStr = labelPrice + '';
  const salePriceStr = salePrice + '';

  return (
    <Block>
      {/*  <animat-dialog id="share-dialog" anim-class="share-dialog">  */}
      {showModalStatus && (
        <View className='share-dialog wk-gcg-share-dialog'>
          {showModalStatus && (
            <View className='commodity_screen' onTouchMove={preventTouchMove} onClick={onClickMaskHide}></View>
          )}

          <AuthUser onReady={userInfoReady}>
            {posterType === POSTER_TYPE.goods && posterDialogVisible && (
              <View className='post-container' onClick={hide} onTouchMove={preventTouchMove}>
                <View
                  className='posters-box'
                  onClick={preventTap}
                  style={_safe_style_(
                    'background: url(' + posterBgImg + ') no-repeat; background-size: 100% 100%;'
                  )}
                >
                  {posterHeaderType === POSTER_HEADER_TYPE.avatar && (
                    <View className='posters-nick-header'>
                      <Image className='posters-head' src={user.headUrl}></Image>
                      <View className='posters-right'>
                        <View className='name limit-line'>
                          <View className='nick-name limit-line'>{user.name}</View>
                          <View className='recommend-tips limit-line'>{recommendTips}</View>
                        </View>
                        <View className='posters-notes limit-line'>{posterTips}</View>
                      </View>
                    </View>
                  )}

                  {posterHeaderType === POSTER_HEADER_TYPE.logo && (
                    <Image className='poster-custom-header' mode='scaleToFill' src={posterLogo || ''}></Image>
                  )}

                  <View className='content-box'>
                    <Image className='posters-img' src={posterImage || ''}></Image>
                    {/* 活动类型、活动价格、原价 */}
                    <View className='posters-detail'>
                      {salesType !== salesTypeEnum.OFFLINE ? (
                        <View className='activity-name'>{posterSalePriceLabel}</View>
                      ) : null}
                      <View className='goods-sprice'>
                        {salesType !== salesTypeEnum.OFFLINE ? '￥' + salePrice : displayPrice || '到店咨询'}
                      </View>
                      {showLabelPrice &&
                        labelPriceStr &&
                        labelPriceStr.length < 9 &&
                        salePriceStr.length < 9 &&
                        salesType !== salesTypeEnum.OFFLINE && (
                          <View className='goods-orgprice'>{'￥' + labelPrice}</View>
                        )}
                    </View>
                    {/* 商品名称 */}
                    <View className='goods-name limit-line line-2'>{posterName}</View>
                    <View className='tap-tips'>长按识别小程序码</View>
                    <Image className='qr-code' src={qrCodeUrl} mode='aspectFit'></Image>
                  </View>
                </View>
              </View>
            )}
          </AuthUser>
          <View
            className='bottom-container'
            onClick={preventTap}
            onTouchMove={preventTouchMove}
            style={_safe_style_(
              'background: url(' + posterMask + ') no-repeat; background-size: 100% 100%;'
            )}
          >
            <View className='share-item'>
              <Button openType='share' className='share-btn'>
                <Image className='share-img' src={shareIcon}></Image>
                <View>分享好友</View>
              </Button>
            </View>

            <View className='share-item'>
              <Button className='share-btn' onClick={handleSaveImage}>
                <Image className='share-img' src={saveIcon}></Image>
                <View>保存图片</View>
              </Button>
            </View>
          </View>
        </View>
      )}

      {/*  </animat-dialog>  */}
    </Block>
  );
};

ShareDialog.defaultProps = {
  visible: false,
  posterData: null,
  qrCodeParams: null,
  isNewQrCode: false,
  posterImage: '',
  posterName: '',
  uniqueShareInfoForQrCode: null,
  posterType: POSTER_TYPE.goods,
  posterTips: '向您推荐了这个商品',
  posterLogo: '',
  posterHeaderType: POSTER_HEADER_TYPE.avatar,
  salePrice: '',
  labelPrice: '',
  showLabelPrice: true,
  posterSalePriceLabel: '优惠价',
  posterLabelPriceLabel: '原价',
  recommendTips: '向你推荐',
  backgroundImage: '',
  salesType: 0,
  displayPrice: '',
};

export default ShareDialog;
