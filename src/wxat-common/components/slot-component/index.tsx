import '@/wxat-common/utils/platform';
import {$getRouter, _safe_style_} from '@/wk-taro-platform';
import React, {ComponentClass, CSSProperties, Component} from 'react';
import Taro from '@tarojs/taro';
import {Block} from '@tarojs/components';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import wxApi from '@/wxat-common/utils/wxApi';
import HoverCart from '@/wxat-common/components/cart/hover-cart';

import Custom from '@/wxat-common/components/custom/index';
import CutPriceList from '@/wxat-common/components/tabbarLink/cut-price-list/index';
import GoodsList from '@/wxat-common/components/tabbarLink/goods-list';
import CouponCenter from '@/wxat-common/components/tabbarLink/coupon-center/index';
import GroupList from '@/wxat-common/components/tabbarLink/group-list/index';
import IntegralList from '@/wxat-common/components/tabbarLink/integral-list/index';
import OrderList from '@/wxat-common/components/tabbarLink/order-list/index';
import TabbarOrder from '@/wxat-common/components/tabbarLink/order/index';
import ShoppingCart from '@/wxat-common/components/tabbarLink/shopping-cart/index';
import InternalPurchase from '@/wxat-common/components/tabbarLink/internal-purchase/index';

import ClassifyPage from '@/wxat-common/components/classify-page';
import QuickBuyPage from '@/wxat-common/components/quick-buy-page';
import NearbyStore from '@/wxat-common/components/nearby-store-page';


import HomeActivityDialog from '@/wxat-common/components/home-activity-dialog/index'

interface PageOwnProps {
  componentId: string;
  customKey?: string;
  style?: CSSProperties;
  className?: string;
  pageRef?: Taro.RefObject<any>;
  isTabbar?: boolean;
  scanId?: string;
  customCodeData?: any
}

interface PageState {
  updateRoomCode: Boolean,
  codeId: String
}

type IProps = PageOwnProps;

interface SlotComponent {
  props: IProps;
}

class SlotComponent extends Component {
  $router = $getRouter()
  static defaultProps = {
    componentId: '',
    isTabbar: false,
    customCodeData: null
  };

  render() {
    const {componentId, customKey, className, style, pageRef, isTabbar, scanId, customCodeData} = this.props;
    const other = {className, style, isTabbar};
    return (
      <Block>
        {componentId === 'custom' && <Block>
          <Custom keyPath={customKey} ref={pageRef} {...other} scanId={scanId} customCodeData={customCodeData}></Custom>
          <HomeActivityDialog showPage={customKey}></HomeActivityDialog>
          <HoverCart/>
        </Block>
        }
        {componentId === 'classify' && <ClassifyPage ref={pageRef} {...other}></ClassifyPage>}
        {componentId === 'quick-buy' && <QuickBuyPage ref={pageRef} {...other}></QuickBuyPage>}
        {componentId === 'tabbar-cut-price-list' && <CutPriceList ref={pageRef} {...other}></CutPriceList>}
        {componentId === 'goods-list' && (
          <Block>
            <GoodsList ref={pageRef} type={goodsTypeEnum.PRODUCT.value} {...other}></GoodsList>
            <HoverCart/>
          </Block>
        )}

        {componentId === 'tabbar-coupon-center' && <CouponCenter ref={pageRef} {...other} />}
        {componentId === 'tabbar-group-list' && (
          <Block>
            <GroupList ref={pageRef} {...other}></GroupList>
            <HoverCart/>
          </Block>
        )}

        {componentId === 'tabbar-integral' && <IntegralList ref={pageRef} {...other}></IntegralList>}
        {componentId === 'tabbar-internal-purchase' && <InternalPurchase ref={pageRef} {...other}></InternalPurchase>}
        {componentId === 'tabbar-order-list' && <OrderList ref={pageRef} {...other}></OrderList>}
        {componentId === 'tabbar-order' && <OrderList ref={pageRef} {...other}></OrderList>}
        {componentId === 'tabbar-shopping-cart' && <ShoppingCart ref={pageRef} isTabbar {...other}></ShoppingCart>}
        {componentId === 'nearby-store' && <NearbyStore ref={pageRef} {...other}></NearbyStore>}


      </Block>
    );
  }
}

export default SlotComponent as ComponentClass<PageOwnProps, PageState>;
