import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import template from '../../utils/template.js';

import './index.scss';

class Dialog extends React.Component {
  static defaultProps = {
    // dialog距离顶部的距离，支持rpx和百分比，属性值通css top
    top: '35%',
    // 是否展示标题
    showTitle: true,
    // 是否展示底部按钮
    showFooter: true,
    // 弹窗标题
    title: '提示',
    message: '',
    // 弹窗内容
    content: '弹窗内容',
    // 弹窗取消按钮文字
    cancelText: '取消',
    // 弹窗确认按钮文字
    confirmText: '确定',
    visible: false,
    //title距离顶部的高度
    titleTop: '35px',
    //背景透明度
    backOpacity: '0',
    contentBackgroundColor: '#f8ffff',
    contentBoxShadow: '0px 2px 12px 0px rgba(0,0,0,0.13)',
    borderRadius: '4px',
    width: '100%',
    maxWidth: '295px',
    //内容的外边距
    contenMargin: '20px 0 57px 0',
    contentPadding: '0 15px',
    //确认回调函数
    onConfirm: null,
    //取消回调函数
    onCancel: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      // 模板样式
      tmpStyle: {},
    };
  } /*请尽快迁移为 componentDidMount 或 constructor*/
  UNSAFE_componentWillMount() {
    this.getTemplateStyle();
  }

  preventTouchMove = () => {}; //阻止滑动

  /**
   * 点击确定
   */
  _confirmEvent = () => {
    const onConfirm = this.props.onConfirm;
    onConfirm && onConfirm();
  };

  /**
   * 隐藏dialog
   */
  _cancelEvent = () => {
    const onCancel = this.props.onCancel;
    onCancel && onCancel();
  };

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const {
      backOpacity,
      top,
      borderRadius,
      width,
      maxWidth,
      contentBackgroundColor,
      contentBoxShadow,
      titleTop,
      title,
      showTitle,
      contenMargin,
      contentPadding,
      message,
      cancelText,
      confirmText,
      showFooter,
      visible,
    } = this.props;
    const { tmpStyle } = this.state;
    const dialogStyle = {
      top,
      width,
      maxWidth,
      borderRadius: borderRadius,
      backgroundColor: contentBackgroundColor,
      boxShadow: contentBoxShadow,
    };

    return (
      !!visible && (
        <View
          data-scoped='wk-wcd-Dialog'
          className='wk-wcd-Dialog wx_dialog_container'
          onTouchMove={this.preventTouchMove}
        >
          <View className='wx-mask' style={{ opacity: backOpacity }} onClick={this._cancelEvent}></View>
          <View className='wx-dialog' style={_safe_style_(dialogStyle)}>
            {!!showTitle && (
              <View className='wx-dialog-title' style={{ marginTop: titleTop }}>
                {title}
              </View>
            )}

            <View className='wx-dialog-content' style={{ margin: contenMargin, padding: contentPadding }}>
              {message}
              {this.props.children}
            </View>
            {!!showFooter && (
              <View className='wx-dialog-footer'>
                {!!cancelText && (
                  <View className='wx-dialog-btn btn-cancel' onClick={this._cancelEvent}>
                    {cancelText}
                  </View>
                )}

                <View
                  className='wx-dialog-btn btn-ok'
                  onClick={this._confirmEvent}
                  style={{ background: tmpStyle.btnColor }}
                >
                  {confirmText}
                </View>
              </View>
            )}
          </View>
        </View>
      )
    );
  }
}

export default Dialog;
