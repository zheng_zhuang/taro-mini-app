import React, { FC } from 'react';
import '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import classNames from 'classnames';
import Taro from '@tarojs/taro';
import './index.scss';
import filters from '@/wxat-common/utils/money.wxs.js';
import wxApi from '@/wxat-common/utils/wxApi';

export type EquityCard = {
  itemNo?: string;
  styleUrl: string;
  name: string;
  salePrice: number;
  cardSalesVolume: number;
  virtualSalesAmount: number;
  type: number;
};

export type EquityCardItemProps = {
  item: EquityCard | null;
};

let EquityCardItem: FC<EquityCardItemProps> = (props) => {
  const { item } = props;

  if (!item) return null;

  const [thumbnail] = (item.styleUrl || '').split(',');

  const navigateToDetail = () => {
    wxApi.navigateTo({ url: `/sub-packages/marketing-package/pages/equity-card/detail/index?itemNo=${item.itemNo}` });
  };

  return (
    <View data-scoped='wk-wcd-EquityCardItem' className='wk-wcd-EquityCardItem' onClick={navigateToDetail}>
      <Image className='thumbnail' src={thumbnail} mode='aspectFill' />
      <View className='info'>
        <Text className={classNames(['name', 'limit-line', 'line-2'])}>{item.name}</Text>
        <View className='sale-info'>
          <Text className='price'>￥ {filters.moneyFilter(item.salePrice, true)}</Text>
          <Text className='sales-volume'>已售{(item.cardSalesVolume || 0) + (item.virtualSalesAmount || 0)}</Text>
          <Button className='btn'>立即购买</Button>
        </View>
      </View>
    </View>
  );
};

EquityCardItem.defaultProps = {
  item: null,
};

export default EquityCardItem;
