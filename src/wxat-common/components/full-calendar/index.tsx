import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { ScrollView, View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import dateUtil from '../../utils/date.js';
import template from '../../utils/template.js';
import './index.scss';

class fullCalendar extends React.Component {

  state = {
    tmpStyle: {}, // 主题模板配置
    calendar: null,
    // 构建顶部日期时使用
    date: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
    inTime: '',
    outTime: '',
    showinTime: '',
    showoutTime: '',
    nowTime: '',
    DaysBetween: '',
    initFilterBoxFixedTop: 1212
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      let inTime = nextProps.inTime;
      let outTime = nextProps.outTime;
      this.setState({
        inTime,
        outTime,
      });

      this.init_date(nextProps);
    }
  }

  componentDidMount() {
    this.init_today()
    this.getTemplateStyle()
  }

  //关闭事件
  closeDialog = () => {
    this.props.onMyevent(false)
  }

  sureForm = () => {
    this.props.sureFullCalendar(
      {
        ishasValue: true,
        showcalendar: false,
        inTime: this.state.inTime,
        outTime: this.state.outTime,
        DaysBetween: this.state.DaysBetween,
        initFilterBoxFixedTop: 0
      }
    )
  }

  //获取主题
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 日历初始化
  dataInit = (setYear, setMonth) => {
    // 当前时间/传入的时间
    const now = setYear ? new Date(setYear, setMonth) : new Date();
    const year = setYear || now.getFullYear();
    // 传入的月份已经加1
    const month = setMonth || now.getMonth() + 1;
    let prevMonthDay = new Date((setMonth - 1 < 0?setYear-1 : setYear), setMonth, 0).getDate();

    // 构建某日数据时使用
    let obj = {};
    // 需要遍历的日历数组数据
    let dateArr = [];
    // 需要的格子数，为当前星期数+当月天数
    let arrLen = 0;
    // 该月加1的数值，如果大于11，则为下年，月份重置为1
    // 目标月1号对应的星期
    let startWeek = new Date(year + '-' + (month < 10 ? '0' + month : month) + '-01').getDay();
    //获取目标月有多少天
    let dayNums = new Date(year, month < 10 ? '0' + month : month, 0).getDate();
    let num = 0;
    // 计算当月需要的格子数 = 当前星期数+当月天数
    arrLen = startWeek * 1 + dayNums * 1;
    for (let i = 0; i < arrLen; i++) {
      if (i >= startWeek) {
        num = i - startWeek + 1;
        obj = {
          /*
            * 返回值说明
            * isToday ： 2018-12-27
            * dateNum :  27
            */
          isToday: year + '-' + (month < 10 ? '0' + month : month) + '-' + (num < 10 ? '0' + num : num),
          dateNum: num,
        };
      } else {
        // 填补空缺
        // 例如2018-12月第一天是星期6，则需要补6个空格
        obj = {
          dateNum: prevMonthDay - startWeek,
          pervMonth: true
        };
        prevMonthDay++
      }
      dateArr[i] = obj;
    }
    return dateArr;
  }

  // 点击了日期，选择入住时间或者离店时间
  dayClick = (e) => {
    const that = this;
    const eTime = e.currentTarget.dataset.day;
    let inTime = that.state.inTime;
    let outTime = that.state.outTime;

    if (inTime != '' && outTime != '') {
      inTime = eTime;
      outTime = '';
    } else if (inTime == '' && outTime == '') {
      inTime = eTime
    } else if (inTime != '' && outTime == '') {
      outTime = eTime
    }
    if (new Date(eTime) < new Date(inTime)) {
      const start = eTime
      const end = inTime
      inTime = start
      outTime = end
    }
    // console.log('--inTime',inTime)
    // console.log('--outTime',outTime)
    // if (inTime == '' || new Date(eTime) <= new Date(inTime) || outTime != '') {
    //   inTime = eTime;
    //   outTime = '';
    // } else{
    //   outTime = eTime;
    // }
    let showinTime = that.MonthDayformat(inTime);
    let showoutTime = that.MonthDayformat(outTime);
    that.setState({
      inTime,
      outTime,
      showinTime,
      showoutTime,
    });

    if (that.getDaysBetween(inTime, outTime)) {
      let DaysBetween = that.getDaysBetween(inTime, outTime);
      that.setState({
        DaysBetween,
      });
    }
  }

  getDaysBetween = (dateString1, dateString2) => {
    let startDate = Date.parse(dateString1);
    const endDate = Date.parse(dateString2);
    if (startDate > endDate) {
      return 0;
    }
    if (startDate == endDate) {
      return 1;
    }
    const days = (endDate - startDate) / (1 * 24 * 60 * 60 * 1000);
    return days;
  }

  //格式化日期，只显示月份和日
  MonthDayformat = (date) => {
    let Dtime = new Date(date);
    var Dtimemonth = Dtime.getMonth() + 1;

    return Dtime.getFullYear() + '-' + `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}-${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }`;
  }

  init_date = (newVal) => {
    let showinTime = this.MonthDayformat(newVal.inTime);
    let showoutTime = this.MonthDayformat(newVal.outTime);
    let DaysBetween = this.getDaysBetween(newVal.inTime, newVal.outTime);
    this.setState({
      showinTime,
      showoutTime,
      DaysBetween,
    });
  }

  init_today = () => {
    const getDay = dateUtil.getNowDay();
    this.getTemplateStyle(); //获取主题信息
    // 获取本月时间
    // const nowTime = new Date();
    let year = 2022;
    let month = 7;
    let time = [];
    let timeArray = [];
    // 循环6个月的数据
    for (let i = 0; i < 20; i++) {
      year = month + 1 > 12 ? year + 1 : year;
      month = month + 1 > 12 ? 1 : month + 1;
      // 每个月的数据
      time = this.dataInit(year, month);
      // 接收数据
      // timeArray[year + '年' + (month < 10 ? '0' + month : month) + '月'] = time;
      timeArray.push(
        {
          id: year + '-' + month,
          title: year + '年' + (month < 10 ? '0' + month : month) + '月',
          date: time
        }
      )
    }

    this.setState({
      calendar: timeArray,
      nowTime: getDay,
    });
  }

  render() {
    const {
      date,
      calendar,
      inTime,
      outTime,
      tmpStyle,
      nowTime,
      showinTime,
      showoutTime,
    } = this.state;
    const { showcalendar, initFilterBoxFixedTop } = this.props
    return (
      showcalendar && (
        <View data-scoped="wk-swcc-FullCalendar" className="wk-swcc-FullCalendar page">
          <View className="tcBox">
            <View className="tcBox-hui" onClick={this.closeDialog}></View>
            <View className="dateBox">
              <View className="full-calendar-header">
                <View className="headtitle-box">
                  <View className="header-title">选择日期</View>
                  <Image
                    onClick={this.closeDialog}
                    className="headClosebtn"
                    src="https://htrip-static.ctlife.tv/distribution/calendar-close.png"
                    mode="aspectFill"
                  ></Image>
                </View>
                <View className="weekBox">
                  {date &&
                    date.map((item, index) => {
                      return (
                        <View className={'weekItem ' + (item == '周六' || item == '周日' ? 'weekMark' : '')} key={'week-' + index}>
                          {item}
                        </View>
                      );
                    })}
                </View>
              </View>
              {/*  日期  */}
              <ScrollView scrollY scrollTop={initFilterBoxFixedTop} className="calendar-date-content">
                {!!calendar &&
                  calendar.map((calendarItem, idx) => {
                    return (
                      <View className="date-box" id={'a' + calendarItem.id} key={'date-content-' + idx}>
                        <View className="calendar-data-box-title">{calendarItem.title}</View>
                        <View className="data-content">
                          {
                            calendarItem.date.map((item, index) => {
                              return (
                                <View
                                  key={'date-' + index}
                                  className={
                                    'days ' +
                                    (item.pervMonth ? 'not-select' : ((item.isToday > inTime && item.isToday < outTime) || item.isToday == inTime || item.isToday == outTime) ? 'day-select' : '')
                                  }
                                  onClick={_fixme_with_dataset_(this.dayClick, { id: item.dateNum, day: item.isToday })}
                                >
                                  {item.dateNum && (
                                    <View
                                      className={'txt ' + ((!item.pervMonth) && item.isToday == inTime ? 'start' : '' || item.isToday == outTime ? 'end' : '')}
                                    >
                                      {item.dateNum}
                                    </View>
                                  )}
                                </View>
                              );
                            })}
                        </View>
                      </View>
                    );
                  })}
              </ScrollView>
              {outTime && (
                <View className="reserve-time">
                  <View className="reserve-box">
                    <View className="reserve-item">
                      <View className='itemvalabel'>开始</View>
                      <View className="itemvalue">{showinTime}</View>
                    </View>
                    <View className="reserve-line"></View>
                    <View className="reserve-item reserve-item-tr">
                      <View className='itemvalabel'>结束</View>
                      <View className="itemvalue">{showoutTime}</View>
                    </View>
                  </View>
                  <View className="surebtnBox">
                    <View
                      onClick={this.sureForm}
                      className="sureBtn"
                    >
                      确定
                    </View>
                  </View>
                </View>
              )}
            </View>
          </View>
        </View>
      )
    );
  }
}

export default fullCalendar;
