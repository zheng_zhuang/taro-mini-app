import React, { Component } from 'react';
import wxApi from '@/wxat-common/utils/wxApi'; // @hostExternalClassesConvered(AnimatDialog)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import screen from '@/wxat-common/utils/screen';
import './index.scss';

const mapStateToProps = (state) => ({});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class AnimatDialog extends Component {
  //todo: remove, dont modify
  // 外部样式类
  //modal-class 遮罩层样式
  static externalClasses = ['anim-class', 'custom-class', 'modal-class'];

  /**
   * 组件的属性列表
   */
  static defaultProps = {
    duration: 200,
    /*是否允许点击遮罩层，隐藏*/
    clickMaskHide: true,
    onHide: null,
    customStyle: '',
  };

  /**
   * 组件的初始数据
   */
  state = {
    start: {
      scale: false,
      translateY: 0,
    },

    showModalStatus: false,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    this.animation = wxApi.createAnimation({
      duration: this._getDuration(),
      timingFunction: 'linear',
      delay: 0,
    });
  }

  preventTouchMove = (e) => {
    e.preventDefault();
    e.stopPropagation();
    return;
  };

  _getDuration() {
    return this.props.duration || 200;
  }

  _raf(callback) {
    setTimeout(
      function () {
        callback();
      }.bind(this),
      16
    );
  }

  _scale(isShow, immediate = false) {
    if (isShow) {
      this.setState({
        animationData: this.animation.scale(0, 0).step().export(),
        showModalStatus: true,
      });

      this._raf(() => {
        this.setState({
          animationData: this.animation.scale(1, 1).step().export(),
        });
      });
    } else {
      this.setState({
        animationData: this.animation.scale(0, 0).step().export(),
      });

      if (immediate) {
        this.setState({
          showModalStatus: false,
        });
      } else {
        setTimeout(() => {
          this.setState({
            showModalStatus: false,
          });
        }, this._getDuration() + 16);
      }
    }
  }

  _translateY(isShow) {
    const { translateY } = this.state.start;
    if (isShow) {
      this.setState({
        animationData: this.animation.translateY(translateY).step().export(),
        showModalStatus: true,
      });

      this._raf(() => {
        this.setState({
          animationData: this.animation.translateY(0).step().export(),
        });
      });
    } else {
      this.setState({
        animationData: this.animation.translateY(translateY).step().export(),
      });

      setTimeout(() => {
        this.setState({
          showModalStatus: false,
        });
      }, this._getDuration() + 16);
    }
  }

  show(options) {
    const { scale, translateY } = options;
    this.setState({
      start: {
        scale,
        translateY,
      },
    });

    if (scale) {
      this._scale(true);
    } else if (translateY) {
      this._translateY(true);
    }
  }

  /**
   *
   * @param immediate 是否立即隐藏遮罩
   */
  hide(immediate = false) {
    const { scale, translateY } = this.state.start;
    if (scale) {
      this._scale(false, immediate);
    } else if (translateY) {
      this._translateY();
    }
    // this.triggerEvent('hide')
    this.props.onHide && this.props.onHide('hide');
  }

  onClickMaskHide = (e) => {
    e.stopPropagation();
    if (this.props.clickMaskHide) {
      this.hide();
    }
  };

  onStopBubbule = (e) => {
    e.stopPropagation();
  };

  render() {
    const { showModalStatus, animationData } = this.state;
    const { customStyle } = this.props;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wca-AnimatDialog'
        className='wk-wca-AnimatDialog'
      >
        {!!showModalStatus && (
          <View
            onTouchMove={this.preventTouchMove}
            className={`commodity_screen ${this.props.modalClass || ''}`}
            onClick={this.onClickMaskHide}
          />
        )}

        {/* 弹出框  */}
        {!!showModalStatus && (
          <View
            animation={animationData}
            onClick={this.onStopBubbule}
            className={`${screen.isFullScreenPhone ? 'full-screen' : ''} ${this.props.customClass || ''} ${
              this.props.animClass || ''
            }`}
            style={_safe_style_(customStyle)}
          >
            {this.props.children}
          </View>
        )}
      </View>
    );
  }
}

export default AnimatDialog;
