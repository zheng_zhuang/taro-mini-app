import React from 'react';
import { PageStyle, _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import reportConstants from '@/sdks/buried/report/report-constants.js';
import screen from '@/wxat-common/utils/screen';

import HoverCart from '@/wxat-common/components/cart/hover-cart/index';
import Classify from '@/wxat-common/components/classify/index';
import { connect } from 'react-redux';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';
import AuthPuop from '@/wxat-common/components/authorize-puop/index';
import './index.scss';

const mapStateToProps = (state) => ({
  currentStoreViewVisible: state.base.currentStoreViewVisible,
  globalData: state.globalData,
});

const mapDispatchToProps = (dispatch) => ({});
// @hoc

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })

class ClassifyPage extends React.Component {
  state = {
    showClassify: true,
    reportSource: reportConstants.SOURCE_TYPE.classify.key,
    navHeight: 0,
    navBackgroundColor: '',
  };

  componentDidMount() {
    this.setState({
      navHeight: screen.totalNavigationBarHeight,
    });

    wxAppProxy.setNavColor('pages/classify/index', 'wxat-common/pages/classify/index');
    const params = wxAppProxy.getNavColor('wxat-common/pages/classify/index');
    this.setNavColor(params);
  } /*请尽快迁移为 componentDidUpdate*/

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const { currentStoreViewVisible, globalData } = this.props;
    if (currentStoreViewVisible !== nextProps.currentStoreViewVisible) {
      this.setState({
        showClassify: nextProps.currentStoreViewVisible,
      });
    } else if (globalData !== nextProps.globalData) {
      this.setState({
        navHeight: screen.totalNavigationBarHeight,
      });
    }
  }

  onReachBottom() {
    this.classifyCOMPT.current && this.classifyCOMPT.current.onScrollViewReachBottom();
  }

  setNavColor(params) {
    if (params) {
      this.setState({
        navBackgroundColor: params.navBackgroundColor || '#ffffff', // 必写项
      });
    }
  }

  classifyCOMPT = React.createRef();

  // config = {
  //   navigationStyle: 'custom',
  //   navigationBarTitleText: '产品分类'
  // }

  render() {
    const { navHeight, navBackgroundColor, reportSource, showClassify } = this.state;
    return (
      // <Block>
      <View
        data-scoped='wk-wpc-Classify'
        className='wk-wpc-Classify page'
        style={_safe_style_(PageStyle(this.props.style))}
      >
        {!!showClassify && (
          <Classify
            ref={this.classifyCOMPT}
            navBackgroundColor={navBackgroundColor}
            isQuickBuyOpen={false}
            reportSource={reportSource}
          />
        )}

        <HoverCart />
        <AuthPuop />
      </View>
      // </Block>
    );
  }
}

export default ClassifyPage;
