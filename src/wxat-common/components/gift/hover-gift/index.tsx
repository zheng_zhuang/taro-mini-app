import { _safe_style_ } from '@/wk-taro-platform';
import { MovableArea, MovableView, View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import store from '@/store';
import template from '../../../utils/template.js';
import authHelper from '../../../utils/auth-helper.js';
import './index.scss';
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl.js'
import wxApi from '@/wxat-common/utils/wxApi';
import MovableViewH5 from '@/wxat-common/components/movable-view-h5'
interface HoverGiftProps {
  dataSource?: any,
  list?: any[],
  isPageShow: boolean,
  onHandleClickGift?: () => void,
}

interface State {
  x: number,
  y: number,
  winHeight: number,
  tmpStyle: any,
  industry: any,
  environment: any,
}

class HoverGift extends React.Component<HoverGiftProps, State> {
  state = {
    x: 350,
    y: -200,
    winHeight: 0,
    tmpStyle: {},
    industry: store.getState().globalData.industry,
    // environment === 'wxwork': 企业微信中运行
    environment: store.getState().globalData.environment,
  };

  componentDidMount() {
    this.getTemplateStyle();
    const sysInfo = wxApi.getSystemInfoSync();
    this.setState({
      industry: store.getState().globalData.industry,
    });
  };

  handlerClickGift = () => {
    const { onHandleClickGift } = this.props
    if (!authHelper.checkAuth()) return;
    onHandleClickGift?.();
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { x, y } = this.state;
    const giftEnterBtnPng = getStaticImgUrl.images.gift_enter_btn_png;
    return (
      <View
        data-scoped="wk-wcgh-HoverGift"
        className="wk-wcgh-HoverGift"
      >
        {
          process.env.TARO_ENV === 'weapp' && (
            <MovableArea className="gift-movable-area" style={_safe_style_('height: 85%;')}>
              <MovableView
                className="gift-movable-view"
                x={x}
                y={y}
                onClick={this.handlerClickGift}
                direction="all"
                style={_safe_style_('top: 100%')}
              >
                <View className="gift">
                  <Image
                    className="gift-img"
                    mode="widthFix"
                    src={giftEnterBtnPng}
                  ></Image>
                  <Text className="gift-title">送礼</Text>
                </View>
              </MovableView>
            </MovableArea>
          )
        }

        {
          process.env.TARO_ENV === 'h5' && (
            <MovableViewH5 key="hover-gift" passClick={this.handlerClickGift}>
              <View className="gift">
                  <Image
                    className="gift-img"
                    mode="widthFix"
                    src={giftEnterBtnPng}
                  ></Image>
                  <Text className="gift-title">送礼</Text>
              </View>
            </MovableViewH5>
          )
        }
      </View>
    );
  }
}

export default HoverGift;
