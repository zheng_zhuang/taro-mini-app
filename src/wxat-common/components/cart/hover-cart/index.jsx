import React from 'react';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import HoverCart from './hover-cart';

const mapStateToProps = (state) => ({
  appInfo: state.base.appInfo,
  currentStore: state.base.currentStore,
  environment: state.globalData.environment,
  themeConfig: state.globalData.themeConfig || null,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class HoverCartWrap extends React.Component {
  render() {
    return <HoverCart {...this.props} />;
  }
}

export default HoverCartWrap;
