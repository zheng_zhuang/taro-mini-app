import React, { useState, useEffect, useRef } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import buyHub from '../buy-hub';
import cartHelper from '../cart-helper';
import template from '../../../utils/template';
import authHelper from '../../../utils/auth-helper';
import SkuDialog from "../sku-dialog";
import './index.scss';

import getStaticImgUrl from '../../../constants/frontEndImgUrl'

const CartCount = (props) => {
  const { spu, formatSkuItem, reportSource, width, height, left, bottom, appInfo, environment, themeConfig } = props;
  const [count, setCount] = useState(0);
  const [formatSpuItem, setFormatSpuItem] = useState(null);
  const [tmpStyle, setTmpStyle] = useState({});
  const [showAddCart, setShowAddCart] = useState(false);
  const cartCountRef = useRef({});

  const refSkuDialog = useRef({});

  useEffect(() => {
    buyHub.hub.onMapChange(acceptMapChange);
    return () => {
      buyHub.hub.offMapChange(acceptMapChange);
    };
  }, []);
  useEffect(() => {
    setShowAddCart(appInfo && appInfo.floatCart && environment !== 'wxwork');
  }, [appInfo]);

  useEffect(() => {
    updateCartItem();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [spu, formatSkuItem]);

  useEffect(() => {
    setTmpStyle(getTemplateStyle());
  }, [themeConfig]);

  useEffect(() => {
    if (formatSpuItem) {
      acceptMapChange();
    }
  }, [formatSpuItem]);

  /**
   * 更具sku展示商品购买信息
   */
  function updateCartItem() {
    if (formatSkuItem && formatSkuItem.itemNo) {
      cartCountRef.current.formatSpuItem = formatSkuItem;
      setFormatSpuItem(() => formatSkuItem);
    } else {
      const spuItem = buyHub.formatSpu(spu.wxItem, null);
      cartCountRef.current.formatSpuItem = spuItem;
      setFormatSpuItem(() => spuItem);
    }
  }

  const acceptMapChange = () => {
    const uuid = buyHub.uuid(cartCountRef.current.formatSpuItem);
    const inBuyHub = Object.keys(buyHub.map).some((key) => {
      return key.indexOf(uuid) !== -1;
    });
    if (buyHub.map[uuid]) {
      cartCountRef.current.count = buyHub.map[uuid].count;
      setCount(() => buyHub.map[uuid].count);
    } else if (inBuyHub) {
      const buyHubCount = buyHub.getSpecGoodsCartCount(cartCountRef.current.formatSpuItem.itemNo);
      cartCountRef.current.count = buyHubCount;
      setCount(() => buyHubCount);
    } else if (cartCountRef.current.count > 0) {
      cartCountRef.current.count = 0;
      setCount(() => 0);
    }
  };

  function clickMinus(e) {
    e.stopPropagation();
    if (cartHelper.isGettingCarts() || !cartHelper.isGettedCarts()) {
      wxApi.showToast({
        title: '正在请求购物车数据，请稍后重试',
        icon: 'none',
      });

      // 只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
      if (!cartHelper.isGettingCarts()) {
        cartHelper.getCarts(true);
      }
      return;
    }
    cartHelper.addOrUpdateCart(formatSpuItem, 1, false, (res) => {
      onInsertOrUpdateEnd(res, count - 1);
    });
  }
  function clickPlus(e) {
    e.stopPropagation();
    if (!authHelper.checkAuth()) {
      return;
    }
    if (cartHelper.isGettingCarts() || !cartHelper.isGettedCarts()) {
      wxApi.showToast({
        title: '正在请求购物车数据，请稍后重试',
        icon: 'none',
      });

      // 只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
      if (!cartHelper.isGettingCarts()) {
        cartHelper.getCarts(true);
      }
      return;
    }
    // 商品库存不足 (库存字段为null或者0  或者 小于0  或者加入的数量大于库存)
    // fixme 保留库存不足的逻辑
    if (!formatSpuItem.stock || formatSpuItem.stock < 0 || formatSpuItem.stock <= count) {
      wxApi.showToast({
        title: '商品库存不足',
        icon: 'none',
      });

      return;
    }
    cartHelper.addOrUpdateCart({ ...formatSpuItem, source: reportSource }, 1, true, (res) => {
      onInsertOrUpdateEnd(res, count + 1);
    });
  }

  function onInsertOrUpdateEnd(res, goodsNum) {
    if (!res.success) {
      if (res.errorCode === cartHelper.cartCode.LOW_STOCK) {
        wxApi.showToast({
          title: '库存不足',
          icon: 'none',
        });
      } else {
        wxApi.showToast({
          title: '添加失败',
          icon: 'none',
        });
      }
    }
  }
  function _selfChange(count) {
    cartCountRef.current.count = count;
    setCount(() => count);
  }
  function stopBubble() {
    console.log('stopBubble');
  }
  function showSkuDialog(e) {
    e.stopPropagation();
    if (!authHelper.checkAuth()) {
      return;
    }
    if (cartHelper.isGettingCarts() || !cartHelper.isGettedCarts()) {
      wxApi.showToast({
        title: '正在请求购物车数据，请稍后重试',
        icon: 'none',
      });

      // 只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
      if (!cartHelper.isGettingCarts()) {
        cartHelper.getCarts(true);
      }
      return;
    }
    if (!formatSpuItem.stock || formatSpuItem.stock < 0 || formatSpuItem.stock <= count) {
      wxApi.showToast({
        title: '商品库存不足',
        icon: 'none',
      });

      return;
    }
    refSkuDialog.current.show(spu);
  }
  // 获取模板配置
  function getTemplateStyle() {
    return template.getTemplateStyle();
  }

  return (
    !!showAddCart && (
      <View data-scoped='wk-ccc-CartCount' className='wk-ccc-CartCount count' onClick={stopBubble}>
        {spu.skuInfoList && spu.skuInfoList.length && !formatSkuItem ? (
          <View
            className={'plus-box ' + (formatSpuItem.stock < 1 ? 'disabled' : '')}
            style={_safe_style_(
              'background: ' + tmpStyle.btnColor + ';width: ' + width + 'px;height: ' + height + 'px;'
            )}
            onClick={showSkuDialog}
          >
            <Image
              src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-add-cart.png"
              className='add-cart-icon'
              style={_safe_style_('width: ' + width + 'px;height: ' + height + 'px;')}
            ></Image>
            {count > 0 && (
              <View className='cart-num' style={_safe_style_('left: ' + left + 'px;bottom: ' + bottom + 'px;')}>
                {count}
              </View>
            )}
          </View>
        ) : (
          <Block>
            {count > 0 && (
              <Image
                className='minus'
                src={getStaticImgUrl.plugin.minus_png}
                onClick={clickMinus}
              ></Image>
            )}

            {count > 0 && <View className='num'>{count}</View>}
            {count > 0 && (
              <Image className='plus' src={getStaticImgUrl.plugin.plus_png} onClick={clickPlus}></Image>
            )}

            {/* fixme 保留库存不足的逻辑 {{(formatSpuItem.stock < 1) ? 'disabled' : ''}} */}
            {count === 0 && (
              <View
                className={'plus-box ' + (formatSpuItem.stock < 1 ? 'disabled' : '')}
                style={_safe_style_(
                  'background: ' + tmpStyle.btnColor + ';width: ' + width + 'px;height: ' + height + 'px;'
                )}
                onClick={clickPlus}
              >
                <Image
                  src="https://bj.bcebos.com/htrip-mp/static/app/images/common/ic-add-cart.png"
                  style={_safe_style_('width: ' + width + 'px;height: ' + height + 'px;')}
                  className='add-cart-icon'
                ></Image>
              </View>
            )}
          </Block>
        )}

        {/*  加入购物车操作按钮  */}
        {/*  选择商品sku对话弹框  */}
        <SkuDialog ref={refSkuDialog} reportSource={reportSource}></SkuDialog>
      </View>
    )
  );
};

export default CartCount;

CartCount.defaultProps = {
  spu: {},
  formatSkuItem: null,
  reportSource: '',
  width: '60',
  height: '60',
  left: '30',
  bottom: '40',
};
