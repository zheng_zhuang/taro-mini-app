import React from 'react';
import '@/wxat-common/utils/platform';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import CartCount from './cart-count';

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
  appInfo: state.base.appInfo,
  environment: state.globalData.environment,
  themeConfig: state.globalData.themeConfig || null,
});

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class HoverCartWrap extends React.Component {
  static defaultProps = {
    spu: {},
    formatSkuItem: null,
    reportSource: '',
    width: '30',
    height: '30',
    left: '15',
    bottom: '20',
  };

  render() {
    return <CartCount {...this.props}></CartCount>;
  }
}

export default HoverCartWrap;
