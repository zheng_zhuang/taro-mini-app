/**
 * Created by love on 18/12/18.
 * @author trumpli<李志伟>
 */
const mapChangeWaits = [];
const map = {};
const mapIndex = [];
export default {
  hub: {
    //监听 集合 改变
    onMapChange(fn) {
      mapChangeWaits.push(fn);
    },
    //移除监听 集合 改变
    offMapChange(fn) {
      const index = mapChangeWaits.findIndex((theFn) => {
        return theFn === fn;
      });
      index !== -1 && mapChangeWaits.splice(index, 1);
    },
    //加入集合 count = 0时，会删除该条购物车记录
    put(key, value, silent) {
      if (!map.hasOwnProperty(key) && value.count !== 0) {
        map[key] = { ...value };
        mapIndex.splice(0, 0, key);
      } else {
        if (value.count === 0) {
          // 寻找key所在mapIndex的位置
          const deleteIndex = mapIndex.findIndex((itemNo) => itemNo == key);
          delete mapIndex[deleteIndex];
          delete map[key];
        } else {
          map[key].count = value.count;
          if (map[key].labelPrice !== value.labelPrice) {
            map[key].labelPrice = value.labelPrice;
          }
        }
      }
      if (!silent) {
        this.emitMapChange();
      }
    },
    clearCarts() {
      if (map) {
        Object.keys(map).forEach((key) => {
          delete map[key];
        });
        if (mapIndex) {
          Object.keys(mapIndex).forEach((key) => {
            delete mapIndex[key];
          });
        }
        this.emitMapChange();
      }
    },
    //集合发生改变 通知 监听者
    emitMapChange() {
      mapChangeWaits.forEach((fn) => {
        fn();
      });
    },
  },
  uuid(cartItem) {
    return '-' + cartItem.itemNo + '-' + (cartItem.skuId || '');
  },
  getCartItem(spuItem) {
    return map[this.uuid(spuItem)];
  },
  getSpecGoodsCartCount(itemNo) {
    let count = 0;
    Object.keys(map).forEach((key) => {
      if (itemNo && key.indexOf(itemNo) !== -1) {
        count += map[key].count;
      }
    });
    return count;
  },
  formatCart,
  formatSpu,
  formatSku,
  map,
  mapIndex,
};

function formatCart(cartItem) {
  return {
    id: cartItem.id,
    itemNo: cartItem.itemNo /*购物车里的商品id*/,
    skuId: cartItem.skuId,
    skuInfo: cartItem.skuInfo,
    name: cartItem.itemName || cartItem.name,
    barcode: cartItem.barcode,
    count: cartItem.itemCount || 0,
    price: cartItem.salePrice,
    labelPrice: cartItem.labelPrice,
    stock: cartItem.itemStock,
    thumbnail: cartItem.thumbnail,
    preSell: cartItem.preSell,
    categoryId: cartItem.categoryId,
    drugType: cartItem.drugType,
  };
}

function formatSpu(wxItem, sku) {
  const itemNo = wxItem ? wxItem.itemNo : ''; /*不论是通过openApi导入的商品还是自己创建的商品标志都改为=itemNo*/
  let skuId = null;
  let skuInfo = '';
  let stock = wxItem ? wxItem.itemStock : '';
  let price = wxItem ? wxItem.salePrice : '';
  let labelPrice = wxItem ? wxItem.labelPrice : '';
  let barcode = wxItem ? wxItem.barcode : '';
  if (!!sku) {
    skuId = sku.skuId;
    stock = sku.stock;
    //组装sku信息
    const skuInfoNames = sku.skuInfoNames;
    if (!!skuInfoNames) {
      const len = skuInfoNames.length;
      skuInfoNames.forEach((info, index) => {
        skuInfo += info.valName;
        if (index < len - 1) {
          skuInfo += '|';
        }
      });
    }
    price = sku.salePrice;
    labelPrice = sku.labelPrice;
    barcode = sku.skuBarcode;
  }
  return {
    skuId,
    stock,
    skuInfo,
    price,
    labelPrice,
    barcode,
    itemNo: itemNo,
    name: wxItem ? wxItem.name : '',
    thumbnail: wxItem ? wxItem.thumbnail : '',
    preSell: wxItem ? wxItem.preSell : '',
    drugType: wxItem ? wxItem.drugType : '',
  };
}
/*格式化指定的sku item信息,如根据barcode拿到确定的sku商品信息*/
function formatSku(skuItem) {
  return {
    skuId: skuItem ? skuItem.skuId : '',
    stock: skuItem ? skuItem.itemStock : '',
    skuInfo: skuItem ? skuItem.itemAttrAsStr || '' : '',
    price: skuItem ? skuItem.salePrice : '',
    labelPrice: skuItem ? skuItem.labelPrice : '',
    itemNo: skuItem ? skuItem.itemNo : '',
    name: skuItem ? skuItem.itemName : '',
    barcode: skuItem ? skuItem.barcode : '',
    thumbnail: skuItem ? skuItem.thumbnail : '',
    preSell: skuItem ? skuItem.preSell : '',
    drugType: skuItem ? skuItem.drugType : '',
    skuItem,
  };
}
