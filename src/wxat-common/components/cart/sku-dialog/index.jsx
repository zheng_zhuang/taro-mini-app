import React from 'react'; /* eslint-disable react/sort-comp */
import '@/wxat-common/utils/platform'; // @externalClassesConvered(AnimatDialog)
import { Image, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import classNames from 'classnames';
import filters from '../../../utils/money.wxs';
import buyHub from '../buy-hub.js';
import template from '../../../utils/template';
import { calcDisabled } from '../../../utils/sku';

import CartCount from '../cart-count';
import AnimatDialog from '../../animat-dialog/index';
import './index.scss';

class SkuDialog extends React.Component {
  static defaultProps = {
    reportSource: '',
  };

  state = {
    name: '',
    spu: null,
    choose: [],
    sku: null,
    allPrice: 0,
    tmpStyle: {},
  };

  componentDidMount() {
    this.getTemplateStyle();
  }

  keys = {};

  show(spu) {
    // 打开上次的 spu
    if (spu === this.state.spu) {
      if (this.skuAnimatDialogCOMPT) {
        this.skuAnimatDialogCOMPT.show({ scale: 1 });
      }
      return;
    }

    const keys = {};
    let choose = [];
    const name = spu['wxItem'].name;

    // 第一个可用的 sku
    let validSku = null;

    spu['skuInfoList'].forEach((sku, index) => {
      const key = [];

      if (!sku.notOptional && !validSku) validSku = sku;

      sku['skuInfoNames'].forEach((si) => {
        if (index === 0) choose.push(0);

        key.push(si['valId']);
      });
      keys[key.join('-')] = buyHub.formatSpu(spu['wxItem'], sku);
    });

    if (validSku) {
      choose = validSku.skuInfoNames.map((info, index) => {
        const valItem = spu.skuTreeList[index].treeValList.find((i) => i.valId === info.valId);
        valItem.active = true;
        return info.valId;
      });
    }

    this.keys = keys;
    this.setState(
      {
        name,
        spu,
        choose,
        sku: validSku ? keys[choose.join('-')] : null,
      },

      () => {
        this.tryPickAlreadyCartItem();
        if (this.skuAnimatDialogCOMPT) {
          this.skuAnimatDialogCOMPT.show({ scale: 1 });
        }
      }
    );
  }

  hide = (e) => {
    e.stopPropagation();
    this.skuAnimatDialogCOMPT && this.skuAnimatDialogCOMPT.hide();
  };

  clickItem(valId, valIndex, e) {
    const { skuTreeList } = this.state.spu;

    skuTreeList[valIndex].treeValList.forEach((t) => (t.active = t.valId === valId));

    e.stopPropagation();
    const keys = this.keys;
    const { choose } = this.state;
    choose[valIndex] = valId;
    const sku = keys[choose.join('-')];
    const price = sku ? sku.price : 0;
    this.setState(
      {
        choose,
        sku: sku || null,
        allPrice: price,
      },

      () => {
        //切换sku时，试图从购物车里查找相应的sku
        this.tryPickAlreadyCartItem();
      }
    );
  }

  findSkuBySkuId(skuId) {
    return this.state.spu.skuInfoList.find((item) => item.skuId === skuId) || null;
  }

  //挑选一个 已在购物车中的spu，展现
  tryPickAlreadyCartItem() {
    const keys = this.keys;
    const { sku: currentSku, spu } = this.state;
    let sku, uuid, item, allPrice;
    for (const key in keys) {
      item = keys[key];
      uuid = buyHub.uuid(item);
      sku = !!uuid ? buyHub.map[uuid] : null;
      if (!!sku) {
        // 如果当前没有初始化过sku或者当前选中的sku在购物车中匹配，则将购物车中的购买数量展示出来
        if (!currentSku || currentSku.skuId === sku.skuId) {
          sku = this.findSkuBySkuId(sku.skuId);
          const choose = [];

          sku.skuInfoNames.forEach((info, index) => {
            choose[index] = info.valId;

            const valItem = spu.skuTreeList[index].treeValList.find((i) => i.valId === info.valId);
            valItem.active = true;
          });

          sku = buyHub.map[uuid];
          allPrice = sku.count * sku.price;

          return this.setState({ sku, choose, allPrice });
        }
      }
    }
  }

  onBuyChange(e) {
    const num = e.detail;
    this.setState({
      allPrice: num * this.state.sku.price,
    });
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  refSkuAnimatDialogCOMPT = (node) => (this.skuAnimatDialogCOMPT = node);

  render() {
    const { reportSource } = this.props;
    const { name, spu, choose, tmpStyle, allPrice, sku } = this.state;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-ccs-SkuDialog'
        className='wk-ccs-SkuDialog'
      >
        <AnimatDialog ref={this.refSkuAnimatDialogCOMPT} animClass='sku-dialog'>
          <Image src="https://bj.bcebos.com/htrip-mp/static/app/images/common/line-close.png" className='close' onClick={this.hide}></Image>
          <View className='title'>{name}</View>
          <View className='sku-wrap'>
            {spu &&
              spu.skuTreeList &&
              spu.skuTreeList.map((item, index) => {
                return (
                  <View className='sku-item' key={index}>
                    <View className='sku-title'>{item.keyName + '：'}</View>
                    <View className='sku-box' key={sku}>
                      {item.treeValList.map((treeItem) => {
                        const disabled = calcDisabled(spu.skuTreeList, spu.skuInfoList, item.keyId, treeItem.valId);
                        return (
                          <View
                            key={treeItem.valId}
                            className={classNames({
                              'sku-name': true,
                              'sku-select': choose[index] === treeItem.valId,
                              disabled: disabled,
                            })}
                            onClick={disabled ? undefined : this.clickItem.bind(this, treeItem.valId, index)}
                            style={
                              choose[index] === treeItem.valId
                                ? {
                                    color: tmpStyle.btnColor,
                                    background: tmpStyle.bgColor,
                                    borderColor: tmpStyle.btnColor,
                                  }
                                : {}
                            }
                          >
                            {treeItem.valName}
                          </View>
                        );
                      })}
                    </View>
                  </View>
                );
              })}
          </View>
          <View className='bottom'>
            {!!sku && (
              <View className='price-container'>
                <Text className='unit' style={{ color: tmpStyle.btnColor }}>
                  ￥
                </Text>
                <Text className='price' style={{ color: tmpStyle.btnColor }}>
                  {filters.moneyFilter(allPrice ? allPrice : sku.price, true)}
                </Text>
              </View>
            )}

            {!!sku && (
              <View className='sku-count'>
                <CartCount formatSkuItem={sku} reportSource={reportSource} spu={spu}></CartCount>
              </View>
            )}
          </View>
        </AnimatDialog>
      </View>
    );
  }
}

export default SkuDialog;
