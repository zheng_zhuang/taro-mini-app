import wxApi from '../../utils/wxApi';
import buyHub from './scan-buy-hub.js';
import api from '../../api/index.js';
import report from '../../../sdks/buried/report/index.js';
import reportConstants from '../../../sdks/buried/report/report-constants.js';

let gettingCarts = false;
let getedCarts = false;

export default {
  cartCode: {
    SUCCESS: '200' /*成功*/,
    NO_PERMISSION: '50001' /*无权限*/,
    LOW_STOCK: '50011' /*库存不足*/,
  },
  /**
   * 加入或更新购物车操作，可以+1，也可以-1，商品数为0时，删除购物车该item
   * @param formatSpuItem 经过转换的spuItem，根据spu和sku转换而来，字段参考buyHub.formatSpu()返回的字段
   * @param count 商品数目
   * @param isPlus 是+1还是-1
   * @param callback 回调函数
   */
  addOrUpdateCart(formatSpuItem, count, isPlus, callback) {
    let cartItem = buyHub.getCartItem(formatSpuItem);
    const sessionId = report.getBrowseItemSessionId(formatSpuItem.itemNo);
    if (!cartItem) {
      //购物车里没有该商品，直接添加
      const data = {
        type: 1,
        itemNo: formatSpuItem.itemNo,
        count: count,
        reportExt: JSON.stringify({ sessionId: sessionId }),
      };
      if (formatSpuItem.skuId) {
        data.skuId = formatSpuItem.skuId;
      }
      wxApi
        .request({
          url: api.shopping_card.add,
          loading: true,
          data,
        })
        .then((res) => {
          cartItem = { ...formatSpuItem };
          cartItem.count = count;
          cartItem.id = res.data; //购物车item id
          cartItem._checked = true; //首次加入购物车，默认选中
          cartItem.reportExt = JSON.stringify({ sessionId: sessionId });
          buyHub.hub.put(buyHub.uuid(cartItem), cartItem, false);
          typeof callback === 'function' && callback(res);
          report.browseItem({
            source: reportConstants.SOURCE_TYPE.scan.key,
            itemNo: formatSpuItem.itemNo,
            sessionId: sessionId,
          });
          report.toShopCart(formatSpuItem.itemNo, sessionId);
        });
    } else {
      //购物车里有该商品，调更新购物车接口
      const data = {
        id: cartItem.id,
        type: 1,
      };
      if (cartItem.skuId) {
        data.skuId = cartItem.skuId;
      }
      if (isPlus) {
        data.count = cartItem.count + count;
      } else {
        data.count = cartItem.count - count;
      }
      wxApi
        .request({
          url: api.shopping_card.update,
          loading: true,
          data,
        })
        .then((res) => {
          if (res.data) {
            cartItem.count = data.count;
            buyHub.hub.put(buyHub.uuid(cartItem), cartItem, false);
            typeof callback === 'function' && callback(res);
          }
        });
    }
  },

  /**
   * 完全删除购物车中某个商品
   * @param idList
   * @param callback 回调函数
   */
  deleteGoods(idList, callback) {
    wxApi
      .request({
        url: api.shopping_card.batch_remove,
        loading: true,
        data: {
          idList: idList.join(','),
          type: 1,
        },
      })
      .then((res) => {
        if (res.data === true) {
          // 寻找hub里面的对应的数据，交叉对比
          const delList = [];
          (idList || []).forEach((id) => {
            for (let key in buyHub.map) {
              const item = buyHub.map[key];
              if (id === item.id) {
                item.count = 0;
                delList.push(item);
              }
            }
          });
          delList.forEach((item) => {
            buyHub.hub.put(buyHub.uuid(item), item, true);
          });
          buyHub.hub.emitMapChange();
        }
        typeof callback === 'function' && callback(res);
      });
  },
  /**
   * 获取购物车数据
   * @param forceUpdate 是否强制请求
   * @param callback 成功后的回调
   * @param errorCallback
   */
  getCarts(forceUpdate = false, callback, errorCallback) {
    // 请求到数据了  或者 已经请求完成了  直接回调buy-hub的数据
    if ((Object.keys(buyHub.map).length || (getedCarts === true && gettingCarts === false)) && !forceUpdate) {
      buyHub.hub.emitMapChange();
      typeof callback === 'function' && callback(buyHub.hub);
    } else {
      gettingCarts = true;
      getedCarts = false;
      wxApi
        .request({
          url: api.shopping_card.queryList,
          loading: false,
          quite: true,
          data: {
            type: 1,
          },
        })
        .then((res) => {
          if (res.success) {
            const cartList = [];
            (res.data || []).forEach((item) => {
              item.skuInfo = (item.skuInfo || '').replace(/\|/g, '  ');
              cartList.push(item);
            });
            buyHub.hub.clearCarts();

            {
              cartList.forEach((cartItem, index) => {
                const formatCartItem = buyHub.formatCart(cartItem);
                formatCartItem._checked = true;
                buyHub.hub.put(buyHub.uuid(formatCartItem), formatCartItem, true);
              });
            }
            buyHub.hub.emitMapChange();
            gettingCarts = false;
            getedCarts = true;
            typeof callback === 'function' && callback(buyHub.hub);
          } else {
            gettingCarts = false;
            getedCarts = false;
            typeof callback === 'function' && callback(null);
          }
        })
        .catch((error) => {
          gettingCarts = false;
          getedCarts = false;
          typeof errorCallback === 'function' && errorCallback(error);
        });
    }
  },

  isGettingCarts() {
    return gettingCarts;
  },

  isGettedCarts() {
    return getedCarts;
  },
};
