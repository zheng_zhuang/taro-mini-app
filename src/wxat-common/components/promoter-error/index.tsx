import React, { FC, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../utils/wxApi';
import './index.scss';

import useTemplateStyle from '@/hooks/useTemplateStyle';

// props的类型定义
type ComponentProps = {
  onCallSomeFun?: () => any;
};

let PromoterErrorModule: FC<ComponentProps> = (props) => {
  const tmpStyle = useTemplateStyle({ autoSetNavigationBar: true });
  const { onCallSomeFun } = props;

  const backHome = () => {
    wxApi.$navigateTo({
      url: 'wxat-common/pages/home/index',
    });
  };

  useEffect(() => {
    onCallSomeFun && onCallSomeFun();
  });

  return (
    <View data-scoped='wk-wct-PromoterError' className='wkt-promoter-error wrap'>
      <Image
        className='bg-image'
        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/mine/ic-error.png'
      ></Image>
      <View className='text'>您不是该导购的推广大使 ，无法进行推广</View>
      <View className='content-bottom' style={_safe_style_('background-color:' + tmpStyle.btnColor)} onClick={backHome}>
        返回首页
      </View>
    </View>
  );
};

export default PromoterErrorModule;
