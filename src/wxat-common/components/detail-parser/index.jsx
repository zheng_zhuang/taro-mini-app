import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Text, Block } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import themeTemplate from '../../utils/template';
import imageUtils from '../../utils/image';
import ParseWrapper from '../parse-wrapper';
import './index.scss';
import wxParse from '../parse-wrapper/wxParse';

@hoc
class DetailParser extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    headerLabel: '商品详情',
    /* 是否显示高亮标题headline */
    showHeadline: false,
    /* 是否显示普通header */
    showHeader: true,
    topOffset: 0,
    parseContext: null,
    itemDescribe: null
  };

  /**
   * 组件的初始数据
   */
  state = {
    tmpStyle: {},
    parseContext: null,
  }; /* 请尽快迁移为 componentDidMount 或 constructor */


  componentDidMount() {
    this.parseDescribe();
    this.getTemplateStyle();
  }

  parseDescribe() {
    let itemDescribe = this.props.itemDescribe;
    if (itemDescribe) {
      try {
        itemDescribe = imageUtils.richTextFilterCdnImage(itemDescribe);
        const parseContext = wxParse('html', itemDescribe);
        this.setState({ parseContext });
      } catch (e) {
        console.log('富文本转换异常', e);
      }
    }
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = themeTemplate.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { topOffset, showHeadline, showHeader, headerLabel } = this.props;
    const { tmpStyle, parseContext } = this.state;

    return (
      !!(parseContext && parseContext.length !== 0) && (
        <View
          data-fixme='02 block to view. need more test'
          data-scoped='wk-wcd-DetailParser'
          className='wk-wcd-DetailParser'
        >
          <View className='detail-container' style={_safe_style_('margin-top: ' + topOffset / 2 + 'px;')}>
            {showHeadline && !showHeader ? (
              <View className='detail-headline'>
                <Text>{headerLabel}</Text>
              </View>
            ) : (
              showHeader && (
                <View className='detail-header'>
                  <Text className='header-label' style={_safe_style_('background: ' + tmpStyle.btnColor)}></Text>
                  <Text>{headerLabel}</Text>
                </View>
              )
            )}

            <View className='detail-desc'>
              <ParseWrapper parseContent={parseContext} />
            </View>
          </View>
        </View>
      )
    );
  }
}

export default DetailParser;
