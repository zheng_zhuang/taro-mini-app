import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import config from '../utils/config';
import api from "../../../api";
import imageUtils from '../../../utils/image';

import TextNavModule from "../../decorate/textNavModule";
import './index.scss';
import hoc from '@/hoc';

interface BaseEbooksModuleProps {
  dataSource: Record<string, any>;
}

interface BaseEbooksModule {
  props: BaseEbooksModuleProps;
}

@hoc
class BaseEbooksModule extends React.Component {
  state = {
    PADDING: config.PADDING,
    renderList: [] as any[],
  };

  componentDidMount() {
    this.fetchData();
  }

  handleGoToDetail = () => {
    wxApi.$navigateTo({ url: '/sub-packages/marketing-package/pages/ebooks/index' });
  };

  fetchData = async () => {
    const idString = this.getIds();
    if (!idString.length) {
      return;
    }

    const { data, success } = await wxApi.request({
      url: api.ebooks.ebooksList,
      quite: true,
      data: { pageNo: 1, pageSize: 6, albumStatus: 1, idString: idString },
    });

    if (!success) return;

    if (!data || !data.length) {
      this.setState({ renderList: [] });
    } else {
      (data || []).forEach((item) => {
        item.coverUrl = imageUtils.thumbnailOneRowTwo(item.coverUrl);
      });
      this.setState({ renderList: data });
    }
  };

  getIds = () => {
    const { dataSource } = this.props;
    const ids = dataSource.data.map(({ id }) => id).join(',') || '';
    return ids;
  };

  render() {
    const { dataSource = {} } = this.props;
    const { PADDING, renderList } = this.state;

    return (
      <View
        data-scoped='wk-cbe-EbooksModule'
        className='wk-cbe-EbooksModule ebooks-module'
        style={_safe_style_(
          'margin:' +
            (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
            ';border-radius: ' +
            dataSource.radius +
            'px;'
        )}
      >
        {!!dataSource.showNav && <TextNavModule dataSource={dataSource.textNavSource}></TextNavModule>}
        <View className='ebooks-module-container'>
          <ScrollView scrollX>
            <View className='ebooks-container' style={_safe_style_('padding-left: ' + (PADDING / 2) + 'px')}>
              {renderList.map((item, index) => {
                return (
                  <View
                    className='ebooks-item'
                    onClick={_fixme_with_dataset_(this.handleGoToDetail, { item: item })}
                    key={index}
                  >
                    <View
                      className='item-img'
                      style={_safe_style_(
                        'background: transparent url(' + item.coverUrl + ') no-repeat center center / cover'
                      )}
                    ></View>
                    <View className='item-name'>{item.title}</View>
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default BaseEbooksModule;
