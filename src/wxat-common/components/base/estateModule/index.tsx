import { _safe_style_ } from '@/wk-taro-platform';
import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
// components/goods-module/index.js
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import handleData from '../utils/handleData.js';
import report from '@/sdks/buried/report/index.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';

import GoodsItem from '../../industry-decorate-style/goods-item/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';

class EstateModule extends React.Component {

  state = {
    renderList: null,
    PADDING: '0',
    defaultReportSource: reportConstants.SOURCE_TYPE.product.key,
    itemClass: null,
    containerClass: null,
  }

  componentDidMount() {
    if (!this.props.list && this.props.dataSource) {
      this.getList();
    }
    const itemClass = this.getClassByShowType('item');
    const containerClass = this.getClassByShowType('container');

    this.setState({
      itemClass: itemClass,
      containerClass: containerClass,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list) {
      let obj = {
        display: this.props.display || 'vertical',
        type: 'product',
        data: this.props.list,
      };

      this.setState({
        renderList: obj,
      });
    }
  }

  getClassByShowType = (type) => {
    const showType = new Map([
      ['vertical', 'vertical-' + type],
      ['horizon', 'horizon-' + type],
      ['oneRowOne', 'oneRowOne-' + type],
      ['oneRowTwo', 'oneRowTwo-' + type],
      ['oneRowThree', 'oneRowThree-' + type],
    ]);

    const display = this.props.dataSource && this.props.dataSource.display;
    return showType.get(display || this.props.display);
  }

  getList() {
    const data = this.props.dataSource.data;
    if (data && data.length > 0) {
      let itemNos = handleData.pickUpItemNos(data, 'itemNo').join(',');
      let params = {
        itemNoList: itemNos,
      };

      wxApi
        .request({
          url: api.classify.itemSkuList,
          data: params,
        })
        .then((res) => {
          let obj = {};
          for (const key in this.props.dataSource) {
            if (this.props.dataSource.hasOwnProperty(key)) {
              if (key === 'data') {
                obj[key] = res.data;
                const goodsExposure = [];
                (obj[key] || []).forEach((item) => {
                  if (!!item.wxItem.itemNo) {
                    goodsExposure.push(item.wxItem.itemNo);
                  }
                });
                if (!!goodsExposure && !!goodsExposure.length) {
                  report.goodsExposure(goodsExposure.join(','));
                }
              } else {
                obj[key] = this.props.dataSource[key];
              }
            }
          }

          if (!this.state.dataSource.hasOwnProperty('display')) {
            const showRectType = parseInt(this.state.dataSource.showRectType);
            if (showRectType === 1) {
              obj.display = 'vertical';
            } else if (showRectType === 1) {
              obj.display = 'oneRowTwo';
            }
          }

          obj.type = 'product';

          this.setState({
            renderList: obj,
          });
        })
        .catch((error) => {
          console.log('get product: error: ' + JSON.stringify(error));
        });
    }
  }

  onItemClick = (e) => {
    // this.triggerEvent('click', e.detail);
  }

  render() {
    const { dataSource, renderList, reportSource, PADDING, containerClass, itemClass, defaultReportSource } = this.data;
    return (
      renderList.data &&
      renderList.data.length && (
        <View
          data-scoped="wk-wcbe-EstateModule"
          className="wk-wcbe-EstateModule product-module"
          style={_safe_style_(
            'padding:' +
              ((dataSource.marginUpDown * 2 || 20) + 'rpx ' + (dataSource.marginLeftRight * 2 || 20) + 'rpx') +
              ';border-radius: ' +
              (dataSource.radius * 2 || 0) +
              'rpx;background: ' +
              (dataSource.backgroundColor || (renderList.display === 'vertical' ? 'rgba(255,255,255,1)' : ''))
          )}
        >
          {dataSource.showNav && (
            <TextNavModule
              reportSource={reportSource}
              dataSource={dataSource.textNavSource}
              padding={PADDING}
              display={renderList.display}
            ></TextNavModule>
          )}

          <View className={containerClass}>
            {renderList.data &&
              renderList.data.map((item, index) => {
                return (
                  <GoodsItem
                    className={itemClass}
                    key={item.index}
                    goodsItem={item}
                    onClick={this.onItemClick}
                    reportSource={reportSource ? reportSource : defaultReportSource}
                    display={renderList.display}
                  ></GoodsItem>
                );
              })}
          </View>
        </View>
      )
    );
  }
}

export default EstateModule;
