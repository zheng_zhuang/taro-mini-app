import {  WithPageLifecycles, _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { Block, View, Image, Input, ScrollView, Text, RichText } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import filters from '@/wxat-common/utils/money.wxs.js';
import goodsTypeEnum from '@/wxat-common/constants/goodsTypeEnum.js';
import util from '@/wxat-common/utils/util.js';
import imageUtils from '@/wxat-common/utils/image.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
// import state from '@/state/index.js';
import Empty from '@/wxat-common/components/empty/empty';
import ChooseAttributeDialog from '@/wxat-common/components/choose-attribute-dialog/index';
import store from '@/store';
import './index.scss';
const storeData = store.getState();
const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};

const mapStateToProps = (state) => {
  return {
    currentStore: state.base.currentStore,
    roomCode: state.globalData.roomCode,
    roomCodeTypeId: state.globalData.roomCodeTypeId,
    roomCodeTypeName: state.globalData.roomCodeTypeName,
    trueCode: state.globalData.roomCodeTypeName + '-' + state.globalData.roomCode,
  };
};
interface ComponentProps {
  dataSource: { data: null };
  isComponent?: boolean;
  optionConfig?: string;
  currentStore?: any;
  roomCode?: any;
  roomCodeTypeId?: any;
  roomCodeTypeName?: any;
  trueCode?: any;
}

interface ComponentState {
  template: any;
  storeServiceList: never[];
  serviceCateGoryIdList: any;
  serverStoreList: [{ count: any }];
  parentIdList: any;
  currentIndex: number | string;
  hasMore: boolean;
  totalPrice: number;
  formBody: [{ value: any }];
  formItem: object;

  tmpStyle: {};
  fircurrentIndex: number;
  seccurrentIndex: number;
  firstLevelList: never[];
  serviceCateGoryId: null;
  secondid: null;
  categoryLevel2: never[];
  dataSourceList: never[];
  pageNo: number;
  Height: any;

}

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@WithPageLifecycles
class ServiceDistributionList extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    dataSource: null,
  };

  state = {
    tmpStyle: {}, // 主题模板配置
    fircurrentIndex: 0, // 默认选中一级分类的第一个
    seccurrentIndex: 0, // 默认选中二级分类的第一个
    firstLevelList: [], // 一级分类列表
    serviceCateGoryId: null, // 一级列表的id
    secondid: null, // 二级列表的id
    categoryLevel2: [], // 二级分类列表
    dataSourceList: [], // 商品列表
    pageNo: 1, // 商品分页
    hasMore: true, // 是否有更多商品数据
    totalCount: 0,
    goodsDetail: null,
    serviceCateGoryIdList: '',
    Height: null,
    firstShow: false,
    chooseAttributeDialogNode: React.createRef()
  };

  componentDidMount(): void {
    // 解析二维码参数
    const that = this;
    that.computedCount();
    that.getTemplateStyle();
    that.getPcProductList();
    Taro.getSystemInfo({
      success: function (res) {
        const clientHeight = res.windowHeight;
        const clientWidth = res.windowWidth;
        const changeHeight = 750 / clientWidth;
        const height = clientHeight * changeHeight;
        that.setState({
          Height: height,
        });
      },
    });

    if (!this.state.firstShow) {
      // 如果组件是加载，TODO：首次加载两次bug
      this.setState({
        firstShow: true
      })
      that.getFirstLevelList();
    }

  }

  componentDidShow () {
      // 页面被展示
      const that = this;
      that.getTemplateStyle();
      that.getPcProductList();
      that.computedCount();
      that.setState({
        fircurrentIndex: 0,
        seccurrentIndex: 0,
      });
      that.getFirstLevelList();
  }

    // 获取pc配置的商品id
    getPcProductList() {
      const that = this;
      const serviceCateGoryIdList = that.props.dataSource.data ? that.props.dataSource.data : [];

      if (serviceCateGoryIdList) {
        const idsList = [];
        const parentIdList = [];
        const resList = serviceCateGoryIdList.filter((num) => {
          return parseInt(num.parentId) > 0;
        });

        serviceCateGoryIdList.forEach((item) => {
          parentIdList.push(item.id);
          idsList.push(item.id);
          if (resList && resList.length != serviceCateGoryIdList.length) {
            if (item.parentId != 0) {
              idsList.push(item.parentId);
            }
          }
        });
        const newidsList = idsList.join(',');
        const newparentIdList = parentIdList.join(',');

        this.setState({
          // serviceCateGoryIdList :"1000000052"
          serviceCateGoryIdList: newidsList,
          parentIdList: newparentIdList,
        });
      } else {
        this.setState({
          serviceCateGoryIdList: '',
          parentIdList: '',
        });
      }
    }

    // 计算初始count
    computedCount() {
      const current = Taro.getStorageSync('storageList') || [];
      let count = 0;
      current.forEach((item) => {
        count = item.count + count;
      });
      this.setState({
        totalCount: count,
      });
    }

    minTotalCount() {
      this.setState({
        totalCount: this.state.totalCount - 1,
      });
    }

    addTotalCount() {
      this.setState({
        totalCount: this.state.totalCount + 1,
      });
    }

    // sku 商品
    gosku(e) {
      const item = e.currentTarget.dataset.item;
      item.categoryType = 5;
      this.selectComponent('#sku-dialog').show(item);
      // this.skuDialog =
      //  this.skuDialog.show(item);
    }

    // 获取主题色
    getTemplateStyle() {
      const templateStyle = template.getTemplateStyle();
      this.setState({
        tmpStyle: templateStyle,
      });
    }

    // 获取配送列表,一级菜单
    getFirstLevelList() {
      wxApi
        .request({
          // url: api.livingService.getFirstLevelList,
          url: api.livingService.getNewFir,
          method: 'GET',
          loading: true,
          data: {
            serviceCateGoryIdList: this.state.serviceCateGoryIdList,
            categoryType: 5,
          },
        })
        .then((res) => {
          const firstLevelList = res.data || [];
          if (firstLevelList.length == 0) return;
          const serviceCateGoryId = res.data && res.data[0].id;
          this.setState({
            firstLevelList,
            serviceCateGoryId,
          },()=>{
            this.getLevel2Data();
          });
        });
    }

    // 查询二级分类
    getLevel2Data() {
      wxApi
        .request({
          url: api.livingService.getNewSec,
          method: 'GET',
          loading: true,
          data: {
            // serviceCateGoryIdList:"",
            parentId: this.state.serviceCateGoryId,
            storeId: this.props.currentStore.id,
            categoryType: 5,
          },
        })
        .then((res) => {
          // let categoryLevel2 = res.data.categoryLevel2 || [];
          const categoryLevel2 = res.data || [];
          if (!!categoryLevel2 && categoryLevel2.length > 0) {
            const secondid = categoryLevel2[0].id;
            // this.queryItemSku(res.data.categoryLevel2[0].id)
            this.queryItemSku(res.data[0].id);
            this.setState({
              categoryLevel2: this.flatterArray(categoryLevel2),
              secondid,
            });
          } else {
            this.setState({
              categoryLevel2,
            });

            // 没有子分类  展示商品数据
            this.queryItemSku();
          }
        })
        .catch((error) => {});
    }

    flatterArray(arr) {
      let x = [];
      arr.forEach((item) => {
        if (item.childrenCategory && item.childrenCategory.length) {
          x = x.concat(this.flatterArray(item.childrenCategory));
        } else {
          x.push(item);
        }
      });
      return x;
    }

    /* 查询分类中的子数据
     */
    queryItemSku(id?:any) {

      const categoryId = id || this.state.firstLevelList[this.state.fircurrentIndex].id;

      const params = {
        status: 1,
        //  pageNo: this.state.pageNo,
        //  pageSize: 10,
        deliveryCategoryId: categoryId,
        sourceType: 1,
      };
      params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];

      wxApi
        .request({
          url: api.classify.itemSkuList,
          loading: true,
          data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
        })
        .then((res) => {
          if (res.success === true) {
            let hasMore = true;
            let pageNo = this.state.pageNo;
            let dataSourceList;
            const resData = res.data || [];
            resData.forEach((item) => {
              const thumbnail = item.thumbnail || (item.wxItem ? item.wxItem.thumbnail : '');
              item.thumbnailMin = imageUtils.thumbnailMin(thumbnail);
              item.thumbnailMid = imageUtils.thumbnailMid(thumbnail);
              item.count = 0;
              //  if(!!item.skuInfoList && item.skuInfoList.length){
              //   item.skuInfoList.forEach((val)=>{
              //     val.itemCount = 0;
              //     val.skuInfoNames.forEach((keys)=>{
              //      keys.itemCount = 0;
              //     })
              //  })
              //  }
            });

            if (this.isLoadMoreRequest()) {
              dataSourceList = this.state.dataSourceList.concat(resData);
            } else {
              dataSourceList = resData;
            }
            if (dataSourceList.length === res.totalCount) {
              hasMore = false;
            } else {
              pageNo += 1;
            }

            this.calcCartNumber(dataSourceList);

            this.setState({
              dataSourceList,
              showType: 0,
              hasMore,
              pageNo,
            });

            const goodsExposure = [];
            resData.forEach((item) => {
              let itemNo;
              if (!!(itemNo = item.wxItem) && !!(itemNo = itemNo.itemNo)) {
                goodsExposure.push(itemNo);
              }
            });
            if (!!goodsExposure && !!goodsExposure.length) {
              report.goodsExposure(goodsExposure.join(','));
            }
          }
        })
        .catch((error) => {
          if (this.isLoadMoreRequest()) {
            this.setState({
              loadMoreStatus: loadMoreStatus.ERROR || '',
            });
          }
        });
    }

    // 计算商品在购物车添加的数量
    calcCartNumber(data) {
      const dataSourceList = data || this.state.dataSourceList;
      const list = Taro.getStorageSync('storageList') || [];
      if (list.length > 0) {
        dataSourceList.forEach((item) => {
          list.forEach((value) => {
            if (item.wxItem.itemNo == value.wxItem.itemNo) {
              item.count += value.count;
            }
          });
        });
      }
      this.setState({
        dataSourceList,
      });
    }

    // 计算多规格商品在购物车添加的数量
    skucalcCartNumber(data) {
      const dataSourceList = data || this.state.dataSourceList;
      const list = Taro.getStorageSync('storageList') || [];
      if (list.length > 0) {
        dataSourceList.forEach((item) => {
          item.count = 0;
          list.forEach((value) => {
            if (item.wxItem.itemNo == value.wxItem.itemNo) {
              item.count += value.count;
            }
          });
        });
      }
      this.setState({
        dataSourceList,
      });
    }

    isLoadMoreRequest() {
      return this.state.pageNo > 1;
    }

    clickPlus=(e)=> {
      e.stopPropagation();
      const index = e.currentTarget.dataset.index;
      let dataSourceList = this.state.dataSourceList;
      let itemCount = dataSourceList[index].count;
      itemCount = itemCount + 1;

      dataSourceList[index].count = itemCount;
      this.setState({
        dataSourceList: dataSourceList,
      });

      this.addTotalCount();

      //  this.setProduct();
      // 优先获取判断再缓存
      const data = {
        ...this.state.dataSourceList[index],
        count: 1,
      };

      const current = Taro.getStorageSync('storageList');
      let newArr = [];
      if (current && current.length) {
        let item = null;
        if (this.state.dataSourceList[index].skuId) {
          item = current.find((i) => i.skuId === this.state.dataSourceList[index].skuId);
        } else {
          item = current.find((i) => i.wxItem.itemNo === this.state.dataSourceList[index].wxItem.itemNo);
        }
        if (item) {
          item.count = item.count + 1;
          newArr = current;
        } else {
          newArr = [data, ...current];
        }
      } else {
        newArr = [data, ...current];
      }

      Taro.setStorageSync('storageList', newArr);
      Taro.setStorageSync('storageOrderList', newArr); // 下单缓存
    }

    clickSkuPlus = (e, item) => {
      e.stopPropagation();
      item.wxItem.skuType = 'skuList';
      item.wxItem.thumbnail = item.thumbnailMin
      item.wxItem.tenantPrice = item.tenantPrice;
      item.categoryType = 5;
      this.setState(
        {
          goodsDetail: item,
        },
        () => {
          this.showAttrDialog()
        }
      )
    }

    // 购物车减少
    clickMinus=(e)=> {
      e.stopPropagation();
      this.minTotalCount();
      const currentItem = e.currentTarget.dataset.item;
      const index = e.currentTarget.dataset.index; //
      const dataSourceList = this.state.dataSourceList;
      let itemCount = dataSourceList[index].count;
      // if(itemCount < 1){
      //   return false;
      // }
      itemCount = itemCount - 1;
      dataSourceList[index].count = itemCount;
      this.setState({
        dataSourceList: dataSourceList,
      });

      const current = Taro.getStorageSync('storageList');
      let newArr = [];
      if (current && current.length) {
        let item = null;
        let currentIdx = null; // 缓存商品的当前点击id 的index
        if (this.state.dataSourceList[index].skuId) {
          item = current.find((i, iIndex) => {
            currentIdx = iIndex;
            return i.skuId === this.state.dataSourceList[index].skuId;
          });
        } else {
          item = current.find((i, iIndex) => {
            currentIdx = iIndex;
            return i.wxItem.itemNo === this.state.dataSourceList[index].wxItem.itemNo;
          });
        }
        if (item) {
          item.count = item.count - 1;
          current.splice(currentIdx, 1);
          newArr = current;
        } else {
          newArr = [currentItem, ...current];
        }
      } else {
        newArr = [currentItem, ...current];
      }

      Taro.setStorageSync('storageList', newArr);
      Taro.setStorageSync('storageOrderList', newArr); // 下单缓存
    }

    /**
     * 点击左边的分类按钮
     */
    clickClassify=(value)=> {
      const id = parseInt(value.currentTarget.id);
      const serviceCateGoryId = parseInt(value.target.dataset.serviceCateGoryId);

      if (this.state.fircurrentIndex === id) return;
      this.setState({
        fircurrentIndex: id,
        seccurrentIndex: 0,
        serviceCateGoryId,
        pageNo: 1,
        hasMore: true,
      },()=>{
        this.getLevel2Data();
      });
    }

    // 点击二级分类按钮
    clicksecondClassify=(value)=> {
      const id = parseInt(value.currentTarget.id);
      const secondId = parseInt(value.currentTarget.dataset.secondId);
      if (this.state.seccurrentIndex === id) return;
      this.setState({
        seccurrentIndex: id,
        pageNo: 1,
      },()=>{
        this.queryItemSku(secondId);
      });
    }

    // 存数据
    // setProduct(){
    //   wx.setStorageSync('storageList', storageList)
    // },
    // 跳转去购物清单
    goServerList() {
      wxApi.$navigateTo({
        url: 'sub-packages/marketing-package/pages/living-service/service-reserve-list/index',
      });
    }

    /**
     * 跳转到配送服务下单页面
     * 2021-04-21 加入新逻辑，通过scan字段判断是否需要扫描二维码
     */
    async goServerDeliverDetail() {
      // 未进行认证则优先认证
      if (!authHelper.checkAuth()) return;
      // 将缓存中的storageList优先提取出来，避免重复提取，以优化代码
      const storageList = Taro.getStorageSync('storageList');
      // 需要有商品才可以下单
      if (!storageList || storageList.length === 0) {
        Taro.showToast({
          title: '请选择商品',
          icon: 'none',
          duration: 2000, // 持续的时间
        });
        return;
      }
      // 判断是否包含需要扫码的商品
      const flag = await util.checkNeedScan(storageList);
      if (!flag) {
        wxApi.$navigateTo({
          url: '/sub-packages/order-package/pages/pay-order/index',
          data: {
            categoryType: 5,
            title: '确认订单',
          },
        });

        return;
      }

      // 循环生成格式缓存，然后扫码
    const serverGoodsList = storageList.map(item => {
      if (item.itemNo) return item
      const { wxItem } = item
      const _item = {
        itemNo: wxItem.itemNo,
        barcode: wxItem.barcode,
        pic: wxItem.thumbnail,
        name: wxItem.name,
        noNeedPay: wxItem.noNeedPay,
        freight: wxItem.freight,
        wxItem: wxItem,
        itemCount: item.count,
        count: item.count,
        salePrice: wxItem.salePrice,
      }
      return _item
    })

      // 获取从商品详情添加的商品
      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', serverGoodsList);
      Taro.setStorageSync('GOODS_LIST_CUT', JSON.stringify(serverGoodsList))
      window.sessionStorage.setItem('GOODS_LIST_CUT', JSON.stringify(serverGoodsList))
      if (this.props.roomCode) {
        wxApi.$navigateTo({
          url: '/sub-packages/order-package/pages/pay-order/index',
          data: {
            roomCode: this.props.roomCode,
            roomCodeTypeId: this.props.roomCodeTypeId,
            roomCodeTypeName: this.props.roomCodeTypeName,
            trueCode: this.props.roomCodeTypeName + '-' + this.props.roomCode,
            categoryType: 5,
            title: '确认订单',
          },
        });
      } else {
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
          data: {
            isDistributionServices: true
          }
        })
      }
    }

    /* 跳转到配送服务商品订单详情
     */
    goGoodsDetail=(e)=> {
      if (!authHelper.checkAuth()) return;
      wxApi.$navigateTo({
        url: '/wxat-common/pages/goods-detail/index',
        data: {
          itemNo: e.currentTarget.dataset.item.wxItem.itemNo,
          categoryType: 5,
        },
      });
    }

    //  /**
    //  * 跳转购物车页面
    //  */
    //   handlerToCart() {
    //     this.showAttrDialog(); // 显示商品属性对话弹框
    //     // 设置当前操作为加入购物车
    //     this.setState({
    //       'chooseAttrForCart': true
    //     })
    //   },
    // 多规格加入缓存
    handlerChooseSku = (info) => {
      const thumbnail = info.thumbnail || (info.wxItem ? info.wxItem.thumbnail : '');
      const data = {
        ...info,
        count: 1,
        scan: this.state.goodsDetail.scan,
        thumbnailMin: imageUtils.thumbnailMin(thumbnail),
        thumbnailMid: imageUtils.thumbnailMin(thumbnail),
        skuInfo: info.skuTreeNames,
      };

      const current = Taro.getStorageSync('storageList');
      let newArr = [];
      if (current && current.length) {
        let item = null;
        if (info.skuId) {
          item = current.find((i) => i.skuId === info.skuId);
        } else {
          item = current.find((i) => i.wxItem.itemNo === info.wxItem.itemNo);
        }
        if (item) {
          item.count = item.count += info.itemCount;
          newArr = current;
        } else {
          data.count = info.itemCount;
          newArr = [data, ...current];
        }
      } else {
        data.count = info.itemCount;
        newArr = [data, ...current];
      }

      Taro.setStorageSync('storageList', newArr);
      Taro.setStorageSync('storageOrderList', newArr); // 下单缓存

      this.computedCount();
      this.skucalcCartNumber();
      this.hideAttrDialog();
    }

    /**
     * 显示商品属性对话弹框
     */
    showAttrDialog() {
      this.state.chooseAttributeDialogNode.current.showAttributDialog();
    }

    /**
     * 隐藏商品属性对话弹框
     */
    hideAttrDialog() {
      this.state.chooseAttributeDialogNode.current.hideAttributeDialog();
    }


  render() {
    const {
      Height,
      fircurrentIndex,
      tmpStyle,
      firstLevelList,
      seccurrentIndex,
      categoryLevel2,
      dataSourceList,
      totalCount,
      limitAmountTitle,
      goodsDetail,
    } = this.state;

    const { isComponent, dataSource } = this.props;

    // TODO: render()

    return (
      <View
        data-scoped="wk-wcbs-ServiceDistributionList"
        data-fixme="01 className existed"
        className={'wk-wcbs-ServiceDistributionList ' + ('classify ' + (isComponent ? '' : 'indexClassify'))}
        style={dataSource ?_safe_style_(
          'margin:' +
            (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
            ';height:' +
            ((isComponent ? '' : Height) / 2) +
            'px;'
        ) : null}
      >
        <View className="content">
          {/* 一级分类 */}
          <View
            className={'left ' + (isComponent ? '' : 'indexLeft')}
            style={_safe_style_('height:' + ((isComponent ? '' : Height) / 2) + 'px;')}
          >
            {firstLevelList &&
              firstLevelList.map((item, index) => {
                return (
                  <View
                    className="left-item"
                    key={Math.random() + index}
                    id={index}
                    style={ tmpStyle ? _safe_style_(
                      'color:' +
                        (fircurrentIndex === index ? tmpStyle.btnColor : 'rgba(162,162,164,1)') +
                        ';background-color:' +
                        (fircurrentIndex === index ? 'rgba(255,255,255,1)' : 'rgba(240,241,245,1)') +
                        ';padding: 20px 0'
                    ): null}
                    onClick={_fixme_with_dataset_(this.clickClassify, { serviceCateGoryId: item.id })}
                  >
                    <Text
                      className="category-item limit-line line-2"
                      style={_safe_style_(
                        'color:' +
                          (fircurrentIndex === index ? tmpStyle.btnColor : '') +
                          '; font-weight:' +
                          (fircurrentIndex === index ? 600 : '')
                      )}
                    >
                      {item.categoryName}
                    </Text>
                  </View>
                );
              })}
          </View>

          {/* 二级分类 */}
          <View
            className={'right ' + (isComponent ? '' : 'indexRight')}
          >
            <View className="content-container">
              {categoryLevel2 && categoryLevel2.length > 0 && (
                <ScrollView className="secondary-classification" scrollX="true">
                  {categoryLevel2.length > 0 &&
                    categoryLevel2.map((item, index) => {
                      return (
                        <View
                          className={'secondary-classification-item ' + (seccurrentIndex === index ? 'cur' : '')}
                          key={Math.random() + index}
                          id={index}
                          onClick={_fixme_with_dataset_(this.clicksecondClassify, {
                            secondId: item.id,
                            secondId: item.id,
                          })}
                          style={_safe_style_(
                            seccurrentIndex === index
                              ? 'color:' + tmpStyle.btnColor + ';border-color:' + tmpStyle.btnColor
                              : ''
                          )}
                        >
                          {item.categoryName}
                        </View>
                      );
                    })}
                </ScrollView>
              )}

              {/* 服务事项展示模式  */}
              {!!dataSourceList && (
                <Block>
                  {dataSourceList &&
                    dataSourceList.map((item, index) => {
                      return (
                        <View key={Math.random() + index} className="child-item">
                          <View
                            className="service-item"
                            onClick={_fixme_with_dataset_(this.goGoodsDetail, { item: item, item: item })}
                          >
                            <Image className="service-itemimg" src={item.thumbnailMin} mode="aspectFill"></Image>
                            <View className="serviceBox">
                              <View className="titlebox">
                                <Text className="serviceNname">{item.wxItem.name}</Text>
                              </View>
                              {/*  <text class="subtitle">配送服务的事项东西订单的地方的副标题</text>  */}
                              <View className="bottom">
                                <View className="bottomBox">
                                  <View>
                                    <View className="servicePrice">
                                      ￥<Text>{filters.moneyFilter(item.wxItem.salePrice || 0, true)}</Text>
                                      <Text className="label-discount">
                                        {'￥' + filters.moneyFilter(item.wxItem.labelPrice || 0, true)}
                                      </Text>
                                    </View>
                                    <View className="h-goods-desc">
                                      {(item.itemSalesVolume || item.wxItem.itemSalesVolume || 0) + '人已购买'}
                                    </View>
                                  </View>
                                  {!item.skuInfoList.length ? (
                                    <View>
                                      {item.count > 0 && (
                                        <View className="count-box">
                                          <View
                                            className="minus left-radius"
                                            onClick={_fixme_with_dataset_(
                                              this.clickMinus,
                                              { index: index, item: item }
                                            )}
                                          >
                                            -
                                          </View>
                                          <View className="num">
                                            <Input
                                              className="input-comp"
                                              type="number"
                                              value={item.count}
                                              disabled
                                            ></Input>
                                          </View>
                                          <View
                                            className="plus right-radius"
                                            onClick={_fixme_with_dataset_(
                                              this.clickPlus,
                                              { index: index }
                                            )}
                                          >
                                            +
                                          </View>
                                        </View>
                                      )}

                                      {/*  <View class="count-box" wx:if="{{item.count > 0}}">
                                           <image class="minus"
                                           src="https://front-end-1302979015.file.myqcloud.com/images/c/images/plugin/minus.png"
                                            catchtap="clickMinus" data-index="{{index}}"></image>
                                         <View class="num" >{{item.count}}</View>
                                         <image class="plus"
                                           src="https://front-end-1302979015.file.myqcloud.com/images/c/images/plugin/plus.png"
                                            catchtap="clickPlus" data-index="{{index}}"></image>
                                         </View>  */}
                                      {/* fixme 保留库存不足的逻辑  {{(formatSpuItem.stock < 1) ? 'disabled' : ''}} */}
                                      {item.count == 0 && (
                                        <View
                                          className="add-img"
                                          onClick={_fixme_with_dataset_(
                                            this.clickPlus,
                                            { index: index }
                                          )}
                                        >
                                          <Image
                                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                                            className="add-cart-icon"
                                            mode="aspectFill"
                                          ></Image>
                                          {/*  <text>添加</text>  */}
                                        </View>
                                      )}
                                    </View>
                                  ) : (
                                    <View>
                                      <View
                                        className="add-img"
                                        onClick={(e) => {this.clickSkuPlus(e, item)}}
                                      >
                                        <Image
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                                          className="add-cart-icon"
                                          mode="aspectFill"
                                        ></Image>
                                        {item.count > 0 && <View className="cartCount">{item.count}</View>}
                                      </View>
                                    </View>
                                  )}
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      );
                    })}
                </Block>
              )}

              {!!dataSourceList && dataSourceList.length == 0 && (
                <Empty emptyStyle={_safe_style_('width:auto')} message="敬请期待..."></Empty>
              )}
            </View>
          </View>

          {/* 底部加购栏 */}
          <View className={isComponent ? 'bottom_box' : 'indexBottom_box'}>
            <View className="iconBox" onClick={this.goServerList}>
              <View className="cart-img-box" style={_safe_style_('background:' + tmpStyle.btnColor)}>
                <Image
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                  className="cart-imgiocn"
                ></Image>
              </View>
              {totalCount > 0 && <View className="cartCount">{totalCount}</View>}
            </View>
            <View
              className="bottom_submite"
              style={_safe_style_('background:' + tmpStyle.btnColor + ';')}
              onClick={this.goServerDeliverDetail.bind(this)}
            >
              立即下单
            </View>
          </View>
        </View>
        {/*  选择商品属性对话弹框  */}
        {goodsDetail && (
          <ChooseAttributeDialog
            id="choose-attribute-dialog"
            ref={this.state.chooseAttributeDialogNode}
            isDistributionServices
            onChoose={this.handlerChooseSku}
            limitAmountTitle={limitAmountTitle}
            goodsDetail={goodsDetail}
          ></ChooseAttributeDialog>
        )}

        {/*  <sku-dialog id="sku-dialog"
           bind:addTotalCount="addTotalCount"
           bind:minTotalCount="minTotalCount"></sku-dialog>  */}
      </View>
    );
  }
}

export default ServiceDistributionList;
