import {  WithPageLifecycles, _fixme_with_dataset_, _safe_style_, $getRouter } from '@/wk-taro-platform';
import {
  View,
  Image,
  Input,
  ScrollView,
  Text,
  Textarea,
  RadioGroup,
  CheckboxGroup,
  Picker,
  Label,
  Radio,
  Checkbox } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import filters from '@/wxat-common/utils/money.wxs.js';
import util from '@/wxat-common/utils/util.js';
import template from '@/wxat-common/utils/template.js';
import authHelper from '@/wxat-common/utils/auth-helper.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import store from '@/store';
import Empty from '@/wxat-common/components/empty/empty';
import AnimatDialog from '@/wxat-common/components/animat-dialog/index';
import RegionPicker from '@/wxat-common/components/region-picker/index'
import './index.scss';
import UploadImg from "@/wxat-common/components/upload-img";
import {getFreeLabel} from '@/wxat-common/utils/service'

const storeData = store.getState();


const rules = {
  phone: {
    reg: /^1[3|4|5|6|8|7|9][0-9]\d{8}$/,
    msg: '无效的手机号码',
  },

  email: {
    reg: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    msg: '邮箱格式不正确',
  },
};
const mapStateToProps = (state) => {
  return {
    currentStore: state.base.currentStore,
    roomCode: state.globalData.roomCode,
    roomCodeTypeId: state.globalData.roomCodeTypeId,
    roomCodeTypeName: state.globalData.roomCodeTypeName,
    trueCode: state.globalData.roomCodeTypeName + '-' + state.globalData.roomCode,
  };
};
interface ComponentProps {
  dataSource: { data: null };
  isComponent?: boolean;
  optionConfig?: string;
  currentStore?: any;
  roomCode?: any;
  roomCodeTypeId?: any;
  roomCodeTypeName?: any;
  trueCode?: any;
}

interface ComponentState {
  template: any;
  storeServiceList: never[];
  serviceCateGoryIdList: any;
  serverStoreList: [{ count: any }];
  parentIdList: any;
  Height: any;
  currentIndex: number | string;
  tmpStyle: any;
  firstLevelList: any;
  serviceCateGoryId: any;
  hasMore: boolean;
  pageNo: number;
  totalPrice: number;
  categoryLevel2: any;
  seccurrentIndex: number;
  formBody: [{ value: any }];
  formItem: object;
  formItemCount:number
}
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@WithPageLifecycles
class ServiceClassificationTree extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    dataSource: null,
  };

  $router = $getRouter();

  state = {
    template: {},
    storeServiceList: [],
    storeServiceCategoryId: null,
    sourceType: null,
    currentIndex: 0,
    totalPrice: 0,
    seccurrentIndex: 0,
    firstLevelList: [], // 一级列表
    parentIdList: [], // 二级分类id
    serviceCateGoryId: null, // 一级列表的id
    serverStoreList: [], // 下级服务事项列表
    categoryLevel2: [], // 下级服务分类列表
    pageNo: 1,
    hasMore: true,
    formBody: [],
    formItem: {
      img: [],
    },

    showDialog: false, // 是否展示弹窗
    count: '',
    Height: null,
    currentCount: 1,
    animationData: {},
    // roomCode: '',
    // roomCodeTypeId: '',
    // roomCodeTypeName: '',
    tmpStyle: {
      btnColor: ''
    },
    serviceCateGoryIdList: '',
    firstShow: false,
    formItemCount:0
  };

  animatDialogRef = React.createRef<any>()

  componentDidMount(): void {
    // 解析二维码参数
    const that = this;

    wxApi.getSystemInfo({
      success: function (res) {
        const clientHeight = res.windowHeight;
        const clientWidth = res.windowWidth;
        const changeHeight = 750 / clientWidth;
        const height = clientHeight * changeHeight;
        that.setState({
          Height: height,
        });
      },
    });
    if (!this.state.firstShow) {
      // 如果组件是加载，TODO：首次加载两次bug
      this.setState({
        firstShow: true
      })
      const options = this.$router.params
      const serverStoreList = this.computedBtnIsShow(this.state.serverStoreList);
      this.count();
      this.setState({
        serverStoreList,
      });
      this.initGetData(options);
    }
  }

  componentDidShow() {
    if (this.state.firstShow) {
      // 如果组件是显示
      const options = this.$router.params
      const serverStoreList = this.computedBtnIsShow(this.state.serverStoreList);
      this.count();
      this.setState({
        serverStoreList,
        currentIndex:0
      });
      this.initGetData(options);
    }
    // const options = this.$router.params
    // const serverStoreList = this.computedBtnIsShow(this.state.serverStoreList);
    // this.count();
    // this.setState({
    //   serverStoreList,
    // });
    // this.initGetData(options);
  }

  componentDidHide(){
    this.setState({
      pageNo: 1
    });
  };

  initGetData(options) {
    const optionConfig = options;
    let newidsList = ''
    let serviceCateGoryDataList;
    if (this.props.isComponent) {
      serviceCateGoryDataList = optionConfig.id ? JSON.parse(decodeURIComponent(optionConfig.id)) : [];
    } else {
      serviceCateGoryDataList = this.props.dataSource.data ? this.props.dataSource.data : [];
    }
    if (serviceCateGoryDataList.length > 0) {
      const idsList = [];
      const parentIdList = [];
      const resList = serviceCateGoryDataList.filter((num) => {
        return parseInt(num.parentId) > 0;
      });

      serviceCateGoryDataList.forEach((item) => {
        parentIdList.push(item.id);
        idsList.push(item.id);
        if (resList && resList.length != serviceCateGoryDataList.length) {
          if (item.parentId != 0) {
            idsList.push(item.parentId);
          }
        }
      });
      newidsList = idsList.join(',');
      const newparentIdList = parentIdList.join(',');
      this.setState({
        serviceCateGoryIdList: newidsList,
        parentIdList: newparentIdList,
      });
    } else {
      this.setState({
        serviceCateGoryIdList: '',
        parentIdList: '',
      });
    }

    this.getFirstLevelList(newidsList);
    this.getTemplateStyle();

    // 初始化动画到实例上
    this.animation = Taro.createAnimation({
      duration: 500,
      timingFunction: 'ease',
      delay: 0,
    });
  }

  // 跳转服务详情
  onGoServiceDetailPage=(e)=> {
    if (!authHelper.checkAuth()) return;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/living-service/service-detail/index',
      data: {
        id: e.currentTarget.dataset.item.id,
        sourceType: e.currentTarget.dataset.item.sourceType,
      },
    });
  }

  // 获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 跳转去服务详情
  goServiceDetails(e) {
    if (!authHelper.checkAuth()) return;
    const item = e.currentTarget.dataset.item;
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/living-service/goods-rental-details/index',
      data: {
        sourceType: 2,
        id: item.id,
        title: item.categoryName,
      },
    });
  }

  // 获取一级分类列表
  getFirstLevelList(serviceCateGoryIdList) {
    const params = {
      serviceCateGoryIdList,
      categoryType: 1,
    };
    wxApi
      .request({
        url: api.livingService.getNewFir,
        method: 'GET',
        loading: true,
        data: params,
      })
      .then((res) => {
        const firstLevelList = res.data || [];
        if (firstLevelList.length == 0) return;
        const serviceCateGoryId = res.data[0].id;
        this.setState({
          firstLevelList,
          serviceCateGoryId,
        }, () => {
          this.getLevel2Data();
        });
      });
  }

  // 查询一级分类下的分类或服务事项,二级分类
  getLevel2Data() {
    wxApi.request({
      url: api.livingService.getNewSec,
      method: 'GET',
      loading: true,
      data: {
        categoryType:1,
        parentId: this.state.serviceCateGoryId,
        storeId: storeData.base.currentStore.id,
      }
    }).then(async res => {
      let hasMore = true;
      let pageNo = this.state.pageNo;
      let serverStoreList;
      let categoryLevel2;
      const serverresData = res.data.serverStoreList || [];
      const categoryresData = res.data || [];
      if (this.state.pageNo > 1) {
        serverStoreList = this.state.serverStoreList.concat(serverresData);
        categoryLevel2 = this.state.concat(categoryresData);
      } else {
        serverStoreList = res.data.serverStoreList || [];
        categoryLevel2 = res.data || [];
      }
      if (categoryLevel2.length && !!categoryLevel2) {
        this.getPlatformList(categoryLevel2[0].id)
      }else{
        this.getPlatformList()
      }
      if (serverStoreList.length === res.totalCount || categoryLevel2.length === res.totalCount) {
        hasMore = false;
      } else {
        pageNo++;
      }
      serverStoreList = await this.computedBtnIsShow(serverStoreList);
      this.setState({
        hasMore,
        pageNo,
        serverStoreList,
        categoryLevel2
      })
    }).catch((error) => {

    });;
  }

  /**
   * 点击左边的分类按钮
   */
  clickClassify = (value) => {
    const id = parseInt(value.currentTarget.id);
    const serviceCateGoryId = parseInt(value.target.dataset.serviceCateGoryId);

    if (this.state.currentIndex === id) return;
    this.setState({
      currentIndex: id,
      serviceCateGoryId,
      pageNo: 1,
      hasMore: true,
      seccurrentIndex: 0,
    }, () => {
      this.getLevel2Data();
    });
  }

  // 点击二级分类按钮
  clicksecondClassify=(value)=> {
    const id = parseInt(value.currentTarget.id);
    const secondId = parseInt(value.currentTarget.dataset.secondId);
    if (this.state.seccurrentIndex === id) return;
    this.setState({
      seccurrentIndex: id,
      pageNo: 1,
      hasMore: true,
    }, () => {
      this.getPlatformList(secondId);
    });
  }

  // 获取二级分类下的服务事项列表
  getPlatformList(secondId?: any): void {
    wxApi.request({
      // url: api.livingService.getPlatformList,
      url: api.livingService.getNewSto,
      method: 'GET',
      loading: true,
      data: {
        // sourceType:this.state.sourceType,
        platformServiceCategoryId:secondId || this.state.serviceCateGoryId,
        // storeServiceCategoryId: secondId || this.state.serviceCateGoryId,
        storeId: storeData.base.currentStore.id,
      }
    }).then(async res => {
      let storeServiceList = res.data || [];
      // storeServiceList.forEach((item) => {
      //   item.widgetValue=[];
      // });
      //  storeServiceList.forEach(item => {
      //   item.imgOne = item.img.split(',')[0];
      // });
      storeServiceList = await this.computedBtnIsShow(storeServiceList);
      this.setState({
        serverStoreList: storeServiceList
      });
    });
  }

  async computedBtnIsShow(storeServiceList = []) {
    if (!Array.isArray(storeServiceList)) {
      return
    }
    return await new Promise((resolve, reject) => {
      const result = Taro.getStorageSync('serviceOrderList') || [];
      if (result && result.length) {
        const list = result.map((item) => {
          return JSON.parse(item);
        });
        storeServiceList.forEach((item) => {
          item.count = 0;
          const flag = list.find((e) => e.itemNo === item.itemNo);
          if (flag) {
            list.forEach((cItem) => {
              if (cItem.itemNo === item.itemNo) {
                item.count += cItem.count;
              }
            });
            item.isShowMark = item.formExt ? JSON.parse(item.formExt).widgetValue : [];
          } else {
            item.count = 0;
            item.isShowMark = item.formExt ? JSON.parse(item.formExt).widgetValue : [];
          }
        });
      } else {
        storeServiceList.forEach((item) => {
          item.count = 0;
          item.isShowMark = item.formExt ? JSON.parse(item.formExt).widgetValue : [];
        });
      }
      resolve(storeServiceList);
    });
  }

  inputValue = (e) => {
    const index = e.currentTarget.dataset.index;
    const value = e.detail.value;
    const formBody = this.state.formBody;
    formBody[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({ formBody });
  }

  onRegionPickerChange=(value,{index})=>{
    const formBody = this.state.formBody;
    formBody[index].value = Object.prototype.toString.call(value) === '[object String]' ? value.trim() : value;
    this.setState({ formBody });
  }

  // 单选修改
  changeRadioValue = (e) => {
    e.stopPropagation()
    const items = e.target.dataset.item.options
    const value = e.detail.value
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false
      if (items[i].value === value) {
        items[i].checked = true
      }
    }
    const formBody: any = this.state.formBody
    const index = e.target.dataset.index
    formBody[index].options = items
    formBody[index].value = value
    this.setState({
      formBody,
    })
  }

  bindValue = (e) => {
    e.stopPropagation()
    const items = e.target.dataset.item.options
    const values = e.detail.value
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false
      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].name === values[j]) {
          items[i].checked = true;
          break;
        }
      }
    }
    const formBody: any = this.state.formBody
    const index = e.target.dataset.index
    formBody[index].options = items
    this.setState({
      formBody,
    })
  }

  chooseGender = ({
    currentTarget: {
      dataset: { value, index },
    },
  }) => {
    const formBody = this.state.formBody;
    formBody[index].value = value;
    this.setState({
      formBody,
    });
  }

  checkForm(dataList) {
    let error = '';
    dataList.forEach((item) => {
      if (error) return;
      if (item.required) {
        if(item.type == "uploadImage"){
          if(!(item.value && item.value.length > 0)){
            error = item.label + '不能为空';
          }
        } else {
          if(!item.value){
            error = item.label + '不能为空';
          }
        }
      } else if (item.required && item.type == 'checkbox' && item.value.length == 0) {
        let count = item.options.filter( x => x.checked == true)
        if(count.length == 0 ){
          error = item.label + '不能为空';
        }
      }
      if (rules[item.type] && item.value && !rules[item.type].reg.test(item.value)) {
        error = rules[item.type].msg;
      }
    });
    return error;
  }

  arrMap=(arr)=>{
    const map = new Map()
    if(arr){
      arr.forEach((item)=>{
        const obj=JSON.parse(item)
        if(map.has(obj.id)){
          const mapObj = map.get(obj.id)
          mapObj.count = mapObj.count + obj.count
          map.set(obj.id,mapObj)
        }else{
          map.set(obj.id,obj)
        }
      })
    }
    return map
  }
  judgeStock=(list)=>{
    const arr = this.arrMap(list);
    if(arr.has(this.state.formItem.id)){
      const detail = arr.get(this.state.formItem.id)
      detail.count = detail.count + this.state.currentCount;
      if(detail.count > this.state.formItem.itemStock){
        return true
      }else{
        return false
      }
    }else{
      return false
    }
  }
  // 弹窗

  async addCart(e) {
    const formItem = e.currentTarget.dataset.item;
    let serviceTreeData = Taro.getStorageSync('serviceTreeData') ? Taro.getStorageSync('serviceTreeData') : [];
    const mapList = this.arrMap(serviceTreeData)
      if(mapList.has(formItem.id)){
        this.setState({
          formItemCount:mapList.get(formItem.id).count || 0
        })
      }else{
        this.setState({
          formItemCount:0
        })
      }
    if(formItem.useStock == 1 && !formItem.itemStock){
      wxApi.showToast({
        icon: "none",
        title:'库存不足'
      })
      return
    }
    if(formItem.useStock == 1 && formItem.itemStock <= formItem.count){
      return
    }
    let formExt;
    formExt = formItem.formExt ? JSON.parse(formItem.formExt).widgetValue : [];
    const formData = formExt.length ? formExt : [];
    if (formData.length === 0) {
      // 无表单类型直接添加
      this.ComputedCurrentCount(e.currentTarget.dataset.index, 1);
      // 无自定义表单

      this.setState({
        formItem,
        showDialog: false,
      }, () => {

        if (serviceTreeData.length) {
          let hasData = false; // 是否有重复数据
          for (let i = 0; i < serviceTreeData.length; i++) {
            const child = JSON.parse(serviceTreeData[i]);
            // if (child.id == this.state.formItem.id) {
            if (child.id == this.state.formItem.id) {
              // 无自定义表单
              // 有相同数据设为true并终止循环
              hasData = true;
              break;
            } else {
              // 没有相同数据设为false继续循环
              hasData = false;
            }
          }
          if (!hasData) {
            // 购物车无数据push
            this.state.formItem.count = 1;
            serviceTreeData.push(JSON.stringify(this.state.formItem));
          } else {
            // 已存在购物车中数量要加1
            const parseData = serviceTreeData.map((item) => {
              return JSON.parse(item);
            });

            parseData.forEach((item) => {
              if (item.id == this.state.formItem.id) {
                item.count++;
              }
            });
            serviceTreeData = parseData.map((item) => {
              return JSON.stringify(item);
            });
          }
          wxApi.setStorageSync('serviceTreeData', serviceTreeData);
          wxApi.setStorageSync('serviceOrderList', serviceTreeData);
        } else {
          // 第一次点击
          this.state.formItem.count = 1;
          serviceTreeData.push(JSON.stringify(this.state.formItem));
          wxApi.setStorageSync('serviceTreeData', serviceTreeData);
          wxApi.setStorageSync('serviceOrderList', serviceTreeData);
        }
        this.count();
        return;
      });
    } else {
      // 拉起弹窗
      this.animatDialogRef.current.show({
        translateY: 300,
      });
    }
    const newFormBody = [];
    formData.forEach((item, index) => {
      if (!item.value) {
        item.value = '';
      }
      if (item.type === 'address') {
        newFormBody.push({
          label: item.label,
          type: 'address',
          required: item.required,
          value: '',
          uuid: item.uuid
        });

        newFormBody.push({
          label: '详细地址',
          type: 'detail-address',
          required: false,
          value: '',
          uuid: item.uuid
        });
      } else if (item.type === 'phone') {
        newFormBody.push({
          label: item.phone ? item.phone : '手机号',
          type: item.type,
          required: item.required,
          value: item.value ? item.value : '',
          uuid: item.uuid
        });
      } else if (item.type === 'radio') {
        const radioArr = []
        item.options.forEach((el) => {
          const objArr = {
            value: el,
            text: el,
            checked: false
          }
          radioArr.push(objArr)
        })
        newFormBody.push({
          label: item.label,
          type: item.type,
          required: item.required,
          options: radioArr,
          value: item.value ? item.value : '',
          uuid: item.uuid
        });
      } else if (item.type == 'checkbox') {
        const arr = [];
        let keyName = [];
        item.options.forEach((child, index) => {
          if (typeof child === 'string') {
            const obj = {
              checked: false,
              key: index,
              name: child,
            };
            keyName = [];
            arr.push(obj);
          } else {
            const obj = {
              checked: child.checked,
              key: child.index,
              name: child.name,
            };
            keyName = item.value;
            arr.push(obj);
          }
        });
        newFormBody.push({
          label: item.label,
          type: item.type,
          required: item.required,
          options: arr,
          value: keyName,
          uuid: item.uuid
        });
      } else if (item.type === 'uploadImage') {
        newFormBody.push({
          label: item.label,
          max: item.max,
          type: item.type,
          required: item.required,
          value: item.value ? item.value : [],
          uuid: item.uuid
        });
      } else {
        newFormBody.push({
          label: item.label,
          type: item.type,
          required: item.required,
          value: item.value ? item.value : '',
          options: item.options,
          uuid: item.uuid
        });
      }
    });
    const serverStoreList = await this.computedBtnIsShow(this.state.serverStoreList);
    this.setState({
      formItem,
      formBody: JSON.parse(JSON.stringify(newFormBody)),
      serverStoreList
    });
  }

  // 提交表单
  async submiteCart() {
    // 隐藏添加按钮
    // this.changeCurrentHideState();
    const { formBody } = this.state;
    const serviceTreeData = wxApi.getStorageSync('serviceTreeData') ? wxApi.getStorageSync('serviceTreeData') : [];
      if(this.judgeStock(serviceTreeData) && this.state.formItem.useStock == 1){
        wxApi.showToast({
          icon: "none",
          title:'库存不足'
        })
        return
      }
    const error = this.checkForm(formBody);
    if (error) {
      Taro.showToast({ icon: 'none', title: error });
      return;
    }
    // 地址栏数据处理
    const list = [];
    formBody.forEach((item, index) => {
      let value = item.value || '';
      if (item.type === 'address') {
        const isNotEmptyArray = !!value && !!value.push;
        if (isNotEmptyArray) {
          if (value[0] === value[1]) {
            value = value.concat().splice(1).join('');
          } else {
            value = value.join('');
          }
        }
        value += formBody[index + 1].value || '';
        list.push({
          type: item.type,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: value || '',
        });
      } else if (item.type == 'checkbox') {
        // 初始化options数据格式
        const arr = [];
        let keyName = [];
        item.options.forEach((child, index) => {
          if (child.checked) {
            keyName.push(child.name)
          }
        });
        list.push({
          type: item.type,
          options: arr,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: keyName,
        });
      } else {
        list.push({
          type: item.type,
          required: item.required,
          label: item.label,
          uuid: item.uuid,
          value: item.value ? item.value : '',
        });
      }
    });
    this.state.formItem.widgetValue = list;
    this.state.formItem.count = this.state.currentCount;

    serviceTreeData.push(JSON.stringify(this.state.formItem));

    wxApi.setStorageSync('serviceTreeData', serviceTreeData);
    wxApi.setStorageSync('serviceOrderList', serviceTreeData);
    const serverStoreList = await this.computedBtnIsShow(this.state.serverStoreList);
    this.setState({
      showDialog: false,
      currentCount: 1,
      serverStoreList,
    }, () => {
      this.animatDialogRef.current.hide();
    });
    this.count();
  }

  // 计算多规格总数

  countMulti() {
    const serviceTreeData = wxApi.getStorageSync('serviceTreeData') || [];
    const index = this.state.serverStoreList.findIndex((e) => e.itemNo === this.state.formItem.itemNo);

    this.state.serverStoreList.forEach((item) => {});
  }

  // 跳转下单页
  async goServerDeliverDetail() {
    if (!authHelper.checkAuth()) return;
    const serviceTreeData = Taro.getStorageSync('serviceTreeData') || [];
    if (serviceTreeData.length == 0) {
      Taro.showToast({
        title: '请添加服务！', // 提示文字
        duration: 2000, // 显示时长
        icon: 'none', // 图标，支持"success"、"loading"
      });
      return;
    }
    // 判断是否有服务不在服务时间内
    let nowTime;
    await wxApi.request({
      url: api.livingService.getNowDate,
    }).then(res=>{
      nowTime = new Date(res.data)
    })
    const noServiceTime = serviceTreeData.filter(item=>{
      return !util.time_range(JSON.parse(item).serverStartTime,JSON.parse(item).serverEndTime,nowTime)
    })
    if (noServiceTime.length) {
      wxApi.showToast({
        title: `${JSON.parse(noServiceTime[0]).serverName}不在服务时间`,//提示文字
        duration: 2000,// 显示时长
        icon: 'none', // 图标，支持"success"、"loading"
      })
      return
    }

    // 判断是否包含需要扫码的商品
    const flag = await util.checkNeedScan(serviceTreeData.map((e) => JSON.parse(e)));
    console.log(flag,'------------------------flagflagflagflagflag')
    if (!flag) {
      wxApi.$navigateTo({
        url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
        data: {},
      });

      return;
    }

    if (this.props.roomCode) {
      wxApi.$navigateTo({
        url: 'sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
        data: {
          roomCode: this.props.roomCode,
          roomCodeTypeId: this.props.roomCodeTypeId,
          roomCodeTypeName: this.props.roomCodeTypeName,
          trueCode: this.props.roomCodeTypeName ? this.props.roomCodeTypeName + '-' + this.props.roomCode : this.props.roomCode,
        },
      });
    } else {
      // if (process.env.WX_OA === 'true') {
        // 公众号
      //   wxApi.$navigateTo({
      //     url: '/sub-packages/marketing-package/pages/living-service/goods-rental-pendingpay-merge/index',
      //     data: {
      //       needScan: true
      //     },
      //   });
      // } else {
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
          data: {
            //  serviceDetail:JSON.stringify(detail),
          },
        });
      // }
    }
  }

  // 跳预订清单
  gostronge() {
    wxApi.$navigateTo({
      url: 'sub-packages/marketing-package/pages/living-service/goods-reserve-list/index',
    });
  }

  // 计算count
  count() {
    const serviceTreeData = Taro.getStorageSync('serviceTreeData') ? Taro.getStorageSync('serviceTreeData') : [];
    let count = 0;
    serviceTreeData.forEach((item) => {
      item = JSON.parse(item);
      count += item.count;
    });
    let totalPrice = 0;
    serviceTreeData.forEach((item) => {
      item = JSON.parse(item);
      totalPrice += parseInt((item.salePrice * item.count) || 0 * item.count) + parseInt((item.deposit * item.count) || 0 * item.count);
    });

    this.setState({
      count,
      totalPrice: totalPrice / 100 || 0,
    });
  }

  // 关闭弹窗
  clsoeDialog = () => {
    this.animatDialogRef.current.hide();
  }

  // 当前商品数量减1处理
  ComputedCurrentCount(index, computedCount) {
    if (index === -1) return;
    const count = 'count';
    const value = 'serverStoreList[' + index + '].' + count + '';

    this.setState({
      [value]: computedCount || 0,
    });
  }

  /**
   * 表单类型的添加和减少
   */
  clickForm = (e) => {
    e.stopPropagation()
    const { flag } = e.currentTarget.dataset;

    // 库存等于选择数量 无法添加
    if(flag === 'plus' && this.state.currentCount + this.state.formItemCount >= this.state.formItem.itemStock && this.state.formItem.useStock == 1){
      return
    }

    if (flag === 'plus') {
      ++this.state.currentCount;
    } else {
      if (this.state.currentCount === 1) return;
      --this.state.currentCount;
    }
    this.setState({
      currentCount: this.state.currentCount,
    });
  }

  /**
   *  购物车添加项
   * @param e 当前项
   * @param func 需要调用的函数
   * @param flag ： plus 添加 minus 减少
   * @param num ：一次添加或者 减少的数量
   */
  clickPlus=(e)=> {
    e.stopPropagation()
    this.commonHandle(e, 'ComputedCurrentCount', 'plus', 1);
  }

  async commonHandle(e, func, flag, num) {
    const currentItem = e.currentTarget.dataset.item;
    // 库存等于选择数量 无法添加
    if(flag === 'plus' && currentItem.count >= currentItem.itemStock && currentItem.useStock == 1){
      return
    }
    // 取值并转换
    let current = Taro.getStorageSync('serviceOrderList') || [];
    current = current.map((e) => JSON.parse(e));
    let newArr = [];
    let item = null;
    if (current && current.length) {
      item = current.find((i) => i.itemNo === currentItem.itemNo);
      // 找到当前项，数量减1，为0从数组中删除，找不到 则赋值
      if (item) {
        if (flag === 'plus') {
          item.count = item.count + num;
        } else {
          item.count = item.count - num;
        }
        this[func](
          this.state.serverStoreList.findIndex((i) => i.itemNo === currentItem.itemNo),
          item.count
        );

        // 数量减一
        if (item.count === 0) {
          current.splice(
            current.findIndex((i) => i.itemNo === currentItem.itemNo),
            1
          );
        }
        newArr = current;
      } else {
        newArr = [currentItem, ...current];
      }
    } else {
      newArr = [currentItem, ...current];
    }
    newArr = newArr.map((item) => JSON.stringify(item));
    wxApi.setStorageSync('serviceTreeData', newArr);
    wxApi.setStorageSync('serviceOrderList', newArr);
    const serverStoreList = await this.computedBtnIsShow(this.state.serverStoreList);
    this.setState({
      serverStoreList
    })
    this.count(); // 计算总数
  }

  /**
   *购物车减少项
   * @param e 当前项
   * @param func 需要调用的函数
   * @param flag ： plus 添加 minus 减少
   * @param num ：一次添加或者 减少的数量
   */

  // 购物车数量减少
  clickMinus=(e)=> {
    e.stopPropagation()
    this.commonHandle(e, 'ComputedCurrentCount', 'minus', 1);
  }

  /**
   * 获取上传的图片
   * @param {*} e
   */
   handleImagesChange = (images: string[], uploadIndex: number) => {
    // 配置多个
    const formBody = this.state.formBody;
    formBody[uploadIndex].value = images;
    this.setState({
      formBody, // 图片上传列表
    });
  }

  render() {
    const {
      Height,
      currentIndex,
      tmpStyle,
      firstLevelList,
      categoryLevel2,
      seccurrentIndex,
      serverStoreList,
      count,
      totalPrice,
      animationData,
      formItem,
      currentCount,
      formBody,
      showDialog,
      formItemCount
    } = this.state;

    const { isComponent, dataSource } = this.props;
    // TODO: render()
    return (
      <View
        data-scoped='wk-wcbs-ServiceClassificationTree'
        data-fixme='01 className existed'
        className={'wk-wcbs-ServiceClassificationTree ' + ('classify ' + (isComponent ? '' : 'indexClassify'))}
        style={
          dataSource
            ? _safe_style_(
                'margin:' +
                  (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
                  ';height:' +
                  ((isComponent ? '' : Height) / 2) +
                  'px;'
              )
            : _safe_style_(`height: ${isComponent ? '100vh' : (Height / 2)}${isComponent ? '' : 'px'}`
            )
        }
      >
        <View
          className='content'
          style={_safe_style_(`height: ${isComponent ? '100vh' : (Height / 2)}${isComponent ? '' : 'px'}`)}>
          <View
            className={'left ' + (isComponent ? '' : 'indexLeft')}
            style={_safe_style_('height:' + ((isComponent ? 'auto' : Height) / 2) + 'px;')}
          >
            {firstLevelList &&
              firstLevelList.map((item, index) => {
                return (
                  <View
                    className='left_item 123'
                    key={Math.random() + index}
                    id={index}
                    onClick={_fixme_with_dataset_(this.clickClassify, { serviceCateGoryId: item.id })}
                    style={_safe_style_(
                      'color:' +
                        (currentIndex === index && tmpStyle ? tmpStyle.btnColor : 'rgba(162,162,164,1)') +
                        ';background-color:' +
                        (currentIndex === index ? 'rgba(255,255,255,1)' : 'rgba(240,241,245,1)') +
                        ';padding: 20px 0'
                    )}
                  >
                    <Text
                      className={'category-item limit-line line-2 ' + (currentIndex === index ? 'height-light' : '')}
                      style={_safe_style_(
                        'color:' + (currentIndex === index && tmpStyle ? tmpStyle.btnColor : 'rgba(162,162,164,1)')
                      )}
                    >
                      {item.categoryName}
                    </Text>
                  </View>
                );
              })}
          </View>
          <ScrollView
            className={'right ' + (isComponent ? '' : 'indexRight')}
            style={_safe_style_('height:' + (isComponent ? '' : Height / 2) + 'px;')}
            scrollY
            onScrollToLower={this.onScrollViewReachBottom}
          >
            {categoryLevel2.length > 0 && (
              <View className="goods-contaier">
                {!!categoryLevel2 && categoryLevel2.length > 0 && (
                  <ScrollView className="secondary-classification" scrollX="true">
                    {categoryLevel2 &&
                      categoryLevel2.map((item, index) => {
                        return (
                          <View
                            className={
                              'secondary-classification-item ' +
                              (seccurrentIndex === index ? 'cur' : '') +
                              ' ' +
                              (seccurrentIndex === index ? 'choose' : 'no-choose')
                            }
                            key={Math.random() + index}
                            id={index}
                            onClick={_fixme_with_dataset_(this.clicksecondClassify, {
                              secondId: item.id
                            })}
                            style={_safe_style_(
                              seccurrentIndex === index
                                ? 'color:' + tmpStyle.btnColor + ';border-color:' + tmpStyle.btnColor
                                : ''
                            )}
                          >
                            {item.categoryName}
                          </View>
                        );
                      })}
                  </ScrollView>
                )}
              </View>
            )}

            <View className="content-container">
              {serverStoreList && serverStoreList.length>0 &&
                serverStoreList.map((item, index) => {
                  return (
                    <View key={Math.random() + index} className="child-item">
                      <View
                        onClick={_fixme_with_dataset_(this.onGoServiceDetailPage, { item: item })}
                        className="service-item"
                      >
                        <Image
                          className="service-itemimg"
                          src={filters.imgListFormat(item.img)}
                          mode="aspectFill"
                        ></Image>
                        <View className="serviceBox">
                          <Text className="serviceNname">{item.serverName}</Text>
                          <Text className="service-desc">{item.serverDesc}</Text>
                          <View className="servicePriceBox">
                            <Text className="servicePrice" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                              {item.needPayFee == 1 ? '￥' + filters.moneyFilter(item.salePrice, true) : getFreeLabel(item.showType)}
                            </Text>
                            {item.originalPrice ? (
                              <Text className="originalPrice" style={_safe_style_('color:')}>
                                {item.originalPrice ? '￥' + filters.moneyFilter(item.originalPrice, true) : 0}
                              </Text>
                            ) : ''}
                          </View>
                          {item.count > 0 && !item.isShowMark.length && (
                            <View className="count-box">
                              <View
                                className="minus left-radius"
                                onClick={_fixme_with_dataset_(this.clickMinus, {
                                  index: index,
                                  item: item,
                                })}
                              >
                                -
                              </View>
                              <View className="num">
                                <Input className="input-comp" type="number" value={item.count} disabled></Input>
                              </View>
                              <View
                                className="plus right-radius"
                                onClick={_fixme_with_dataset_(this.clickPlus, {
                                  index: index,
                                  item: item,
                                })}
                                style={_safe_style_(item.count >= item.itemStock && item.useStock == 1 ? 'color:#cccccc':null)}
                              >
                                +
                              </View>
                            </View>
                          )}
                        </View>
                      </View>
                      {!!(item.count == 0 || !item.count || item.isShowMark.length) ? (
                        <View
                          className="add-img"
                          onClick={_fixme_with_dataset_(this.addCart.bind(this), { item: item, index: index })}
                        >
                          <Image
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png"
                            style={_safe_style_(item.useStock == 1 && (!item.itemStock || item.count >= item.itemStock) ? 'background-color:#cccccc': 'background-color: ' + tmpStyle.btnColor)}
                            className="add-cart-icon"
                            mode="aspectFill"
                          ></Image>
                          {item.count !== 0 && <View className="tip">{item.count}</View>}
                        </View>
                      ) : ''}
                    </View>
                  );
                })}
              {!!serverStoreList && serverStoreList.length == 0 && (
                <Empty emptyStyle="width:auto;" message="敬请期待..."></Empty>
              )}
            </View>
          </ScrollView>

          <View className={isComponent ? 'bottom_box' : 'indexBottom_box'}>
            <View className='iconBox' onClick={this.gostronge}>
              <View className='cart-img-box' style={tmpStyle ? _safe_style_('background:' + tmpStyle.btnColor) : null}>
                <Image
                  src='https://front-end-1302979015.file.myqcloud.com/images/c/images/ic-add-cart.png'
                  className='cart-imgiocn'
                ></Image>
              </View>
              {!!count && (
                <View className='cartCount'>
                  <Text>{count}</Text>
                </View>
              )}
            </View>
            <View className='price'>
              <Text>{'￥' + totalPrice}</Text>
            </View>
            <View
              className='bottom_submite'
              style={tmpStyle ? _safe_style_('background:' + tmpStyle.btnColor + ';') : null}
              onClick={this.goServerDeliverDetail.bind(this)}
            >
              立即下单
            </View>
          </View>
        </View>
        <AnimatDialog ref={this.animatDialogRef} id="animat-dialog" animClass="commodity_attr_box">
            <View className='bottom-dialog bottom-dialog-nocomponent'>
              <View className="dialog-content">
                <View className="product-box">
                  <Image className="product-image" src={filters.imgListFormat(typeof formItem.img === 'string' ? formItem.img : '')} mode="aspectFill"></Image>
                  <View className="product-info">
                    <View className="product-name">{formItem.serverName}</View>
                    <View className="product-time">服务时段：{formItem.serverStartTime || ''}-{formItem.serverEndTime || ''}</View>
                    <View className="product-info-bottom">
                      <View className="product-price-count .price">
                        {formItem.needPayFee == 1 ? '￥' + filters.moneyFilter(formItem.salePrice, true) : getFreeLabel(formItem.showType)}
                      </View>
                      {formItem.showNumber === 1 &&
                      <View className="count-box">
                        <View
                          className="minus"
                          onClick={_fixme_with_dataset_(this.clickForm, {
                            flag: 'minus',
                            item: formItem,
                          })}
                        >
                          -
                        </View>
                        <View className="num">{currentCount}</View>
                        <View
                          className="plus"
                          onClick={_fixme_with_dataset_(this.clickForm, {
                            flag: 'plus',
                            item: formItem,
                          })}
                          style={_safe_style_(formItemCount+ currentCount >= formItem.itemStock && formItem.useStock == 1 ? 'color:#cccccc':null)}
                        >
                          +
                        </View>
                      </View>
                      }
                    </View>
                  </View>
                </View>
                <View className="form-info">
                  <View className="form-body">
                    <View className="white-box">
                      {
                        formBody?.map((item, index) => {
                          return (
                            <View className={'form-item ' + item.type + '-item'} key={item.uuid}>
                              {item.type !== 'comment' && (
                                <View className="form-item-container">
                                  <Text className={'form-item-label ' + (item.required ? 'required' : '')}>
                                    {item.label}
                                  </Text>
                                  {/*  <text style="color: #f56c6c; padding-left: 5rpx" wx:if="{{item.required}}">*</text>  */}
                                  <View className="form-item-ipt">
                                    {item.type === 'sex' && (
                                      <View className="form-item-content form-sex">
                                        <View
                                          onClick={_fixme_with_dataset_(this.chooseGender, {
                                            value: '男',
                                            index: index,
                                          })}
                                          className={item.value === '男' ? 'selected' : ''}
                                        >
                                          <Image
                                            mode="aspectFit"
                                            src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/male.png"
                                          ></Image>
                                          <View>男</View>
                                        </View>
                                        <View
                                          onClick={_fixme_with_dataset_(this.chooseGender, {
                                            value: '女',
                                            index: index,
                                          })}
                                          className={item.value === '女' ? 'selected' : ''}
                                        >
                                          <Image
                                            mode="aspectFit"
                                            src="https://front-end-1302979015.file.myqcloud.com/images/c/sub-packages/marketing-package/images/form-tool/female.png"
                                          ></Image>
                                          <View>女</View>
                                        </View>
                                      </View>
                                    )}

                                    {/*  单选框  */}
                                    {item.type === 'radio' && (
                                      <RadioGroup
                                        className="form-item-content form-radio"
                                        name={'radio-group' + item.uuid}
                                        key={'radio-group' + item.uuid}
                                        onChange={_fixme_with_dataset_(this.changeRadioValue, { index: index, item: item })}
                                      >
                                        {item.options &&
                                          item.options.map((optionItem, index) => {
                                            return (
                                              <Label
                                                className="radio-box-item"
                                                style={_safe_style_(
                                                  `background: ${optionItem.checked === true ? tmpStyle.btnColor:'#fff'};
                                                  color:${optionItem.checked === true ? '#fff':'#343434'};
                                                  border-color:${optionItem.checked === true?tmpStyle.btnColor:'#979797'}`
                                                )}
                                                key={'radio-box-' + item.uuid + index}
                                                for={'radio-box-' + item.uuid + index}>
                                                <Radio
                                                  value={optionItem.value}
                                                  checked={optionItem.checked}
                                                  color={tmpStyle.btnColor}>
                                                  {optionItem.value}
                                                </Radio>
                                              </Label>
                                            );
                                          })}
                                      </RadioGroup>
                                    )}

                                    {/*  多选框  */}
                                    {item.type === 'checkbox' && (
                                      <View className="form-item-content form-radio">
                                      <CheckboxGroup
                                        className="form-item-content form-radio"
                                        name={'checkbox-group' + item.uuid}
                                        key={'checkbox-group' + item.uuid}
                                        onChange={_fixme_with_dataset_(this.bindValue, { index: index, item: item })}
                                      >
                                        {item.options &&
                                          item.options.map((child, index) => {
                                            return (
                                              <Label
                                                className="checkout-box-item"
                                                style={_safe_style_(
                                                  `background: ${child.checked === true ? tmpStyle.btnColor:'#fff'};
                                                  color:${child.checked === true ? '#fff':'#343434'};
                                                  border-color:${child.checked === true?tmpStyle.btnColor:'#979797'}`
                                                )}
                                                key={'checkbox-item' + item.uuid + index} for={'checkbox-item' + item.uuid + index}>
                                                <Checkbox value={child.name} checked={child.checked}></Checkbox>
                                                {child.name}
                                              </Label>
                                            );
                                          })}
                                      </CheckboxGroup>
                                      </View>
                                    )}

                                    {/*  时间选择器  */}
                                    {item.type === 'date' && (
                                      <Picker
                                        className="form-item-content form-date"
                                        mode="date"
                                        start={item.dateRange ? item.dateRange[0] : ''}
                                        end={item.dateRange ? item.dateRange[1] : ''}
                                        onChange={_fixme_with_dataset_(this.inputValue, { index: index })}
                                      >
                                        {item.value ? (
                                          <Text className="picker">{item.value}</Text>
                                        ) : (
                                          <Text style={_safe_style_('color: #888;')}>请选择</Text>
                                        )}

                                        <Image
                                          className="picker-icon"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                        ></Image>
                                      </Picker>
                                    )}

                                    {/*  文本类型单行输入框  */}
                                    {(item.type === 'text' || item.type === 'email') && (
                                      <Input
                                        className="form-item-content form-input"
                                        placeholder="请输入"
                                        value={item.value}
                                        onBlur={_fixme_with_dataset_(this.inputValue, { index: index })}
                                      ></Input>
                                    )}

                                    {/*  数字类型单行输入框  */}
                                    {item.type === 'phone' && (
                                      <Input
                                        className="form-item-content form-input"
                                        placeholder="请输入"
                                        type="phone"
                                        value={item.value}
                                        onBlur={_fixme_with_dataset_(this.inputValue, { index: index })}
                                      ></Input>
                                    )}

                                    {/*  多行输入框  */}
                                    {item.type === 'textarea' && (
                                      <View className="form-textarea">
                                        <Textarea
                                          className="form-item-content"
                                          maxlength="-1"
                                          placeholder="请输入留言内容"
                                          onInput={_fixme_with_dataset_(this.inputValue, { index: index })}
                                        ></Textarea>
                                        {/*  <View class="num-tip">5/50</View>  */}
                                      </View>
                                    )}

                                    {/*  地址  */}
                                    {item.type === 'address' && (
                                      <RegionPicker
                                        // className="form-item-content form-address"
                                        // mode="region"
                                        // onChange={_fixme_with_dataset_(this.inputValue, { index: index })}

                                        className="form-item-content form-address"
                                      // mode="region"
                                      options={{index:index}}
                                      onChange={this.onRegionPickerChange}
                                      >
                                        {item.value ? (
                                          <Text className="picker">{item.value}</Text>
                                        ) : (
                                          <Text style={_safe_style_('color: #888;')}>请选择</Text>
                                        )}

                                        <Image
                                          className="picker-icon"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                                        ></Image>
                                      </RegionPicker>
                                    )}

                                    {/* 详细地址  */}
                                    {item.type === 'detail-address' && (
                                      <View className="textarea">
                                        <Textarea
                                          className="form-item-content form-detail-address"
                                          disableDefaultPadding
                                          autoHeight
                                          placeholder="请输入详细地址"
                                          onInput={_fixme_with_dataset_(this.inputValue, { index: index })}
                                          value={item.value}
                                        ></Textarea>
                                      </View>
                                    )}

                                    {item.type === 'comment' && (
                                      <View className="comment">
                                        <Text>{item.value}</Text>
                                      </View>
                                    )}

                                    {(item.type === 'uploadImage') && (
                                      <View className="form-item-content form-uploadImage uploadImage-box">
                                        <View className='img-box'>
                                          <UploadImg onChange={this.handleImagesChange} value={item.value} max={item.max} containerIndex={index}></UploadImg>
                                        </View>
                                      </View>
                                    )}
                                  </View>
                                </View>
                              )}
                            </View>
                          );
                        })}
                    </View>
                  </View>
                </View>
                <View className="button-box">
                  <View className="btn btn-close" onClick={this.clsoeDialog}>
                    取消
                  </View>
                  <View
                    className="btn btn-submit"
                    onClick={this.submiteCart.bind(this)}
                    style={_safe_style_('background:' + tmpStyle.btnColor + ';')}
                  >
                    加入预订
                  </View>
                </View>
              </View>
            </View>
          </AnimatDialog>
      </View>
    );
  }
}

export default ServiceClassificationTree;
