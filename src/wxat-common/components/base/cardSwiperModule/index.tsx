import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';

// import pay from '../../../utils/pay.js';
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import config from '../utils/config.js';
import CardSwiperModule from '../../decorate/cardSwiperModule/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const decorateImg = cdnResConfig.decorate;
const times_card = decorateImg.timesCard;
const recharge_card = decorateImg.rechargeCard;

type CardPackModuleProps = {
  dataSource: Record<string, any>;
};

let CardSwiperModuleWrap: FC<CardPackModuleProps> = ({ dataSource = {} }) => {
  const appId = useSelector((state) => state.ext.appId);
  const storeId = useSelector((state) => state.base.currentStore.id);

  const [dsRemote, setDsRemote] = useState([]);
  const [numberCardType, setNumberCardType] = useState(3); // 3 次数卡 4充值卡
  const [bgImages, setDgImages] = useState<Array<string>>([]);

  useEffect(() => {
    if (dataSource.data && dataSource.data.length > 0) {
      const items: any[] = [];
      dataSource.data.forEach((item) => {
        items.push(item.itemNo);
      });
      const itemNos = items.join(',');
      const params = {
        appId,
        storeId,
        itemNo: itemNos,
      };

      wxApi
        .request({
          url: api.card.idForList,
          data: params,
        })
        .then((res) => {
          if (res.data) {
            handleBgImage(res.data);
          }
        })
        .catch((error) => {
          console.log('get cardSwiperModule: error: ' + JSON.stringify(error));
        });
    }
  }, []);

  function handleBgImage(val) {
    const images: Array<string> = [];
    val.forEach((item) => {
      if (item.styleUrl) {
        images.push(item.styleUrl);
      } else {
        if (item.type === numberCardType) {
          images.push(times_card);
        } else {
          images.push(recharge_card);
        }
      }
    });
    setDgImages(images);
    setDsRemote(val);
  }

  function handleGoToCardDetail(cardItem) {
    if (cardItem) {
      wxApi.$navigateTo({
        url: '/sub-packages/server-package/pages/card-detail/index',
        data: { itemNo: cardItem.itemNo },
      });
    }
  }

  return (
    <View
      data-scoped='wk-cbc-CardSwiperModule'
      className='wk-cbc-CardSwiperModule card-module'
      style={{
        margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
          (dataSource.marginLeftRight || 0) * 2
        )}`,
        borderRadius: Taro.pxTransform((dataSource.radius || 0) * 2),
      }}
    >
      {!!dataSource.showNav && (
        <TextNavModule dataSource={dataSource.textNavSource} padding={config.PADDING}></TextNavModule>
      )}

      <CardSwiperModule
        dataList={dsRemote}
        bgImages={bgImages}
        PADDING={config.PADDING}
        onClick={handleGoToCardDetail}
      ></CardSwiperModule>
    </View>
  );
};

export default CardSwiperModuleWrap;
