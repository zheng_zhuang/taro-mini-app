import React, { FC, useState, useEffect } from 'react';
import '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';

// import filters from '@/wxat-common/utils/money.wxs.js';
import handleData from '../utils/handleData.js';
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import config from '../utils/config.js';
import CollageModule from '../../decorate/collageModule/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';

interface CollageModuleProps {
  dataSource: Record<string, any>;
  list?: Array<Record<string, any>>;
}

const CollageModuleWrap: FC<CollageModuleProps> = ({ dataSource, list }) => {
  const [_renderList, setRenderList] = useState<Array<Record<string, any>>>([]); // 渲染的数据列表
  const [showGroupType, setShowGroupType] = useState(0); // 子组件展示类型，0：上图下文(默认展示的类型)，1：左图右文
  const [textNavSource, setTextNavSource] = useState({}); // 标题数据对象

  useEffect(() => {
    // 获取父组件传过来的拼团类型
    if (dataSource) {
      const showGroupType = dataSource.showType || 0;
      // 在标题数据对象中添加展示类型参数是为了跳转拼团列表的时候方便展示拼团模式
      const textNavSource = dataSource.textNavSource;
      if (textNavSource) {
        textNavSource.showGroupType = showGroupType;
        setTextNavSource(textNavSource);
      }
      setShowGroupType(showGroupType);
    }

    if (list && !!list.length) {
      setRenderList(list || []);
    } else {
      getList();
    }
  }, [dataSource, list]);

  /**
   * 获取拼团活动列表
   */
  function getList() {
    const data = dataSource.data;
    if (data && data.length) {
      const itemNos = handleData.pickUpItemNos(data, 'id').join(',');
      wxApi
        .request({
          url: api.activity.list,
          data: {
            activityIdList: itemNos,
            status: 1,
          },
        })
        .then((res) => {
          if (res.success && !!res.data) {
            setRenderList(res.data || []);
          }
        });
    }
  }

  function click(item) {
    // const item = e.detail.item;
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/group/detail/index',
      data: {
        activityId: item.id,
        itemNo: item.itemNo,
      },
    });
  }

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbc-CollageModule'
      className='wk-cbc-CollageModule'
    >
      {!!(!!_renderList && !!_renderList.length) && (
        <View
          className='collage-module'
          style={{
            margin: `${Taro.pxTransform((dataSource.marginUpDown || 0) * 2)} ${Taro.pxTransform(
              (dataSource.marginLeftRight || 0) * 2
            )}`,
            borderRadius: Taro.pxTransform((dataSource.radius || 0) * 2),
          }}
        >
          {!!dataSource.showNav && <TextNavModule dataSource={textNavSource} padding={config.PADDING}></TextNavModule>}
          <CollageModule
            PADDING={config.PADDING}
            dataList={_renderList}
            showGroupType={showGroupType}
            onClick={click}
          ></CollageModule>
        </View>
      )}
    </View>
  );
};

CollageModuleWrap.defaultProps = {
  dataSource: {},
  list: [],
};

export default CollageModuleWrap;
