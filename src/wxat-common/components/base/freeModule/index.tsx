import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';

// import filters from '../../../utils/money.wxs.js';
import handleData from '../utils/handleData.js';
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import config from '../utils/config.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import template from '../../../utils/template.js';
import MarketingItem from '../../marketing-item/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';

const SHOW_TYPE = {
  vertical: 1, // 经典列表
  horizon: 4, // 水平滑动
  rowOne: 0, // 一行一个
  rowTwo: 2, // 一行两个
  rowThree: 3, // 一行三个
};

const PADDING = config.PADDING;

type FreeModuleWrapProps = {
  dataSource: Record<string, any>;
  list?: Array<Record<string, any>>;
  showType?: number;
};

let FreeModuleWrap: FC<FreeModuleWrapProps> = ({ dataSource, list, showType }) => {
  const [renderList, setRenderList] = useState([]);
  const [showFreeType, setShowFreeType] = useState(0); // 展示类型枚举参考MarketingModule中的SHOW_TYPE
  const [textNavSource, setTextNavSource] = useState({});
  const [itemClass, setItemClass] = useState<string | undefined>('vertical-item');
  const [containerClass, setContainerClass] = useState<string | undefined>('vertical-container');
  const [tmpStyle, setTmpStyle] = useState({});

  useEffect(() => {
    assembleMarketingData(list);
  }, [list]);

  useEffect(() => {
    getTemplateStyle();
    let _showFreeType = showType;
    let _textNavSource = {};
    if (dataSource.data && dataSource.data.length > 0) {
      _showFreeType = dataSource.showType || SHOW_TYPE.rowOne;
      // 在标题数据对象中添加展示类型参数是为了跳转拼团列表的时候方便展示拼团模式
      _textNavSource = dataSource.textNavSource;
      _textNavSource.showFreeType = _showFreeType;
    }
    setShowFreeType(_showFreeType);
    setTextNavSource(_textNavSource);
    const itemClass = getClassByShowType('item', _showFreeType);
    const containerClass = getClassByShowType('container', _showFreeType);

    setItemClass(itemClass);
    setContainerClass(containerClass);

    if (list && list.length) {
      assembleMarketingData(list);
    } else {
      getList();
    }
  }, []);

  /**
   * 处理多种样式的展示形式
   * 传container是父级class，item是子级class
   * @param {type} item or container
   * @returns className
   */
  function getClassByShowType(type, currentFreeType) {
    const showType = SHOW_TYPE;
    const currentFreeTypes = currentFreeType || showFreeType;
    switch (currentFreeTypes) {
      case showType.vertical:
        return 'vertical-' + type;
        break;
      case showType.horizon:
        return 'horizon-' + type;
        break;
      case showType.rowOne:
        return 'oneRowOne-' + type;
        break;
      case showType.rowTwo:
        return 'oneRowTwo-' + type;
        break;
      case showType.rowThree:
        return 'oneRowThree-' + type;
        break;
    }
  }
  /**
   * 获取赠品活动列表
   */
  function getList() {
    const data = dataSource.data;
    if (data && data.length) {
      const idList = handleData.pickUpItemNos(data, 'id').join(',');
      wxApi
        .request({
          url: api.giftActivity.list,
          data: {
            activityIds: idList,
            pageNo: 1,
            pageSize: 10,
          },

          quite: true,
        })
        .then((res) => {
          if (res.data) {
            assembleMarketingData(res.data);
          }
        });
    }
  }

  function assembleMarketingData(renderList) {
    const assembledRendList = (renderList || []).map((item) => {
      const labelList: Array<{ name: string }> = [];
      labelList.push({
        name: item.levelName,
      });

      if (item.everyoneLimitCount) {
        labelList.push({
          name: `限领${item.everyoneLimitCount}件`,
        });
      }
      const attrValCombineName = item.attrValCombineName ? '(' + item.attrValCombineName + ')' : '';
      return {
        id: item.id,
        thumbnail: item.thumbnail,
        name: item.name + attrValCombineName,
        receivedCount: item.receivedCount,
        canReceived: item.canReceived,
        canNotReason: item.canNotReason,
        everyoneLimitCount: item.everyoneLimitCount,
        levelName: item.levelName,
        labelList: labelList,
        saleAmount: `已兑换${item.receivedCount}件`,
        btn: '立即领取',
        disabledBtn: !item.canReceived,
      };
    });
    setRenderList(assembledRendList);
  }

  function go(item) {
    if (!item.canReceived) {
      return wxApi.showToast({
        title: item.canNotReason,
        icon: 'none',
      });
    }
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        type: goodsTypeEnum.FREE_GOODS_ACTIVITY.value,
        activityId: item.id,
      },
    });
  }
  //获取模板配置
  function getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();

    setTmpStyle(templateStyle);
  }

  if (!renderList || renderList.length === 0) {
    return null;
  }

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-cbf-FreeModule' className='wk-cbf-FreeModule'>
      <View
        className='gift-module'
        style={{
          margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
            dataSource.marginLeftRight * 2 || 0
          )}`,
          borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
        }}
      >
        {!!dataSource.showNav && <TextNavModule dataSource={textNavSource} padding={PADDING}></TextNavModule>}
        <View className='module-container'>
          <View className={containerClass}>
            {renderList.map((item, index) => {
              return (
                <View className={itemClass} key={item.id}>
                  <MarketingItem
                    onClick={go}
                    marketingItem={item}
                    customContent
                    showType={showFreeType}
                    renderOneRowTwoContent={
                      <Block>
                        <View className='content-box'>
                          <View className='label-box'>
                            <View
                              className='label'
                              style={{
                                color: tmpStyle.btnColor,
                                background: tmpStyle.bgColor,
                              }}
                            >
                              {item.levelName || '未知'}
                            </View>
                            {!!item.everyoneLimitCount && (
                              <View
                                className='label'
                                style={{
                                  color: tmpStyle.btnColor,
                                  background: tmpStyle.bgColor,
                                }}
                              >
                                {'限领' + item.everyoneLimitCount + '件'}
                              </View>
                            )}
                          </View>
                          <View
                            className={'btn ' + (item.canReceived ? '' : 'disable-btn')}
                            style={{
                              background: item.canReceived ? tmpStyle.btnColor : '#E1E1E1',
                            }}
                          >
                            立即领取
                          </View>
                          <View className='count'>{'已兑换' + item.receivedCount + '件'}</View>
                        </View>
                      </Block>
                    }
                    renderOneRowThreeContent={
                      <Block>
                        <View className='one-row-three-btn-box'>
                          <View
                            className='one-row-three-btn'
                            style={{
                              background: item.canReceived ? tmpStyle.btnColor : '#E1E1E1',
                            }}
                          >
                            立即领取
                          </View>
                        </View>
                      </Block>
                    }
                    renderHorizonContent={
                      <Block>
                        <View className='one-row-three-btn-box'>
                          <View
                            className='one-row-three-btn'
                            style={{
                              background: item.canReceived ? tmpStyle.btnColor : '#E1E1E1',
                            }}
                          >
                            立即领取
                          </View>
                        </View>
                      </Block>
                    }
                  ></MarketingItem>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    </View>
  );
};

FreeModuleWrap.defaultProps = {
  list: [], // list: null //直接渲染list列表
  showType: 0,
  dataSource: {}, //传dataSource只是取dataSource里面的id  进行再次查询
};

export default FreeModuleWrap;
