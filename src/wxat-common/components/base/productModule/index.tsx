import React, {useState, useEffect, FC, useRef, useLayoutEffect} from 'react';
import '@/wxat-common/utils/platform';
import {ScrollView, View} from '@tarojs/components';
import Taro, {useDidShow, useReachBottom} from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import api from "../../../api";
import handleData from '../utils/handleData.js';
import report from "../../../../sdks/buried/report";
import reportConstants from '../../../../sdks/buried/report/report-constants.js';

import GoodsItem from "../../industry-decorate-style/goods-item";
import displayEnum from '../../../constants/displayEnum';
import TextNavModule from "../../decorate/textNavModule";
import Empty from '@/wxat-common/components/empty/empty';
import './index.scss';
import classNames from 'classnames';
import {usePageScroll} from "@tarojs/runtime";
import util from '@/wxat-common/utils/util';

const PADDING = 0;
let storeIds = null;

interface IProps {
  reportSource?: string;
  dataSource: Record<string, any>;
  list?: Array<{}>;
  display?: string;
  onClick?: Function;
  backgroundColor?: string;
}

interface RenderList {
  display: string;
  data: any[];
  type: string;
}

const defaultReportSource = reportConstants.SOURCE_TYPE.product.key;


const ProductModule: FC<IProps> = (props) => {
  const {dataSource, list, reportSource, display, onClick, backgroundColor} = props;
  const [groupActiveIdx, setGroupActiveIdx] = useState(0);

  const ref = useRef(null)
  const [width, setWidth] = useState(0);

  const [barHeight, setBarHeight] = useState(0);

  useLayoutEffect(() => {
    if (process.env.TARO_ENV == 'h5') {
      setWidth(ref.current.offsetWidth);
    }
    // 微信小程序兼容处理
    else {
      // 计算顶部状态栏高度 用于商品分组吸顶位置定位
      const barHeight = wx.getSystemInfoSync().statusBarHeight;
      setBarHeight(barHeight);
      // 计算容器宽度 解决吸顶定位时宽度混乱
      setTimeout(() => {
        queyElementYPosition("#wk-cbp-ProductModule", this).then(v => {
          if (process.env.TARO_ENV != "h5") {
            setWidth(v.width)
          }
        })
      }, 500)
    }
  }, []);


  const [group, setGroup] = useState({
    index: 0,
    pageNo: 1,
    pageSize: 30,
    isMore: false,
    loading: false
  });
  const [renderList, setRenderList] = useState<RenderList>({
    display: '',
    type: '',
    data: [],
  });

  const [itemClass, setItemClass] = useState('');
  const [containerClass, setContainerClass] = useState('');

  const [toTop, setToTop] = useState(false);


  useReachBottom(() => {
  })

  useEffect(() => {
    if ((!list || !list.length) && !!dataSource) {
      storeIds = wxApi.getStorageSync('storeId');
      const data = dataSource && dataSource.data;
      if (!!data && !!data.length) getList(dataSource, data, null);
    } else if (list && list.length) {
      storeIds = wxApi.getStorageSync('storeId');
      const _renderList: RenderList = {
        display: display || displayEnum.VERTICAL,
        type: 'product',
        data: list,
      };

      setRenderList(_renderList);
    }
    const dataSourceDisplay = dataSource && dataSource.display;
    const _itemClass = displayEnum.getClassByShowType(displayEnum.classTypeEnum.item, dataSourceDisplay || display);
    const _containerClass = displayEnum.getClassByShowType(
      displayEnum.classTypeEnum.container,
      dataSourceDisplay || display
    );

    setItemClass(_itemClass + '');
    setContainerClass(_containerClass + '');
  }, [dataSource, list, display, storeIds]);

  Taro.usePageScroll(() => {
    // 分组吸顶处理
    if (dataSource.comModel === 'group' && dataSource.groupList && dataSource.groupList.length != 0 && dataSource.groupList[0].cateId != '') {
      queyElementYPosition("#wk-cbp-ProductModule", this).then(v => {
        if (v.top <= 0) {
          setToTop(true)
        } else {
          setToTop(false)
        }
      })
    } else {
      setToTop(false)
    }
  })

  const getList = (dataSource, data, cateId) => {
    if (data && data.length > 0) {
      const itemNos = handleData.pickUpItemNos(data, 'itemNo').join(',');
      let params = {
        itemNoList: itemNos,
      };

      if (dataSource.comModel === 'group') {
        params = {
          categoryId: cateId || dataSource.groupList[0].cateId,
          pageSize: group.pageSize,
          pageNo: group.pageNo
        };
      }
      wxApi
        .request({
          url: api.classify.itemSkuList,
          loading: true,
          data: params,
        })
        .then((res) => {
          const _renderList: RenderList = {
            display: '',
            type: '',
            data: [],
          };

          Object.keys(dataSource).forEach((key) => {
            if (dataSource.hasOwnProperty(key)) {
              if (key === 'data') {
                _renderList[key] = res.data || [];
                const goodsExposure: any[] = [];
                (_renderList[key] || []).forEach((item) => {
                  if (item.wxItem.itemNo) {
                    goodsExposure.push(item.wxItem.itemNo);
                  }
                });
                if (!!goodsExposure && !!goodsExposure.length) {
                  report.goodsExposure(goodsExposure.join(','));
                }
              } else {
                _renderList[key] = dataSource[key];
              }
            }
          });

          if (!dataSource.hasOwnProperty('display')) {
            const showRectType = parseInt(dataSource.showRectType);
            if (showRectType === 1) {
              _renderList.display = displayEnum.VERTICAL;
            } else if (showRectType === 1) {
              _renderList.display = displayEnum.ONE_ROW_TWO;
            }
          }

          _renderList.type = 'product';
          setRenderList(_renderList);
        })
        .catch((error) => {
          console.log('get product: error: ' + error);
        });
    }
  }

  const handleChangeCate = (obj) => {
    setRenderList((oldData) => {
      return {
        ...oldData,
        data: []
      }
    })
    setGroup({
      index: 0,
      pageNo: 1,
      pageSize: 30,
      isMore: false,
      loading: false
    })
    setGroupActiveIdx(obj.index);
    getList(dataSource, dataSource.data, obj.item.cateId)
  };

  // 判断元素与顶部距离
  const queyElementYPosition = (selector, component) => {
    let query;
    if (process.env.TARO_ENV == 'h5') {
      query = Taro.createSelectorQuery().in(component)
    } else {
      query = Taro.createSelectorQuery()
    }
    return new Promise((resolve, reject) => {
      const tmp = query.select(selector)
        .fields({size: true, rect: true, scrollOffset: true, dataset: true}, (rect) => {
          rect ? resolve(rect) : reject(rect);
        });
      tmp && tmp.exec();
    })
  }

  const onItemClick = (item) => {
    util.sensorsReportData('product_item_click', {
      event_name: '商品点击',
      prod_item_no: item.detail?.name,
      prod_item_name: item.detail?.itemNo
    })
    return !onClick ? false : onClick(item) || true;
  };

  const isOneRowTow = containerClass === 'oneRowTwo-container';


  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbp-ProductModule'
      className='wk-cbp-ProductModule'
      id="wk-cbp-ProductModule"
    >
      {(
        <View
          className={toTop ? 'product-module product-module-top' : 'product-module'}
          ref={ref}
          id="productModule"
          style={{
            width: toTop && width ? width : 'auto',
            top: process.env.TARO_ENV == 'h5' ? '0px' : barHeight + 30,
            padding: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 20)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2 || 20
            )}`,
            margin: (dataSource.marginUpDown * 2 + 'px ' + dataSource.marginLeftRight + 'px'),
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
            background:
              dataSource.backgroundColor ||
              backgroundColor ||
              (renderList.display === displayEnum.VERTICAL ? 'rgba(255,255,255,1)' : ''),
          }}
        >
          {/* 商品分组 */}
          {!!(dataSource && dataSource.comModel === 'group' && dataSource.groupList && dataSource.groupList.length) && (
            <ScrollView scrollX className="group-nav">
              {dataSource.groupList.map((item, index) => {
                return (
                  <View className={index === groupActiveIdx ? 'group-nav__item is-active' : 'group-nav__item'}
                        key={index + 'proGroup'}
                        onClick={() => {
                          handleChangeCate({item: item, index: index})
                        }}>
                    <View className="group-title">{item.title}</View>
                    <View className="group-desc">{item.subTitle}</View>
                  </View>
                )
              })}
            </ScrollView>
          )}

          {!!(dataSource && dataSource.showNav) && (
            <TextNavModule
              reportSource={reportSource}
              dataSource={dataSource.textNavSource}
              padding={PADDING}
              display={renderList.display}
            />
          )}

          <View className={containerClass}>
            {isOneRowTow && (
              <ScrollView scrollY={toTop}
                          className={(dataSource.comModel === 'group' && renderList.data.length > 0) ? (process.env.TARO_ENV == 'h5' ? 'shop-list' : 'shop-list-wx') : ''}>
                <View className='shop-list-wx'>
                  <View className='left'>
                    {renderList.data
                      .filter((_, index) => index % 2 === 0)
                      .map((item, index) => (
                        <View
                          className={classNames([itemClass, 'goods-wrap'])}
                          key={index}
                        >
                          <GoodsItem
                            goodsItem={item}
                            onClick={onItemClick}
                            reportSource={reportSource || defaultReportSource}
                            display={renderList.display}
                          />
                        </View>
                      ))}
                  </View>
                  <View className='right'>
                    {renderList.data
                      .filter((_, index) => index % 2)
                      .map((item, index) => (
                        <View
                          className={classNames([itemClass, 'goods-wrap'])}
                          key={index}
                        >
                          <GoodsItem
                            goodsItem={item}
                            onClick={onItemClick}
                            reportSource={reportSource || defaultReportSource}
                            display={renderList.display}
                          />
                        </View>
                      ))}
                  </View>
                </View>
              </ScrollView>

            )}

            {/* {isOneRowTow && (
              <ScrollView scrollY={toTop}
                scrollWidth={350}
                          className={(dataSource.comModel === 'group' && renderList.data.length > 0) ? (process.env.TARO_ENV == 'h5' ? 'shop-list' : 'shop-list-wx') : ''}>


              </ScrollView>
            )} */}

            {!isOneRowTow && (
              <ScrollView scrollY={toTop}
                          className={(dataSource.comModel === 'group' && renderList.data.length > 0) ? (process.env.TARO_ENV == 'h5' ? 'shop-list' : 'shop-list-wx') : ''}>
                <View className={containerClass}>
                  {renderList.data.map((item, index) => {
                    return (
                      <View className={itemClass} key={index}>
                        <GoodsItem
                          goodsItem={item}
                          onClick={onItemClick}
                          reportSource={reportSource || defaultReportSource}
                          display={renderList.display}
                        />
                      </View>
                    );
                  })}
                </View>
              </ScrollView>
            )}
            {Array.isArray(renderList.data) && renderList.data.length == 0 && (<Empty></Empty>)}
          </View>
        </View>
      )}
      {/* 占位 */}
      {dataSource.comModel === 'group' && (
        <View className="space"></View>
      )}
    </View>
  );
};

ProductModule.defaultProps = {
  dataSource: {},
  list: [],
  reportSource: '',
  display: displayEnum.VERTICAL,
  // item点击回调函数
};

export default ProductModule;
