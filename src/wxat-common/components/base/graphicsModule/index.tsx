import React, { FC, useState, useEffect } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Text, Image } from '@tarojs/components';
import Taro, { useDidShow } from '@tarojs/taro';
import report from '@/sdks/buried/report/index';
import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';

import api from "../../../api";
import wxApi from '../../../utils/wxApi';
import TextNavModule from "../../decorate/textNavModule";

import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import filters from '../../../utils/money.wxs.js';
// import filters from '@/wxat-common/utils/string-utils.wxs';
import pageLinkHandler from '@/wxat-common/components/decorate/utils/pageLinkHandler';

const liveImg = cdnResConfig.live;

interface GraphicsModuleWrapProps {
  dataSource: Record<string, any>;
  list?: Array<Record<string, any>>;
}

const GraphicsModuleWrap: FC<GraphicsModuleWrapProps> = ({ dataSource }) => {
  const [graphList, setGraphList] = useState([]);
  const [textNavSource, setTextNavSource] = useState({});

  // 组件显示时，重新获取一遍数据
  useDidShow(() => {
    // 打开模块时同步评论和点赞数
    fetchData();
  });

  useEffect(() => {
    // 设置装修设置 营销图文
    // const { linkPage } = pageLinkHandler.getLinkPage({
    //   linkPage: (dataSource && dataSource.textNavSource && dataSource.textNavSource.linkPage) || '',
    // });
    if(Taro.getEnv() === Taro.ENV_TYPE.WEB) fetchData();
    let linkPage = `${dataSource.textNavSource.linkPage}?graphicStyle=${dataSource.graphicStyle || 'title'}`;
    if (dataSource.graphicStyle === 'waterfall') {
      linkPage = `sub-packages/marketing-package/pages/graph/list/index?graphicStyle=title`;
    }

    setTextNavSource({
      ...dataSource.textNavSource,
      // linkPage:
      //   dataSource && dataSource.textNavSource
      //     ? dataSource.textNavSource.linkPage + `?graphicStyle=${dataSource.graphicStyle || 'title'}`
      //     : '',
      linkPage: linkPage ? `${linkPage}?graphicStyle=${dataSource.graphicStyle || 'title'}` : '',
    });

    // this.getList(this.getIds().join(","));
    // fetchData();
  }, [dataSource]);

  function getIds() {
    const data = dataSource.data;
    const ids: any[] = [];
    (data || []).forEach((item) => {
      ids.push(item.id);
    });
    return ids;
  }

  function getList(ids) {
    if (!ids.length) {
      setGraphList([]);
      return;
    }
    // Request URL: https://wkb.wakedata.com/cs/auth/market_article/query_available?ids=123,234,567
    // Response:  与 /market_article/query/list的一致， 屏蔽掉content字端，屏蔽已删除已下架的文章
    wxApi
      .request({
        url: api.article.availableList,
        quite: true,
        data: {
          ids,
        },
      })
      .then((res) => {
        const { success, data } = res;
        if (!success) {
          return;
        }
        if (!data || !data.length) {
          setGraphList([]);
        } else {
          const _data = data;
          _data.forEach((element) => {
            report.videoExposure(element);
          });
          setGraphList(_data);
        }
      })
      .catch((err) => {
        console.error('获取图文列表err', err);
      });
  }

  function fetchData() {
    getList(getIds().join(','));
  }

  function goDetail(e) {
    // FIXME:
    const item = e.detail.item;
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/graph/detail/index',
      data: { id: item.id },
    });
  }
  function jump(e) {
    // if (!authHelper.checkAuth()) return;
    // 上报营销图文的对应的图文点击量
    report.clickVideoGraphic(e.currentTarget.dataset);
    const id = e.currentTarget.dataset.id;
    const type = e.currentTarget.dataset.type;
    const videoList = graphList.filter((v) => v.articleType === 2);
    const index = videoList.findIndex((v) => v.id == id);
    console.log(222,id,type)
      if(Number(type) === 2){
        const data = dataSource.data;
        // this.setData({graphList: data});
        const ids:any[] = [];
        data.forEach(item => {
          ids.push(item.id);
        });
        wxApi.$navigateTo({
        url: `/sub-packages/marketing-package/pages/graph/video-detail/index`,
          data:{
          index,
          id,
          ids
          }
        })
      }else{
        wxApi.$navigateTo({
          url: `/sub-packages/moveFile-package/pages/graph/detail/index?id=${id}`
        });
      }
  }
  function goMore() {
    wxApi.$navigateTo({
      url: "/sub-packages/moveFile-package/pages/graph/list/index"
    });
  }

  return (
    <View
      data-scoped="wk-wcbg-GraphicsModule"
      className="wk-wcbg-GraphicsModule graphics"
      style={_safe_style_(
        'margin:' +
          (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
          ';border-radius: ' +
          dataSource.radius +
          'px;'
      )}
    >
      {!!(Array.isArray(graphList) && !!graphList.length) && (
        <View
          className='graphics'
          style={{
            margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
          }}
        >
          <View>
            {!!dataSource.showNav && <TextNavModule dataSource={textNavSource}></TextNavModule>}
            <View className='graphics-list'>
              {dataSource.graphicStyle !== 'waterfall' && (
                <Block>
                  {graphList?.map((item, index) => {
                    return (
                      <Block key={index}>
                        {dataSource.graphicStyle !== 'image' ? (
                          <View
                            className="graphics-list-item"
                            onClick={_fixme_with_dataset_(jump, {
                              id: item.id,
                              type: item.articleType,
                              mediaType: item.mediaType ? item.mediaType : '',
                            })}
                          >
                            <View>
                              <View className="graphics-list-item-title">{item.title}</View>

                              {!!item.commentCount && (
                                <Text className="graphics-list-item-comment">{item.commentCount+ '评论'}</Text>
                              )}

                              <Text className="graphics-list-item-comment">{`${item.virtualLikeCount}点赞`}</Text>
                            </View>
                            <Image
                              className="graphics-list-item-image"
                              mode="aspectFill"
                              src={item.coverUrl}
                            ></Image>
                          </View>
                        ) : (
                          <View
                            className="graphics-item"
                            onClick={_fixme_with_dataset_(jump, { id: item.id, type: item.articleType })}
                          >
                            <Image mode="aspectFill" src={item.coverUrl} className="image-cover cover"></Image>
                            <View className="main">
                              <View className="title">{item.title || ''}</View>
                              <View className="footer">
                                {item.digest && <Text className="pure-text content">{item.digest || ''}</Text>}

                                <View className="comment">
                                  <Image
                                    className="pure-image"
                                    mode="aspectFit"
                                    src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/comment.png"
                                  ></Image>
                                  <Text className="pure-text">{item.commentCount}</Text>
                                </View>
                                <View className="like">
                                  <Image
                                    className="pure-image"
                                    mode="aspectFit"
                                    src={
                                      !item.liked
                                        ? 'https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/like.png'
                                        : 'https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/graph/active-like.png'
                                    }
                                  ></Image>
                                  <Text className="pure-text">{item.virtualLikeCount}</Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        )}

                        {/*  图片优先样式  */}
                      </Block>
                    );
                  })}
                </Block>
              )}

              {/* -瀑布流优先样式- */}
              <View className="waterfallBox">
                {dataSource.graphicStyle == 'waterfall' && (
                  <View className="waterfall">
                    <View className="waterfall-list">
                      {graphList?.map((item, index) => {
                        return (
                          <View key={index} data-item={item} className="waterfall-list-item">
                            {index % 2 == 0 && (
                              <View
                                className="waterfall-item"
                                onClick={_fixme_with_dataset_(jump, {
                                  id: item.id,
                                  type: item.articleType,
                                  mediaType: item.mediaType ? item.mediaType : '',
                                })}
                              >
                                <View className="waterfall-content">
                                  <View className="waterfall-imgBox">
                                    <Image
                                      className="waterfall-item-image"
                                      mode="widthFix"
                                      src={item.coverUrl}
                                    ></Image>
                                    {item.articleType == 2 && (
                                      <View className="waterfall-video">
                                        <Image
                                          className="waterfall-video-img"
                                          mode="widthFix"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_video.svg"
                                        ></Image>
                                      </View>
                                    )}

                                    {!!item.labelPosition && (
                                      <View className="waterfall-location">
                                        <Image
                                          className="location-img"
                                          mode="widthFix"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/dingwei.svg"
                                        ></Image>
                                        <Text className="txt">{item.labelPosition}</Text>
                                      </View>
                                    )}
                                  </View>
                                  <View className="waterfall-text-content">
                                    <View className="waterfall-item-title">{item.title}</View>
                                    {!!item.labelOrdinary && (
                                      <View className="waterfall-item-tag">
                                        {!!filters.imgArrFormat(item.labelOrdinary) &&
                                          filters.imgArrFormat(item.labelOrdinary).map((item, index) => {
                                            return (
                                              <View key={index} data-item={item} className="waterfall-item-tag-v">
                                                <Text className="waterfall-item-tag-t">{item}</Text>
                                              </View>
                                            );
                                          })}
                                      </View>
                                    )}

                                    <View className="waterfall-item-comment">
                                      {!!item.labelPrice && (
                                        <View className="price">
                                          <Text className="priceNumber">{item.labelPrice}</Text>
                                        </View>
                                      )}

                                      {!!item.totalLikeCount && (
                                        <View className="totalLikeCount">
                                          <Image
                                            className="img"
                                            mode="widthFix"
                                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_ready.svg"
                                          ></Image>
                                          <Text className="txt">{item.totalLikeCount}</Text>
                                        </View>
                                      )}
                                    </View>
                                  </View>
                                </View>
                              </View>
                            )}
                          </View>
                        );
                      })}
                    </View>
                    <View className="waterfall-list">
                      {graphList?.map((item, index) => {
                        return (
                          <View key={index} data-item={item} className="waterfall-list-item">
                            {index % 2 == 1 && (
                              <View
                                className="waterfall-item"
                                onClick={_fixme_with_dataset_(jump, {
                                  id: item.id,
                                  type: item.articleType,
                                  mediaType: item.mediaType ? item.mediaType : '',
                                })}
                              >
                                <View className="waterfall-content">
                                  <View className="waterfall-imgBox">
                                    <Image
                                      className="waterfall-item-image"
                                      mode="widthFix"
                                      src={item.coverUrl}
                                    ></Image>
                                    {item.articleType == 2 && (
                                      <View className="waterfall-video">
                                        <Image
                                          className="waterfall-video-img"
                                          mode="widthFix"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_video.svg"
                                        ></Image>
                                      </View>
                                    )}

                                    {!!item.labelPosition && (
                                      <View className="waterfall-location">
                                        <Image
                                          className="location-img"
                                          mode="widthFix"
                                          src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/dingwei.svg"
                                        ></Image>
                                        <Text>{item.labelPosition}</Text>
                                      </View>
                                    )}
                                  </View>
                                  <View className="waterfall-text-content">
                                    <View className="waterfall-item-title">{item.title}</View>
                                    {!!item.labelOrdinary && (
                                      <View className="waterfall-item-tag">
                                        {!!filters.imgArrFormat(item.labelOrdinary) &&
                                          filters.imgArrFormat(item.labelOrdinary).map((item, index) => {
                                            return (
                                              <View key={index} data-item={item} className="waterfall-item-tag-v">
                                                <Text className="waterfall-item-tag-t">{item}</Text>
                                              </View>
                                            );
                                          })}
                                      </View>
                                    )}

                                    <View className="waterfall-item-comment">
                                      {!!item.labelPrice && (
                                        <View className="price">
                                          <Text className="priceNumber">{item.labelPrice}</Text>
                                        </View>
                                      )}

                                      {!!item.totalLikeCount && (
                                        <View className="totalLikeCount">
                                          <Image
                                            className="img"
                                            mode="widthFix"
                                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/components/icon_ready.svg"
                                          ></Image>
                                          <Text className="txt">{item.totalLikeCount}</Text>
                                        </View>
                                      )}
                                    </View>
                                  </View>
                                </View>
                              </View>
                            )}
                          </View>
                        );
                      })}
                    </View>
                  </View>
                )}
              </View>

            </View>
          </View>
        </View>
      )}
    </View>
  );
};

GraphicsModuleWrap.defaultProps = {
  list: [], // list: null //直接渲染list列表
  dataSource: {
    type: Object,
    value: null,
    observer(newVal) {
      if (newVal) {
        this.fetchData();
      }
    },
  },
  // 传dataSource只是取dataSource里面的id  进行再次查询
};

export default GraphicsModuleWrap;


