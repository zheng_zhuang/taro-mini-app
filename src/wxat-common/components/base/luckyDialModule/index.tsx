import React, { FC, useEffect, useState } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from "@/wxat-common/api";
import cdnResConfig from '@/wxat-common/constants/cdnResConfig.js';
import './index.scss';

// props的类型定义
interface ComponentProps {
  dataSource: Record<string, any>;
}

const LuckyDialModule: FC<ComponentProps> = ({ dataSource = {} }) => {
  // return <Text>{dataSource.id}</Text>;
  const [activityStatus, setActivityStatus] = useState(-1);
  let isUnMount = false
  useEffect(() => {
    if (dataSource && dataSource.data) {
      getActivityDetails();
    }
    return ()=>{
      isUnMount = true
    }
  }, [dataSource]);

  const getActivityDetails = () => {
    wxApi
      .request({
        url: api.luckyDial.getActivityDetails,
        quite: true,
        data: {
          luckyTurningId: dataSource.data.id,
        },
      })
      .then((res) => {
        if(!isUnMount){
          setActivityStatus(res.data.activityStatus);
        }
      });
  };
  const jump = () => {
    // if (!authHelper.checkAuth()) return;
    wxApi.$navigateTo({
      // url: `/sub-packages/marketing-package/pages/lucky-dial/index?id=${dataSource.data.id}`,
      url: `/sub-packages/moveFile-package/pages/lucky-dial/index?id=${dataSource.data.id}`,
    });
  };

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbl-LuckyDialModule'
      className='wk-cbl-LuckyDialModule'
    >
      {activityStatus === 3 && (
        <View
          style={{
            margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2
            )}`,
          }}
        >
          <View onClick={jump}>
            <Image
              className='lucky-dial-content-image'
              src={dataSource.img || cdnResConfig.lucky_dial}
              style={{
                borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
              }}
            ></Image>
          </View>
        </View>
      )}
    </View>
  );
};

// 给props赋默认值
LuckyDialModule.defaultProps = {
  dataSource: {},
};

export default LuckyDialModule;
