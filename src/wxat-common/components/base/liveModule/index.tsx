import React, { FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Navigator, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

// props的类型定义
type ComponentProps = {
  dataSource: Record<string, any>;
};

let LiveModule: FC<ComponentProps> = ({ dataSource }) => {
  return (
    <View data-scoped='wk-cbl-LiveModule' className='wk-cbl-LiveModule live-module'>
      <Navigator
        hoverClass='none'
        url={'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=' + dataSource.data.roomId}
      >
        <View
          style={{
            margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2
            )}`,
          }}
        >
          <Image
            className='poster'
            src={dataSource.data.src}
            mode='widthFix'
            style={{
              marginTop: Taro.pxTransform(dataSource.margin * 2 || 0),
              borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
            }}
          ></Image>
        </View>
      </Navigator>
    </View>
  );
};

// 给props赋默认值
LiveModule.defaultProps = {
  dataSource: {},
};

export default LiveModule;
