import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import protectedMailBox from '../../../utils/protectedMailBox';
import api from "../../../api";
import authHelper from '../../../utils/auth-helper';
import TextNavModule from "../../decorate/textNavModule";
import HoverCart from "../../../components/cart/hover-cart";
import './index.scss';

interface MicroDecorateModuleProps {
  dataSource: any;
}

interface MicroDecorateModule {
  props: MicroDecorateModuleProps;
}

class MicroDecorateModule extends React.Component {
  state = {
    renderList: [],
  };

  static defaultProps = {
    dataSource: {},
  };

  componentDidMount() {
    this.fetchData();
    Taro.eventCenter.on('midecorate-FetchData', () => {
      this.fetchData();
    });
  }

  componentWillUnmount() {
    Taro.eventCenter.off('midecorate-FetchData');
  }

  getIds = () => {
    const { dataSource } = this.props;
    const ids = dataSource.data.map((item: any) => item.id).join(',') || '';
    return ids;
  };

  fetchData = async () => {
    const idString = this.getIds();
    if (!idString.length) {
      this.setState({ renderList: [] });
      return;
    }

    const { success, data } = await wxApi.request({
      url: api.micro_decorate.decorateList,
      quite: true,
      data: {
        pageNo: 1,
        pageSize: 10,
        idString,
      },
    });

    if (!success) return;

    if (!data || !data.length) {
      this.setState({
        renderList: [],
      });
    } else {
      const renderList = (data || []).filter((item) => item.isShelf);
      this.setState({ renderList });
    }
  };

  openVrPreview = ({ currentTarget }) => {
    const vrLink = currentTarget.dataset.url;
    protectedMailBox.send('sub-packages/marketing-package/pages/receipt/index', 'receiptURL', vrLink);
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/receipt/index',
      data: {},
    });
  };

  onChangeLike = async ({ currentTarget }) => {
    if (!authHelper.checkAuth()) return false;
    const item = currentTarget.dataset.item;
    const data = { sceneModelId: item.id, likeStatus: item.isLiked ? 0 : 1 };

    await wxApi.request({
      url: api.micro_decorate.like,
      loading: true,
      data,
    });

    this.fetchData();
  };

  handleGoToDetail = ({ currentTarget }) => {
    const item = currentTarget.dataset.item;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/micro-decorate/detail/index',
      data: { id: item.id },
    });
  };

  render() {
    const { renderList } = this.state;
    const { dataSource } = this.props;

    return (
      <View
        data-scoped='wk-cbm-MicroDecorateModule'
        className='wk-cbm-MicroDecorateModule micro-decorate-module'
        style={_safe_style_(
          'margin:' + (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') + ';'
        )}
      >
        <HoverCart />
        {!!dataSource.showNav && <TextNavModule dataSource={dataSource.textNavSource}></TextNavModule>}
        {!!(renderList && renderList.length) && (
          <View className='micro-decorate-container'>
            {renderList.map((item: any, index) => {
              return (
                <View
                  className='decorate-item'
                  key={index}
                  style={_safe_style_('border-radius:' + dataSource.radius + 'px;')}
                >
                  <Image
                    className='item-img'
                    onClick={_fixme_with_dataset_(this.handleGoToDetail, { item: item })}
                    src={item.thumbnail}
                    mode='aspectFill'
                    style={_safe_style_('border-radius:' + dataSource.radius + 'px;')}
                  ></Image>
                  {!!item.vrUrl && (
                    <Image
                      className='vr-btn'
                      onClick={_fixme_with_dataset_(this.openVrPreview, { url: item.vrUrl })}
                      src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/view.png'
                    ></Image>
                  )}

                  <View className='item-tit'>{item.name}</View>
                  <View className='bottom-info'>
                    <View className='item-icon'>
                      <Image
                        className='icon-img'
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/order.png'
                        mode='aspectFill'
                      ></Image>
                      {item.appointmentNum + '人预约'}
                    </View>
                    <View className='item-icon' style={_safe_style_('margin-left:20px')}>
                      <Image
                        className='icon-img'
                        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/look.png'
                        mode='aspectFill'
                      ></Image>
                      {item.viewNum}
                    </View>
                    <View
                      style={_safe_style_('flex:1;text-align:right')}
                      onClick={_fixme_with_dataset_(this.onChangeLike, { item: item })}
                      className='item-icon icon-like'
                    >
                      {item.isLiked ? (
                        <Image
                          className='icon-img'
                          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like-checked.png'
                        ></Image>
                      ) : (
                        <Image
                          className='icon-img'
                          src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/micro-decorate/like.png'
                          mode='aspectFill'
                        ></Image>
                      )}

                      {item.approvalNum}
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        )}
      </View>
    );
  }
}

export default MicroDecorateModule;
