import React, { FC, useState, useEffect, useMemo } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';

import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';
import config from '../utils/config.js';

import HaggleModule from '../../decorate/haggleModule/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';
import { NOOP_OBJECT } from '@/wxat-common/utils/noop';

const PADDING = config.PADDING;

type HaggleModuleWrapProps = {
  dataSource: Record<string, any>;
  list?: Array<Record<string, any>>;
};

let HaggleModuleWrap: FC<HaggleModuleWrapProps> = ({ dataSource, list }) => {
  const [finalList, setFinalList] = useState<Array<Record<string, any>>>(list);
  const showType = (dataSource && dataSource.showType) || 0;
  const textNavSource = useMemo(() => {
    return dataSource ? { ...dataSource.textNavSource, showType } : NOOP_OBJECT;
  }, [dataSource, showType]);

  function getList() {
    const data = dataSource.data;
    if (data && data.length) {
      const activityItemQueryList = data.map((item) => ({
        itemNo: item.itemNo,
        skuId: item.skuId,
      }));

      wxApi
        .request({
          url: api.cut_price.itemNosSkuIdToList,
          method: 'POST',
          data: activityItemQueryList,
        })
        .then((res) => {
          console.log('res.data --> ', res);
          if (res.success && res.data) {
            setFinalList(res.data);
          }
        });
    }
  }

  useEffect(() => {
    if (!list || !list.length) {
      getList();
    }
  }, [dataSource, list]);

  function handleClick(e) {
    const item = e;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/cut-price/detail/index',
      data: {
        activityId: item.id,
        detail: e,
      },
    });
  }

  return (
    <View data-scoped='wk-cbh-HaggleModule' className='wk-cbh-HaggleModule'>
      {!!(!!finalList && !!finalList.length) && (
        <View
          className='haggle-module'
          style={{
            margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
          }}
        >
          {!!dataSource.showNav && <TextNavModule dataSource={textNavSource} padding={PADDING}></TextNavModule>}
          <View>
            <HaggleModule dataList={finalList} showType={showType} onClick={handleClick}></HaggleModule>
          </View>
        </View>
      )}
    </View>
  );
};

HaggleModuleWrap.defaultProps = {
  dataSource: {}, //传dataSource只是取dataSource里面的id  进行再次查询
};

export default HaggleModuleWrap;
