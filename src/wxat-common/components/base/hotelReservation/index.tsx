import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { View, Image, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import { connect } from 'react-redux';
import dateUtil from '@/wxat-common/utils/date.js';
import template from '@/wxat-common/utils/template.js';
import H5UrlEnum from '@/wxat-common/constants/H5UrlEnum.js';
import authHelper from '@/wxat-common/utils/auth-helper';
import Icalendar from "../../calendar"
import store from '@/store';
import { updateBaseAction } from '@/redux/base';

import './index.scss';

interface ComponentProps {
  dataSource: Object;
}

interface ComponentState {
  tmpStyle: any;
  showcalendar: boolean;
  inTime: string;
  outTime: string;
  DaysBetween: string | any;

  showinTime: string;
  showoutTime: string;
  outValue: string;
  address: string;

  title: string;
  longitude: any;
  latitude: any;
  defalutTime: any;
  tabSelectedId: number;
}

const mapStateToProps = (state) => {
  return {
    gps:  state.base.gps,
    userInfo: state.base.userInfo,
    appInfo: state.base.appInfo,
    storeVO: state.base.storeVO,
    loginInfo: state.base.loginInfo,
    enterpriseStaff: state.base.enterpriseStaff,
    enterpriseStaffInfo: state.base.enterpriseStaffInfo,
    currentStore:state.base.currentStore
  };
};
const mapDispatchToProps = () => ({});
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class HotelReservation extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    dataSource: null,
  };

  state = {
    tmpStyle: {}, // 主题模板配置
    showcalendar: false,
    inTime: '',
    outTime: '',
    DaysBetween: '',
    showinTime: '',
    showoutTime: '',
    outValue: '',
    address: '',
    title: '',
    longitude: null,
    latitude: null,
    defalutTime: {
      inTime: '',
      outTime: '',
    },
    tabSelectedId: 1
  };

  componentDidMount() {
    this.getTemplateStyle(); // 获取主题模板配置
    this.getthisLocation();
    const inTime = dateUtil.getNowDay();
    const outTime = dateUtil.getDay(1);
    const showinTime = this.MonthDayformat(inTime);
    const showoutTime = this.MonthDayformat(outTime);
    const DaysBetween = this.getDaysBetween(inTime, outTime);
    const outValue = this.getWeekDate(outTime);
    this.setState({
      inTime,
      outTime,
      showinTime,
      showoutTime,
      DaysBetween,
      outValue,
    });
    if (!!this.props.enterpriseStaff) {
      this.setState({
        tabSelectedId: 2
      })
    }else{
      this.setState({
        tabSelectedId: 1
      })
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!!nextProps.enterpriseStaff) {
      this.setState({
        tabSelectedId: 2
      })
    }else{
      this.setState({
        tabSelectedId: 1
      })
    }
  }
  componentWillUnMount = () => {
    this.setState = (state, callback) => {

    };
  };

  // 获取主题
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  checkDate = () => {
    this.setState({
      'defalutTime.inTime': this.state.inTime,
      'defalutTime.outTime': this.state.outTime,
      showcalendar: true,
    });
  }

  closeDialog = (e) => {
    if (e.ishasValue) {
      const inTime = e.inTime;
      const outTime = e.outTime;
      const showinTime = this.MonthDayformat(inTime);
      const showoutTime = this.MonthDayformat(outTime);
      const outValue = this.getWeekDate(outTime);
      this.setState({
        showcalendar: false,
        inTime,
        outTime,
        showinTime,
        showoutTime,
        outValue,
        DaysBetween: e.DaysBetween,
      });
    } else {
      this.setState({
        showcalendar: false,
      });
    }
  }

  // 定位经纬度＋地址解析
  wxLocation() {
    const { longtitude, latitude } = this.props.gps;
    return wxApi
      .request({
        url: api.poi.location + '?lng=' + longtitude + '&lat=' + latitude,
        loading: true,
      })
      .then((res) => {
        const data = res.data || {};
        this.setState({
          address: data.address || '',
          title: data.title || '',
        });
      });
  }

  // 获取当前位置
  getthisLocation() {
    this.wxLocation().finally(() => {
    });
    return;
    Taro.onLocationChange({
      type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        const longitude = res.longitude;
        const latitude = res.latitude;
        that.setState({
          longitude,
          latitude,
        });

        return wxApi
          .request({
            url: api.poi.location + '?lng=' + longitude + '&lat=' + latitude,
            loading: true,
          })
          .then((res) => {
            const data = res.data || {};
            that.setState({
              address: data.address || '',
              title: data.title || '',
            });
          });
      },
      fail: function () {
        // fail
      },
      complete: function () {
        // complete
      },
    });
  }

  // 选择当前位置
  clickLocation() {
    const that = this;
    Taro.getLocation({
      type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        // success
        // 成功后选择位置
        Taro.chooseLocation({
          success: function (res) {
            const data = res || [];
            that.setState({
              address: data.address || '',
              title: data.name || '',
              longitude: data.longitude || '',
              latitude: data.latitude || '',
            });
          },
          fail: function () {
            // fail
          },
          complete: function () {
            // complete
          },
        });
      },
      fail: function () {
        Taro.showToast(
          {
            title: '请开启定位设置'
          }
        )
      },
      complete: function () {
        // complete
      },
    });
  }

  // 跳转去易企住酒店列表
  checkoutHotels = () => {
    const { userInfo, appInfo, storeVO, loginInfo,currentStore } = this.props;
    const { inTime, outTime } = this.state;
    const { longtitude, latitude } = this.props.gps;
    const is_company = this.props.dataSource.reservationType==='addAgreementCompany' ? 1 : 2;
    const user_type = this.state.tabSelectedId=== 1 ? 2 : 1;
    const openId = userInfo?.wxOpenId || loginInfo?.openId;
    if (!authHelper.checkAuth()) return;

    // window.location.href=`${H5UrlEnum.H5_URL}${H5UrlEnum.HOTELLISTPAGE.url}?openid=${openId}&name=${userInfo.nickname}&phone=${userInfo.phone}&avatar=${userInfo.avatarImgUrl}&env=H5&user_id=${userInfo.id}&lat=${latitude}&lng=${longtitude}&start_date=${inTime}&end_date=${outTime}&user_type=${user_type}&hotel_contract=${currentStore.outStoreId||''}&hotel_brand=${appInfo.outAppId||''}&is_company=${is_company}&aid=${loginInfo.wxAppId}&h_status=other&brand_id=${loginInfo.appId}&storeId=${currentStore.id}`
    wxApi.$locationTo(`${H5UrlEnum.H5_URL}${H5UrlEnum.HOTELLISTPAGE.url}?openid=${openId}&name=${userInfo.nickname}&phone=${userInfo.phone}&avatar=${userInfo.avatarImgUrl}&env=H5&user_id=${userInfo.id}&lat=${latitude}&lng=${longtitude}&start_date=${inTime}&end_date=${outTime}&user_type=${user_type}&hotel_contract=${currentStore.outStoreId||''}&hotel_brand=${appInfo.outAppId||''}&is_company=${is_company}&aid=${loginInfo.wxAppId}&h_status=other&brand_id=${loginInfo.appId}&storeId=${currentStore.id}`)
  };

  // 跳转去易企住企业注册
  registerLink() {
    const { userInfo, appInfo, storeVO, loginInfo,currentStore } = this.props;
    const { longitude, latitude, inTime, outTime } = this.state;
    const is_company = this.props.dataSource.reservationType==='addAgreementCompany' ? 1 : 2;
    const user_type = this.state.tabSelectedId=== 1 ? 2 : 1;
    const openId = userInfo?.wxOpenId || loginInfo?.openId;
    if (!authHelper.checkAuth()) return;
    // TODO 处理
    // window.location.href=`${H5UrlEnum.H5_URL}${H5UrlEnum.REGISTERPAGE.url}?openid=${openId}&name=${userInfo.nickname}&phone=${userInfo.phone}&avatar=${userInfo.avatarImgUrl}&env=H5&user_id=${userInfo.id}&lat=${latitude}&lng=${longitude}&start_date=${inTime}&end_date=${outTime}&user_type=${user_type}&hotel_contract=${currentStore.outStoreId||''}&hotel_brand=${appInfo.outAppId||''}&is_company=${is_company}&aid=${loginInfo.wxAppId}&h_status=index&brand_id=${loginInfo.appId}&storeId=${currentStore.id}`
    wxApi.$locationTo(`${H5UrlEnum.H5_URL}${H5UrlEnum.REGISTERPAGE.url}?openid=${openId}&name=${userInfo.nickname}&phone=${userInfo.phone}&avatar=${userInfo.avatarImgUrl}&env=H5&user_id=${userInfo.id}&lat=${latitude}&lng=${longitude}&start_date=${inTime}&end_date=${outTime}&user_type=${user_type}&hotel_contract=${currentStore.outStoreId||''}&hotel_brand=${appInfo.outAppId||''}&is_company=${is_company}&aid=${loginInfo.wxAppId}&h_status=index&brand_id=${loginInfo.appId}&storeId=${currentStore.id}`)
  }

  getDaysBetween(dateString1, dateString2) {
    const startDate = Date.parse(dateString1);
    const endDate = Date.parse(dateString2);
    if (startDate > endDate) {
      return 0;
    }
    if (startDate == endDate) {
      return 1;
    }
    const days = (endDate - startDate) / (1 * 24 * 60 * 60 * 1000);
    return days;
  }

  // 格式化日期，只显示月份和日
  MonthDayformat(date) {
    const Dtime = new Date(date);
    const Dtimemonth = Dtime.getMonth() + 1;

    // Dtimemonth < 10 ? '0' + Dtimemonth : Dtimemonth
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}月${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }日`;
  }

  // 获取当前是周几
  getWeekDate(dateDay) {
    const now = new Date(dateDay);
    const day = now.getDay();
    const weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];

    const week = weeks[day];
    return week;
  }

  // 切换酒店预订企业/个人身份
  tabSelectTap(tabSelectedId) {
    console.log('tabSelectedId',tabSelectedId);

    this.setState({
      tabSelectedId
    }, () => {
      if (tabSelectedId === 2) {
        this.outStaff();

      }
    })
  }

  outStaff() {
    wxApi.request({
        url: api.user.outStaff,
        data: {}
    })
    .then(res => {
        if (!!res.data && !!res.data.id) {
          store.dispatch(updateBaseAction({
            enterpriseStaff: res.data.id? true: false,
            enterpriseStaffInfo: res.data
          }));
          return true
        } else {
          this.registerLink();
          return false
        }
    })
    .catch(error => {
        console.log("outStaff: error: " + JSON.stringify(error));
        return false
    });
  }

  render() {
    const { address, showinTime, outValue, showoutTime, DaysBetween, defalutTime, showcalendar, tabSelectedId } = this.state;
    const { dataSource } = this.props;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcbh-HotelReservation'
        className='wk-wcbh-HotelReservation'
      >
        <View
          className='hotel-reservation'
          style={_safe_style_('background-image:url(' + dataSource.backgroundUrl + ');')}
        >
          <View
            className='hotel-reservation-box'
            style={_safe_style_(
              'margin:' +
                (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
                ';border-radius: ' +
                dataSource.radius +
                'px;'
            )}
          >
            { dataSource.reservationType === 'addAgreementCompany' && (
              <View className="hotel-reservation-item">
                <View className="hotel-reservation-tab">
                    <View className="tab-item" onClick={()=>{this.tabSelectTap(1)}} style={_safe_style_('background:' + (tabSelectedId===1 ? dataSource.btnColor : ''))}>
                        <View className="tabItemText" style={_safe_style_('color:' + (tabSelectedId===1 ? dataSource.titleColor : ''))}>个人</View>
                    </View>
                    <View className="tab-item" onClick={()=>{this.tabSelectTap(2)}} style={_safe_style_('background:' + (tabSelectedId===2 ? dataSource.btnColor : ''))}>
                        <View className="tabItemText" style={_safe_style_('color:' + (tabSelectedId===2 ? dataSource.titleColor : ''))}>企业用户</View>
                    </View>
                </View>
              </View>
            )}
            <View className='hotel-reservation-item'>
              <View className='hotel-location'>{address}</View>
              <View className='clickLocation' onClick={this.clickLocation}>
                <Image
                  className='store-location-icon'
                  src='https://front-end-1302979015.file.myqcloud.com/images/c/images/gun-location.png'
                  mode='aspectFit'
                ></Image>
                <View>我的位置</View>
              </View>
            </View>
            <View className='hotel-reservation-item' onClick={this.checkDate}>
              <View className='form_group'>
                <Text>今天入住</Text>
                <View className='textss' style={_safe_style_('text-align:left;')}>
                  {showinTime}
                </View>
              </View>
              <View className='form_group'>
                <Text></Text>
                <View>
                  <View className='xian'></View>
                </View>
              </View>
              <View className='form_group'>
                <Text>{outValue + '离店'}</Text>
                <View className='textss' style={_safe_style_('text-align:left;')}>
                  {showoutTime}
                </View>
              </View>
              <View className='form_group form_count'>
                <Text></Text>
                <View className='count' style={_safe_style_('text-align:right;')}>
                  {'共' + DaysBetween + '晚'}
                </View>
              </View>
            </View>
            <View
              className='checkOutBtn hotel-reservation-item'
              onClick={this.checkoutHotels}
              style={_safe_style_('background:' + dataSource.btnColor)}
            >
              <View style={_safe_style_('color: ' + dataSource.titleColor + ';')} className='checkOutBtntext'>
                {dataSource.btnText ? dataSource.btnText : '酒店预订'}
              </View>
            </View>
          </View>
        </View>
        <Icalendar
          className="calendar"
          defalutTime={defalutTime}
          showcalendar={showcalendar}
          onMyevent={this.closeDialog}
        ></Icalendar>
      </View>
    );
  }
}

export default HotelReservation;
