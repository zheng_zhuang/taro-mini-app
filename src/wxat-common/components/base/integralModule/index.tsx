import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';

import handleData from '../utils/handleData.js';
import wxApi from '../../../utils/wxApi';
import api from '../../../api';
import config from '../utils/config.js';
import IntegralModule, { IntegralItem } from '../../decorate/integralModule';
import TextNavModule from '../../decorate/textNavModule';
import './index.scss';
import {useSelector} from 'react-redux'
const PADDING = config.PADDING;

interface IntegralModuleWrapProps {
  dataSource: Record<string, any>;
  list?: IntegralItem[];
}

const IntegralModuleWrap: FC<IntegralModuleWrapProps> = ({ dataSource, list }) => {
  const [renderList, setRenderList] = useState(list || []);
  const scoreName = useSelector(state => state.globalData.scoreName)
  let isUnMount = false
  useEffect(() => {
    /**
     * 获取积分商品列表
     */
    function getList() {
      const data = dataSource ? dataSource.data : null;

      if (data && data.length) {
        const params = {
          type: 22,
        };
        const itemNos = handleData.pickUpItemNos(data, 'id');
        itemNos.forEach((item, index) => {
          params['activityIdList[' + index + ']'] = item;
        });
        wxApi
          .request({
            url: api.goods.integralQuery,
            data: params,
          })
          .then((res) => {
            if (res.data && !isUnMount) {
              setRenderList(res.data || []);
            }
          });
      }
    }

    if (!list || !list.length) {
      getList();
    }
    return ()=>{
      isUnMount = true
    }
  }, [dataSource]);

  // todo... click
  function click(e) {
    // FIXME:
    const item = e.detail.item;
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/group/detail/index',
      data: {
        activityId: item.id,
        itemNo: item.itemNo,
      },
    });
  }

  return (
    <View data-scoped='wk-cbi-IntegralModule' className='wk-cbi-IntegralModule'>
      {!!(renderList && renderList.length) && (
        <View
          className='coupons-module'
          style={{
            margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
          }}
        >
          {!!dataSource.showNav && <TextNavModule dataSource={dataSource.textNavSource}></TextNavModule>}
          <View>
            <IntegralModule scoreName={scoreName} dataSource={dataSource} PADDING={PADDING} _renderList={renderList}></IntegralModule>
          </View>
        </View>
      )}
    </View>
  );
};

IntegralModuleWrap.defaultProps = {
  list: [], // list: null //直接渲染list列表
  dataSource: {}, // 传dataSource只是取dataSource里面的id  进行再次查询
};

export default IntegralModuleWrap;
