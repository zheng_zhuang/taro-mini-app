import React, {useState, useEffect, FC} from 'react';
import {View, Image, Text} from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import { useSelector } from 'react-redux';
import { formatLocation } from '@/wxat-common/utils/location';
import dateUtil from '@/wxat-common/utils/date';
import filters from '@/wxat-common/utils/money.wxs';
import TextNavModule from '../../decorate/textNavModule/index';
import distance from '@/wxat-common/utils/distance';
import './index.scss';
import useToYQZ from '@/hooks/useToYQZ';
import {_fixme_with_dataset_} from "@/wxat-common/utils/platform";

type IProps = {
  reportSource?: string;
  dataSource?: Record<string, any>;
  list?: Array<{}>;
  onClick?: Function;
  backgroundColor?: string;
};

const mapStateToProps = ({ base }) => ({
  gps: base.gps
});

let RecommendHotelModule: FC<IProps> = (props) => {
  const {dataSource} = props;
  const { gps } = useSelector(mapStateToProps);
  const [recommendList, setRecommendList] = useState([])
  const { toYQZHotelList, toYQZHotelDetail } = useToYQZ()

  useEffect(() => {
    getRecommendHotelList()
  }, [gps])

  async function getRecommendHotelList () {
    try {
      const { latitude, longtitude } = gps
      const { city, cityCode, lat, lng } = await formatLocation(latitude, longtitude)
      const params = {
        cityName: city,
        cityCode: cityCode,
        checkInData: dateUtil.getDay(0),
        checkOutData: dateUtil.getDay(1),
        latitude: lat,
        longitude: lng,
        tags: ['4'],
        pageNo: 1,
        pageSize: 10
      }
      const { data } = await wxApi
        .request({
          url: api.hotel.getRecommendList,
          loading: true,
          data: params,
          method: 'POST'
        })
      setRecommendList(data)
    } catch (error) {
      console.log('error: ', error);
    }
  }
  function handleToYQZHotelList () {
    toYQZHotelList()
  }
  function handleToHotelDetail (e) {
    const item = e.currentTarget.dataset
    toYQZHotelDetail(item)
  }

  return (
    <View className='recommend-hotel-module'
      style={{
        padding: `${Taro.pxTransform(dataSource?.marginUpDown * 2)} ${Taro.pxTransform(
          dataSource?.marginLeftRight * 2
        )}`,
        borderRadius: Taro.pxTransform(dataSource?.radius * 2 || 0),
      }}
    >
      {
        dataSource?.showNav && (
          <TextNavModule
            dataSource={dataSource.textNavSource}
            onClick={handleToYQZHotelList}
          ></TextNavModule>
      )}
      <View className='recommend-hotel-list'>
        {
          recommendList && recommendList.length > 0 && recommendList.map((item: any) => {
            return (
              <View className='recommend-hotel-item' key={item.itemHotelNo} onClick={_fixme_with_dataset_(handleToHotelDetail, item)}>
                <View className="hotel-image">
                  <Image mode="aspectFill" className='hotel-pic' src={item.mainPic}></Image>
                  <View className="hotel-type">
                    <View className="type">{ item.types == 1 ? '民宿公寓' : '酒店' }</View>
                    <View className="line"></View>
                    {/* <u-icon className="u-m-r-8" name="map-fill" color="#ffffff" size="24"></u-icon> */}
                    <View className="distance">距您直线{ distance(item.distance * 10) }</View>
                  </View>
                </View>
                <View className="hotel-info">
                  <View className="hotel-title limit-line line-2">
                    { item.hotelName }
                  </View>
                  <View className="oneRowTwo-container">
                    <View className="hotel-price">
                      <Text>￥</Text>
                      <Text>{ filters.moneyFilter(item.price || 0, true)}</Text>
                      <Text>起</Text>
                    </View>
                    <View className="hotel-sold">
                      关注{ item.orderCount + item.collectCount }
                    </View>
                  </View>
                </View>

              </View>
            )
          })
        }
      </View>
    </View>
  )
}


RecommendHotelModule.defaultProps = {
  dataSource: {},
  list: [],
  reportSource: ''
  //item点击回调函数
};

export default RecommendHotelModule;