import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/goods-module/index.js
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import handleData from '../utils/handleData.js';
import config from '../utils/config.js';
// import report from '@/sdks/buried/report/index.js';
import reportConstants from '@/sdks/buried/report/report-constants.js';

import GoodsItem from '../../industry-decorate-style/goods-item/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';

// props的类型定义
type ComponentProps = {
  dataSource: Record<string, any>;
  reportSource?: string;
  display?: string;
};

let SeriesModule: FC<ComponentProps> = ({ dataSource, reportSource, display }) => {
  const defaultRenderList: Array<Record<string, any>> = [];
  const [_renderList, setRenderList] = useState(defaultRenderList);

  useEffect(() => {
    if (!!dataSource) {
      getList();
    }
  }, [dataSource]);

  const getList = () => {
    const data = dataSource.data;
    if (data && data.length > 0) {
      const itemNos = handleData.pickUpItemNos(data, 'itemNo').join(',');
      const params = {
        itemNoList: itemNos,
        type: 29,
      };

      wxApi
        .request({
          url: api.classify.itemSku,
          data: params,
        })
        .then((res) => {
          const obj = res.data;

          obj.forEach((item) => {
            item.display = dataSource.display;
          });

          setRenderList(obj);
        })
        .catch((error) => {
          console.log('get product: error: ' + JSON.stringify(error));
        });
    }
  };
  const getClassByShowType = (type) => {
    switch (dataSource.display) {
      case 'vertical':
        return 'vertical-' + type;
        break;
      case 'horizon':
        return 'horizon-' + type;
        break;
      case 'oneRowOne':
        return 'oneRowOne-' + type;
        break;
      case 'oneRowTwo':
        return 'oneRowTwo-' + type;
        break;
      case 'oneRowThree':
        return 'oneRowThree-' + type;
        break;
    }
  };
  const click = (e) => {
    // FIXME:
    const itemNo = e.detail.item.itemNo;
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: itemNo,
      },
    });
  };

  // PADDING: config.PADDING,
  // defaultReportSource: reportConstants.SOURCE_TYPE.product.key,
  // itemClass: 'vertical-item',
  // containerClass: 'vertical-container'

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbs-SeriesModule'
      className='wk-cbs-SeriesModule'
    >
      {!!(Array.isArray(_renderList) && _renderList.length > 0) && (
        <View
          className='series-module'
          style={{
            padding: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 20)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2 || 20
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
            background: dataSource.backgroundColor || '',
          }}
        >
          {!!dataSource.showNav && (
            <TextNavModule dataSource={dataSource.textNavSource} padding={config.PADDING}></TextNavModule>
          )}

          {/*  一行1个  */}
          <View className={getClassByShowType('container') || 'vertical-container'}>
            {_renderList.map((item, index) => {
              return (
                <GoodsItem
                  goodsItem={item}
                  key={'index' + { index }}
                  className={getClassByShowType('item') || 'vertical-item'}
                  reportSource={reportSource ? reportSource : reportConstants.SOURCE_TYPE.product.key}
                  display={dataSource.display}
                ></GoodsItem>
              );
            })}
          </View>
        </View>
      )}
    </View>
  );
};

// 给props赋默认值
SeriesModule.defaultProps = {
  dataSource: {},
  reportSource: '',
  display: 'vertical',
};

export default SeriesModule;
