import React, { FC, useState, useEffect } from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Button, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';

import getShopGuideInfo from '../../../utils/shopping-guide.js';
import screen from '../../../utils/screen';
import cdnResConfig from '../../../../wxat-common/constants/cdnResConfig.js';
import authHelper from '../../../utils/auth-helper.js';

import './index.scss';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import { useSelector } from 'react-redux';
import wxApi from '../../../../wxat-common/utils/wxApi';

const defaultSGImage = cdnResConfig.dialog.dialogShoppingGuide,
  defaultMsgImage = cdnResConfig.dialog.dialogMsg,
  defaultCollapse = cdnResConfig.dialog.dialogCollapse;

type stateDefine = {
  base: Record<string, any>;
};

type DialogModuleWrapProps = {
  dataSource: Record<string, any>;
};

let DialogModuleWrap: FC<DialogModuleWrapProps> = ({ dataSource }) => {
  const tabbarHeight = screen.tabbarHeight;
  const tmpStyle = useTemplateStyle();
  const [expand, setExpand] = useState(false);
  const [source, setSource] = useState(null);
  const [registered, setRegistered] = useState(false);
  const appInfo = useSelector((state: stateDefine) => state.base.appInfo);

  useEffect(() => {
    setRegistered(authHelper.checkAuth(false));

    getShopGuideMessage();
  }, []);

  // todo... 待后续调整
  // on: {
  //   stateChange(data) {
  //     //切换店铺
  //     if (data && data.key === 'currentStore') {
  //       getShopGuideMessage()
  //     }
  //   }
  // },

  function toggle() {
    setExpand(!expand);
  }

  function getShopGuideMessage() {
    getShopGuideInfo().then((res) => {
      setSource(res);
    });
  }

  function auth() {
    setRegistered(authHelper.checkAuth());
  }

  function gotoContact() {
    wxApi.$navigateTo({ url: '/sub-packages/guide-package/pages/contact-webview/index' });
  }

  // 非微信小程序不支持在线客服
  if (process.env.TARO_ENV !== 'weapp') {
    return null;
  }

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbd-DialogModule'
      className='wk-cbd-DialogModule'
    >
      <View className='dialog-trigger' style={{ bottom: tabbarHeight + 10 + 'px' }}>
        {/* TATA木门appid=824接客户三方客服，web-view形式 */}
        {appInfo.appId == 824 ? (
          registered ? (
            <Button className='image-btn' onClick={gotoContact}>
              <Image
                mode='aspectFill'
                src={dataSource.msgImage || defaultMsgImage}
                className='shopping-guide image'
                style={{
                  background: tmpStyle.btnColor,
                }}
              ></Image>
            </Button>
          ) : (
            <Button className='image-btn' onClick={auth}>
              <Image
                mode='aspectFill'
                src={dataSource.msgImage || defaultMsgImage}
                className='shopping-guide image'
                style={{
                  background: tmpStyle.btnColor,
                }}
              ></Image>
            </Button>
          )
        ) : (
          <Block>
            {registered ? (
              <Button className='image-btn' openType='contact'>
                {!!(
                  (dataSource.shoppingGuide && expand && source) ||
                  (!dataSource.wechatMsg && dataSource.shoppingGuide && source)
                ) && (
                  <Image
                    mode='aspectFill'
                    src={dataSource.sgImage || defaultSGImage}
                    className='shopping-guide image'
                    style={{
                      background: tmpStyle.btnColor,
                    }}
                  ></Image>
                )}
              </Button>
            ) : (
              <Button className='image-btn' onClick={auth}>
                {!!(
                  (dataSource.shoppingGuide && expand && source) ||
                  (!dataSource.wechatMsg && dataSource.shoppingGuide && source)
                ) && (
                  <Image
                    mode='aspectFill'
                    src={dataSource.sgImage || defaultSGImage}
                    className='shopping-guide image'
                    style={{
                      background: tmpStyle.btnColor,
                    }}
                  ></Image>
                )}
              </Button>
            )}

            {!!(
              (dataSource.wechatMsg && expand) ||
              (dataSource.wechatMsg && !(dataSource.shoppingGuide && source))
            ) && (
              <Button className='image-btn' openType='contact'>
                <Image
                  mode='aspectFill'
                  className='wechat-msg image'
                  src={dataSource.msgImage || defaultMsgImage}
                  style={{
                    background: tmpStyle.btnColor,
                  }}
                ></Image>
              </Button>
            )}

            {!!(dataSource.wechatMsg && dataSource.shoppingGuide && source) && (
              <View className='image-btn' onClick={toggle}>
                <Image
                  mode='aspectFill'
                  className='image'
                  src={(expand ? dataSource.expandImage : dataSource.collapseImage) || defaultCollapse}
                  style={{
                    background: tmpStyle.btnColor,
                  }}
                ></Image>
              </View>
            )}
          </Block>
        )}
      </View>
    </View>
  );
};

DialogModuleWrap.defaultProps = {
  dataSource: {},
};

export default DialogModuleWrap;
