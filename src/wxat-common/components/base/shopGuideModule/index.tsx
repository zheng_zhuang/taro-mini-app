import React, { FC, useState, useEffect, useMemo } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import '@/wxat-common/utils/platform';
import { Block, View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
// wxat-common/components/base/shopGuideModule/index.js
import getShopGuideInfo from '../../../utils/shopping-guide.js';
import authHelper from '../../../utils/auth-helper.js';
import { useSelector } from 'react-redux';
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import { useSk } from '@/hooks/use-sk';

const commonImg = cdnResConfig.common;

// props的类型定义
type ComponentProps = {
  dataSource: Record<string, any>;
  showType?: number;
  shareParams: object | undefined;
};

type stateDefine = {
  base: Record<string, any>;
};

/**
 * FIXME: 客服消息只支持微信
 */
let ShopGuideModule: FC<ComponentProps> = ({ dataSource, shareParams }) => {
  const defaultSource: Record<string, any> = {};
  const [source, setSource] = useState(defaultSource);
  const [registered, setRegistered] = useState(false);

  const [shareUserId, setShareUserId] = useState('');
  const userId = (shareParams && (shareParams._ref_useId || shareParams.refUseId)) || '';
  userId && setShareUserId(userId);

  const currentStore = useSelector((state: stateDefine) => state.base.currentStore);
  const getShopGuideMessage = (shareUserId: string) => {
    getShopGuideInfo(shareUserId).then((res) => {
      setSource(res);
    });
  };

  // useSk((sk) => {
  //   // if (sk) {
  //     const userId = sk._ref_useId || sk.refUseId || ''
  //     setShareUserId(userId);
  //     // getShopGuideMessage(userId);
  //   // }
  // })

  useEffect(() => {
    // if(authHelper.checkAuth()) {
    getShopGuideMessage(shareUserId);
    // }
  }, [currentStore, shareUserId]);

  const auth = () => {
    setRegistered(authHelper.checkAuth());
  };

  const contact = () => {
    wxApi.makePhoneCall({
      phoneNumber: source.mobile,
    });
  };

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbs-ShopGuideModule'
      className='wk-cbs-ShopGuideModule'
    >
      <View className='shopGuide'>
        <Block>
          {!!(dataSource.cardStyle === 3 && source && source.name) && (
            <View className='business-card'>
              <View className='business-card-content'>
                <Image src={source.avatar ? source.avatar : commonImg.noAvatar} className='picture'></Image>
                <View className='content'>
                  <Text className='name'>{source.name + '\n'}</Text>
                  <Text className='position'>{source.position}</Text>
                </View>
              </View>
              <View>
                <Image className='contact' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/sg-contact.png" onClick={contact}></Image>
                {process.env.TARO_ENV === 'weapp' && (
                  <Block>
                    {registered ? (
                      <Button className='image-btn' openType='contact'>
                        <Image className='message' src="https://bj.bcebos.com/htrip-mp/static/app/images/common/sg-message.png"></Image>
                      </Button>
                    ) : (
                      <Button className='image-btn'>
                        <Image
                          className='message'
                          src="https://bj.bcebos.com/htrip-mp/static/app/images/common/sg-message.png"
                          onClick={auth}
                        ></Image>
                      </Button>
                    )}
                  </Block>
                )}
              </View>
            </View>
          )}
        </Block>
      </View>
    </View>
  );
};

// 给props赋默认值
ShopGuideModule.defaultProps = {
  dataSource: {},
  showType: 0,
};

export default ShopGuideModule;
