/*
import { _safe_style_ } from '@/wk-taro-platform';
import { Block, View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import api from '../../../api/index.js';
import wxApi from '@/wxat-common/utils/wxApi/index';
import config from '../utils/config.js';

import VirtualGoodsItem from '../../virtual-goods-item/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';


export interface VirtualGoodsModuleProps{
  dataSource:any
}



interface State {
  list: any[];
  textNavSource:any;
  PADDING:string
}

class VirtualGoodsModule extends React.Component<State> {

  state = {
    list: [], //直接渲染list列表
    // 带上装修数据
    textNavSource: null,
    PADDING: config.PADDING,
  };

  componentDidMount(): void {
    // 设置装修设置
    const dataSource = this.props.dataSource;
    console.log(dataSource);
    this.setState({
      textNavSource: {
        ...dataSource.textNavSource,
        linkPage: `${dataSource.textNavSource.linkPage}?title=${dataSource.textNavSource.title}`,
      },
    });

    this.getList(this.getIds().join(','));
  }

  componentDidCatch(error, errorInfo) {
    console.log('==============',error,errorInfo);
  }

  // componentWillUnmount(): void {
  //   this.setState = (state,callback)=>{
  //     return;
  //   };
  // }

  getIds() {
    return this.props.dataSource.data.map((item) => item.itemNo);
  };

  /!**
   * 请求商品列表
   * @param {*} ids
   *!/
  getList = (ids) => {
    if (!ids.length) return;
    wxApi
      .request({
        url: api.virtualGoods.list,
        data: {
          type: 41,
          status: 1,
          itemNoList: ids,
        },
      })
      .then(({ data }) => {
        data = data || [];
        let list = data.filter((item) => item.pocketStatus !== 3 && item.pocketStatus !== 4);
        console.log(list,'打印一下数据');
        this.setState({ list });
      });
  };

  /!**
   * 点击商品跳转详情
   * @param {*} cardItem
   *!/
  onItemClick = (cardItem) => {
    // const itemNo = cardItem.detail.id;
    const itemNo = cardItem.itemNo;
    wxApi.$navigateTo({
      url: `/sub-packages/marketing-package/pages/virtual-goods/detail/index?itemNo=${itemNo}`,
    });
  };


  render() {
    const { dataSource } = this.props;
    const {  PADDING, textNavSource,  list } = this.state;
    return (
      <View
        data-scoped="wk-wcbv-VirtualGoodsModule"
        className="wk-wcbv-VirtualGoodsModule card-package"
        style={_safe_style_(
          'margin:' +
          (dataSource.marginUpDown * 2 + 'px ' + dataSource.marginLeftRight * 2 + 'px') +
          ';border-radius: ' +
          dataSource.radius * 2 +
          'px;'
        )}
      >
        <View >
          {dataSource.showNav && <TextNavModule dataSource={textNavSource}></TextNavModule>}

          {list.length && (
            <Block>
              {dataSource.showType === 0 && (
                <View className="card-container has-padding">
                  {list &&
                  list.map((item, index) => {
                    return (
                      <VirtualGoodsItem
                        virtualGoodsItem={item}
                        key={index}
                        onClick={this.onItemClick}
                        display="bigPictureMode"
                      ></VirtualGoodsItem>
                    );
                  })}
                </View>
              )}

              {/!*  经典列表  *!/}
              {dataSource.showType === 1 && (
                <View className="card-container has-padding">
                  {list &&
                  list.map((item, index) => {
                    return (
                      <VirtualGoodsItem
                        virtualGoodsItem={item}
                        key={index}
                        onClick={this.onItemClick}
                        display="classicList"
                      ></VirtualGoodsItem>
                    );
                  })}
                </View>
              )}

              {/!*  一行两个  *!/}
              {dataSource.showType === 2 && (
                <View className="card-container has-padding card-one-row-two-container">
                  {list &&
                  list.map((item, index) => {
                    return (
                      <VirtualGoodsItem
                        className="one-row-two-item"
                        virtualGoodsItem={item}
                        key={index}
                        onClick={this.onItemClick}
                        display="oneRowTwo"
                      ></VirtualGoodsItem>
                    );
                  })}
                </View>
              )}

              {/!*  一行三个  *!/}
              {dataSource.showType === 3 && (
                <View className="card-container has-padding card-one-row-three-container">
                  {list &&
                  list.map((item, index) => {
                    return (
                      <VirtualGoodsItem
                        className="one-row-three-item"
                        virtualGoodsItem={item}
                        key={index}
                        onClick={this.onItemClick}
                        display="oneRowThree"
                      ></VirtualGoodsItem>
                    );
                  })}
                </View>
              )}

              {/!*  水平滑动  *!/}
              {dataSource.showType === 4 && (
                <View
                  style={_safe_style_('overflow-x:auto;white-space:nowrap;')}
                  className="card-container has-padding horizon-container"
                >
                  {list &&
                  list.map((item, index) => {
                    return (
                      <VirtualGoodsItem
                        className="horizon-container-item"
                        virtualGoodsItem={item}
                        key={index}
                        onClick={this.onItemClick}
                        display="horizon"
                      ></VirtualGoodsItem>
                    );
                  })}
                </View>
              )}
            </Block>
          )}
        </View>
      </View>
    );
  }
}

export default VirtualGoodsModule;


*/

import  { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from "../../../api";
import wxApi from '../../../utils/wxApi';
import VirtualGoodsItem from "../../virtual-goods-item";

import TextNavModule from "../../decorate/textNavModule";
import './index.scss';


interface VirtualGoodsModuleProps {
  dataSource: Record<string, any>;
  isPageShow?: boolean;
}

const VirtualGoodsModule: FC<VirtualGoodsModuleProps> = ({ dataSource = {}, isPageShow }) => {
  const [list, setList] = useState([]); // 直接渲染list列表
  const [textNavSource, setTextNavSource] = useState({}); // 带上装修数据
  useEffect(() => {
    if (dataSource && dataSource.textNavSource) {
      // 设置装修设置
      setTextNavSource({
        ...dataSource.textNavSource,
        linkPage: `${dataSource.textNavSource.linkPage}?title=${dataSource.textNavSource.title}`,
      });
    }
    if (dataSource && dataSource.data && dataSource.data.length) {
      getList(getIds().join(','));
    }
  }, [dataSource]);

  useEffect(() => {
    if (dataSource && dataSource.data) {
      getList(getIds().join(','));
    }
  }, [isPageShow]);

  function getIds() {
    const idLists = dataSource.data.map((item) => item.itemNo) || '';
    return idLists;
  }

  function getList(ids) {
    if (!ids || !ids.length) {
      setList([]);
      return;
    }

    wxApi
      .request({
        url: api.virtualGoods.list,
        data: {
          type: 41,
          status: 1,
          itemNoList: ids,
        },
      })
      .then((res) => {
        const { success, data } = res;
        if (!success) {
          return;
        }
        if (!data || !data.length) {
          setList([]);
        } else {
          const list = data.filter((item) => item.pocketStatus !== 3 && item.pocketStatus !== 4);
          setList(list);
        }
      })
      .catch((err) => {
        console.error('获取列表失败', err);
      });
  }


  function onItemClick(cardItem) {
    const itemNo = cardItem.itemNo;
    wxApi.$navigateTo({
      url: `/sub-packages/marketing-package/pages/virtual-goods/detail/index?itemNo=${itemNo}`,
    });
  }

  return (
    <View
      data-scoped="wk-wcbv-VirtualGoodsModule"
      className="wk-wcbv-VirtualGoodsModule card-package"
      style={_safe_style_(
        'margin:' +
        (dataSource.marginUpDown + 'px ' + dataSource.marginLeftRight + 'px') +
        ';border-radius: ' +
        dataSource.radius +
        'px;'
      )}
    >
      <View >
        {dataSource.showNav && <TextNavModule dataSource={textNavSource}></TextNavModule>}

        {!!list.length && (
          <Block>
            {dataSource.showType === 0 && (
              <View className="card-container has-padding">
                {list &&
                list.map((item, index) => {
                  return (
                    <VirtualGoodsItem
                      virtualGoodsItem={item}
                      key={index}
                      onClick={onItemClick}
                      display="bigPictureMode"
                    ></VirtualGoodsItem>
                  );
                })}
              </View>
            )}

            {/*  经典列表  */}
            {dataSource.showType === 1 && (
              <View className="card-container has-padding">
                {list &&
                list.map((item, index) => {
                  return (
                    <VirtualGoodsItem
                      virtualGoodsItem={item}
                      key={index}
                      onClick={onItemClick}
                      display="classicList"
                    ></VirtualGoodsItem>
                  );
                })}
              </View>
            )}

            {/*  一行两个  */}
            {dataSource.showType === 2 && (
              <View className="card-container has-padding card-one-row-two-container">
                {list &&
                list.map((item, index) => {
                  return (
                    <VirtualGoodsItem
                      className="one-row-two-item"
                      virtualGoodsItem={item}
                      key={index}
                      onClick={onItemClick}
                      display="oneRowTwo"
                    ></VirtualGoodsItem>
                  );
                })}
              </View>
            )}

            {/*  一行三个  */}
            {dataSource.showType === 3 && (
              <View className="card-container has-padding card-one-row-three-container">
                {list &&
                list.map((item, index) => {
                  return (
                    <VirtualGoodsItem
                      className="one-row-three-item"
                      virtualGoodsItem={item}
                      key={index}
                      onClick={onItemClick}
                      display="oneRowThree"
                    ></VirtualGoodsItem>
                  );
                })}
              </View>
            )}

            {/*  水平滑动  */}
            {dataSource.showType === 4 && (
              <View
                style={_safe_style_('overflow-x:auto;white-space:nowrap;')}
                className="card-container has-padding horizon-container"
              >
                {list &&
                list.map((item, index) => {
                  return (
                    <VirtualGoodsItem
                      className="horizon-container-item"
                      virtualGoodsItem={item}
                      key={index}
                      onClick={onItemClick}
                      display="horizon"
                    ></VirtualGoodsItem>
                  );
                })}
              </View>
            )}
          </Block>
        )}
      </View>
    </View>
  );
};

export default VirtualGoodsModule;
