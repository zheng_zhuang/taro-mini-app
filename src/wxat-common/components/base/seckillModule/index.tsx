import React, { FC, useEffect, useState, useMemo } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import {  View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import handleData from '../utils/handleData.js';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index.js';
import seckillEnum from '@/wxat-common/constants/seckillEnum.js';

import SeckillModule from '@/wxat-common/components/decorate/seckillModule/index';
import TextNavModule from '@/wxat-common/components/decorate/textNavModule/index';
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import useTimeElapse from '@/hooks/useTimeElapse';
import { NOOP_OBJECT } from '@/wxat-common/utils/noop';

const seckillEnumStatus = seckillEnum.STATUS;

// props的类型定义
type ComponentProps = {
  dataSource: Record<string, any>;
  dataList?: Array<Record<string, any>>;
  showType?: number;
};

let SeckillModuleWrap: FC<ComponentProps> = ({ dataSource, dataList, showType }) => {
  const [activityList, setActivityList] = useState([]);
  const finalShowType = (showType != null ? showType : dataSource.showType) || 0;
  const textNavSource = useMemo(() => {
    return dataSource ? { ...dataSource.textNavSource, showType: finalShowType } : NOOP_OBJECT;
  }, [dataSource, finalShowType]);
  const [timeElapse, setTimeElapse] = useState(0);
  const list = dataList || activityList;

  let isUnMount = false
  /**
   * 获取秒杀活动渲染列表
   */
  const getSeckillList = async () => {
    const data = dataSource.data;
    if (!data || !data.length) return;

    const activityIdList = handleData.pickUpItemNos(data, 'id').join(',');
    const params = {
      activityIdList: activityIdList,
      statusList: seckillEnumStatus.NOT_STARTED.value + ',' + seckillEnumStatus.ON_GOING.value,
    };

    const res = await wxApi.request({ url: api.seckill.getSeckillList, data: params });
    const activityList = res.data || [];

    if(!isUnMount){
      setActivityList(activityList);
    }
  };

  const assembledList = useMemo(() => {
    /**
     * 调配每个秒杀商品的剩余时间
     */
    return list.map((goodsItem) => {
      goodsItem = { ...goodsItem };
      let startCountDown = (goodsItem.startCountDown || 0) - timeElapse;
      let endCountDown = (goodsItem.endCountDown || 0) - timeElapse;
      goodsItem.currentStatus = seckillEnumStatus.NOT_STARTED.value; // 未开始

      startCountDown = goodsItem.startCountDown = startCountDown > 0 ? startCountDown : 0;
      endCountDown = goodsItem.endCountDown = endCountDown > 0 ? endCountDown : 0;

      if (startCountDown && endCountDown && startCountDown < endCountDown) {
        goodsItem.remainTitle = '距开始';
        goodsItem.remainTime = Math.floor(startCountDown / 1000);
      } else if (endCountDown && endCountDown > 0) {
        goodsItem.remainTitle = '距结束';
        goodsItem.remainTime = Math.floor(endCountDown / 1000);
        goodsItem.currentStatus = seckillEnumStatus.ON_GOING.value; // 进行中
      } else {
        goodsItem.remainTitle = '距结束';
        goodsItem.remainTime = 0;
        goodsItem.currentStatus = seckillEnumStatus.ENDED.value; // 已结束
      }

      // 倒计时
      if (!goodsItem.remainTime || goodsItem.remainTime <= 0) {
        goodsItem.displayTime = '00:00:00';
      } else {
        const day = Math.floor(goodsItem.remainTime / (24 * 3600));

        const hourS = Math.floor(goodsItem.remainTime % (24 * 3600));
        const hourInt = Math.floor(hourS / 3600);
        const hour = hourInt < 10 ? '0' + hourInt : hourInt;

        const minuteS = Math.floor(hourS % 3600);
        const minuteInt = Math.floor(minuteS / 60);
        const minute = minuteInt < 10 ? '0' + minuteInt : minuteInt;

        const secondS = Math.floor(minuteS % 60);
        const second = secondS < 10 ? '0' + secondS : secondS;
        goodsItem.displayTime = day ? `${day}天${hour}:${minute}:${second}` : `${hour}:${minute}:${second}`;
      }

      return goodsItem;
    });
  }, [list, timeElapse]);

  useTimeElapse(
    !!list && !!list.length && assembledList.some((i) => i.currentStatus !== seckillEnumStatus.ENDED.value),
    setTimeElapse
  );

  useEffect(() => {
    if (!dataList || dataList.length === 0) {
      getSeckillList(); // 获取秒杀活动渲染列表
    }
    return ()=>{
      isUnMount = true
    }
  }, []);

  /**
   * 跳转秒杀详情页面
   * @param {*} ev
   */
  const handleClick = (ev: ITouchEvent) => {
    // FIXME: 没有被触发
    // FIXME: detail 可能不存在
    const item = ev.detail.item;
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/seckill/detail/index',
      data: {
        itemNo: item.itemNo || null,
        activityId: item.id || null,
        seckillStatus: item.status || 0,
      },
    });
  };

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbs-SeckillModule'
      className='wk-cbs-SeckillModule'
    >
      {!!(!!list && list.length) && (
        <View
          className='seckill-module'
          style={{
            padding: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 0)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2 || 0
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
          }}
        >
          {!!dataSource.showNav && <TextNavModule dataSource={textNavSource}></TextNavModule>}
          {/*  商品组件-显示秒杀商品模板  */}
          <View>
            <SeckillModule _renderList={assembledList} showType={finalShowType} onClick={handleClick}></SeckillModule>
          </View>
        </View>
      )}
    </View>
  );
};

SeckillModuleWrap.defaultProps = {
  dataSource: {},
};

export default SeckillModuleWrap;
