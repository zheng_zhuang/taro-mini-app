import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';
import wxApi from '../../../utils/wxApi';
import api from '../../../api/index.js';
import handleData from '../utils/handleData.js';
import config from '../utils/config.js';
import GoodsItem from '../../industry-decorate-style/goods-item/index';
// import TextNavModule from '../../decorate/textNavModule/index';
import displayEnum from '../../../constants/displayEnum';

import './index.scss';

const PADDING = config.PADDING;

type StateType = {
  base: {
    currentStore: Record<string, any>;
  };

  ext: {
    appId: number | string;
  };
};

// props的类型定义
type IProps = {
  dataSource: Record<string, any>;
  list?: Array<Record<string, any>>;
  display?: string;
  defaultReportSource?: string;
  reportSource?: string;
};

let ServeModule: FC<IProps> = ({ dataSource, list, display, reportSource, defaultReportSource }) => {
  const [dataList, setDataList] = useState<Record<string, any>>({});
  const [itemClass, setItemClass] = useState('');
  const [containerClass, setContainerClass] = useState('');

  const appId = useSelector((state: StateType) => state.ext.appId);
  const currentStore = useSelector((state: StateType) => state.base.currentStore);

  useEffect(() => {
    let obj = {
      type: 'serve',
      display: display || displayEnum.VERTICAL,
      data: list,
    };

    setDataList(obj);
  }, [list]);

  useEffect(() => {
    if (!list && dataSource) {
      getList();
    } else {
      const _dataList = {
        display: display || displayEnum.VERTICAL,
        type: 'serve',
        data: list,
      };

      setDataList(_dataList);
    }
    const dataSourceDisplay = dataSource && dataSource.display;

    const _itemClass = displayEnum.getClassByShowType(displayEnum.classTypeEnum.item, dataSourceDisplay || display);
    const _containerClass = displayEnum.getClassByShowType(
      displayEnum.classTypeEnum.container,
      dataSourceDisplay || display
    );

    setContainerClass(_containerClass || '');
    setItemClass(_itemClass || '');
  }, []);

  function getList() {
    const data = dataSource.data;
    if (data && data.length > 0) {
      const itemNos = handleData.pickUpItemNos(data, 'itemNo').join(',');
      const params = {
        appId,
        storeId: currentStore.id,
        itemNo: itemNos,
      };

      wxApi
        .request({
          url: api.goods.listWithItemNos,
          data: params,
          isShelf: 1,
        })
        .then((res) => {
          const obj: Record<string, any> = {};
          // for (const key in this.props.dataSource) {
          //   if (this.props.dataSource.hasOwnProperty(key)) {
          //     obj[key] = key === 'data' ? res.data : this.props.dataSource[key];
          //   }
          // }

          Object.keys(dataSource).map((key) => {
            if (dataSource.hasOwnProperty(key)) {
              obj[key] = key === 'data' ? res.data : dataSource[key];
            }
          });

          if (!dataSource.hasOwnProperty('display')) {
            const showRectType = parseInt(dataSource.showRectType);
            if (showRectType === 1) {
              obj.display = 'vertical';
            } else if (showRectType === 1) {
              obj.display = 'oneRowTwo';
            }
          }

          obj.type = 'serve';

          setDataList(obj);
        })
        .catch((error) => {
          console.log('get product: error: ' + JSON.stringify(error));
        });
    }
  }

  function click(item) {
    const itemNo = item.itemNo;
    wxApi.$navigateTo({
      url: '/sub-packages/server-package/pages/serve-detail/index',
      data: { itemNo: itemNo },
    });

    return true;
  }

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-cbs-ServeModule' className='wk-cbs-ServeModule'>
      {!!(Array.isArray(dataList.data) && dataList.data.length > 0) && (
        <View
          className='serve-module'
          style={{
            margin: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 0)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2 || 0
            )}`,
            borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
            padding: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 20)} ${Taro.pxTransform(
              dataSource.marginLeftRight * 2 || 20
            )}`,
          }}
        >
          {/* {dataSource.showNav && (
                  <TextNavModule
                    dataSource={dataSource.textNavSource}
                    padding={PADDING}
                  ></TextNavModule>
                )} */}

          <View className={containerClass}>
            {dataList.data.map((item) => {
              return (
                <View className={itemClass} key={item.id}>
                  <GoodsItem
                    onClick={click}
                    goodsItem={item}
                    reportSource={reportSource ? reportSource : defaultReportSource}
                    display={dataList.display}
                  ></GoodsItem>
                </View>
              );
            })}
          </View>
        </View>
      )}
    </View>
  );
};

ServeModule.defaultProps = {
  dataSource: {},
  reportSource: '',
  defaultReportSource: '',
  display: displayEnum.VERTICAL,
};

export default ServeModule;
