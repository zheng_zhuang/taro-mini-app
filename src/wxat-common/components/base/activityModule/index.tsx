import React, { FC, useEffect, useState } from 'react';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import TextNavModule from '../../decorate/textNavModule/index';
import wxApi from '@/wxat-common/utils/wxApi';
import api from '@/wxat-common/api/index';
import { activityDateFormatSimple } from '@/wxat-common/utils/activityFormat';
import { ActivityStatus, ActivityStatusName, ActivityStatusBgColor } from '@/types/activetype';
import './style.scss';

interface Activity {
  activityAddress: string;
  activityDetail: string;
  activityNo: string;
  activityTitle: string;
  id: number;
  imgUrl: string;
  inventoryNum: number;
  itemDTOList: Array<{ startTime: string; endTime: string }>;
  saleNum: number;
  status: number;
  ticketPrice: number;
}

function formatedDate(item) {
  const list = item.itemDTOList;
  if (list && list.length) {
    return activityDateFormatSimple(list);
  }
  return '待定';
}

const ActivityModule: FC<{
  dataSource: {
    radius: number;
    marginUpDown: number;
    marginLeftRight: number;
    showNav: boolean;
    showTitle: boolean;
    list: number[];
    backgroundColor?: string;
    textNavSource: {
      componentsPadding: number;
      radius: number;
      marginUpDown: number;
      marginLeftRight: number;
      fontSize: string;
      bgColor: string;
      bgMode: string;
      textMode: string;
      isLock: boolean;
      title: string;
      subTitle: string;
      titleColor: string;
      subTitleColor: string;
      linkPage: string;
      linkName: string;
      showMore: boolean;
    };
  };
}> = (props) => {
  const { dataSource } = props;
  const [list, setList] = useState<Activity[]>([]);
  useEffect(() => {
    if (dataSource.list && dataSource.list.length) {
      wxApi
        .request({
          url: api.activeModule.getActivityByIds,
          loading: false,
          method: 'POST',
          data: dataSource.list,
        })
        .then((res) => {
          setList(res.data);
        });
    }
  }, dataSource.list);

  if (list == null || !list.length) {
    return null;
  }

  return (
    <View
      className='ky-activity-module'
      style={{
        padding: `${Taro.pxTransform(dataSource.marginUpDown * 2 || 20)} ${Taro.pxTransform(
          dataSource.marginLeftRight * 2 || 20
        )}`,
        borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
        background: dataSource.backgroundColor || 'rgba(255,255,255,1)',
      }}
    >
      {!!(dataSource && dataSource.showNav) && <TextNavModule dataSource={dataSource.textNavSource}></TextNavModule>}
      <View className='ky-activity-module__content'>
        {list.map((item) => {
          return (
            <View
              className='activity'
              key={item.id}
              onClick={() => {
                wxApi.$navigateTo({
                  url: 'sub-packages/marketing-package/pages/activity-module/activity-detail/index',
                  data: { id: item.id },
                });
              }}
            >
              <Image className='activity__header' mode='aspectFill' src={item.imgUrl} />
              <View className='activity__body'>
                <View className='activity__name'>
                  <View
                    style={{ background: ActivityStatusBgColor[item.status] }}
                    className={`activity__status ${item.status == ActivityStatus.Doing && 'active'}`}
                  >
                    {ActivityStatusName[item.status]}
                  </View>
                  {item.activityTitle}
                </View>
              </View>
              <View className='activity__date'>活动时间: {formatedDate(item)}</View>
            </View>
          );
        })}
      </View>
    </View>
  );
};

ActivityModule.defaultProps = {
  // @ts-expect-error
  dataSource: {},
};

export default ActivityModule;
