import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';

import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';

// const SHOW_TYPE = {
//   vertical: 1, // 经典列表
//   horizon: 4, // 水平滑动
//   rowOne: 0, // 大图
//   rowTwo: 2, // 一行两个
//   rowThree: 3 // 一行三个
// };
import CardsItem from '../../cards-item/index';
// import Error from '../../error/error';
// import Empty from '../../empty/empty';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';

const CARDSTATUS = [1, 2]; // 0：未开始 1：进行中 2：已售罄 3：已结束 4：已删除

type CardPackModuleProps = {
  dataSource: Record<string, any>;
  isPageShow?: boolean;
};

let CardPackModule: FC<CardPackModuleProps> = ({ dataSource = {}, isPageShow }) => {
  const [list, setList] = useState([]); //直接渲染list列表
  const [textNavSource, setTextNavSource] = useState({}); // 带上装修数据

  useEffect(() => {
    console.log('cardPackModule dataSource', dataSource);
    if (dataSource && dataSource.textNavSource) {
      // 设置装修设置
      setTextNavSource({
        ...dataSource.textNavSource,
        linkPage: `${dataSource.textNavSource.linkPage}?title=${dataSource.textNavSource.title}`,
      });
    }
    if (dataSource && dataSource.data && dataSource.data.length) {
      getList(getIds().join(','));
    }
  }, [dataSource]);

  useEffect(() => {
    if (dataSource && dataSource.data) {
      getList(getIds().join(','));
    }
  }, [isPageShow]);

  function getIds() {
    const idLists = dataSource.data.map((item) => item.id) || '';
    return idLists;
  }

  function getList(ids) {
    if (!ids || !ids.length) {
      setList([]);
      return;
    }
    const params = {
      pocketIds: ids, // 卡包ID列表，逗号分隔
      // isShelf: 1 // 上架与否，1上架，0，下架
    };
    // CARDSTATUS.forEach((item, i) => {
    //   let key = `statusList[${i}]`;
    //   params[key] = item;
    // });
    wxApi
      .request({
        url: api.couponpocket.couponpocketList,
        data: params,
      })
      .then((res) => {
        const { success, data } = res;
        if (!success) {
          return;
        }
        if (!data || !data.length) {
          setList([]);
        } else {
          const list = data.filter((item) => item.pocketStatus !== 3 && item.pocketStatus !== 4);
          setList(list);
        }
      })
      .catch((err) => {
        console.error('获取列表失败', err);
      });
  }

  function goMore() {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/card-pack/list/index',
    });
  }

  function onItemClick(cardItem) {
    const itemNo = cardItem.itemNo;
    wxApi.$navigateTo({
      url: `/sub-packages/marketing-package/pages/card-pack/detail/index?itemNo=${itemNo}`,
    });
  }

  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-cbc-CardPackModule'
      className='wk-cbc-CardPackModule'
    >
      {!!dataSource && (
        <View
          className='card-package'
          style={{
            margin: `${Taro.pxTransform((dataSource.marginUpDown || 0) * 2)} ${Taro.pxTransform(
              (dataSource.marginLeftRight || 0) * 2
            )}`,
            borderRadius: Taro.pxTransform((dataSource.radius || 0) * 2),
          }}
        >
          <View>
            {!!dataSource.showNav && <TextNavModule dataSource={textNavSource} />}
            {!!list.length && (
              <Block>
                {dataSource.showType === 0 && (
                  <View className='card-container has-padding'>
                    {list.map((item, index) => {
                      return (
                        <CardsItem
                          cardsItem={item}
                          key={index}
                          onClick={onItemClick}
                          display='bigPictureMode'
                        ></CardsItem>
                      );
                    })}
                  </View>
                )}

                {/*  经典列表  */}
                {dataSource.showType === 1 && (
                  <View className='card-container has-padding'>
                    {list.map((item, index) => {
                      return (
                        <CardsItem cardsItem={item} key={index} onClick={onItemClick} display='classicList'></CardsItem>
                      );
                    })}
                  </View>
                )}

                {/*  一行两个  */}
                {dataSource.showType === 2 && (
                  <View className='card-container has-padding card-one-row-two-container'>
                    {list.map((item, index) => {
                      return (
                        <View className='one-row-two-item' key={index}>
                          <CardsItem cardsItem={item} onClick={onItemClick} display='oneRowTwo'></CardsItem>
                        </View>
                      );
                    })}
                  </View>
                )}

                {/*  一行三个  */}
                {dataSource.showType === 3 && (
                  <View className='card-container has-padding card-one-row-three-container'>
                    {list.map((item, index) => {
                      return (
                        <View className='one-row-three-item' key={index}>
                          <CardsItem cardsItem={item} onClick={onItemClick} display='oneRowThree'></CardsItem>
                        </View>
                      );
                    })}
                  </View>
                )}

                {/*  水平滑动  */}
                {dataSource.showType === 4 && (
                  <View
                    style={_safe_style_('overflow-x:auto;white-space:nowrap;')}
                    className='card-container has-padding horizon-container'
                  >
                    {list.map((item, index) => {
                      return (
                        <View className='horizon-container-item' key={index}>
                          <CardsItem cardsItem={item} onClick={onItemClick} display='horizon'></CardsItem>
                        </View>
                      );
                    })}
                  </View>
                )}
              </Block>
            )}
          </View>
        </View>
      )}
    </View>
  );
};

export default CardPackModule;
