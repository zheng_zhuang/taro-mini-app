import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';

import Taro from '@tarojs/taro';

// components/goods-module/index.js
// import pay from '../../../utils/pay.js';
import wxApi from '../../../utils/wxApi';
// import api from '../../../api/index.js';
import config from '../utils/config.js';

import GiftCardModule from '../../decorate/giftCardModule/index';
import TextNavModule from '../../decorate/textNavModule/index';
import './index.scss';

const PADDING = config.PADDING;

type GiftCardModuleWrapProps = {
  dataSource: Record<string, any>;
};

let GiftCardModuleWrap: FC<GiftCardModuleWrapProps> = ({ dataSource = {} }) => {
  const [dsRemote, setDsRemote] = useState([]);

  useEffect(() => {
    if (dataSource && dataSource.data) {
      setDsRemote(dataSource.data);
    }
  }, [dataSource]);

  function handleGoToCardDetail(item) {
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/gift-card/list/index',
      data: { bg: JSON.stringify(item) },
    });
  }

  return (
    <View
      data-scoped='wk-cbg-GiftCardModule'
      className='wk-cbg-GiftCardModule gift-card-module'
      style={{
        margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(dataSource.marginLeftRight * 2)}`,
        borderRadius: Taro.pxTransform(dataSource.radius * 2 || 0),
      }}
    >
      {!!dataSource.showNav && <TextNavModule dataSource={dataSource.textNavSource} padding={PADDING}></TextNavModule>}
      <View onClick={handleGoToCardDetail}>
        <GiftCardModule dataList={dsRemote} padding={PADDING} onClick={handleGoToCardDetail}></GiftCardModule>
      </View>
    </View>
  );
};

GiftCardModuleWrap.defaultProps = {
  dataSource: {},
};

export default GiftCardModuleWrap;
