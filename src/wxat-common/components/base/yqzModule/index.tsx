import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { View, Image, Text, Input } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import login from '@/wxat-common/x-login';
import api from '@/wxat-common/api/index.js';
import { connect } from 'react-redux';
import dateUtil from '@/wxat-common/utils/date.js';
import authHelper from '@/wxat-common/utils/auth-helper';
import { formatLocation } from '@/wxat-common/utils/location';
import { toJumpYQZ } from '@/wxat-common/utils/yqz';
import { defaultLocation } from '@/wxat-common/constants/yqz';
import store from '@/store';
import { updateBaseAction } from '@/redux/base';
import template from '@/wxat-common/utils/template';

import './index.scss';
import classNames from 'classnames';

interface ComponentProps {
  dataSource: Record<string, any>;
  dateCall: Function;
  enterpriseStaff?: Boolean;
  gps: Record<string, any>;
  currentStore?: Record<string, any>;
}

interface ComponentState {
  tmpStyle: string | any;
  inTime: string;
  outTime: string;
  DaysBetween: string | any;

  showinTime: string;
  showoutTime: string;
  weeksStart: string;
  outValue: string;
  address: string;

  title: string;
  longitude: any;
  latitude: any;
  tabSelectedId: number;
  locationInfo: object;
  locationFlag: Boolean;
  activeIndex: number;
  headIconPos: Record<string, any>;
  headLine: Record<string, any>;
  keyword: string;
}

const mapStateToProps = (state) => {
  return {
    gps: state.base.gps,
    userInfo: state.base.userInfo,
    appInfo: state.base.appInfo,
    storeVO: state.base.storeVO,
    loginInfo: state.base.loginInfo,
    enterpriseStaff: state.base.enterpriseStaff,
    enterpriseStaffInfo: state.base.enterpriseStaffInfo,
    currentStore: state.base.currentStore,
  };
};
const mapDispatchToProps = () => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class YqzModule extends React.Component<ComponentProps, ComponentState> {
  static defaultProps = {
    dataSource: null,
  };

  state = {
    tmpStyle:'',
    inTime: '',
    outTime: '',
    DaysBetween: '',
    showinTime: '',
    showoutTime: '',
    weeksStart:'',
    outValue: '',
    address: '',
    title: '',
    longitude: null,
    latitude: null,
    locationInfo: {
      province: '',
      city: '',
      district: '',
    },
    tabSelectedId: 1,
    activeIndex: 0,
    headIconPos: {
      left: '0%',
      url:`https://bj.bcebos.com/htrip-mp/static/mall/decorate/yqzTabActive_1.png`,
    },
    headLine: {
      left: '16.3%',
    },
    keyword: '',
    locationFlag: false
  };

  componentDidMount() {
    this.getTemplateStyle()
    this.getthisLocation();
    const inTime = dateUtil.getNowDay();
    const outTime = dateUtil.getDay(1);
    const showinTime = this.MonthDayformat(inTime);
    const showoutTime = this.MonthDayformat(outTime);
    const DaysBetween = this.getDaysBetween(inTime, outTime);
    const weeksStart = this.getWeekDate(inTime);
    const outValue = this.getWeekDate(outTime);
    this.setState({
      inTime,
      outTime,
      showinTime,
      showoutTime,
      DaysBetween,
      weeksStart,
      outValue
    });
    if (this.props.enterpriseStaff) {
      this.setState({
        tabSelectedId: 2,
      });
    } else {
      this.setState({
        tabSelectedId: 1,
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.gps.latitude !== this.props.gps.latitude) {
      store.dispatch(updateBaseAction({gps: nextProps.gps}))
      setTimeout(() => {
        this.getthisLocation();
      }, 0)
    }
    if (nextProps.enterpriseStaff) {
      this.setState({
        tabSelectedId: 2,
      });
    } else {
      this.setState({
        tabSelectedId: 1,
      });
    }
  }

  componentWillUnMount = () => {
    this.setState = (state, callback) => {};
  };

  checkDate = () => {
    const { inTime, outTime } = this.state

    if (this.props.dateCall) {
      this.props.dateCall({
        inTime,
        outTime
      })
    }
  };

  // 选择当前位置
  handleChooseLocation = () => {
    wxApi
    .getSetting()
    .then((res) => {
      if (!res.authSetting['scope.userLocation']) {
        return wxApi
          .authorize({
            scope: 'scope.userLocation',
          })
          .then(
            () => {
              this.setState({
                locationFlag: true
              })
              return wxApi.chooseLocation();
            },
            (error) => {
              console.log('error: ', error);
              if (error.errMsg === 'authorize:fail auth deny' || error.errMsg === 'authorize:fail:auth deny') { // 已经拒绝授权引导跳到设置页面
                this.setState({
                  locationFlag: false
                }, () => {
                  wxApi.showModal({
                    title: '提示',
                    content: '需要获取用户位置信息权限',
                    confirmText: '前往设置',
                    confirmColor: '#3CC51F',
                    success(res) {
                      if (res.confirm) {
                        wxApi.openSetting(); // 打开小程序设置页面，可以让用户开启需要的权限
                      }
                    }
                  })
                })
              }
            }
          );
      } else {
        this.setState({
          locationFlag: true
        })
        return wxApi.chooseLocation();
      }
    }).then(async (location) => {
      if (!location) return
      const { name, longitude, latitude } = location
      const locationInfo = await formatLocation(latitude, longitude)
      this.setState({
        address: name,
        title: name,
        longitude: locationInfo.lng,
        latitude: locationInfo.lat,
        locationInfo
      })
    }).finally((e)=>{
      this.setState({
        locationFlag: false
      })
    });
  }

  closeDialog = (e) => {
    const inTime = e.inTime;
    const outTime = e.outTime;
    const showinTime = this.MonthDayformat(inTime);
    const showoutTime = this.MonthDayformat(outTime);
    const weeksStart = this.getWeekDate(inTime);
    const outValue = this.getWeekDate(outTime);
    this.setState({
      inTime,
      outTime,
      showinTime,
      showoutTime,
      weeksStart,
      outValue,
      DaysBetween: e.DaysBetween,
    });
  };

  // 定位经纬度＋地址解析
  wxLocation() {
    const { longtitude, latitude } = this.props.gps;
    if (!longtitude && !latitude) {
      this.setState({
        address: defaultLocation.address || '',
        longitude: defaultLocation.lng,
        latitude: defaultLocation.lat,
      });
      return
    }
    return wxApi
      .request({
        url: api.poi.location + '?lng=' + longtitude + '&lat=' + latitude,
        loading: true,
        quite: true
      })
      .then((res) => {
        const data = res.data || {};
        this.setState({
          address: data.address || '',
          title: data.title || '',
          longitude: data.longitude,
          latitude: data.latitude,
        });
      }).catch((err) => {
        console.log(err)
        this.setState({
          address: defaultLocation.address || '',
          longitude: defaultLocation.lng,
          latitude: defaultLocation.lat,
        });
      })
  }

  // 获取当前位置
  getthisLocation() {
    this.wxLocation();
  }

  // H5编译地址
  getCompileAddressByH5 (latitude, longitude) {
    return new Promise((resolve, reject) => {
      const url = 'https://api.map.baidu.com/reverse_geocoding/v3/'
      wxApi.$jsonp(url, {
          location: `${latitude},${longitude}'`,
          ak: 'tGeEkf70CzM4DEj5fSYPGT07RWqT4Z05',
          output: 'json',
          coordtype: 'wgs84ll'
        }
      )
        .then(res => {
          if (res.result) {
            const { addressComponent } = res.result
            const { district, province, city, adcode } = addressComponent
            const cityCode = adcode.replace(adcode.slice(4), '00')
            const data = {
              district, province, city, cityCode
            }
            resolve(data)
          } else {
            reject('格式化地理位置失败！');
          }
        }).catch(err => {
          reject('格式化地理位置失败！');
        })
    })
  }

  getDaysBetween(dateString1, dateString2) {
    const startDate = Date.parse(dateString1);
    const endDate = Date.parse(dateString2);
    if (startDate > endDate) {
      return 0;
    }
    if (startDate == endDate) {
      return 1;
    }
    const days = (endDate - startDate) / (1 * 24 * 60 * 60 * 1000);
    return days;
  }

  // 格式化日期，只显示月份和日
  MonthDayformat(date) {
    const Dtime = new Date(date);
    const Dtimemonth = Dtime.getMonth() + 1;

    // Dtimemonth < 10 ? '0' + Dtimemonth : Dtimemonth
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}月${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }日`;
  }

  // 获取当前是周几
  getWeekDate(dateDay) {
    const now = new Date(dateDay);
    const day = now.getDay();
    const weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];

    const week = weeks[day];
    return week;
  }

  setHeadActive(index) {
    if (index > 0) {
      wxApi.showToast({
        title: '暂未开放，请耐心等待',
        icon: 'none',
      });
      return
    }
    const imgUrl = {
      0: `https://bj.bcebos.com/htrip-mp/static/mall/decorate/yqzTabActive_1.png`,
      1: `https://bj.bcebos.com/htrip-mp/static/mall/decorate/yqzTabActive_3.png`,
      2: `https://bj.bcebos.com/htrip-mp/static/mall/decorate/yqzTabActive_2.png`,
    }
    this.setState({
      activeIndex: index,
      headIconPos: {
        left: index == 0 ? '0%' : index == 1 ? '16.6%' : '52%' ,
        url: imgUrl[index],
      },
      headLine: {
        left: index == 0 ? '16.3%' : index == 1 ? '50%' : '84%' ,
      },
    });
  }

  keywordInput = (val) => {
    const value = val.detail.value;
    this.setState({
      keyword: value,
    });
  };

  toYqz = async () => {
    if (!authHelper.checkAuth()) return;
    let path = 'pages/index/index';
    if (this.state.activeIndex == 0) {
      path = 'hotel/hotelList/hotelList';
    }
    wxApi.showLoading({
      title: '加载中',
    });
    if (process.env.TARO_ENV === 'weapp') {
      await login.reLogin() // 重新登录，防止过去初始化获取不到用户信息
    } else {
      await login.login()
    }
    const params = await this.handleYqzParams();
    wxApi.hideLoading();
    toJumpYQZ(path, params);
  };

  handleYqzParams = async () => {
    let query = {};
    if (this.state.activeIndex == 0) {
      // 时间处理
      const startTime = this.state.showinTime.replace('月', '-').replace('日', '');
      const endTime = this.state.showoutTime.replace('月', '-').replace('日', '');
      const { longtitude, latitude } = this.props.gps;
      const lat = this.state.latitude || latitude
      const lng = this.state.longitude || longtitude
      const { city, district, province, cityCode } = await formatLocation(lat, lng)
      query = {
        query: JSON.stringify({
          address: this.state.address,
          keyword: this.state.keyword,
          lat,
          lng,
          types: 0,
          areaName: district || '',
          cityName: city || '',
          provinceName: province || '',
          cityCode: cityCode || '',
          startTime: startTime,
          endTime: endTime,
          start_date: this.state.inTime,
          end_date: this.state.outTime,
          storeId: this.props.currentStore?.id
        }),
      };
    }
    // 飞机票
    else if (this.state.activeIndex == 1) {
      query = {
        tabValue: 2,
      };
    }
    // 火车票
    else if (this.state.activeIndex == 2) {
      query = {
        tabValue: 1,
      };
    }
    return query;
  };

  getDate = (date) => {
    let time_str = "";
    if (new Date(date).getDate() === new Date().getDate()) {
      time_str = "今天";
    } else if (new Date(date).getDate() === (new Date().getDate() - 1)) {
      time_str = "昨天";
    } else if (new Date(date).getDate() === (new Date().getDate() + 1)) {
      time_str = "明天";
    }
    return time_str;
  };

  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const {
      inTime,
      outTime,
      address,
      showinTime,
      showoutTime,
      weeksStart,
      outValue,
      DaysBetween,
      activeIndex,
      headIconPos,
      headLine,
      keyword,
      locationFlag,
      tmpStyle
    } = this.state;
    const { dataSource } = this.props;
    return (
      !locationFlag ? <View
        className='yqz-module'
        style={{
          margin:
            Taro.pxTransform(dataSource.marginUpDown * 2) + ' ' + Taro.pxTransform(dataSource.marginLeftRight * 2),
        }}
      >
        {/* 占位 */}
        {/* <View className='pos'></View> */}
        {/* 头部 */}
        <View className='yqz-module-header'>
          <View
            className={activeIndex == 0 ? 'header active-header' : 'header'}
            onClick={() => {
              this.setHeadActive(0);
            }}
          >
            酒店/公寓
          </View>
          <View
            className={activeIndex == 1 ? 'header active-header' : 'header'}
            style={_safe_style_(activeIndex == 1 ? `color: ${tmpStyle.btnColor}` : '')}
            onClick={() => {
              this.setHeadActive(1);
            }}
          >
            机票
          </View>
          <View
            className={activeIndex == 2 ? 'header active-header' : 'header'}
            style={_safe_style_(activeIndex == 2 ? `color: ${tmpStyle.btnColor}` : '')}
            onClick={() => {
              this.setHeadActive(2);
            }}
          >
            火车票
          </View>
          {/* src={require("../../../images/highLight_0.png")} */}
          {/* src='https://htrip-static.ctlife.tv/xlt/headActive.png' */}
          {/* {src = 'https://bj.bcebos.com/htrip-mp/static/mall/decorate/yqzTabActive_1.png'} */}

          <Image
            className={classNames('head-icon', activeIndex == 1 ? 'head-icon-height_1' : '')}
            style={{
              position: 'absolute',
              left: headIconPos.left,
            }}
            src={headIconPos.url}
          ></Image>
          <View
            className='line'
             style={ _safe_style_(`background: ${tmpStyle.btnColor};position: 'absolute';left:${headLine.left}` )
          }></View>
        </View>
        {/* 内容 */}
        <View className='yqz-module-content'>
          {/* 定位 */}
          <View className='content-row' onClick={this.handleChooseLocation}>
            <View className='content-address'>{address}</View>
            <View className='content-right'>
              <Image
                src='https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png'
                className='content-arrow'
              ></Image>
              <View className='content-positioning' >
                <Image
                  src='https://htrip-static.ctlife.tv/xlt/posIcon.png'
                  className='content-icon'
                ></Image>
                当前位置
              </View>
            </View>
          </View>
          {/* 日历时间筛选 */}
          <View className='content-row' onClick={this.checkDate}>
            <View className=''>
              <View className='cal-text'>{this.getDate(inTime)}入住</View>
              <View className='cal-time'> {showinTime} <Text className='weeks-text'>{ weeksStart }</Text></View>
            </View>
            <View >
              <View className='cal-text'>{this.getDate(outTime)}离店</View>
              <View className='cal-time'>{showoutTime} <Text className='weeks-text'>{outValue}</Text></View>
            </View>
            <View className='between-text'>
              共{DaysBetween}晚
              <Image
                src='https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png'
                className='content-arrow'
              ></Image>
              </View>
          </View>
          {/* 搜索框 */}
          <View className='content-row'>
            <Image
              src='https://bj.bcebos.com/htrip-mp/static/mall/decorate/yqzSearchIcon.png'
              className='content-icon'
            ></Image>
            <Input
              className='content-input'
              placeholder='关键字/位置/品牌/酒店名'
              placeholderClass='content-placeholder'
              value={keyword}
              onInput={(val) => {
                this.keywordInput(val);
              }}
            ></Input>
            {/* <View className='content-tip'>1间1人</View> */}
            <Image
              src='https://front-end-1302979015.file.myqcloud.com/images/c/images/right-angle-gray.png'
              className='content-arrow'
            ></Image>
          </View>
          <View className='content_btn' style={_safe_style_(`background: ${tmpStyle.bgGradualChange}`)} onClick={this.toYqz}>
            查找
          </View>
        </View>
      </View> : null
    );
  }
}

export default YqzModule;
