import React, { useState, useEffect, FC } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/base/couponsModule/index.js

import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';
import config from '../utils/config.js';
import handleData from '../utils/handleData.js';
import authHelper from '../../../utils/auth-helper.js';
import report from '@/sdks/buried/report';
import TextNavModule from '../../decorate/textNavModule';
import RetailCoupons from '../../decorate/couponsModule/retail';
import wxAppProxy from '@/wxat-common/utils/wxAppProxy';
import {useSelector} from 'react-redux'
import './index.scss';

const app = Taro.getApp();

const PADDING = config.PADDING;
interface CouponsModuleWrapProps {
  dataSource: Record<string, any>;
  list?: Array<Record<string, any>>;
}
const CouponsModule: FC<CouponsModuleWrapProps> = (props) => {
  const scoreName = useSelector(state => state.globalData.scoreName)
  const { dataSource = {}, list = [] } = props;
  const [_renderList, setRenderList] = useState<Array<Record<string, any>>>([]);

  useEffect(() => {
    if (Array.isArray(list) && list.length > 0) {
      setRenderList(list);
    } else {
      getList();
    }
    // 优惠劵曝光上报
    report.showCouponsModule();
  }, [dataSource, list]);

  const getList = () => {
    const { data } = dataSource;
    if (data && data.length) {
      const ids = handleData.pickUpItemNos(data, 'id').join(',');
      wxApi
        .request({
          url: api.coupon.query_status,
          checkSession: true,
          data: {
            couponIds: ids,
            isShelf: 1,
          },
        })
        .then((res) => {
          const renderList = res.data || [];
          // 判断当前优惠券是可领取
          renderList.forEach((item) => {
            item.canReceive = hasRemainCoupon(item) && canSpecUserReceive(item);
          });
          setRenderList(res.data || []);
        })
        .catch((error) => console.error(error));
    }
  };

  /**
   * 是否有剩余的优惠券
   * @param item
   * @return {*|boolean}
   */
  const hasRemainCoupon = (item) => {
    // quantity = 0表示不限制发放次数
    // remainAmount 优惠券剩余数
    return item.quantity === 0 || item.remainAmount > 0;
  };

  /**
   * 该用户是否可以继续领取
   * @param item
   */
  const canSpecUserReceive = (item) => {
    return item.claimNumber === 0 || item.userCollectCount < item.claimNumber;
  };

  const clickReceive = (item) => {
    if (!authHelper.checkAuth()) {
      return;
    }
    // const item = e.detail.item;
    if (item.canReceive) {
      report.clickGetCoupon(true, item.id);
      const couponNo = item.id;
      const receiveMethod = item.receiveMethod;
      if (receiveMethod) {
        getIntegralCoupon(couponNo);
      } else {
        getFreeCoupon(couponNo);
      }
    } else if (!hasRemainCoupon(item)) {
      wxApi.showToast({
        title: '优惠券已领完',
        icon: 'none',
      });
    } else if (!canSpecUserReceive(item)) {
      wxApi.showToast({
        title: '已达到领取上限',
        icon: 'none',
      });
    }
  };

  const gotoUse = (e) => {
    const item = e.detail.item;
    console.log('去使用item1 ->', item);
    handleToList();
  };

  const handleToList = () => {
    if (wxAppProxy.jumpToClassify) {
      wxAppProxy.jumpToClassify(null, null);
    } else {
      wxApi.$navigateTo({
        url: '/wxat-common/pages/homes/index',
      });
    }
  };

  const getFreeCoupon = (couponNo) => {
    const reqData = {
      couponId: couponNo,
    };

    wxApi
      .request({
        url: api.coupon.collect,
        loading: true,
        checkSession: true,
        data: reqData,
      })
      .then((res) => {
        wxApi.showToast({
          icon: 'none',
          title: '领取成功',
        });

        getList();
      })
      .catch(() => {
        // 出错时也刷新一次请求，目的是防止券被其他人抢完时，无法刷新的问题
        getList();
      });
  };

  const getIntegralCoupon = (couponNo) => {
    const reqData = {
      couponId: couponNo,
    };

    wxApi.showModal({
      title: `是否消耗${scoreName}兑换？`,
      content: '',
      success: (res) => {
        if (res.confirm) {
          wxApi
            .request({
              url: api.coupon.integral,
              loading: true,
              checkSession: true,
              data: reqData,
            })
            .then(() => {
              wxApi.showToast({
                icon: 'none',
                title: '领取成功',
              });

              getList();
            })
            .catch(() => {
              // 出错时也刷新一次请求，目的是防止券被其他人抢完时，无法刷新的问题
              getList();
            });
        }
      },
    });
  };

  const couponsStyle = {
    margin: `${Taro.pxTransform(dataSource.marginUpDown * 2)} ${Taro.pxTransform(dataSource.marginLeftRight * 2)}`,
    borderRadius: Taro.pxTransform(dataSource.radius * 2),
  };

  return (
    <View
      data-scoped='wk-cbc-CouponsModule'
      className='wk-cbc-CouponsModule coupons-module'
      style={_safe_style_(couponsStyle)}
    >
      {!!dataSource.showNav && (
        <TextNavModule dataSource={{ ...dataSource, ...dataSource.textNavSource }} padding={PADDING} />
      )}

      <View>
        <RetailCoupons
          PADDING={config.PADDING}
          _renderList={_renderList}
          onReceive={clickReceive}
          // FIXME: 没有触发
          onUse={gotoUse}
        />
      </View>
    </View>
  );
};

CouponsModule.defaultProps = {
  list: [], // list: null //直接渲染list列表
  dataSource: {}, // 传dataSource只是取dataSource里面的id  进行再次查询
};

export default CouponsModule;
