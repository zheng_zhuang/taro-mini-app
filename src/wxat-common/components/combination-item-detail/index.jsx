import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';

class CombinationItemDetail extends React.Component {
  static defaultProps = {
    combinationDTOS: [],
  };

  render() {
    const { combinationDTOS } = this.props;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcc-CombinationItemDetail'
        className='wk-wcc-CombinationItemDetail'
      >
        {!!(combinationDTOS && combinationDTOS.length > 0) && (
          <View className='combination-item-detail'>
            <View className='combination-item-title'>产品参数</View>
            <View className='combination-item-box'>
              <View className='combination-item-box-left'>组合明细：</View>
              <View className='combination-item-box-right'>
                {combinationDTOS.map((item, index) => {
                  return (
                    <View className='combination-item-box-tab'>
                      <Text className='limit-line line-2'>
                        {item.combinationItemName +
                          (item.combinationItemAttr ? ' (' + item.combinationItemAttr + ')' : '')}
                      </Text>
                      <Text className='combination-count'>{'x' + item.combinationCount}</Text>
                    </View>
                  );
                })}
              </View>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default CombinationItemDetail;
