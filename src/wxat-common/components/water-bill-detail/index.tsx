import React, { Component } from 'react'; // @externalClassesConvered(Empty)
import '@/wxat-common/utils/platform';
import { View, Picker, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import Empty from '@/wxat-common/components/empty/empty';
import filters from '@/wxat-common/utils/money.wxs.js';
import './index.scss';
import hoc from '@/hoc/index';
import { connect } from 'react-redux';
import utilDate from '@/wxat-common/utils/date';
export interface Props {
  dataSource: Array<Record<string, any>>;
  isMoney: boolean;
  onDateChangeEvent: (date: string) => void;
  scoreName?: string;
}

// 组件的state声明
interface State {
  title: string;
  selectDate:string | Date
}
const mapStateToProps = (state) => ({
  scoreName: state.globalData.scoreName || '积分',
});
@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
@hoc
class WaterBillDetail extends Component<Props, State> {
  static defaultProps = {
    dataSource: [],
    isMoney: false,
  };

  state = {
    title:'',
    selectDate:''
  };

  bindDateChange = (e) => {
    if (e.detail.value) {
      const startTime = utilDate.format(new Date(e.detail.value));
      this.setState({
        title: (this.props.isMoney ? '流水账单' : `${this.props.scoreName}明细`) + '(' + e.detail.value + ')',
        selectDate:startTime
      });

      const { onDateChangeEvent } = this.props;
      onDateChangeEvent && onDateChangeEvent(e.detail.value);
    }
  };
  componentDidMount(){
    this.setState({
      title: this.props.isMoney ? '账单' : `${this.props.scoreName}明细`,
    })
  }
  render() {
    const { title } = this.state;
    const { isMoney, dataSource } = this.props;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcw-WaterBillDetail'
        className='wk-wcw-WaterBillDetail'
      >
        <View className='water-bill-header'>
          <View className='title'>{title}</View>
          <Picker value={this.state.selectDate} mode='date' fields='month' start='2015-09-01' end='2100-09-01' onChange={this.bindDateChange}>
            <Image
              className='datetime'
              src='https://front-end-1302979015.file.myqcloud.com/images/c/images/mine/datetime.png'
            ></Image>
          </Picker>
        </View>
        <View style={{ background: 'white' }}>
          {!!(dataSource && dataSource.length === 0) && (
            <Empty style={{ margin: 0, width: '100%' }} message='暂无流水账单'></Empty>
          )}

          {!!(dataSource && dataSource.length > 0) && (
            <View className='water-bill-list'>
              {dataSource.map((item, i) => {
                return (
                  <View className='timeline' key={'water-bill-list-' + i}>
                    {item.bills.map((items, index) => {
                      return (
                        <View className='item' key={'water-bill-item-' + index}>
                          <View className='left'>
                            <View className='desc'>{items.desc || '-'}</View>
                            <View className='create-time'>{items.createTime}</View>
                          </View>
                          {isMoney ? (
                            <View className='right' style={{ color: items.amount > 0 ? '#FF743B' : '' }}>
                              {(items.amount > 0 ? '+' : '') + ' ' + filters.moneyFilter(items.amount, true)}
                            </View>
                          ) : (
                            <View className='right' style={{ color: items.amount > 0 ? '#FF743B' : '' }}>
                              {(items.amount > 0 ? '+' : '') + ' ' + items.amount}
                            </View>
                          )}
                        </View>
                      );
                    })}
                  </View>
                );
              })}
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default WaterBillDetail;
