import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../utils/money.wxs.js';
import api from '../../../api/index.js';
import wxApi from '../../../utils/wxApi';
import './index.scss';
import { connect } from 'react-redux';

type TicketDialogProps = {
  title?: string;
  backOpacity?: string;
  couponId?: number;
  order: string;
  activityId?: number;
  onHandleTicket: () => void;
};

type StateProps = {
  storeId: number;
};

type IProps = TicketDialogProps & StateProps;

interface TicketDialog {
  props: IProps;
}

const mapStateToProps = (state) => {
  return {
    storeId: (state.base.currentStore || {}).id,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class TicketDialog extends React.Component {
  static defaultProps = {
    title: '优惠券',
    backOpacity: '0.5',
    storeId: null,
  };

  state = {
    visible: false,
    couponData: null,
    ticketApplyDisable: false,
    ticketApplyTitle: '立即领取',
  };

  componentDidMount() {
    this.getInfo();
  }

  getInfo = async () => {
    const { order, activityId, couponId, onHandleTicket, storeId } = this.props;

    const params = {
      orderNo: order ? order : null, // 订单号
      storeId: storeId, // 门店id
      couponId: couponId ? +couponId : null,
      orderPresentInfoId: activityId ? +activityId : null, // 下单有礼活动的id
    };

    try {
      const { data, success } = await wxApi.request({
        url: api.courtesy.getOrderPresentCoupon,
        method: 'POST',
        loading: true,
        quite: true,
        data: params,
      });

      if (!success || !data || data.status === 3) {
        wxApi.showToast({ title: '该优惠券已用完', icon: 'none', duration: 2000 });
        onHandleTicket();
        return;
      }
      this.setState({ ticketApplyTitle: '确定' });

      const endTime = new Date();
      if (data.endTime) {
        data.endTime = new Date(data.endTime);
      } else {
        endTime.setDate(endTime.getDate() + data.fixedTerm);
        data.endTime = endTime;
      }
      data.endTime = +data.endTime;
      this.setState({ couponData: data, visible: true });
    } catch (err) {
      let title = '领取失败';
      if (err && err.data && err.data.errorMessage) {
        title = err.data.errorMessage;
      }

      wxApi.showToast({ title: title, icon: 'none', duration: 2000 });
      this.hideDialog();
    }
  };

  preventTouchMove = () => {
    // preventTouchMove
  };

  hideDialog = () => {
    const { onHandleTicket } = this.props;
    onHandleTicket();
  };
  render() {
    const { couponData, ticketApplyDisable, ticketApplyTitle, visible } = this.state;

    const { backOpacity, title } = this.props;

    const coupon = couponData || ({} as any);

    if (visible) {
      return (
        <View
          data-scoped='wk-cpt-TicketDialog'
          className='wk-cpt-TicketDialog ticket-dialog'
          onTouchMove={this.preventTouchMove}
        >
          <View className='wx-mask' style={_safe_style_('opacity:' + backOpacity + ';')} onClick={this.hideDialog} />

          <View className='ticket-float'>
            <Image className='ticket-float__image' src={require('./pay/float.png')} />
          </View>

          <View className='ticket-close-btn' onClick={this.hideDialog}>
            <Image className='ticket-close-btn__image' src={require('./pay/close.png')}></Image>
          </View>

          <View className='wx-dialog'>
            <View className='wx-dialog-title'>{title}</View>
            <View className='wx-dialog-content'>
              <View className='wx-dialog-item'>
                <View className='wx-dialog-info'>
                  <Text className='wx-dialog-info-title'>{coupon.name}</Text>
                  <Text className='wx-dialog-desc'>指定商品:所有商品</Text>
                  <Text className='wx-dialog-desc duration'>{'有效期:' + filters.dateFormat(coupon.endTime)}</Text>
                </View>
                <View className='wx-dialog-num'>
                {coupon.couponCategory === 2 ? (
                    <Text className='num'>{coupon.discountFee / 10}折</Text>
                  ) : (
                    <Text className='num'>{'¥' + filters.moneyFilter(coupon.discountFee, true)}</Text>
                  )}                  
                  {coupon.minimumFee ? (
                    <Text className='condition'>{'满' + filters.moneyFilter(coupon.minimumFee, true) + '可用'}</Text>
                  ) : (
                    <Text className='condition'>无门槛</Text>
                  )}
                </View>
              </View>
            </View>
            <View className={'btn-apply ' + (ticketApplyDisable ? 'disable-btn-apply' : '')} onClick={this.hideDialog}>
              {ticketApplyTitle}
            </View>
          </View>
        </View>
      );
    }

    return null;
  }
}

export default TicketDialog;
