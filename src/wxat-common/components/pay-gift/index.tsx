import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../utils/wxApi';
import TicketDialog from "./ticket-dialog";
import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import { connect } from 'react-redux';
import api from '@/wxat-common/api/index.js';
import React, { ComponentClass } from 'react';

interface PayGiftProps {
  order: string;
}

interface StateProps {
  storeId: number;
  userId: number;
}

type IProps = PayGiftProps & StateProps;

interface PayGift {
  props: IProps;
}

const mapStateToProps = (state) => {
  return {
    storeId: (state.base.currentStore || {}).id,
    userId: (state.base.userInfo || {}).id,
  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })
class PayGift extends React.Component {
  state = {
    dialogShow: false,
    showTicket: false,
    showLucky: false,
    ticketData: [],
    luckyData: [],
    disableCouponIds: {},
    id: undefined,
    couponId: undefined,
  };

  componentDidMount() {
    this.api();
  }

  toTicket = (ev: ITouchEvent) => {
    const { disableCouponIds } = this.state;

    const couponId = ev.currentTarget.dataset.coupon;
    const id = ev.currentTarget.dataset.id;

    if (disableCouponIds.hasOwnProperty(couponId)) {
      wxApi.showToast({ title: '你已经领取过优惠券了', icon: 'none', duration: 2000 });
      return;
    }

    this.setState({ id, dialogShow: true, couponId });
  };

  toLucky = (ev: ITouchEvent) => {
    const luckyId = ev.currentTarget.dataset.luckyid;
    // wxApi.$navigateTo({ url: `/sub-packages/marketing-package/pages/lucky-dial/index?id=${luckyId}` });
    wxApi.$navigateTo({ url: `/sub-packages/moveFile-package/pages/lucky-dial/index?id=${luckyId}` });
  };

  handleTicket = () => {
    const { couponId, disableCouponIds } = this.state;
    if (couponId) {
      this.setState({
        dialogShow: false,
        disableCouponIds: { ...disableCouponIds, [(couponId as unknown) as string]: true },
      });
    }
  };

  api = async () => {
    const { storeId, userId, order } = this.props;
    try {
      const { data, success } = await wxApi.request({
        url: api.courtesy.participate,
        method: 'POST',
        data: { orderNo: order, userId, storeId },
        quite: true,
      });

      if (!success) return;
      // 有下单有礼资格

      const courtesyData = data || [];
      const ticketData = courtesyData.filter((item) => item.couponId);
      const luckyData = courtesyData.filter((item) => item.luckyTurningId);
      this.setState({
        ticketData: ticketData,
        luckyData: luckyData,
      });

      if (ticketData.length) {
        this.setState({ showTicket: true });
      }
      if (luckyData.length) {
        this.setState({ showLucky: true });
      }
    } catch (err) {
      console.error('下单err', err);
    }
  };

  render() {
    const { showTicket, ticketData, showLucky, luckyData, couponId, id, dialogShow, disableCouponIds } = this.state;

    const { order } = this.props;

    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-wcp-PayGift' className='wk-wcp-PayGift'>
        <View className='pay-gift-container'>
          {!!showTicket && (
            <View className='ticket-area'>
              {ticketData.map((item: any) => {
                return (
                  <View key={item.couponId}>
                    <View
                      className='pay-gift-item'
                      onClick={_fixme_with_dataset_(this.toTicket, { id: item.id, coupon: item.couponId })}
                      style={{
                        backgroundImage: `url(${item.activityPhoto})`,
                        opacity: disableCouponIds[item.couponId] ? '0.7' : '1',
                      }}
                    ></View>
                  </View>
                );
              })}
            </View>
          )}

          {!!showLucky && (
            <View className='lucky-area'>
              {luckyData.map((item: any) => {
                return (
                  <View key={item.luckyTurningId}>
                    <View
                      className='pay-gift-item'
                      onClick={_fixme_with_dataset_(this.toLucky, { luckyid: item.luckyTurningId })}
                      style={{ backgroundImage: `url(${item.activityPhoto})` }}
                    ></View>
                  </View>
                );
              })}
            </View>
          )}
        </View>
        {!!dialogShow && (
          <TicketDialog
            couponId={couponId}
            activityId={id}
            order={order}
            onHandleTicket={this.handleTicket}
          ></TicketDialog>
        )}
      </View>
    );
  }
}

export default PayGift as ComponentClass<PayGiftProps>;
