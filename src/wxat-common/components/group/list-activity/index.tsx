import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
// components/buy-now/index.js
import wxApi from '../../../utils/wxApi';
import template from '../../../utils/template.js';

import CollageModule from '../../decorate/collageModule/index';
import './index.scss';

class ListActivity extends React.Component {

  state = {
    tmpStyle: {}
  }

  componentDidMount() {
    this.getTemplateStyle();
  }

  onGroup = (e) => {
    const item = e.detail.item;
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/group/detail/index',
      data: {
        activityId: item.id,
        itemNo: item.itemNo,
      },
    });
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { PADDING, activityList, showGroupType } = this.props;
    return (
      <View
        data-fixme="03 add view wrapper. need more test"
        data-scoped="wk-wcgl-ListActivity"
        className="wk-wcgl-ListActivity"
      >
        <CollageModule
          padding={PADDING}
          renderList={activityList}
          showGroupType={showGroupType}
          onClick={this.onGroup}
        ></CollageModule>
      </View>
    );
  }
}

export default ListActivity;
