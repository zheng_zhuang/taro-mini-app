import React from 'react'; // @externalClassesConvered(AnimatDialog)
import '@/wxat-common/utils/platform';
import {Button, Canvas, Image, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';
import ButtonWithOpenType from "@/wxat-common/components/button-with-open-type";
import wxApi from "@/wxat-common/utils/wxApi";
import api from "@/wxat-common/api";
import html2canvas from 'html2canvas'

interface IProps {
  posterData?: Record<string, any> | null;
  showShareDialog?: boolean;
  /**
   * 海报的二维码请求参数：
   * verificationNo: itemNo,
   * verificationType: 5,
   * maPath: 'wxat-common/pages/goods-detail/index'
   */
  qrCodeParams: Record<string, any> | null;
  /**
   * 是否为新的二维码接口，新的二维码接口请求结构有变
   * 接口变更为/auth/vip/verification/getNewQRCode
   * 直接传参sence和maPath，获取时也是直接获取
   */
  isNewQrCode?: boolean;
  posterType?: string;
  /**
   * 海报上面的提示语
   */
  posterTips: string;
  /**
   * 海报顶部logo，默认展示微信头像
   */
  posterLogo?: string;
  /**
   * 海报头部类型
   */
  posterHeaderType?: number;
  /**
   * 海报主图
   */
  posterImage?: string;
  /**
   * 海报图下面的名字
   */
  posterName?: string;
  /**
   * 海报优惠价格，已经转化过成元的价格
   */
  salePrice?: string;
  /**
   * 海报原价，已经转化过成元的价格
   */
  labelPrice?: string;
  /**
   * 海报优惠价标签
   */
  posterSalePriceLabel?: string;
  /**
   * 海报优惠价标签
   */
  posterLabelPriceLabel?: string;
  onSave: () => any;
  childRef?: any;
  /**
   * 二维码生成方式，使用映射ID的方式创建
   */
  uniqueShareInfoForQrCode?: object | null;
  /**
   * 是否需要展示海报原价
   */
  showLabelPrice: boolean;
  qrCodeUrlBase64: string;
}


class newShareDialog extends React.Component {
  state = {
    showShareDialog: false,
    posterData: null,
    qrCodeParams: null,
    isNewQrCode: false,
    posterTips: '向您推荐了这个商品',
    posterLogo: '',
    posterImage: '',
    posterName: '',
    salePrice: '',
    labelPrice: '',
    posterSalePriceLabel: '优惠价',
    posterLabelPriceLabel: '原价',
    showLabelPrice: true,
    uniqueShareInfoForQrCode: null,
    wxShareGuid: false,
    h5CodeUrl: null,
    base64Img: null,
    showBase64Img: false
  }
  componentDidMount() {
    wxApi
      .request({
        url: api.distribution.selectSingle,
        data: {
          activityType: 0,
        },
      })
      .then((res) => {
        this.setState({
          h5CodeUrl: res.data.wxGroupPicUrl
        })
      })
  }

  show = () => {}

  handleCloseShare = () => {
    this.props.onCloseEvent(false)
  }

  saveGoodsPosterImage = async () => {
    const imageWrapper = document.getElementById("postersContentMain");
    const imageWrapperWidth = imageWrapper.offsetWidth;
    const imageWrapperHeight = imageWrapper.offsetHeight;
    try {
      let canvas = await html2canvas(imageWrapper, {
        backgroundColor: null,
        useCORS: true,
        allowTaint: true,
        taintTest: false,
        width: imageWrapperWidth,
        height: imageWrapperHeight,
        scale: window.devicePixelRatio,
      });
      const posterUrl = canvas.toDataURL("image/png");
      this.setState({
        showBase64Img: true,
        base64Img: posterUrl
      })
    } catch (err) {}
  };
  closeBase64Img = () => {
    this.setState({
      showBase64Img: false
    })
  }
  openShareGuide = () => {
    this.setState({
      wxShareGuid: true
    })
  }
  closeShareGuide = () => {
    this.setState({
      wxShareGuid: false
    })
  }
  render() {
    const {
      wxShareGuid,
      h5CodeUrl,
      base64Img,
      showBase64Img,
    } = this.state;

    const {
      showShareDialog,
      salePrice,
      posterImage,
      posterName,
      labelPrice,
      commissionAmount,
      qrCodeUrlBase64,
    } = this.props;
    return (
      showShareDialog && (
        <View data-fixme='02 block to view. need more test' data-scoped='wk-wcs-NewShareDialog' className='wk-wcs-NewShareDialog'>
          <View className='commodity_screen'>
            <View className='new-share-dialog'>
              <View className='close-btn' onClick={this.handleCloseShare}></View>
              <View className='posters-content'>
                <View>
                  <View className='posters-title'>
                    <View className='posters-title-item'>
                      <View className='posters-title-left'></View>
                      <View className='posters-title-txt'>推荐购买预计赚</View>
                      <View className='posters-title-right'></View>
                    </View>
                  </View>
                  <View className='posters-price-box'>
                    <View className='t1'>¥</View>
                    <View className='t2'>{commissionAmount}</View>
                  </View>
                  <View className='posters-box'>
                    <View id='postersContentMain' className='posters-image'>
                      <View className='product-images'>
                        <Image className='images' src={`data:image/jpg;base64,${posterImage}`} />
                      </View>
                      <View className='product-name'>{posterName}</View>
                      <View className='product-tag'>抢购价</View>
                      <View className='product-price'>
                        <View className='p1'>¥{salePrice}</View>
                        <View className='p2'>¥{labelPrice}</View>
                      </View>
                      <View className='product-info'>
                        <View className='icon i1'>
                          <Image className='image' src={require('./icon_share_jtxx.png')}></Image>
                          <View className='txt'>精挑细选</View>
                        </View>
                        <View className='icon i2'>
                          <Image className='image' src={require('./icon_share_ccyh.png')}></Image>
                          <View className='txt'>超值优惠</View>
                        </View>
                        <View className='icon i3'>
                          <Image className='image' src={require('./icon_share_pzbz.png')}></Image>
                          <View className='txt'>品质保障</View>
                        </View>
                      </View>
                      <View className='qr-code'>
                        <Image className='qr-code-image' src={`data:image/jpg;base64,${qrCodeUrlBase64}`}></Image>
                      </View>
                      <View className='qr-tis'>
                        <View className='tis'>长按立享优惠</View>
                      </View>
                    </View>
                  </View>
                </View>
                <View className='btn-tool'>
                  <View className='share-item'>
                    <ButtonWithOpenType openType='share' className='share-btn' onClick={this.openShareGuide}>
                      <View className='image-block'>
                        <View className='share-img friend'></View>
                        <View className='txt'>微信好友</View>
                      </View>
                    </ButtonWithOpenType>
                  </View>
                  <View className='share-item'>
                    <Button className='share-btn' onClick={this.saveGoodsPosterImage}>
                      <View className='image-block'>
                        <View className='share-img posters'></View>
                        <View className='txt'>生成海报</View>
                      </View>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </View>
          {wxShareGuid && (
            <View className="wx-share-guide">
              <View className="wx-share-guide-box">
                <Image className='tis-image' src="https://htrip-static.ctlife.tv/wk/marketing-base/guide.png"></Image>
                <Image onClick={this.closeShareGuide} className="close-image" src="https://htrip-static.ctlife.tv/wk/marketing-base/button.png"></Image>
              </View>
            </View>
          )}
          <Canvas id='shareCanvas' canvasId='shareCanvas' className='canvas'></Canvas>
          {showBase64Img && (
            <View className='showBase64ImgBox'>
              <View>
                <Image className='base64Img' src={base64Img} ></Image>
                <View className='longTis'>长按图片保存</View>
                <View className='closeImg' onClick={this.closeBase64Img}></View>
              </View>
            </View>
          )}
        </View>
      )
    )
  }
}
export default newShareDialog;











