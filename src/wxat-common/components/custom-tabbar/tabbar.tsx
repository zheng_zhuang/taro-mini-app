import React, {FC, useEffect, useMemo, useState} from 'react';
import {_safe_style_, assets} from '@/wxat-common/utils/platform';
import {Block, View, Form, Button, Image, Text} from '@tarojs/components';
import Taro from '@tarojs/taro';
import {useSelector} from 'react-redux';
import screen from '../../utils/screen';
import wxApi from '../../utils/wxApi';
import api from "../../api";
import report from '@/sdks/buried/report/index.js';
import './tabbar.scss';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import classNames from 'classnames';

// 一级页面的菜单导航组件（底部菜单导航）
interface CustomTabbarProps {
  pagePath?: string;
}

const mapStateToProps = ({globalData}) => ({
  tabbars: globalData.tabbars
});

const editTabbar = (tabbar1, currentPagePath) => {
  if (!tabbar1) return;

  const tabbar = {...tabbar1};

  let pagePath = currentPagePath || wxApi.$getCurrentPageRoute();
  if (pagePath === '/' || !pagePath) {
    pagePath = '/wxat-common/pages/home/index';
  }

  pagePath.indexOf('/') != 0 && (pagePath = '/' + pagePath);

  tabbar.list = tabbar.list.map((item) => {
    const tabbarItem = {...item};
    // if (tabbarItem.linkId) {
    //   tabbarItem.pagePath = JSON.parse(tabbarItem.linkId).index
    // }
    tabbarItem.pagePath = tabbarItem.pagePath || '';
    if (!tabbarItem.pagePath.startsWith('/')) {
      tabbarItem.pagePath = '/' + tabbarItem.pagePath;
    }
    if (!tabbarItem.pagePath.startsWith('/wxat-common')) {
      tabbarItem.pagePath = '/wxat-common' + tabbarItem.pagePath;
    }

    if (tabbarItem.iconPath.startsWith('images')) {
      // 是否是本地图标，如果是本地图标，选中时，需要加背景色
      tabbarItem.isLocalIcon = true;
      // 不能使用asset函数拼接图片的路径（路径是 images/tabbar/xxxx）,因为项目设置打包后的部署路径是在/taro-h5/目录下，这样会拼接上项目的部署目录，而图片资源的引用路径都是第三方路径
      // tabbarItem.iconPath = assets('/' + tabbarItem.iconPath);

      tabbarItem.iconPath = `/${tabbarItem.iconPath}`
    }

    if (tabbarItem.selectedIconPath.startsWith('images')) {
      // tabbarItem.selectedIconPath = assets('/' + tabbarItem.selectedIconPath);

      tabbarItem.selectedIconPath = `/${tabbarItem.selectedIconPath}`
    }

    // 是否是本地图标，如果是本地图标，选中时，需要加背景色
    if (tabbarItem.selectedIconPath.startsWith('/images')) {
      tabbarItem.isLocalIcon =
        tabbarItem.selectedIconPath.startsWith('images') || tabbarItem.selectedIconPath.startsWith('/images');
    }

    const params = Taro.getCurrentInstance().router.params
    if (tabbarItem.pagePath.includes(pagePath) || pagePath.includes(tabbarItem.pagePath) || tabbarItem.pagePath.includes(params.$componentId)) {
      tabbarItem.selected = true;
    } else {
      tabbarItem.selected = false;
    }

    if (!tabbarItem.iconPath.startsWith('http')) {
      tabbarItem.iconPath =
        'https://front-end-1302979015.file.myqcloud.com/images/c/images' + tabbarItem.iconPath.slice(7);
    }
    if (!tabbarItem.selectedIconPath.startsWith('http')) {
      tabbarItem.selectedIconPath =
        'https://front-end-1302979015.file.myqcloud.com/images/c/images' + tabbarItem.selectedIconPath.slice(7);
    }
    return tabbarItem;
  });

  return tabbar;
};

const sendTempMessage = (event) => {
  const formId = event.detail.formId === 'the formId is a mock one' ? null : event.detail.formId;
  if (formId) return;
  wxApi.request({url: api.tempMessage, data: {formId}, quite: true, loading: false}).then(() => {
  });
};

const handleNavigate = ({page, index, title, item}, event) => {

  // FIX: 多次点击当前页
  const curPage = Taro.Current.page;
  const curPageth = (curPage && curPage.path) || '';
  if (curPageth.includes(page)) return;


  sendTempMessage(event);
  const urlObj = {
    url: page,
    data: {
      title,
      IS_TABBAR: 'tabbar',
    },
  }

  console.log("item", item)
  // 储存当前导航自定义页面的config
  if (item.config) {
    Taro.preload({
      pageConfig: item.config,
      type: "custom"
    })
  }
  // 无config 不是自定义页面 存储页面信息给tabbar页面判断
  else {
    Taro.preload({
      pageConfig: item,
      type: "system"
    })
  }

  if (process.env.TARO_ENV === 'h5') {
    wxApi.$redirectTo(urlObj);
  } else {
    wxApi.$navigateTo(urlObj);
  }
};

const CustomTabbar: FC<CustomTabbarProps> = (props) => {
  const tabbarHeight = screen.tabbarHeight;
  const tmpStyle = useTemplateStyle();
  const {tabbars} = useSelector(mapStateToProps);
  const [imageLoaded, setImageLoaded] = useState(0);

  const tabbarData = useMemo(() => editTabbar(tabbars, props.pagePath), [tabbars]);
  const handleImageLoad = (index) => {
    // 延迟加载背景色，等图片加载完毕时，再加载背景，否则会有一坨背景色在那里
    setImageLoaded((bit) => bit | (1 << index));
  };

  useEffect(() => {
    wxApi.hideTabBar();
  }, []);

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-wcc-Tabbar' className='wk-wcc-Tabbar'>
      {!!(!!tabbarData && tabbarData.list) && (
        <View
          className='tabbar_box'
          style={_safe_style_(`background-color:${tabbarData.backgroundColor}; height: ${tabbarHeight}px;`)}
        >
          {(tabbarData.list || []).map((item, index) => {
            return (
              <Block key={'pagePath' + index}>
                <Form onSubmit={handleNavigate.bind(null, {
                  index: index,
                  page: item.pagePath,
                  title: item.name,
                  item: item
                })}
                      reportSubmit>
                  <Button className='tabbar_nav' formType='submit'>
                    <View
                      className='tabbar-icon-box'
                      style={{
                        background: `${
                          item.selected && item.isLocalIcon && imageLoaded & (1 << index)
                            ? tmpStyle.btnColor
                            : '#ffffff'
                        }`,
                      }}
                    >
                      <Image
                        className='tabbar_icon'
                        src={item.selected ? item.selectedIconPath : item.iconPath}
                        onLoad={() => handleImageLoad(index)}
                      ></Image>
                    </View>
                    <Text
                      className='tab-text'
                      style={_safe_style_(`color:${item.selected ? tmpStyle.btnColor : 'black'};`)}
                    >
                      {item.text}
                    </Text>
                  </Button>
                </Form>
              </Block>
            );
          })}
        </View>
      )}
    </View>
  );
};

export default CustomTabbar;
