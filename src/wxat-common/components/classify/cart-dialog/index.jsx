import React from 'react'; // @externalClassesConvered(AnimatDialog)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { View, Text, Label, Radio, Image, Input } from '@tarojs/components';
import Taro from '@tarojs/taro';
import buyHub from '../../cart/buy-hub';
import cartHelper from '../../cart/cart-helper.js';
import AnimatDialog from '../../animat-dialog/index';
import './index.scss';
import screen from '../../../utils/screen';
import filters from '../../../utils/money.wxs.js';
import template from '../../../utils/template.js';
import protectedMailBox from '../../../utils/protectedMailBox';
import pageLinkEnum from '../../../constants/pageLinkEnum';
import wxApi from '../../../utils/wxApi';

class CartDialog extends React.Component {
  static defaultProps = {
    onClear: null,
  };

  /**
   * 组件的初始数据
   */
  state = {
    tabbarHeight: screen.tabbarHeight,
    list: [],
    tmpStyle: {},
    price: 0,
    editMode: null,
  }; /*请尽快迁移为 componentDidMount 或 constructor*/

  UNSAFE_componentWillMount() {
    this.getTemplateStyle();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  //购买数量改变，修改count
  // FIXME:  没有被调用
  onBuyChange(e) {
    const { list } = this.state;
    const index = e.target.dataset.index;
    list[index].count = e.detail;
    if (list[index].count === 0) {
      //删除当前项
      list.splice(index, 1);
    }
    this.setState({ list });
  }

  handleClear = () => {
    this.props.onClear && this.props.onClear();
  };

  //减少商品件数
  handleDecreaseBuyNum = ({ index }, e) => {
    e.stopPropagation();
    const item = this.state.list[index];
    if (!cartHelper.isGettingCarts() && !cartHelper.isGettedCarts()) {
      wxApi.showToast({
        title: '正在请求购物车数据，请稍后重试',
        icon: 'none',
      });

      //只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
      if (!cartHelper.isGettingCarts()) {
        cartHelper.getCarts(true);
      }
      return;
    }
    cartHelper.addOrUpdateCart(item, 1, false, (res) => {
      this.onInsertOrUpdateEnd(res);
    });
  };

  //增加商品件数
  handleIncreaseBuyNum = ({ index }, e) => {
    e.stopPropagation();
    const item = this.state.list[index];
    // fixme 保留库存不足的逻辑
    if (item.count < item.stock) {
      if (!cartHelper.isGettingCarts() && !cartHelper.isGettedCarts()) {
        wxApi.showToast({
          title: '正在请求购物车数据，请稍后重试',
          icon: 'none',
        });

        //只有请求出错时才重新加载购物车，正在请求购物车数据，不用再发起请求
        if (!cartHelper.isGettingCarts()) {
          cartHelper.getCarts(true);
        }
        return;
      }
      cartHelper.addOrUpdateCart(item, 1, true, (res) => {
        this.onInsertOrUpdateEnd(res);
      });
    } else {
      wxApi.showToast({
        title: '商品库存不足',
        icon: 'none',
      });
    }
  };

  onInsertOrUpdateEnd(res) {
    if (!res.data) {
      if (res.errorCode === cartHelper.cartCode.LOW_STOCK) {
        wxApi.showToast({
          title: '库存不足',
          icon: 'none',
        });
      } else {
        wxApi.showToast({
          title: '添加失败',
          icon: 'none',
        });
      }
    }
    this.listCart();
  }

  show() {
    this.setState({
      list: this.listCart(),
    });

    setTimeout(() => {
      this.animatDialogCOMPT && this.animatDialogCOMPT.show({ translateY: 300 });
    }, 17);
  }

  listCart() {
    const list = [];
    buyHub.mapIndex.forEach((itemNo) => {
      list.push(buyHub.map[itemNo]);
    });
    this.setState({ list }, this.count);
    return list;
  }

  count = () => {
    let price = 0;
    this.state.list.forEach((item) => {
      if (item._checked) {
        price += item.price * item.count; /*商品价格乘以总数，才是总价格*/
      }
    });
    this.setState({ price });
  };

  //去购买
  handlePay = () => {
    const skuList = [];
    this.state.list.forEach((item) => {
      if (item._checked) {
        skuList.push({
          itemCount: item.count,
          itemNo: item.itemNo,
          barcode: item.barcode,
          name: item.name,
          skuId: item.skuId || null,
          salePrice: item.price,
          pic: item.thumbnail,
          skuTreeNames: item.skuInfo,
          freight: item.freight,
          noNeedPay: item.noNeedPay,
          reportExt: item.reportExt,
          drugType: item.drugType,
        });
      }
    });
    if (!skuList.length) {
      wxApi.showToast({
        title: '未选中商品',
        icon: 'none',
      });
    } else {
      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', skuList);
      wxApi.$navigateTo({
        url: pageLinkEnum.orderPkg.payOrder,
      });
    }
  };

  //单条sku，点击check按钮
  handlerCheck = ({ index }) => {
    const item = { ...this.state.list[index] };
    item._checked = !item._checked;
    const list = [...this.state.list];
    list[index] = item;
    this.setState({ list }, this.count);
  };

  hide() {
    this.animatDialogCOMPT && this.animatDialogCOMPT.hide();
  }

  refAnimatDialogCOMPT = (node) => (this.animatDialogCOMPT = node);

  render() {
    const { tabbarHeight, tmpStyle, list, price, editMode } = this.state;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-ccc-CartDialog'
        className='wk-ccc-CartDialog'
      >
        <AnimatDialog
          ref={this.refAnimatDialogCOMPT}
          animClass='card-dialog'
          customStyle={'bottom:' + tabbarHeight + 'px;'}
        >
          <View className='title'>
            <Text>已选商品</Text>
            <Text onClick={this.handleClear} className='clear' style={_safe_style_('color:' + tmpStyle.btnColor)}>
              清空购物车
            </Text>
          </View>
          <View className='row-wrap'>
            {list.map((item, index) => {
              const bound = { index };
              return (
                <Label className='row' key={index} onClick={this.handlerCheck.bind(this, bound)}>
                  <Radio color={tmpStyle.btnColor} value={item._checked} checked={item._checked}></Radio>
                  <View className='thumbnail-box'>
                    <Image
                      className='thumbnail'
                      src={item.thumbnail}
                      modes='aspectFill'
                      onClick={_fixme_with_dataset_(this.click, { item: item.itemNo })}
                    ></Image>
                    {!!item.preSell && <View className='pre-sale-label'>预售</View>}
                  </View>
                  <View className='info'>
                    <View className='name limit-line'>{item.name}</View>
                    <View className='attrs'>{item.skuInfo}</View>
                    <View className='price-count'>
                      <View className='price' style={_safe_style_('color:' + tmpStyle.btnColor)}>
                        {'￥' + filters.moneyFilter(item.price, true)}
                      </View>
                      <View className='num-box'>
                        <View
                          className={'num-decrease left-radius ' + (item.count == 0 ? 'num-disable' : '')}
                          onClick={this.handleDecreaseBuyNum.bind(this, bound)}
                        >
                          -
                        </View>
                        <View className='num-input'>
                          <Input className='input-cpt' type='number' value={item.count} disabled></Input>
                        </View>
                        {/* fixme 保留库存不足的逻辑 */}
                        <View
                          className={'num-increase right-radius ' + (item.count == item.itemStock ? 'num-disable' : '')}
                          onClick={this.handleIncreaseBuyNum.bind(this, bound)}
                        >
                          +
                        </View>
                      </View>
                    </View>
                  </View>
                </Label>
              );
            })}
          </View>
          <View className='btn-box'>
            <View>
              <Text className='count-tip'>合计：</Text>
              {!editMode && <Text className='money'>{'￥' + filters.moneyFilter(price, true)}</Text>}
            </View>
            {!editMode && (
              <View
                className='pay-btn'
                onClick={this.handlePay}
                style={_safe_style_('background:' + tmpStyle.btnColor)}
              >
                结算
              </View>
            )}
          </View>
        </AnimatDialog>
      </View>
    );
  }
}

export default CartDialog;
