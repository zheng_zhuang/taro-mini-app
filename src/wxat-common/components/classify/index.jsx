import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Image, ScrollView } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../utils/wxApi';
import api from "../../api";
import constants from "../../constants";
import report from "../../../sdks/buried/report";
import template from '../../utils/template.js';
import screen from '../../utils/screen';
import goodsTypeEnum from '../../constants/goodsTypeEnum.js';
import util from '../../utils/util.js';
import imageUtils from '../../utils/image.js';

import GoodsItem from '../industry-decorate-style/goods-item';
import LoadMore from '../load-more/load-more';
import Error from '../error/error';
import Empty from '../empty/empty';
import Search from "../search";
import BuyBottom from "./buy-bottom";
import './index.scss';
import { connect } from 'react-redux';

import getStaticImgUrl from '../../constants/frontEndImgUrl'

const IMG_OF_NORMAL = getStaticImgUrl.sort.normal_png;
const IMG_OF_DESC = getStaticImgUrl.sort.desc_png;
const IMG_OF_ASC = getStaticImgUrl.sort.asc_png;

const loadMoreStatusEnum = constants.order.loadMoreStatus;

const mapStateToProps = (state) => ({
  currentStore: state.base.currentStore,
});

const mapDispatchToProps = (dispatch) => ({});

@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
class Classify extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    // 是否打开快速购买
    isQuickBuyOpen: false,
    reportSource: '',
    navBackgroundColor: '#ffffff',
  };

  /**
   * 组件的初始数据
   */
  state = {
    tabbarHeight: screen.tabbarHeight,
    statusBarHeight: 0,
    navHeight: 0,
    error: false,
    // 左侧的分类
    category: null,
    // 分类对应的商品数据
    dataSource: [],
    // 点击左侧的索引
    currentIndex: 0,
    // 排序的索引
    currentSortIndex: 0,
    bannerImage: '',
    loadMoreStatus: loadMoreStatusEnum.HIDE,
    customCategoryLength: 0,
    tmpStyle: {},
    frontColor: '#ffffff',
    sortArray: [
      {
        name: '综合',
        id: null,
        icon: null,
      },

      {
        name: '销量',
        id: 'salesVolume',
        icon: null,
      },

      {
        name: '价格',
        id: 'salePrice',
        icon: null,
      },
    ],

    sortField: null,
    // true：降序， false: 升序
    sortType: true,
    // 分类的类型 一级还是多级
    categoryType: 0,
    // 多级分类的排列方式
    columnNum: 3,
    showType: 0,
    // 某个多级分类的下一层的子数据
    childCategorys: null,
    twoAndthree: false, // 多级分类下 有的2级分类下面没有3级分类   有的2级分类下面有3级分类
  };

  hasMore = true;
  pageNo = 1;

  componentDidMount() {
    this.setState({
      statusBarHeight: screen.statusBarHeight,
      navHeight: screen.navigationBarHeight,
      navTotalHeight: screen.totalNavigationBarHeight,
    });

    // 获取分类的类型 1级分类 还是多级分类
    this.queryCategoryType();
    this.getTemplateStyle();
    // 查询当前页面的否在tabbar的页面

    if (wxApi.$getCurrentPageRoute() === 'wxat-common/pages/slot-page/index') {
      this.setState({
        tabbarHeight: 0,
      });
    }
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    const { currentStore } = this.props;
    if (currentStore !== nextProps.currentStore) {
      this.queryCategoryType();
    }
  }

  /**
   * 点击重新获取分类数据
   */
  onReloadConfig = () => {
    this.queryCategoryType(); // 分类类型查询
  };

  handleChangeStore() {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/store-list/index',
    });
  }

  /**
   * 分类类型查询
   */
  queryCategoryType() {
    wxApi
      .request({
        // url: api.classify.categoryType,
        url: api.classify.categoryTypeV2,
      })
      .then((res) => {
        const data = res.data ? JSON.parse(res.data) : { categoryLevel: 1, columnNum: 3 };
        if (data.categoryLevel === 1) {
          // 1级分类
          this.queryCategory();
        } else {
          // 多级分类
          this.queryRootCategory();
        }
        this.setState({
          categoryType: data.categoryLevel,
          columnNum: data.columnNum,
        });
      })
      .catch((error) => {
        this.setState({
          error: true,
        });
      });
  }

  /**
   * 查询一级分类
   */
  queryCategory() {
    this.setState({
      error: false,
    });

    const params = {
      // itemType: 1
      typeList: [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value],
    };

    wxApi
      .request({
        url: api.classify.categoryNew,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        const category = res.data || [];
        this.setState(
          {
            category,
          },

          () => {
            if (res.success === true && res.data && category.length) {
              this.freshGoodsList();
            } else {
              this.resetData();
            }
          }
        );
      })
      .catch((error) => {
        this.setState({
          error: true,
        });
      });
  }

  /**
   * 查询多级分类
   */
  queryRootCategory() {
    wxApi
      .request({
        url: api.classify.categoryRoot,
        data: {
          itemType: 1,
        },
      })
      .then((res) => {
        const category = res.data || [];
        this.setState({
          category,
        }, () => {
          if (res.success === true && res.data && category.length) {
            this.setState(
              {
                error: false,
                childCategorys: [],
                dataSource: [],
              },
  
              () => {
                this.queryRootCategoryChild(category[0].id);
              }
            );
          } else {
            this.resetData();
          }
        });

      })
      .catch((error) => {
        this.setState({
          error: true,
        });
      });
  }

  /**
   * 查询多级分类下面的子分类
   */
  queryRootCategoryChild(parentId) {
    this.setState({
      error: false,
    });

    wxApi
      .request({
        url: api.classify.categoryChildNew,
        loading: true,
        data: {
          id: parentId,
        },
      })
      .then((res) => {
        if (res.success === true && !!res.data) {
          // 处理数据，将数据扁平化
          const childCategorys = res.data.childrenCategory;
          // 有子分类
          if (childCategorys && childCategorys.length) {
            this.setState({
              childCategorys: this.flatterArray(childCategorys),
              showType: 1,
            });
          } else {
            // 没有子分类  展示商品数据
            this.queryItemSku(res.data.id);
          }
        }
      })
      .catch((error) => {
        this.setState({
          error: true,
        });
      });
  }

  flatterArray(arr) {
    let x = [];
    arr.forEach((item) => {
      if (item.childrenCategory && item.childrenCategory.length) {
        x = x.concat(this.flatterArray(item.childrenCategory));
      } else {
        x.push(item);
      }
    });
    return x;
  }

  reload() {
    this.buyBottomCOMPT && this.buyBottomCOMPT.reload();
  }

  /**
   * 点击左边的分类按钮
   */
  clickClassify(value) {
    const { category, currentIndex, categoryType } = this.state;
    const id = parseInt(value.currentTarget.id);
    report.clickCategoryItem(0, category[id].id);
    if (currentIndex === id) {
      return;
    }
    if (categoryType === 1) {
      this.setState(
        {
          currentIndex: id,
          // dataSource: [],屏蔽请求时露出空提示
          bannerImage: null,
        },

        () => {
          this.freshGoodsList();
        }
      );
    } else {
      this.pageNo = 1;
      this.setState(
        {
          currentIndex: id,
          childCategorys: [],
          // dataSource: [],
        },

        () => {
          this.queryRootCategoryChild(category[id].id);
        }
      );
    }
  }

  /**
   * 点击排序按钮
   */
  clicksortBtn = (value) => {
    const { currentSortIndex, sortType } = this.state;
    const index = parseInt(value.currentTarget.id);
    if (index !== 2) {
      if (currentSortIndex !== index) {
        // index相同的不做出来
        this.setState(
          {
            currentSortIndex: index,
          },

          () => {
            this.freshGoodsList();
          }
        );
      }
    } else {
      // 价格排序
      let newSortType;
      if (currentSortIndex === index) {
        newSortType = !sortType;
      } else {
        // 重置之后先降序
        newSortType = true;
      }
      this.setState(
        {
          currentSortIndex: index,
          sortType: newSortType,
        },

        () => {
          this.freshGoodsList();
        }
      );
    }
  };

  resetData() {
    this.setState({
      error: false,
      dataSource: [],
      currentIndex: 0,
      currentSortIndex: 0,
      sortField: null,
      // true：降序， false: 升序
      sortType: true,
    });

    this.hasMore = true;
    this.pageNo = 1;
  }

  // 底部加载更多
  onScrollViewReachBottom = () => {
    const hasMore = this.hasMore;
    const { category, currentIndex, categoryType } = this.state;
    if (hasMore && category) {
      this.setState(
        {
          loadMoreStatus: loadMoreStatusEnum.LOADING,
        },

        () => {
          if (categoryType === 1) {
            this.queryItemSku();
          } else {
            this.queryRootCategoryChild(category[currentIndex].id);
          }
        }
      );
    }
  };

  // 重试加载
  onRetryLoadMore = () => {
    this.hasMore = true;
    this.setState(
      {
        loadMoreStatus: loadMoreStatusEnum.LOADING,
      },

      () => {
        this.queryItemSku();
      }
    );
  };

  freshGoodsList() {
    this.pageNo = 1;
    this.hasMore = true;
    this.queryItemSku();
  }

  /**
   * 查询分类中的子数据
   */
  queryItemSku(id) {
    const { dataSource, category, sortArray, sortType, currentIndex, currentSortIndex } = this.state;
    const categoryId = id || category[currentIndex].id;
    const sortField = sortArray[currentSortIndex].id;

    const params = {
      status: 1,
      // type: 1,
      pageNo: this.pageNo,
      pageSize: 10,
      categoryId: categoryId,
      // 按 xx 方式排序
    };
    if (sortField) {
      params.sortField = sortField;
    }
    // 价格才有排序
    if (currentSortIndex === 2) {
      params.sortType = sortType ? 'desc' : 'asc';
    }
    params.typeList = [goodsTypeEnum.PRODUCT.value, goodsTypeEnum.COMBINATIONITEM.value];

    report.clickChangeRank(params.sortField, params.sortType);
    wxApi
      .request({
        url: api.classify.itemSkuList,
        loading: true,
        data: util.formatParams(params), // 将接口请求参数转化成单个对应的参数后再传参
      })
      .then((res) => {
        if (res.success === true) {
          let hasMore = true;
          let pageNo = this.pageNo;
          let newDataSource;
          const resData = res.data || [];
          resData.forEach((item) => {
            const thumbnail = item.thumbnail || (item.wxItem ? item.wxItem.thumbnail : '');
            item.thumbnailMin = imageUtils.thumbnailMin(thumbnail);
            item.thumbnailMid = imageUtils.thumbnailMid(thumbnail);
          });

          if (this.isLoadMoreRequest()) {
            newDataSource = dataSource.concat(resData);
          } else {
            newDataSource = resData;
          }
          if (newDataSource.length === res.totalCount) {
            hasMore = false;
          } else {
            pageNo += 1;
          }

          this.hasMore = hasMore;
          this.pageNo = pageNo;

          this.setState({
            dataSource: newDataSource,
            showType: 0,
          });

          const goodsExposure = [];
          resData.forEach((item) => {
            let itemNo;
            if (!!(itemNo = item.wxItem) && !!(itemNo = itemNo.itemNo)) {
              goodsExposure.push(itemNo);
            }
          });
          if (!!goodsExposure && !!goodsExposure.length) {
            report.goodsExposure(goodsExposure.join(','));
          }
        }
      })
      .catch((error) => {
        if (this.isLoadMoreRequest()) {
          this.setState({
            loadMoreStatus: loadMoreStatusEnum.ERROR,
          });
        }
      });
  }

  isLoadMoreRequest() {
    return this.pageNo > 1;
  }

  clickBannerImage = (e) => {
    const { category, currentIndex } = this.state;
    const imgUrlValue = category[currentIndex].imgUrlValue;
    if (!imgUrlValue) {
      return;
    }
    const detailId = this.getImgUrlValueParams(imgUrlValue, 'detailId');
    report.clickCategoryBanner(detailId);
    wxApi.$navigateTo({
      url: '/wxat-common/pages/goods-detail/index',
      data: {
        itemNo: detailId,
      },
    });
  };

  // 分解后台返回的商品图片字符串中的参数
  getImgUrlValueParams(str, name) {
    let reName = str.split(name + '=')[1];
    if (reName) {
      reName = reName.split('&')[0];
    } else {
      return null;
    }
    return reName;
  }

  clickChildCategory(e) {
    const item = e.currentTarget.dataset.item;
    report.clickCategoryItem(1, item.id);
    wxApi.$navigateTo({
      url: '/sub-packages/marketing-package/pages/category-detail/index',
      data: {
        id: item.id,
        name: item.category,
        source: this.props.reportSource,
      },
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  refBuyBottomCOMPT = (node) => (this.buyBottomCOMPT = node);

  render() {
    const { isQuickBuyOpen, reportSource, navBackgroundColor } = this.props;
    const {
      category,
      error,
      statusBarHeight,
      navHeight,
      tabbarHeight,
      navTotalHeight,
      currentIndex,
      currentSortIndex,
      tmpStyle,
      sortArray,
      sortType,
      showType,
      dataSource,
      childCategorys,
      loadMoreStatus,
      columnNum,
    } = this.state;

    const categoryLoading = !category;
    const categoryEmpty = !!category && category.length === 0;

    // 出错
    if (error)
      return (
        <View data-scoped='wk-wcc-Classify' className='wk-wcc-Classify classify'>
          <Error onRetry={this.onReloadConfig} />
        </View>
      );

    // 加载中
    if (categoryLoading) return <View data-scoped='wk-wcc-Classify' className='wk-wcc-Classify classify' />;

    // 数据为空
    if (categoryEmpty)
      return (
        <View data-scoped='wk-wcc-Classify' className='wk-wcc-Classify classify'>
          <Empty message='暂无商品，切换门店试试' onAction={this.handleChangeStore} />
        </View>
      );

    return (
      <View data-scoped='wk-wcc-Classify' className='wk-wcc-Classify classify'>
        <Search />
        {/* 顶部搜索框区域 */}
        <View className='content'>
          <ScrollView 
            scrollY
            className='left' style={_safe_style_(`bottom:${tabbarHeight + (isQuickBuyOpen ? 44 : 0)}px; padding-bottom: 55px`)}>
            {!!category &&
              category.map((item, index) => {
                return (
                  <View
                    key={`${item.id}${index}`}
                    id={'00'+index} // 前缀加00规避taro的遍历渲染事件被覆盖
                    onClick={this.clickClassify.bind(this)}
                    style={_safe_style_(
                      'color:' +
                        (currentIndex === index ? 'rgba(55,58,68,1)' : 'rgba(162,162,164,1)') +
                        ';background-color:' +
                        (currentIndex === index ? 'rgba(255,255,255,1)' : 'rgba(240,241,245,1)') +
                        ';padding:' +
                        '10px 0'
                    )}
                  >
                    <Text className='category-item limit-line line-2'>{item.category}</Text>
                  </View>
                );
              })}
          </ScrollView>
          <ScrollView className='right'
            scrollY
            onScrollToLower={this.onScrollViewReachBottom}
            style={_safe_style_('min-height: calc(100vh - 48px);bottom: ' + (tabbarHeight + (isQuickBuyOpen ? 44 : 0)) + 'px;')}>
            {!showType && (
              <View className='header'>
                <View className='tip-title'>{category[currentIndex].category}</View>
                <View className='sort-container'>
                  {!!sortArray &&
                    sortArray.map((item, index) => {
                      return (
                        <View
                          key={index}
                          id={index}
                          className='sort-container__view'
                          onClick={this.clicksortBtn.bind(this)}
                          style={_safe_style_(
                            'color:' +
                              (currentSortIndex === index ? 'rgba(255,255,255,1)' : 'rgba(162,162,164,1)') +
                              ';background-color:' +
                              (currentSortIndex === index ? tmpStyle.btnColor : 'rgba(240,241,245,1)')
                          )}
                        >
                          <View className='sort-title'>
                            <Text>{item.name}</Text>
                          </View>
                          {index === 2 && (
                            <Image
                              src={currentSortIndex !== index ? IMG_OF_NORMAL : sortType ? IMG_OF_DESC : IMG_OF_ASC}
                              className='sort-image'
                            />
                          )}
                        </View>
                      );
                    })}
                </View>
              </View>
            )}

            {!!category && !!category[currentIndex].imgUrl && (
              <Image
                className={'banner-img ' + (showType === 1 ? 'max-banner' : '')}
                onClick={this.clickBannerImage}
                mode='aspectFill'
                src={category[currentIndex].imgUrl}
              />
            )}

            {!showType ? (
              <View className='goods-contaier'>
                {!!dataSource &&
                  dataSource.map((item, index) => {
                    return (
                      <Block key={item && item.wxItem ? item.wxItem.itemNo : index}>
                        <GoodsItem
                          display='vertical'
                          itemSize='small'
                          goodsItem={item}
                          showDivider={index !== dataSource.length - 1}
                          reportSource={reportSource}
                        />
                      </Block>
                    );
                  })}
                {!!(loadMoreStatus !== loadMoreStatusEnum.LOADING && !!dataSource && dataSource.length === 0) && (
                  <Empty style='width:auto;' message='暂无产品' />
                )}

                <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore} />
              </View>
            ) : (
              <View className='content-container'>
                {!!childCategorys &&
                  childCategorys.map((item, index) => {
                    return (
                      <View
                        key={index}
                        className={'child-item ' + ('column_' + columnNum)}
                        onClick={_fixme_with_dataset_(this.clickChildCategory.bind(this), { item: item })}
                      >
                        {item.categoryImgUrl ? (
                          <Image
                            className='icon'
                            src={imageUtils.thumbnailMid(item.categoryImgUrl)}
                            mode='aspectFill'
                          />
                        ) : (
                          <View
                            className='icon'
                            style={_safe_style_('background: rgba(240,241,245,1); display: inline-block;')}
                          />
                        )}

                        <View className='name'>{item.category}</View>
                      </View>
                    );
                  })}
              </View>
            )}
          </ScrollView>
        </View>

        {/* 购物车 */}
        {!!isQuickBuyOpen && (
          <View style={_safe_style_(`position: fixed;bottom:${tabbarHeight}px;left:0;width:100%; z-index:999;`)}>
            <BuyBottom ref={this.refBuyBottomCOMPT} />
          </View>
        )}
      </View>
    );
  }
}

export default Classify;
