import React from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../../utils/money.wxs.js';
import buyHub from '../../cart/buy-hub.js';
import cartHelper from '../../cart/cart-helper.js';
import wxApi from '../../../utils/wxApi';
import template from '../../../utils/template.js';
import protectedMailBox from '../../../utils/protectedMailBox.js';
import pageLinkEnum from '../../../constants/pageLinkEnum.js';
import CartDialog from "../cart-dialog";
import './index.scss';

import getStaticImgUrl from '../../../constants/frontEndImgUrl'

class BuyBottom extends React.Component {
  /**
   * 组件的初始数据
   */
  state = {
    price: 0,
    count: 0,
    tmpStyle: {},
  };

  loading = true;

  componentDidMount() {
    this.__onCartMapChange = this.onCartMapChange.bind(this);
    buyHub.hub.onMapChange(this.__onCartMapChange);
    this.init();
    this.getTemplateStyle();
  }

  componentWillUnmount() {
    buyHub.hub.offMapChange(this.__onCartMapChange);
  }

  // 弹起购物车
  handlerCart() {
    this.cartDialogCOMPT && this.cartDialogCOMPT.show();
  }

  handlerClear() {
    const ids = [];
    for (const key in buyHub.map) {
      ids.push(buyHub.map[key].id);
    }
    if (ids.length) {
      wxApi.showModal({
        title: '温馨提示：',
        content: '确定清除购物车吗？',
        cancelText: '取消',
        confirmText: '确定',
        success: (res) => {
          res.confirm && this.clearAllCart();
        },
      });
    }
  }

  clearAllCart() {
    const ids = [];
    for (const key in buyHub.map) {
      ids.push(buyHub.map[key].id);
    }
    if (ids.length > 0) {
      cartHelper.deleteGoods(ids, (res) => {
        this.count();
        this.cartDialogCOMPT && this.cartDialogCOMPT.hide();
      });
    }
  }

  // page.onShow时调用
  reload() {
    // console.log('bottom reload');
    this.cartDialogCOMPT && this.cartDialogCOMPT.hide();
    this.init();
  }

  // 去购买
  handlerPay() {
    if (this.loading) {
      return;
    }
    const skuList = [];
    let item;
    for (const key in buyHub.map) {
      item = buyHub.map[key];
      skuList.push({
        itemCount: item.count,
        itemNo: item.itemNo,
        barcode: item.barcode,
        name: item.name,
        skuId: item.skuId || null,
        salePrice: item.price,
        pic: item.thumbnail,
        skuTreeNames: item.skuInfo,
        freight: item.freight,
        noNeedPay: item.noNeedPay,
        reportExt: item.reportExt,
      });
    }
    if (!skuList.length) {
      wxApi.showToast({
        title: '未选中商品',
        icon: 'none',
      });
    } else {
      protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', skuList);
      wxApi.$navigateTo({
        url: pageLinkEnum.orderPkg.payOrder,
      });
    }
  }

  count() {
    let price = 0;
      let count = 0;
    for (const uuid in buyHub.map) {
      const data = buyHub.map[uuid];
      price += data.price * data.count; /* 商品价格乘以总数，才是总价格 */
      count += data.count;
    }
    this.setState({ price, count });
  }

  init() {
    cartHelper.getCarts(false, () => {
      this.count();
      this.loading = false;
    });
  }

  onCartMapChange() {
    let price = 0;
      let count = 0;
    for (const uuid in buyHub.map) {
      const data = buyHub.map[uuid];
      price += data.price * data.count; /* 商品价格乘以总数，才是总价格 */
      count += data.count;
    }
    this.setState({
      price: price.toFixed(2),
      count: count,
    });
  }

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  refCartDialogCOMPT = (node) => (this.cartDialogCOMPT = node);

  render() {
    const { count, tmpStyle, price } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-ccb-BuyBottom' className='wk-ccb-BuyBottom'>
        <View className='classify-cart'>
          <View
            className='left'
            onClick={this.handlerCart.bind(this)}
            style={_safe_style_('background:' + (count === 0 ? '' : tmpStyle.btnColor))}
          >
            {count == 0 ? (
              <Image src={getStaticImgUrl.plugin.emptyCart_png} className='cart'></Image>
            ) : (
              <Image src={getStaticImgUrl.plugin.cart_png} className='cart'></Image>
            )}

            {count > 0 && <View className='count'>{count}</View>}
          </View>
          <View className='right'>
            <View className='price'>
              <Text>￥</Text>
              {filters.moneyFilter(price, true)}
            </View>
            <View
              className='buy'
              onClick={this.handlerPay.bind(this)}
              style={_safe_style_('background:' + tmpStyle.btnColor)}
            >
              立即购买
            </View>
          </View>
        </View>
        <CartDialog ref={this.refCartDialogCOMPT} onClear={this.handlerClear.bind(this)}></CartDialog>
      </View>
    );
  }
}

export default BuyBottom;
