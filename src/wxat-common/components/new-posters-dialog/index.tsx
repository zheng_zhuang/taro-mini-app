import React, { FC, useEffect, useState, useImperativeHandle } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Button } from '@tarojs/components';
import Taro, { Ref } from '@tarojs/taro';
import filters from '../../utils/money.wxs.js';
import { useSelector } from 'react-redux';
import cdnResConfig from '../../constants/cdnResConfig.js';
import AuthUser from '../auth-user';
import './index.scss';

const commonImg = cdnResConfig.common;

const POSTER_TYPE = {
  goods: 'goods',
  redPacket: 'red-packet',
  internal: 'internal',
};

const POSTER_HEADER_TYPE = {
  avatar: 1,
  logo: 2,
};

// props的类型定义
type ComponentProps = {
  dataSource?: Record<string, any> | null; //商品信息，存在商品信息时，展示商品海报
  goods?: Record<string, any> | null;
  redPacket: Record<string, any> | null; //红包信息，存在红包信息时，展示红包海报
  qrCode: string;
  visible: boolean;
  backOpacity?: string; //背景透明度
  posterType: string;
  posterTips: string /** * 海报上面的提示语*/;
  posterLogo: string /*** 海报顶部logo，默认展示微信头像*/;
  posterHeaderType: number /*** 海报头部类型*/;
  posterImage: string /*** 海报主图 */;
  posterName: string /*** 海报图下面的名字*/;
  salePrice: string /** * 海报优惠价格，已经转化过成元的价格 */;
  labelPrice: string /*** 海报原价，已经转化过成元的价格 */;
  posterSalePriceLabel: string /** * 海报优惠价标签*/; //拼团价、最低价（砍价）、秒杀价
  posterLabelPriceLabel: string /*** 海报优惠价标签*/; //活动结束价
  onSave: (object) => any;
  /**
   * 是否需要展示海报原价
   */
  showLabelPrice: boolean;
  childRef: Ref<any>;
};

let PostersDialog: FC<ComponentProps> = (props) => {
  //提前解构props，减少getter的操作
  const {
    dataSource,
    goods,
    redPacket,
    qrCode,
    visible,
    backOpacity,
    posterType,
    posterTips,
    posterLogo,
    posterHeaderType,
    posterImage,
    posterName,
    salePrice,
    labelPrice,
    posterSalePriceLabel,
    posterLabelPriceLabel,
    onSave,
    childRef,
    showLabelPrice,
  } = props;

  const [user, setUser] = useState({});
  const [showVisiable, setShowVisiable] = useState(false);
  useImperativeHandle(childRef, () => ({
    hideDialog,
    showDialog,
  }));

  const wxUserInfo = useSelector((state) => state.base.wxUserInfo);
  useEffect(() => {
    setShowVisiable(!!visible);
  }, [visible]);

  const preventTouchMove = () => {
    //阻止滑动
  };

  const saveImage = () => {
    // triggerEvent('save', {
    //   user: user,
    //   goods: goods,
    //   redPacket: redPacket
    // });
    onSave &&
      onSave({
        user,
        goods,
        redPacket,
      });
  };
  const userInfoReady = () => {
    setUser({
      name: wxUserInfo ? wxUserInfo.nickName : '',
      headUrl: wxUserInfo ? wxUserInfo.avatarUrl : '',
    });
  };

  //隐藏弹框
  const hideDialog = () => {
    setShowVisiable(false);
  };
  //展示弹框
  const showDialog = () => {
    setShowVisiable(true);
  };

  /**
   * 隐藏dialog
   */
  const handleClose = () => {
    hideDialog();
  };
  return (
    <View
      data-fixme='02 block to view. need more test'
      data-scoped='wk-wcp-PostersDialog'
      className='wk-wcp-PostersDialog'
    >
      {!!showVisiable && (
        <View className='wx_dialog_container' onTouchMove={preventTouchMove}>
          <AuthUser onReady={userInfoReady}>
            <View className='posters-mask' style={_safe_style_('opacity:' + backOpacity + ';')}></View>
            {posterType === POSTER_TYPE.goods && (
              <View className='posters-dialog'>
                <View className='posters-box'>
                  {posterHeaderType === POSTER_HEADER_TYPE.avatar && (
                    <View className='posters-nick-header'>
                      <Image className='posters-head' src={user.headUrl}></Image>
                      <View className='posters-right'>
                        <View className='name'>{user.name}</View>
                        <View className='posters-notes'>{posterTips}</View>
                      </View>
                    </View>
                  )}

                  {posterHeaderType === POSTER_HEADER_TYPE.logo && (
                    <Image className='poster-custom-header' mode='aspectFit' src={posterLogo}></Image>
                  )}

                  {/* 商品图片 */}
                  <Image className='posters-img' src={posterImage}></Image>
                  {/* 商品名称 */}
                  <View className='goods-name limit-line line-2'>{posterName}</View>
                  {/* 活动类型、活动价格、原价 */}
                  <View className='posters-detail'>
                    <View>
                      <View className='activity-name'>{posterSalePriceLabel}</View>
                      <View className='goods-sprice'>{'￥' + salePrice}</View>
                      {showLabelPrice ? (
                        <View className='goods-orgprice'>{posterLabelPriceLabel + '￥' + labelPrice}</View>
                      ) : null}
                    </View>
                    <Image className='qr-code' src={qrCode} mode='aspectFit'></Image>
                  </View>
                </View>
                <Button className='btn-save' onClick={saveImage}>
                  保存海报
                </Button>
                <Image onClick={handleClose} className='posters-close' src={commonImg.close}></Image>
              </View>
            )}

            {!!(posterType === POSTER_TYPE.redPacket && redPacket) && (
              <View className='red-packet-posters-dialog'>
                <View className='red-packet-posters-box'>
                  <Image className='red-packet-bg' mode='aspectFit' src={cdnResConfig.redPacket.shareBg}></Image>
                  {posterHeaderType === POSTER_HEADER_TYPE.avatar && (
                    <View className='posters-nick-header'>
                      <Image className='posters-head' src={user.headUrl}></Image>
                      <View className='posters-right'>
                        <View className='name'>{user.name}</View>
                        <View className='posters-notes'>{posterTips}</View>
                      </View>
                    </View>
                  )}

                  {posterHeaderType === POSTER_HEADER_TYPE.logo && (
                    <Image className='poster-custom-header' mode='aspectFit' src={posterLogo}></Image>
                  )}

                  <View className='title limit-line'>{redPacket.name}</View>
                  <View className='money'>{filters.moneyFilter(redPacket.totalFee, true) + '元'}</View>
                  <View className='date date-label'>活动截止日期:</View>
                  <View className='date'>{redPacket.endTime}</View>
                  <Image src={qrCode} className='qr-code'></Image>
                </View>
                <Button className='btn-save' onClick={saveImage}>
                  保存海报
                </Button>
                <Image onClick={handleClose} className='posters-close' src={commonImg.close}></Image>
              </View>
            )}

            {/*  内购海报  */}
            {posterType === POSTER_TYPE.internal && (
              <View className='internal-posters-dialog'>
                <View className='internal-posters-box'>
                  <View className='head-box'>
                    <Image className='head' src={user.headUrl}></Image>
                  </View>
                  <View className='internal-title'>送你亲友内购券，品牌好货尽享低价</View>
                  <Image src={cdnResConfig.internalBuy.posterBg} className='internal-bg'></Image>
                  <Image src={qrCode} className='qr-code'></Image>
                  <View className='desc'>长按识别小程序码，立即领取</View>
                </View>
                <Button className='btn-save' onClick={saveImage}>
                  保存海报
                </Button>
                <Image onClick={handleClose} className='posters-close' src={commonImg.close}></Image>
              </View>
            )}
          </AuthUser>
        </View>
      )}
    </View>
  );
};

// 给props赋默认值
PostersDialog.defaultProps = {
  dataSource: null,
  goods: null,
  redPacket: null,
  qrCode: '',
  visible: false,
  backOpacity: '0.6',
  posterType: POSTER_TYPE.goods,
  posterTips: '向您推荐了这个商品' /** * 海报上面的提示语*/,
  posterLogo: '' /*** 海报顶部logo，默认展示微信头像*/,
  posterHeaderType: POSTER_HEADER_TYPE.avatar /*** 海报头部类型*/,
  posterImage: '' /*** 海报主图 */,
  posterName: '' /*** 海报图下面的名字*/,
  salePrice: '' /** * 海报优惠价格，已经转化过成元的价格 */,
  labelPrice: '' /*** 海报原价，已经转化过成元的价格 */,
  posterSalePriceLabel: '优惠价' /** * 海报优惠价标签*/, //拼团价、最低价（砍价）、秒杀价
  posterLabelPriceLabel: '原价' /*** 海报优惠价标签*/, //活动结束价
  showLabelPrice: true,
};

export default PostersDialog;
