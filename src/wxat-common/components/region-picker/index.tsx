import React from 'react';
import Taro from '@tarojs/taro';
import {Picker} from '@tarojs/components';
import regionList from './region'
interface IState{
  range:any[];
  returnValue:string[];
  value:number[]
}
interface IProps{
  className?:string;
  onChange:Function;
  options?:any;
}
class RegionPicker extends React.Component<IProps,IState>{
  static defaultProps={
    className:'',
  }
  state = {
    range:[],
    returnValue:[],
    value:[0,0,0]
  }
  componentDidMount(){
    let arr:any[] = [],cityArr:string[] = [],areaArr:string[]=[],province:string[] =[]
    if(Array.isArray(regionList)){
      province=regionList.map((item)=>{
        return item.name
      })
      cityArr = regionList[0].city.map((item)=>{
        return item.name
      })
      areaArr = regionList[0].city[0].area
      arr = [province,cityArr,areaArr]
      this.setState({
        range:arr
      })
    }
  }
  onChange = (e) =>{
    const range = this.state.range
    const value:number[] = e.detail.value
    this.setState({
      returnValue:[range[0][value[0]],range[1][value[1]],range[2][value[2]]],
      value:value
    },()=>{
      this.props.onChange(this.state.returnValue,this.props.options)
    })
  }
  onColumnChange=(e)=>{
    const {column,value} = e.detail
    const {range} = this.state
    let pIndex=0,cIndex=0,aIndex=0
    let cityArr:string[] = [],areaArr:string[]=[];
    if(column === 0){
      pIndex = value
      cIndex = 0
      aIndex = 0
      cityArr = regionList[value].city.map((item)=>{
        return item.name
      })
      areaArr = regionList[value].city[cIndex].area
      this.setState({
        range:[range[0],cityArr,areaArr],
        value:[pIndex,cIndex,aIndex]
      })
    }else if(column === 1){
      pIndex = this.state.value[0]
      cIndex = value
      aIndex = 0
      areaArr = regionList[pIndex].city[cIndex].area
      console.log(areaArr)
      this.setState({
        range:[range[0],range[1],areaArr],
        value:[pIndex,cIndex,aIndex]
      })
    }else{
      pIndex = this.state.value[0]
      cIndex = this.state.value[1]
      this.setState({
        value:[pIndex,cIndex,value]
      })
    }
  }
  render(){
    const {range,value} = this.state
    return(
      <Picker onColumnChange={this.onColumnChange} className={this.props.className} mode="multiSelector" range={range} value={value} onChange={this.onChange}>
        {this.props.children}
      </Picker>
    )
  }
}
export default RegionPicker
