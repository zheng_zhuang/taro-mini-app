import React from 'react';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import withWeapp from '@tarojs/with-weapp';
import filters from '../../utils/money.wxs.js';
// components/goods-module/index.js
import pay from '../../utils/pay.js';
import wxApi from '../../utils/wxApi';
import getStaticImgUrl from '../../constants/frontEndImgUrl'
import './index.scss';

export default class CardModule extends React.Component {
  static defaultProps = {
    dataSource: null,
    isMine: false,
  };

  constructor(props) {
    super(props);
  }

  /**
   * 组件的初始数据
   */
  state = {
    bgImages: [],
    dataList: [],
  };

  numberCardType = 3; // 3 次数卡 4充值卡

  componentDidMount() {
    const images = [];
    (this.props.dataSource || []).forEach((item) => {
      if (item.styleUrl) {
        images.push(item.styleUrl);
      } else {
        if (item.type === this.numberCardType) {
          images.push(getStaticImgUrl.goods.cardMinCount_png);
        } else {
          images.push(getStaticImgUrl.goods.rechargeCard_png);
        }
      }
    });
    this.setState({
      bgImages: images,
      dataList: this.props.dataSource,
    });
  } /* 请尽快迁移为 componentDidUpdate */

  UNSAFE_componentWillReceiveProps(nextProps) {
    const dataList = nextProps.dataSource || [];
    this.setState({
      dataList,
    });
  }

  onClickGoodsItem(e) {
    const item = e.currentTarget.dataset.goods;
    if (this.props.isMine) {
      wxApi.$navigateTo({
        url: '/sub-packages/server-package/pages/my-card-detail/index',
        data: { item: JSON.stringify(item) },
      });
    } else {
      wxApi.$navigateTo({
        url: '/sub-packages/server-package/pages/card-detail/index',
        data: { itemNo: item.itemNo },
      });
    }
  }

  render() {
    const { dataList, bgImages } = this.state;
    return (
      <View className='card-container'>
        {!!dataList &&
          dataList.map((item, index) => {
            return (
              <View className='card-item-container' key='itemNo' data-goods={item} onClick={this.onClickGoodsItem}>
                <View className='mine-card-item'>
                  {!!bgImages[index] && (
                    <Image className='mine-card-bg' mode='aspectFill' src={bgImages[index]}></Image>
                  )}

                  <View className='container'>
                    <View className='name'>{item.name}</View>
                    {item.giftAmount && (
                      <View className='discount'>{'送' + filters.moneyFilter(item.giftAmount, true)}</View>
                    )}

                    <View className='footer'>
                      <View className='validity'>{'￥' + filters.moneyFilter(item.salePrice, true)}</View>
                      <View className='validity'>{item.typeDesc}</View>
                    </View>
                  </View>
                </View>
                <View className='content-sub-container'>
                  <View className='card-name'>{item.name}</View>
                  <View className='card-desc'>
                    {item.type == 4 && (
                      <Block>
                        {'充 ' +
                          filters.moneyFilter(item.salePrice, true) +
                          '送' +
                          filters.moneyFilter(item.giftAmount, true)}
                      </Block>
                    )}

                    {item.itemCardServerList.map((serve, serveIndex) => {
                      return (
                        <Block key='unique'>
                          {serveIndex > 0 && <Text decode>,&nbsp;</Text>}
                          {serve.serverItemName + 'x' + serve.serverCount}
                        </Block>
                      );
                    })}
                  </View>
                  {item.type == 3 ? (
                    <View className='card-time'>
                      {'有效期：' + (item.validityType === 0 ? '永久' : item.validity + '天')}
                    </View>
                  ) : (
                    <View className='card-time'></View>
                  )}

                  <View className='card-price'>
                    <Text className='unit'>￥</Text>
                    {filters.moneyFilter(item.salePrice, true)}
                  </View>
                </View>
              </View>
            );
          })}
      </View>
    );
  }
}
