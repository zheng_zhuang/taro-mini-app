import React from 'react';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { ScrollView, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import utilDate from '../../utils/date.js';
import wxApi from '../../utils/wxApi';
import constants from "../../constants";
import template from '../../utils/template.js';
import commentConfig from '../../constants/commentConfig.js';
import protectedMailBox from '../../utils/protectedMailBox.js';
import authHelper from '../../utils/auth-helper.js';

import LoadMore from '../load-more/load-more';
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

import getStaticImgUrl from '../../constants/frontEndImgUrl'

const graphImg = cdnResConfig.graph;
const GRADE = constants.comment.grade;
const REPLY = constants.comment.sellerReplyStatus;
const loadMoreStatusEnum = constants.order.loadMoreStatus;
const DEFAULT_AVATAR = getStaticImgUrl.wxatCommon.defaultImg_png;


const commentTye = {
  goods: 'GOODS',
  article: 'ARTICLE',
};


class CommentModule extends React.Component {
  /**
   * 组件的属性列表
   */
  static defaultProps = {
    dataSource: null,
    /* 评论的主体类型，商品还是文章 //商品：GOOD 文章：ARTICLE */
    type: 'GOODS',
    // id
    itemNo: '',
    order: 'like',
    /* 评论的标题，点击跳转到评论列表时，可设置评论列表页面的导航标题 */
    commentTitle: '',
    /* 评论的header tab名称 */
    headerLabel: '评论',
    /* 是否显示高亮标题headline */
    showHeadline: false,
    /* 是否显示普通header */
    showHeader: false,
    /* 距离顶部的距离 */
    topOffset: 0,
    /* 是否为缩略展示，如果只展示几个简单的评论，则设置为true，并且会展示一个按钮跳转到更多评论页面 */
    isBrief: false,
    /* 缩略展示时，展示几个评论 */
    briefCount: 2,
  };

  /**
   * 组件的初始数据
   */
  state = {
    canLike: commentConfig.GOODS.canLike,
    canReplay: commentConfig.GOODS.canReplay,
    noGrade: commentConfig.GOODS.noGrade,
    canDelete: !!commentConfig.GOODS.deleteComment,
    gradelist: [
      {
        name: '全部',
        value: null,
      },

      {
        name: '好评',
        value: 1,
      },

      {
        name: '中评',
        value: 2,
      },

      {
        name: '差评',
        value: 3,
      },
    ],
    pageNo: 1,
    pageSize: 10,
    tagType: null,
    commentCount: null,
    commentList: [],
    totalCommentCount: 0,
    loadMoreStatus: loadMoreStatusEnum.LOADING,
    hasMore: true,
    loading: false, // 防止初始化和上拉同时调接口的并发问题
  };

  UNSAFE_componentWillMount() {
    this.init(false);
    this.getTemplateStyle();
  }

  handlerScroll = () => {
    const {hasMore, pageNo} = this.state
    if (hasMore) {
      this.setState(
        {
          pageNo: pageNo + 1
        },
        () => {
          this.getCommentList()
        }
      )
    }
  }


  // refresh: 是否刷新评论数
  init(refresh = false) {
    const { type, itemNo, isBrief } = this.props;
    this.setState({
      canLike: commentConfig[type].canLike,
      canReplay: commentConfig[type].canReplay,
      noGrade: commentConfig[type].noGrade,
      canDelete: !!commentConfig[type].deleteComment,
    });

    if (itemNo) {
      this.getCommentList();
      if (type === commentTye.goods && !isBrief) {
        this.getCommentCount();
      }
    }
  }

  onRetryLoadMore = () => {
    this.getCommentList();
  };

  hanldTab = (e) => {
    const tabType = e.currentTarget.dataset.grade;
    this.setState({
      commentList: [],
      tagType: tabType,
      pageNo: 1
    }, () => {
      this.getCommentList();
    });
  };

  handleShowLargeImage = (img, images) => () => {
    wxApi.previewImage({
      current: img, // 当前显示图片的http链接
      urls: images, // 需要预览的图片http链接列表
    });
  };

  getTime(commentList) {
    if (!commentList) {
      return commentList;
    }
    commentList.forEach((item) => {
      if (item.createTime) {
        let createTime = '';
        if (typeof item.createTime === 'string') {
          createTime = new Date(item.createTime.replace(/-/g, '/'));
        } else {
          createTime = new Date(item.createTime);
        }
        const today = new Date().toLocaleDateString();
        const start = new Date(today).getTime();
        const current = new Date(createTime).getTime();
        if (current > start) {
          item.createTime = utilDate.format(createTime, 'hh:mm:ss');
        } else {
          item.createTime = utilDate.format(createTime, 'yyyy-MM-dd hh:mm:ss');
        }
      }
    });
    return commentList;
  }

  // 获取评论数量
  getCommentCount() {
    const { itemNo, type } = this.props;
    const data = {
      itemNo: itemNo,
    };

    commentConfig[type]
      .getCommentCount(data)
      .then((res) => {
        this.setState(
          {
            commentCount: res.data,
          },

          () => {
            this.getAllCount();
          }
        );
      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });
  }

  // 获取总评论数量
  getAllCount() {
    const { commentCount } = this.state;
    if (commentCount) {
      const all = commentCount.positiveCount + commentCount.moderateCount + commentCount.negativeCount;
      commentCount.all = all;
      this.setState({
        commentCount,
      });
    }
  }

  // 获取评论列表
  getCommentList() {
    const { type, isBrief, briefCount, itemNo, order } = this.props;
    const { tagType, commentList, pageNo, pageSize } = this.state;

    const data = {
      pageNo: pageNo,
      pageSize: isBrief ? briefCount : pageSize,
      itemNo: itemNo,
      grade: tagType || '',
      orderBy: order === 'like' ? 0 : 1,
    }

    this.setState(
      {
        hasMore: true,
        loadMoreStatus: loadMoreStatusEnum.LOADING
      }
    )

    return commentConfig[type]
      .getCommentList(data)
      .then((res) => {
        res.data = this.getTime(res.data);

        let cpCommentList = commentList.concat(res.data || [])

        if (cpCommentList.length === res.totalCount && res.totalCount) {
          this.setState({
            loadMoreStatus: loadMoreStatusEnum.BASELINE
          })
        } else {
          this.setState({
            loadMoreStatus: loadMoreStatusEnum.HIDE
          })
        }

        this.setState({
          commentList: cpCommentList,
          totalCommentCount: res.totalCount || 0,
          hasMore: cpCommentList.length < res.totalCount,
        })
      })
      .catch(() => {
        this.setState({
          error: true,
          loadMoreStatus: loadMoreStatusEnum.HIDE
        })
      })
  }

  onDeleteComment = (e) => {
    const comment = e.currentTarget.dataset.item;
    const data = {
      commentId: +comment.id,
    };

    const handle = commentConfig[this.state.type].deleteComment;
    if (handle) {
      handle(data).then((res) => {
        // this.triggerEvent('deleteComment')
        this.props.onDeleteComment();
        this.setState({
          commentList: this.state.commentList.filter((item) => +item.id !== data.commentId),
        });
      });
    }
  };

  // 评论点赞操作
  onChangeLike = (e) => {
    if (!authHelper.checkAuth()) return false;
    const comment = e.currentTarget.dataset.item;
    const data = {
      generalId: +comment.id,
      status: +!comment.liked,
    };

    commentConfig[this.properties.type].changeLike(data).then((res) => {
      // 需要刷新已显示的评论（更改 pages），更新后还原 pages
      const { pageNo, pageSize, commentList } = this.state;
      // 需要刷新已显示的评论
      this.setState({
        pageNo: 1,
        pageSize: commentList.length,
      });

      this.getCommentList().then(() => {
        // 还原 pages
        this.setState({
          pageNo,
          pageSize,
        });
      });
    });
  };

  handleGotoAllComment = () => {
    const { itemNo, commentTitle } = this.props;
    const commentData = {
      commentId: itemNo,
      commentTitle: commentTitle,
    };

    protectedMailBox.send('sub-packages/moveFile-package/pages/comment-list/index', 'comment-data', commentData);
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/comment-list/index',
    });
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { topOffset, headerLabel, showHeadline, showHeader, isBrief } = this.props;
    const {
      totalCommentCount,
      tmpStyle,
      commentCount,
      noGrade,
      gradelist,
      canDelete,
      canLike,
      hasMore,
      commentList,
      tagType,
      loadMoreStatus,
    } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcc-CommentModule'
        className={['wk-wcc-CommentModule', !isBrief ? 'isBriefClass' : '']}
      >
        <ScrollView scrollY={true} scrollWithAnimation scrollAnchoring style={{'height': '100%', 'overflowAnchor': 'auto', 'WebkitOverflowScrolling': 'touch'}} onScrollToLower={this.handlerScroll}>
        <View className='comment-module' style={_safe_style_('margin-top: ' + (topOffset / 2) + 'px;')}>
          {showHeadline && !showHeader ? (
            <View className='comment-headline'>
              <Text>{headerLabel + '(' + totalCommentCount + ')'}</Text>
            </View>
          ) : (
            showHeader && (
              <View className='comment-header'>
                <Text className='header-label' style={_safe_style_('background: ' + tmpStyle.btnColor)}></Text>
                <Text>{headerLabel + '(' + totalCommentCount + ')'}</Text>
              </View>
            )
          )}

          {!!(!noGrade && !isBrief) && (
            <View className='comment-title'>
              {!!commentCount &&
                gradelist.map((item, index) => {
                  return (
                    <View
                      className='comment-label'
                      key={index}
                      onClick={_fixme_with_dataset_(this.hanldTab, { grade: item.value })}
                    >
                      <View
                        className='comment-btn'
                        style={_safe_style_(
                          'color: ' +
                            (tagType === item.value ? tmpStyle.btnColor : '#8893A6') +
                            ';border-bottom: ' +
                            (tagType === item.value ? '3.5px solid ' + tmpStyle.btnColor : 'none') +
                            ';font-weight:' +
                            (tagType === item.value ? 'bold' : 400)
                        )}
                      >
                        {item.value === GRADE.All && (
                          <View>{item.name + '(' + (commentCount.all > 999 ? '999+' : commentCount.all) + ')'}</View>
                        )}

                        {item.value === GRADE.PRAISE && (
                          <View>
                            {item.name +
                              '(' +
                              (commentCount.positiveCount > 999 ? '999+' : commentCount.positiveCount) +
                              ')'}
                          </View>
                        )}

                        {item.value === GRADE.MIDDLE && (
                          <View>
                            {item.name +
                              '(' +
                              (commentCount.moderateCount > 999 ? '999+' : commentCount.moderateCount) +
                              ')'}
                          </View>
                        )}

                        {item.value === GRADE.BAD && (
                          <View>
                            {item.name +
                              '(' +
                              (commentCount.negativeCount > 999 ? '999+' : commentCount.negativeCount) +
                              ')'}
                          </View>
                        )}
                      </View>
                    </View>
                  );
                })}
            </View>
          )}

          {!!commentList.length && (
            <View className='comment-list'>
              {commentList.map((item, index) => {
                return (
                  <View className='comment-item' key={index}>
                    <View className='user-img'>
                      <Image
                        className='user-img-image'
                        src={item.avatarImgUrl ? item.avatarImgUrl : DEFAULT_AVATAR}
                      ></Image>
                    </View>
                    <View className='comment-detail'>
                      <View className='user-name'>{item.userName}</View>
                      {!noGrade && (
                        <View className='comment-grade'>
                          <Text>评价：</Text>
                          {item.grade === GRADE.PRAISE && (
                            <Image
                              className='comment-grade-image'
                              src={getStaticImgUrl.comment.PRAISE_png}
                            ></Image>
                          )}

                          {item.grade === GRADE.MIDDLE && (
                            <Image
                              className='comment-grade-image'
                              src={getStaticImgUrl.comment.MIDDLE_png}
                            ></Image>
                          )}

                          {item.grade === GRADE.BAD && (
                            <Image
                              className='comment-grade-image'
                              src={getStaticImgUrl.comment.BAD_png}
                            ></Image>
                          )}
                        </View>
                      )}

                      <View className='comment-info'>
                        {!!item.content && <View className='comment-content'>{item.content}</View>}
                        {!!(item.images && item.images.length !== 0) && (
                          <View className='comment-imgBox'>
                            {item.images.map((img, imgIndex) => {
                              return (
                                <Image
                                  className='comment-img'
                                  key={imgIndex}
                                  src={img}
                                  onClick={this.handleShowLargeImage(img, item.images)}
                                  mode='aspectFill'
                                ></Image>
                              );
                            })}
                          </View>
                        )}

                        {item.sellerReplyStatus === REPLY.REPLIED && (
                          <View className='comment-reply'>{'商家回复：' + item.sellerReplyContent}</View>
                        )}
                      </View>
                      <View className='option-info'>
                        <View className='comment-time'>
                          <Text>{item.createTime}</Text>
                          {!!(canDelete && item.canDelete) && (
                            <View
                              className='comment-delete'
                              onTap={_fixme_with_dataset_(this.onDeleteComment, { item: item })}
                            >
                              删除
                            </View>
                          )}
                        </View>
                        {!!canLike && (
                          <View className='option-like' onTap={_fixme_with_dataset_(this.onChangeLike, { item: item })}>
                            {item.liked ? (
                              <Image className='like-logo' src={graphImg.activeLike}></Image>
                            ) : (
                              <Image className='like-logo' src={graphImg.like}></Image>
                            )}

                            <Text className='like-count'>{item.likeCount || 0}</Text>
                          </View>
                        )}
                      </View>
                    </View>
                  </View>
                );
              })}
              {!!(isBrief && hasMore) && (
                <View className='all-comment-btn-box'>
                  <View className='all-comment-btn' onClick={this.handleGotoAllComment}>
                    全部评论
                  </View>
                </View>
              )}
            </View>
          )}
          {
            !loadMoreStatus && !commentList.length && <View className='comment-empty'>暂无评论</View>
          }
        </View>
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
        </ScrollView>
      </View>
    );
  }
}

export default CommentModule;
