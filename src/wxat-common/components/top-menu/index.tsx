import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, ScrollView, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import filters from '../../utils/money.wxs.js';
import './index.scss';

type IProps = {
  dataSource?: Record<string, any>;
  list: Array<Record<string, any>>;
  color?: string;
  onTabClick: (partId: any) => any;
};

let TopMenu: FC<IProps> = ({ list, color, onTabClick }) => {
  const [tabType, setTabType] = useState<string | number>('');

  useEffect(() => {
    const partId = Array.isArray(list) && list.length > 0 ? list[0].partId : 0;
    if (!!partId && tabType !== partId) {
      setTabType(partId);
      getData(partId);
    }
  }, [list]);

  const hanldTab = (type) => {
    if (type !== tabType) {
      setTabType(type);
      getData(type);
    }
  };

  const getData = (type = '') => {
    onTabClick && onTabClick(type || tabType);
  };

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-wct-TopMenu' className='wk-wct-TopMenu'>
      {!!(Array.isArray(list) && list.length > 0) && (
        <View className='menu'>
          <ScrollView scrollX className='menu-list' style={_safe_style_('background:' + color)}>
            {list.map((item, index) => {
              return (
                <View className='menu-item' key={item.id} onClick={() => hanldTab(item.partId)}>
                  <Text
                    className={tabType === item.partId ? 'chcked text' : 'text'}
                    style={_safe_style_('color:' + (tabType === item.partId ? color : ''))}
                  >
                    {item.partName}
                  </Text>
                </View>
              );
            })}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

TopMenu.defaultProps = {
  color: '#1bc3b8',
};

export default TopMenu;
