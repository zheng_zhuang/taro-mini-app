import { _safe_style_ } from '@/wxat-common/utils/platform';
import React, { ComponentClass } from 'react';
import { View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../utils/money.wxs';
import template from '../../utils/template';
import './index.scss';

type PageOwnProps = {
  frontMoney: number;
  restMoney: number;
  state?: boolean;
};

type PageState = {};

interface FrontMoneyState {
  props: PageOwnProps;
}

class FrontMoneyState extends React.Component {
  static defaultProps = {
    frontMoney: 0,
    restMoney: 0,
    state: false,
  };

  state = {
    tmpStyle: {},
  };

  componentDidMount() {
    this.getTemplateStyle();
  }

  //获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { frontMoney, state, restMoney } = this.props;
    const { tmpStyle } = this.state;
    return (
      <View data-scoped='wk-wcf-FrontMoneyState' className='wk-wcf-FrontMoneyState front-money-box'>
        <View className='front-money-title' style={_safe_style_('color:' + tmpStyle.btnColor1)}>
          (如有券后价，所含优惠将在尾款使用)
        </View>
        <View className='front-money-tab done'>
          <View className='front-money-tab-left'>
            <Text className='front-money-logo' style={_safe_style_('background-color:' + tmpStyle.btnColor)}></Text>
            <Text className='front-money-name' style={_safe_style_('color:' + tmpStyle.btnColor)}>
              商品定金
            </Text>
          </View>
          <View className='front-money-tab-right' style={_safe_style_('color:' + tmpStyle.btnColor)}>
            {'¥' + filters.moneyFilter(frontMoney, true)}
          </View>
        </View>
        {/*  进度条  */}
        <View className='front-money-tab'>
          <View className='progress-bar-box'>
            <View className='progress-bar1' style={_safe_style_('border-color:' + tmpStyle.btnColor)}></View>
            <View className='progress-bar2' style={_safe_style_('border-color:' + tmpStyle.bgColor)}></View>
          </View>
        </View>
        <View className='front-money-tab'>
          <View className='front-money-tab-left'>
            <Text
              className='front-money-logo'
              style={_safe_style_('background-color: ' + (state ? tmpStyle.btnColor : 'rgba(139, 151, 169, 0.2)'))}
            ></Text>
            <Text
              className='front-money-name'
              style={_safe_style_('color: ' + (state ? tmpStyle.btnColor : 'rgba(139, 151, 169, 1)'))}
            >
              待支付尾款
            </Text>
          </View>
          <View className='front-money-tab-right'>{'¥' + filters.moneyFilter(restMoney, true)}</View>
        </View>
      </View>
    );
  }
}

export default FrontMoneyState as ComponentClass<PageOwnProps, PageState>;
