import React from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Input, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import hoc from '@/hoc/index';
import { gainSelectedSkuItemInfo } from '../../utils/sku';
import filters from '../../utils/money.wxs';
import constants from "../../constants";
import goodsTypeEnum from '../../constants/goodsTypeEnum';
import './index.scss';
import { connect } from 'react-redux';
import protectedMailBox from '@/wxat-common/utils/protectedMailBox.js';
import pageLinkEnum from '@/wxat-common/constants/pageLinkEnum.js';
import jsonp from '@/wxat-common/utils/wxApi/jsonp';

const mapStateToProps = (state) => {
  return { scoreName:state.globalData.scoreName || '积分'  };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })

@hoc
class GoodsAttribute extends React.Component {
  static defaultProps = {
    goodsType: constants.goods.type.product,
    goodsDetail: null,
    limitAmountTitle: null,
    // 默认展示库存信息(虚拟商品，如代金卡包不展示)
    showStock: true,
    // 选择确认回调
    onChoose: null,
  };

  /**
   * 组件的初始数据
   */
  state = {
    thumbnail: null,
    skuIntegral: null, // 商品积分
    innerBuyCouponNum: null,
    selectSkuValIds: [],
    skuSalePrice: null,
    sku: null,
    skuId: null,
    skuTreeNames: '',
    buyNumber: 1,
    buyNumMin: 1,
    buyNumMax: 0,
    enableBuy: false,
    btnText: '确定',
    allLimitAmountText: null, // 总限购文案
    onceLimitAmountText: null, // 单次购买数量文案
    ignoreStock: false,
    isshowSku: false
  };

  componentDidMount() {
    this.initSetData(this.props.goodsDetail); // 初始化数据
    this.handleSelectTreeVal() // 去除已选择的sku
  }

  /**
   * 初始化数据
   */
  initSetData() {
    const { goodsDetail, limitAmountTitle } = this.props;
    if (!(goodsDetail.skuTreeList && goodsDetail.skuTreeList.length)) {
      let skuSalePrice = goodsDetail.wxItem.salePrice;
      const skuIntegral = goodsDetail.wxItem.exchangeIntegral;
      const innerBuyCouponNum = goodsDetail.wxItem.innerBuyCouponNum;
      // 判断是否正在进行中的秒杀活动商品，是则将商品的秒杀价格设置为属性显示及购买价格
      const seckillActivityStatus =
        goodsDetail.seckillItemDTO && goodsDetail.seckillItemDTO.status ? goodsDetail.seckillItemDTO.status : null;
      if (
        seckillActivityStatus &&
        seckillActivityStatus === 1 &&
        this.props.goodsType === constants.goods.type.seckill
      ) {
        skuSalePrice = goodsDetail.wxItem.seckillPrice;
      }

      let buyNumMax = goodsDetail.wxItem.itemStock;
      // 如果是积分商品库存等于可兑换数量
      if (goodsDetail.itemIntegralShopDTO) {
        buyNumMax = goodsDetail.wxItem.exchangeNum;
      }
      // fixme 保留库存不足的逻辑
      this.setState({
        skuSalePrice,
        skuIntegral,
        innerBuyCouponNum,
        buyNumMax,
        enableBuy: true,
      });
    }

    if (
      goodsDetail.wxItem.type === goodsTypeEnum.SERVER.value ||
      goodsDetail.wxItem.type === goodsTypeEnum.SLEEVE_SYSTEM.value
    ) {
      this.setState({
        buyNumMax: Number.MAX_VALUE,
        ignoreStock: true,
      });
    }

    if (limitAmountTitle) {
      let allLimitAmountText = null; // 总限购文案
      let onceLimitAmountText = null; // 单次购买数量文案

      const seckillItemDTO = goodsDetail.seckillItemDTO;
      const ptActivityItemDTO = goodsDetail.ptActivityItemDTO;
      const itemIntegralShopDTO = goodsDetail.itemIntegralShopDTO;

      if (limitAmountTitle === '秒杀' && seckillItemDTO) {
        allLimitAmountText = seckillItemDTO.peopleLimitAmount
          ? '秒杀每人限购' + seckillItemDTO.peopleLimitAmount + '件'
          : null;
      } else if (limitAmountTitle === '拼团' && ptActivityItemDTO) {
        allLimitAmountText = ptActivityItemDTO.limitTimes
          ? '拼团每人可发起' + ptActivityItemDTO.limitTimes + '次'
          : null;
        onceLimitAmountText = ptActivityItemDTO.limitItemCount
          ? '单次可购买' + ptActivityItemDTO.limitItemCount + '件'
          : null;
      } else if (limitAmountTitle === '积分商品' && itemIntegralShopDTO) {
        allLimitAmountText = itemIntegralShopDTO.exchangeRestrict
          ? `${this.props.scoreName}商品每人限制兑换` + itemIntegralShopDTO.exchangeRestrict + '件'
          : null;
      }

      this.setState({
        allLimitAmountText: allLimitAmountText,
        onceLimitAmountText: onceLimitAmountText,
      });
    }
    this.initSelectedSkuIds(); // 初始化选中的sku类型
  }

  // 关闭弹窗
  closePopupTap() {
    this.triggerEvent('close-popup');
  }

  // 商品数量减1
  handleDecreaseBuyNum = () => {
    const { buyNumber, buyNumMin } = this.state;
    if (buyNumber > buyNumMin) {
      let currentNum = buyNumber;
      currentNum--;
      this.setState({
        buyNumber: currentNum,
      });
    }
  };

  // 商品数量加1
  handleIncreaseBuyNum = () => {
    // 编辑的商品详情
    const { goodsDetail } = this.props;
    const { buyNumber, buyNumMax, skuId } = this.state;

    if (!buyNumMax || (buyNumMax && buyNumber >= buyNumMax)) {
      wxApi.showToast({
        title: '库存不足',
        icon: 'none',
      });

      return;
    }

    // 参与秒杀商品剩余数量集合
    const activityRemain =
      goodsDetail && goodsDetail.seckillItemDTO && goodsDetail.seckillItemDTO.activityRemain
        ? goodsDetail.seckillItemDTO.activityRemain
        : [];
    // 商品没有sku时，只判断活动剩余数量数组中的第一个
    if (activityRemain.length === 1 && buyNumber >= activityRemain[0].totalRemainCount) {
      wxApi.showToast({
        title: '活动商品剩余数量不足',
        icon: 'none',
      });

      return;
    } else if (activityRemain.length > 1 && skuId) {
      // 商品有sku时，判断选中的sku商品剩余数量
      const selectedItem = activityRemain.find((item) => {
        return item.skuId === skuId;
      });
      if (buyNumber >= selectedItem.totalRemainCount) {
        wxApi.showToast({
          title: '活动商品剩余数量不足',
          icon: 'none',
        });

        return;
      }
    }

    // 商品没有sku时，判断wxItem中的限制购买数量
    if (!goodsDetail.skuInfoList || goodsDetail.skuInfoList.length === 0) {
      // 判断是否为内购商品
      const innerBuyCount =
        goodsDetail.wxItem.innerBuyCount || goodsDetail.wxItem.innerBuyCount === 0
          ? goodsDetail.wxItem.innerBuyCount
          : null;
      if (innerBuyCount && innerBuyCount > 0 && buyNumber >= innerBuyCount) {
        wxApi.showToast({
          title: '购买数量不能大于内购数量上限' + innerBuyCount + '件',
          icon: 'none',
        });

        return;
      }
    } else if (skuId) {
      // 商品有sku时，判断选中的sku中的限制购买数量
      const selectedItem = goodsDetail.skuInfoList.find((item) => {
        return item.skuId === skuId;
      });
      // 判断是否为内购商品
      if (selectedItem.innerBuyCount && selectedItem.innerBuyCount > 0 && buyNumber >= selectedItem.innerBuyCount) {
        wxApi.showToast({
          title: '购买数量不能大于内购数量上限' + selectedItem.innerBuyCount + '件',
          icon: 'none',
        });

        return;
      }
    }

    let currentNum = buyNumber;
    currentNum++;
    this.setState({
      buyNumber: currentNum,
    });
  };

  /**
   * 初始化选中的sku类型
   */
  initSelectedSkuIds() {
    const { skuInfoList, skuTreeList } = this.props.goodsDetail;
    const selectSkuValIds = this.state.selectSkuValIds;
    if (skuTreeList && skuTreeList.length && skuInfoList && skuInfoList.length) {
      let validSku = skuInfoList.find((skuInfo) => {
        return skuInfo.stock && skuInfo.stock > 0;
      });
      if (!validSku) {
        validSku = skuInfoList[0];
      }
      if (validSku) {
        skuTreeList.forEach((skuTree, skuTreeIndex) => {
          const skuTreeId = skuTree.keyId;
          const curTreeVal = validSku.skuInfoNames.find((skuInfoName) => {
            return skuInfoName.keyId === skuTreeId;
          });
          const selectedTreeVal = skuTree.treeValList.find((treeVal) => {
            return treeVal.valId === curTreeVal.valId;
          });
          //注释下面一行代码，因为这样写会存在sku同时高亮
          // selectedTreeVal.active = true; // 给当前选择的sku属性添加高亮属性
          if (selectedTreeVal.peopleCount) {
            this._minPeople = selectedTreeVal.peopleCount;
          }
          // 获取sku下选中的规格的id
          selectSkuValIds[skuTreeIndex] = {
            treeValId: curTreeVal.valId,
            skuTreeId: curTreeVal.keyId,
          };
        });
        this.setState(
          {
            selectSkuValIds,
          },

          () => {
            this.computeData();
          }
        );
      }
    }
  }

  /**
   * 选则某个sku属性时触发
   * @param {*} e
   */
  handleSelectTreeVal = (e) => {

    const { goodsDetail } = this.props;
    const skuTreeList = goodsDetail.skuTreeList;

    if (typeof e == 'undefined') {
      // !!skuTreeList && skuTreeList.length > 0 && skuTreeList['0'].treeValList.forEach((item, index) => {
      //   if (index === 0) {
      //     item.active = true
      //     return;
      //   }
      //   item.active = false
      // })
      // return
      !!skuTreeList && skuTreeList.length > 0 && skuTreeList.forEach(item => {
        item.treeValList.forEach((item, index) => {
          if (index === 0) {
            item.active = true
            return;
          }
          item.active = false
        })
      })
      return
    };
    const { selectSkuValIds } = this.state;
    const { skuTreeIndex, treeValIndex, treeValId, skuTreeId } = e.currentTarget.dataset;
    // 取消该分类下的子栏目选中状态,同时选中当前点击的子栏目
    skuTreeList[skuTreeIndex].treeValList.forEach((item, index) => {
      item.active = treeValIndex === index;
      // 判断是否有拼团属性，有则记录拼团人数
      if (item.active && item.peopleCount) {
        this._minPeople = item.peopleCount;
      }
    });

    // 获取sku下选中的规格的id
    selectSkuValIds[skuTreeIndex] = {
      treeValId,
      skuTreeId,
    };



    this.computeData();
  };

  /**
   * 计算商品库存及价格
   */
  computeData() {
    const { goodsDetail, goodsType } = this.props;
    const { selectSkuValIds } = this.state;
    const skuTreeList = goodsDetail.skuTreeList;
    const skuInfoList = goodsDetail.skuInfoList;

    // 获取所有的选中规格尺寸数据
    const needSelectNum = skuTreeList.length;

    let curSelectNum = 0;
    let skuTreeIds = '';
    let skuTreeNames = '';
    let buyNumMax = 0;
    let salePrice = null;
    let skuIntegral = null;
    let innerBuyCouponNum = null;
    let enableBuy = false;
    let btnText = '确定';
    let skuId = 0;
    let thumbnail = goodsDetail.wxItem.thumbnail;

    for (let i = 0; i < skuTreeList.length; i++) {
      const childs = skuTreeList[i].treeValList;
      for (let j = 0; j < childs.length; j++) {
        if (childs[j].active) {
          curSelectNum++;
          skuTreeIds = skuTreeIds + skuTreeList[i].keyId + ':' + childs[j].valId + ',';
          skuTreeNames = skuTreeNames + skuTreeList[i].keyName + ':' + childs[j].valName + '  ';
          break;
        }
      }
    }

    let selectSkuInfo = null;

    // 选择规格和必选规格相等，可以计算价格了。
    if (needSelectNum === curSelectNum) {
      enableBuy = true;
      selectSkuInfo = skuInfoList.find((skuInfo) => {
        const length = skuInfo.skuInfoNames.length;
        // 规格列表中记录的有多少个规格，和选中的规格数不匹配，不是要找的记录
        if (length !== selectSkuValIds.length) {
          return false;
        }
        // 规格数匹配上了，匹配valId
        const sortedSkuInfoNames = [];
        selectSkuValIds.forEach((item) => {
          const specSkuInfoName = skuInfo.skuInfoNames.find((skuInfoName) => {
            return item.skuTreeId === skuInfoName.keyId;
          });
          sortedSkuInfoNames.push(specSkuInfoName);
        });

        for (let i = 0; i < length; i++) {
          if (sortedSkuInfoNames[i].valId !== selectSkuValIds[i].treeValId) {
            return false;
          }
        }
        return true;
      });
      if (selectSkuInfo) {
        // sku对应的 最大购买数量，销售价格，id，图片
        buyNumMax = selectSkuInfo.stock;
        if (
          goodsDetail.wxItem.type === goodsTypeEnum.SERVER.value ||
          goodsDetail.wxItem.type === goodsTypeEnum.SLEEVE_SYSTEM.value
        ) {
          buyNumMax = Number.MAX_VALUE;
        }
        // 如果是积分商品
        if (goodsDetail.itemIntegralShopDTO) {
          skuIntegral = selectSkuInfo.exchangeIntegral;
          buyNumMax = selectSkuInfo.exchangeNum;
        }
        salePrice = selectSkuInfo.salePrice;
        innerBuyCouponNum = selectSkuInfo.innerBuyCouponNum;
        // 判断是否正在进行中的秒杀活动商品，是则将商品的秒杀价格设置为属性显示及购买价格
        const seckillActivityStatus =
          goodsDetail.seckillItemDTO && goodsDetail.seckillItemDTO.status ? goodsDetail.seckillItemDTO.status : null;
        if (seckillActivityStatus && seckillActivityStatus === 1 && goodsType === constants.goods.type.seckill) {
          salePrice = selectSkuInfo.seckillPrice;
        }

        skuId = selectSkuInfo.skuId;
        // 获取当前sku对应图片信息
        const valId = selectSkuInfo.skuInfoNames[0] ? selectSkuInfo.skuInfoNames[0].valId : 0;
        if (valId && skuTreeList[0].showImage === 1) {
          skuTreeList[0].treeValList.forEach((item) => {
            if (item.valId === valId) {
              if (!!item.imageUrl && item.imageUrl.length > 10) {
                thumbnail = item.imageUrl;
              }
            }
          });
        }
      }
    }

    // 状态展示优先级: 不参与拼团/商品抢光/已下架/库存不足
    const itemInfo = gainSelectedSkuItemInfo(skuInfoList, selectSkuValIds);
    // skuDisableStatus 0 不禁用, 1禁用
    if (itemInfo && itemInfo.skuDisableStatus === 1) {
      enableBuy = false;
      btnText = '该商品不参与拼团';
    } else {
      // 0 不限制; 1 活动总数量; 2 门店数量;
      const { ptActivitySkuSaleLimitType, ptActivitySkuSaleLimitList } = goodsDetail;

      // itemInfo.skuId

      if (ptActivitySkuSaleLimitType === 1) {
        if (ptActivitySkuSaleLimitList[0].saleLimitRemain === 0) {
          enableBuy = false;
          btnText = '该商品已被抢光';
        }
      } else if (ptActivitySkuSaleLimitType === 2) {
        const temp = ptActivitySkuSaleLimitList.filter((item) => item.skuId === itemInfo.skuId);

        if (temp[0].saleLimitRemain === 0) {
          enableBuy = false;
          btnText = '该商品已被抢光';
        }
      }
    }

    this.setState(
      {
        selectSkuValIds,
        enableBuy: enableBuy,
        btnText: btnText,
        skuSalePrice: salePrice,
        skuIntegral: skuIntegral,
        innerBuyCouponNum: innerBuyCouponNum,
        skuTreeIds: skuTreeIds,
        buyNumber: 1,
        buyNumMax: buyNumMax,
        skuTreeNames: skuTreeNames,
        skuId: skuId,
        thumbnail: thumbnail,
        sku: selectSkuInfo,
      },

      () => this.getStock()
    );
  }

  /**
   * 获取库存
   */
  getStock() {
    const { goodsDetail } = this.props;
    const { enableBuy, buyNumMax } = this.state;
    if (enableBuy) {
      if (
        buyNumMax <= 0 &&
        goodsDetail.wxItem.type !== goodsTypeEnum.SERVER.value &&
        goodsDetail.wxItem.type !== goodsTypeEnum.SLEEVE_SYSTEM.value &&
        goodsDetail.wxItem.type !== goodsTypeEnum.TICKETS.value &&
        goodsDetail.wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value
      ) {
        this.setState({
          enableBuy: false,
          btnText: '库存不足',
        });
      } else {
        this.setState({
          btnText: '确定',
        });
      }
    }
  }

  /**
   * 底部操作按钮
   */
  handlerSure() {
    const { buyNumMax } = this.state;
    const {  goodsDetail } = this.props;
    if (!buyNumMax && goodsDetail.wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value) {
      wxApi.showToast({
        title: '活动商品剩余数量不足',
        icon: 'none',
      });
      return;
    }
    const goodsInfo = this.getChoose();
    protectedMailBox.send(pageLinkEnum.orderPkg.payOrder, 'goodsInfoList', [goodsInfo]);
    Taro.setStorageSync('GOODS_LIST_CUT', JSON.stringify([goodsInfo]))
    // window.sessionStorage.setItem('GOODS_LIST_CUT', JSON.stringify([goodsInfo]))
    const { onChoose } = this.props;
    !!goodsInfo && onChoose && onChoose(goodsInfo);
  }

  /**
   * 获取组建立即购买的订单信息
   */
  getChoose() {
    const { enableBuy, btnText } = this.state;
    const {  goodsDetail } = this.props;
    if (enableBuy || goodsDetail.wxItem.type === goodsTypeEnum.VIRTUAL_GOODS.value) {
      return this.buildGoodsInfo();
    } else {
      if (btnText !== '确定') {
        this.warning(btnText);
      } else {
        this.warning('请选择相应的规格');
      }
    }
  }

  /**
   * 警告弹框
   * @param {*} msg
   */
  warning(msg) {
    wxApi.showToast({
      icon: 'none',
      title: msg,
    });
  }

  /**
   * 组建立即购买的订单信息
   */
  buildGoodsInfo() {
    const { goodsDetail } = this.props;
    const {
      skuTreeIds,
      skuTreeNames,
      skuSalePrice,
      skuIntegral,
      innerBuyCouponNum,
      buyNumber,
      skuId,
      sku,
    } = this.state;
    const goodsInfo = {};
    goodsInfo.itemNo = goodsDetail.wxItem.itemNo;
    goodsInfo.scan = goodsDetail.scan;
    goodsInfo.barcode = goodsDetail.wxItem.barcode;
    goodsInfo.pic = goodsDetail.wxItem.thumbnail;
    goodsInfo.name = goodsDetail.wxItem.name;
    goodsInfo.skuTreeIds = skuTreeIds;
    goodsInfo.skuTreeNames = skuTreeNames;
    goodsInfo.salePrice = skuSalePrice;
    goodsInfo.skuIntegral = skuIntegral;
    goodsInfo.innerBuyCouponNum = innerBuyCouponNum;
    goodsInfo.itemCount = buyNumber;
    goodsInfo.count = buyNumber;
    goodsInfo.skuId = skuId;
    goodsInfo.noNeedPay = goodsDetail.wxItem.noNeedPay;
    goodsInfo.freight = goodsDetail.wxItem.freight;
    goodsInfo.itemStock = goodsDetail.wxItem.itemStock;
    goodsInfo.wxItem = goodsDetail.wxItem;
    goodsInfo.sku = sku;
    if (sku) {
      goodsInfo.barcode = sku.skuBarcode;
    }

    if (goodsDetail.ptActivityItemDTO) {
      goodsInfo.activityName = goodsDetail.ptActivityItemDTO.activityName;
      goodsInfo.leaderPromFee = goodsDetail.ptActivityItemDTO.leaderPromFee;
    }

    if (goodsDetail.groupNo) {
      // 有groupNo表示是参团
      goodsInfo.groupNo = goodsDetail.groupNo;
    } else if (goodsDetail.ptActivityItemDTO) {
      const { activityId } = goodsDetail.ptActivityItemDTO;
      // 没groupNo有activityId表示开团
      goodsInfo.activityId = activityId;
      goodsInfo.minPeople = this._minPeople;
    }

    // 推广大使卡包
    if (goodsDetail.isCardPack) {
      goodsInfo.channel = goodsDetail.channel;
      goodsInfo.activityId = goodsDetail.activityId;
      goodsInfo.refUseId = goodsDetail.refUseId;
    }
    return goodsInfo;
  }

  render() {
    const {
      goodsDetail,
      limitAmountTitle,
      showStock,
      scoreName,
      $global: { $tmpStyle },
    } = this.props;
    const {
      thumbnail,
      buyNumMax,
      ignoreStock,
      skuIntegral,
      skuSalePrice,
      buyNumber,
      buyNumMin,
      allLimitAmountText,
      onceLimitAmountText,
      enableBuy,
      btnText,
    } = this.state;
    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcg-GoodsAttribute'
        className='wk-wcg-GoodsAttribute'
      >
        {!!goodsDetail && (
          <View>
            <View className='pop-goods-info'>
              <View className='pop-img-box'>
                <Image src={thumbnail || goodsDetail.wxItem.thumbnail} className='goods-thumbnail'></Image>
              </View>
              <View className='pop-goods-des'>
                <View className='pop-goods-title'>{goodsDetail.wxItem.name}</View>
                { goodsDetail.wxItem.type !== goodsTypeEnum.VIRTUAL_GOODS.value &&
                (<Block>
                    {goodsDetail.itemIntegralShopDTO ? (
                      <View className='pop-goods-stock'>{'剩余' + (buyNumMax || 0) + '件'}</View>
                    ) : (
                      !ignoreStock &&
                      showStock && <View className='pop-goods-stock'>{'库存' + (buyNumMax || 0) + '件'}</View>
                    )}
                  </Block>)
                }


                {goodsDetail.itemIntegralShopDTO ? (
                  <View className='pop-goods-price' style={_safe_style_('color:' + $tmpStyle.btnColor)}>
                    {(skuIntegral || skuIntegral == 0 ? skuIntegral : goodsDetail.wxItem.exchangeIntegral) +
                      `${scoreName}+ ` +
                      filters.moneyFilter(
                        skuSalePrice || skuSalePrice == 0 ? skuSalePrice : goodsDetail.wxItem.salePrice,
                        true
                      ) +
                      '元'}
                  </View>
                ) : (
                  <View className='pop-goods-price' style={_safe_style_('color:' + $tmpStyle.btnColor)}>
                    {'¥ ' + filters.moneyFilter(skuSalePrice || goodsDetail.wxItem.salePrice || '', true)}
                  </View>
                )}
              </View>
            </View>

            {/*  属性选择区域  */}
            <View className='attribute-label-box'>
              {!!goodsDetail.skuTreeList &&
                (goodsDetail.skuTreeList || []).map((skuTree, treeIndex) => {
                  return (
                    <Block key={skuTree.keyId}>
                      <View className='label'>{skuTree.keyName}</View>
                      <View className='label-item-box'>
                        {skuTree.treeValList.map((treeVal, treeValIndex) => {
                          return (
                            <View
                              className={'label-item ' + (treeVal.active ? 'active' : '')}
                              key={treeVal.valId}
                              onClick={_fixme_with_dataset_(this.handleSelectTreeVal, {
                                skuTreeIndex: treeIndex,
                                skuTreeId: skuTree.keyId,
                                skuTreeName: skuTree.keyName,
                                treeValIndex: treeValIndex,
                                treeValId: treeVal.valId,
                                treeValName: treeVal.valName,
                              })}
                              style={_safe_style_(
                                treeVal.active
                                  ? 'color:' +
                                      $tmpStyle.btnColor +
                                      ';background:' +
                                      $tmpStyle.bgColor +
                                      ';border-color:' +
                                      $tmpStyle.bgColor
                                  : ''
                              )}
                            >
                              {treeVal.valName}
                            </View>
                          );
                        })}
                      </View>
                    </Block>
                  );
                })}
            </View>

            {/*  购买数量区域  */}
            <View className='buy-num-box'>
              <View className='num-box'>
                <View className='num-label'>购买数量</View>
                <View className='num-operation-box'>
                  { goodsDetail.wxItem.type !==goodsTypeEnum.VIRTUAL_GOODS.value &&
                    <View
                      className={'num-decrease ' + (buyNumber == buyNumMin ? 'num-disable' : '')}
                      onClick={this.handleDecreaseBuyNum}
                    >
                      -
                    </View>
                  }
                  <View className='num-input'>
                    <Input className='num-input-input' type='number' value={buyNumber} disabled></Input>
                  </View>
                  { goodsDetail.wxItem.type !==goodsTypeEnum.VIRTUAL_GOODS.value &&
                    <View
                      className={'num-increase ' + (buyNumber == buyNumMax ? 'num-disable' : '')}
                      onClick={this.handleIncreaseBuyNum}
                    >
                      +
                    </View>
                  }
                </View>
              </View>
              {/*  限购数量提示区域  */}
              {!!limitAmountTitle && (
                <View className='limit-amount-box'>
                  {!!allLimitAmountText && <Text>{allLimitAmountText}</Text>}
                  {!!(allLimitAmountText && onceLimitAmountText) && <Text>，</Text>}
                  {!!onceLimitAmountText && <Text>{onceLimitAmountText}</Text>}
                </View>
              )}
            </View>
            {/*  底部操作按钮  */}
            <View className='popup-join-btn-box'>
              <View
                className='popup-join-btn'
                onClick={this.handlerSure.bind(this)}
                style={_safe_style_('background:' + (enableBuy ? $tmpStyle.btnColor : '#cccccc'))}
              >
                {btnText}
              </View>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default GoodsAttribute;
