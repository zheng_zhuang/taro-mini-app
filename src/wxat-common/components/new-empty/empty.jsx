import React from 'react'; // @hostExternalClassesConvered(Empty)
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import classNames from 'classnames';

import './empty.scss';

const DEF_EMPTY = 'https://htrip-static.ctlife.tv/distribution/emptyGoodsList.png';

class Empty extends React.Component {
  static defaultProps = {
    message: '空白如也',
    icon: DEF_EMPTY,
    isDialog: false,
    emptyIconStyle: '',
    //点击回调
    onAction: null,
    messageStyle: {},
  };

  //todo: remove, dont modify

  static externalClasses = ['icon-class'];

  handleClickEmpty = () => {
    const onAction = this.props.onAction;
    onAction && onAction();
  };

  render() {
    const { isDialog, style, icon, emptyIconStyle, message, className, messageStyle } = this.props;
    return (
      <View
        data-scoped='wk-wce-Empty'
        className={classNames('wk-wce-Empty', 'com-empty', className, { dialog: isDialog != null && isDialog })}
        onClick={this.handleClickEmpty}
        style={_safe_style_(style)}
      >
        <View className='com-empty__wrapper'>
          <Image className={`bg ${this.props.iconClass}`} src={icon} style={_safe_style_(emptyIconStyle)}></Image>
          <View className='message' style={_safe_style_(messageStyle)}>
            {message}
          </View>
        </View>
      </View>
    );
  }
}

export default Empty;
