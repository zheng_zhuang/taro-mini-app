import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { View, Image, Text } from '@tarojs/components';
import React, {ComponentClass} from 'react';
import Taro from '@tarojs/taro';
import dateUtil from '../../utils/date.js';
import template from '../../utils/template.js';
import './index.scss';

interface ComponentProps {
  dataSource: { data: null };
  isComponent?: boolean;
  optionConfig?: string;
  currentStore?: any;
  roomCode?: any;
  roomCodeTypeId?: any;
  roomCodeTypeName?: any;
  trueCode?: any;
  onMyevent?: Function;
}

interface ComponentState {
  tmpStyle: {};
  date: never[];
  calendar: null;
  outTime: string;
  inTime: string;
  nowTime: string;
  DaysBetween: string;

}

class Calendar extends React.Component<ComponentProps, ComponentState> {

  state = {
    tmpStyle: {}, // 主题模板配置
    calendar: null,
    // 构建顶部日期时使用
    date: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
    inTime: '2021-01-07',
    outTime: '2021-01-08',
    showinTime: '',
    showoutTime: '',
    nowTime: '',
    DaysBetween: '',
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      let inTime = nextProps.inTime;
      let outTime = nextProps.outTime;
      this.setState({
        inTime,
        outTime,
      });

      this.init_date(nextProps);
    }
  }

  componentDidMount() {
    this.init_today()
    this.getTemplateStyle()
  }

  //关闭事件
  closeDialog = () => {
    this.props.onMyevent({showcalendar: false})
  }

  sureForm = () => {
    this.props.onMyevent(
      {
        ishasValue: true,
        showcalendar: false,
        inTime: this.state.inTime,
        outTime: this.state.outTime,
        DaysBetween: this.state.DaysBetween,
      }
    )
  }

  //获取主题
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  // 日历初始化
  dataInit = (setYear, setMonth) => {
    // 当前时间/传入的时间
    var now = setYear ? new Date(setYear, setMonth) : new Date();
    var year = setYear || now.getFullYear();
    // 传入的月份已经加1
    var month = setMonth || now.getMonth() + 1;
    // 构建某日数据时使用
    var obj = {};
    // 需要遍历的日历数组数据
    var dateArr = [];
    // 需要的格子数，为当前星期数+当月天数
    var arrLen = 0;
    // 该月加1的数值，如果大于11，则为下年，月份重置为1
    // 目标月1号对应的星期
    var startWeek = new Date(year + '-' + (month < 10 ? '0' + month : month) + '-01').getDay();
    //获取目标月有多少天
    var dayNums = new Date(year, month < 10 ? '0' + month : month, 0).getDate();
    var num = 0;
    // 计算当月需要的格子数 = 当前星期数+当月天数
    arrLen = startWeek * 1 + dayNums * 1;
    for (var i = 0; i < arrLen; i++) {
      if (i >= startWeek) {
        num = i - startWeek + 1;
        obj = {
          /*
            * 返回值说明
            * isToday ： 2018-12-27
            * dateNum :  27
            */
          isToday: year + '-' + (month < 10 ? '0' + month : month) + '-' + (num < 10 ? '0' + num : num),
          dateNum: num,
        };
      } else {
        // 填补空缺
        // 例如2018-12月第一天是星期6，则需要补6个空格
        obj = {};
      }
      dateArr[i] = obj;
    }
    return dateArr;
  }

  // 点击了日期，选择入住时间或者离店时间
  dayClick = (e) => {
    var that = this;
    var eTime = e.currentTarget.dataset.day;
    var inTime = that.state.inTime;
    var outTime = that.state.outTime;
    if (inTime == '' || new Date(eTime) <= new Date(inTime) || outTime != '') {
      // 如果入住时间为空或选择的时间小于等于入住时间，则选择的时间为入住时间
      inTime = eTime;
      outTime = '';
    } else {
      outTime = eTime;
    }
    let showinTime = that.MonthDayformat(inTime);
    let showoutTime = that.MonthDayformat(outTime);
    that.setState({
      inTime,
      outTime,
      showinTime,
      showoutTime,
    });

    if (that.getDaysBetween(inTime, outTime)) {
      let DaysBetween = that.getDaysBetween(inTime, outTime);
      that.setState({
        DaysBetween,
      });
    }
  }

  getDaysBetween = (dateString1, dateString2) => {
    var startDate = Date.parse(dateString1);
    var endDate = Date.parse(dateString2);
    if (startDate > endDate) {
      return 0;
    }
    if (startDate == endDate) {
      return 1;
    }
    var days = (endDate - startDate) / (1 * 24 * 60 * 60 * 1000);
    return days;
  }

  //格式化日期，只显示月份和日
  MonthDayformat = (date) => {
    let Dtime = new Date(date);
    var Dtimemonth = Dtime.getMonth() + 1;

    // Dtimemonth < 10 ? '0' + Dtimemonth : Dtimemonth
    return `${Dtime.getMonth() + 1 < 10 ? '0' + Dtimemonth : Dtimemonth}-${
      Dtime.getDate() < 10 ? '0' + Dtime.getDate() : Dtime.getDate()
    }`;
  }

  init_date = (newVal) => {
    let showinTime = this.MonthDayformat(newVal.inTime);
    let showoutTime = this.MonthDayformat(newVal.outTime);
    let DaysBetween = this.getDaysBetween(newVal.inTime, newVal.outTime);
    this.setState({
      showinTime,
      showoutTime,
      DaysBetween,
    });
  }

  init_today = () => {
    const getDay = dateUtil.getNowDay();
    this.getTemplateStyle(); //获取主题信息
    // 获取本月时间
    var nowTime = new Date();
    var year = nowTime.getFullYear();
    var month = nowTime.getMonth();
    var time = [];
    var timeArray = [];
    // 循环6个月的数据
    for (var i = 0; i < 6; i++) {
      year = month + 1 > 12 ? year + 1 : year;
      month = month + 1 > 12 ? 1 : month + 1;
      // 每个月的数据
      time = this.dataInit(year, month);
      // 接收数据
      // timeArray[year + '年' + (month < 10 ? '0' + month : month) + '月'] = time;
      timeArray.push(
        {
          title: year + '年' + (month < 10 ? '0' + month : month) + '月',
          date: time
        }
      )
    }


    this.setState({
      calendar: timeArray,
      nowTime: getDay,
    });
  }

  render() {
    const {
      date,
      calendar,
      inTime,
      outTime,
      tmpStyle,
      nowTime,
      showinTime,
      showoutTime,
      DaysBetween,
    } = this.state;
    const { showcalendar } = this.props
    return (
      showcalendar && (
        <View data-scoped="wk-swcc-Calendar" className="wk-swcc-Calendar page">
          <View className="tcBox">
            <View className="tcBox-hui" onClick={this.closeDialog}></View>
            <View className="dateBox">
              <View className="header">
                <View className="headtitle-box">
                  <View className="header-title">请选择入住离店日期</View>
                  <Image
                    onClick={this.closeDialog}
                    className="headClosebtn"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/line-close.png"
                    mode="aspectFill"
                  ></Image>
                </View>
                <View className="weekBox">
                  {date &&
                    date.map((item, index) => {
                      return (
                        <View className={'weekItem ' + (item == '周六' || item == '周日' ? 'weekMark' : '')} key={'week-' + index}>
                          {item}
                        </View>
                      );
                    })}
                </View>
              </View>
              {/*  日期  */}
              <View className="date-content">
                {!!calendar &&
                  calendar.map((calendarItem, idx) => {
                    return (
                      <View className="date-box" key={'date-content-' + idx}>
                        <View className="title">{calendarItem.title}</View>
                        <View className="content">
                          {
                            calendarItem.date.map((item, index) => {
                              return (
                                <View
                                  key={'date-' + index}
                                  style={_safe_style_(
                                    'background:' +
                                      ((item.isToday > inTime && item.isToday < outTime) ||
                                      item.isToday == inTime ||
                                      item.isToday == outTime
                                        ? tmpStyle.bgColor
                                        : '#ffffff')
                                  )}
                                  className={
                                    'days ' +
                                    ((item.isToday > inTime && item.isToday < outTime) ||
                                    item.isToday == inTime ||
                                    item.isToday == outTime
                                      ? 'day-select'
                                      : '' && item.isToday == outTime
                                      ? 'dadsaf'
                                      : '' || item.isToday >= nowTime
                                      ? 'canselect'
                                      : 'not-select')
                                  }
                                  onClick={_fixme_with_dataset_(this.dayClick, { id: item.dateNum, day: item.isToday })}
                                >
                                  <View
                                    className={
                                      item.isToday == inTime ? 'start' : '' || item.isToday == outTime ? 'end' : ''
                                    }
                                    style={_safe_style_(
                                      'background:' +
                                        (item.isToday == inTime || item.isToday == outTime ? tmpStyle.btnColor : '')
                                    )}
                                  >
                                    {item.dateNum}
                                  </View>
                                </View>
                              );
                            })}
                        </View>
                      </View>
                    );
                  })}
              </View>
              {outTime && (
                <View className="reserve-time">
                  <View className="reserve-box">
                    <View className="reserve-item">
                      <Text>入住：</Text>
                      <Text className="itemvalue">{showinTime}</Text>
                    </View>
                    <View className="reserve-item reserve-item-middle">
                      <Text>离店：</Text>
                      <Text className="itemvalue">{showoutTime}</Text>
                    </View>
                    <View className="reserve-item reserve-count itemvalue">
                      共<Text>{DaysBetween}</Text>晚
                    </View>
                  </View>
                  <View className="surebtnBox">
                    <View
                      onClick={this.sureForm}
                      className="sureBtn"
                      style={_safe_style_('background:' + tmpStyle.btnColor + ';')}
                    >
                      确定
                    </View>
                  </View>
                </View>
              )}
            </View>
          </View>
        </View>
      )
    );
  }
}

export default Calendar as ComponentClass<PageOwnProps, PageState>;
