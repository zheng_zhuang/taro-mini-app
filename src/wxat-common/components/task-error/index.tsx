import React, { FC, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { useSelector } from 'react-redux';
import wxApi from '../../utils/wxApi';
import './index.scss';
import state from '@/state/index.js';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';
import useTemplateStyle from '@/hooks/useTemplateStyle';
import store from '@/store/index'
const decorateImg = cdnResConfig.decorate;

// props的类型定义
type ComponentProps = {
  onCallSomeFun?: () => any;
  storeName: any;
};

interface HaggleModule {
  props: ComponentProps,
}


let HaggleModule: FC<ComponentProps> = (props) => {
  const storeInfo = useSelector((state) => state.base.currentStore);
  const tmpStyle = useTemplateStyle({ autoSetNavigationBar: true });
  const { onCallSomeFun, storeName } = props;

  const pickStore = () => {
    wxApi.$navigateTo({
      url: 'wxat-common/pages/store-list/index',
    });
  };

  useEffect(() => {
    onCallSomeFun && onCallSomeFun();
  }, [storeInfo]);

  return (
    <View data-scoped='wk-wct-TaskError' className='wk-wct-TaskError wrap'>
      <Image
        className='bg-image'
        src='https://cdn.wakedata.com/resources/dss-web-portal/cdn/wxma/mine/ic-task-error.png'
      ></Image>
      <View className='wrap-position'>
        <View className='transition-one'></View>
        <View className='transition-two'></View>
        <View className='into-wrap'>
          <View className='content'>
            <Text>您当前定位的门店未参与活动,</Text>
            <Text>请切换门店参与活动</Text>
          </View>
          <View
            className='content-bottom'
            style={_safe_style_('background-color:' + tmpStyle.btnColor)}
            onClick={pickStore}
          >
            <View className='image-wrap'>
              <Image className='image-wrap__image' src={decorateImg.location}></Image>
            </View>
            <View className='content-bottom-inner'>
              <Text>{storeName ? storeName : '请选择门店'}</Text>
              <Text>{'>'}</Text>
            </View>
          </View>
        </View>
        <View className='background-mask'></View>
      </View>
    </View>
  );
};

// 给props赋默认值
HaggleModule.defaultProps = {
  dataList: [],
  showType: 0,
};

export default HaggleModule;
