import React, { ComponentClass } from 'react';
import wxApi from '@/wxat-common/utils/wxApi';
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import utilDate from '../../utils/date';
import constants from "../../constants";
import template from '../../utils/template';
import commentConfig from '../../constants/commentConfig';
import authHelper from '../../utils/auth-helper';

import LoadMore from '../load-more/load-more';
import './index.scss';

import getStaticImgUrl from '../../constants/frontEndImgUrl'
import { ITouchEvent } from '@tarojs/components/types/common';
import { connect } from 'react-redux';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const graphImg = cdnResConfig.graph;

const loadMoreStatus = constants.order.loadMoreStatus;

interface CommentArticleProps {
  type: string;
  itemNo: string;
  order: string;
  onDeleteComment: Function;
}

interface StateProps {
  templ: any;
}

type IProps = CommentArticleProps & StateProps;

interface CommentArticle {
  props: IProps;
}

const mapStateToProps = () => {
  // getTemplateStyle 依赖 state
  return { templ: template.getTemplateStyle() };
};

@connect(mapStateToProps, undefined, undefined, { forwardRef: true })

class CommentArticle extends React.Component {
  state = {
    canLike: commentConfig.ARTICLE.canLike,
    canDelete: !!commentConfig.ARTICLE.deleteComment,
    commentList: [],
    pageNo: 1,
    pageSize: 10,
    hasMore: true,
    loadMoreStatus: loadMoreStatus.HIDE,
  };

  componentDidMount() {
    this.init();
  }

  init = () => {
    const { itemNo } = this.props;
    if (!itemNo) return;

    this.setState({ pageNo: 1, commentList: [] }, () => {
      this.getCommentList();
    });
  };

  onReachBottom = () => {
    const { hasMore } = this.state;
    if (!hasMore) return;

    this.setState({ loadMoreStatus: loadMoreStatus.LOADING });
    this.getCommentList();
  };

  onRetryLoadMore = () => {
    this.setState({ loadMoreStatus: loadMoreStatus.LOADING });
    this.getCommentList();
  };

  handleShowLargeImage = (e: ITouchEvent) => {
    wxApi.previewImage({
      current: '', // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.src], // 需要预览的图片http链接列表
    });
  };

  getCommentList = async (orderBy?: string) => {
    if (orderBy) {
      this.setState({ pageNo: 1, hasMore: true });
    }
    const { pageNo: _pageNo, pageSize, commentList } = this.state;
    const pageNo = orderBy ? 1 : _pageNo;

    const { itemNo, order, type } = this.props;

    // setState 一定是异步的
    const params = {
      pageNo: orderBy ? 1 : pageNo,
      pageSize: pageSize,
      itemNo: itemNo,
      orderBy: (orderBy || order) === 'like' ? 0 : 1,
    };

    const { data, totalCount } = await commentConfig[type].getCommentList(params);
    const formatData = this.formatTime(data);
    if (pageNo !== 1) {
      formatData.unshift(...commentList);
    }
    this.setState({
      commentList: formatData,
      hasMore: formatData.length < totalCount,
      pageNo: pageNo + 1,
    });
  };

  formatTime = (commentList: any[]) => {
    if (!commentList) {
      return [];
    }
    return commentList.map((item) => {
      if (item.createTime) {
        let createTime;
        if (typeof item.createTime === 'string') {
          createTime = new Date(item.createTime.replace(/-/g, '/'));
        } else {
          createTime = new Date(item.createTime);
        }
        const today = new Date().toLocaleDateString();
        const start = new Date(today).getTime();
        const current = new Date(createTime).getTime();
        if (current > start) {
          return {
            ...item,
            createTime: utilDate.format(createTime, 'hh:mm:ss'),
          };
        } else {
          return {
            ...item,
            createTime: utilDate.format(createTime, 'yyyy-MM-dd hh:mm:ss'),
          };
        }
      }
    });
  };

  onDeleteComment = async (e: ITouchEvent) => {
    const { type, onDeleteComment } = this.props;
    const { commentList } = this.state;
    const comment = e.currentTarget.dataset.item;
    const params = { commentId: +comment.id };

    const handle = commentConfig[type].deleteComment;
    if (handle) {
      await handle(params);
      onDeleteComment();
      this.setState({
        commentList: commentList.filter((i: any) => i.id !== params.commentId),
      });
    }
  };

  onChangeLike = async (e: ITouchEvent) => {
    const { itemNo, type } = this.props;
    const { pageNo, pageSize, commentList } = this.state;

    if (!authHelper.checkAuth()) return false;
    const comment = e.currentTarget.dataset.item;

    const params = {
      articleId: +itemNo,
      generalId: +comment.id,
      status: +!comment.liked,
    };

    await commentConfig[type].changeLike(params);
    // setState 是异步的
    this.setState({ pageNo: 1, pageSize: commentList.length }, async () => {
      await this.getCommentList();
      this.setState({ pageNo, pageSize });
    });
  };

  render() {
    const { canDelete, canLike, hasMore, commentList, loadMoreStatus } = this.state;

    const { templ } = this.props;

    return (
      <View
        data-fixme='02 block to view. need more test'
        data-scoped='wk-wcc-CommentArticle'
        className='wk-wcc-CommentArticle'
      >
        <View className='comment-module'>
          {commentList && commentList.length ? (
            <View className='comment-list'>
              {commentList.map((item: any) => {
                return (
                  <View className='comment-item' key={item.id}>
                    <View className='user-img'>
                      <Image
                        className='user-img-image'
                        src={item.avatarImgUrl ? item.avatarImgUrl : getStaticImgUrl.getDefaultImg}
                      ></Image>
                    </View>
                    <View className='comment-detail'>
                      <View style={_safe_style_('line-height: 45rpx')}>
                        <Text className='user-name'>{item.userName}</Text>
                        {!!item.topValue && <Text className='sticky'>置顶</Text>}
                      </View>
                      <View className='comment-info'>
                        {!!item.content && <View className='comment-content'>{item.content}</View>}
                        {!!(item.images && item.images.length !== 0) && (
                          <View className='comment-imgBox'>
                            {item.images.map((img, index) => {
                              return (
                                <Image
                                  className='comment-img'
                                  key={index}
                                  src={img}
                                  onClick={_fixme_with_dataset_(this.handleShowLargeImage, { src: img })}
                                  mode='aspectFill'
                                ></Image>
                              );
                            })}
                          </View>
                        )}
                      </View>
                      <View className='option-info'>
                        <View className='comment-time'>
                          <Text>{item.createTime}</Text>
                          {!!(canDelete && item.canDelete) && (
                            <View
                              className='comment-delete'
                              onClick={_fixme_with_dataset_(this.onDeleteComment, { item: item })}
                            >
                              删除
                            </View>
                          )}
                        </View>
                        {!!canLike && (
                          <View
                            className='option-like'
                            onClick={_fixme_with_dataset_(this.onChangeLike, { item: item })}
                          >
                            {item.liked ? (
                              <Image
                                className='like-logo'
                                src={graphImg.activeLikeNew}
                                style={_safe_style_('background:' + templ.btnColor)}
                              ></Image>
                            ) : (
                              <Image
                                className='like-logo'
                                src={graphImg.like1}
                                style={_safe_style_('background: #999')}
                              ></Image>
                            )}

                            <Text className='like-count'>{item.likeCount || 0}</Text>
                          </View>
                        )}
                      </View>
                      {!!(item.childComment && item.childComment.length) && (
                        <View className='reply-info'>
                          {item.childComment.map((replyItem) => {
                            return (
                              <View className='reply-item' key={replyItem.id}>
                                <View className='reply-content'>
                                  <View className='reply-content-label'>商家回复：</View>
                                  <View className='reply-content-box'>{replyItem.content}</View>
                                </View>
                                <Text className='reply-time'>{replyItem.createTime}</Text>
                              </View>
                            );
                          })}
                        </View>
                      )}
                    </View>
                  </View>
                );
              })}
              {!hasMore && <View className='no-more'>已显示全部评论</View>}
            </View>
          ) : (
            <View className='comment-empty'>暂无评论</View>
          )}
        </View>
        <LoadMore status={loadMoreStatus} onRetry={this.onRetryLoadMore}></LoadMore>
      </View>
    );
  }
}

export default CommentArticle as ComponentClass<CommentArticleProps>;
