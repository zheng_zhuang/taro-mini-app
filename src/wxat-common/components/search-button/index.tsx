import { Block, View, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import wxClass from '../../utils/wxClass.js';
import wxApi from '@/wxat-common/utils/wxApi';
import industryEnum from '../../constants/industryEnum.js';
import { connect } from 'react-redux';
import hoc from '@/hoc/index';

import './index.scss';


// 父子组件直接传递的props
interface PageOwnProps {
  currentStore: Object,
  navOpacity: Number
}

// 组件的state声明
interface PageState {
  industryEnum: any;
  navBarHeight: number;
}

type IProps = PageOwnProps;

interface SearchButton {
  props: IProps;
}

const mapStateToProps = (state) => ({
  industry: state.globalData.industry,
});
  
@connect(mapStateToProps, undefined)
@hoc
class SearchButton extends React.Component {
  /**
   * 组件的属性列表
   */
   static defaultProps = {
    currentStore: {},
    navOpacity: 0,
  };

  state = {
    industryEnum,
    navBarHeight: 0,
  }

  componentDidMount() {
    // this.onUserInfoReady()
    this.init();
  }

  clickSearch = () => {
    wxApi.$navigateTo({
      url: '/wxat-common/pages/search/index',
    });
  };

  handleClickCity = () => {
    wxApi.$navigateTo({
      url: '/sub-packages/moveFile-package/pages/store-list/index',
    });
  };

  init = () => {
    // 获取系统信息
    const systemInfo = wxApi.getSystemInfoSync();
    // 胶囊按钮位置信息
    const menuButtonInfo = wxApi.getMenuButtonBoundingClientRect();
    // 导航栏高度 = 状态栏到胶囊的间距（胶囊距上距离-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
    const navBarHeight =
      (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
    const menuRight = systemInfo.screenWidth - menuButtonInfo.right;
    const menuButton = menuButtonInfo.top - systemInfo.statusBarHeight;
    const menuHeight = menuButtonInfo.height;

    this.setState({
      navBarHeight,
    });
  };

  render() {
    const { industryEnum } = this.state;
    const { currentStore, industry } = this.props;
    return (
      <View data-scoped="wk-swcs-SearchButton" className="wk-swcs-SearchButton search-container">
        {industry === industryEnum.type.estate.value && (
          <View className="city-btn" onClick={this.handleClickCity.bind(this)}>
            <View className="city-name">{currentStore.city}</View>
            <View className="triangle"></View>
          </View>
        )}

        <Image
          className="search-btn"
          onClick={this.clickSearch}
          src="https://front-end-1302979015.file.myqcloud.com/images/c/images/search-white.svg"
        ></Image>
      </View>
    );
  }
}

export default SearchButton;
