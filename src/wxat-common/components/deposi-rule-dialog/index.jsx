import React from 'react'; // @externalClassesConvered(AnimatDialog)
import '@/wxat-common/utils/platform';
import { Block, ScrollView, View, Text, Image } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from '../../api/index';
import wxApi from '../../utils/wxApi';
import AnimatDialog from '../animat-dialog/index';
import './index.scss';
import cdnResConfig from '@/wxat-common/constants/cdnResConfig';

const commonImg = cdnResConfig.common;
class DeposiRuleDialog extends React.Component {
  static defaultProps = {
    appLogoPath: '',
    height: 0,
  };

  state = {
    ruleDetail: '',
  };

  componentDidMount() {
    this.getRule();
  }

  getRule() {
    wxApi
      .request({
        url: api.goods.depositRule,
        data: {},
      })
      .then((res) => {
        this.setState({
          ruleDetail: res.data,
        });
      })
      .catch((error) => {});
  }

  show() {
    this.nimatDialogCOMPT &&
      this.nimatDialogCOMPT.show({
        scale: 1,
      });
  }

  hide = () => {
    this.nimatDialogCOMPT && this.nimatDialogCOMPT.hide(true);
  };

  refAnimatDialogCOMPT = (node) => (this.nimatDialogCOMPT = node);

  render() {
    const { ruleDetail } = this.state;
    return (
      <View
        data-fixme='03 add view wrapper. need more test'
        data-scoped='wk-wcd-DeposiRuleDialog'
        className='wk-wcd-DeposiRuleDialog'
      >
        <AnimatDialog ref={this.refAnimatDialogCOMPT} animClass='rule-dialog'>
          <ScrollView className='scroll-view_H, scroll' scrollY>
            <View className='rule'>
              <View className='title'>定金商品规则</View>
              <Text className='block'>{ruleDetail}</Text>
            </View>
          </ScrollView>
          <Image className='icon-close' src={commonImg.close} onClick={this.hide}></Image>
        </AnimatDialog>
      </View>
    );
  }
}

export default DeposiRuleDialog;
