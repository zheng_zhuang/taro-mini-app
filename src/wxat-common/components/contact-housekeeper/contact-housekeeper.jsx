import React, {useEffect, useState} from 'react';
import {_safe_style_} from '@/wxat-common/utils/platform';
import {Image, MovableArea, MovableView, View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import buyHub from '../cart/buy-hub';
import cartHelper from '../cart/cart-helper';
import template from '../../utils/template';
import './index.scss';
import MovableH5 from '@/wxat-common/components/movable-h5'
import wxApi from '@/wxat-common/utils/wxApi'

const HoverCart = (props) => {
  const { appInfo, currentStore, environment, themeConfig, typeChange,onGetType } = props;
  const [price, setPrice] = useState(0);
  const [count, setCount] = useState(0);
  const [tmpStyle, setTmpStyle] = useState({});
  const [showHoverCart, setShowHoverCart] = useState(false);

  useEffect(() => {
    initCart();
    return () => {
      buyHub.hub.offMapChange(onCartMapChange);
    };
  }, []);

  useEffect(() => {
    // handlerClickCard(typeChange);
  }, [typeChange]);

  useEffect(() => {
    setShowHoverCart(appInfo && appInfo.floatCart && environment !== 'wxwork' && (Taro.ENV_TYPE.WEAPP === Taro.getEnv() || Taro.ENV_TYPE.WEB === Taro.getEnv()));
  }, [appInfo]);

  useEffect(() => {
    // 切换门店需要强制请求购物车数据，刷新门店下的商品价格
    cartHelper.getCarts(true);
  }, [currentStore]);

  useEffect(() => {
    setTmpStyle(getTemplateStyle());
  }, [themeConfig]);

  // 获取模板配置
  function getTemplateStyle() {
    return template.getTemplateStyle();
  }

  function initCart() {
    buyHub.hub.onMapChange(onCartMapChange);
    onCartMapChange();
    cartHelper.getCarts();
  }

  function handlerClickCard() {
    if(typeChange){
      if(typeChange.customerType == 3){
        // window.location.href='https://www.baidu.com';
        // window.location.href=typeChange.imgUrl;
        wxApi.$locationTo(typeChange.imgUrl)
      }
      if(typeChange.customerType == 2){
        onGetType(typeChange.customerType);//子组件回传一个状态给父组件
      }
    }
    // wxAppProxy.jumpToCart();
    // Taro.navigateTo({
    //   url: '/sub-packages/marketing-package/pages/shopping-cart/index'
    // })
  }
  function onCartMapChange() {
    let newPrice = 0;
      let newCount = 0;
    for (const uuid in buyHub.map) {
      const data = buyHub.map[uuid];
      newPrice += data.price * data.count; /* 商品价格乘以总数，才是总价格 */
      newCount += data.count;
    }
    setPrice(newPrice.toFixed(2));
    setCount(newCount);
  }

  let innerWidth = window.innerWidth
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-cch-HoverCart' className='wk-cch-HoverCart'>
        { (process.env.TARO_ENV === 'weapp') && (
          <MovableArea className='cart-movable-area' style={_safe_style_('height: 85%;')}>
            <MovableView
              className='cart-movable-view'
              onClick={handlerClickCard}
              direction='all'
              style={_safe_style_('top: 100%')}
            >
              <View className='cart' style={_safe_style_('background:transparent')}>
                <Image className='cart-img-k' src="https://htrip-static.ctlife.tv/wk/cicle.png" />
              </View>
            </MovableView>
          </MovableArea>
        )}
        {
          (process.env.TARO_ENV === 'h5') && (
            <MovableH5 top={190}   left={innerWidth - 64 - 5 - 15} margin={5} key="hover-cart" passClick={handlerClickCard}>
              <View className='cart-h5' style={_safe_style_('background:transparent')}>
                <Image className='cart-img-k' src="https://htrip-static.ctlife.tv/wk/cicle.png" />
              </View>
            </MovableH5>
          )
        }
      </View>
    );

};

export default HoverCart;
