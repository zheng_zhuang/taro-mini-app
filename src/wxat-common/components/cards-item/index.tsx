import React, { FC, useState, useEffect } from 'react';
import { _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import filters from '../../utils/money.wxs.js';
import format from '../../utils/filter-card-pack.wxs.js';
import template from '../../utils/template.js';

import './index.scss';
import useTemplateStyle from '@/hooks/useTemplateStyle';

const DisplayType = {
  BIGPICTURE: 'bigPictureMode',
};

// props的类型定义
type ComponentProps = {
  display: string;
  showDivider?: boolean /*是否显示底部分割线,可根据列表是否时最后一个元素控制分割线是否展示，此属性只有在display=='horizon'有效*/;
  cardsItem: Record<string, any>;
  reportSource?: string;
  onClick?: (object) => any;
};

let CardsItem: FC<ComponentProps> = ({ display, showDivider, cardsItem, reportSource, onClick }) => {
  const tmpStyle = useTemplateStyle();

  /**
   * 点击商品item
   * @param {*} event
   */
  const onClickGoodsItem = (event) => {
    // this.triggerEvent('click', cardsItem);
    onClick && onClick(cardsItem);
  };

  // 一行1个 大图模式
  const _renderBigPictureMode = (
    <View className='n-cards-item-container big-picture-mode' onClick={onClickGoodsItem}>
      <View className='cards-img-box'>
        <Image className='cards-img-box__image' mode='aspectFill' src={cardsItem.itemImageUrls[0]} />
        {cardsItem.pocketStatus !== 1 && (
          <View className='sold-out-cover'>{format.formatStatus(cardsItem.pocketStatus)}</View>
        )}
      </View>
      <View className='card-detail'>
        <View className='title-wraper'>
          {cardsItem.provNum !== 0 && (
            <View className='limit-order' style={_safe_style_('background:{tmpStyle.bgGradualChange}')}>
              <Text>限购{cardsItem.buyLimit}组</Text>
            </View>
          )}

          <Text className='ellipsis title'>{cardsItem.name}</Text>
        </View>
        <View className='ellipsis content'>{cardsItem.subtitle}</View>
        <View>
          <View className='price' style={{ color: tmpStyle.btnColor }}>
            <Text className='price-symbol'>￥</Text>
            {filters.moneyFilter(cardsItem.salePrice, true)}
          </View>
        </View>
        <View className='sold'>{cardsItem.cardSalesVolume || 0}人已购买</View>
        <Button className='btn-red' style={{ background: format.formatStatus(cardsItem.pocketStatus) }}>
          {format.formatStatus(cardsItem.pocketStatus)}
        </Button>
      </View>
    </View>
  );

  // 单行形式(左图右文) 经典列表

  const _renderClassicList = (
    <View className='n-cards-item-container classic-list' onClick={onClickGoodsItem}>
      <View className='cards-img-box'>
        <Image className='cards-img-box__image' mode='aspectFill' src={cardsItem.itemImageUrls[0]} />
        {cardsItem.pocketStatus !== 1 && (
          <View className='sold-out-cover'>{format.formatStatus(cardsItem.pocketStatus)}</View>
        )}
      </View>
      <View className='card-detail'>
        <View className='more-ellipsis title-wraper'>
          {cardsItem.provNum !== 0 && (
            <View className='limit-order' style={{ background: tmpStyle.bgGradualChange }}>
              <Text>限购{cardsItem.buyLimit}组</Text>
            </View>
          )}

          <Text className='title'>{cardsItem.name}</Text>
        </View>
        <View>
          <View>
            <View className='price' style={{ color: tmpStyle.btnColor }}>
              <Text className='price-symbol'>￥</Text>
              {filters.moneyFilter(cardsItem.salePrice, true)}
            </View>
            {/* <View className="original-price">￥{cardsItem.originalPrice}</View> */}
          </View>
          <View className='sold'>{cardsItem.cardSalesVolume || 0}人已购买</View>
        </View>
        <Button
          className='btn-red'
          style={{
            background: cardsItem.pocketStatus !== 1 ? 'rgba(204, 204, 204, 1)' : tmpStyle.btnColor,
          }}
        >
          {format.formatStatus(cardsItem.pocketStatus)}
        </Button>
      </View>
    </View>
  );

  const _renderOneRowTwo = (
    <View className='n-cards-item-container one-row-two' onClick={onClickGoodsItem}>
      <View className='cards-img-box'>
        <Image className='cards-img-box__image' mode='aspectFill' src={cardsItem.itemImageUrls[0]} />
        {cardsItem.pocketStatus !== 1 && (
          <View className='sold-out-cover'>{format.formatStatus(cardsItem.pocketStatus)}</View>
        )}
      </View>
      <View className='card-detail'>
        <View className='more-ellipsis title-wraper'>
          {cardsItem.provNum !== 0 && (
            <View
              className='limit-order'
              style={{
                background: tmpStyle.bgGradualChange,
              }}
            >
              <Text>限购{cardsItem.cardSalesVolume || 0}组</Text>
            </View>
          )}

          <Text className='title'>{cardsItem.name}</Text>
        </View>
        <View className='card-detail-item'>
          <View className='one-row-two-detail'>
            <View
              className='price'
              style={{
                color: tmpStyle.btnColor,
              }}
            >
              <Text className='price-symbol'>￥</Text>
              {filters.moneyFilter(cardsItem.salePrice, true)}
            </View>
            {/* <View className="original-price">￥{cardsItem.originalPrice}</View> */}
          </View>
          <View>
            <Button
              className='btn-red'
              style={{
                background: cardsItem.pocketStatus !== 1 ? 'rgba(204, 204, 204, 1)' : tmpStyle.btnColor,
              }}
            >
              {format.formatStatus(cardsItem.pocketStatus)}
            </Button>
            <View className='sold'>{cardsItem.cardSalesVolume || 0}已售</View>
          </View>
        </View>
      </View>
    </View>
  );

  const _renderOneRowwThree = (
    <View className='n-cards-item-container one-row-three' onClick={onClickGoodsItem}>
      <View className='cards-img-box'>
        <Image className='cards-img-box__image' mode='aspectFill' src={cardsItem.itemImageUrls[0]} />
        {cardsItem.pocketStatus !== 1 && (
          <View className='sold-out-cover'>{format.formatStatus(cardsItem.pocketStatus)}</View>
        )}
      </View>
      <View className='card-detail'>
        <Text className='ellipsis title'>{cardsItem.name}</Text>
        <View className='price' style={{ color: tmpStyle.btnColor }}>
          <Text className='price-symbol'>￥</Text>
          {filters.moneyFilter(cardsItem.salePrice, true)}
        </View>
        <View className='original-price'>
          <Text>￥{cardsItem.originalPrice}</Text>
        </View>
      </View>
    </View>
  );

  const _renderHorizon = (
    <View className='n-cards-item-container horizon' onClick={onClickGoodsItem}>
      <View className='cards-img-box'>
        <Image className='cards-img-box__image' mode='aspectFill' src={cardsItem.itemImageUrls[0]} />
        {cardsItem.pocketStatus !== 1 && (
          <View className='sold-out-cover'>{format.formatStatus(cardsItem.pocketStatus)}</View>
        )}
      </View>
      <View className='card-detail'>
        <Text className='ellipsis title'>{cardsItem.name}</Text>
        <View className='price' style={{ color: tmpStyle.btnColor }}>
          <Text className='price-symbol'>￥</Text>
          {filters.moneyFilter(cardsItem.salePrice, true)}
        </View>
        {/* <View class="original-price">￥{cardsItem.originalPrice}</View> */}
      </View>
    </View>
  );

  // const _renderFunc = {
  //   bigPictureMode: _renderBigPictureMode,
  //   classicList: _renderClassicList,
  //   oneRowTwo: _renderOneRowTwo,
  //   oneRowThree: _renderOneRowwThree,
  //   horizon: _renderHorizon
  // };

  return (
    <View data-fixme='02 block to view. need more test' data-scoped='wk-wcc-CardsItem' className='wk-wcc-CardsItem'>
      {/* {_renderFunc[display]()} */}
      {display === 'bigPictureMode' && _renderBigPictureMode}
      {display === 'classicList' && _renderClassicList}
      {display === 'oneRowTwo' && _renderOneRowTwo}
      {display === 'oneRowThree' && _renderOneRowwThree}
      {display === 'horizon' && _renderHorizon}
    </View>
  );
};

// 给props赋默认值
CardsItem.defaultProps = {
  display: DisplayType.BIGPICTURE,
  showDivider: true,
  cardsItem: {},
  reportSource: '',
};

export default CardsItem;
