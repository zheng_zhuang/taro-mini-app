import React from 'react'; // @externalClassesConvered(Empty)
import { _safe_style_, _fixme_with_dataset_ } from '@/wxat-common/utils/platform';
import { Block, View, Text, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';
import api from "../../../api";
import wxApi from '../../../utils/wxApi';
import template from '../../../utils/template';
import Edit from "../edit";
import Error from '../../error/error';
import Empty from '../../empty/empty';
import './index.scss';
import SafeAreaPlaceholder from "../../safe-area-placeholder";

class AddressList extends React.Component {
  static defaultProps = {
    onChange: null,
    isSelect: false,
  };

  state = {
    userReady: false,
    list: null,
    error: false,
    showEdit: false,
    editItem: null,
    tmpStyle: {},
  };

  componentDidMount() {
    this.getTemplateStyle();
    this.fetchAddres();
  }

  fetchAddres = () => {
    wxApi
      .request({
        url: api.address.list,
        loading: true,
      })
      .then((res) => {
        if (!res.data) wxApi.setStorageSync('selectedAddress', 'refresh')
        this.setState({
          list: res.data,
          error: false,
        });
      })
      .catch((error) => {
        this.setState({
          error: true,
        });
      });
  };

  setDefault = () => (event) => {
    const item = event.currentTarget.dataset.item;
    wxApi
      .request({
        url: api.address.setDefault,
        loading: true,
        data: {
          id: item.id,
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log('address setDefault error: ' + JSON.stringify(error));
      });
  };

  updateAddress = () => (event) => {
    const item = event.currentTarget.dataset.item;
    wxApi
      .request({
        url: api.address.update,
        loading: true,
        data: {
          id: item.id,
          consignee: 'bobo',
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log('address setDefault error: ' + JSON.stringify(error));
      });
  };

  deleteAddress = (event) => {
    const item = event.currentTarget.dataset.item;
    wxApi
      .request({
        url: api.address.delete,
        loading: true,
        data: {
          id: item.id,
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log('address setDefault error: ' + JSON.stringify(error));
      });
  };

  onAdd = () => {
    this.setState({
      showEdit: true,
      editItem: null,
    });
  };

  onEdit = (item, e) => {
    e.stopPropagation();
    this.setState({
      showEdit: true,
      editItem: item,
    });
  };

  onSave = () => {
    console.log('kkkk');
    this.setState({
      showEdit: false,
      editItem: null,
    });

    this.fetchAddres();
  };

  onDelete = () => {
    this.setState({
      showEdit: false,
      editItem: null,
    });

    this.fetchAddres();
  };

  onSelected = (e) => {
    const selected = e.currentTarget.dataset.item;
    this.props.onChange && this.props.onChange(selected);
    if (this.props.isSelect) {
      wxApi.setStorageSync('selectedAddress', selected)
      wxApi.navigateBack()
    }
  };

  // 获取模板配置
  getTemplateStyle = () => {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  };

  render() {
    const { showEdit, list, error, tmpStyle, editItem } = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-cal-List' className='wk-cal-List'>
        <View className='address-list' style={_safe_style_('display:' + (showEdit ? 'none' : ''))}>
          {!!(list && !list.length) && <Empty />}
          {!!error && <Error />}
          {!!list && (
            <View className='address-list__list'>
              {(list || []).map((item, index) => {
                return (
                  <View
                    className='address-list__item'
                    key={index}
                    onClick={_fixme_with_dataset_(this.onSelected, { item: item })}
                  >
                    <View>
                      <Text className='address-list__item-name'>{item.consignee}</Text>
                      <Text className='address-list__item-mobile'>{item.mobile}</Text>
                    </View>
                    <View className='address-list__item-address'>
                      {(item.province || '') +
                        (item.city || '') +
                        (item.region || '') +
                        (item.address || '') +
                        (item.roomNumber || '')}
                    </View>
                    <View className='address-list__item-operator'>
                      {item.isDefault === 1 && (
                        <Text
                          className='address-list__item-default'
                          style={_safe_style_(
                            'color:' +
                              tmpStyle.btnColor +
                              ';border-color:' +
                              tmpStyle.btnColor +
                              ';background:' +
                              tmpStyle.bgColor
                          )}
                        >
                          我的默认
                        </Text>
                      )}

                      <Button className='address-list__item-edit' onClick={this.onEdit.bind(this, item)}>
                        编辑
                      </Button>
                    </View>
                  </View>
                );
              })}
            </View>
          )}

          {!error && (
            <Button className='address-list__footer' onClick={this.onAdd}>
              +添加新地址
              <SafeAreaPlaceholder />
            </Button>
          )}
        </View>
        {!!showEdit && (
          <Edit editItem={editItem} onSave={this.onSave.bind(this)} onDelete={this.onDelete.bind(this)}></Edit>
        )}
      </View>
    );
  }
}

export default AddressList;
