import React from 'react';
import {_safe_style_} from '@/wxat-common/utils/platform';
import {Block, View, Text, Input, Image, RadioGroup, Radio, Button} from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '../../../utils/wxApi';
import api from "../../../api";
import template from '../../../utils/template';
import report from "../../../../sdks/buried/report";
import Dialog from "../../dialog";
import getStaticImgUrl from '@/wxat-common/constants/frontEndImgUrl'
import './index.scss';

const AK = 'BSNBZ-4C2LP-ONWDL-VLMU5-4QC47-4TBLX';
const rules = {
  consignee: [
    {
      required: true,
      message: '请输入联系人',
    },

    {
      reg: /^[0-9A-Za-z\u4e00-\u9fa5]+$/i,
      message: '联系人姓名由字母、数字或汉字组成',
    },
  ],

  mobile: [
    {
      required: true,
      message: '请输入手机号码',
    },

    {
      reg: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
      message: '手机号码无效',
    },
  ],

  address: [
    {
      required: true,
      message: '请选择收货地址',
    },
  ],

  roomNumber: [
    {
      required: true,
      message: '请输入详细地址',
    },
  ],
};

class AddressEdit extends React.Component {
  static defaultProps = {
    editItem: null,
    onSave: null,
    onDelete: null,
  };

  /**
   * 页面的初始数据
   */
  state = {
    form: {
      consignee: '',
      mobile: '',
      address: '',
      source: 'manual',
      status: 1,
      roomNumber: '',
      longitude: null,
      latitude: null,
    },

    checked: false,
    isEdit: false,
    dialogShow: false,
    cancelText: '',
    tipMsg: '',
    tmpStyle: {},
  };

  componentDidMount() {
    this.getTemplateStyle();
    const {editItem: model} = this.props;
    const {form} = this.state;
    let title = '添加新地址';
    if (model) {
      form.consignee = model.consignee;
      form.mobile = model.mobile;
      form.address = model.address;
      form.roomNumber = model.roomNumber;
      form.longitude = model.longitude;
      form.latitude = model.latitude;
      this.setState({
        form: {...form},
        isEdit: true,
        checked: model.isDefault === 1,
      });

      this.model = model;
      title = '修改地址';
    }
    wxApi.setNavigationBarTitle({
      title: title,
    });
  }

  // 修改联系人
  nameInput = (e) => {
    const {form} = this.state;
    form.consignee = e.detail.value.trim();
    this.setState({
      form,
    });
  };

  // 手机号码
  mobileInput = (e) => {
    const {form} = this.state;
    form.mobile = e.detail.value.trim();
    this.setState({
      form,
    });
  };

  // 详细地址
  addressInput = (e) => {
    const {form} = this.state;
    form.roomNumber = e.detail.value.trim();
    this.setState({
      form,
    });
  };

  // 默认地址
  checkboxChange = (e) => {
    this.setState({
      checked: !this.state.checked,
    });
  };

  // 选择地图地址
  clickChooseLocation = () => {
    const {form} = this.state;
    const params = {longitude: form.longitude, latitude: form.latitude};
    wxApi
      .getSetting()
      .then((res) => {
        console.log("Setting", res)
        // if (!res.authSetting['scope.userLocation']) {
        //   return wxApi
        //     .authorize({
        //       scope: 'scope.userLocation',
        //     })
        //     .then(
        //       () => {
        //         // 有经纬度定位之前的经纬度
        //         if (params.longitude && params.latitude) {
        //           return wxApi.chooseLocation(params);
        //         } else {
        //           return wxApi.chooseLocation();
        //         }
        //       },
        //       () => {
        //         this.openSetting();
        //       }
        //     );
        // } else {
        // 有经纬度定位之前的经纬度
        if (params.longitude && params.latitude) {
          return wxApi.chooseLocation(params);
        } else {
          return wxApi.chooseLocation();
        }
        // }
      })
      .then((location) => {
        if (location.address) {
          if (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true') {
            form.address = location.address;
          } else {
            form.address = location.name;
          }
          form.longitude = location.longitude;
          form.latitude = location.latitude;
          this.setState({
            form,
          });
        }
      });
  };

  // 地址校验
  validate() {
    const {form} = this.state;
    try {
      Object.keys(rules).forEach((key) => {
        const value = form[key];
        const rule = rules[key];

        let isValied = true;
        let msg = '';
        try {
          rule.forEach((r) => {
            if (r.required && !value) {
              throw Error(r.message);
            } else if (r.reg && !r.reg.test(value)) {
              throw Error(r.message);
            }
          });
        } catch (error) {
          isValied = false;
          msg = error.message;
        }
        if (!isValied) {
          throw Error(msg);
        }
      });
    } catch (error) {
      wxApi.showToast({
        title: error.message,
        icon: 'none',
      });

      return false;
    }
    return true;
  }

  // 增加修改地址
  addAddress = () => {
    if (!this.validate()) return;
    const {form, checked, isEdit} = this.state;
    const params = {...form};
    params.isDefault = checked ? 1 : 0;

    let url = api.address.add;
    if (isEdit) {
      params.id = this.model.id;
      url = api.address.update;
    }

    wxApi
      .request({
        url: url,
        loading: true,
        data: params,
      })
      .then((res) => {
        wxApi.showToast({
          title: '保存成功。',
        });

        if (this.props.onSave) {
          this.props.onSave();
        }
      })
      .catch((error) => {
        console.log('address setDefault error: ' + JSON.stringify(error));
      });
  };

  cancel = () => {
    this.setState({
      dialogShow: false,
    });
  };

  // 删除地址
  deleteAddress = () => {
    this.setState({
      dialogShow: false,
    });

    if (this.model.isDefault === 1) {
      return;
    }
    wxApi
      .request({
        url: api.address.delete,
        loading: true,
        data: {
          id: this.model.id,
        },
      })
      .then((res) => {
        wxApi.showToast({
          title: '删除成功。',
        });

        this.props.onDelete && this.props.onDelete();
      })
      .catch((error) => {
        console.log('address setDefault error: ' + JSON.stringify(error));
      });
  };

  // 删除地址弹窗
  onConfirmDelete = () => {
    const newValue = {
      dialogShow: true,
      cancelText: '取消',
      tipMsg: '确定要删除地址吗？',
    };

    if (this.model.isDefault === 1) {
      newValue.cancelText = '';
      newValue.tipMsg = '默认地址不能删除。';
    }
    this.setState(newValue);
  };

  // 使用微信地址
  getWxAddress = () => {
    wxApi.chooseAddress({
      success: (res) => {
        report.reportAccAddress(true);
        const {form} = this.state;
        form.consignee = res.userName;
        form.mobile = res.telNumber;
        form.address = res.detailInfo;
        if (res.provinceName) {
          form.province = res.provinceName;
          form.city = res.cityName;
          form.county = res.countyName;
        }
        this.setState({form});
      },
      fail: () => {
        report.reportAccAddress(false);
      },
    });
  };

  // 获取模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });

    if (templateStyle.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templateStyle.titleColor, // 必写项
      });
    }
  }

  // 打开授权界面
  openSetting = () => {
    wxApi.showModal({
      title: '是否授权当前位置',
      content: '需要获取您的地理位置，请确认授权，否则地图功能将无法使用',
      success: function (tip) {
        if (tip.confirm) {
          wxApi.openSetting({
            success: function (data) {
              if (data.authSetting['scope.userLocation'] === true) {
                wxApi.showToast({
                  title: '授权成功',
                  icon: 'success',
                  duration: 1000,
                });
              } else {
                wxApi.showToast({
                  title: '授权失败',
                  icon: 'none',
                  duration: 1000,
                });
              }
            },
          });
        }
      },
    });
  };

  render() {
    const {form, checked, tmpStyle, isEdit, cancelText, dialogShow, tipMsg} = this.state;
    return (
      <View data-fixme='02 block to view. need more test' data-scoped='wk-cae-Edit' className='wk-cae-Edit'>
        <View className='form'>
          <View className='input-view'>
            <Text className='input-view-text'>联系人：</Text>
            <Input
              className='form-input'
              placeholderClass='placeholder'
              maxLength='16'
              value={form.consignee}
              onBlur={this.nameInput}
              placeholder='请输入联系人(最大16个字)'
            />
          </View>
          <View className='input-view'>
            <Text className='input-view-text'>手机号码：</Text>
            <Input
              className='form-input'
              placeholderClass='placeholder'
              value={form.mobile}
              onBlur={this.mobileInput}
              type='number'
              placeholder='请输入手机号码'
            />
          </View>
          <View className='address-area' onClick={this.clickChooseLocation}>
            <View className='region-pikcer'>
              <View className='placeholder'>收货地址：</View>
              {!form.address ? (
                <View className='input-view-label limit-line' style={_safe_style_('color:#A2A2A4')}>
                  点击选择
                </View>
              ) : (
                <View className='input-view-label limit-line'>{form.address}</View>
              )}

              <Image className='right-icon' src={getStaticImgUrl.images.rightAngleGray_png}/>
            </View>
          </View>
          <View className='input-view'>
            <Text className='input-view-text'>详细地址：</Text>
            <Input
              className='form-input'
              placeholderClass='placeholder'
              value={form.roomNumber}
              onBlur={this.addressInput}
              placeholder='如道路门牌号、小区楼栋号、门牌号等'
            />
          </View>
          <RadioGroup onClick={this.checkboxChange} className='default'>
            <Radio value={checked} checked={checked} color={tmpStyle.btnColor}>
              设置为默认地址
            </Radio>
          </RadioGroup>
        </View>
        <View className='footer'>
          {isEdit ? (
            <Button className='btn' onClick={this.onConfirmDelete}>
              删除地址
            </Button>
          ) : process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true' ? (<View></View>) : (
            <Button className='btn' onClick={this.getWxAddress}>
              使用微信地址
            </Button>
          )}
          {isEdit ? (
            <Button
              className='btn second'
              onClick={this.addAddress}
              style={_safe_style_('background:' + tmpStyle.btnColor + ';margin-top: 0px;')}
            >
              保存
            </Button>
          ) : (
            <Button
              className='btn second'
              onClick={this.addAddress}
              style={_safe_style_('background:' + tmpStyle.btnColor + ';margin-top: 0px;' + (process.env.TARO_ENV === 'h5' && process.env.WX_OA === 'true' ? 'width: 100%;' : ''))}
            >
              保存
            </Button>
          )}
        </View>
        <Dialog
          cancelText={cancelText}
          visible={dialogShow}
          onCancel={this.cancel}
          onConfirm={this.deleteAddress}
          message={tipMsg}
        />
      </View>
    );
  }
}

export default AddressEdit;
