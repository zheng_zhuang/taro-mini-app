import { View } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import store from '@/store';
import filters from '../../../utils/money.wxs.js';
import wxApi from '../../../utils/wxApi';
import protectedMailBox from '../../../utils/protectedMailBox.js';
import report from '@/sdks/buried/report';
import template from '../../../utils/template.js';
import industryEnum from '../../../constants/industryEnum.js';
import cartHelper from '../../cart/cart-helper.js';
import buyHub from '../../cart/buy-hub.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import pageLinkEnum from '../../../constants/pageLinkEnum.js';
import shareUtil from '../../../utils/share.js';
import imageUtils from '../../../utils/image.js';
import state from '../../../../state';
import ShareGift from '../../share-gift';
import ShareDialog from '../../share-dialog';
import ChooseAttributeDialog from '../../choose-attribute-dialog';
import BuyNow from '../../buy-now';
import './index.scss';
import util from '@/wxat-common/utils/util';

const storeData = store.getState();
interface BottomOperationProps {
  // 商品详情
  goodsDetail: object;
  // 商品的sessionId
  sessionId: string;
  // 商品活动类型
  activityType: string;
  // 商品活动id
  activityId: string;
  // 生成分享海报二维码的请求参数（映射版）
  uniqueShareInfoForQrCode: object;
  // 是否是配送服务
  isDistributionServices: boolean;
  onHandleCanvas?: () => void;
}

class BottomOperation extends React.Component<BottomOperationProps> {
  state = {
    scanFlag: this.props.goodsDetail.scan, // 判断此商品是否需要扫描二维码
    tmpStyle: {}, // 主题模板
    goodsTypeEnum, // 商品类型常量
    industryEnum, // 行业类型常量
    industry: store.getState().globalData.industry, // 当前行业类型
    itemNo: this.props.goodsDetail.wxItem?.itemNo ? this.props.goodsDetail.wxItem.itemNo : null, // 商品单号
    wxItem: this.props.goodsDetail.wxItem || null, // 商品信息
    giftActivityDTO: this.props.goodsDetail.giftActivityDTO || null, // 活动详情
    itemIntegralShopDTO: this.props.goodsDetail.itemIntegralShopDTO || null, // 积分商品详情
    shareGiftShow: false, // 是否展示分享有礼
    chooseAttrForCart: false, // 当前操作是否为加入购物车
    limitAmountTitle: null, // 限购类型标题，有则传到子组件以便展示限购数量
    isEstateType: !!(
      this.props.goodsDetail.wxItem && this.props.goodsDetail.wxItem.type === goodsTypeEnum.ESTATE.value
    ), // 是否为楼盘类型商品
    isGiveGift: false, // 是否送礼
    bookingList: false, // 是否配送服务预订
    // goodsInfo:null, //配送的商品详情
    _shareDialog: React.createRef(),
  };

  componentDidMount() {
    wxApi.removeStorageSync('goodsInfo');
    this.getTemplateStyle(); // 获取模板配置
    // if (this.props.isDistributionServices) {
    //   this.setState({
    //     isDistributionServices: true,
    //   });
    // }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.goodsDetail) {
      const itemNo = nextProps.goodsDetail.wxItem?.itemNo ? nextProps.goodsDetail.wxItem.itemNo : null;
      const isEstateType = !!(
        nextProps.goodsDetail.wxItem && nextProps.goodsDetail.wxItem.type === goodsTypeEnum.ESTATE.value
      ); // 是否为楼盘类型
      this.setState({
        scanFlag: nextProps.goodsDetail.scan,
        itemNo, // 商品单号
        wxItem: nextProps.goodsDetail.wxItem || null, // 商品信息
        giftActivityDTO: nextProps.goodsDetail.giftActivityDTO || null, // 活动详情
        itemIntegralShopDTO: nextProps.goodsDetail.itemIntegralShopDTO || null, // 积分商品详情
        isEstateType: isEstateType, // 是否为楼盘类型商品
      });
    }
  }

  // 根据商品类型判断是否在底部显示购买及加入购物车按钮
  isGoodsBuy = () => {
    // 获取当前商品类型
    const goodsType = this.state.wxItem ? this.state.wxItem.type : null;
    // 判断商品类型是否为产品类型或组合商品类型，是则显示购买及加入购物车按钮
    if (goodsType === goodsTypeEnum.PRODUCT.value || goodsType === goodsTypeEnum.COMBINATIONITEM.value) {
      return true;
    }
    return false;
  };

  // 按钮名称
  defaultText = () => {
    let defaultText = '立即购买';
    if (this.state.wxItem?.frontMoneyItem) {
      defaultText = '立即预定';
    }
    if (this.state.wxItem?.drugType) {
      defaultText = '提交需求';
    }
    if (this.props.isDistributionServices) {
      // 新增配送服务的下单文本
      defaultText = '立即下单';
    }
    return defaultText;
  };

  /**
   * 获取模板配置
   */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
   * 错误提示
   * @param {*} msg
   */
  errorTip(msg) {
    Taro.showToast({
      icon: 'none',
      title: msg,
    });
  }

  // 打开分享对话弹框
  handleSelectChanel = () => {
    this.state._shareDialog.current.show();
  };

  // 关闭分享有礼弹窗
  closeDialog = () => {
    this.setState({
      shareGiftShow: false,
    });
  };

  /**
   * 发射事件通知父级调用Canvas
   * 因为Canvas必须放在page里，否则无法保存图片
   */
  handleCanvas = () => {
    this.props.onHandleCanvas?.();
  };

  /**
   * 保存海报
   * 因为Canvas必须放在page里，否则无法保存图片，所以只能页面重新父组件调用该方法
   * @param {*} context 父级页面的this
   */
  handleSavePoster(context) {
    this.state._shareDialog.current.savePosterImage(context);
  }

  /**
   * 分享朋友圈
   */
  onShareAppMessage() {
    report.share(true);
    this.state._shareDialog.current.hide();
    setTimeout(() => {
      this.setState({
        shareGiftShow: true,
      });
    }, 300);
    const name = this.state.wxItem?.name ? this.state.wxItem.name : '';
    let item = this.state.itemNo;
    // 判断是否有商品活动类型及活动ID，是则加上商品活动类型及活动id
    if (this.props.activityType && this.props.activityId) {
      item += '&type=' + this.props.activityType + '&activityId=' + this.props.activityId;
    }
    const url = '/wxat-common/pages/goods-detail/index?itemNo=' + item;
    return {
      title: name,
      path: shareUtil.buildShareUrlPublicArguments(url, shareUtil.ShareBZs.BOTTOM_OPERATION),
    };
  }

  /**
   * 跳转配送服务预订清单页面
   */
  handleBookingList = () => {
    this.showAttrDialog(); // 显示商品属性对话弹框
    // 设置当前操作为加入购物车
    this.setState({
      bookingList: true,
      chooseAttrForCart: false,
    });
  };

  /**
   * 跳转配送服务下单页面
   */
  overDeliveryService() {
    this.showAttrDialog(); // 显示商品属性对话弹框
    // 设置当前操作为加入购物车
    this.setState({
      bookingList: false,
      chooseAttrForCart: false,
    });
  }

  /**
   * 立即购买
   */
  handleBuyNow = (e, isGiveGift = false) => {
    this.setState({ isGiveGift });
    if (this.state.giftActivityDTO) {
      const { wxItem, giftActivityDTO } = this.state;
      const info = {
        name: wxItem.name,
        itemNo: this.state.itemNo,
        barcode: giftActivityDTO.barcode,
        itemCount: 1,
        isShelf: true,
        pic: wxItem.thumbnail,
        salePrice: 0,
        skuTreeNames: giftActivityDTO.attrValCombineName,
        itemType: goodsTypeEnum.GIFT_GOODS.value,
        activityType: goodsTypeEnum.FREE_GOODS_ACTIVITY.value,
        activityId: giftActivityDTO.id,
      };
      this.handlerChooseSku(info); // 选择商品sku属性
    } else {
      // 如果是积分商品
      if (this.state.itemIntegralShopDTO) {
        // 设置限购标题
        this.setState({
          limitAmountTitle: '积分商品',
        });
      }

      if (this.props.isDistributionServices) {
        this.overDeliveryService(); // 配送服务下单逻辑-商品属性对话弹框
      }
      {
        this.showAttrDialog(); // 显示商品属性对话弹框
      }

      report.clickBuy(this.state.itemNo);
      // 设置当前操作为不是加入购物车，即当前操作为购买
      this.setState({
        chooseAttrForCart: false,
      });
    }
  };

  /**
   * 预约客房票务
   */
  handleToReservation = () => {
    const goodsInfo = {
      itemCount: 1,
      wxItem: this.state.wxItem,
    };
    let url = pageLinkEnum.orderPkg.payOrder;
    if (this.state.wxItem?.type === goodsTypeEnum.ROOMS.value) {
      url = '/sub-packages/tourism-package/pages/rooms-info/index';
      protectedMailBox.send('sub-packages/tourism-package/pages/rooms-info/index', 'goodsInfo', goodsInfo);
    } else if (this.state.wxItem?.type === goodsTypeEnum.TICKETS.value) {
      url = '/sub-packages/tourism-package/pages/tickets-info/index';
      protectedMailBox.send('sub-packages/tourism-package/pages/tickets-info/index', 'goodsInfo', goodsInfo);
    }
    wxApi.$navigateTo({
      url: url,
    });
  };

  /**
   * 点击我要咨询
   */
  handleContact = () => {
    report.clickConsult(); // 点击客服咨询埋点事件
  };

  /**
   * 跳转购物车页面
   */
  handlerToCart = () => {
    this.showAttrDialog(); // 显示商品属性对话弹框
    // 设置当前操作为加入购物车
    this.setState({
      chooseAttrForCart: true,
    });
  };

  /**
   * 地产行业立即认筹
   */
  handleBuyEstate = () => {
    const goodsInfo = {
      wxItem: this.state.wxItem, // 商品信息
      activityType: this.props.activityType || null, // 商品活动类型
      activityId: this.props.activityId || null, // 商品活动id
    };
    protectedMailBox.send('sub-packages/estate-package/pages/choice-house/index', 'goodsInfoList', [goodsInfo]);

    wxApi.$navigateTo({
      url: '/sub-packages/estate-package/pages/choice-house/index',
    });
  };

  /**
   * 选择商品sku属性
   * @param {*} info
   */
  handlerChooseSku = (info) => {
    // info =info.detail
    const { activityType, activityId, sessionId, isDistributionServices } = this.props;
    const { itemNo, chooseAttrForCart, itemIntegralShopDTO, giftActivityDTO } = this.state;
    const goodsInfo = info;

    // 埋点参数对象
    const reportParams = {
      sku_num: goodsInfo.skuId || null,
      itemNo: goodsInfo.itemNo || null,
      barcode: goodsInfo.barcode || null,
      product_count: goodsInfo.itemCount || null,
    };

    // 配送服务加入预订清单
    if (goodsInfo.bookingList) {
      const thumbnail = info.thumbnail || (info.wxItem ? info.wxItem.thumbnail : '');
      const data = {
        ...info,
        count: 1,
        thumbnailMin: imageUtils.thumbnailMin(thumbnail),
        thumbnailMid: imageUtils.thumbnailMin(thumbnail),
        skuInfo: info.skuTreeNames,
      };

      const current = wxApi.getStorageSync('storageList');
      let newArr = [];
      if (current && current.length) {
        let item = null;
        if (info.skuId) {
          item = current.find((i) => i.skuId === info.skuId);
        } else {
          item = current.find((i) => i.wxItem.itemNo === info.wxItem.itemNo);
        }

        if (item) {
          item.count = item.count += info.itemCount;
          newArr = current;
        } else {
          data.count = info.itemCount;
          newArr = [data, ...current];
        }
      } else {
        data.count = info.itemCount;
        newArr = [data, ...current];
      }

      wxApi.setStorageSync('storageList', newArr);
      wxApi.setStorageSync('storageOrderList', newArr);
      wxApi.showToast({
        title: '预定成功',
      });

      this.hideAttrDialog(); // 隐藏商品属性对话弹框
      return;
    }
    // 购物车
    if (this.state.chooseAttrForCart) {
      cartHelper.addOrUpdateCart(
        buyHub.formatSpu(goodsInfo.wxItem, goodsInfo.sku),
        goodsInfo.itemCount,
        true,
        (res) => {
          wxApi.showToast({
            title: '已添加到购物车',
          });
        }
      );
      util.sensorsReportData('to_shopping_cart', {
        event_name: '加入购物车',
        prod_item_no: goodsInfo.name || '',
        prod_item_name: goodsInfo.itemNo || '',
        product_count: goodsInfo.count || '',
        prod_sku: goodsInfo.skuId || ''
      })
      report.toShopCart(this.state.itemNo, this.state.sessionId);
    } else {
      report.clickBuyNowProductpage(reportParams);
      util.sensorsReportData('buy_now_product', {
        event_name: '立即购买',
        prod_item_no: goodsInfo.name || '',
        prod_item_name: goodsInfo.itemNo || '',
        product_count: goodsInfo.count || '',
        prod_sku: goodsInfo.skuId || ''
      })
      goodsInfo.reportExt = JSON.stringify({
        sessionId: this.state.sessionId,
      });

      // 促销活动
      if (goodsInfo.wxItem && goodsInfo.wxItem.mpActivityId) {
        goodsInfo.mpActivityId = goodsInfo.wxItem.mpActivityId;
        goodsInfo.mpActivityType = goodsInfo.wxItem.mpActivityType;
        goodsInfo.mpActivityName = goodsInfo.wxItem.mpActivityName;
      }

      // 处方药
      if (goodsInfo.wxItem && goodsInfo.wxItem.drugType) {
        goodsInfo.drugType = goodsInfo.wxItem.drugType;
      }

      const url = pageLinkEnum.orderPkg.payOrder;
      protectedMailBox.send(url, 'goodsInfoList', [goodsInfo]);

      if (this.state.isGiveGift) {
        const params = { ...info };
        delete params.wxItem;

        // return
        wxApi.$navigateTo({
          url: '/sub-packages/marketing-package/pages/give-gift/create/index',
          data: {
            params: JSON.stringify(params),
          },
        });
      } else if (this.state.itemIntegralShopDTO) {
        // 判断是否为积分商品，
        const limitItemCount = this.state.itemIntegralShopDTO.exchangeRestrict;
        // 判断购买数量是否大于限购数量
        if (limitItemCount && goodsInfo.itemCount > limitItemCount) {
          Taro.showToast({
            title: `每人限制兑换${limitItemCount}件`,
            icon: 'none',
          });

          return;
        }
        const orderRequestPromDTO = {
          integralNo: this.props.activityId,
        };
        wxApi.$navigateTo({
          url: url,
          data: {
            payDetails: encodeURI(
              JSON.stringify([
                {
                  name: 'freight',
                },

                {
                  name: 'integral',
                },
              ])
            ),

            params: encodeURI(
              JSON.stringify({
                orderRequestPromDTO: orderRequestPromDTO,
              })
            ),
          },
        });
      } else if (this.state.giftActivityDTO) {
        // 判断是否为赠品
        const payDetails = [
          {
            name: 'freight',
          },

          {
            name: 'giftActivity',
          },
        ];
        wxApi.$navigateTo({
          url: url,
          data: {
            payDetails: encodeURI(JSON.stringify(payDetails)),
          },
        });
      } else if (this.props.activityType == goodsTypeEnum.INTERNAL_BUY.value.toString()) {
        // 判断是否为内购商品
        const orderRequestPromDTO = {
          innerBuyNo: this.props.activityId,
        };

        wxApi.$navigateTo({
          url: url,
          data: {
            payDetails: encodeURI(
              JSON.stringify([
                {
                  name: 'freight',
                },

                {
                  name: 'innerBuy',
                },
              ])
            ),

            params: encodeURI(
              JSON.stringify({
                orderRequestPromDTO: orderRequestPromDTO,
              })
            ),
          },
        });
      } else if (isDistributionServices) {
        const newArr = [];
        newArr.push(goodsInfo);
        Taro.setStorageSync('goodsInfo', newArr);
        // 判断是否需要扫码
        if (!this.state.scanFlag) {
          wxApi.$navigateTo({
            url: '/sub-packages/order-package/pages/pay-order/index',
            data: {
              isDistributionServices: true,
              title: '确认订单',
            },
          });

          return;
        }

        if (storeData.globalData.roomCode) {
          wxApi.$navigateTo({
            url: '/sub-packages/order-package/pages/pay-order/index',
            data: {
              roomCode: storeData.globalData.roomCode,
              roomCodeTypeId: storeData.globalData.roomCodeTypeId,
              roomCodeTypeName: storeData.globalData.roomCodeTypeName,
              trueCode: storeData.globalData.roomCodeTypeName + '-' + storeData.globalData.roomCode,
              isDistributionServices: true,
              title: '确认订单',
              categoryType: this.props.isDistributionServices ? 5 : null,
            },
          });
        } else {
          // if (process.env.WX_OA === 'true') { // 公众号
          //   this.contactNewScanData(goodsInfo)
          // } else { // 小程序
          wxApi.$navigateTo({
            url: '/sub-packages/marketing-package/pages/living-service/scan-code/index',
            data: {
              isDistributionServices: true,
            },
          });
          // }
        }
      } else {
        wxApi.$navigateTo({
          url: url,
        });
      }
    }
    this.hideAttrDialog(); // 隐藏商品属性对话弹框
  };

  /* 扫码数据缓存处理 */
  contactNewScanData(info) {
    const data = {
      ...info,
      count: info.itemCount,
      scan: true,
      thumbnailMin: info.pic,
      thumbnailMid: info.pic,
      skuInfo: info.skuTreeNames,
    };

    const newArr = [data];
    Taro.setStorageSync('storageList', newArr);
    Taro.setStorageSync('storageOrderList', newArr); // 下单缓存
    wxApi.$navigateTo({
      url: '/sub-packages/order-package/pages/pay-order/index',
      data: {
        categoryType: 5,
        needScan: true,
        title: '确认订单',
      },
    });
  }

  /**
   * 显示商品属性对话弹框
   */
  showAttrDialog() {
    this.chooseAttributeDialogCOMPT && this.chooseAttributeDialogCOMPT.showAttributDialog();
  }

  /**
   * 隐藏商品属性对话弹框
   */
  hideAttrDialog() {
    this.chooseAttributeDialogCOMPT && this.chooseAttributeDialogCOMPT.hideAttributeDialog();
  }

  refChooseAttributeDialogCOMPT = (node) => (this.chooseAttributeDialogCOMPT = node);

  render() {
    const {
      wxItem,
      industry,
      industryEnum,
      itemIntegralShopDTO,
      giftActivityDTO,
      goodsTypeEnum,
      isEstateType,
      shareGiftShow,
      bookingList,
      limitAmountTitle,
    } = this.state;
    const { goodsDetail, activityType, uniqueShareInfoForQrCode, isDistributionServices } = this.props;
    return (
      <View>
        {wxItem && (
          <View
            data-fixme='02 block to view. need more test'
            data-scoped='wk-wcgb-BottomOperation'
            className='wk-wcgb-BottomOperation'
          >
            <View>{this.isGoodsBuy()}</View>
            <View>
              {/* {false && (
            <BuyNow
              operationText={wxItem.itemStock <= 0 ? '库存不足' : '已下架'}
              disable={wxItem.itemStock <= 0 || wxItem.isShelf === 0}
              btnType="customerService"
              immediateShare={false}
              showCart={industry === industryEnum.type.retail.value}
              onShare={this.handleSelectChanel}
              onShoppingCart={this.handlerToCart}
              onBuyNow={this.handleBuyNow}
            ></BuyNow>
          )} */}

              {/*  积分商品底部购买区域模块  */}
              {itemIntegralShopDTO ? (
                <BuyNow
                  operationText={wxItem.itemStock <= 0 ? '库存不足' : '已下架'}
                  defaultText='立即兑换'
                  disable={wxItem.itemStock <= 0 || wxItem.isShelf === 0}
                  btnType='customerService'
                  immediateShare={false}
                  showCart={false}
                  onShare={this.handleSelectChanel}
                  onBuyNow={this.handleBuyNow}
                ></BuyNow>
              ) : giftActivityDTO ? (
                <BuyNow
                  operationText={wxItem.itemStock <= 0 ? '库存不足' : '已下架'}
                  defaultText='立即领取'
                  disable={wxItem.itemStock <= 0 || wxItem.isShelf === 0}
                  btnType='customerService'
                  immediateShare={false}
                  showCart={false}
                  onShare={this.handleSelectChanel}
                  onBuyNow={this.handleBuyNow}
                ></BuyNow>
              ) : activityType == goodsTypeEnum.INTERNAL_BUY.value ? (
                <BuyNow
                  operationText={wxItem.itemStock <= 0 ? '库存不足' : '已下架'}
                  defaultText='员工价购买'
                  disable={wxItem.itemStock <= 0 || wxItem.isShelf === 0}
                  btnType='customerService'
                  immediateShare={false}
                  showCart={false}
                  onShare={this.handleSelectChanel}
                  onBuyNow={this.handleBuyNow}
                ></BuyNow>
              ) : wxItem.type == goodsTypeEnum.ROOMS.value || wxItem.type == goodsTypeEnum.TICKETS.value ? (
                <BuyNow
                  defaultText='预订'
                  disable={wxItem.isShelf === 0}
                  btnType='customerService'
                  immediateShare={false}
                  showCart={false}
                  onShare={this.handleSelectChanel}
                  onBuyNow={this.handleToReservation}
                ></BuyNow>
              ) : isEstateType ? (
                <BuyNow
                  operationText='立即认筹'
                  defaultText='立即认筹'
                  btnType='customerService'
                  immediateShare={false}
                  showCart={false}
                  showContact
                  onShare={this.handleSelectChanel}
                  onContact={this.handleContact}
                  onBuyNow={this.handleBuyEstate}
                ></BuyNow>
              ) : this.isGoodsBuy() && !isDistributionServices ? (
                <BuyNow
                  operationText={wxItem.itemStock <= 0 ? '库存不足' : '已下架'}
                  defaultText={this.defaultText()}
                  disable={wxItem.itemStock <= 0 || wxItem.isShelf === 0}
                  btnType='customerService'
                  immediateShare={false}
                  showCart={
                    !wxItem.frontMoneyItem &&
                    (industry === industryEnum.type.retail.value || industry === industryEnum.type.medicine.value)
                  }
                  onShare={this.handleSelectChanel}
                  onShoppingCart={this.handlerToCart}
                  onBuyNow={this.handleBuyNow}
                ></BuyNow>
              ) : (
                this.isGoodsBuy() &&
                isDistributionServices && (
                  <BuyNow
                    showCart={false}
                    operationText={wxItem.itemStock <= 0 ? '库存不足' : '已下架'}
                    defaultText={this.defaultText()}
                    isDistributionServices={isDistributionServices}
                    disable={wxItem.itemStock <= 0 || wxItem.isShelf === 0}
                    btnType='customerService'
                    immediateShare={false}
                    onShare={this.handleSelectChanel}
                    onBookingList={this.handleBookingList}
                    onBuyNow={this.handleBuyNow}
                  ></BuyNow>
                )
              )}

              {/*  赠品底部购买区域模块  */}
              {/*  内购底部购买区域模块  */}
              {/*  客房票务底部购买区域模块  */}
              {/*  地产房源底部操作按钮  */}
              {/*  底部购买区域模块  */}
              {/*  配送服务底部购买模块  */}
            </View>
            {/* 分享有礼 */}
            {shareGiftShow && <ShareGift onCloseDialog={this.closeDialog}></ShareGift>}

            {/*  选择商品属性对话弹框  */}
            {goodsDetail && (
              <ChooseAttributeDialog
                bookingList={bookingList}
                isDistributionServices={isDistributionServices}
                ref={this.refChooseAttributeDialogCOMPT}
                onChoose={this.handlerChooseSku}
                limitAmountTitle={limitAmountTitle}
                goodsDetail={goodsDetail}
              ></ChooseAttributeDialog>
            )}

            {/*  分享对话弹框  */}
            <ShareDialog
              key='share-dialog'
              childRef={this.state._shareDialog}
              qrCodeParams={null}
              showLabelPrice
              posterTips='向您推荐了这个商品'
              posterSalePriceLabel='优惠价'
              posterLabelPriceLabel='零售价'
              posterName={wxItem.name}
              posterItemNo={wxItem.itemNo}
              posterImage={wxItem.thumbnail}
              salePrice={wxItem.skuSaleLowPrice || filters.moneyFilter(wxItem.salePrice, true)}
              labelPrice={wxItem.skuLabelHighPrice || filters.moneyFilter(wxItem.labelPrice, true)}
              uniqueShareInfoForQrCode={uniqueShareInfoForQrCode}
              onSave={this.handleCanvas}
            ></ShareDialog>
          </View>
        )}
      </View>
    );
  }
}

export default BottomOperation;
