import { _safe_style_ } from '@/wk-taro-platform';
import { Block, View, Text, Image } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../utils/money.wxs.js';
import wxApi from '../../../utils/wxApi.js';
import api from '../../../api/index.js';
import template from '../../../utils/template.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import industryEnum from '../../../constants/industryEnum.js';
import state from '../../../../state/index.js';
import date from '../../../utils/date.js';
import BottomDialog from '../../bottom-dialog/index';
import CombinationItemDetail from '../../combination-item-detail/index';
import StoreModule from '../../decorate/storeModule/index';
import FrontMoneyItem from '../../front-money-item/index';
import './index.scss';
const app = Taro.getApp();

class GoodsInfo extends React.Component {

  state = {
    tmpStyle: {}, // 主题模板
    goodsTypeEnum, // 商品类型常量
    industryEnum, // 行业类型常量
    industry: app.globalData.industry, // 当前行业类型
    wxItem: null, // 商品信息
    giftActivityDTO: null, // 活动详情
    deliveryAddress: null, //商品发货地址
    isEstateType: false, // 是否为楼盘类型商品
  }

  deliveryTime = () => {
    const wxItem = this.state.wxItem; // 商品详情
    let dt = null;

    if (wxItem) {
      const preSell = wxItem.preSell; // 是否预售
      const deliveryTimeType = wxItem.deliveryTimeType; // 发货时间类型 0：固定时间，1：购买后几天范围内发货
      const deliveryTime = wxItem.deliveryTime; // 固定发货时间
      const daysAfterBuyRange = wxItem.daysAfterBuyRange; // 购买后几天范围内发货
      if (deliveryTimeType === 0 && deliveryTime && preSell) {
        dt = date.format(new Date(deliveryTime), 'yyyy/MM/dd');
      } else if (deliveryTimeType === 1 && daysAfterBuyRange) {
        dt = daysAfterBuyRange + '天';
      }
    }
    return dt
  }

  componentDidShow() {
    this.getTemplateStyle(); // 获取模板配置

    this.setState({
      industry: app.globalData.industry, // 行业类型常量
    });
    this.getItemWarehouseInfo(this.state.skuBarcode || this.state.wxItem.barcode); // 获取商品仓库信息
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.goodsDetail) {
      const isEstateType = nextProps.goodsDetail.wxItem && nextProps.goodsDetail.wxItem.type === goodsTypeEnum.ESTATE.value ? true : false; // 是否为楼盘类型
      this.setState({
        wxItem: nextProps.goodsDetail.wxItem || null, // 商品信息
        giftActivityDTO: nextProps.goodsDetail.giftActivityDTO || null, // 活动详情
        isEstateType: isEstateType, // 是否为楼盘类型商品
      });
    }
  }

  /**
  * 获取模板配置
  */
  getTemplateStyle() {
   const templateStyle = template.getTemplateStyle();
   this.setState({
     tmpStyle: templateStyle,
   });
  }

  // 获取优惠对话框
  showChoseDialog = () => {
    if (!this._choseDialog) {
      this._choseDialog = this.selectComponent('#choose-promotion');
    }
    this._choseDialog.showModal();
  }

  colseChoseDialog = () => {
    if (!this._choseDialog) {
      this._choseDialog = this.selectComponent('#choose-promotion');
    }
    this._choseDialog.hideModal();
  }

  /**
  * 获取商品仓库信息
  * @param {*} barcode
  */
   getItemWarehouseInfo(barcode) {
    // 判断是否为楼盘类型商品，是则不需要获取商品仓库信息
    if (this.state.isEstateType) {
      return
    }

    wxApi
      .request({
        url: api.goods.itemWarehouseInfo,
        quite: true,
        loading: false,
        data: {
          barcode: barcode || null,
          storeId: this.state.wxItem.storeId || null,
          provinceId: state.base.currentStore.provinceId || null,
          cityId: state.base.currentStore.cityId || null,
          districtId: state.base.currentStore.districtId || null,
        },
      })
      .then((res) => {
        const data = res.data;
        if (!!data) {
          const deliveryAddress = data[0].province + data[0].city;
          this.setState({
            deliveryAddress: deliveryAddress,
          });
        }
      })
  }

  render() {
    const {
      tmpStyle,
      giftActivityDTO,
      wxItem,
      goodsTypeEnum,
      isEstateType,
      deliveryAddress,
      promotionList,
    } = this.state;
    return (
      <View data-fixme="02 block to view. need more test" data-scoped="wk-wcgg-GoodsInfo" className="wk-wcgg-GoodsInfo">
        <View className="goods-info">
          <View className="goods-name">
            {giftActivityDTO && (
              <Text className="gift-icon" style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                赠品
              </Text>
            )}

            <View>
              {wxItem.drugType && (
                <Text className="drug-tag" style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                  处方药
                </Text>
              )}

              <Text>{wxItem.name}</Text>
              {giftActivityDTO && giftActivityDTO.attrValCombineName && (
                <Text>{'(' + giftActivityDTO.attrValCombineName + ')'}</Text>
              )}
            </View>
          </View>
          {/*  商品副标题  */}
          {wxItem.subName && <View className="goods-sub-name">{wxItem.subName}</View>}

          {/*  楼盘标签  */}
          {wxItem.labelNames && wxItem.labelNames.length > 0 && (
            <View className="label-box">
              {wxItem.labelNames &&
                wxItem.labelNames.map((item, index) => {
                  return (
                    <Text className="label" key={item.index}>
                      {item}
                    </Text>
                  );
                })}
            </View>
          )}

          {/*  价格与销量区域  */}
          <View className="price-count-box">
            <View className="price-box">
              {this.props.goodsDetail.itemIntegralShopDTO ? (
                <View className="sale-price integral-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  {wxItem.exchangeIntegral + this.props.scoreName + '+' + filters.moneyFilter(wxItem.salePrice, true) + '元'}
                </View>
              ) : giftActivityDTO ? (
                <View className="gift-label-box">
                  <View
                    className="gift-label"
                    style={_safe_style_('background: ' + tmpStyle.bgColor + ';color: ' + tmpStyle.btnColor)}
                  >
                    {giftActivityDTO.levelName}
                  </View>
                  {giftActivityDTO.everyoneLimitCount && (
                    <View
                      className="gift-label"
                      style={_safe_style_('background: ' + tmpStyle.bgColor + ';color: ' + tmpStyle.btnColor)}
                    >
                      {'限领' + giftActivityDTO.everyoneLimitCount + '件'}
                    </View>
                  )}
                </View>
              ) : this.props.activityType == goodsTypeEnum.INTERNAL_BUY.value ? (
                <View className="sale-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                  <Text>¥</Text>
                  <Text className="price">{wxItem.innerBuyPriceRange}</Text>
                </View>
              ) : isEstateType ? (
                <View className="estate-price-box">
                  {wxItem.averagePrice && (
                    <View className="estate-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      <Text>约</Text>
                      <Text className="price">{filters.moneyFilter(wxItem.averagePrice, true)}</Text>
                      <Text>元/㎡</Text>
                    </View>
                  )}

                  <View className="estate-price-tips">
                    {wxItem.averagePrice && wxItem.averagePrice != 0 ? '参考均价' : '暂无参考价'}
                  </View>
                </View>
              ) : (
                <View className="goods-price">
                  {!this.props.isDistributionServices && (
                    <View className="sale-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      <Text>¥</Text>
                      <Text className="price">
                        {wxItem.salePriceRange || filters.moneyFilter(wxItem.salePrice, true)}
                      </Text>
                    </View>
                  )}

                  {this.props.isDistributionServices && (
                    <View className="sale-price" style={_safe_style_('color:' + tmpStyle.btnColor)}>
                      <Text>¥</Text>
                      <Text className="price">
                        {filters.moneyFilter(wxItem.salePrice, true) || wxItem.salePriceRange}
                      </Text>
                    </View>
                  )}

                  {/*  标价  */}
                  {wxItem.skuSaleLowPrice < wxItem.skuLabelHighPrice && wxItem.salePrice < wxItem.labelPrice && (
                    <View className="label-price">
                      {'¥' + (wxItem.skuLabelHighPrice || filters.moneyFilter(wxItem.labelPrice, true))}
                    </View>
                  )}
                </View>
              )}

              {/*  赠品  */}
              {/*  内购商品  */}
              {/*  楼盘价格信息  */}
              {/*  普通商品  */}
            </View>
            {/*  销量模块  */}
            {!isEstateType && (
              <View className="count-box">
                {giftActivityDTO ? (
                  <View className="count">{'已兑换' + giftActivityDTO.receivedCount + '件'}</View>
                ) : (
                  <View className="count">{'已售' + wxItem.itemSalesVolume + '件'}</View>
                )}

                {/*  已售  */}
              </View>
            )}
          </View>
          {/*  商品介绍描述信息  */}
          <View className="desc-info">
            {wxItem.type === goodsTypeEnum.ROOMS.value && (
              <Block>
                <View className="label-box">
                  <Text className="label">{wxItem.hotelType ? '钟点房' : '普通房'}</Text>
                </View>
                <View className="desc-box">
                  <Text className="desc-title">床型</Text>
                  <Text className="desc-text">{wxItem.hotelBedSize || '-'}</Text>
                </View>
                <View className="desc-box">
                  <Text className="desc-title">房间设施</Text>
                  <Text className="desc-text">{wxItem.hotelFacility || '-'}</Text>
                </View>
              </Block>
            )}

            {/*  票务独有信息  */}
            {wxItem.type === goodsTypeEnum.TICKETS.value && (
              <Block>
                <View className="label-box">
                  <Text className="label">{wxItem.ticketType ? '套餐票' : '单日票'}</Text>
                </View>
                <View className="desc-box">
                  <Text className="desc-title">特色</Text>
                  {wxItem.labels &&
                    wxItem.labels.map((label, index) => {
                      return (
                        <Text className="desc-text" key={label.index}>
                          {label}
                        </Text>
                      );
                    })}
                </View>
              </Block>
            )}
          </View>
          {/*  处方提示  */}
          {wxItem.drugType && (
            <View className="drug-tip">本品为处方药，需提交个人信息和处方信息，医生审核通过后发货</View>
          )}

          {/*  发货信息  */}
          {(deliveryAddress || this.deliveryTime()) && (
            <View className="goods-delivery">
              <Text className="delivery-label">发货</Text>
              {deliveryAddress && <Text className="delivery-address">{deliveryAddress}</Text>}

              {this.deliveryTime() && <Text className="delivery-time">{'预计' + this.deliveryTime() + '发货'}</Text>}
            </View>
          )}

          {/*  品牌信息  */}
          {wxItem.brand && (
            <View className="goods-brand-box">
              <Text>品牌</Text>
              <Text className="goods-brand">{wxItem.brand}</Text>
            </View>
          )}

          {/*  <View class="goods-hr"></View>
                <View class="tag-box">
                  <View class="tag-item">
                    <image src="../../images/goods/authentication.png"></image>
                    <text>认证企业</text>
                  </View>
                  <View class="tag-item">
                    <image src="../../images/goods/authentication.png"></image>
                    <text>支持改期</text>
                  </View>
                  <View class="tag-item">
                    <image src="../../images/goods/authentication.png"></image>

                    <text>极速退款</text>
                  </View>
                </View>
                <View class="goods-hr"></View>  */}
          {/*  店铺列表  */}
          {/*  <View class="goods-hr"></View>
          <store-module padding="0" />
          <View class="goods-hr"></View>  */}
        </View>
        {/*  定金模块  */}
        {wxItem.frontMoneyItem && <FrontMoneyItem frontMoney={wxItem.frontMoney}></FrontMoneyItem>}

        {/*  促销优惠  */}
        {this.props.promotionList && this.props.promotionList.length && (
          <View className="coupon-box" onClick={this.showChoseDialog}>
            <View className="coupon-title">优惠</View>
            <View className="promotion-box">
              {this.props.promotionList &&
                this.props.promotionList.map((item, index) => {
                  return (
                    <Text className="promotion-item" key={index}>
                      {(index > 0 ? '、' : '') + item.ruleName}
                    </Text>
                  );
                })}
            </View>
            <Image
              className="right-arrow"
              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
            ></Image>
          </View>
        )}

        {/*  灰色间隔  */}
        <View className="gray"></View>
        {(wxItem.sourceType == 1 || wxItem.sourceType == 4) && (
          <View className="xsd-cartBox">
            <Image
              className="xsd-cart"
              mode="aspectFit"
              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/zysp.svg"
            ></Image>
            <Text className="xsd-cartText">品牌自营商品，现货</Text>
          </View>
        )}

        {(wxItem.sourceType == 1 || wxItem.sourceType == 4) && <View className="gray"></View>}

        {/*  组合商品明细  */}
        {wxItem.combinationDTOS && wxItem.combinationDTOS.length > 0 && (
          <CombinationItemDetail combinationDtos={wxItem.combinationDTOS}></CombinationItemDetail>
        )}

        {wxItem.combinationDTOS && wxItem.combinationDTOS.length > 0 && <View className="gray"></View>}

        {/*  优惠对话框  */}
        <BottomDialog id="choose-promotion" customClass="bottom-dialog-custom-class">
          <View className="promotion-title">优惠</View>
          <Image
            className="promotion-close"
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/line-close.png"
            onClick={this.colseChoseDialog}
          ></Image>
          {/*  优惠类型  */}
          <View className="promotion">
            {this.props.promotionList &&
              this.props.promotionList.map((item, index) => {
                return <Text key={index}>{(index > 0 ? '、' : '') + item.ruleName}</Text>;
              })}
          </View>
        </BottomDialog>
      </View>
    );
  }
}

export default GoodsInfo;
