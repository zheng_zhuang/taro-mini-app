import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { Block, View, Text, Image, Map, Input } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import filters from '../../../utils/money.wxs.js';
import wxApi from '../../../utils/wxApi.js';
import api from '../../../api/index.js';
import cdnResConfig from '../../../constants/cdnResConfig.js';
import protectedMailBox from '../../../utils/protectedMailBox.js';
import template from '../../../utils/template.js';
import authHelper from '../../../utils/auth-helper.js';
import report from '../../../sdks/buried/report/index.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';

import AnimatDialog from '../../animat-dialog/index';
import './index.scss';
const phoneRules = {
  reg: /^1[3|4|5|6|8|7][0-9]\d{8}$/,
  message: '手机号码格式不正确',
};

class EstateDetail extends React.Component {

  state = {
    tmpStyle: {}, // 主题模板
    estateImages: cdnResConfig.estate, // cdn静态资源图片

    wxItem: null, // 商品详情

    estateInfoList: null, // 楼盘详情列表
    estateInfoMore: false, // 是否展示更多楼盘详情

    contactList: null, // 预约及通知等列表
    contactType: null, // 通知类型，用于显示不同的通知对话弹框
    phone: null, // 用户输入的手机号码，用于变价通知及开盘通知

    layoutList: null, // 主力户型列表

    adviserList: null, // 置业顾问列表

    // 项目地点的地图信息
    mapInfo: null,

    pageEnterTime: null, // 进入页面时间
    pageLeaveTime: null, // 离开页面时间
  }

  // 在组件实例进入页面节点树时执行
  componentDidMount() {
    this.getTemplateStyle(); // 获取模板配置
    this.getLayoutList(); // 获取主力户型列表
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.goodsDetail) {
      this.setState({
        wxItem: nextProps.goodsDetail.wxItem || null,
        adviserList: nextProps.goodsDetail.wxItem && nextProps.goodsDetail.wxItem.staffList ? nextProps.goodsDetail.wxItem.staffList : null,
      })
      // 格式化数据
      this.formatData()
    }
  }

  /**
  * 获取模板配置
  */
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  /**
  * 错误提示
  * @param {*} msg
  */
  errorTip(msg) {
    Taro.showToast({
      icon: 'none',
      title: msg,
    });
  }

  /**
  * 格式化数据
  * 格式化父组件传过来的参数为本组件展示的格式
  */
  formatData() {
    const wxItem = this.state.wxItem || null;
    if (wxItem) {
      let estateInfoList = []; // 楼盘详情列表
      let list = [
        {
          label: '建筑面积',
          key: 'buildArea',
          value: null,
        },

        {
          label: '地址',
          key: 'address',
          value: null,
        },

        {
          label: '开盘时间',
          key: 'openingTime',
          value: null,
        },

        {
          label: '产品类型',
          key: 'estateType',
          value: null,
        },

        {
          label: '开发商',
          key: 'developers',
          value: null,
        },

        {
          label: '售楼电话',
          key: 'salesCall',
          value: null,
        },

        {
          label: '容积率',
          key: 'volumeRatio',
          value: null,
        },

        {
          label: '绿化率',
          key: 'greenRatio',
          value: null,
        },

        {
          label: '预售许可证',
          key: 'fyszd',
          value: null,
        },

        {
          label: '楼盘简介',
          key: 'profiles',
          value: null,
        },
      ];

      // 循环判断详情列表中的参数，进行格式转化，有值的参数添加到渲染的楼盘详情列表中
      list.forEach((item) => {
        if (wxItem.hasOwnProperty(item.key) && (wxItem[item.key] || wxItem[item.key] === 0)) {
          if (item.key === 'buildArea') {
            // 建筑面积
            item.value = wxItem[item.key] + '/㎡';
          } else if (item.key === 'greenRatio') {
            // 绿化率
            item.value = wxItem[item.key] + '%';
          } else if (item.key === 'openingTime') {
            // 开盘时间
            // 开盘时间数据类型转化
            let time = new Date(wxItem[item.key]);
            let year = time.getFullYear();
            let month =
              parseInt(time.getMonth() + 1) < 10
                ? '0' + parseInt(time.getMonth() + 1)
                : parseInt(time.getMonth() + 1);
            let day = time.getDate() < 10 ? '0' + time.getDate() : time.getDate();
            item.value = year + '-' + month + '-' + day;
          } else {
            item.value = wxItem[item.key];
          }

          // 添加有值的参数渲染的楼盘详情列表中
          estateInfoList.push(item);
        }
      });

      // 项目地点的地图信息
      let mapInfo = {
        latitude: wxItem.latitude,
        longitude: wxItem.longitude,
        addressName: wxItem.name,
        address: wxItem.address,
        markers: [
          {
            iconPath: '/images/icon-path.png',
            id: 0,
            latitude: wxItem.latitude,
            longitude: wxItem.longitude,
            width: 20,
            height: 33,
          },
        ],
      };

      let contactList = wxItem.smsTemplateDTOS || [];
      contactList.forEach((item) => {
        if (item.smsTemName.indexOf('开盘') != -1) {
          item.image = 'https://front-end-1302979015.file.myqcloud.com/images/c/images/logo-open.png';
          item.type = 'open';
        } else if (item.smsTemName.indexOf('变价') != -1) {
          item.image = 'https://front-end-1302979015.file.myqcloud.com/images/c/images/logo-variable.png';
          item.type = 'variable';
        }
      });

      this.setState({
        estateInfoList, // 楼盘详情列表
        contactList, // 预约及通知等列表
        mapInfo, // 项目地点的地图信息
      });
    }
  }

  /**
  * 获取主力户型列表
  */
  getLayoutList() {
   wxApi
     .request({
       url: api.estate.layoutList,
       data: {
         estateItemNo: this.state.wxItem.itemNo || null,
       },
     })
     .then((res) => {
       this.setState({
         layoutList: res.data || [],
       });
     })
  }

  /**
  * 展示更多楼盘信息
  */
  handleMoreInfo = () => {
    const estateInfoMore = this.state.estateInfoMore;
    this.setState({
      estateInfoMore: !estateInfoMore,
    });
  }

  /**
  * 预约看房
  */
  handleAppointment() {
    if (!authHelper.checkAuth()) {
      return;
    }

    wxApi.$navigateTo({
      url: '/sub-packages/realEstate-package/pages/appointment/index',
    });
  }

  /**
  * 打开通知对话弹框
  * @param {*} e
  */
  handleShowDialog = (e) => {
    if (!authHelper.checkAuth()) {
      return;
    }

    this.setState({
      contactType: e.currentTarget.dataset.contactType || null,
    });

    this.selectComponent('#animat-dialog').show({
      scale: 1,
    });
  }

  /**
  * 关闭对话弹框
  */
  handleCloseDialog = () => {
    this.setState({
      contactType: null,
      phone: null,
    });

    this.selectComponent('#animat-dialog').hide();
  }

  /**
  * 输入手机号码
  * @param {*} e
  */
  phoneInput = (e) => {
    this.setState({
      phone: e.detail.value,
    });
  }

  /**
  * 保存变价通知及开盘通知
  */
  onDialogSave = () => {
    if (!this.state.phone) {
      return this.errorTip('请输入您的手机号码');
    } else if (!phoneRules.reg.test(this.state.phone)) {
      return this.errorTip(phoneRules.message);
    }

    const contact = this.state.contactList.find((item) => {
      return item.type === this.state.contactType;
    });

    const params = {
      userPhone: this.state.phone,
      smsTemId: contact.smsTemId,
      smsTemName: contact.smsTemName,
      itemNo: this.state.wxItem.itemNo || null,
      itemName: this.state.wxItem.name || null,
    };

    wxApi
      .request({
        url: api.estate.subSms,
        method: 'POST',
        loading: true,
        data: params,
      })
      .then((res) => {
        Taro.showToast({
          icon: 'none',
          title: '提交成功',
        });
      })
      .finally(() => {
        this.handleCloseDialog(); // 关闭对话弹框
      });
  }

  /**
  * 户型详情
  * @param {*} e
  */
    handleLayoutDetail = (e) => {
     const goodsInfo = {
       currentLayout: e.currentTarget.dataset.layout || null, // 当前户型
       wxItem: this.state.wxItem, // 商品信息
       activityType: this.props.activityType || null, // 商品活动类型
       activityId: this.props.activityId || null, // 商品活动id
       uniqueShareInfoForQrCode: this.props.uniqueShareInfoForQrCode || null, // 海报小程序二维码参数
     };
     protectedMailBox.send('sub-packages/estate-package/pages/layout-detail/index', 'goodsInfoList', [goodsInfo]);

     wxApi.$navigateTo({
       url: '/sub-packages/estate-package/pages/layout-detail/index',
     });
  }

  /**
  * 查看更多置业顾问
  */
  handleAdviserList = () => {
    const adviserListInfo = {
      allowChoice: false, // 是否允许选择
      adviserList: this.state.wxItem.staffList || null,
    };

    protectedMailBox.send(
      'sub-packages/estate-package/pages/adviser/adviser-list/index',
      'adviserListInfo',
      adviserListInfo
    );

    wxApi.$navigateTo({
      url: '/sub-packages/estate-package/pages/adviser/adviser-list/index',
    });
  }

  /**
  * 拨打电话
  */
    handlePhoneCall = (e) => {
     const phone = e.currentTarget.dataset.phone;
     Taro.makePhoneCall({
       phoneNumber: phone,
     });
  }

  /**
  * 点击地图跳转楼盘周边页面
  */
  handleClickMap = () => {
    const aroundInfo = {
      // 当前项目地点在地图的信息
      mapInfo: {
        latitude: this.state.mapInfo.latitude || null,
        longitude: this.state.mapInfo.longitude || null,
        addressName: this.state.mapInfo.addressName || null,
        address: this.state.mapInfo.address || null,
      },

      aroundType: ['交通', '学校', '医疗', '生活'], // 周边功能类型列表
    };
    protectedMailBox.send('sub-packages/map-package/pages/around/index', 'aroundInfo', aroundInfo);

    wxApi.$navigateTo({
      url: '/sub-packages/map-package/pages/around/index',
    });
  }

  /**
  * 浏览地产商品埋点事件
  */
  pageviewIn = () => {
    const wxItem = this.state.goodsDetail && this.state.goodsDetail.wxItem ? this.state.goodsDetail.wxItem : {};
    // 是否为楼盘类型商品
    const isEstateType = wxItem.type && wxItem.type == goodsTypeEnum.ESTATE.value ? true : false;
    if (!isEstateType) {
      return;
    }

    // 设置进入页面时间
    this.setState({
      pageEnterTime: new Date().getTime(),
    });

    const params = {
      page_title: '楼盘详情', // 页面标
      page_type: 'estate', // 页面类型
      item_id: wxItem.id || null, // 地产-商品(楼盘)id
      item_no: wxItem.itemNo || null, // 地产-商品(楼盘)no
      item_name: wxItem.name || null, // 地产-商品(楼盘)名称

      // 清除空数据
    };
    Object.keys(params).forEach((key) => {
      if (!params[key] || params[key] === '' || params[key].length < 1) {
        delete params[key];
      }
    });
    report.pageviewIn(params);
  }

  /**
  * 用户退出页面埋点事件
  */
  pageviewOut = () => {
    const wxItem = this.props.goodsDetail && this.props.goodsDetail.wxItem ? this.props.goodsDetail.wxItem : {};
    // 是否为楼盘类型商品
    const isEstateType = wxItem.type && wxItem.type == goodsTypeEnum.ESTATE.value ? true : false;
    if (!isEstateType) {
      return;
    }

    let params = {
      page_title: '楼盘详情', // 页面标
      page_type: 'estate', // 页面类型
      item_id: wxItem.id || null, // 地产-商品(楼盘)id
      item_no: wxItem.itemNo || null, // 地产-商品(楼盘)no
      item_name: wxItem.name || null, // 地产-商品(楼盘)名称

      // 设置离开页面时间
    };
    this.setState({
      pageLeaveTime: new Date().getTime(),
    });

    // 计算页面停留时长(毫秒)
    params.stay_dur = null;
    if (this.state.pageEnterTime && this.state.pageLeaveTime) {
      params.stay_dur = this.state.pageLeaveTime - this.state.pageEnterTime;
    }

    // 清除空数据
    Object.keys(params).forEach((key) => {
      if (!params[key] || params[key] === '' || params[key].length < 1) {
        delete params[key];
      }
    });
    report.pageviewOut(params);
  }

  render() {
    const {
      estateInfoMore,
      estateInfoList,
      contactList,
      layoutList,
      adviserList,
      mapInfo,
      phone,
      tmpStyle,
      contactType,
    } = this.state;
    return (
      <View
        data-fixme="02 block to view. need more test"
        data-scoped="wk-wcge-EstateDetail"
        className="wk-wcge-EstateDetail"
      >
        <View className="estate-detail">
          {estateInfoList && estateInfoList.length > 0 && (
            <View className="tab">
              <View className="tab-top">
                <View className="title">项目详情</View>
              </View>
              <View className={'estate-info-box ' + (estateInfoMore ? 'estate-info-more' : '')}>
                {estateInfoList &&
                  estateInfoList.map((item, index) => {
                    return (
                      <View className="estate-tab" key={item.index}>
                        <Text className="estate-lebal">{item.label}</Text>
                        <Text className={'estate-text ' + (item.key === 'address' ? 'limit-line line-2' : '')}>
                          {item.value}
                        </Text>
                      </View>
                    );
                  })}
              </View>
              {estateInfoList.length > 4 && (
                <View className="estate-more">
                  {!estateInfoMore ? (
                    <View className="pack-down" onClick={this.handleMoreInfo}>
                      <Text>查看更多信息</Text>
                      <Image
                        className="right-icon"
                        src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                      ></Image>
                    </View>
                  ) : (
                    <View className="pack-up" onClick={this.handleMoreInfo}>
                      收起
                    </View>
                  )}
                </View>
              )}
            </View>
          )}

          {/*  联系模块  */}
          <View className="tab contact-box">
            {contactList &&
              contactList.map((item, index) => {
                return (
                  <View
                    className="contact-tab"
                    key={item.index}
                    onClick={_fixme_with_dataset_(this.handleShowDialog, { contactType: item.type })}
                  >
                    <View className="logo-contact-box">
                      <Image className="logo-contact" src={item.image}></Image>
                    </View>
                    <View className="contact-name">{item.smsTemName}</View>
                  </View>
                );
              })}
          </View>
          {/*  主力户型  */}
          {layoutList && layoutList.length > 0 && (
            <View className="tab layout-box">
              <View className="tab-top">
                <View className="title">主力户型</View>
              </View>
              <View className="layout-tab-box">
                {layoutList &&
                  layoutList.map((item, index) => {
                    return (
                      <View
                        className="layout-tab"
                        key={item.itemNo}
                        onClick={_fixme_with_dataset_(this.handleLayoutDetail, { layout: item })}
                      >
                        <Image className="icon-layout" src={item.thumbnail}></Image>
                        <View className="layout-content">
                          {item.name && <View className="layout-title">{item.name}</View>}

                          {item.buildArea && <View className="layout-text">{'建面 约' + item.buildArea + '㎡'}</View>}

                          {item.referencePrice && (
                            <View className="layout-price">
                              {'约' + filters.moneyFilter(item.referencePrice, true) + '万元/套'}
                            </View>
                          )}
                        </View>
                      </View>
                    );
                  })}
              </View>
            </View>
          )}

          {/*  置业顾问  */}
          {adviserList && adviserList.length > 0 && (
            <View className="tab">
              <View className="tab-top" onClick={this.handleAdviserList}>
                <View className="title">置业顾问</View>
                <View className="more">
                  <Text>查看更多</Text>
                  <Image
                    className="right-icon"
                    src="https://front-end-1302979015.file.myqcloud.com/images/c/images/right-icon.png"
                  ></Image>
                </View>
              </View>
              <View className="adviser-box">
                {index < 3 && (
                  <Block>
                    {adviserList &&
                      adviserList.map((item, index) => {
                        return (
                          <View className="tab-adviser" key={item.index}>
                            <Image
                              className="icon-head"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/head-default.png"
                            ></Image>
                            <View className="adviser-info">
                              <View className="adviser-name">{item.name}</View>
                              <View className="adviser-phone">{item.phone}</View>
                            </View>
                            {/*  <image class="icon-wechat" src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-wechat.png" data-adviser="{{item}}" bindtap="handleWechatCard"></image>  */}
                            <Image
                              className="icon-call"
                              src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-call.png"
                              onClick={_fixme_with_dataset_(this.handlePhoneCall, { phone: item.phone })}
                            ></Image>
                          </View>
                        );
                      })}
                  </Block>
                )}
              </View>
            </View>
          )}

          {/*  项目周边  */}
          <View className="tab">
            <View className="tab-top">
              <View className="title">项目周边</View>
            </View>
            <Map
              id="map"
              className="tab-map"
              longitude={mapInfo.longitude}
              latitude={mapInfo.latitude}
              scale="18"
              showLocation
              onClick={this.handleClickMap}
              onControltap={this.handleClickMap}
              markers={mapInfo.markers}
              onMarkertap={this.handleClickMap}
            ></Map>
            <View className="facility-box" onClick={this.handleClickMap}>
              <View className="tab-facility">
                <Image
                  className="icon-facility"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-traffic.png"
                ></Image>
                <View className="facility-name">交通</View>
              </View>
              <View className="tab-facility">
                <Image
                  className="icon-facility"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-school.png"
                ></Image>
                <View className="facility-name">学校</View>
              </View>
              <View className="tab-facility">
                <Image
                  className="icon-facility"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-medical.png"
                ></Image>
                <View className="facility-name">医疗</View>
              </View>
              <View className="tab-facility">
                <Image
                  className="icon-facility"
                  src="https://front-end-1302979015.file.myqcloud.com/images/c/images/icon-life.png"
                ></Image>
                <View className="facility-name">生活</View>
              </View>
            </View>
          </View>
          {/*  项目动态  */}
          {/*  <View class="tab">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <View class="tab-top">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <View class="title">项目动态</View>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              </View>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <image class="icon-dynamic" src=""></image>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </View>  */}
        </View>
        {/*  变价通知  */}
        <AnimatDialog id="animat-dialog" animClass="animat-dialog">
          {contactType === 'variable' ? (
            <View className="dialog-content">
              <View className="dialog-title">变价通知</View>
              <View className="dialog-text">您将第一时间获取变价信息</View>
              <Input
                className="dialog-input"
                type="number"
                placeholder="请输入您的手机号码"
                value={phone}
                onInput={this.phoneInput}
              ></Input>
              <View
                className="dialog-btn"
                style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                onClick={this.onDialogSave}
              >
                确定
              </View>
            </View>
          ) : (
            contactType === 'open' && (
              <View className="dialog-content">
                <View className="dialog-title">开盘通知</View>
                <View className="dialog-text">您将第一时间获取开盘信息</View>
                <Input
                  className="dialog-input"
                  type="number"
                  placeholder="请输入您的手机号码"
                  value={phone}
                  onInput={this.phoneInput}
                ></Input>
                <View
                  className="dialog-btn"
                  style={_safe_style_('background-color:' + tmpStyle.btnColor)}
                  onClick={this.onDialogSave}
                >
                  确定
                </View>
              </View>
            )
          )}

          {/*  开盘通知  */}
          <Image
            className="icon-close"
            src="https://front-end-1302979015.file.myqcloud.com/images/c/images/close.png"
            onClick={this.handleCloseDialog}
          ></Image>
        </AnimatDialog>
      </View>
    );
  }
}

export default EstateDetail;
