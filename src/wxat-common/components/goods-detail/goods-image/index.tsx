import { _fixme_with_dataset_, _safe_style_ } from '@/wk-taro-platform';
import { Block, View, Swiper, SwiperItem, Image, Video } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import withWeapp from '@tarojs/with-weapp';
import wxApi from '../../../utils/wxApi.js';
import cdnResConfig from '../../../constants/cdnResConfig.js';
import protectedMailBox from '../../../utils/protectedMailBox.js';
import date from '../../../utils/date.js';
import state from "../../../../state";
import imageUtils from '../../../utils/image.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import imgRotate from '../../../utils/imgRotate.js';

import './index.scss';
class GoodsImage extends React.Component {
  state = {
    vrLogo: cdnResConfig.goods.vrLogo, // cdn静态资源图片
    goodsImages: [], // 商品图片，用于大图展示及轮播
    wxItem: null, // 商品详情
    countDownTime: null, // 倒计时
    timeInterval: null, // 定时器
    isVideoShow: true, // 视频显示
    videoPosterHide: false, // 隐藏封面图
    isVideoPlaying: false,
    isShowPauseIcon: false,
    videoCtx: null
  }

  // 发货时间
  deliveryTime = () => {
    const wxItem = this.state.wxItem; // 商品详情
    let dt = null;

    if (wxItem) {
      const preSell = wxItem.preSell; // 是否预售
      const deliveryTimeType = wxItem.deliveryTimeType; // 发货时间类型 0：固定时间，1：购买后几天范围内发货
      const deliveryTime = wxItem.deliveryTime; // 固定发货时间
      const daysAfterBuyRange = wxItem.daysAfterBuyRange; // 购买后几天范围内发货
      if (deliveryTimeType === 0 && deliveryTime && preSell) {
        dt = date.format(new Date(deliveryTime), 'yyyy/MM/dd');
      } else if (deliveryTimeType === 1 && daysAfterBuyRange) {
        dt = daysAfterBuyRange + '天';
      }
    }
    return dt
  }

  componentDidMount() {
    this.countdownPreSellTime(); // 预售时间倒计时
    this.state.videoCtx = Taro.createVideoContext('goodsVideo', this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.goodsDetail) {
      const goodsImages = (nextProps.goodsDetail.materialUrls || []).map((i) => {
        return new Promise((resolve) => {
          imgRotate.getImgRotate(imageUtils.cdn(i)).then((res) => {
            resolve({
              url: imageUtils.cdn(i),
              imgRotate: res,
            });
          });
        });
      });
      Promise.all(goodsImages).then((res) => {
        this.setState({
          goodsImages: res,
          wxItem: nextProps.goodsDetail.wxItem || null,
        });
      })
    }
  }

  componentDidUpdate(preProps) {
    // 当前播放的视频
    if (preProps && preProps.key === 'currentVideoId') {
      this.setCurrentVideo();
    }
  }

  componentWillUnmount() {
    if (this.state.timeInterval) {
      clearTimeout(this.state.timeInterval);
    }
  }

  /**
  * 跳转VR展示页面
  */
  handleGoVR = () => {
   const wxItem = this.state.wxItem || {};
   protectedMailBox.send('sub-packages/moveFile-package/pages/receipt/index', 'receiptURL', wxItem.vrUrl);

   // 埋点事件集合
   let reportList = null;
   if (wxItem.type && wxItem.type == goodsTypeEnum.ESTATE.value) {
     reportList = [
       {
         event: 'pageviewOut',
         params: {
           page_title: wxItem.name, // 页面标
           page_type: 'estate_vr', // 页面类型
           item_id: wxItem.id || null, // 地产-商品(楼盘)id
           item_no: wxItem.itemNo || null, // 地产-商品(楼盘)no
           item_name: wxItem.name || null, // 地产-商品(楼盘)名称
         },
       },
     ];
   }

   wxApi.$navigateTo({
     url: '/sub-packages/moveFile-package/pages/receipt/index',
     data: {
       title: wxItem.name,
       reportList,
     }
   })
  }

  // 预售
  // 预售时间倒计时
  countdownPreSellTime() {
    const wxItem = this.state.wxItem;
    if (wxItem && wxItem.preSellEndTime) {
      this.countDownTime();
      this.state.timeInterval = setTimeout(() => {
        this.countdownPreSellTime();
      }, 1000);
      if (wxItem.preSellEndTime <= 1000) {
        clearTimeout(this.state.timeInterval);
      }
    }
  }

  // 预售时间倒计时计算
  countDownTime() {
    let countDownTime = null;
    const preSellEndTime = this.state.wxItem && this.state.wxItem.preSellEndTime;
    const curDateTime = new Date().getTime();
    if (preSellEndTime && preSellEndTime > curDateTime) {
      countDownTime = {
        day: '',
        hour: '',
        minute: '',
        second: '',
      };

      const intervalTime = preSellEndTime - curDateTime;
      const oneDayStamp = 3600 * 24 * 1000;
      const oneHourStamp = 3600 * 1000;
      const oneMinuteStamp = 60 * 1000;
      const oneSecondStamp = 1000;
      const day = parseInt(intervalTime / oneDayStamp);
      let dayStr = day + '';
      if (dayStr.length === 1) {
        dayStr = '0' + dayStr;
      }
      countDownTime.day = dayStr;

      const hour = parseInt((intervalTime - day * oneDayStamp) / oneHourStamp);
      let hourStr = hour + '';
      if (hourStr.length === 1) {
        hourStr = '0' + hourStr;
      }
      countDownTime.hour = hourStr;

      const minute = parseInt((intervalTime - day * oneDayStamp - hour * oneHourStamp) / oneMinuteStamp);

      let minuteStr = minute + '';
      if (minuteStr.length === 1) {
        minuteStr = '0' + minuteStr;
      }
      countDownTime.minute = minuteStr;

      const second = parseInt(
        (intervalTime - day * oneDayStamp - hour * oneHourStamp - minute * oneMinuteStamp) / oneSecondStamp
      );

      let secondStr = second + '';
      if (secondStr.length === 1) {
        secondStr = '0' + secondStr;
      }
      countDownTime.second = secondStr;
    }
    if (countDownTime) {
      // console.log(countDownTime.day + '天' + countDownTime.hour + '小时' + countDownTime.minute + '分' + countDownTime.second + '秒');
    }
    this.setState({
      countDownTime,
    });
  }

  /**
  * 轮播预览大图
  */
  previewImg = (e) => {
    const currentUrl = e.currentTarget.dataset.currenturl;
    const previewUrls = this.state.goodsImages;
    Taro.previewImage({
      current: currentUrl,
      urls: previewUrls,
    });
  }

  swiperChange = (e) => {
    if (!this.state.isVideoShow || this.state.isVideoPlaying) {
      this.state.videoCtx.pause();
      this.setState({
        isShowPauseIcon: true,
      });
    }
    this.setState({
      isVideoShow: true,
      videoPosterHide: false,
      isVideoPlaying: false,
    });
  }

  // 监听视频播放结束事件
  videoEnd = (e) => {
    this.setState({
      videoPosterHide: false,
      isVideoShow: true,
      isVideoPlaying: false,
      isShowPauseIcon: false,
    });
  }

  play = () => {
    const videoCtx = Taro.createVideoContext('goodsVideo', this);
    videoCtx.pause();
    setTimeout(() => {
      videoCtx.play();
      state.base.currentVideoId = 'goodsVideo';
    }, 150);
    this.setState({
      videoPosterHide: true,
      isVideoShow: false,
      isVideoPlaying: true,
      isShowPauseIcon: false,
    });
  }

  hanleVideoPlay = (e) => {
    const videoCtx = Taro.createVideoContext('goodsVideo', this);
    if (this.state.isVideoPlaying) {
      videoCtx.pause();
      this.setState({
        isVideoPlaying: false,
        isShowPauseIcon: true,
      });
    } else {
      this.setState({
        isVideoPlaying: true,
        isShowPauseIcon: false,
      });

      videoCtx.play();
      state.base.currentVideoId = 'goodsVideo';
    }
  }

  onShowPauseIcon = () => {
    this.setState({
      isShowPauseIcon: true,
    });
  }

  setCurrentVideo() {
    const videoCtx = Taro.createVideoContext('goodsVideo', this);
    if ((!this.state.isVideoShow || this.state.isVideoPlaying) && state.base.currentVideoId !== 'goodsVideo') {
      videoCtx.pause();
      this.setState({
        isVideoPlaying: false,
        isShowPauseIcon: true,
      })
    }
  }

  render() {
    const {
      goodsImages,
      wxItem,
      isVideoShow,
      isVideoPlaying,
      isShowPauseIcon,
      vrLogo,
      countDownTime,
    } = this.state
    return (
      <View data-scoped="wk-wcgg-GoodsImage" className="wk-wcgg-GoodsImage goods-image-box">
        <Swiper
          indicatorDots={goodsImages.length > 1 || (wxItem.videoUrl && goodsImages.length)}
          className="goods-image"
          style={_safe_style_('position: absolute;')}
          onChange={this.swiperChange}
        >
          {!!wxItem.videoUrl && (
            <SwiperItem>
              {wxItem.videoCoverUrl ? (
                <View className="goods-video-wraper">
                  {isVideoShow ? (
                    <Block>
                      {!this.props.videoPosterShow && (
                        <Image className="temp-poster" mode="aspectFill" src={wxItem.videoCoverUrl}></Image>
                      )}

                      {isVideoShow && (
                        <View className="temp-play" onClick={this.play}>
                          <Image
                            className="temp-img"
                            mode="aspectFill"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/play.png"
                          ></Image>
                        </View>
                      )}
                    </Block>
                  ) : (
                    !isVideoShow && (
                      <Block>
                        {!isVideoShow && (
                          <Video
                            id="goodsVideo"
                            onClick={this.hanleVideoPlay}
                            className="goods-video"
                            src={wxItem.videoUrl}
                            controls={false}
                            showCenterPlayBtn={false}
                            objectFit="cover"
                            onEnded={this.videoEnd}
                          ></Video>
                        )}

                        {!isVideoPlaying && (
                          <Image
                            mode="aspectFill"
                            onClick={this.hanleVideoPlay}
                            className="pause-img"
                            src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/pause.png"
                          ></Image>
                        )}
                      </Block>
                    )
                  )}
                </View>
              ) : (
                <View className="goods-video-wraper">
                  <Video
                    id="goodsVideo"
                    onClick={this.hanleVideoPlay}
                    onPause={this.onShowPauseIcon}
                    className="goods-video"
                    src={wxItem.videoUrl}
                    controls={false}
                    objectFit="cover"
                    onEnded={this.videoEnd}
                  ></Video>
                  {isShowPauseIcon && (
                    <Image
                      mode="aspectFill"
                      onClick={this.hanleVideoPlay}
                      className="pause-img"
                      src="https://front-end-1302979015.file.myqcloud.com/images/c/wxat-common/images/pause.png"
                    ></Image>
                  )}
                </View>
              )}
            </SwiperItem>
          )}

          {goodsImages &&
            goodsImages.map((item, index) => {
              return (
                <Block key={item.unique}>
                  <SwiperItem>
                    <Image
                      className="goods-image"
                      src={item.url}
                      style={_safe_style_('transform: rotate(' + (item.imgRotate || 0) + 'deg)')}
                      mode="aspectFill"
                      onClick={_fixme_with_dataset_(this.previewImg, { currenturl: item.url })}
                    ></Image>
                  </SwiperItem>
                </Block>
              );
            })}
        </Swiper>
        {/*  vr功能入口  */}
        {wxItem.vrUrl && (
          <View
            className="vr-box"
            onClick={this.handleGoVR}
            style={_safe_style_('bottom: ' + (wxItem.preSell ? 82 : 19) + 'px')}
          >
            <Image className="vr-logo" mode="aspectFill" src={vrLogo}></Image>
            <View className="vr-label">VR全景</View>
          </View>
        )}

        {/*  预售信息  */}
        {wxItem.preSell && (
          <View className="pre-sale-info">
            <View className="pre-sell-item">
              <View className="label">预售中</View>
              {countDownTime && (
                <View className="countdown-box">
                  {countDownTime.day &&
                    countDownTime.day.map((item, index) => {
                      return (
                        <View className="unit num" key={item.index}>
                          {item}
                        </View>
                      );
                    })}
                  <View className="unit">天</View>
                  {countDownTime.hour &&
                    countDownTime.hour.map((item, index) => {
                      return (
                        <View className="unit num" key={item.index}>
                          {item}
                        </View>
                      );
                    })}
                  <View className="unit">小时</View>
                  {countDownTime.minute &&
                    countDownTime.minute.map((item, index) => {
                      return (
                        <View className="unit num" key={item.index}>
                          {item}
                        </View>
                      );
                    })}
                  <View className="unit">分</View>
                  {countDownTime.second &&
                    countDownTime.second.map((item, index) => {
                      return (
                        <View className="unit num" key={item.index}>
                          {item}
                        </View>
                      );
                    })}
                  <View className="unit">秒</View>
                </View>
              )}
            </View>
            {/*  预计发货时间  */}
            <View className="pre-sell-item">
              <View className="shipping-time">{'预计发货时间: ' + this.deliveryTime()}</View>
              <View className="shipping-time">截止时间</View>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default GoodsImage;
