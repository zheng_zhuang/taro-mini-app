import { _safe_style_ } from '@/wk-taro-platform';
import { Block, View, Text } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
import template from '../../../utils/template.js';
import goodsTypeEnum from '../../../constants/goodsTypeEnum.js';
import industryEnum from '../../../constants/industryEnum.js';

import './index.scss';
const app = Taro.getApp();

class DrugsInfo extends React.Component {

  state = {
    tmpStyle: {}, // 主题模板
    goodsTypeEnum, // 商品类型常量
    industryEnum, // 行业类型常量
    // industry: app.globalData.industry, // 当前行业类型
    wxItem: {}
  }

  componentDidMount () {
    this.getTemplateStyle(); // 获取模板配置
  }

  /**
     * 获取模板配置
     */
  getTemplateStyle () {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render () {
    const { tmpStyle, wxItem } = this.state;
    return (
      <View data-scoped="wk-wcgd-DrugsInfo" className="wk-wcgd-DrugsInfo drugs-info">
        <View className="detail-header">
          <Text className="header-label" style={_safe_style_('background: ' + tmpStyle.btnColor)}></Text>
          <Text>药品说明</Text>
        </View>
        <View className="drugs-box">
          <View className="drug-item">
            <View className="label">药品分类</View>
            <View className="content">{(wxItem.drugType ? '' : '非') + '处方药'}</View>
          </View>
          {wxItem.drugCommonName && (
            <View className="drug-item">
              <View className="label">通用名称</View>
              <View className="content">{wxItem.drugCommonName}</View>
            </View>
          )}

          {wxItem.drugApprovalNumber && (
            <View className="drug-item">
              <View className="label">批准文号</View>
              <View className="content">{wxItem.drugApprovalNumber}</View>
            </View>
          )}

          {wxItem.drugBrand && (
            <View className="drug-item">
              <View className="label">品牌</View>
              <View className="content">{wxItem.drugBrand}</View>
            </View>
          )}

          {wxItem.drugManufacturer && (
            <View className="drug-item">
              <View className="label">生产商</View>
              <View className="content">{wxItem.drugManufacturer}</View>
            </View>
          )}

          {wxItem.drugCategory && (
            <View className="drug-item">
              <View className="label">药品类别</View>
              <View className="content">{wxItem.drugCategory}</View>
            </View>
          )}

          {wxItem.drugUnit && (
            <View className="drug-item">
              <View className="label">药品单位</View>
              <View className="content">{wxItem.drugUnit}</View>
            </View>
          )}

          {wxItem.drugSpecification && (
            <View className="drug-item">
              <View className="label">药品规格</View>
              <View className="content">{wxItem.drugSpecification}</View>
            </View>
          )}

          {wxItem.usageMethod && (
            <View className="drug-item">
              <View className="label">使用方法</View>
              <View className="content">{wxItem.usageMethod}</View>
            </View>
          )}

          {wxItem.usageAndDosage && (
            <View className="drug-item">
              <View className="label">使用剂量</View>
              <View className="content">{wxItem.usageAndDosage}</View>
            </View>
          )}

          {wxItem.applicableSymptom && (
            <View className="drug-item">
              <View className="label">适用症状/功能主治</View>
              <View className="content">{wxItem.applicableSymptom}</View>
            </View>
          )}

          {wxItem.usageTaboo && (
            <View className="drug-item">
              <View className="label">用药禁忌</View>
              <View className="content">{wxItem.usageTaboo}</View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default DrugsInfo;
