import React from 'react';
import { _fixme_with_dataset_,_safe_style_ } from '@/wxat-common/utils/platform';
import { Block, View, Image,Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import wxApi from '@/wxat-common/utils/wxApi';
import { ITouchEvent } from '@tarojs/components/types/common';
import api from '@/wxat-common/api/index';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';
import template from '@/wxat-common/utils/template.js';
import EXIF from "exif-js"

import './index.scss';

export interface Props {
  disabled?: boolean;
  containerIndex?: number; // 多个上传图片组件
  max: number;
  uploadUrl: string;
  showPreview: boolean;
  /**
   * 受控模式
   */
  value: string[];
  onChange: (value: string[], containerIndex: number) => void;
  customBg: string;
}

class UploadImg extends React.Component<Props> {
  static defaultProps = {
    containerIndex: 0,
    disabled: false,
    value: [],
    max: 3,
    uploadUrl: api.upload,
    showPreview: true
  };

  state = {
    tmpStyle: {},
    imgLimit: 5, // 限制上传图片大小
    imgQuality: 0.5, // 图片压缩质量
  }

  handleDelete = (e: ITouchEvent) => {
    const { id } = e.currentTarget.dataset;
    const { onChange, value = NOOP_ARRAY, containerIndex } = this.props;
    const newValue = [...value];
    newValue.splice(+id, 1);

    onChange(newValue, containerIndex);
  };

  // 小程序不支持一次上传多张图片，改为多次上传一张图片
  uploadimg = async (paths: string[]) => {
    const { uploadUrl, onChange, value = NOOP_ARRAY, containerIndex } = this.props;

    try {
      // wxApi.showLoading({ title: '正在上传' });
      // const uploaded = await Promise.all(
      //   paths.map(async (i) =>
      //     wxApi
      //       .$uploadFile({
      //         loading: false,
      //         quite: true,
      //         url: uploadUrl,
      //         filePath: i,
      //         name: 'files',
      //         formData: {}, // 额外参数
      //       })
      //       .then((res) => res.data[0])
      //   )
      // );
      const newValue = [...value, ...paths];
      onChange(newValue, containerIndex);
    } catch (err) {
      console.error(err)
      wxApi.showToast({
        title: `上传失败(${err.data ? err.data.errorMessage : '系统繁忙'})`,
        icon: 'none',
      });
    } finally {
      wxApi.hideLoading();
    }
  };

  handleShowLargeImage = (e: ITouchEvent) => {
    e.preventDefault();
    e.stopPropagation();
    const { disabled } = this.props;
    if(disabled) return;
    wxApi.previewImage({
      current: '', // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.src], // 需要预览的图片http链接列表
    });
  };

  handleChooseImage = () => {
    const { max, value = NOOP_ARRAY } = this.props;

    // 限制上传数量
    if (value.length >= max) {
      wxApi.showToast({
        title: '最多上传' + max + '张图片',
        icon: 'none',
        mask: true,
      });

      return;
    }
    wxApi.chooseImage({
      count: 1, // 最多可以选择的图片张数，默认3
      sizeType: ['original', 'compressed'], // 可选择原图或压缩后的图片
      sourceType: ['album', 'camera'], // 可选择性开放访问相册、相机
      success: (res) => {
        wxApi
          .$uploadFile({
            loading: true,
            url: api.upload,
            filePath: res.tempFilePaths[0],
            name: 'files',
            formData: {}, //额外参数
          })
          .then((res) => {
            console.log('uploadFile', res)
            this.uploadimg(res.data);
          })
          .finally((error) => {
          });
      },
    });
    // wxApi.chooseImage({
    //   count: max - value.length, // 最多可以选择的图片张数，默认3
    //   sizeType: ['original', 'compressed'], // 可选择原图或压缩后的图片
    //   sourceType: ['album', 'camera'], // 可选择性开放访问相册、相机
    //   success: async (res) => {
    //     console.log('wxApi.chooseImage', res)
    //     const path = res.tempFilePaths.slice(0,max - value.length);
    //     // const limit = this.state.imgLimit;
    //     // const arr = res.tempFiles.filter(file=>{
    //     //   return file.size >= 1024*1024*limit
    //     // })
    //     // if (arr.length) { // 图片大于等于限制大小，弹出一个提示框
    //     //   wxApi.showToast({
    //     //     title: `上传图片不能大于${limit}M!`,
    //     //     icon: 'none'
    //     //   })
    //
    //     // } else {
    //         // this.uploadimg(path);
    //         const tempFiles = res.tempFiles.slice(0,max - value.length)
    //         // console.log('wxApi.chooseImage', {
    //         //   path,
    //         //   res,
    //         //   tempFiles
    //         // })
    //         const data = await this.imgCompress(tempFiles)
    //         if(data && data.length > 0){
    //           this.uploadimg(data);
    //         }
    //     // }
    //   },
    // });
  };

  async imgCompress(files = []) {
    wxApi.showLoading({ title: '正在处理图片' });
    let arr = []
    for(let i = 0; i<files.length; i++){
      const file = files[i]
      // console.log('imgCompress', file)
      // if (!/image\/\w+/.test(file.type)) {
      //   wxApi.showToast({
      //     title: `图片格式错误`,
      //     icon: 'none',
      //   });
      //   return false;
      // }
      console.log('imgCompress', file)
      await this.loadImg(file).then(item => {
        if(item){
          arr.push(item)
        }
      })
    }
    return arr
  };

  loadImg(file){
    return new Promise((resolve, reject) => {
      // const reader = new FileReader();// 读取用户上传的图片
      // reader.readAsDataURL(file.originalFileObj);// readAsDataURL可以获取API异步读取文件数据另存为数据URL;将该URL绑定到img标签的src属性上，可以实现图片上传预览
      // reader.onload = async(e) => {
        let content= file.path; // 图片的src，base64格式
        let img = document.createElement('img');
        img.src = content;
        const _this = this;
        let Orientation = null;//上传图片时图片角度问题
        img.onload = () => {
          EXIF.getData(img, function () {
            EXIF.getAllTags(this);
            Orientation = EXIF.getTag(this, 'Orientation');
            const item = _this.canvasCompress(img,Orientation)
            resolve(item)
          });
        }
      // }
    })
  };


  canvasCompress(img,Orientation){
    const imgQuality = this.state.imgQuality
    let canvas = document.createElement("canvas")
    let ctx = canvas.getContext("2d")
    let initSize=img.src.length;
    let width=img.width;
    let height=img.height;
    canvas.width = width;
    canvas.height = height;
    const isAndroid =  /(android)/i.test(wxApi.getSystemInfoSync().system)
    if (Orientation != "" && Orientation != 1 && Orientation != undefined && isAndroid) {
      switch (Orientation) {
        case 6://需要顺时针90度旋转
          canvas.width = height;
          canvas.height = width;
          ctx.rotate(90 * Math.PI / 180);
          ctx.drawImage(img, 0, -height);
          break;
        case 8://需要逆时针90度旋转
          canvas.width = height;
          canvas.height = width;
          ctx.rotate(-90 * Math.PI / 180);
          ctx.drawImage(img, -width, 0);
          break;
        case 3://需要180度旋转
          ctx.rotate(180 * Math.PI / 180);
          ctx.drawImage(img, -width, -height);
          break;
      }
    }else{
      ctx.drawImage(img, 0, 0, width, height);
    }
    let dataURL = canvas.toDataURL("image/jpeg", imgQuality);
    // return this.convertBase64UrlToBlob(dataURL)
    let Blob = this.convertBase64UrlToBlob(dataURL)
    canvas = null;
    ctx = null;
    dataURL = null;
    return Blob

  };

  convertBase64UrlToBlob (urlData){
    const arr = urlData.split(',');
    // console.log(arr)
    const mime = arr[0].match(/:(.*?);/)[1];

    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    const res = new Blob([u8arr], {
      type: mime,
    })

    const pathUrl = Object.assign(res,{
      // 这里一定要处理一下 URL.createObjectURL
      preview: URL.createObjectURL(res),
    });
    const limit = this.state.imgLimit;
    // console.log('size', limit, pathUrl.size/(1024*1024))
    if(pathUrl.size <= limit*1024*1024){
      return URL.createObjectURL(pathUrl);

    }else{
      wxApi.showToast({
        title: '上传图片大小超过限制',
        icon: 'none',
        duration: 3000
      });
      return false
    }
  };


  componentDidMount(){
    this.getTemplateStyle();
  }

  // 获取主题模板配置
  getTemplateStyle() {
    const templateStyle = template.getTemplateStyle();
    this.setState({
      tmpStyle: templateStyle,
    });
  }

  render() {
    const { showPreview, value = NOOP_ARRAY, max, disabled, customBg } = this.props;
    const { tmpStyle } = this.state;

    return (
      <View data-scoped='wk-wcu-UploadImg' className='wk-wcu-UploadImg upload-img-box'>
        {!!showPreview && (
          <Block>
            {value.map((item, index) => {
              return (
                <View className='upload-img' key={`${item}_${index}`}>
                  {!disabled &&(
                  // <Image
                  //   src={require('../../images/del.png')}
                  //   className='upload-img__del'
                  //   onClick={_fixme_with_dataset_(this.handleDelete, { id: index })}
                  //   mode='aspectFill'
                  // ></Image>
                    <Text className='upload-img__del' onClick={_fixme_with_dataset_(this.handleDelete, { id: index })}>×</Text>
                  )}
                  <Image
                    className='upload-img__img'
                    src={item}
                    onClick={_fixme_with_dataset_(this.handleShowLargeImage, { src: item })}
                    mode='aspectFill'
                  ></Image>
                </View>
              );
            })}
          </Block>
        )}

        {value.length < max && !disabled && (
          // <Image
          //   className='upload-img__logo'
          //   src={require('../../../images/upload-logo.png')}
          //   onClick={this.handleChooseImage}
          // ></Image>
          <View
            className="upload-img__logo"
            onClick={this.handleChooseImage}
            style={_safe_style_(`border-color:${tmpStyle.btnColor}`)}
          >
            {
              customBg ? (
                <Image className="img_upload" src={customBg}></Image>
              ) : (
                <Block>
                  <Text className="iconfont icon-shangchuan"
                    style={_safe_style_(`color:${tmpStyle.btnColor}`)}
                  ></Text>
                  <View className="tipText">点击上传</View>
                </Block>
              )
            }
          </View>
        )}
      </View>
    );
  }
}

export default UploadImg;
