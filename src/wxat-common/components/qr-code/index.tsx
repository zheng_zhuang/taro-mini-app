import React, { FC, useState, useEffect } from 'react';
import { _fixme_with_dataset_, _safe_style_ } from '@/wxat-common/utils/platform';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// components/qr-code/index.js
import api from '../../api/index.js';
import wxApi from '../../utils/wxApi';
import constant from '../../constants/index.js';
import pageLinkEnum from '../../constants/pageLinkEnum.js';

import './index.scss';
import { ITouchEvent } from '@tarojs/components/types/common';
import { useScreenHightlight } from '@/hooks/useScreenHightlight';

const check = {
  isChecking: false,
  loopTimes: 0,
  loopId: -1,
};

type QrCodeProps = {
  verificationNo: string;
  verificationType: number;
  showBarcode?: boolean;
  showQrcode?: boolean;
  isScale?: boolean;
  showTip?: boolean;
};

let QrCode: FC<QrCodeProps> = (props) => {
  const [initState, setInitState] = useState({
    barCode: '',
    qrCode: '',
    tipMessage: '到店请出示给收银员核销',
    maPath: pageLinkEnum.orderPkg.verification,
    selected: 0,
  });

  useScreenHightlight();

  useEffect(() => {
    getQrCodePath();
  }, [props.verificationNo]);

  // 销毁定时器
  useEffect(() => {
    return () => {
      stopQuery();
    };
  }, []);

  function handleShowLargeImage(e: ITouchEvent) {
    wxApi.previewImage({
      current: '', // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.src], // 需要预览的图片http链接列表
    });
  }

  function selectMa() {
    setInitState({ ...initState, selected: 0 });
  }

  function selectCode() {
    setInitState({ ...initState, selected: 1 });
  }

  // 获取核销条形码和二维码
  function getQrCodePath() {
    const { verificationNo, verificationType } = props;
    console.log('No -> ' + verificationNo);
    console.log('Type -> ' + verificationType);

    const reqParam = {
      verificationNo: verificationNo,
      verificationType: verificationType,
      maPath: initState.maPath,
    };

    wxApi
      .request({
        url: api.verification.getQRCode,
        loading: true,
        checkSession: true,
        data: reqParam,
      })
      .then((res) => {
        console.log(res.data);

        setInitState({
          ...initState,
          barCode: res.data ? constant.PREFIX_BASE64 + res.data.barCode : '',
          qrCode: res.data ? res.data.qrCode : '',
        });

        // 开始轮寻核销状态
        startQuery();
      })
      .catch((err) => {
        console.log('getQrCodePath error -> ' + JSON.stringify(err));
      });
  }

  // 查询核销状态
  function _queryVerificationStatus() {
    const reqParam = {
      verificationNo: props.verificationNo,
      verificationType: props.verificationType,
    };

    wxApi
      .request({
        url: api.verification.status,
        checkSession: true,
        data: reqParam,
      })
      .then((res) => {
        console.log(JSON.stringify(res));
        // 60 表示已完成，10 待支付，100 待核销
        let status = res.data ? res.data : 0;

        if (status === 100) {
          startQuery();
        } else {
          stopQuery();
        }

        if (status === 60) {
          setInitState((initState) => ({ ...initState, tipMessage: '核销已完成' }));
        }
      })
      .catch((err) => {
        console.log('queryVerificationStatus error -> ' + JSON.stringify(err));
      });
  }

  // 开始查询
  function startQuery() {
    console.log('startQuery, loopTimes -->' + check.loopTimes);
    console.log('startQuery, maxTimes -->' + constant.verification.MAX_LOOP_TIMES);
    check.isChecking = true;

    // 轮寻次数少于最大次数
    if (check.loopTimes++ < constant.verification.MAX_LOOP_TIMES) {
      check.loopId = setTimeout(() => {
        if (check.isChecking) {
          _queryVerificationStatus();
        }
      }, constant.verification.LOOP_INTERVAL);
    }
  }

  // 停止轮寻
  function stopQuery() {
    check.isChecking = false;
    check.loopTimes = 0;
    if (check.loopId) {
      clearTimeout(check.loopId);
    }
  }

  let $barcode: JSX.Element | null = null;
  if (props.showBarcode) {
    $barcode = (
      <View className='image-wrap'>
        <Image
          className='bar-code'
          style={_safe_style_('transform: scale(' + (props.isScale ? 1.2 : 1) + ');')}
          src={initState.barCode}
          onClick={_fixme_with_dataset_(handleShowLargeImage, { src: initState.barCode })}
        />

        {!!props.showTip && <Text className='tip'>条形码核销</Text>}
      </View>
    );
  }

  let $qrcode: JSX.Element | null = null;
  if (props.showQrcode) {
    $qrcode = (
      <View className='image-wrap'>
        <Image
          className='qr-code'
          src={initState.qrCode}
          onClick={_fixme_with_dataset_(handleShowLargeImage, { src: initState.qrCode })}
        />

        {!!props.showTip && <Text className='tip'>小程序码核销</Text>}
      </View>
    );
  }

  return (
    <View data-scoped='wk-wcq-QrCode' className='wk-wcq-QrCode container'>
      {$barcode}
      {$qrcode}
    </View>
  );
};

QrCode.defaultProps = {
  verificationNo: '',
  verificationType: 1,
  showBarcode: true,
  showQrcode: true,
  showTip: true,
  isScale: false,
};

export default QrCode;
