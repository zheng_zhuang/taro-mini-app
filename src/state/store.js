import Taro from '@tarojs/taro';
import objUtils from '../wxat-common/utils/object.js';
import base from './base.js';
import report from "../sdks/buried/report";
import shareUtil from '../wxat-common/utils/share.js';
// import wakeApi from "../sdks/buried/wkapi/1.2.1";

const totalStores = objUtils.toComputed([]);

// raf
const raf = (() => {
  if (typeof requestAnimationFrame === 'undefined') {
    return function (callback) {
      setTimeout(function () {
        callback();
      }, 17);
    };
  }
  return requestAnimationFrame;
})();

/**
 * 全部店铺的id为-999
 * @type {number}
 */
const STORE_ALL_ID = -999;

const STORE_ALL_NAME = '我的所有店铺';

const config = {
  oldStoreId: -1,
};

const lastSearchStore = objUtils.toComputed({
  id: STORE_ALL_ID,
  name: STORE_ALL_NAME,
});

const LAST_LOCATION_STORE_KEY = 'location_store_1121';

// const updateCurrentStore = (newStore,isNotFromUserPick) => {
const updateCurrentStore = (newStore, isFromUserPick) => {
  const store = (base.currentStore = newStore);
  const oldStoreId = config.oldStoreId;
  const change = !!store.id && store.id !== oldStoreId;
  if (!change) {
    return;
  }
  // wakeApi.updateRunTimeBzParam({ store: store.id });

  // 记忆下最新选择的店铺id
  if (isFromUserPick) {
    Taro.setStorageSync(LAST_LOCATION_STORE_KEY, store.id + '');
  }

  // 初次直接渲染
  if (oldStoreId === -1) {
    base.currentStoreViewVisible = true;
  }
  // 再次，间隔渲染
  else {
    base.currentStoreViewVisible = false;
    raf(() => {
      base.currentStoreViewVisible = true;
    });
  }
  config.oldStoreId = store.id;
  base.userInfo.storeId = store.id;
  shareUtil.setStoreId(store.id);
  report.user(base.loginInfo, base.userInfo);
};

export default {
  lastSearchStore,
  totalStores,
  STORE_ALL_ID,
  STORE_ALL_NAME,
  updateCurrentStore,
};
