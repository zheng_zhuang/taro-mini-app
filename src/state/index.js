import base from './base.js';
import store from './store.js';

export default {
  base,
  store,
};
