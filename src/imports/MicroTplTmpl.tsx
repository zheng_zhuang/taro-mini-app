import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Form, Image, Text, Input, Checkbox, Button } from '@tarojs/components';
import filters from '@/wxat-common/utils/money.wxs';
import Taro from '@tarojs/taro';
import getStaticImgUrl from '../wxat-common/constants/frontEndImgUrl'
class MicroTplTmpl extends React.Component {
  render() {
    const { goodsInfoList } = this.props;
    return (
      <Block>
        <View className='micro-box'>
          {(goodsInfoList || []).map((item, index) => {
            return (
              <View className='item-tab' key={index}>
                <View className='item-icon-box'>
                  {item.pic1 ? (
                    <Image className='item-icon' src={item.pic1}></Image>
                  ) : (
                    <View className='item-icon'>
                      <Image src={getStaticImgUrl.order.default_png}></Image>
                    </View>
                  )}
                </View>
                {/*  商品详情  */}
                <View className='item-info'>
                  <View className='item-name'>{item.itemName || ''}</View>
                  {/*  商品规格  */}
                  <View className='item-attribute'>{'厂家：' + (item.manufacturerName || '')}</View>
                  {/*  商品数量及单价  */}
                  <View className='item-count'>{'x' + item.num}</View>
                  {!!item.fee && <View className='item-price'>{'￥' + filters.moneyFilter(item.fee, true)}</View>}
                </View>
              </View>
            );
          })}
        </View>
      </Block>
    );
  }
}

export default MicroTplTmpl;
