export default {
  split: function (str, sign) {
    return str ? str.split(sign) : [];
  },
};
