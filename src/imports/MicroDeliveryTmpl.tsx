import React from 'react';
import '@/wxat-common/utils/platform';
import { Block, View, Form, Image, Text, Input, Checkbox, Button } from '@tarojs/components';
import Taro from '@tarojs/taro';

import getStaticImgUrl from '../wxat-common/constants/frontEndImgUrl'
interface MicroDeliveryTmpl {
  props: {
    goodsInfoList: Array<Record<string, any>>;
    deliveryWay: any;
  };
}

class MicroDeliveryTmpl extends React.Component {
  static defaultProps = {
    goodsInfoList: [],
    deliveryWay: null,
  };

  static options = {
    addGlobalClass: true,
  };

  addAddress = () => {
    const { onAddAddress } = this.props;
    onAddAddress && onAddAddress();
  };
  render() {
    const { goodsInfoList, deliveryWay } = this.props;
    console.log('goodsInfoList deliveryWay', goodsInfoList, deliveryWay);
    return (
      <Block>
        {!!(!!goodsInfoList && !!goodsInfoList.length) && (
          <View className='micro-delivery'>
            <View className='delivery-way-box'>
              <View className='delivery-way-label'>配送</View>
              {goodsInfoList[0] && goodsInfoList[0].expressType == deliveryWay.SELF_DELIVERY.value ? (
                <View className='delivery-micro-name'>自提</View>
              ) : (
                <View className='delivery-micro-name'>快递</View>
              )}
            </View>
            {/*  订购门店自提  */}
            {!!(goodsInfoList[0] && goodsInfoList[0].expressType == deliveryWay.SELF_DELIVERY.value) && (
              <View className='self-delivery-store-box'>
                <View className='store-box'>
                  <View className='delivery-store-info-item store-name limit-line'>
                    <Image className='ic-location-dark' src={getStaticImgUrl.images.ic_location_dark_png}></Image>
                    {'取货门店：' + ((goodsInfoList[0] && goodsInfoList[0].storeName) || '-')}
                  </View>
                  <View className='delivery-store-info-item phone'>{goodsInfoList[0].storeTel || '-'}</View>
                  <View className='delivery-store-info-item address limit-line line-2'>
                    <Text className='address-title'></Text>
                    <Text className='address-text'>{(goodsInfoList[0] && goodsInfoList[0].storeAddress) || '-'}</Text>
                  </View>
                </View>
              </View>
            )}

            {/*  订购快递配送  */}
            {!!(goodsInfoList[0] && goodsInfoList[0].expressType == deliveryWay.EXPRESS.value) && (
              <View className='address-box'>
                <View className='show-address' onClick={this.addAddress}>
                  <View className='name-tel'>
                    <Image className='ic-location-dark' src={getStaticImgUrl.images.ic_location_dark_png}></Image>
                    {'收货人：' + goodsInfoList[0].consignee}
                  </View>
                  <View className='addr-phone'>{goodsInfoList[0].consigneeMobile}</View>
                  <View className='addr-text'>{goodsInfoList[0].detailAddress}</View>
                </View>
              </View>
            )}
          </View>
        )}
      </Block>
    );
  }
}

export default MicroDeliveryTmpl;
