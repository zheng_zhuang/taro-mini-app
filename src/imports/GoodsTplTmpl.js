import { _fixme_with_dataset_, _safe_style_ } from 'wk-taro-platform';
import { Block, View, Form, Image, Text, Input, Checkbox, Button } from '@tarojs/components';
import React from 'react';
import Taro from '@tarojs/taro';
export default class GoodsTplTmpl extends React.Component {
  render() {
    const {
      data: { goodsInfoList, index, goodsInfo, address, tmpStyle, freightType, isExpress, categoryType, filters },
    } = this.props;
    return (
      <Block>
        <View className="goods-list">
          {goodsInfoList &&
            goodsInfoList.map((goodsInfo, index) => {
              return (
                <View
                  className="a-goods"
                  style={_safe_style_('border-top: ' + (index > 0 ? '1px solid #E2E5EB' : ''))}
                  key={goodsInfo.index}
                >
                  <View className="img-box">
                    <Image
                      src={goodsInfo.pic || 'http://img0.imgtn.bdimg.com/it/u=2174541640,3479780226&fm=26&gp=0.jpg'}
                      mode="aspectFill"
                      className="img"
                    ></Image>
                    {(goodsInfo.itemStock < goodsInfo.itemCount || !goodsInfo.isShelf) && !address && (
                      <View className="low-stock">
                        {goodsInfo.isShelf ? (
                          <View className="low-label">{goodsInfo.itemStock === 0 ? '缺货' : '库存不足'}</View>
                        ) : (
                          <View className="low-label" style={_safe_style_('width: 80rpx')}>
                            已下架
                          </View>
                        )}
                      </View>
                    )}
                  </View>
                  <View className="goods-info-box">
                    <View className="goods-name-box">
                      <View className="goods-name limit-line">
                        {goodsInfo.drugType && (
                          <Text className="drug-tag" style={_safe_style_('background: ' + tmpStyle.btnColor)}>
                            处方药
                          </Text>
                        )}

                        {goodsInfo.name}
                      </View>
                      <View className="goods-label limit-line">{goodsInfo.skuTreeNames || ''}</View>
                    </View>
                    <View>
                      {goodsInfo.freightType === freightType.freightCollect && isExpress && (
                        <View className="goods-label freight-collect">运费到付</View>
                      )}

                      <View className="price-stock-box">
                        {categoryType != 5 && (
                          <View className="goods-price">{'¥ ' + filters.moneyFilter(goodsInfo.salePrice, true)}</View>
                        )}

                        {categoryType == 5 && (
                          <View className="goods-price">
                            {'¥ ' + filters.moneyFilter(goodsInfo.price ? goodsInfo.price : goodsInfo.salePrice, true)}
                          </View>
                        )}

                        {goodsInfo.itemStock >= goodsInfo.itemCount && (
                          <View className="goods-num">{'x ' + goodsInfo.itemCount}</View>
                        )}

                        {goodsInfo.itemStock < goodsInfo.itemCount && (
                          <View className="remain-stock">
                            剩余库存：
                            <Text style={_safe_style_('color: #FB4938')}>{goodsInfo.itemStock || 0}</Text>
                          </View>
                        )}

                        {goodsInfo.itemStock < goodsInfo.itemCount && goodsInfo.itemStock !== 0 && goodsInfo.isShelf && (
                          <View className="num-box">
                            <View
                              className={'num-decrease left-radius ' + (goodsInfo.itemCount == 0 ? 'num-disable' : '')}
                              onClick={_fixme_with_dataset_(this.handleDecreaseBuyNum, { index: goodsInfo.barcode })}
                            >
                              -
                            </View>
                            <View className="num-input">
                              <Input className="input-comp" type="number" value={goodsInfo.itemCount} disabled></Input>
                            </View>
                            <View
                              className="num-increase right-radius"
                              onClick={_fixme_with_dataset_(this.handleIncreaseBuyNum, { index: goodsInfo.barcode })}
                            >
                              +
                            </View>
                          </View>
                        )}
                      </View>
                      {goodsInfo.wxItem && goodsInfo.wxItem.frontMoneyItem && (
                        <View className="front-money">
                          {'¥' +
                            filters.moneyFilter(
                              goodsInfo.sku ? goodsInfo.sku.frontMoney : goodsInfo.wxItem.frontMoney,
                              true
                            ) +
                            '（定金）'}
                        </View>
                      )}
                    </View>
                  </View>
                </View>
              );
            })}
        </View>
      </Block>
    );
  }
}
