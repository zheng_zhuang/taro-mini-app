/**
 * 自定义 pageKey
 * @param key
 */
declare function setBuryPageKey(key: string): void;
declare function setBuryPageLoadArgument(callback: () => any): void;
declare function useSetBuryPageKey(key: string): void;
declare function useSetBuryPageLoadArgument(callback: () => any): void;
declare const _default: {
    setBuryPageKey: typeof setBuryPageKey;
    setBuryPageLoadArgument: typeof setBuryPageLoadArgument;
    useSetBuryPageKey: typeof useSetBuryPageKey;
    useSetBuryPageLoadArgument: typeof useSetBuryPageLoadArgument;
    VERSION: string;
    config: (opt: import("wkapi-core/dist/lib/sendTask").InitAccountParams) => void;
    updateRunTimeBzParam: typeof import("wkapi-core/dist/lib/sendTask").updateRunTimeBzParam;
    badJs: {
        BAD_SCENE: typeof import("wkapi-core/dist/lib/badReport").BAD_SCENE;
        openBadReport: typeof import("wkapi-core/dist/lib/badReport").openBadReport;
        reportBad: typeof import("wkapi-core/dist/lib/badReport").reportBad;
        setBadReportEnable: typeof import("wkapi-core/dist/lib/badReport").setBadReportEnable;
    };
    currentPageInfo: typeof import("wkapi-core/dist/lib/pageInfo").default;
    getCurrentPageInfo: typeof import("wkapi-core/dist/lib/pageInfo").default;
    report: (eventName: any, params?: {} | undefined) => void;
    reportUser: (user?: {} | undefined, userProfile?: {} | undefined) => void;
    reportLocation: (message?: {} | undefined) => void;
    reportAuthOperation: (allowed?: boolean | undefined, authType?: 0 | 1 | undefined) => void;
    pageHiden: () => void;
    appHiden: () => void;
};
export default _default;
