import wkApi from '@/wkapi-core';
import Taro from '@tarojs/taro';
import { NativeAPI, interceptTaroEvents, $getRouter } from '@/wk-taro-platform';
import { useEffect, useRef } from 'react';

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

let launchOptions;
let lastPage;

const {
  setContext,
  initLaunch,
  appShow,
  appHidden,
  pageLoad,
  pageShow,
  pageHidden,
  BURY_CONFIG_KEY,
  BURY_CONFIG_ARGUMENTS
} = wkApi,
      other = _objectWithoutPropertiesLoose(wkApi, ["setContext", "initLaunch", "appShow", "appHidden", "pageLoad", "pageShow", "pageHidden", "BURY_CONFIG_KEY", "BURY_CONFIG_ARGUMENTS"]);

setContext(Object.assign({}, NativeAPI, {
  // 规范化路由返回
  getCurrentPages() {
    const pages = NativeAPI.getCurrentPages().filter(Boolean);

    if (pages.length && lastPage) {
      // 规范化页面返回值
      const page = pages[pages.length - 1];

      if (page.route == null) {
        page.route = lastPage.path.slice(1);
      }

      if (page.options == null) {
        page.options = lastPage.params;
      }
    }

    return pages;
  }

}));
interceptTaroEvents({
  TARO_APP_EVENT_LAUNCH(app, options) {
    launchOptions = options;
    initLaunch(launchOptions);
  },

  TARO_APP_EVENT_SHOW(app) {
    appShow();
  },

  TARO_APP_EVENT_HIDE(app) {
    appHidden();
  },

  TARO_PAGE_EVENT_LOAD(page, options) {
    pageLoad();
  },

  TARO_PAGE_EVENT_SHOW(page, options) {
    try {
      lastPage = $getRouter();
    } catch (_unused) {}

    pageShow();
  },

  TARO_PAGE_EVENT_HIDE(page) {
    pageHidden();
  },

  TARO_PAGE_EVENT_UNLOAD(page) {
    pageHidden();
  }

});
/**
 * 自定义 pageKey
 * @param key
 */

function setBuryPageKey(key) {
  const page = Taro.getCurrentInstance().page;

  if (page) {
    page[BURY_CONFIG_KEY] = key;
  }
}

function setBuryPageLoadArgument(callback) {
  const page = Taro.getCurrentInstance().page;

  if (page) {
    page[BURY_CONFIG_ARGUMENTS] = callback;
  }
}

function usePageInstance() {
  const ref = useRef();

  if (ref.current == null) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    ref.current = Taro.getCurrentInstance().page;
  }

  return ref.current;
}

function useSetBuryPageKey(key) {
  const page = usePageInstance();
  useEffect(() => {
    page[BURY_CONFIG_KEY] = key;
  }, [key]);
}

function useSetBuryPageLoadArgument(callback) {
  const page = usePageInstance();
  useEffect(() => {
    page[BURY_CONFIG_ARGUMENTS] = callback;
  }, [callback]);
}

var index = _extends({}, other, {
  setBuryPageKey,
  setBuryPageLoadArgument,
  useSetBuryPageKey,
  useSetBuryPageLoadArgument
});

export default index;
//# sourceMappingURL=index.modern.js.map
