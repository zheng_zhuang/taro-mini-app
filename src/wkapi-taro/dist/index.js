var wkApi = require('@/wkapi-core');
var Taro = require('@tarojs/taro');
var wkTaroPlatform = require('@/wk-taro-platform');
var react = require('react');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var wkApi__default = /*#__PURE__*/_interopDefaultLegacy(wkApi);
var Taro__default = /*#__PURE__*/_interopDefaultLegacy(Taro);

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var launchOptions;
var lastPage;

var setContext = wkApi__default['default'].setContext,
    initLaunch = wkApi__default['default'].initLaunch,
    appShow = wkApi__default['default'].appShow,
    appHidden = wkApi__default['default'].appHidden,
    pageLoad = wkApi__default['default'].pageLoad,
    pageShow = wkApi__default['default'].pageShow,
    pageHidden = wkApi__default['default'].pageHidden,
    BURY_CONFIG_KEY = wkApi__default['default'].BURY_CONFIG_KEY,
    BURY_CONFIG_ARGUMENTS = wkApi__default['default'].BURY_CONFIG_ARGUMENTS,
    other = _objectWithoutPropertiesLoose(wkApi__default['default'], ["setContext", "initLaunch", "appShow", "appHidden", "pageLoad", "pageShow", "pageHidden", "BURY_CONFIG_KEY", "BURY_CONFIG_ARGUMENTS"]);

setContext(Object.assign({}, wkTaroPlatform.NativeAPI, {
  // 规范化路由返回
  getCurrentPages: function getCurrentPages() {
    var pages = wkTaroPlatform.NativeAPI.getCurrentPages().filter(Boolean);

    if (pages.length && lastPage) {
      // 规范化页面返回值
      var page = pages[pages.length - 1];

      if (page.route == null) {
        page.route = lastPage.path.slice(1);
      }

      if (page.options == null) {
        page.options = lastPage.params;
      }
    }

    return pages;
  }
}));
wkTaroPlatform.interceptTaroEvents({
  TARO_APP_EVENT_LAUNCH: function TARO_APP_EVENT_LAUNCH(app, options) {
    launchOptions = options;
    initLaunch(launchOptions);
  },
  TARO_APP_EVENT_SHOW: function TARO_APP_EVENT_SHOW(app) {
    appShow();
  },
  TARO_APP_EVENT_HIDE: function TARO_APP_EVENT_HIDE(app) {
    appHidden();
  },
  TARO_PAGE_EVENT_LOAD: function TARO_PAGE_EVENT_LOAD(page, options) {
    pageLoad();
  },
  TARO_PAGE_EVENT_SHOW: function TARO_PAGE_EVENT_SHOW(page, options) {
    try {
      lastPage = wkTaroPlatform.$getRouter();
    } catch (_unused) {}

    pageShow();
  },
  TARO_PAGE_EVENT_HIDE: function TARO_PAGE_EVENT_HIDE(page) {
    pageHidden();
  },
  TARO_PAGE_EVENT_UNLOAD: function TARO_PAGE_EVENT_UNLOAD(page) {
    pageHidden();
  }
});
/**
 * 自定义 pageKey
 * @param key
 */

function setBuryPageKey(key) {
  var page = Taro__default['default'].getCurrentInstance().page;

  if (page) {
    page[BURY_CONFIG_KEY] = key;
  }
}

function setBuryPageLoadArgument(callback) {
  var page = Taro__default['default'].getCurrentInstance().page;

  if (page) {
    page[BURY_CONFIG_ARGUMENTS] = callback;
  }
}

function usePageInstance() {
  var ref = react.useRef();

  if (ref.current == null) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    ref.current = Taro__default['default'].getCurrentInstance().page;
  }

  return ref.current;
}

function useSetBuryPageKey(key) {
  var page = usePageInstance();
  react.useEffect(function () {
    page[BURY_CONFIG_KEY] = key;
  }, [key]);
}

function useSetBuryPageLoadArgument(callback) {
  var page = usePageInstance();
  react.useEffect(function () {
    page[BURY_CONFIG_ARGUMENTS] = callback;
  }, [callback]);
}

var index = _extends({}, other, {
  setBuryPageKey: setBuryPageKey,
  setBuryPageLoadArgument: setBuryPageLoadArgument,
  useSetBuryPageKey: useSetBuryPageKey,
  useSetBuryPageLoadArgument: useSetBuryPageLoadArgument
});

module.exports = index;
//# sourceMappingURL=index.js.map
