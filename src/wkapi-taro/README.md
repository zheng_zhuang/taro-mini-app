# 惟客宝埋点 SDK Taro 3.x 适配版本

## 安装

```shell
$ yarn add wkapi-taro
```

<br>
<br>

## 初始化

```ts
import wkApi from 'wkapi-taro';
import React from 'react';

class App extends React.Component {
  componentDidMount() {
    const ext = getExtConfig();
    wxApi.config({
      appId: ext.appId,
      appKey: ext.maAppId,
    });
  }
}
```

> 注意： 不需要调用 initLaunch、setContext。App、页面生命周期会自动监听

<br>
<br>

## 配置页面业务参数

```ts
@connect(mapStateToProps, mapDispatchToProps, undefined, { forwardRef: true })
@hoc
class GoodsDetail extends WaitComponent {
  // ...
  constructor(props) {
    super(props);
    // 埋点:页面标识
    // 取代旧的 ftBuryPageKey
    wkApi.setBuryPageKey(buryConfig.ITEM_DETAIL.key);

    // 埋点: 页面业务参数，供埋点sdk调用 **不能删除**
    // 取代旧的 ftBuryLoadArgument
    wkApi.setBuryPageLoadArgument(() => {
      const { itemNo, activityType, activityId, distributorId, source } = this.state;

      if (itemNo || activityId) {
        return {
          itemNo,
          activityType,
          activityId,
          distributorId,
          source,
        };
      }
      return null;
    });
  }

  // ...
}
```

<br>

Hooks 版本

```ts
function Foo() {
  useSetBuryPageKey(buryConfig.ITEM_DETAIL.key);
  useSetBuryPageLoadArgument(() => {
    /*...*/
  });
}
```

<br>
<br>

## 向下兼容

其余内容和旧版本保持一致
