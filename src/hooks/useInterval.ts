import React, { useRef, useEffect } from 'react';

import usePageVisible from './usePageVisible';

/**
 * 定时执行器
 * @param option
 * @property option.duration 定时器间隔，为 0 时停止
 * @property option.immediate 执行定时器前，立即执行一次
 */
export default function useInterval({
  fn,
  duration,
  immediate,
}: {
  fn: () => void;
  duration: number;
  immediate?: boolean;
}) {
  const fnRef = useRef(fn);
  const pageVisible = usePageVisible();

  fnRef.current = fn;

  useEffect(() => {
    if (!pageVisible || duration === 0) {
      return;
    }

    const timer = setInterval(() => {
      fnRef.current();
    }, duration);

    if (immediate) {
      fnRef.current();
    }

    return () => {
      clearInterval(timer);
    };
  }, [duration, pageVisible]);
}
