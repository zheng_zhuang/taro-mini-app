import checkOptions from '@/wxat-common/utils/check-options';
import wxApi from '@/wxat-common/utils/wxApi';
import { useWait } from './use-wait';

export function useSk(cb: ($sk: any) => any) {
  const { query } = wxApi.getLaunchOptionsSync();
  useWait(async () => {
    const $sk = await checkOptions.checkOnLoadOptions(query);
    cb($sk);
  });
}
