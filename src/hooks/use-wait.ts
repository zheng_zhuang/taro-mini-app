import React, { useEffect } from 'react';

import { useSelector } from 'react-redux';

export function useWait(cb: () => any) {
  const sessionId = useSelector((state: any) => state.base.sessionId);
  useEffect(() => {
    if (sessionId) cb();
  }, [sessionId]);
}
