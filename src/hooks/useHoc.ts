/**
 * hoc 的 hooks 版本
 */
import useHoc from '../hoc/hook';

export default useHoc;
