import React, { useState } from 'react';
import { toJumpYQZ } from '@/wxat-common/utils/yqz';
import { useSelector } from 'react-redux';
import { formatLocation } from '@/wxat-common/utils/location';
import wxApi from '@/wxat-common/utils/wxApi';
import dateUtil from '@/wxat-common/utils/date';
/**
 * 页面是否可见, 可以用于任意组件
 */

const mapStateToProps = ({ base }) => ({
  gps: base.gps
});
export default function useToQYZ() {
  const { gps } = useSelector(mapStateToProps);
  const { longtitude, latitude } = gps;
  // 跳转到酒店列表
  const toYQZHotelList = async () => {
    wxApi.showLoading({
      title: '正在为您跳转',
      mask: true
    });
    const { city, district, province, cityCode, lat, lng } = await formatLocation(latitude, longtitude)
    wxApi.hideLoading();
    const query = {
      query: JSON.stringify({
        lat,
        lng,
        types: 0,
        areaName: district || '',
        cityName: city || '',
        provinceName: province || '',
        cityCode: cityCode || '',
        showAddress: city
      }),
    };
    toJumpYQZ('hotel/hotelList/hotelList', query)
  }
  // 跳转到酒店详情
  const toYQZHotelDetail = async ({itemHotelNo}) => {
    wxApi.showLoading({
      title: '正在为您跳转',
      mask: true
    });
    const { lat, lng } = await formatLocation(latitude, longtitude)
    wxApi.hideLoading();
    const query = {
      lat,
      lng,
      start_date: dateUtil.getDay(0),
      end_date: dateUtil.getDay(1),
      id: itemHotelNo
    };
    toJumpYQZ('hotel/hotelDetail/hotelDetail', query)
  }
  // 装修选项跳转
  const handleDecorateToQYZ = async (item) => {
    wxApi.showLoading({
      title: '正在为您跳转',
      mask: true
    });
    let query = {}
    const { city, district, province, cityCode, lat, lng } = await formatLocation(latitude, longtitude)
    wxApi.hideLoading();
    if (item.linkPageKey === 'yqzIndex') {
      query = {
        cityInfo: JSON.stringify({
          latitude: lat,
          longitude: lng,
          types: 0,
          areaName: district || '',
          cityName: city || '',
          provinceName: province || '',
          cityCode: cityCode || '',
          showAddress: city
        }),
      };
    }
    if (item.linkPageKey === 'yqzHotelList') {
      query = {
        query: JSON.stringify({
          lat,
          lng,
          types: 0,
          areaName: district || '',
          cityName: city || '',
          provinceName: province || '',
          cityCode: cityCode || '',
          showAddress: city
        }),
      };
    }
    toJumpYQZ(item.linkPage, query)
  }

  return {
    handleDecorateToQYZ,
    toYQZHotelList,
    toYQZHotelDetail
  }
}
