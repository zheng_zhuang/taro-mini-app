import React, { useEffect } from 'react';

import wxApi from '@/wxat-common/utils/wxApi';
import usePageVisible from './usePageVisible';

/**
 * 提高屏幕亮度
 */
export function useScreenHightlight(maxBrightness = 0.8) {
  const pageVisible = usePageVisible();
  useEffect(() => {
    if (pageVisible) {
      let exited = false;
      let setted = false;
      let org = 0.5;
      (async () => {
        const { value } = await wxApi.getScreenBrightness();
        if (exited) return;
        if (value < maxBrightness) {
          org = value;
          console.log('正在调高屏幕亮度', maxBrightness);
          wxApi.setScreenBrightness({ value: maxBrightness });
          setted = true;
        }
      })();

      return () => {
        exited = true;
        // 恢复
        if (setted) {
          console.log('正在恢复屏幕亮度', org);
          wxApi.setScreenBrightness({ value: org });
        }
      };
    }
    return undefined;
  }, [pageVisible]);
}
