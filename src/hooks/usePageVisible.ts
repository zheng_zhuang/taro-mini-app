import React, { useState } from 'react';
import { useDidShow, useDidHide } from '@tarojs/taro';

/**
 * 页面是否可见, 可以用于任意组件
 */
export default function usePageVisible() {
  // TODO: 获取初始状态
  const [visible, setVisible] = useState(true);

  useDidShow(() => {
    setVisible(true);
  });

  useDidHide(() => {
    setVisible(false);
  });

  return visible;
}
