import React, { useMemo, useEffect } from 'react';
import { useSelector } from 'react-redux';

import t from '@/wxat-common/utils/template';
import wxApi from '@/wxat-common/utils/wxApi';
import { NOOP_OBJECT } from '@/wxat-common/utils/noop';

const selectGlobalData = (state) => state.globalData;

const DEFAULT_OPTIONS = {};

/**
 * 获取模板样式
 */
export default function useTemplateStyle(options: { autoSetNavigationBar?: boolean } = NOOP_OBJECT) {
  const finalOptions = { ...DEFAULT_OPTIONS, ...options };
  const globalData = useSelector(selectGlobalData);
  const templ = useMemo(() => t.getTemplateStyle(globalData), [globalData]);

  useEffect(() => {
    if (finalOptions.autoSetNavigationBar && templ && templ.titleColor) {
      wxApi.setNavigationBarColor({
        frontColor: '#ffffff', // 必写项
        backgroundColor: templ.titleColor, // 必写项
      });
    }
  }, [templ]);

  return templ;
}
