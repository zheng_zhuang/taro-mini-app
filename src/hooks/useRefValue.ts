import React, { useRef } from 'react';

export default function useRefValue<T>(value: T) {
  const ref = useRef<T>(value);
  ref.current = value;

  return ref;
}
