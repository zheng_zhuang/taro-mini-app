import React, { useState, useRef, useEffect } from 'react';

import timer from '@/wxat-common/utils/timer';
import usePageVisible from './usePageVisible';

/**
 * 时间流逝器, 可以用于计算倒计时
 */
export default function useTimeElapse(condition: boolean, updator?: (elapse: number) => void) {
  const start = useRef<number>();
  const [timeElapse, setTimeElapse] = useState(0);
  const pageVisible = usePageVisible();
  // 页面隐藏时停止计时器
  condition = condition && pageVisible;

  useEffect(() => {
    if (start.current == null) {
      start.current = Date.now();
    }

    if (condition) {
      const timerRef = timer.addQueue(() => {
        // eslint-disable-next-line
        const elapse = Date.now() - start.current!;
        setTimeElapse(elapse);
        if (updator) {
          updator(elapse);
        }
      });
      return () => timer.deleteQueue(timerRef);
    }
  }, [condition]);

  return timeElapse;
}
