import React, { useRef } from 'react';

import protectedMailBox from '@/wxat-common/utils/protectedMailBox';

export default function useProtectedMailBox<T>(name): T {
  const value = useRef();

  if (value.current == null) {
    value.current = protectedMailBox.read(name);
  }

  // @ts-expect-error
  return value.current as T;
}
