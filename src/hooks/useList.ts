import React, { useState, useEffect, useRef } from 'react';
import { NOOP_ARRAY } from '@/wxat-common/utils/noop';
import { useReachBottom, useDidShow } from '@tarojs/taro';
import useRefValue from './useRefValue';

export enum ListStatus {
  Initial,
  Loading,
  Error,
}

/**
 * 简单列表加载
 */
export default function useList<T>(options: {
  /**
   * 分页大小，默认 10
   */
  pageSize?: number;
  /**
   * API 请求
   */
  query: (page: number, pageSize: number, list: T[]) => Promise<T[]>;
  /**
   * 页面在重新显示时是否重新刷新, 默认为 true
   */
  resetOnReshow?: boolean;
  /**
   * 是否监听 reach bottom, 默认为 true
   */
  listenReachBottom?: boolean;
}) {
  const { pageSize = 10, resetOnReshow = true, query, listenReachBottom = true } = options;
  const [page, setPage] = useState(0);
  const [list, setList] = useState<T[]>(NOOP_ARRAY);
  const [hasMore, setHasMore] = useState(true);
  const [status, setStatus] = useState<ListStatus>(ListStatus.Initial);
  const [error, setError] = useState<any>(null);
  const [fetchId, setFetchId] = useState(0);
  const fetchIdRef = useRefValue(fetchId);
  const loaded = useRef(false);

  const fetch = async () => {
    if (status === ListStatus.Loading || !hasMore) {
      return;
    }

    try {
      const currentFetchId = fetchId;
      setError(null);
      setStatus(ListStatus.Loading);
      const newPage = page + 1;
      const newList = await query(newPage, pageSize, list);

      // 有更新的请求发起
      if (currentFetchId !== fetchIdRef.current) {
        return;
      }
      if (newList.length < pageSize) {
        setHasMore(false);
      }
      setPage(newPage);
      setList(list.concat(newList));
      setStatus(ListStatus.Initial);
      loaded.current = true;
    } catch (err) {
      setError(err);
      setStatus(ListStatus.Error);
    }
  };

  const load = () => {
    setFetchId((c) => {
      return (fetchIdRef.current = c + 1);
    });
  };

  const forceLoad = () => {
    setPage(0);
    setHasMore(true);
    setList(NOOP_ARRAY);
    setStatus(ListStatus.Initial);
    setError(null);

    // 触发重新加载列表
    load();
  };

  useDidShow(() => {
    // 强制重新刷新
    // 避免和useEffect 重发
    if (resetOnReshow && loaded.current) {
      forceLoad();
    }
  });

  useReachBottom(() => {
    if( fetchId > 0 ){
      if (listenReachBottom) {
        load();
      }
    }
  });

  useEffect(() => {
    fetch();
  }, [fetchId]);

  return {
    load: load,
    retry: load,
    reload: forceLoad,
    empty: list.length === 0 && !hasMore,
    page,
    list,
    hasMore,
    status,
    error,
  };
}
