import React, { Component } from 'react';

import login from '@/wxat-common/x-login';
import wxApi from '@/wxat-common/utils/wxApi';
import store from '@/store';

export abstract class WaitComponent<P = {}, S = {}> extends Component<P, S> {
  componentDidMount() {
    super.componentDidMount && super.componentDidMount();
    // 不用 connect，因为不考虑 update
    const state = store.getState();
    if (state.base.sessionId) {
      this.safeCallOnWait();
    } else {
      wxApi.eventCenter.once(login.EVENT_LOGIN_SUCCESS, this.safeCallOnWait);
    }
  }

  abstract onWait(): void;

  private safeCallOnWait = () => {
    if (this.onWait) {
      this.onWait();
    }
  };
}
