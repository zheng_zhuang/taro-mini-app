import React, {ComponentClass} from 'react';
import pageLinkHandler from '@/wxat-common/components/decorate/utils/pageLinkHandler';

import {connect} from 'react-redux';
import wxApi from '@/wxat-common/utils/wxApi';
import api from "@/wxat-common/api";

export type Tabbars = Array<{ pagePath: string; realPath: string }>;

export type QRCodeRedirectPageProps = {
  tabbars: Tabbars;
};

// kv to qs
function qs(obj: Record<string, string | number | boolean>) {
  return Object.keys(obj)
    .reduce((str, key) => `${str}&${key}=${obj[key]}`, '')
    .slice(1);
}

const mapStateToProps = (state) => {
  let tabbars: Tabbars = [];

  if (state.globalData.tabbars && state.globalData.tabbars.list) {
    tabbars = state.globalData.tabbars.list;
  }

  return {
    tabbars,
  };
};

export function QRCodeRedirectPage<T extends ComponentClass>(OwnerClass: T) {
  const WrapperClass = (OwnerClass as unknown) as ComponentClass<QRCodeRedirectPageProps>;

  class WrapperComponent extends WrapperClass {
    componentDidMount() {
      if (super.componentDidMount) {
        super.componentDidMount();
      }
      console.error('临时解决：taro 小程序与 原小程序路径不一致的问题');


      let {path, query} = wxApi.getLaunchOptionsSync();
      // 住中码逻辑处理
      let params = {};
      if (query.scene) {
        let scene = decodeURIComponent(query.scene);
        scene.split('&').forEach(item => {
          const key = item.split('=')[0];
          const value = item.split('=')[1];
          params[key] = value;
        })
        wxApi.request({
          url: api.queryQrCodeScenes,
          isLoginRequest: true,
          method: 'GET',
          data: {
            id: params.id
          },
        }).then(res => {
          const dataObjII: any = {};
          res.data.split('&').forEach(item => {
            const key = item.split('=')[0];
            const value = item.split('=')[1];
            dataObjII[key] = value;
          })
          // 自定义页
          if (dataObjII.pId) {
            let str = scene + "&pId=" + dataObjII.pId;
            console.log("Str", str)
            query = (str);
          }
          this.handleLinkPage(path, query)
        })
      } else {
        this.handleLinkPage(path, qs(query))
      }
      // const {linkPage} = pageLinkHandler.getLinkPage({linkPage: `${path}?${qs(query)}`});
      //
      // console.log("new linkPage",linkPage)
      //
      // if (!linkPage.includes(path.replace(/^\//, ''))) {
      //   wxApi.$redirectTo({url: linkPage});
      //   return;
      // }
      //
      // this.redirectTabbar();
    }

    componentDidUpdate(preProps: QRCodeRedirectPageProps) {
      if (preProps.tabbars !== this.props.tabbars) {
        this.redirectTabbar();
      }
    }

    lookup = [
      {
        realPath: 'wxat-common/pages/tabbar-integral/index',
        pagePath: 'wxat-common/pages/slot-page/index?tabType=all&$componentId=tabbar-integral&$title=积分商城',
      },

      {
        realPath: 'wxat-common/pages/tabbar-internal-purchase/index',
        pagePath: 'wxat-common/pages/slot-page/index?tabType=all&$componentId=tabbar-internal-purchase&$title=内购专场',
      },

      {
        realPath: 'wxat-common/pages/tabbar-order-list/index',
        pagePath: 'wxat-common/pages/slot-page/index?tabType=all&$componentId=tabbar-order-list&$title=订单',
      },

      {
        realPath: 'wxat-common/pages/tabbar-shopping-cart/index',
        pagePath: 'wxat-common/pages/shopping-cart/index',
      },

      {
        realPath: 'wxat-common/pages/tabbar-coupon-center/index',
        pagePath: 'wxat-common/pages/slot-page/index?tabType=all&$componentId=tabbar-coupon-center&$title=领券中心',
      },

      {
        realPath: 'wxat-common/pages/tabbar-custom/index',
        pagePath: 'wxat-common/pages/slot-page/index?$componentId=custom',
      },
    ];

    handleLinkPage(path, query) {
      const {linkPage} = pageLinkHandler.getLinkPage({linkPage: `${path}?${(query)}`});

      console.log("new linkPage", linkPage)

      if (!linkPage.includes(path.replace(/^\//, ''))) {
        console.log(linkPage)
        wxApi.$redirectTo({url: linkPage});
        return;
      }

      this.redirectTabbar();
    }

    /**
     * 优先跳 tabbar
     * 若 tabbar 未配置改页面，跳 wxat-common/pages/slot-page/index
     */
    redirectTabbar = () => {
      const {tabbars} = this.props;
      const {path, query} = wxApi.getLaunchOptionsSync();
      let page = tabbars.find((t) => t.realPath.includes(path));
      if (!page && tabbars.length) {
        page = this.lookup.find((t) => t.realPath.includes(path));
        page && wxApi.$redirectTo({url: `${page.pagePath}${page.pagePath.includes('?') ? '&' : '?'}${qs(query)}`});
        return;
      }
      if (page) {
        if (process.env.TARO_ENV === 'h5') {
          wxApi.$redirectTo({url: `${page.pagePath}${page.pagePath.includes('?') ? '&' : '?'}${qs(query)}`});
        } else {
          wxApi.switchTab({url: `${page.pagePath}${page.pagePath.includes('?') ? '&' : '?'}${qs(query)}`})
        }
      }
    };
  }

  return (connect(mapStateToProps, undefined, undefined, {forwardRef: true})(WrapperComponent) as unknown) as T;
}
