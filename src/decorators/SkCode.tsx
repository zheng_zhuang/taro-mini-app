import { $getRouter } from 'wk-taro-platform';
import checkOptions from '@/wxat-common/utils/check-options';
import { WaitComponent } from './Wait';

export class SkCodeComponent<P = {}, S = {}> extends WaitComponent<P, S> {
  $sk = {};
  $router = $getRouter();

  async onWait() {
    this.$sk = await checkOptions.checkOnLoadOptions(this.$router.params);
  }
}
